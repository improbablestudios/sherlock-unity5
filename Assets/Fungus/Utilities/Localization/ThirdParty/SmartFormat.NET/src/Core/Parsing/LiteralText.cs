﻿using System;
using System.Text;
using ImprobableStudios.Localization.SmartFormat.Core.Settings;

namespace ImprobableStudios.Localization.SmartFormat.Core.Parsing
{
    /// <summary>
    /// Represents the literal text that is found
    /// in a parsed format string.
    /// </summary>
    public class LiteralText : FormatItem
    {
        public LiteralText(SmartSettings smartSettings, Format parent, int startIndex) : base(smartSettings, parent, startIndex)
        {
        }

        public LiteralText(SmartSettings smartSettings, Format parent) : base(smartSettings, parent, parent.startIndex)
        {
        }

        public override string ToString()
        {
            return SmartSettings.ConvertCharacterStringLiterals
                ? ConvertCharacterLiteralsToUnicode()
                : baseString.Substring(startIndex, endIndex - startIndex);
        }

        private string ConvertCharacterLiteralsToUnicode()
        {
            var source = this.baseString.Substring(startIndex, endIndex - startIndex);

            // No character literal escaping - nothing to do
            if (source[0] != Parser.CharLiteralEscapeChar)
                return source;

            // The string length should be 2: espace character \ and literal character
            if (source.Length < 2)
            {
                throw new ArgumentException(string.Format("Missing escape sequence in literal: \"{0}\"", source));
            }

            char c;
            switch (source[1])
            {
                case '\'':
                    c = '\'';
                    break;
                case '\"':
                    c = '\"';
                    break;
                case '\\':
                    c = '\\';
                    break;
                case '0':
                    c = '\0';
                    break;
                case 'a':
                    c = '\a';
                    break;
                case 'b':
                    c = '\b';
                    break;
                case 'f':
                    c = '\f';
                    break;
                case 'n':
                    c = '\n';
                    break;
                case 'r':
                    c = '\r';
                    break;
                case 't':
                    c = '\t';
                    break;
                case 'v':
                    c = '\v';
                    break;
                default:
                    throw new ArgumentException(string.Format("Unrecognized escape sequence in literal: \"{0}\"", source));
            }

            return c.ToString();
        }
    }
}
