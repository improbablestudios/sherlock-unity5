﻿namespace ImprobableStudios.Localization.SmartFormat.Core.Settings
{
    public enum CaseSensitivityType
    {
        CaseSensitive,
        CaseInsensitive
    }
}