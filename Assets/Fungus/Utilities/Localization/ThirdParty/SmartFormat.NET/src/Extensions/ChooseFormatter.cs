﻿using System;
using System.Collections.Generic;
using ImprobableStudios.Localization.SmartFormat.Core.Extensions;
using ImprobableStudios.Localization.SmartFormat.Core.Parsing;

namespace ImprobableStudios.Localization.SmartFormat.Extensions
{
    public class ChooseFormatter : IFormatter
    {
        public string[] Names { get; set; }

        private char _splitChar = '|';
        public char SplitChar { get { return _splitChar;  } set { _splitChar = value; } }

        public ChooseFormatter()
        {
            Names = new string[] { "choose", "c" };
        }

        public bool TryEvaluateFormat(IFormattingInfo formattingInfo)
        {
            string[] chooseOptions = formattingInfo.FormatterOptions.Split(_splitChar);
            IList<Format> formats = formattingInfo.Format.Split(_splitChar);

            Format chosenFormat = DetermineChosenFormat(formattingInfo, formats, chooseOptions);
            
            formattingInfo.Write(chosenFormat, formattingInfo.CurrentValue);
            
            return true;
        }

        private static Format DetermineChosenFormat(IFormattingInfo formattingInfo, IList<Format> choiceFormats, string[] chooseOptions)
        {
            object currentValue = formattingInfo.CurrentValue;
            string currentValueString = (currentValue == null) ? "null" : currentValue.ToString();
            bool hasFormatterOptions = !string.IsNullOrEmpty(formattingInfo.FormatterOptions);

            int chosenIndex = -1;
            if (!hasFormatterOptions && currentValue is int)
            {
                chosenIndex = (int)currentValue;
            }
            else
            {
                chosenIndex = Array.IndexOf(chooseOptions, currentValueString);
            }
            
            // Validate the number of formats:
            if (hasFormatterOptions)
            {
                if (choiceFormats.Count < chooseOptions.Length)
                {
                    throw formattingInfo.FormattingException("You must specify at least " + chooseOptions.Length + " choices");
                }
                else if (choiceFormats.Count > chooseOptions.Length + 1)
                {
                    throw formattingInfo.FormattingException("You cannot specify more than " + (chooseOptions.Length + 1) + " choices");
                }
                else if (chosenIndex == -1 && choiceFormats.Count == chooseOptions.Length)
                {
                    throw formattingInfo.FormattingException("\"" + currentValueString + "\" is not a valid choice, and a \"default\" choice was not supplied");
                }
            }
            else
            {
                if (choiceFormats.Count == 0)
                {
                    throw formattingInfo.FormattingException("If using a choose token with no formatter options, you must specify at least 1 choice");
                }
                else if (!(currentValue is int))
                {
                    throw formattingInfo.FormattingException("If using a choose token with no formatter options, " + "\"" + currentValueString + "\" must be an int to specify the index of the choice to use");
                }
                else if (chosenIndex < 0 || chosenIndex >= choiceFormats.Count)
                {
                    throw formattingInfo.FormattingException("If using a choose token with no formatter options, " + "\"" + currentValueString + "\" must be within the range of 0 to " + (choiceFormats.Count - 1));
                }
            }

            if (chosenIndex == -1) {
                chosenIndex = choiceFormats.Count - 1;
            }

            Format chosenFormat = choiceFormats[chosenIndex];
            return chosenFormat;
        }
    }
}