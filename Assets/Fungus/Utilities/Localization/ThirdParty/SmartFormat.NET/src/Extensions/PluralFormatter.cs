﻿using System;
using System.Globalization;
using System.Linq;
using ImprobableStudios.Localization.SmartFormat.Core.Extensions;
using ImprobableStudios.Localization.SmartFormat.Core.Formatting;
using ImprobableStudios.Localization.SmartFormat.Utilities;
using System.Collections.Generic;
using System.Reflection;

namespace ImprobableStudios.Localization.SmartFormat.Extensions
{
    public class PluralFormatter : IFormatter
    {
        private char _splitChar = ',';

        public string[] Names { get; set; }

        public Dictionary<string, PluralRule.PluralRuleDelegate> PluralRules { get; set; }

        /// <summary>
        /// Initializes the plugin with rules for many common languages.
        /// </summary>
        public PluralFormatter()
        {
            Names = new string[] { "plural", "p", "" };
            PluralRules = DefaultPluralRules.GetDefaultPluralRules();
        }
        
        private bool TryParsePluralType(string str, out PluralRule.Type pluralType)
        {
            foreach (PluralRule.Type pt in Enum.GetValues(typeof(PluralRule.Type)))
            {
                string enumName = Enum.GetName(typeof(PluralRule.Type), pt);
                if (MatchesEnumName(enumName, str))
                {
                    pluralType = pt;
                    return true;
                }
            }
            pluralType = PluralRule.Type.Cardinal;
            return false;
        }

        /// <summary>
        /// Support option "cardinal" or "c" for cardinal pluralization and "ordinal" or "o" for ordinal pluralization
        /// </summary>
        private bool IsPluralType(string str)
        {
            foreach (PluralRule.Type pt in Enum.GetValues(typeof(PluralRule.Type)))
            {
                string enumName = Enum.GetName(typeof(PluralRule.Type), pt);
                if (MatchesEnumName(enumName, str))
                {
                    return true;
                }
                if (MatchesFirstLetterOfEnumName(enumName, str))
                {
                    return true;
                }
            }
            return false;
        }

        private bool MatchesEnumName(string enumName, string str)
        {
            if (!string.IsNullOrEmpty(enumName) && !string.IsNullOrEmpty(str))
            {
                return enumName.ToLower() == str.ToLower();
            }
            return false;
        }

        private bool MatchesFirstLetterOfEnumName(string enumName, string str)
        {
            if (!string.IsNullOrEmpty(enumName) && !string.IsNullOrEmpty(str))
            {
                return enumName.ToLower()[0].ToString() == str.ToLower();
            }
            return false;
        }

        private bool IsNumberFormatType(string str, object value)
        {
            if (IsNumber(value))
            {
                try
                {
                    string.Format("{0:" + str + "}", value);
                    return true;
                }
                catch
                {
                }
            }
            return false;
        }
        
        private bool IsNumber(object value)
        {
            return
                value is sbyte ||
                value is byte ||
                value is short ||
                value is int ||
                value is long ||
                value is float ||
                value is double ||
                value is decimal ||
                value is ushort ||
                value is uint ||
                value is ulong;
        }

        private decimal ConvertFormattedNumberToDecimal(string numberFormatType, object current)
        {
            decimal value;

            try
            {
                var valueAsFormattedString = string.Format(CultureInfo.InvariantCulture, "{0:" + numberFormatType + "}", current);
                value = decimal.Parse(valueAsFormattedString, CultureInfo.InvariantCulture);

                // Workaround for parsing 0.000...
                var numberOfFractionalDigitsInValue = BitConverter.GetBytes(decimal.GetBits(value)[3])[2];
                if (value == 0m && numberOfFractionalDigitsInValue == 0)
                {
                    var oneAsFormattedString = string.Format(CultureInfo.InvariantCulture, "{0:" + numberFormatType + "}", 1);
                    value = decimal.Parse(oneAsFormattedString, CultureInfo.InvariantCulture) - 1;
                }
            }
            catch
            {
                value = Convert.ToDecimal(current);
            }

            return value;
        }
        
        private PluralRule.PluralRuleDelegate GetPluralRule(string twoLetterISOLanguageName)
        {
            if (PluralRules.ContainsKey(twoLetterISOLanguageName))
            {
                return PluralRules[twoLetterISOLanguageName];
            }
            return null;
        }

        private PluralRule.PluralRuleDelegate GetPluralRule(IFormattingInfo formattingInfo)
        {
            var current = formattingInfo.CurrentValue;

            // See if the type and/or language was explicitly passed:
            var pluralOptions = formattingInfo.FormatterOptions.Split(_splitChar);
            if (pluralOptions.Length > 0 && !string.IsNullOrEmpty(pluralOptions[0]))
            {
                if (!IsPluralType(pluralOptions[0]) && !IsNumberFormatType(pluralOptions[0], current))
                {
                    return GetPluralRule(pluralOptions[0]);
                }
            }

            var provider = formattingInfo.FormatDetails.Provider;
            var cultureInfo = provider as CultureInfo;

            // See if a CustomPluralRuleProvider is available from the FormatProvider:
            if (provider != null)
            {
                var pluralRuleProvider = (CustomPluralRuleProvider)provider.GetFormat(typeof(CustomPluralRuleProvider));
                if (pluralRuleProvider != null)
                {
                    if (cultureInfo != null)
                    {
                        return pluralRuleProvider.GetPluralRule(cultureInfo.TwoLetterISOLanguageName);
                    }
                    else
                    {
                        return pluralRuleProvider.GetPluralRule();
                    }
                }
            }

            // Use CultureInfo.CurrentCulture, if no culture info is provided:
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.CurrentCulture;
            }
            var culturePluralRule = GetPluralRule(cultureInfo.TwoLetterISOLanguageName);
            return culturePluralRule;
        }

        public bool TryEvaluateFormat(IFormattingInfo formattingInfo)
        {
            var format = formattingInfo.Format;
            var current = formattingInfo.CurrentValue;
            var provider = formattingInfo.FormatDetails.Provider;

            // Ignore formats that start with "?" (this can be used to bypass this extension)
            if (format == null || format.baseString[format.startIndex] == ':')
            {
                return false;
            }

            // Extract options the format string:
            var pluralType = PluralRule.Type.Cardinal;
            var numberFormatType = "";
            
            var pluralOptions = formattingInfo.FormatterOptions.Split(_splitChar);
            foreach (string option in pluralOptions)
            {
                PluralRule.Type pt;
                if (TryParsePluralType(option, out pt))
                {
                    pluralType = pt;
                    continue;
                }
                if (IsNumberFormatType(option, current))
                {
                    numberFormatType = option;
                    continue;
                }
            }

            // Extract the plural words from the format string:
            var pluralWords = format.Split('|');
            // This extension requires at least two plural words:
            if (pluralWords.Count == 1) return false;

            decimal value;
            
            // We can format numbers, and IEnumerables. For IEnumerables we look at the number of items
            // in the collection: this means the user can e.g. use the same parameter for both plural and list, for example
            // 'Smart.Format("The following {0:plural:person is|people are} impressed: {0:list:{}|, |, and}", new[] { "bob", "alice" });'
            if (IsNumber(current))
            {
                // Need to ensure we convert the value to a decimal with the correct number of decimal places
                value = ConvertFormattedNumberToDecimal(numberFormatType, current);
            }
            else if (current is IEnumerable<object>)
            {
                // Relay on IEnumerable covariance, but don't care about non-generic IEnumerable
                value = ((IEnumerable<object>)current).Count();
            }
            else
            {
                // This extension only permits numbers and IEnumerables
                return false;
            }


            // Get the plural rule:
            var pluralRule = GetPluralRule(formattingInfo);

            if (pluralRule == null)
            {
                // Not a supported language.
                return false;
            }

            var pluralIndex = pluralRule(pluralType, value);
            
            if (pluralIndex < 0 || pluralWords.Count <= pluralIndex)
            {
                // The plural rule should always return a value in-range!
                throw new FormattingException(format, "Invalid number of plural parameters", pluralWords.Last().endIndex);
            }

            // Output the selected word (allowing for nested formats):
            var pluralForm = pluralWords[pluralIndex];
            formattingInfo.Write(pluralForm, current);
            return true;
        }
    }

    /// <summary>
    /// Use this class to provide custom plural rules to Smart.Format
    /// </summary>
    public class CustomPluralRuleProvider : IFormatProvider
    {
        public Dictionary<string, PluralRule.PluralRuleDelegate> PluralRules { get; set; }

        public string DefaultPluralRuleKey { get; set; }

        public object GetFormat(Type formatType)
        {
            return (formatType == typeof(CustomPluralRuleProvider)) ? this : null;
        }

        public CustomPluralRuleProvider(Dictionary<string, PluralRule.PluralRuleDelegate> pluralRules, string defaultPluralRuleKey)
        {
            PluralRules = pluralRules;
            DefaultPluralRuleKey = defaultPluralRuleKey;
        }

        public PluralRule.PluralRuleDelegate GetPluralRule(string twoLetterISOLanguageName)
        {
            if (PluralRules.ContainsKey(twoLetterISOLanguageName))
            {
                return PluralRules[twoLetterISOLanguageName];
            }
            return null;
        }

        public PluralRule.PluralRuleDelegate GetPluralRule()
        {
            if (PluralRules.ContainsKey(DefaultPluralRuleKey))
            {
                return PluralRules[DefaultPluralRuleKey];
            }
            return null;
        }
    }

}
