﻿using System;

namespace ImprobableStudios.Localization.SmartFormat.Utilities
{
    public static class PluralRule
    {
        public enum Type
        {
            Cardinal,
            Ordinal,
            Range,
        }

        /// <summary>
        /// Determines which singular or plural word
        /// should be chosen for the given quantity
        ///
        /// This allows each language to define its own behavior
        /// for singular or plural words.
        ///
        /// It should return the index of the correct parameter.
        /// </summary>
        /// <param name="pluralType">The type of plural (cardinal = 1,2,3) (ordinal = 1st, 2nd, 3rd) (range = 1-2, 3-4, 5-6)</param>
        /// <param name="d">The decimal value that is being referenced by the singular or plural words</param>
        /// <returns></returns>
        public delegate int PluralRuleDelegate(Type pluralType, decimal value);

        /// <summary>
        /// Returns True if the value is an integer inclusively between the min and max.
        /// </summary>
        public static bool In(this decimal value, decimal min, decimal max)
        {
            return (value % 1 == 0) && (value >= min) && (value <= max);
        }

        /// <summary>
        /// n absolute value of the source number (integer and decimals).
        /// </summary>
        public static decimal N(this decimal value)
        {
            return Math.Abs(value);
        }

        /// <summary>
        /// i integer digits of n.
        /// </summary>
        public static decimal I(this decimal value)
        {
            decimal n = value.N();
            return Math.Truncate(n);
        }

        /// <summary>
        /// v number of visible fraction digits in n, with trailing zeros.
        /// </summary>
        public static decimal V(this decimal value)
        {
            decimal n = value.N();
            return BitConverter.GetBytes(decimal.GetBits(n)[3])[2];
        }

        /// <summary>
        /// w number of visible fraction digits in n, without trailing zeros.
        /// </summary>
        public static decimal W(this decimal value)
        {
            decimal n = value.N();
            n = Math.Abs(n);
            n -= Math.Truncate(n);
            decimal decimalPlaces = 0;
            while (n > 0)
            {
                decimalPlaces++;
                n *= 10;
                n -= (int)n;
            }
            return decimalPlaces;
        }

        /// <summary>
        /// f visible fractional digits in n, with trailing zeros.
        /// </summary>
        public static decimal F(this decimal value)
        {
            decimal n = value.N();
            decimal numOfFractionalDigitsWithTrailingZeros = n.V();
            return (int)((n - Decimal.Truncate(n)) * (decimal)Math.Pow(10, (double)numOfFractionalDigitsWithTrailingZeros));
        }

        /// <summary>
        /// t visible fractional digits in n, without trailing zeros.
        /// </summary>
        public static decimal T(this decimal value)
        {
            decimal n = value.N();
            decimal numOfFractionalDigitsWithoutTrailingZeros = n.W();
            return (int)((n - Decimal.Truncate(n)) * (decimal)Math.Pow(10, (double)numOfFractionalDigitsWithoutTrailingZeros));
        }
    }
}