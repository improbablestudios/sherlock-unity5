﻿using System.Collections.Generic;

namespace ImprobableStudios.Localization.SmartFormat.Utilities
{
    public static class DefaultPluralRules
    {
        public static Dictionary<string, PluralRule.PluralRuleDelegate> GetDefaultPluralRules()
        {
            return new Dictionary<string, PluralRule.PluralRuleDelegate>()
            {
                {
                    "af",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ak",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "am",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ar",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 0) return 0; // zero
                                if (d.N() == 1) return 1; // one
                                if (d.N() == 2) return 2; // two
                                if ((d.N() % 100).In(3, 10)) return 3; // few
                                if ((d.N() % 100).In(11, 99)) return 4; // many
                                return 5; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ars",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 0) return 0; // zero
                                if (d.N() == 1) return 1; // one
                                if (d.N() == 2) return 2; // two
                                if ((d.N() % 100).In(3, 10)) return 3; // few
                                if ((d.N() % 100).In(11, 99)) return 4; // many
                                return 5; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "as",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1 || d.N() == 5|| d.N() == 7 || d.N() == 8 || d.N() == 9 || d.N() == 10) return 0; // one
                                if (d.N() == 2 || d.N() == 3) return 1; // two
                                if (d.N() == 4) return 2; // few
                                if (d.N() == 6) return 3; // many
                                return 4; // other
                        }
                        return -1;
                    }
                },
                {
                    "ast",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "asa",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "az",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if ((d.I() % 10 == 1 || d.I() % 10 == 2 || d.I() % 10 == 5 || d.I() % 10 == 7 || d.I() % 10 == 8) ||
                                    (d.I() % 100 == 20 || d.I() % 100 == 50 || d.I() % 100 == 70 || d.I() % 100 == 80)) return 0; // one
                                if ((d.I() % 10 == 3 || d.I() % 10 == 4) ||
                                    (d.I() % 1000 == 100 || d.I() % 1000 == 200 || d.I() % 1000 == 300 || d.I() % 1000 == 400 || d.I() % 1000 == 500 || d.I() % 1000 == 600 || d.I() % 1000 == 700 || d.I() % 1000 == 800 || d.I() % 1000 == 900)) return 1; // few
                                if (d.I() == 0 || d.I() % 10 == 6 || (d.I() % 100 == 40 || d.I() % 100 == 60 || d.I() % 100 == 90)) return 2; // many
                                return 3; // other
                        }
                        return -1;
                    }
                },
                {
                    "be",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() % 10 == 1 && d.N() % 100 != 11) return 0; // one
                                if ((d.N() % 10).In(2, 4) && !(d.N() % 100).In(12, 14)) return 1; // few
                                if (d.N() % 10 == 0 || (d.N() % 10).In(5, 9) || (d.N() % 100).In(11, 14)) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                if ((d.N() % 10 == 2 || d.N() % 10 == 3) && !(d.N() % 100 == 12 || d.N() % 100 == 13)) return 0; // few
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "bem",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "bez",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "bg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "bh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "bm",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "bn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1 || d.N() == 5 || d.N() == 7 || d.N() == 7 || d.N() == 8 || d.N() == 9 || d.N() == 10) return 0; // one
                                if (d.N() == 2 || d.N() == 3) return 1; // two
                                if (d.N() == 4) return 2; // few
                                if (d.N() == 6) return 3; // many
                                return 4; // other
                        }
                        return -1;
                    }
                },
                {
                    "bo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "br",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() % 10 == 1 && !(d.N() % 100 == 11 || d.N() % 100 == 71 || d.N() % 100 == 91)) return 0; // one
                                if (d.N() % 10 == 2 && !(d.N() % 100 == 12 || d.N() % 100 == 72 || d.N() % 100 == 92)) return 1; // two
                                if (((d.N() % 10).In(3, 4) || (d.N() % 10) == 9) && !((d.N() % 100).In(10, 19) || (d.N() % 100).In(70, 79) || (d.N() % 100).In(90,99))) return 2; // few
                                if (d.N() != 0 && d.N() % 1000000 == 0) return 3; // many
                                return 4; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "brx",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "bs",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11 || d.F() % 10 == 1 && d.F() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14) || (d.F() % 10).In(2, 4) && !(d.F() % 100).In(12, 14)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ca",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1 || d.N() == 3) return 0; // one
                                if (d.N() == 2) return 1; // two
                                if (d.N() == 4) return 2; // few
                                return 3; // other
                        }
                        return -1;
                    }
                },
                {
                    "ce",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "cgg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "chr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ckb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/e
                        }
                        return -1;
                    }
                },
                {
                    "cs",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.I().In(2, 4) && d.V() == 0) return 1; // few
                                if (d.V() != 0) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "cy",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 0) return 0; // zero
                                if (d.N() == 1) return 1; // one
                                if (d.N() == 2) return 2; // two
                                if (d.N() == 3) return 3; // few
                                if (d.N() == 6) return 4; // many
                                return 5; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 0 || d.N() == 7 || d.N() == 8 || d.N() == 9) return 0; // zero
                                if (d.N() == 1) return 1; // one
                                if (d.N() == 2) return 2; // two
                                if (d.N() == 3 || d.N() == 4) return 3; // few
                                if (d.N() == 5 || d.N() == 6) return 4; // many
                                return 5; // other
                        }
                        return -1;
                    }
                },
                {
                    "da",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1 || d.T() != 0 && (d.I() == 0 || d.I() == 1)) return 0; // zero
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "de",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // zero
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "dsb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 100 == 1 || d.F() % 100 == 1) return 0; // one
                                if (d.V() == 0 && d.I() % 100 == 2 || d.F() % 100 == 2) return 1; // two
                                if (d.V() == 0 && (d.I() % 100).In(3, 4) || (d.F() % 100).In(3, 4)) return 2; // few
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "dv",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "dz",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ee",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "el",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "en",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() % 10 == 1 && d.N() % 100 != 11) return 0; // one
                                if (d.N() % 10 == 2 && d.N() % 100 != 12) return 1; // two
                                if (d.N() % 10 == 3 && d.N() % 100 != 13) return 2; // few
                                return 3; // other
                        }
                        return -1;
                    }
                },
                {
                    "eo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "es",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "et",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "eu",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "fa",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ff",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.I() ==  1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "fi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "fil",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && 
                                (d.I() == 1 || d.I() == 2 || d.I() == 3) || d.V() == 0 && 
                                !((d.I() % 10) == 4 || (d.I() % 10) == 6 || (d.I() % 10) == 9) || d.V() != 0 && 
                                !((d.F() % 10) == 4 || (d.F() % 10) == 6 || (d.F() % 10) == 9)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "fo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "fr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.I() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "fur",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "fy",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ga",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                if (d.N().In(3, 6)) return 2; // few
                                if (d.N().In(7, 10)) return 3; // many
                                return 4; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "gd",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1 || d.N() == 11) return 0; // one
                                if (d.N() == 2 || d.N() == 12) return 1; // two
                                if (d.N().In(3, 10) || d.N().In(13, 19)) return 2; // few
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "gl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "gsw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "gu",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2 || d.N() == 3) return 1; // two
                                if (d.N() == 4) return 2; // few
                                if (d.N() == 6) return 3; // many
                                return 4; // other
                        }
                        return -1;
                    }
                },
                {
                    "guw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "gv",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1) return 0; // one
                                if (d.V() == 0 && d.I() % 10 == 2) return 1; // two
                                if (d.V() == 0 && (d.I() % 100 == 0 || d.I() % 100 == 20 || d.I() % 100 == 40 || d.I() % 100 == 60 || d.I() % 100 == 80)) return 2; // few
                                if (d.V() != 0) return 3; // many
                                return 4; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ha",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "haw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "he",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.I() == 2 && d.V() == 0) return 1; // two
                                if (d.V() == 0 && !d.N().In(0, 10) && d.N() % 10 == 0) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "hi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2 || d.N() == 3) return 1; // two
                                if (d.N() == 4) return 2; // few
                                if (d.N() == 6) return 3; // many
                                return 4; // other
                        }
                        return -1;
                    }
                },
                {
                    "hr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11 || d.F() % 10 == 1 && d.F() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14) || (d.F() % 10).In(2, 4) && !(d.F() % 100).In(12, 14)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "hsb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 100 == 1 || d.F() % 100 == 1) return 0; // one
                                if (d.V() == 0 && d.I() % 100 == 2 || d.F() % 100 == 2) return 1; // two
                                if (d.V() == 0 && (d.I() % 100).In(3, 4) || (d.F() % 100).In(3, 4)) return 2; // few
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "hu",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1 || d.N() == 5) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "hy",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.I() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "id",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ig",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "li",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "in",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "is",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.T() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11 || d.T() != 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "it",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 11 || d.N() == 8 || d.N() == 80 || d.N() == 800) return 0; // many
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "iu",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "iw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.I() == 2 && d.V() == 0) return 1; // two
                                if (d.V() == 0 && !d.N().In(0, 10) && d.N() % 10 == 0) return 3; // many
                                return 4; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ja",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "jbo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "jgo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ji",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "jmc",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "jv",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "jw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ka",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.I() == 1) return 0; // one
                                if (d.I() == 0 || ((d.I() % 100).In(2, 20) || (d.I() % 100) == 40 || (d.I() % 100) == 60 || (d.I() % 100) == 80)) return 1; // many
                                return 2; // other
                        }
                        return -1;
                    }
                },
                {
                    "kab",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.I() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kaj",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kcg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kde",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kea",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() % 10 == 6 || d.N() % 10 == 9 || d.N() % 10 == 0 && d.N() != 0) return 0; // many
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "kkj",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "km",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "kn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ko",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ks",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ksb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ksh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 0) return 0; // zero
                                if (d.N() == 1) return 1; // one
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ku",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "kw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/q
                        }
                        return -1;
                    }
                },
                {
                    "ky",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "lag",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 0) return 0; // zero
                                if ((d.I() == 0 || d.I() == 1) && d.N() != 0) return 1; // one
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "lb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "lg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "lkt",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ln",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "lo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "lt",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() % 10 == 1 && !(d.N() % 100).In(11, 19)) return 0; // one
                                if ((d.N() % 10).In(2, 9) && !(d.N() % 100).In(11, 19)) return 1; // few
                                if (d.F() != 0) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "lv",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() % 10 == 0 || (d.N() % 100).In(11, 19) || d.V() == 2 && (d.F() % 100).In(11, 19)) return 0; // zero
                                if (d.N() % 10 == 1 && d.N() % 100 != 11 || d.V() == 2 && d.F() % 10 == 1 && d.F() % 100 != 11 || d.V() != 2 && d.F() % 10 == 1) return 1; // one
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "mas",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "mg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "mgo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "mk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 || d.F() % 10 == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.I() % 10 == 1 && d.I() % 100 != 11) return 0; // one
                                if (d.I() % 10 == 2 && d.I() % 100 != 12) return 1; // two
                                if ((d.I() % 10 == 7 || d.I() % 10 == 8) && !(d.I() % 100 == 17 || d.I() % 100 == 18)) return 2; // many
                                return 3; // other
                        }
                        return -1;
                    }
                },
                {
                    "ml",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "mn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "mo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.V() != 0 || d.N() == 0 || d.N() != 1 && (d.N() % 100).In(1, 19)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "mr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2 || d.N() == 3)  return 1; // two
                                if (d.N() == 4) return 2; // few
                                return 3; // other
                        }
                        return -1;
                    }
                },
                {
                    "ms",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "mt",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 0 || (d.N() % 100).In(2, 10)) return 1; // few
                                if ((d.N() % 100).In(11, 19)) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "my",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "nah",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "naq",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nb",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "nd",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ne",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N().In(1, 4)) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "nl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "nn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nnh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "no",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nqo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nso",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ny",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "nyn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "om",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "or",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "os",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "pa",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "pap",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "pl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14)) return 1; // few
                                if (d.V() == 0 && d.I() != 1 && (d.I() % 10).In(0, 1) || d.V() == 0 && (d.I() % 10).In(5, 9) || d.V() == 0 && (d.I() % 100).In(12, 14)) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "prg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() % 10 == 0 || (d.N() % 100).In(11, 19) || d.V() == 2 && (d.F() % 100).In(11, 19)) return 0; // zero
                                if (d.N() % 10 == 1 && d.N() % 100 != 11 || d.V() == 2 && d.F() % 10 == 1 && d.F() % 100 != 11 || d.V() != 2 && d.F() % 10 == 1) return 1; // one
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ps",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "pt",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "rm",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ro",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.V() != 0 || d.N() == 0 || d.N() != 1 && (d.N() % 100).In(1, 19)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "rof",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "root",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ru",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14)) return 1; // few
                                if (d.V() == 0 && d.I() % 10 == 0 || d.V() == 0 && (d.I() % 10).In(5, 9) || d.V() == 0 && (d.I() % 100).In(11, 14)) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "rwk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sah",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "saq",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sdh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "se",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "seh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ses",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sg",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11 || d.F() % 10 == 1 && d.F() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14) || (d.F() % 10).In(2, 4) && !(d.F() % 100).In(12, 14)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "shi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                if (d.N().In(2, 10)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "si",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if ((d.N() == 0 || d.N() == 1) || d.I() == 0 && d.F() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "sk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                if (d.I().In(2, 4) && d.V() == 0) return 1; // few
                                if (d.V() != 0) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "sl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 100 == 1) return 0; // one
                                if (d.V() == 0 && d.I() % 100 == 2) return 1; // two
                                if (d.V() == 0 && (d.I() % 100).In(3, 4) || d.V() != 0) return 2; // few
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "sma",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "smi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "smj",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "smn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sms",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() == 2) return 1; // two
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "so",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sq",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                if (d.N() % 10 == 4 && d.N() % 100 != 14) return 1; // many
                                return 2; // other
                        }
                        return -1;
                    }
                },
                {
                    "sr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11 || d.F() % 10 == 1 && d.F() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14) || (d.F() % 10).In(2, 4) && !(d.F() % 100).In(12, 14)) return 1; // few
                                return 2; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ss",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ssy",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "st",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "sv",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if ((d.N() % 10 == 1 || d.N() % 10 == 2) && !(d.N() % 100 == 11 || d.N() % 100 == 12)) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "sw",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "syr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ta",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "te",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "teo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "th",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ti",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "tig",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "tk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "tl",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && (d.I() == 1 || d.I() == 2 || d.I() == 3) || d.V() == 0 && !(d.I() % 10 == 4 || d.I() % 10 == 6 || d.I() % 10 == 9) || d.V() != 0 && !(d.F() % 10 == 4 || d.F() % 10 == 6 || d.F() % 10 == 9)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "tn",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "to",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "tr",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ts",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "tzm",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1) || d.N().In(11, 99)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "ug",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "uk",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.V() == 0 && d.I() % 10 == 1 && d.I() % 100 != 11) return 0; // one
                                if (d.V() == 0 && (d.I() % 10).In(2, 4) && !(d.I() % 100).In(12, 14)) return 1; // few
                                if (d.V() == 0 && d.I() % 10 == 0 || d.V() == 0 && (d.I() % 10).In(5, 9) || d.V() == 0 && (d.I() % 100).In(11, 14)) return 2; // many
                                return 3; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() % 10 == 3 && d.N() % 100 != 13) return 0; // few
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "ur",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "uz",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "ve",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "vi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                        }
                        return -1;
                    }
                },
                {
                    "vo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "vun",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "wa",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N().In(0, 1)) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "wae",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "wo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "xh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "xog",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "yi",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 1 && d.V() == 0) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "yo",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // n/a
                        }
                        return -1;
                    }
                },
                {
                    "yue",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "zh",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                return 0; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
                {
                    "zu",
                    (t, d) =>
                    {
                        switch (t)
                        {
                            case PluralRule.Type.Cardinal:
                                if (d.I() == 0 || d.N() == 1) return 0; // one
                                return 1; // other
                            case PluralRule.Type.Ordinal:
                                return 0; // other
                        }
                        return -1;
                    }
                },
            };
        }
    }
}