﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ImprobableStudios.Localization
{
    
    public class LocalizationSerializer
    {
        Dictionary<Type, LocalizationConverter> m_localizationConverters = new Dictionary<Type, LocalizationConverter>();
        Dictionary<Type, Type> m_defaultLocalizationConverterTypes = new Dictionary<Type, Type>();

        public LocalizationSerializer()
        {
            AddDefaultConverters();
        }

        public LocalizationSerializer(LocalizationConverter[] localizationConverters)
        {
            AddDefaultConverters();
            foreach (LocalizationConverter localizationConverter in localizationConverters)
            {
                AddConverter(localizationConverter);
            }
        }

        protected virtual void AddDefaultConverters()
        {
            AddConverter(new StringLocalizationConverter());
            AssignDefaultConverter(typeof(string), typeof(StringLocalizationConverter));

            AddConverter(new CharLocalizationConverter());
            AssignDefaultConverter(typeof(char), typeof(CharLocalizationConverter));

            AddConverter(new ByteLocalizationConverter());
            AssignDefaultConverter(typeof(byte), typeof(ByteLocalizationConverter));

            AddConverter(new ShortLocalizationConverter());
            AssignDefaultConverter(typeof(short), typeof(ShortLocalizationConverter));

            AddConverter(new IntLocalizationConverter());
            AssignDefaultConverter(typeof(int), typeof(IntLocalizationConverter));

            AddConverter(new LongLocalizationConverter());
            AssignDefaultConverter(typeof(long), typeof(LongLocalizationConverter));

            AddConverter(new FloatLocalizationConverter());
            AssignDefaultConverter(typeof(float), typeof(FloatLocalizationConverter));

            AddConverter(new DoubleLocalizationConverter());
            AssignDefaultConverter(typeof(Double), typeof(DoubleLocalizationConverter));

            AddConverter(new DecimalLocalizationConverter());
            AssignDefaultConverter(typeof(Decimal), typeof(DecimalLocalizationConverter));

            AddConverter(new PathLocalizationConverter());
        }

        public Type GetDefaultConverter(Type objType)
        {
            List<KeyValuePair<Type, int>> matchingTypes = new List<KeyValuePair<Type, int>>();
            foreach (KeyValuePair<Type, Type> kvp in m_defaultLocalizationConverterTypes)
            {
                if (kvp.Key.IsAssignableFrom(objType))
                {
                    matchingTypes.Add(new KeyValuePair<Type, int>(kvp.Value, GetInheritanceDistance(objType, kvp.Key)));
                }
            }

            if (matchingTypes.Count > 0)
            {
                return matchingTypes.OrderBy(x => x.Value).First().Key;
            }

            return null;
        }

        private int GetInheritanceDistance(Type childType, Type baseType)
        {
            return GetInheritanceDistanceInternal(childType, baseType, 0);
        }

        private int GetInheritanceDistanceInternal(Type childType, Type baseType, int level)
        {
            if (childType == null || baseType == null || childType == baseType)
            {
                return level;
            }
            
            return GetInheritanceDistanceInternal(childType.BaseType, baseType, level + 1);
        }

        public bool IsDefaultConverterType(Type converterType)
        {
            if (m_defaultLocalizationConverterTypes.Values.Contains(converterType))
            {
                return true;
            }

            return false;
        }

        public bool TypeCanBeLocalized(Type objType)
        {
            foreach (LocalizationConverter localizationConverter in m_localizationConverters.Values)
            {
                if (localizationConverter.GetLocalizedObjectType().IsAssignableFrom(objType))
                {
                    return true;
                }
            }
            
            return false;
        }

        public void AddConverter(LocalizationConverter converter)
        {
            m_localizationConverters[converter.GetType()] = converter;
        }

        public void RemoveConverter(Type converterType)
        {
            if (converterType != null && m_localizationConverters.ContainsKey(converterType))
            {
                m_localizationConverters.Remove(converterType);
            }
            else
            {
                LocalizationSerializerError e = new LocalizationSerializerError(GetType(), "LocalizationSerializer doesn't contain converter: " + converterType.Name);
                Localizer.OnError.Invoke(e);
                throw e;
            }
        }

        public void AssignDefaultConverter(Type objType, Type converterType)
        {
            if (m_localizationConverters.ContainsKey(converterType))
            {
                m_defaultLocalizationConverterTypes[objType] = converterType;
            }
        }

        public LocalizationConverter GetConverter(Type converterType)
        {
            if (converterType != null && m_localizationConverters.ContainsKey(converterType))
            {
                return m_localizationConverters[converterType];
            }

            return null;
        }

        public int GetConverterOrderedPriority(Type converterType)
        {
            if (converterType != null && m_localizationConverters.ContainsKey(converterType))
            {
                return m_localizationConverters.Keys.ToList().IndexOf(converterType);
            }

            return -1;
        }

        public string Serialize(object obj, Type converterType)
        {
            if (converterType != null && !m_localizationConverters.ContainsKey(converterType))
            {
                CreateConverter(converterType);
            }

            return m_localizationConverters[converterType].SerializeToString(obj);
        }

        public object Deserialize(string str, Type converterType)
        {
            if (!m_localizationConverters.ContainsKey(converterType))
            {
                CreateConverter(converterType);
            }

            return m_localizationConverters[converterType].DeserializeFromString(str);
        }

        public bool StringIsValid(string str, Type converterType)
        {
            if (converterType != null)
            {
                if (!m_localizationConverters.ContainsKey(converterType))
                {
                    CreateConverter(converterType);
                }
                return m_localizationConverters[converterType].StringIsValid(str);
            }

            return true;
        }

        public void AutoPopulateTranslations(Type converterType, string sourceLanguage, Dictionary<string, string> languageDictionary)
        {
            if (!m_localizationConverters.ContainsKey(converterType))
            {
                CreateConverter(converterType);
            }

            foreach (string targetLanguage in languageDictionary.Keys.ToList())
            {
                if (targetLanguage != sourceLanguage)
                {
                    if (languageDictionary.ContainsKey(sourceLanguage))
                    {
                        string sourceString = languageDictionary[sourceLanguage];
                        string translation = m_localizationConverters[converterType].GetAutoTranslatedString(sourceString, sourceLanguage, targetLanguage);
                        if (translation != null)
                        {
                            languageDictionary[targetLanguage] = translation;
                        }
                    }
                }
            }
        }

        protected LocalizationConverter CreateConverter(Type converterType)
        {
            LocalizationConverter converter = Activator.CreateInstance(converterType) as LocalizationConverter;
            m_localizationConverters.Add(converterType, converter);
            return converter;
        }
    }

    public abstract class LocalizationConverter
    {
        public abstract string SerializeToString(object obj);

        public abstract object DeserializeFromString(string str);

        public virtual string GetAutoTranslatedString(string sourceString, string sourceLanguage, string targetLanguage)
        {
            return null;
        }

        public abstract bool StringIsValid(string str);

        public abstract Type GetLocalizedObjectType();
    }

    public abstract class LocalizationConverter<T> : LocalizationConverter
    {
        public sealed override string SerializeToString(object obj)
        {
            return ConvertObjectValueToString((T)obj);
        }

        public sealed override object DeserializeFromString(string str)
        {
            return ConvertStringToObjectValue(str);
        }
        
        public sealed override Type GetLocalizedObjectType()
        {
            return typeof(T);
        }

        protected abstract string ConvertObjectValueToString(T obj);

        protected abstract T ConvertStringToObjectValue(string str);
    }

    public class StringLocalizationConverter : LocalizationConverter<string>
    {
        protected override string ConvertObjectValueToString(string obj)
        {
            return obj;
        }

        protected override string ConvertStringToObjectValue(string str)
        {
            return str;
        }

        public override bool StringIsValid(string str)
        {
            return true;
        }
    }

    public class CharLocalizationConverter : LocalizationConverter<char>
    {
        protected override string ConvertObjectValueToString(char obj)
        {
            return obj.ToString();
        }

        protected override char ConvertStringToObjectValue(string str)
        {
            return char.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            char obj;
            return char.TryParse(str, out obj);
        }
    }

    public class ByteLocalizationConverter : LocalizationConverter<byte>
    {
        protected override string ConvertObjectValueToString(byte obj)
        {
            return obj.ToString();
        }

        protected override byte ConvertStringToObjectValue(string str)
        {
            return byte.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            byte obj;
            return byte.TryParse(str, out obj);
        }
    }

    public class ShortLocalizationConverter : LocalizationConverter<short>
    {
        protected override string ConvertObjectValueToString(short obj)
        {
            return obj.ToString();
        }

        protected override short ConvertStringToObjectValue(string str)
        {
            return short.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            short obj;
            return short.TryParse(str, out obj);
        }
    }

    public class IntLocalizationConverter : LocalizationConverter<int>
    {
        protected override string ConvertObjectValueToString(int obj)
        {
            return obj.ToString();
        }

        protected override int ConvertStringToObjectValue(string str)
        {
            return int.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            int obj;
            return int.TryParse(str, out obj);
        }
    }

    public class LongLocalizationConverter : LocalizationConverter<long>
    {
        protected override string ConvertObjectValueToString(long obj)
        {
            return obj.ToString();
        }

        protected override long ConvertStringToObjectValue(string str)
        {
            return long.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            long obj;
            return long.TryParse(str, out obj);
        }
    }

    public class FloatLocalizationConverter : LocalizationConverter<float>
    {
        protected override string ConvertObjectValueToString(float obj)
        {
            return obj.ToString();
        }

        protected override float ConvertStringToObjectValue(string str)
        {
            return float.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            float obj;
            return float.TryParse(str, out obj);
        }
    }

    public class DoubleLocalizationConverter : LocalizationConverter<double>
    {
        protected override string ConvertObjectValueToString(double obj)
        {
            return obj.ToString();
        }

        protected override double ConvertStringToObjectValue(string str)
        {
            return double.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            double obj;
            return double.TryParse(str, out obj);
        }
    }

    public class DecimalLocalizationConverter : LocalizationConverter<decimal>
    {
        protected override string ConvertObjectValueToString(decimal obj)
        {
            return obj.ToString();
        }

        protected override decimal ConvertStringToObjectValue(string str)
        {
            return decimal.Parse(str);
        }

        public override bool StringIsValid(string str)
        {
            decimal obj;
            return decimal.TryParse(str, out obj);
        }
    }

    public class PathLocalizationConverter : LocalizationConverter<string>
    {
        public override string GetAutoTranslatedString(string sourceString, string sourceLanguage, string targetLanguage)
        {
            if (sourceString != null)
            {
                sourceString = sourceString.Replace(sourceLanguage, targetLanguage);
                sourceString = sourceString.Replace(sourceLanguage.ToLower(), targetLanguage.ToLower());
                sourceString = sourceString.Replace(sourceLanguage.ToUpper(), targetLanguage.ToUpper());
                return sourceString;
            }
            return null;
        }

        protected override string ConvertObjectValueToString(string obj)
        {
            return obj;
        }

        protected override string ConvertStringToObjectValue(string str)
        {
            return str;
        }

        public override bool StringIsValid(string str)
        {
            return true;
        }
    }

}