﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ImprobableStudios.Localization
{
    
    // ILocalizables allow the user to localize any field or property marked with a LocalizeAttribute
    public interface ILocalizable
    {
        string GetKey();
        string GetLocalizableOrderedPriority();
        void OnLocalizeComplete();
    }

    // IExplicitLocalizables allow user to localize anything explicitly (without the use of LocalizeAttributes)
    public interface IExplicitLocalizable : ILocalizable
    {
        void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary);
        void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary);
        void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary);
        void ExplicitlyGetConverters(Dictionary<string, Type> dictionary);
    }

    // Fields or properties of type IExplicitLocalizedValue are always localized even if they are not marked with a LocalizeAttribute.
    // The only time they are not localized are if they are marked with a DontLocalizeAttribute
    public interface IExplicitLocalizableValue
    {
        bool ShouldBeLocalized(MemberInfo memberInfo);
        void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary);
        void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary);
        void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary);
        void ExplicitlyGetConverters(Dictionary<string, Type> dictionary);
    }

}