﻿using System;

namespace ImprobableStudios.Localization
{

    public class LocalizationException : Exception
    {
        public LocalizationException() : base()
        {
        }

        public LocalizationException(string message) : base(message)
        {
        }

        public LocalizationException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationError : LocalizationException
    {
        public LocalizationError() : base()
        {
        }

        public LocalizationError(string message) : base(message)
        {
        }

        public LocalizationError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationWarning : LocalizationException
    {
        public LocalizationWarning() : base()
        {
        }

        public LocalizationWarning(string message) : base(message)
        {
        }

        public LocalizationWarning(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationLanguageError : LocalizationError
    {
        public LocalizationLanguageError() : base()
        {
        }

        public LocalizationLanguageError(string message) : base(message)
        {
        }

        public LocalizationLanguageError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationParseError : LocalizationError
    {
        public LocalizationParseError() : base()
        {
        }

        public LocalizationParseError(string message) : base(message)
        {
        }

        public LocalizationParseError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationEntryError : LocalizationError
    {
        public LocalizationEntryError() : base()
        {
        }

        public LocalizationEntryError(string message) : base(message)
        {
        }

        public LocalizationEntryError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationEntryWarning : LocalizationWarning
    {
        public LocalizationEntryWarning() : base()
        {
        }

        public LocalizationEntryWarning(string message) : base(message)
        {
        }

        public LocalizationEntryWarning(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationEntryDictionaryError : LocalizationError
    {
        public LocalizationEntryDictionaryError() : base()
        {
        }

        public LocalizationEntryDictionaryError(string message) : base(message)
        {
        }

        public LocalizationEntryDictionaryError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizationSerializerError : LocalizationError
    {
        public LocalizationSerializerError(Type localizationSerializerType) : base("(" + localizationSerializerType.Name + ")")
        {
        }

        public LocalizationSerializerError(Type localizationSerializerType, string message) : base("(" + localizationSerializerType.Name + ")" + " " + message)
        {
        }

        public LocalizationSerializerError(Type localizationSerializerType, string message, Exception inner) : base("(" + localizationSerializerType.Name + ")" + " "  + message, inner)
        {
        }
    }

    public class LocalizedDictionaryError : LocalizationError
    {
        public LocalizedDictionaryError() : base()
        {
        }

        public LocalizedDictionaryError(string message) : base(message)
        {
        }

        public LocalizedDictionaryError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class LocalizableError : LocalizationError
    {
        public LocalizableError(ILocalizable localizable) : base(localizable.ToString())
        {
        }

        public LocalizableError(ILocalizable localizable, string message) : base("(" + localizable.ToString() + ")" + " "+ message)
        {
        }

        public LocalizableError(ILocalizable localizable, string message, Exception inner) : base("(" + localizable.ToString() + ")" + " " + message, inner)
        {
        }
    }
}