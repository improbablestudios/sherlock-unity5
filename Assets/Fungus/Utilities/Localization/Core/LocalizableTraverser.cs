﻿using System;
using System.Reflection;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.Localization
{

    public class LocalizableTraverser
    {
        public virtual MemberInfo[] GetLocalizableMembers(Type objType)
        {
            return objType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public virtual bool ShouldTraverseMembers(Type objType)
        {
            return objType.IsReflectableType() && !typeof(IExplicitLocalizableValue).IsAssignableFrom(objType);
        }
    }

}