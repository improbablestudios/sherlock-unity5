﻿using System;

namespace ImprobableStudios.Localization
{

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class LocalizeAttribute : Attribute
    {
        public LocalizeAttribute()
        {
        }

        public LocalizeAttribute(Type converterType)
        {
            ConverterType = converterType;
        }

        public LocalizeAttribute(string description)
        {
            Description = description;
        }
        
        public LocalizeAttribute(string key, Type converterType, string description)
        {
            Key = key;
            ConverterType = converterType;
            Description = description;
        }

        public string Key
        {
            get;
            set;
        }

        public Type ConverterType
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class DontLocalizeAttribute : Attribute
    {
    }

}