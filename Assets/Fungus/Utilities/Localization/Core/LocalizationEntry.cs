﻿using ImprobableStudios.Localization.CSV;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ImprobableStudios.Localization
{

    [Serializable]
    public class LocalizationEntry
    {
        public const char KEY_DELIMITER = '|';

        public const char COLUMN_DELIMITER = ',';

        public string m_Converter = "";

        public string m_Description = "";
        
        public Dictionary<string, string> m_LocalizedDictionary = new Dictionary<string, string>();

        public LocalizationEntry(LocalizationEntry localizationEntry)
        {
            m_Converter = localizationEntry.m_Converter;
            m_Description = localizationEntry.m_Description;
            m_LocalizedDictionary = localizationEntry.m_LocalizedDictionary.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public LocalizationEntry(string[] languages)
        {
            foreach (string language in languages)
            {
                if (!string.IsNullOrEmpty(language))
                {
                    m_LocalizedDictionary[language] = null;
                }
            }
        }

        public LocalizationEntry(string converterTypeName, string description, string[] languages)
        {
            m_Converter = converterTypeName;
            m_Description = description;

            foreach (string language in languages)
            {
                if (!string.IsNullOrEmpty(language))
                {
                    m_LocalizedDictionary[language] = null;
                }
            }
        }

        public LocalizationEntry(string[] headerColumns, string[] entryColumns, string[] languages)
        {
            m_Converter = CsvParser.Unescape(entryColumns[1]);
            m_Description = CsvParser.Unescape(entryColumns[2]);

            foreach (string language in languages)
            {
                if (!string.IsNullOrEmpty(language))
                {
                    m_LocalizedDictionary[language] = null;
                }
            }

            for (int i = 3; i < entryColumns.Length; i++)
            {
                string language = GetLanguageCodeFromLanguageDisplayName(headerColumns[i]);
                string languageEntry = CsvParser.Unescape(entryColumns[i]);
                
                m_LocalizedDictionary[language] = languageEntry;
            }
        }

        public static string GetKey(string[] entryColumns)
        {
            return CsvParser.Unescape(entryColumns[0]);
        }
        
        public string GetLocalizedString(string language)
        {
            if (m_LocalizedDictionary.ContainsKey(language))
            {
                return m_LocalizedDictionary[language];
            }
            return null;
        }

        public void SetLocalizedString(string language)
        {
            m_LocalizedDictionary[language] = language;
        }

        public static Type GetConverterType(string converterName)
        {
            Type converterType = Type.GetType(converterName);

            if (converterType != null)
            {
                return converterType;
            }

            converterType = Type.GetType(GetBuiltInConverterNameFormat(converterName));

            return converterType;
        }

        public static string GetConverterName(Type converterType)
        {
            string converterName = converterType.Name;

            if (Type.GetType(converterName) != null)
            {
                return converterName;
            }

            converterName = GetBuiltInConverterNameFormat(converterType.Name);

            if (Type.GetType(converterName) != null)
            {
                return converterType.Name;
            }

            converterName = converterType.FullName;

            if (Type.GetType(converterName) != null)
            {
                return converterName;
            }

            converterName = GetConverterFullNameWithAssembly(converterType);

            if (Type.GetType(converterName) != null)
            {
                return converterName;
            }

            return converterType.AssemblyQualifiedName;
        }

        public static string GetConverterFullNameWithAssembly(Type type)
        {
            string typeFullName = type.Name;
            if (!string.IsNullOrEmpty(type.Namespace))
            {
                typeFullName = type.Namespace + "." + typeFullName;
            }
            string assemblyName = type.Assembly.GetName().Name;
            return typeFullName + "," + assemblyName;
        }

        public static string GetBuiltInConverterNameFormat(string typename)
        {
            Type stringConverterType = typeof(StringLocalizationConverter);
            string typeFullName = typename;
            if (!string.IsNullOrEmpty(stringConverterType.Namespace))
            {
                typeFullName = stringConverterType.Namespace + "." + typeFullName;
            }
            string assemblyName = stringConverterType.Assembly.GetName().Name;
            return typeFullName + "," + assemblyName;
        }

        public Type GetConverterType()
        {
            return GetConverterType(m_Converter);
        }

        public static string GetCombinedKeys(string[] keys)
        {
            return string.Join(KEY_DELIMITER.ToString(), keys);
        }

        public static string[] GetSplitKeys(string key)
        {
            return key.Split(KEY_DELIMITER);
        }

        /// <summary>
        /// Takes several localization entries and outputs a combined description for all of them.
        /// e.g. [ClassA], [ClassA], [ClassB], [ClassC] => [ClassA(x2)|ClassB|ClassC]
        /// </summary>
        /// <param name="localizationEntries">The localization entries whose descriptions will be combined.</param>
        /// <returns>a combined description string</returns>
        public static string GetCombinedDescriptions(LocalizationEntry[] localizationEntries)
        {
            LocalizationEntry firstLocalizationEntry = localizationEntries.First();

            string descriptionWithoutPrefix = firstLocalizationEntry.GetDescriptionWithoutPrefix();

            string[] combinedDescriptionPrefixesArray = localizationEntries
                .GroupBy(x => x.GetDescriptionSource())
                .Select(x => GetDescriptionSourceWithCount(x.Key, x.Count()))
                .ToArray();

            string combinedDescriptionPrefixes = string.Join(KEY_DELIMITER.ToString(), combinedDescriptionPrefixesArray);

            return GetPrefixedDescription(combinedDescriptionPrefixes, descriptionWithoutPrefix);
        }

        public string[] GetSplitDescriptions(int expectedDescriptionCount)
        {
            return GetSplitDescriptions(m_Description, expectedDescriptionCount);
        }

        public static string[] GetSplitDescriptions(string description, int expectedDescriptionCount)
        {
            if (description != null)
            {
                List<string> descriptions = new List<string>();

                string descriptionWithoutPrefix = GetDescriptionWithoutPrefix(description);
                string unsplitSource = GetDescriptionSource(description);

                if (unsplitSource != null)
                {
                    string[] splitSourcesWithCounts = unsplitSource.Split(KEY_DELIMITER);
                    foreach (string sourceWithCount in splitSourcesWithCounts)
                    {
                        string sourceWithoutCount = GetSourceWithoutCount(sourceWithCount);
                        int count = GetSourceCount(sourceWithCount);

                        descriptions.AddRange(Enumerable.Repeat(GetPrefixedDescription(sourceWithoutCount, descriptionWithoutPrefix), count));
                    }

                    if (descriptions.Count < expectedDescriptionCount)
                    {
                        descriptions.AddRange(Enumerable.Repeat(descriptionWithoutPrefix, expectedDescriptionCount - descriptions.Count));
                    }

                    return descriptions.ToArray();
                }
                else
                {
                    descriptions.AddRange(Enumerable.Repeat(descriptionWithoutPrefix, expectedDescriptionCount));
                    return descriptions.ToArray();
                }
            }
            return null;
        }

        private static string GetDescriptionSourceWithCount(string prefix, int count)
        {
            return count > 1 ? prefix + "(x" + count + ")" : prefix;
        }

        private static string DescriptionPrefixRegex()
        {
            return @"^\[(.*?)\]";
        }

        private static string DescriptionCountRegex()
        {
            return @"\(x([0-9]+)\)$";
        }

        public string GetPrefixedDescription(string source)
        {
            return GetPrefixedDescription(source, m_Description);
        }

        public static string GetPrefixedDescription(string source, string description)
        {
            if (!string.IsNullOrEmpty(source))
            {
                if (!string.IsNullOrEmpty(description))
                {
                    return "[" + source + "] " + description;
                }
                else
                {
                    return "[" + source + "]";
                }
            }
            return description;
        }

        public int GetSourceCount()
        {
            return GetSourceCount(GetDescriptionSource());
        }

        public static int GetSourceCount(string sourceWithCount)
        {
            int count = 1;
            Match countMatch = Regex.Match(sourceWithCount, DescriptionCountRegex());
            if (countMatch != null)
            {
                Group countGroup = countMatch.Groups[1];
                if (countGroup != null)
                {
                    string countString = countGroup.Value;
                    if (!string.IsNullOrEmpty(countString))
                    {
                        int.TryParse(countString, out count);
                    }
                }
            }

            return count;
        }

        public string GetSourceWithoutCount()
        {
            return GetSourceWithoutCount(GetDescriptionSource());
        }

        public static string GetSourceWithoutCount(string sourceWithCount)
        {
            return Regex.Replace(sourceWithCount, DescriptionCountRegex(), "").Trim(); ;
        }

        public string GetDescriptionSource()
        {
            return GetDescriptionSource(m_Description);
        }

        public static string GetDescriptionSource(string description)
        {
            if (description != null)
            {
                Match prefixMatch = Regex.Match(description, DescriptionPrefixRegex());
                if (prefixMatch != null)
                {
                    Group sourceGroup = prefixMatch.Groups[1];
                    if (sourceGroup != null)
                    {
                        return sourceGroup.Value.Trim();
                    }
                }
            }
            return null;
        }

        public string GetDescriptionWithoutPrefix()
        {
            return GetDescriptionWithoutPrefix(m_Description);
        }

        public static string GetDescriptionWithoutPrefix(string description)
        {
            if (description != null)
            {
                return Regex.Replace(description, DescriptionPrefixRegex(), "").TrimStart();
            }
            return description;
        }
        
        public string GetGroupingKey(string sourceLanguage)
        {
            return ((m_Converter != null) ? CsvParser.Escape(m_Converter.ToString()) : "") + "|" +
                   ((GetDescriptionWithoutPrefix() != null) ? CsvParser.Escape(GetDescriptionWithoutPrefix()) : "") + "|" +
                   ((GetLocalizedString(sourceLanguage) != null) ? CsvParser.Escape(GetLocalizedString(sourceLanguage)) : "");
        }

        public static string GetLanguageDisplayNameFromLanguageCode(string languageCode)
        {
            try
            {
                CultureInfo ci = CultureInfo.GetCultureInfo(languageCode);
                return string.Format("[{0}]\t{1}", ci.Name, ci.NativeName);
            }
            catch (ArgumentException)
            {
                return languageCode;
            }
        }

        public static string GetLanguageCodeFromLanguageDisplayName(string languageDisplayName)
        {
            string regexPattern = @"\[(.*?)\]";
            Match match = Regex.Match(languageDisplayName, regexPattern);
            if (match.Success)
            {
                return match.Groups[1].Value;
            }

            return languageDisplayName;
        }

        public static string[][] GetCsvTable(Dictionary<string, LocalizationEntry> localizationEntryDictionary, string sourceLanguage, string[] translationLanguages)
        {
            List<string[]> rows = new List<string[]>();

            rows.Add(GetHeaderColumns(sourceLanguage, translationLanguages));

            foreach (string key in localizationEntryDictionary.Keys)
            {
                LocalizationEntry localizationEntry = localizationEntryDictionary[key];

                if (localizationEntry != null)
                {
                    rows.Add(localizationEntry.GetColumns(key, sourceLanguage, translationLanguages));
                }
            }

            return rows.ToArray();
        }

        public static string[] GetHeaderNonLanguageColumns()
        {
            return new string[] { "KEYS", "CONVERTER", "DESCRIPTION" };
        }

        public static string[] GetHeaderLanguageColumns(string sourceLanguage, string[] translationLanguages)
        {
            string[] languageColumns = new string[1 + translationLanguages.Length];
            languageColumns[0] = sourceLanguage;
            for (int i = 0; i < translationLanguages.Length; i++)
            {
                languageColumns[i + 1] = translationLanguages[i];
            }
            return languageColumns.Select(x => GetLanguageDisplayNameFromLanguageCode(x)).ToArray();
        }

        public static string[] GetHeaderColumns(string sourceLanguage, string[] translationLanguages)
        {
            return GetHeaderNonLanguageColumns().Concat(GetHeaderLanguageColumns(sourceLanguage, translationLanguages)).ToArray();
        }

        public string[] GetColumns(string key, string sourceLanguage, string[] translationLanguages)
        {
            string[] nonLanguageColumns = new string[] { CsvParser.Escape(key), CsvParser.Escape(m_Converter), CsvParser.Escape(m_Description) };

            string[] languageColumns = new string[1 + translationLanguages.Length];

            string sourceString = "";
            if (m_LocalizedDictionary.ContainsKey(sourceLanguage))
            {
                sourceString = CsvParser.Escape(m_LocalizedDictionary[sourceLanguage]);
            }
            languageColumns[0] = sourceString;

            for (int i = 0; i < translationLanguages.Length; i++)
            {
                string translationLanguage = translationLanguages[i];
                string translationString = "";
                if (m_LocalizedDictionary.ContainsKey(translationLanguage))
                {
                    translationString = CsvParser.Escape(m_LocalizedDictionary[translationLanguage]);
                }
                languageColumns[i + 1] = translationString;
            }
            return nonLanguageColumns.Concat(languageColumns).ToArray();
        }

        public static bool StringsAreEqual(string language, LocalizationEntry localizationEntry1, LocalizationEntry localizationEntry2)
        {
            return StringsAreEqual(localizationEntry1.GetLocalizedString(language), localizationEntry2.GetLocalizedString(language));
        }

        public static bool StringsAreEqual(string str1, string str2)
        {
            return CsvParser.Escape(str1) == CsvParser.Escape(str2);
        }

        public static bool LocalizationCsvIsFormattedCorrectly(string csv)
        {
            string expectedHeaderFormat = string.Join(COLUMN_DELIMITER.ToString(), GetHeaderNonLanguageColumns());
            return csv.Trim().StartsWith(expectedHeaderFormat, StringComparison.Ordinal);
        }

        public static string GetRowString(string[] columns)
        {
            return string.Join(COLUMN_DELIMITER.ToString(), columns) + "\n";
        }

        public static string GetCsvFromLocalizationEntryDictionary(Dictionary<string, LocalizationEntry> localizationEntryDictionary, string sourceLanguage, string[] translationLanguages)
        {
            string[][] csvTable = GetCsvTable(localizationEntryDictionary, sourceLanguage, translationLanguages);
            var sb = new StringBuilder(string.Empty);
            foreach (string[] row in csvTable)
            {
                sb.Append(GetRowString(row));
            }
            return sb.ToString();
        }

        public static Dictionary<string, LocalizationEntry> GetLocalizationEntryDictionaryFromCsv(string csvData, string sourceLanguage, string[] translationLanguages)
        {
            Dictionary<string, LocalizationEntry> localizationEntryDictionary = new Dictionary<string, LocalizationEntry>();
            if (!LocalizationCsvIsFormattedCorrectly(csvData))
            {
                LocalizationParseError e = new LocalizationParseError("CSV Parse Error: csv is not formatted correctly. Make sure you opened and saved the csv using UTF-8 encoding and separated columns by comma only.");
                Localizer.OnError.Invoke(e);
                throw e;
            }

            CsvParser csvParser = new CsvParser();
            string[][] csvTable = csvParser.Parse(csvData);

            if (csvTable.Length == 0)
            {
                LocalizationParseError e = new LocalizationParseError("CSV Parse Error: No valid csv data found");
                Localizer.OnError.Invoke(e);
                throw e;
            }

            string[] headerColumns = csvTable[0];

            string[] languages = new string[] { sourceLanguage }.Concat(translationLanguages).ToArray();

            for (int i = 1; i < csvTable.Length; ++i)
            {
                string[] entryColumns = csvTable[i];

                string key = GetKey(entryColumns);
                LocalizationEntry csvLocalizationEntry = new LocalizationEntry(headerColumns, entryColumns, languages);
                localizationEntryDictionary[key] = csvLocalizationEntry;
            }

            return localizationEntryDictionary;
        }
        
        public static int CountLocalizationEntriesInCsv(string csvData)
        {
            int count = 0;

            CsvParser csvParser = new CsvParser();
            string[][] csvTable = csvParser.Parse(csvData);

            if (csvTable.Length <= 1)
            {
                // No data rows in file
                return 0;
            }

            for (int i = 1; i < csvTable.Length; ++i)
            {
                string[] entryColumns = csvTable[i];

                string key = entryColumns[0];
                string[] localizationEntryIds = key.Split(KEY_DELIMITER);

                foreach (string localizationEntryId in localizationEntryIds)
                {
                    count++;
                }
            }

            return count;
        }
    }

}