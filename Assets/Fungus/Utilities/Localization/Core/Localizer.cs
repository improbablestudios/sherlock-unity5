﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using ImprobableStudios.Localization.CSV;
using UnityEngine;

namespace ImprobableStudios.Localization
{

    public static class Localizer
    {
        public const string DEFAULT_LANGUAGE = "en-US";

        public const string UNREGISTERED_KEY_PREFIX = "UNREGISTERED";

        public const string OUTDATED_ENTRY_PREFIX = "OUTDATED";

        public const string INVALID_ENTRY_PREFIX = "INVALID";

        public static readonly Regex BALANCED_BRACKET_REGEX = new Regex(@"^
                                \[                      # First '['
                                    (?:                 
                                    [^\[\]]               # Match all non-braces
                                    |
                                    (?<open> \[ )       # Match '[', and capture into 'open'
                                    |
                                    (?<-open> \] )      # Match ']', and delete the 'open' capture
                                    )+
                                    (?(open)(?!))       # Fails if 'open' stack isn't empty!

                                \]                      # Last ']'
                                ", RegexOptions.IgnorePatternWhitespace);

        private static string s_activeLanguage = DEFAULT_LANGUAGE;

        private static string s_sourceLanguage = DEFAULT_LANGUAGE;

        private static string[] s_translationLanguages = new string[] { };

        private static string[] s_languages;

        private static string[] s_dotNetSpecificCultureNames;

        private static LocalizableTraverser s_localizableTraverser = new LocalizableTraverser();

        private static LocalizationSerializer s_localizationSerializer = new LocalizationSerializer();

        private static Dictionary<string, Dictionary<string, LocalizationEntry>> s_localizableLocalizationEntryDictionary = new Dictionary<string, Dictionary<string, LocalizationEntry>>();

        private static Dictionary<string, ILocalizable> s_localizableInstanceDictionary = new Dictionary<string, ILocalizable>();
        
        public delegate void OnErrorDelegate(LocalizationError error);

        private static event OnErrorDelegate s_onError = delegate { };

        public delegate void OnWarningDelegate(LocalizationWarning warning);

        private static event OnWarningDelegate s_onWarning = delegate { };

        public static string ActiveLanguage
        {
            get
            {
                return s_activeLanguage;
            }
            set
            {
                SetLanguage(s_activeLanguage);
            }
        }

        public static string SourceLanguage
        {
            get
            {
                return s_sourceLanguage;
            }
            set
            {
                s_sourceLanguage = value;
                UpdateLanguages();
            }
        }

        public static string[] TranslationLanguages
        {
            get
            {
                return s_translationLanguages;
            }
            set
            {
                if (!value.All(x => !string.IsNullOrEmpty(x)))
                {
                    LocalizationLanguageError e = new LocalizationLanguageError("Language in Localizer.TranslationLanguages cannot be null or empty");
                    OnError.Invoke(e);
                    throw e;
                }
                s_translationLanguages = value;
                UpdateLanguages();
            }
        }

        public static string[] Languages
        {
            get
            {
                if (s_languages == null)
                {
                    UpdateLanguages();
                }
                return s_languages;
            }
        }

        public static string[] DotNetSpecificCultureNames
        {
            get
            {
                if (s_dotNetSpecificCultureNames == null)
                {
                    s_dotNetSpecificCultureNames = CultureInfo.GetCultures(CultureTypes.SpecificCultures).Select(i => i.Name).OrderBy(i => i).ToArray();
                }
                return s_dotNetSpecificCultureNames;
            }
        }

        public static LocalizableTraverser LocalizableTraverser
        {
            get
            {
                return s_localizableTraverser;
            }
            set
            {
                s_localizableTraverser = value;
            }
        }

        public static LocalizationSerializer LocalizationSerializer
        {
            get
            {
                return s_localizationSerializer;
            }
            set
            {
                s_localizationSerializer = value;
            }
        }

        public static Dictionary<string, Dictionary<string, LocalizationEntry>> LocalizableLocalizationEntryDictionary
        {
            get
            {
                if (s_localizableLocalizationEntryDictionary == null)
                {
                    s_localizableLocalizationEntryDictionary = new Dictionary<string, Dictionary<string, LocalizationEntry>>();
                }
                return s_localizableLocalizationEntryDictionary;
            }
        }

        public static Dictionary<string, ILocalizable> LocalizableInstanceDictionary
        {
            get
            {
                return s_localizableInstanceDictionary;
            }
        }

        public static OnErrorDelegate OnError
        {
            get
            {
                return s_onError;
            }
            set
            {
                s_onError = value;
            }
        }

        public static OnWarningDelegate OnWarning
        {
            get
            {
                return s_onWarning;
            }
            set
            {
                s_onWarning = value;
            }
        }

        private static void UpdateLanguages()
        {
            List<string> languages = new List<string>();
            languages.Add(s_sourceLanguage);
            languages.AddRange(s_translationLanguages);
            s_languages = languages.ToArray();
        }

        public static void RegisterLocalizationEntryDictionary(Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            if (localizationEntryDictionary != null)
            {
                foreach (string key in localizationEntryDictionary.Keys)
                {
                    string localizableKey = GetLocalizableKeyFromLocalizationEntryKey(key);
                    s_localizableLocalizationEntryDictionary[localizableKey] = localizationEntryDictionary;
                }
            }
        }

        public static void RegisterLocalizable(ILocalizable localizable, bool localize = true)
        {
            if (localizable != null)
            {
                string key = localizable.GetKey();

                if (!s_localizableInstanceDictionary.ContainsKey(key))
                {
                    if (localize)
                    {
                        Dictionary<string, LocalizationEntry> localizationEntryDictionary = FindLocalizationEntryDictionary(key);
                        if (localizationEntryDictionary != null)
                        {
                            Localize(localizable, localizationEntryDictionary);
                        }
                        localizable.OnLocalizeComplete();
                    }
                    s_localizableInstanceDictionary[key] = localizable;
                }
            }
        }

        public static void RemoveLocalizable(string localizableKey)
        {
            if (localizableKey != null)
            {
                s_localizableInstanceDictionary.Remove(localizableKey);
            }
        }

        public static void Localize(ILocalizable localizable, Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            localizable.SetLocalizedValues(GetActiveLanguageDictionary(localizable, localizationEntryDictionary));
        }

        public static void SetLanguage(string activeLanguage)
        {
            if (Languages == null || Languages.Length == 0 || !Languages.Contains(activeLanguage))
            {
                LocalizationLanguageError e = new LocalizationLanguageError("Language (" + activeLanguage + ") does not exist in Localizer.Languages");
                OnError.Invoke(e);
                throw e;
            }

            s_activeLanguage = activeLanguage;

            if (DotNetSpecificCultureNames.Contains(activeLanguage))
            {
                CultureInfo.CurrentCulture = new CultureInfo(activeLanguage);
                CultureInfo.CurrentUICulture = new CultureInfo(activeLanguage);
            }

            foreach (KeyValuePair<string, ILocalizable> kvp in s_localizableInstanceDictionary.ToList())
            {
                string key = kvp.Key;
                ILocalizable localizable = kvp.Value;
                Dictionary<string, LocalizationEntry> localizationEntryDictionary = FindLocalizationEntryDictionary(key);
                if (localizationEntryDictionary != null)
                {
                    Localize(localizable, localizationEntryDictionary);
                }
                localizable.OnLocalizeComplete();
            }
        }

        public static Dictionary<string, LocalizationEntry> FindLocalizationEntryDictionary(string localizableKey)
        {
            if (!string.IsNullOrEmpty(localizableKey))
            {
                if (s_localizableLocalizationEntryDictionary.ContainsKey(localizableKey))
                {
                    return s_localizableLocalizationEntryDictionary[localizableKey];
                }
            }

            return null;
        }

        public static LocalizationEntry GetLocalizationEntry(ILocalizable localizable, string key)
        {
            Dictionary<string, LocalizationEntry> localizationEntryDictionary = FindLocalizationEntryDictionary(localizable.GetKey());
            string localizationEntryKey = localizable.GetMemberKeyPrefix() + key;
            if (localizationEntryDictionary != null && localizationEntryDictionary.ContainsKey(localizationEntryKey))
            {
                return localizationEntryDictionary[localizationEntryKey];
            }
            else
            {
                throw new LocalizedDictionaryError("Cannot get LocalizationEntry for key: " + key);
            }
        }

        public static Dictionary<string, T> SafeGetLocalizedDictionary<T>(string localizationEntryKey, LocalizationEntry localizationEntry)
        {
            Dictionary<string, T> result;

            GetLocalizedDictionaryWithExceptionInfo(localizationEntryKey, localizationEntry, out result).OfType<LocalizationError>().ToArray();

            return result;
        }

        public static bool TryGetLocalizedDictionary<T>(string localizationEntryKey, LocalizationEntry localizationEntry, out Dictionary<string, T> result)
        {
            return GetLocalizedDictionaryWithExceptionInfo(localizationEntryKey, localizationEntry, out result).Length == 0;
        }

        public static Dictionary<string, T> GetLocalizedDictionary<T>(string localizationEntryKey, LocalizationEntry localizationEntry)
        {
            Dictionary<string, T> result;

            LocalizationError[] errors = GetLocalizedDictionaryWithExceptionInfo(localizationEntryKey, localizationEntry, out result).OfType<LocalizationError>().ToArray();

            if (errors.Length > 0)
            {
                throw new AggregateException("Encountered errors when attempting to get the Localized Dictionary.", errors);
            }

            return result;
        }

        public static LocalizationException[] GetLocalizedDictionaryWithExceptionInfo<T>(string localizationEntryKey, LocalizationEntry localizationEntry, out Dictionary<string, T> result)
        {
            result = new Dictionary<string, T>();

            List<LocalizationException> exceptions = new List<LocalizationException>();
            
            Type converterType = localizationEntry.GetConverterType();
            if (converterType == null)
            {
                LocalizationEntryError e = new LocalizationEntryError(string.Format("Invalid Converter ({0}): '{1}'", localizationEntryKey, localizationEntry.m_Converter));
                OnError.Invoke(e);
                exceptions.Add(e);
            }

            foreach (KeyValuePair<string, string> kvp in localizationEntry.m_LocalizedDictionary)
            {
                string language = kvp.Key;
                string translation = kvp.Value;
                object obj = null;
                if (!string.IsNullOrEmpty(translation))
                {
                    if (HasPrefix(translation, INVALID_ENTRY_PREFIX))
                    {
                        LocalizationEntryError e = new LocalizationEntryError(string.Format("Invalid translation ({0}): '{1}'", localizationEntryKey, language));
                        OnError.Invoke(e);
                        exceptions.Add(e);
                    }
                    else
                    {
                        if (HasPrefix(translation, OUTDATED_ENTRY_PREFIX))
                        {
                            LocalizationEntryWarning e = new LocalizationEntryWarning(string.Format("Outdated translation ({0}): '{1}'.", localizationEntryKey, language));
                            OnWarning.Invoke(e);
                            exceptions.Add(e);
                        }
                        if (converterType != null)
                        {
                            obj = s_localizationSerializer.Deserialize(translation, converterType);
                            if (obj == null)
                            {
                                LocalizationEntryError e = new LocalizationEntryError(string.Format("Failed to deserialize translation ({0}): '{1}'", localizationEntryKey, language));
                                OnError.Invoke(e);
                                exceptions.Add(e);
                            }
                        }
                    }
                }
                else
                {
                    LocalizationEntryWarning e = new LocalizationEntryWarning(string.Format("Untranslated translation ({0}): '{1}'", localizationEntryKey, language));
                    OnWarning.Invoke(e);
                    exceptions.Add(e);
                }
                result[language] = (obj != null) ? (T)obj : default(T);
            }
            
            return exceptions.ToArray();
        }

        public static bool ValidateLocalizationEntry(string localizationEntryKey, LocalizationEntry localizationEntry)
        {
            bool valid = true;

            if (HasPrefix(localizationEntry.m_Converter, INVALID_ENTRY_PREFIX))
            {
                LocalizationEntryError e = new LocalizationEntryError(string.Format("Invalid Converter ({0}): '{1}'", localizationEntryKey, localizationEntry.m_Converter));
                OnError.Invoke(e);
                valid = false;
            }

            foreach (KeyValuePair<string, string> kvp in localizationEntry.m_LocalizedDictionary)
            {
                string language = kvp.Key;
                string translation = kvp.Value;
                if (!string.IsNullOrEmpty(translation))
                {
                    if (HasPrefix(translation, INVALID_ENTRY_PREFIX))
                    {
                        LocalizationEntryError e = new LocalizationEntryError(string.Format("Invalid translation ({0}): '{1}'", localizationEntryKey, language));
                        OnError.Invoke(e);
                        valid = false;
                    }
                    if (HasPrefix(translation, OUTDATED_ENTRY_PREFIX))
                    {
                        LocalizationEntryWarning e = new LocalizationEntryWarning(string.Format("Outdated translation ({0}): '{1}'.", localizationEntryKey, language));
                        OnWarning.Invoke(e);
                        valid = false;
                    }
                }
                else
                {
                    LocalizationEntryWarning e = new LocalizationEntryWarning(string.Format("Untranslated translation ({0}): '{1}'", localizationEntryKey, language));
                    OnWarning.Invoke(e);
                    valid = false;
                }
            }

            return valid;
        }

        public static string GetLocalizableKeyFromLocalizationEntryKey(string localizationEntryKey)
        {
            int entryKeyStartIndex = localizationEntryKey.IndexOf("+", StringComparison.Ordinal);
            if (entryKeyStartIndex >= 0)
            {
                return localizationEntryKey.Remove(entryKeyStartIndex);
            }

            return localizationEntryKey;
        }

        public static Dictionary<string, object> GetActiveLanguageDictionary(ILocalizable localizable, Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            Dictionary<string, object> activeLanguageDictionary = new Dictionary<string, object>();

            string localizableKey = localizable.GetKey();
            
            foreach (KeyValuePair<string, LocalizationEntry> kvp in localizationEntryDictionary)
            {
                string key = kvp.Key;
                LocalizationEntry localizationEntry = kvp.Value;

                if (key.StartsWith(localizableKey, StringComparison.Ordinal))
                {
                    string localizationEntryValue = localizationEntry.GetLocalizedString(s_activeLanguage);

                    Type converterType = localizationEntry.GetConverterType();
                    if (converterType == null)
                    {
                        LocalizationEntryError e = new LocalizationEntryError("LocalizationConverter type is not valid: " + localizationEntry.m_Converter);
                        OnError.Invoke(e);
                        throw e;
                    }

                    object localizedObjectValue = s_localizationSerializer.Deserialize(localizationEntryValue, converterType);
                    if (localizedObjectValue != null)
                    {
                        activeLanguageDictionary[key] = localizedObjectValue;
                    }
                }
            }
            
            return activeLanguageDictionary;
        }
        
        public static Dictionary<string, LocalizationEntry> BuildLocalizationEntryDictionary(ILocalizable[] localizables, string currentCsvData, bool removeEmptyEntries = true, bool combineIdenticalEntries = false)
        {
            Dictionary<string, LocalizationEntry> localizationEntryDictionary = new Dictionary<string, LocalizationEntry>();
            Dictionary<string, string> localizableOrderDictionary = new Dictionary<string, string>();

            foreach (ILocalizable localizable in localizables)
            {
                PopulateLocalizationEntryDictionaryWithApplicationData(localizationEntryDictionary, localizable);
                localizableOrderDictionary[localizable.GetKey()] = localizable.GetLocalizableOrderedPriority();
            }

            if (!string.IsNullOrEmpty(currentCsvData))
            {
                PopulateLocalizationEntryDictionaryWithCsvData(localizationEntryDictionary, currentCsvData, true);
            }

            if (removeEmptyEntries)
            {
                RemoveEmptyEntriesFromLocalizationEntryDictionary(localizationEntryDictionary);
            }

            localizationEntryDictionary = GetOrderedLocalizationEntryDictionary(localizationEntryDictionary, localizableOrderDictionary);

            if (combineIdenticalEntries)
            {
                localizationEntryDictionary = GetCombinedLocalizationEntryDictionary(localizationEntryDictionary);
            }

            return localizationEntryDictionary;
        }

        public static void PopulateLocalizationEntryDictionaryWithApplicationData(Dictionary<string, LocalizationEntry> localizationEntryDictionary, ILocalizable localizable)
        {
            if (localizable != null)
            {
                Dictionary<string, string> descriptionDictionary = localizable.GetDescriptions();
                Dictionary<string, Type> converterDictionary = localizable.GetConverters();
                foreach (KeyValuePair<string, object> kvp in localizable.GetOriginalValues())
                {
                    if (kvp.Value != null)
                    {
                        LocalizationEntry localizationEntry = new LocalizationEntry(Languages);

                        if (descriptionDictionary.ContainsKey(kvp.Key))
                        {
                            localizationEntry.m_Description = descriptionDictionary[kvp.Key];
                        }

                        Type converterType = null;
                        if (converterDictionary.ContainsKey(kvp.Key))
                        {
                            converterType = converterDictionary[kvp.Key];
                        }

                        if (converterType != null)
                        {
                            localizationEntry.m_Converter = LocalizationEntry.GetConverterName(converterType);
                            localizationEntry.m_LocalizedDictionary[s_sourceLanguage] = s_localizationSerializer.Serialize(kvp.Value, converterType);
                            s_localizationSerializer.AutoPopulateTranslations(converterDictionary[kvp.Key], s_sourceLanguage, localizationEntry.m_LocalizedDictionary);
                        }

                        localizationEntryDictionary[kvp.Key] = localizationEntry;
                    }
                }
            }
        }
        
        public static void PopulateLocalizationEntryDictionaryWithCsvData(Dictionary<string, LocalizationEntry> localizationEntryDictionary, string csvData, bool correctOrMarkErrors)
        {
            foreach (KeyValuePair<string, LocalizationEntry> kvp in LocalizationEntry.GetLocalizationEntryDictionaryFromCsv(csvData, s_sourceLanguage, s_translationLanguages))
            {
                PopulateLocalizationEntryDictionaryWithCsvData(localizationEntryDictionary, kvp.Key, kvp.Value, correctOrMarkErrors);
            }
        }
        
        public static void PopulateLocalizationEntryDictionaryWithCsvData(Dictionary<string, LocalizationEntry> localizationEntryDictionary, string csvKey, LocalizationEntry csvLocalizationEntry, bool correctOrMarkErrors)
        {
            string converterTypeName = csvLocalizationEntry.m_Converter;
            string description = csvLocalizationEntry.m_Description;
            string sourceString = csvLocalizationEntry.GetLocalizedString(s_sourceLanguage);
            Dictionary<string, string> languageDictionary = csvLocalizationEntry.m_LocalizedDictionary;

            if (correctOrMarkErrors)
            {
                converterTypeName = RemovePrefix(converterTypeName, INVALID_ENTRY_PREFIX);
            }

            Type converterType = LocalizationEntry.GetConverterType(converterTypeName);
            LocalizationConverter converter = s_localizationSerializer.GetConverter(converterType);

            if (correctOrMarkErrors)
            {
                if (converterType == null)
                {
                    converterTypeName = AddPrefix(converterTypeName, INVALID_ENTRY_PREFIX);
                }
            }

            string[] splitKeys = LocalizationEntry.GetSplitKeys(csvKey);
            string[] splitDescriptions = LocalizationEntry.GetSplitDescriptions(description, splitKeys.Length);

            for (int keyIndex = 0; keyIndex < splitKeys.Length; keyIndex++)
            {
                string splitKey = splitKeys[keyIndex];

                if (correctOrMarkErrors)
                {
                    splitKey = RemovePrefix(splitKey, UNREGISTERED_KEY_PREFIX);
                }

                LocalizationEntry targetLocalizationEntry = null;

                LocalizationEntry appLocalizationEntryWithoutTranslations = null;

                LocalizationEntry csvLocalizationEntryWithTranslations = new LocalizationEntry(Languages);

                csvLocalizationEntryWithTranslations.m_Converter = converterTypeName;
                csvLocalizationEntryWithTranslations.m_Description = splitDescriptions[keyIndex];
                csvLocalizationEntryWithTranslations.m_LocalizedDictionary[s_sourceLanguage] = sourceString;

                if (localizationEntryDictionary.ContainsKey(splitKey))
                {
                    appLocalizationEntryWithoutTranslations = localizationEntryDictionary[splitKey];
                    targetLocalizationEntry = appLocalizationEntryWithoutTranslations;
                }
                else
                {
                    if (correctOrMarkErrors)
                    {
                        bool preserveTranslation = converter != null && (languageDictionary.Any(x => x.Key != s_sourceLanguage && !string.IsNullOrEmpty(x.Value) && converter.GetAutoTranslatedString(sourceString, s_sourceLanguage, x.Key) == null));

                        // Key not found but a translation exists which cannot be regenerated through autotranslation. Assume we want to preserve the translation so add an entry for it.
                        // Otherwise, correct this by simply deleting the entry
                        if (preserveTranslation)
                        {
                            splitKey = AddPrefix(splitKey, UNREGISTERED_KEY_PREFIX);
                            targetLocalizationEntry = csvLocalizationEntryWithTranslations;
                            localizationEntryDictionary[splitKey] = csvLocalizationEntryWithTranslations;
                        }
                    }
                    else
                    {
                        targetLocalizationEntry = csvLocalizationEntryWithTranslations;
                        localizationEntryDictionary[splitKey] = csvLocalizationEntryWithTranslations;
                    }
                }

                if (targetLocalizationEntry != null)
                {
                    if (correctOrMarkErrors)
                    {
                        targetLocalizationEntry.m_LocalizedDictionary[s_sourceLanguage] = GetCorrectedTranslation(s_sourceLanguage, targetLocalizationEntry.GetLocalizedString(s_sourceLanguage), converterType, appLocalizationEntryWithoutTranslations, csvLocalizationEntryWithTranslations);
                    }

                    foreach (KeyValuePair<string, string> tkvp in languageDictionary)
                    {
                        string language = tkvp.Key;
                        string translation = tkvp.Value;

                        if (language != s_sourceLanguage)
                        {
                            if (correctOrMarkErrors)
                            {
                                translation = GetCorrectedTranslation(language, translation, converterType, appLocalizationEntryWithoutTranslations, csvLocalizationEntryWithTranslations);
                            }

                            targetLocalizationEntry.m_LocalizedDictionary[language] = translation;
                        }
                    }
                }
            }
        }

        private static string GetCorrectedTranslation(string language, string translation, Type converterType, LocalizationEntry appLocalizationEntryWithoutTranslations, LocalizationEntry csvLocalizationEntryWithTranslations)
        {
            if (!string.IsNullOrEmpty(translation))
            {
                LocalizationEntry targetLocalizationEntry = csvLocalizationEntryWithTranslations;
                if (appLocalizationEntryWithoutTranslations != null)
                {
                    targetLocalizationEntry = appLocalizationEntryWithoutTranslations;
                }


                if (converterType != null)
                {
                    LocalizationConverter converter = s_localizationSerializer.GetConverter(converterType);
                    
                    if (converter != null)
                    {
                        translation = RemovePrefix(translation, INVALID_ENTRY_PREFIX);

                        string autoTranslation = converter.GetAutoTranslatedString(targetLocalizationEntry.GetLocalizedString(s_sourceLanguage), s_sourceLanguage, language);
                        if (autoTranslation != null)
                        {
                            if (translation != autoTranslation)
                            {
                                translation = autoTranslation;
                            }
                            if (!s_localizationSerializer.StringIsValid(translation, converterType))
                            {
                                translation = AddPrefix(translation, INVALID_ENTRY_PREFIX);
                            }
                        }
                        else
                        {
                            if (language != s_sourceLanguage)
                            {
                                if (appLocalizationEntryWithoutTranslations != null && csvLocalizationEntryWithTranslations != null)
                                {
                                    if (!LocalizationEntry.StringsAreEqual(s_sourceLanguage, appLocalizationEntryWithoutTranslations, csvLocalizationEntryWithTranslations))
                                    {
                                        translation = AddPrefix(translation, OUTDATED_ENTRY_PREFIX + "(" + csvLocalizationEntryWithTranslations.GetLocalizedString(s_sourceLanguage) + ")");
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return translation;
        }

        private static Match GetPrefixMatch(this string str, string prefix)
        {
            Match m = BALANCED_BRACKET_REGEX.Match(str);
            if (m.Success && m.Value.StartsWith("[" + prefix))
            {
                return m;
            }

            return null;
        }

        public static string GetPrefix(string str, string prefix)
        {
            return str.GetPrefixMatch(prefix).Value;
        }

        public static bool HasPrefix(string str, string prefix)
        {
            return str.GetPrefixMatch(prefix) != null;
        }

        public static string AddPrefix(string str, string prefix)
        {
            if (!HasPrefix(str, prefix))
            {
                return "[" + prefix + "]" + str;
            }

            return str;
        }

        public static string RemovePrefix(string str, string prefix)
        {
            if (HasPrefix(str, prefix))
            {
                return str.Substring(GetPrefix(str, prefix).Length);
            }

            return str;
        }

        public static Dictionary<string, LocalizationEntry> GetOrderedLocalizationEntryDictionary(Dictionary<string, LocalizationEntry> localizationEntryDictionary, Dictionary<string, string> localizableOrderDictionary)
        {
            return localizationEntryDictionary
                .OrderBy(x => s_localizationSerializer.GetConverterOrderedPriority(LocalizationEntry.GetConverterType(x.Value.m_Converter)))
                .ThenBy(x => { string localizableKey = GetLocalizableKeyFromLocalizationEntryKey(x.Key); return localizableOrderDictionary.ContainsKey(localizableKey) ? localizableOrderDictionary[localizableKey] : null; })
                .ToDictionary(x => x.Key, x => x.Value);
        }


        public static void RemoveEmptyEntriesFromLocalizationEntryDictionary(Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            foreach (string key in localizationEntryDictionary.Keys.ToList())
            {
                if (string.IsNullOrEmpty(localizationEntryDictionary[key].GetLocalizedString(s_sourceLanguage)))
                {
                    localizationEntryDictionary.Remove(key);
                }
            }
        }

        public static Dictionary<string, LocalizationEntry> GetCombinedLocalizationEntryDictionary(Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            Dictionary<string, LocalizationEntry> combinedLocalizationEntries = new Dictionary<string, LocalizationEntry>();
            KeyValuePair<string, LocalizationEntry>[][] groups = localizationEntryDictionary.GroupBy(x => x.Value.GetGroupingKey(s_sourceLanguage)).Select(x => x.ToArray()).ToArray();
            foreach (KeyValuePair<string, LocalizationEntry>[] group in groups)
            {
                KeyValuePair<string, LocalizationEntry> firstInGroup = group.First();
                KeyValuePair<string, LocalizationEntry>[] trimmedGroup = group.Where(x => !HasPrefix(x.Key, UNREGISTERED_KEY_PREFIX)).OrderBy(x => x.Value.GetDescriptionSource()).ToArray();
                if (trimmedGroup.Length == 0)
                {
                    trimmedGroup = group;
                }

                string[] combinedKeyArray = trimmedGroup.Select(x => x.Key).ToArray();
                LocalizationEntry[] combinedLocalizationEntryArray = trimmedGroup.Select(x => x.Value).ToArray();
                string combinedKeys = LocalizationEntry.GetCombinedKeys(combinedKeyArray);
                if (!combinedLocalizationEntries.ContainsKey(combinedKeys))
                {
                    LocalizationEntry combinedLocalizationEntry = new LocalizationEntry(Languages);
                    combinedLocalizationEntry.m_Converter = firstInGroup.Value.m_Converter;
                    combinedLocalizationEntry.m_Description = LocalizationEntry.GetCombinedDescriptions(combinedLocalizationEntryArray);
                    foreach (KeyValuePair<string, LocalizationEntry> ckvp in group)
                    {
                        string k = ckvp.Key;
                        LocalizationEntry ls = ckvp.Value;
                        foreach (string language in ls.m_LocalizedDictionary.Keys)
                        {
                            string translation = ls.m_LocalizedDictionary[language];
                            if (!string.IsNullOrEmpty(translation))
                            {
                                combinedLocalizationEntry.m_LocalizedDictionary[language] = translation;
                            }
                        }
                    }
                    combinedLocalizationEntries[combinedKeys] = combinedLocalizationEntry;
                }
            }
            return combinedLocalizationEntries;
        }

        public static string ExportLocalizationEntryDictionaryToCsv(Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            return LocalizationEntry.GetCsvFromLocalizationEntryDictionary(localizationEntryDictionary, s_sourceLanguage, s_translationLanguages);
        }
    }
}
