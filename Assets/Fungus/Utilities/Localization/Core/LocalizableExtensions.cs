﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.Localization
{

    public delegate string StringFormatter(string format);

    public static class LocalizableExtensions
    {
        public static string[] SafeGetLocalizedObjects<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter, string language) where TLocalizable : ILocalizable
        {
            string[] result;
            localizable.TryGetLocalizedObjects<TLocalizable>(stringFormatter, language, out result);
            return result;
        }

        public static TValue[] SafeGetLocalizedObjects<TLocalizable, TValue>(this TLocalizable localizable, string language) where TLocalizable : ILocalizable
        {
            TValue[] result;
            localizable.TryGetLocalizedObjects<TLocalizable, TValue>(language, out result);
            return result;
        }

        public static Dictionary<string, string>[] SafeGetLocalizedDictionaries<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter) where TLocalizable : ILocalizable
        {
            Dictionary<string, string>[] result;
            localizable.TryGetLocalizedDictionaries<TLocalizable>(stringFormatter, out result);
            return result;
        }

        public static Dictionary<string, TValue>[] SafeGetLocalizedDictionaries<TLocalizable, TValue>(this TLocalizable localizable) where TLocalizable : ILocalizable
        {
            Dictionary<string, TValue>[] result;
            localizable.TryGetLocalizedDictionaries<TLocalizable, TValue>(out result);
            return result;
        }

        public static Dictionary<string, string> SafeGetLocalizedDictionary<TLocalizable>(this TLocalizable localizable, Expression<Func<TLocalizable, string>> localizedMember, StringFormatter stringFormatter) where TLocalizable : ILocalizable
        {
            Dictionary<string, string> result;
            localizable.TryGetLocalizedDictionary<TLocalizable>(localizedMember, stringFormatter, out result);
            return result;
        }

        public static Dictionary<string, string> SafeGetLocalizedDictionary(this ILocalizable localizable, string key, StringFormatter stringFormatter)
        {
            Dictionary<string, string> result;
            localizable.TryGetLocalizedDictionary(key, stringFormatter, out result);
            return result;
        }

        public static Dictionary<string, TValue> SafeGetLocalizedDictionary<TLocalizable, TValue>(this TLocalizable localizable, Expression<Func<TLocalizable, TValue>> localizedMember) where TLocalizable : ILocalizable
        {
            Dictionary<string, TValue> result;
            localizable.TryGetLocalizedDictionary<TLocalizable, TValue>(localizedMember, out result);
            return result;
        }

        public static Dictionary<string, TValue> SafeGetLocalizedDictionary<TValue>(this ILocalizable localizable, string key)
        {
            Dictionary<string, TValue> result;
            localizable.TryGetLocalizedDictionary<TValue>(key, out result);
            return result;
        }

        public static bool TryGetLocalizedObjects<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter, string language, out string[] result) where TLocalizable : ILocalizable
        {
            Dictionary<string, string>[] localizedDictionaries;
            bool valid = localizable.TryGetLocalizedDictionaries<TLocalizable>(stringFormatter, out localizedDictionaries);
            result = localizedDictionaries.Select(x => x[language]).ToArray();
            return valid;
        }

        public static bool TryGetLocalizedObjects<TLocalizable, TValue>(this TLocalizable localizable, string language, out TValue[] result) where TLocalizable : ILocalizable
        {
            Dictionary<string, TValue>[] localizedDictionaries;
            bool valid = localizable.TryGetLocalizedDictionaries<TLocalizable, TValue>(out localizedDictionaries);
            result = localizedDictionaries.Select(x => x[language]).ToArray();
            return valid;
        }

        public static bool TryGetLocalizedDictionaries<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter, out Dictionary<string, string>[] result) where TLocalizable : ILocalizable
        {
            Dictionary<string, object> dictionary = localizable.GetOriginalValues();

            List<Dictionary<string, string>> localizedDictionaries = new List<Dictionary<string, string>>();

            bool valid = true;
            foreach (string key in dictionary.Keys)
            {
                Dictionary<string, string> localizedDictionary;
                if (!localizable.TryGetLocalizedDictionary(key, stringFormatter, out localizedDictionary))
                {
                    valid = false;
                }
                localizedDictionaries.Add(localizedDictionary);
            }

            result = localizedDictionaries.ToArray();

            return valid;
        }

        public static bool TryGetLocalizedDictionaries<TLocalizable, TValue>(this TLocalizable localizable, out Dictionary<string, TValue>[] result) where TLocalizable : ILocalizable
        {
            Dictionary<string, object> dictionary = localizable.GetOriginalValues();

            List<Dictionary<string, TValue>> localizedDictionaries = new List<Dictionary<string, TValue>>();

            bool valid = true;
            foreach (string key in dictionary.Keys)
            {
                Dictionary<string, TValue> localizedDictionary;
                if (!localizable.TryGetLocalizedDictionary<TValue>(key, out localizedDictionary))
                {
                    valid = false;
                }
                localizedDictionaries.Add(localizedDictionary);
            }

            result = localizedDictionaries.ToArray();

            return valid;
        }

        public static bool TryGetLocalizedDictionary<TLocalizable>(this TLocalizable localizable, Expression<Func<TLocalizable, string>> localizedMember, StringFormatter stringFormatter, out Dictionary<string, string> result) where TLocalizable : ILocalizable
        {
            string key = localizable.GetLocalizedMemberKey(localizedMember);
            return localizable.TryGetLocalizedDictionary(key, stringFormatter, out result);
        }

        public static bool TryGetLocalizedDictionary(this ILocalizable localizable, string key, StringFormatter stringFormatter, out Dictionary<string, string> result)
        {
            bool valid = localizable.TryGetLocalizedDictionary<string>(key, out result);
            result.FormatLocalizedDictionary(stringFormatter);
            return valid;
        }

        public static bool TryGetLocalizedDictionary<TLocalizable, TValue>(this TLocalizable localizable, Expression<Func<TLocalizable, TValue>> localizedMember, out Dictionary<string, TValue> result) where TLocalizable : ILocalizable
        {
            string key = localizable.GetLocalizedMemberKey(localizedMember);
            return localizable.TryGetLocalizedDictionary<TValue>(key, out result);
        }

        public static bool TryGetLocalizedDictionary<TValue>(this ILocalizable localizable, string key, out Dictionary<string, TValue> result)
        {
            return GetLocalizedDictionaryWithExceptionInfo(localizable, key, out result) == null;
        }
        
        public static string[] GetLocalizedObjects<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter, string language) where TLocalizable : ILocalizable
        {
            return localizable.GetLocalizedDictionaries<TLocalizable>(stringFormatter).Select(x => x[language]).ToArray();
        }

        public static TValue[] GetLocalizedObjects<TLocalizable, TValue>(this TLocalizable localizable, string language) where TLocalizable : ILocalizable
        {
            return localizable.GetLocalizedDictionaries<TLocalizable, TValue>().Select(x => x[language]).ToArray();
        }

        public static Dictionary<string, string>[] GetLocalizedDictionaries<TLocalizable>(this TLocalizable localizable, StringFormatter stringFormatter) where TLocalizable : ILocalizable
        {
            Dictionary<string, object> dictionary = localizable.GetOriginalValues();

            List<Dictionary<string, string>> localizedDictionaries = new List<Dictionary<string, string>>();

            foreach (string key in dictionary.Keys)
            {
                localizedDictionaries.Add(localizable.GetLocalizedDictionary(key, stringFormatter));
            }

            return localizedDictionaries.ToArray();
        }

        public static Dictionary<string, TValue>[] GetLocalizedDictionaries<TLocalizable, TValue>(this TLocalizable localizable) where TLocalizable : ILocalizable
        {
            Dictionary<string, object> dictionary = localizable.GetOriginalValues();

            List<Dictionary<string, TValue>> localizedDictionaries = new List<Dictionary<string, TValue>>();

            foreach (string key in dictionary.Keys)
            {
                localizedDictionaries.Add(localizable.GetLocalizedDictionary<TValue>(key));
            }

            return localizedDictionaries.ToArray();
        }

        public static string GetLocalizedMemberKey<TLocalizable, TValue>(this TLocalizable localizable, Expression<Func<TLocalizable, TValue>> localizedMember) where TLocalizable : ILocalizable
        {
            return localizedMember.GetMemberPath();
        }

        public static string GetLocalizedMemberKey<TLocalizable>(this TLocalizable localizable, Expression<Func<TLocalizable, object>> localizedMember) where TLocalizable : ILocalizable
        {
            return localizedMember.GetMemberPath();
        }

        public static string GetLocalizedMemberKey<TLocalizable>(Expression<Func<TLocalizable, object>> localizedMember) where TLocalizable : ILocalizable
        {
            return localizedMember.GetMemberPath();
        }

        public static Dictionary<string, TValue> GetLocalizedDictionary<TLocalizable, TValue>(this TLocalizable localizable, Expression<Func<TLocalizable, TValue>> localizedMember) where TLocalizable : ILocalizable
        {
            string key = localizedMember.GetMemberPath();
            return localizable.GetLocalizedDictionary<TValue>(key);
        }

        public static Dictionary<string, TValue> GetLocalizedDictionary<TValue>(this ILocalizable localizable, string key)
        {
            Dictionary<string, TValue> result;
            LocalizationError[] errors = GetLocalizedDictionaryWithExceptionInfo(localizable, key, out result).OfType<LocalizationError>().ToArray();
            if (errors.Length > 0)
            {
                throw new AggregateException("Encountered errors when attempting to get the Localized Dictionary.", errors);
            }
            return result;
        }

        public static LocalizationException[] GetLocalizedDictionaryWithExceptionInfo<TValue>(this ILocalizable localizable, string key, out Dictionary<string, TValue> result)
        {
            List<LocalizationException> exceptions = new List<LocalizationException>();
            if (!string.IsNullOrEmpty(key))
            {
                Dictionary<string, LocalizationEntry> localizationEntryDictionary = Localizer.FindLocalizationEntryDictionary(localizable.GetKey());
                if (localizationEntryDictionary != null)
                {
                    string localizationEntryKey = localizable.GetMemberKeyPrefix() + key;

                    if (localizationEntryDictionary.ContainsKey(localizationEntryKey))
                    {
                        LocalizationEntry localizationEntry = localizationEntryDictionary[localizationEntryKey];
                        if (localizationEntry != null)
                        {
                            exceptions.AddRange(Localizer.GetLocalizedDictionaryWithExceptionInfo(localizationEntryKey, localizationEntry, out result));
                            if (result == null)
                            {
                                LocalizedDictionaryError e = new LocalizedDictionaryError(string.Format("Localized Dictionary is null ({0})", localizationEntryKey));
                                Localizer.OnError.Invoke(e);
                                exceptions.Add(e);
                            }
                        }
                        else
                        {
                            result = null;
                            LocalizedDictionaryError e = new LocalizedDictionaryError(string.Format("Localization Entry is null ({0})", localizationEntryKey));
                            Localizer.OnError.Invoke(e);
                            exceptions.Add(e);
                        }
                    }
                    else
                    {
                        result = null;
                        LocalizedDictionaryError e = new LocalizedDictionaryError(string.Format("Localization Entry was not found ({0})", localizationEntryKey));
                        Localizer.OnError.Invoke(e);
                        exceptions.Add(e);
                    }
                }
                else
                {
                    result = null;
                    LocalizedDictionaryError e = new LocalizedDictionaryError(string.Format("Localization Entry Dictionary was not registered for localizable: '{0}'", localizable.GetKey()));
                    Localizer.OnError.Invoke(e);
                    exceptions.Add(e);
                }
            }
            else
            {
                result = null;
                LocalizedDictionaryError e = new LocalizedDictionaryError(string.Format("Localizable Member Key was null or empty"));
                Localizer.OnError.Invoke(e);
                exceptions.Add(e);
            }
            return exceptions.ToArray();
        }
        
        public static Dictionary<string, string> GetLocalizedDictionary<TLocalizable>(this TLocalizable localizable, Expression<Func<TLocalizable, string>> localizedMember, StringFormatter stringFormatter) where TLocalizable : ILocalizable
        {
            string key = localizable.GetLocalizedMemberKey(localizedMember);
            return localizable.GetLocalizedDictionary(key, stringFormatter);
        }

        public static Dictionary<string, string> GetLocalizedDictionary(this ILocalizable localizable, string key, StringFormatter stringFormatter)
        {
            Dictionary<string, string> localizedDictionary = localizable.GetLocalizedDictionary<string>(key);
            localizedDictionary.FormatLocalizedDictionary(stringFormatter);
            return localizedDictionary;
        }

        public static void FormatLocalizedDictionary(this Dictionary<string, string> localizationEntryDictionary, StringFormatter stringFormatter)
        {
            if (localizationEntryDictionary != null && stringFormatter != null)
            {
                foreach (string language in localizationEntryDictionary.Keys.ToList())
                {
                    localizationEntryDictionary[language] = stringFormatter(localizationEntryDictionary[language]);
                }
            }
        }

        public static void AppendLocalizedDictionary(this Dictionary<string, string> localizationEntryDictionary, Dictionary<string, string> otherLocalizationEntryDictionary)
        {
            if (localizationEntryDictionary != null && otherLocalizationEntryDictionary != null)
            {
                foreach (string key in localizationEntryDictionary.Keys.ToList())
                {
                    if (otherLocalizationEntryDictionary.ContainsKey(key))
                    {
                        localizationEntryDictionary[key] = localizationEntryDictionary[key] + otherLocalizationEntryDictionary[key];
                    }
                }
            }
        }

        private static bool MemberIsALocalizedCollection(MemberInfo member)
        {
            return (Attribute.IsDefined(member, typeof(LocalizeAttribute), true)) && typeof(ICollection).IsAssignableFrom(member.GetMemberType());
        }

        private static bool MemberShouldBeLocalized(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, TypeCondition typeIsReflectable)
        {
            if (member != null && !Attribute.IsDefined(member, typeof(DontLocalizeAttribute), true))
            {
                if (!typeof(ICollection).IsAssignableFrom(memberType) &&
                    (Attribute.IsDefined(member, typeof(LocalizeAttribute), true) || (declaringMember != null && MemberIsALocalizedCollection(declaringMember) && (!typeIsReflectable(memberType) || typeof(IExplicitLocalizableValue).IsAssignableFrom(memberType)))) ||
                   typeof(IExplicitLocalizableValue).IsAssignableFrom(memberType))
                {
                    object memberValue = member.GetMemberValue(declaringObj, propertyParameters);
                    if (memberValue != null)
                    {
                        IExplicitLocalizableValue explicitLocalizedValue = memberValue as IExplicitLocalizableValue;
                        if (explicitLocalizedValue != null)
                        {
                            if (explicitLocalizedValue.ShouldBeLocalized(member))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static string GetMemberKeyPrefix(this ILocalizable localizable)
        {
            return localizable.GetKey() + "+";
        }

        public static string[] GetLocalLocalizationEntryKeys(this ILocalizable localizable)
        {
            return GetOriginalValues(localizable).Keys.Select(x => x.Replace(localizable.GetMemberKeyPrefix(), "")).ToArray();
        }

        public static string[] GetGlobalLocalizationEntryKeys(this ILocalizable localizable)
        {
            return GetOriginalValues(localizable).Keys.ToArray();
        }

        private static string GetMemberLocalizeKey(MemberInfo member)
        {
            LocalizeAttribute localizeAttribute = member.GetCustomAttribute<LocalizeAttribute>(true);
            if (localizeAttribute != null && !string.IsNullOrEmpty(localizeAttribute.Key))
            {
                return localizeAttribute.Key;
            }

            return member.Name;
        }

        public static Dictionary<string, object> GetOriginalValues(this ILocalizable localizable)
        {
            LocalizableTraverser localizableTraverser = Localizer.LocalizableTraverser;

            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            IExplicitLocalizable explicitLocalizable = localizable as IExplicitLocalizable;
            if (explicitLocalizable != null)
            {
                Dictionary<string, object> explicitDictionary = new Dictionary<string, object>();
                explicitLocalizable.ExplicitlyGetSourceValues(explicitDictionary);
                foreach (KeyValuePair<string, object> kvp in explicitDictionary)
                {
                    string key = GetMemberKeyPrefix(localizable) + kvp.Key;
                    dictionary[key] = kvp.Value;
                }
            }

            localizable.PerformActionOnAllMembersThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => MemberShouldBeLocalized(md, mp, hc, m, mt, d, p, dm, localizableTraverser.ShouldTraverseMembers),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetOriginalValue(md, mp, hc, m, mt, d, p, dm, localizable, dictionary),
                GetMemberLocalizeKey,
                localizableTraverser.GetLocalizableMembers,
                localizableTraverser.ShouldTraverseMembers,
                false);

            return dictionary;
        }

        private static void GetOriginalValue(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, ILocalizable localizable, Dictionary<string, object> dictionary)
        {
            if (member != null)
            {
                string key = GetMemberKeyPrefix(localizable) + memberPath;
                object originalObjectValue = member.GetMemberValue(declaringObj, propertyParameters);
                IExplicitLocalizableValue explicitLocalizedValue = originalObjectValue as IExplicitLocalizableValue;
                if (explicitLocalizedValue != null)
                {
                    Dictionary<string, object> explicitDictionary = new Dictionary<string, object>();
                    explicitLocalizedValue.ExplicitlyGetSourceValues(explicitDictionary);
                    foreach (KeyValuePair<string, object> kvp in explicitDictionary)
                    {
                        dictionary[key + "." + kvp.Key] = kvp.Value;
                    }
                }
                else
                {
                    dictionary[key] = originalObjectValue;
                }
            }
        }

        public static void SetLocalizedValues(this ILocalizable localizable, Dictionary<string, object> dictionary)
        {
            LocalizableTraverser localizableTraverser = Localizer.LocalizableTraverser;

            IExplicitLocalizable explicitLocalizable = localizable as IExplicitLocalizable;
            if (explicitLocalizable != null)
            {
                Dictionary<string, object> localDictionary = ConvertGlobalLocalizedDictionaryToLocalLocalizedDictionary(GetMemberKeyPrefix(localizable), dictionary);
                explicitLocalizable.ExplicitlySetLocalizedValues(localDictionary);
            }

            localizable.PerformActionOnAllMembersThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => MemberShouldBeLocalized(md, mp, hc, m, mt, d, p, dm, localizableTraverser.ShouldTraverseMembers),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => SetLocalizedValue(md, mp, hc, m, mt, d, p, dm, localizable, dictionary),
                GetMemberLocalizeKey,
                localizableTraverser.GetLocalizableMembers,
                localizableTraverser.ShouldTraverseMembers,
                false);
        }

        private static void SetLocalizedValue(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, ILocalizable localizable, Dictionary<string, object> dictionary)
        {
            if (member != null)
            {
                string key = GetMemberKeyPrefix(localizable) + memberPath;

                object originalObjectValue = member.GetMemberValue(declaringObj, propertyParameters);
                IExplicitLocalizableValue explicitLocalizedValue = originalObjectValue as IExplicitLocalizableValue;
                if (explicitLocalizedValue != null)
                {
                    Dictionary<string, object> localDictionary = ConvertGlobalLocalizedDictionaryToLocalLocalizedDictionary(key + ".", dictionary);
                    explicitLocalizedValue.ExplicitlySetLocalizedValues(localDictionary);
                }
                else
                {
                    if (dictionary.ContainsKey(key))
                    {
                        object localizedObject = dictionary[key];
                        member.SetMemberValue(declaringObj, localizedObject, propertyParameters);
                    }
                }
            }
        }

        public static Dictionary<string, string> GetDescriptions(this ILocalizable localizable)
        {
            LocalizableTraverser localizableTraverser = Localizer.LocalizableTraverser;

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            IExplicitLocalizable explicitLocalizable = localizable as IExplicitLocalizable;
            if (explicitLocalizable != null)
            {
                Dictionary<string, string> explicitDictionary = new Dictionary<string, string>();
                explicitLocalizable.ExplicitlyGetDescriptions(explicitDictionary);
                foreach (KeyValuePair<string, string> kvp in explicitDictionary)
                {
                    string key = GetMemberKeyPrefix(localizable) + kvp.Key;
                    string description = GetDescriptionPrefix(localizable);
                    if (!string.IsNullOrEmpty(kvp.Value))
                    {
                        description += " " + kvp.Value;
                    }
                    dictionary.Add(key, description);
                }
            }

            localizable.PerformActionOnAllMembersThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => MemberShouldBeLocalized(md, mp, hc, m, mt, d, p, dm, localizableTraverser.ShouldTraverseMembers),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetDescription(md, mp, hc, m, d, p, dm, localizable, dictionary),
                GetMemberLocalizeKey,
                localizableTraverser.GetLocalizableMembers,
                localizableTraverser.ShouldTraverseMembers,
                false);

            foreach (KeyValuePair<string, object> kvp in localizable.GetOriginalValues())
            {
                if (!dictionary.ContainsKey(kvp.Key))
                {
                    dictionary.Add(kvp.Key, GetDescriptionPrefix(localizable));
                }
            }

            return dictionary.Where(kvp => kvp.Value != null).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        private static void GetDescription(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, ILocalizable localizable, Dictionary<string, string> dictionary)
        {
            if (member != null)
            {
                string key = GetMemberKeyPrefix(localizable) + memberPath;

                object originalObjectValue = member.GetMemberValue(declaringObj, propertyParameters);
                IExplicitLocalizableValue explicitLocalizedValue = originalObjectValue as IExplicitLocalizableValue;
                if (explicitLocalizedValue != null)
                {
                    Dictionary<string, string> explicitDictionary = new Dictionary<string, string>();
                    explicitLocalizedValue.ExplicitlyGetDescriptions(explicitDictionary);
                    foreach (KeyValuePair<string, string> kvp in explicitDictionary)
                    {
                        dictionary[key + "." + kvp.Key] = kvp.Value;
                    }
                }

                LocalizeAttribute localizeAttribute = member.GetCustomAttribute<LocalizeAttribute>(true);
                if (localizeAttribute == null)
                {
                    if (declaringMember != null)
                    {
                        localizeAttribute = declaringMember.GetCustomAttribute<LocalizeAttribute>(true);
                    }
                }

                if (localizeAttribute != null)
                {
                    string description = GetDescriptionPrefix(localizable);
                    if (!string.IsNullOrEmpty(localizeAttribute.Description))
                    {
                        description += " " + localizeAttribute.Description;
                    }
                    if (!dictionary.ContainsKey(key))
                    {
                        dictionary.Add(key, description);
                    }
                }
            }
        }

        public static string GetDescriptionPrefix(this ILocalizable localizable)
        {
            return string.Format("[{0}]", localizable.GetType().Name);
        }

        public static Dictionary<string, Type> GetConverters(this ILocalizable localizable)
        {
            LocalizableTraverser localizableTraverser = Localizer.LocalizableTraverser;
            LocalizationSerializer localizationSerializer = Localizer.LocalizationSerializer;

            Dictionary<string, Type> dictionary = new Dictionary<string, Type>();

            IExplicitLocalizable explicitLocalizable = localizable as IExplicitLocalizable;
            if (explicitLocalizable != null)
            {
                Dictionary<string, Type> explicitDictionary = new Dictionary<string, Type>();
                explicitLocalizable.ExplicitlyGetConverters(explicitDictionary);
                foreach (KeyValuePair<string, Type> kvp in explicitDictionary)
                {
                    string key = GetMemberKeyPrefix(localizable) + kvp.Key;
                    dictionary.Add(key, kvp.Value);
                }
            }

            localizable.PerformActionOnAllMembersThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => MemberShouldBeLocalized(md, mp, hc, m, mt, d, p, dm, localizableTraverser.ShouldTraverseMembers),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetConverter(md, mp, hc, m, mt, d, p, dm, localizationSerializer, localizable, dictionary),
                GetMemberLocalizeKey,
                localizableTraverser.GetLocalizableMembers,
                localizableTraverser.ShouldTraverseMembers,
                false);

            foreach (KeyValuePair<string, object> kvp in localizable.GetOriginalValues())
            {
                if (!dictionary.ContainsKey(kvp.Key))
                {
                    if (kvp.Value != null)
                    {
                        Type originalValueType = kvp.Value.GetType();
                        Type converterType = localizationSerializer.GetDefaultConverter(originalValueType);
                        if (converterType != null)
                        {
                            dictionary.Add(kvp.Key, converterType);
                        }
                    }
                }
            }

            return dictionary.Where(kvp => kvp.Value != null).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        private static void GetConverter(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, LocalizationSerializer localizationSerializer, ILocalizable localizable, Dictionary<string, Type> dictionary)
        {
            if (member != null)
            {
                string key = GetMemberKeyPrefix(localizable) + memberPath;

                object originalObjectValue = member.GetMemberValue(declaringObj, propertyParameters);
                IExplicitLocalizableValue explicitLocalizedValue = originalObjectValue as IExplicitLocalizableValue;
                if (explicitLocalizedValue != null)
                {
                    Dictionary<string, Type> explicitDictionary = new Dictionary<string, Type>();
                    explicitLocalizedValue.ExplicitlyGetConverters(explicitDictionary);
                    foreach (KeyValuePair<string, Type> kvp in explicitDictionary)
                    {
                        dictionary[key + "." + kvp.Key] = kvp.Value;
                    }
                }

                LocalizeAttribute localizeAttribute = member.GetCustomAttributes(true).ToList().OfType<LocalizeAttribute>().FirstOrDefault();
                if (localizeAttribute == null)
                {
                    if (declaringMember != null)
                    {
                        localizeAttribute = declaringMember.GetCustomAttributes(true).ToList().OfType<LocalizeAttribute>().FirstOrDefault();
                    }
                }

                if (localizeAttribute != null)
                {
                    Type converterType = null;
                    if (localizeAttribute.ConverterType != null)
                    {
                        converterType = localizeAttribute.ConverterType;
                    }
                    else
                    {
                        converterType = localizationSerializer.GetDefaultConverter(memberType);
                    }

                    if (converterType != null)
                    {
                        dictionary[key] = converterType;
                    }
                }
            }
        }

        public static Dictionary<string, object> ConvertGlobalLocalizedDictionaryToLocalLocalizedDictionary(string keyPrefix, Dictionary<string, object> globalDictionary)
        {
            Dictionary<string, object> localDictionary = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> kvp in globalDictionary)
            {
                string key = kvp.Key;
                if (key.StartsWith(keyPrefix, StringComparison.Ordinal))
                {
                    key = key.Substring(keyPrefix.Length);
                }
                localDictionary.Add(key, globalDictionary[kvp.Key]);
            }
            return localDictionary;
        }
    }
}