#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.UnityObjectUtilities;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ImprobableStudios.Localization
{

    [CanEditMultipleObjects, CustomEditor(typeof(LocalizationManager), true)]
    public class LocalizationManagerEditor : IdentifiableScriptableObjectEditor
    {
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();
            
            GUILayout.Space(20f);

            using (new EditorGUILayout.VerticalScope(ProcessorBoxStyle))
            {
                string errorTitle = nameof(LocalizationManager) + " Error";
                string errorOk = "OK";

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(LocalizableText).Name + "s",
                    "Attaching " + typeof(LocalizableText).Name + "s",
                    errorTitle,
                    "No Text components found",
                    errorOk,
                    AttachLocalizableTextsToGameObject);

                ExtendedEditorGUI.LineSeparator();

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(LocalizableSprite).Name + "s",
                    "Attaching " + typeof(LocalizableSprite).Name + "s",
                    errorTitle,
                    "No Sprite components found",
                    errorOk,
                    AttachLocalizableSpritesToGameObject);

                ExtendedEditorGUI.LineSeparator();

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(LocalizableAudioClip).Name + "s",
                    "Attaching " + typeof(LocalizableAudioClip).Name + "s",
                    errorTitle,
                    "No AudioClip components found",
                    errorOk,
                    AttachLocalizableAudioClipsToGameObject);

                ExtendedEditorGUI.LineSeparator();

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(LocalizableVideoClip).Name + "s",
                    "Attaching " + typeof(LocalizableVideoClip).Name + "s",
                    errorTitle,
                    "No VideoClip components found",
                    errorOk,
                    AttachLocalizableAudioClipsToGameObject);
            }
        }

        public bool AttachLocalizableTextsToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type localizableType = typeof(LocalizableText);

            Component[] components = 
                (processChildren) ?
                gameObject.GetTextComponentsInChildren() :
                gameObject.GetTextComponents();

            foreach (Component component in components)
            {
                if (component.gameObject.GetComponent(localizableType) == null)
                {
                    Undo.AddComponent(component.gameObject, localizableType);
                }
                found = true;
            }

            return found;
        }

        public bool AttachLocalizableSpritesToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;
            
            Type localizableType = typeof(LocalizableAudioClip);

            Component[] components =
                (processChildren) ?
                gameObject.GetSpriteComponentsInChildren() :
                gameObject.GetSpriteComponents();

            foreach (Component component in components)
            {
                if (component.gameObject.GetComponent(localizableType) == null)
                {
                    Undo.AddComponent(component.gameObject, localizableType);
                }
                found = true;
            }

            return found;
        }

        public bool AttachLocalizableAudioClipsToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type localizableType = typeof(LocalizableAudioClip);
            Type type = typeof(AudioSource);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (Component component in components)
            {
                if (component.gameObject.GetComponent(localizableType) == null)
                {
                    Undo.AddComponent(component.gameObject, localizableType);
                }
                found = true;
            }

            return found;
        }

        public bool AttachLocalizableVideoClipsToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type localizableType = typeof(LocalizableVideoClip);
            Type type = typeof(VideoPlayer);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (Component component in components)
            {
                if (component.gameObject.GetComponent(localizableType) == null)
                {
                    Undo.AddComponent(component.gameObject, localizableType);
                }
                found = true;
            }

            return found;
        }

        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            LocalizationManager t = target as LocalizationManager;

            if (property.name == "m_" + nameof(t.ActiveLanguage))
            {
                ActiveLanguagePopup(GetPropertyControlRect(property, label, options), property, label);
            }
            else
            {
                if (Application.isPlaying)
                {
                    GUI.enabled = false;
                }
                base.DrawProperty(property, label);
                GUI.enabled = true;
            }
        }

        protected void ActiveLanguagePopup(Rect position, SerializedProperty property, GUIContent label)
        {
            LocalizationManager t = target as LocalizationManager;
            
            EditorGUI.BeginChangeCheck();
            LanguageFieldDrawer.DrawProperty(position, property, label);
            if (EditorGUI.EndChangeCheck())
            {
                if (Application.isPlaying)
                {
                    t.ActiveLanguage = property.stringValue;
                }
            }
        }
    }

}

#endif