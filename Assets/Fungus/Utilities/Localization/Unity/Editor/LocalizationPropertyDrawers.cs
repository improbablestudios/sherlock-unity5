#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System;
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.Localization
{

    [CustomPropertyDrawer(typeof(LanguageFieldAttribute))]
    public class LanguageFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            string selectedLanguage = property.stringValue;
            selectedLanguage = DrawField(position, selectedLanguage, label);
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = selectedLanguage;
            }
            EditorGUI.EndProperty();
        }

        public static string DrawField(Rect position, string language, GUIContent label)
        {
            string[] languageCultureNames = LocalizationManager.Instance.Languages;
            int selectedIndex = Array.FindIndex(languageCultureNames, i => i == language);
            GUIContent[] displayOptions = languageCultureNames.Select(i => new GUIContent(LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(i))).ToArray();
            EditorGUI.BeginChangeCheck();
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, displayOptions);
            if (EditorGUI.EndChangeCheck())
            {
                language = languageCultureNames[selectedIndex];
            }
            return language;
        }
    }

    [CustomPropertyDrawer(typeof(SelectNewLanguageFieldAttribute))]
    public class SelectNewLanguageFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            string selectedLanguage = property.stringValue;
            selectedLanguage = DrawField(position, selectedLanguage, label);
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = selectedLanguage;
            }
            EditorGUI.EndProperty();
        }

        public static string DrawField(Rect position, string language, GUIContent label)
        {
            string[] newLanguageCultureNames = Localizer.DotNetSpecificCultureNames;
            GUIContent[] newLanguageCultureDisplayOptions = newLanguageCultureNames.Select(i => new GUIContent(LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(i))).ToArray();
            language = SearchPopupGUI.TextSearchPopupField(position, language, label, null, newLanguageCultureNames, newLanguageCultureDisplayOptions, true);
            return language;
        }
    }

}
#endif