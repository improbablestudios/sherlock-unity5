#if UNITY_EDITOR
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.ReorderableListUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ImprobableStudios.ResourceTrackingUtilities;
using ImprobableStudios.BaseWindowUtilities;

namespace ImprobableStudios.Localization
{
    public class LocalizationWindow : BaseWindow
    {
        public enum FilterType
        {
            ShowAll,
            ShowNeedsTranslation,
        }

        [Flags]
        public enum SearchType
        {
            Key = 1,
            Converter = 2,
            Description = 4,
            SourceText = 8,
            TranslationText = 16,
        }

        [MenuItem("Tools/Localization/Localization Window", false, 10010)]
        [MenuItem("Window/Localization/Localization", false, 10010)]
        private static void Init()
        {
            ShowWindow<LocalizationWindow>();
        }

        public override Vector2 GetMinWindowSize()
        {
            return new Vector2(400, 200);
        }

        public class CachedLocalizationInfo
        {
            public LocalizationBuilder m_LocalizationBuilder;
            public bool m_Foldout;
            public bool m_HasUnsavedChanges;
            public bool m_NeedsUpdate;
            public int m_RegisteredEntryPageIndex;
            public int m_UnregisteredEntryPageIndex;
            public FilterType m_FilterType;
            public SearchType m_SearchType = (SearchType)~0;
            public string m_SearchString = "";
            public Dictionary<string, LocalizationEntry> m_LocalizationEntryDictionary = new Dictionary<string, LocalizationEntry>();
            public Dictionary<string, bool> m_LocalizationEntryFoldoutDictionary = new Dictionary<string, bool>();
            public Dictionary<KeyValuePair<string, string>, bool> m_LocalizationEntryNeedsUpdateDictionary = new Dictionary<KeyValuePair<string, string>, bool>();

            public CachedLocalizationInfo(LocalizationBuilder localizationBuilder)
            {
                m_LocalizationBuilder = localizationBuilder;
                RefreshLocalizationEntries();
            }

            public void RefreshAll(bool discardUserChanges)
            {
                if (discardUserChanges)
                {
                    RefreshLocalizationEntries();
                }
                else
                {
                    UpdateLocalizationEntries();
                }
            }

            private Dictionary<string, LocalizationEntry> GetLocalizationEntryDictionary()
            {
                if (m_LocalizationBuilder.LocalizationFile != null)
                {
                    Dictionary<string, LocalizationEntry> localizationEntries = m_LocalizationBuilder.GetLocalizationEntryDictionaryFromLocalizationFile();
                    if (localizationEntries != null)
                    {
                        if (m_LocalizationBuilder.CombineIdenticalEntries)
                        {
                            localizationEntries = Localizer.GetCombinedLocalizationEntryDictionary(localizationEntries);
                        }
                        return localizationEntries;
                    }
                }
                return new Dictionary<string, LocalizationEntry>();
            }

            // THIS WILL DISCARD USER CHANGES
            public void RefreshLocalizationEntries()
            {
                m_LocalizationEntryDictionary = GetLocalizationEntryDictionary();
                RefreshLocalizationEntryFoldouts();
                RefreshTranslationNeedsUpdate();
                m_HasUnsavedChanges = false;
                m_NeedsUpdate = m_LocalizationEntryNeedsUpdateDictionary.Any(x => x.Value);
            }

            public void RefreshLocalizationEntryFoldouts()
            {
                m_LocalizationEntryFoldoutDictionary.Clear();
                foreach (string key in m_LocalizationEntryDictionary.Keys)
                {
                    if (!m_LocalizationEntryFoldoutDictionary.ContainsKey(key))
                    {
                        m_LocalizationEntryFoldoutDictionary[key] = false;
                    }
                }
            }

            public void RefreshTranslationNeedsUpdate()
            {
                m_LocalizationEntryNeedsUpdateDictionary.Clear();
                foreach (string key in m_LocalizationEntryDictionary.Keys)
                {
                    LocalizationEntry localizationEntry = m_LocalizationEntryDictionary[key];
                    foreach (string language in localizationEntry.m_LocalizedDictionary.Keys)
                    {
                        string translation = localizationEntry.m_LocalizedDictionary[language];
                        KeyValuePair<string, string> keyAndLanguage = new KeyValuePair<string, string>(key, language);
                        if (!m_LocalizationEntryNeedsUpdateDictionary.ContainsKey(keyAndLanguage))
                        {
                            m_LocalizationEntryNeedsUpdateDictionary[keyAndLanguage] = NeedsTranslationUpdate(translation);
                        }
                    }
                }
            }

            public void UpdateLocalizationEntries()
            {
                Dictionary<string, LocalizationEntry> newLocalizationEntries = GetLocalizationEntryDictionary();
                foreach (KeyValuePair<string, LocalizationEntry> kvp in newLocalizationEntries)
                {
                    if (!m_LocalizationEntryDictionary.ContainsKey(kvp.Key))
                    {
                        m_LocalizationEntryDictionary[kvp.Key] = kvp.Value;
                    }
                }
                foreach (string key in m_LocalizationEntryDictionary.Keys.ToList())
                {
                    if (!newLocalizationEntries.ContainsKey(key))
                    {
                        m_LocalizationEntryDictionary.Remove(key);
                    }
                }
                RefreshLocalizationEntryFoldouts();
            }

            public bool GetCachedNeedsTranslationUpdate(string key, string langauge)
            {
                KeyValuePair<string, string> pair = new KeyValuePair<string, string>(key, langauge);
                return m_LocalizationEntryNeedsUpdateDictionary.ContainsKey(pair) && m_LocalizationEntryNeedsUpdateDictionary[pair];
            }

            protected bool NeedsTranslationUpdate(string translation)
            {
                return string.IsNullOrEmpty(translation) || Localizer.HasPrefix(translation, Localizer.OUTDATED_ENTRY_PREFIX) || Localizer.HasPrefix(translation, Localizer.INVALID_ENTRY_PREFIX);
            }

            public bool IsInSearch(string key, LocalizationEntry localizationEntry, string activeLanguage, string sourceLanguage)
            {
                if (string.IsNullOrEmpty(m_SearchString))
                {
                    return true;
                }
                if (m_SearchType.HasFlag(SearchType.Key) && key.Contains(m_SearchString))
                {
                    return true;
                }
                if (m_SearchType.HasFlag(SearchType.Converter) && localizationEntry.m_Converter.Contains(m_SearchString))
                {
                    return true;
                }
                if (m_SearchType.HasFlag(SearchType.Description) && localizationEntry.m_Description.Contains(m_SearchString))
                {
                    return true;
                }
                if (m_SearchType.HasFlag(SearchType.SourceText) && !string.IsNullOrEmpty(localizationEntry.GetLocalizedString(sourceLanguage)) && localizationEntry.GetLocalizedString(sourceLanguage).Contains(m_SearchString))
                {
                    return true;
                }
                if (m_SearchType.HasFlag(SearchType.TranslationText) && !string.IsNullOrEmpty(localizationEntry.GetLocalizedString(activeLanguage)) && localizationEntry.GetLocalizedString(activeLanguage).Contains(m_SearchString))
                {
                    return true;
                }
                return false;
            }
        }

        private string m_activeLanguage = "";
        private string m_sourceLanguage = "";
        private LocalizationBuilder[] m_allLocalizationBuilders;
        private LocalizationBuilder[] m_localizationBuildersInScene;
        private LocalizationBuilder[] m_localizationBuildersInAssets;
        private float m_selectableLabelWidth;
        private string m_searchControlName;
        private readonly Color m_errorColor = new Color(1, 0.8f, 0.8f, 1);
        public const int NUM_ENTRIES_PER_PAGE = 10;
        public const string DELETE_KEY = "<DELETE KEY>";

        private Dictionary<LocalizationBuilder, CachedLocalizationInfo> m_cachedLocalizationInfos = new Dictionary<LocalizationBuilder, CachedLocalizationInfo>();

        private Vector2 m_scrollPosition = new Vector2();

        private GUIStyle m_titleStyle;
        private GUIStyle m_numberStyle;
        private GUIStyle m_foldoutDescriptionStyle;
        private GUIStyle m_selectableLabelStyle;
        private GUIStyle m_toolbarSearchTypeStyle;

        private GUIStyle TitleStyle
        {
            get
            {
                if (m_titleStyle == null)
                {
                    m_titleStyle = new GUIStyle(EditorStyles.label);
                    m_titleStyle.alignment = TextAnchor.MiddleCenter;
                }
                return m_titleStyle;
            }
        }
        
        private GUIStyle NumberStyle
        {
            get
            {
                if (m_numberStyle == null)
                {
                    m_numberStyle = new GUIStyle(EditorStyles.label);
                    m_numberStyle.alignment = TextAnchor.MiddleCenter;
                }
                return m_numberStyle;
            }
        }

        private GUIStyle FoldoutDescriptionStyle
        {
            get
            {
                if (m_foldoutDescriptionStyle == null)
                {
                    m_foldoutDescriptionStyle = new GUIStyle(EditorStyles.foldout);
                    m_foldoutDescriptionStyle.wordWrap = false;
                    m_foldoutDescriptionStyle.fontStyle = FontStyle.Bold;
                    m_foldoutDescriptionStyle.clipping = TextClipping.Clip;
                }
                return m_foldoutDescriptionStyle;
            }
        }

        private GUIStyle SelectableLabelStyle
        {
            get
            {
                if (m_selectableLabelStyle == null)
                {
                    m_selectableLabelStyle = new GUIStyle(EditorStyles.wordWrappedLabel);
                    m_selectableLabelStyle.wordWrap = true;
                }
                return m_selectableLabelStyle;
            }
        }

        private GUIStyle HeaderStyle
        {
            get
            {
                GUIStyle headerStyle = new GUIStyle(GUI.skin.GetStyle("In BigTitle"));
                headerStyle.font = EditorStyles.boldLabel.font;
                headerStyle.fixedWidth = 0;
                headerStyle.fixedHeight = 0;
                headerStyle.stretchWidth = true;
                headerStyle.stretchHeight = true;
                return headerStyle;
            }
        }

        private GUIStyle ToolbarFilterTypeStyle
        {
            get
            {
                return EditorStyles.toolbarDropDown;
            }
        }

        private GUIStyle ToolbarSearchTextFieldStyle
        {
            get
            {
                return GUI.skin.FindStyle("ToolbarSeachTextField");
            }
        }

        private GUIStyle ToolbarSearchCancelButtonStyle
        {
            get
            {
                return GUI.skin.FindStyle("ToolbarSeachCancelButton");
            }
        }

        private GUIStyle ToolbarSearchCancelButtonEmptyStyle
        {
            get
            {
                return GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty");
            }
        }

        private GUIStyle ToolbarSearchTypeStyle
        {
            get
            {
                if (m_toolbarSearchTypeStyle == null)
                {
                    Texture2D icon = EditorGUIUtility.FindTexture("FilterByType");
                    m_toolbarSearchTypeStyle = new GUIStyle();
                    m_toolbarSearchTypeStyle.imagePosition = ImagePosition.ImageOnly;
                    m_toolbarSearchTypeStyle.normal.background = icon;
                    m_toolbarSearchTypeStyle.active.background = icon;
                    m_toolbarSearchTypeStyle.focused.background = icon;
                    m_toolbarSearchTypeStyle.hover.background = icon;
                    m_toolbarSearchTypeStyle.stretchWidth = false;
                    m_toolbarSearchTypeStyle.stretchHeight = false;
                    m_toolbarSearchTypeStyle.fixedWidth = icon.width;
                    m_toolbarSearchTypeStyle.fixedHeight = icon.height;
                }

                return m_toolbarSearchTypeStyle;
            }
        }

        private GUIStyle ToolbarFoldStyle
        {
            get
            {
                return EditorStyles.toolbarButton;
            }
        }

        private string ViewingHelpText
        {
            get
            {
                return "VIEWING SOURCE: " + LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(m_sourceLanguage).ToUpper();
            }
        }

        private string TranslatingHelpText
        {
            get
            {
                return "TRANSLATING TO: " + LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(m_activeLanguage).ToUpper();
            }
        }

        protected virtual void OnEnable()
        {
            if (m_cachedLocalizationInfos.Count == 0)
            {
                EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
                autoRepaintOnSceneChange = true;
                RefreshWindow();
            }
            m_searchControlName = GetHashCode() + ".Search";
        }

        protected virtual void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        protected virtual void OnFocus()
        {
            RefreshCachedLocalizations();
        }

        protected virtual void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                if (m_cachedLocalizationInfos.Any(kvp => kvp.Value.m_HasUnsavedChanges))
                {
                    if (EditorUtility.DisplayDialog("Save Changes?",
                                "Changes will be lost upon playing if they are not saved.",
                                "SAVE CHANGES", "DISCARD CHANGES"))
                    {
                        LocalizationManager.Instance.SetupLocalizer();
                        foreach (LocalizationBuilder localizationBuilder in m_allLocalizationBuilders)
                        {
                            ExportTranslation(localizationBuilder, GetLocalizationPath(localizationBuilder), Localizer.ExportLocalizationEntryDictionaryToCsv(m_cachedLocalizationInfos[localizationBuilder].m_LocalizationEntryDictionary));
                        }
                    }
                }
            }
        }

        protected virtual void RefreshWindow()
        {
            RefreshCachedLocalizations();
            RefreshCachedLocalizationInfos();
        }

        protected virtual void UpdateWindow()
        {
            RefreshCachedLocalizations();
            UpdateCachedLocalizationInfos();

            Repaint();
        }

        private void RefreshCachedLocalizations()
        {
            m_localizationBuildersInScene = ResourceTracker.FindLoadedInstanceObjectsOfType<LocalizationBuilder>();
            m_localizationBuildersInAssets = ResourceTracker.GetCachedPrefabObjectsOfType<LocalizationBuilder>();
            List<LocalizationBuilder> localizationBuildersToDisplay = new List<LocalizationBuilder>();
            localizationBuildersToDisplay.AddRange(m_localizationBuildersInScene);
            localizationBuildersToDisplay.AddRange(m_localizationBuildersInAssets);
            m_allLocalizationBuilders = localizationBuildersToDisplay.Distinct().ToArray();
        }

        private void RefreshCachedLocalizationInfos()
        {
            m_cachedLocalizationInfos.Clear();
            UpdateCachedLocalizationInfos();
        }

        private void UpdateCachedLocalizationInfos()
        {
            foreach (LocalizationBuilder localizationBuilder in m_allLocalizationBuilders)
            {
                UpdateCachedLocalizationInfo(localizationBuilder);
            }
            foreach (LocalizationBuilder cachedLocalizationBuilder in m_cachedLocalizationInfos.Keys.ToList())
            {
                if (!m_allLocalizationBuilders.ToList().Contains(cachedLocalizationBuilder) || cachedLocalizationBuilder == null)
                {
                    m_cachedLocalizationInfos.Remove(cachedLocalizationBuilder);
                }
            }
        }

        private void UpdateCachedLocalizationInfo(LocalizationBuilder localizationBuilder)
        {
            if (!m_cachedLocalizationInfos.ContainsKey(localizationBuilder))
            {
                if (localizationBuilder != null)
                {
                    CachedLocalizationInfo cachedLocalizationInfo = new CachedLocalizationInfo(localizationBuilder);
                    m_cachedLocalizationInfos[localizationBuilder] = cachedLocalizationInfo;
                    localizationBuilder.PropertyChanged += UpdateWindow;
                }
            }
            else
            {
                if (localizationBuilder != null && m_cachedLocalizationInfos[localizationBuilder].m_LocalizationBuilder != null)
                {
                    m_cachedLocalizationInfos[localizationBuilder].UpdateLocalizationEntries();
                }
                else
                {
                    localizationBuilder.PropertyChanged -= UpdateWindow;
                    m_cachedLocalizationInfos.Remove(localizationBuilder);
                }
            }
        }
        
        protected override void OnGUI()
        {
            base.OnGUI();

            DrawWindow();
        }

        protected virtual void DrawWindow()
        {
            m_sourceLanguage = LocalizationManager.Instance.SourceLanguage;

            if (m_allLocalizationBuilders != null)
            {
                if (m_allLocalizationBuilders.Length == 0)
                {
                    DrawMessage("Add a localization component to the GameObject which contains objects you want to localize", MessageType.Info);
                    if (GUILayout.Button("Search for Localization Builders"))
                    {
                        RefreshCachedLocalizations();
                    }
                    return;
                }
            }
            else
            {
                return;
            }

            Rect blockRect = new Rect(6, 6, this.position.width - 12, this.position.height - 12);
            using (GUILayout.AreaScope areaScope = new GUILayout.AreaScope(blockRect))
            {
                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    GUIContent title = new GUIContent("Localization");
                    EditorGUILayout.LabelField(title, HeaderStyle, GUILayout.Height(HeaderStyle.CalcSize(title).y));

                    DrawSourceLanguageField();
                    DrawActiveLanguageField();
                }

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_scrollPosition))
                    {
                        m_scrollPosition = scrollView.scrollPosition;

                        if (m_localizationBuildersInScene.Length > 0)
                        {
                            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                            {
                                EditorGUILayout.LabelField(new GUIContent("Scene Localization Builders"), TitleStyle);

                                foreach (LocalizationBuilder localizationBuilder in m_localizationBuildersInScene)
                                {
                                    if (localizationBuilder != null)
                                    {
                                        if (m_cachedLocalizationInfos.ContainsKey(localizationBuilder))
                                        {
                                            DrawLocalizationGUI(localizationBuilder);
                                        }
                                        else
                                        {
                                            RefreshCachedLocalizationInfos();
                                        }
                                    }
                                    else
                                    {
                                        RefreshCachedLocalizations();
                                    }
                                }
                            }
                        }

                        if (m_localizationBuildersInAssets.Length > 0)
                        {
                            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                            {
                                EditorGUILayout.LabelField(new GUIContent("Asset Localization Builders"), TitleStyle);

                                foreach (LocalizationBuilder localizationBuilder in m_localizationBuildersInAssets)
                                {
                                    if (localizationBuilder != null)
                                    {
                                        if (m_cachedLocalizationInfos.ContainsKey(localizationBuilder))
                                        {
                                            DrawLocalizationGUI(localizationBuilder);
                                        }
                                        else
                                        {
                                            RefreshCachedLocalizationInfos();
                                        }
                                    }
                                    else
                                    {
                                        RefreshCachedLocalizations();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected virtual void DrawSourceLanguageField()
        {
            EditorGUILayout.LabelField(new GUIContent("Source Language"), new GUIContent(LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(LocalizationManager.Instance.SourceLanguage)), EditorStyles.helpBox);
        }

        protected virtual void DrawActiveLanguageField()
        {
            if (m_activeLanguage == "")
            {
                m_activeLanguage = m_sourceLanguage;
            }
            EditorGUI.BeginChangeCheck();
            m_activeLanguage = LanguageFieldDrawer.DrawField(EditorGUILayout.GetControlRect(), m_activeLanguage, new GUIContent("Active Language"));
            if (EditorGUI.EndChangeCheck())
            {
                if (Application.isPlaying)
                {
                    LocalizationManager.Instance.ActiveLanguage = m_activeLanguage;
                }
            }
            else
            {
                if (Application.isPlaying)
                {
                    m_activeLanguage = LocalizationManager.Instance.ActiveLanguage;
                }
            }
        }

        protected virtual void DrawLocalizationGUI(LocalizationBuilder localizationBuilder)
        {
            if (m_cachedLocalizationInfos.ContainsKey(localizationBuilder))
            {
                CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];

                if (info.m_NeedsUpdate)
                {
                    GUI.backgroundColor = m_errorColor;
                }

                using (new EditorGUILayout.VerticalScope(ReorderableListStyles.GetHeaderStyle(info.m_Foldout)))
                {
                    GUI.backgroundColor = Color.white;
                    info.m_Foldout = EditorGUI.Foldout(EditorGUILayout.GetControlRect(), info.m_Foldout, new GUIContent(localizationBuilder.gameObject.name), true);
                }

                if (info.m_Foldout)
                {
                    using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ContainerStyle))
                    {
                        DrawExportLocalizationButton(localizationBuilder);

                        FoldoutToolbar(info);
                        DrawRegisteredLocalizationEntries(localizationBuilder);
                        if (HasUnregisteredLocalizationEntries(localizationBuilder))
                        {
                            DrawUnregisteredLocalizationEntries(localizationBuilder);
                        }
                    }
                }
            }
        }

        public void DrawExportLocalizationButton(LocalizationBuilder localizationBuilder)
        {
            CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];
            Dictionary<string, LocalizationEntry> localizationEntries = info.m_LocalizationEntryDictionary;

            if (info.m_HasUnsavedChanges)
            {
                GUI.backgroundColor = m_errorColor;
            }
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.white;
                EditorGUILayout.ObjectField(new GUIContent("Localization Builder"), localizationBuilder, typeof(LocalizationBuilder), false);
                using (new EditorGUILayout.HorizontalScope())
                {
                    localizationBuilder.LocalizationFile = EditorGUILayout.ObjectField(new GUIContent("Localization File"), localizationBuilder.LocalizationFile, typeof(TextAsset), false) as TextAsset;
                    if (GUILayout.Button("Save", GUILayout.MaxWidth(50)))
                    {
                        LocalizationManager.Instance.SetupLocalizer();
                        ExportTranslation(localizationBuilder, GetLocalizationPath(localizationBuilder), Localizer.ExportLocalizationEntryDictionaryToCsv(localizationEntries));
                    }
                    if (localizationBuilder.LocalizationFile != null)
                    {
                        if (GUILayout.Button("Load", GUILayout.MaxWidth(50)))
                        {
                            if (EditorUtility.DisplayDialog("Load translations from CSV file",
                                "Loading from the csv file will overwrite all translations entered in the editor for this localization. Are you sure you want to overwrite these changes?",
                                "YES", "NO"))
                            {
                                LoadLocalizationCSV(localizationBuilder);
                            }
                        }
                    }
                }
                if (m_cachedLocalizationInfos[localizationBuilder].m_HasUnsavedChanges)
                {
                    EditorGUILayout.HelpBox("CLICK 'SAVE' TO SAVE YOUR CHANGES!", MessageType.Warning);
                }
                else
                {
                    if (m_activeLanguage == m_sourceLanguage)
                    {
                        EditorGUILayout.HelpBox(ViewingHelpText, MessageType.Info);
                    }
                    else
                    {
                        EditorGUILayout.HelpBox(TranslatingHelpText, MessageType.Info);
                    }
                }
            }
        }

        public string GetLocalizationPath(LocalizationBuilder localizationBuilder)
        {
            if (localizationBuilder.LocalizationFile == null)
            {
                return LocalizationBuilderEditor.GetExportLocalizationFilePath(localizationBuilder.name);
            }

            return AssetDatabase.GetAssetPath(localizationBuilder.LocalizationFile);
        }

        public void ExportTranslation(LocalizationBuilder localizationBuilder, string path, string csvData)
        {
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    File.WriteAllText(path, csvData);
                    AssetDatabase.ImportAsset(path);

                    EditorUtility.DisplayDialog("Localization File Updated", "Exported " + LocalizationEntry.CountLocalizationEntriesInCsv(csvData) + " localization items.", "OK");

                    TextAsset textAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;
                    if (textAsset != null)
                    {
                        localizationBuilder.LocalizationFile = textAsset;
                    }

                    LoadLocalizationCSV(localizationBuilder);
                }
            }
            catch (Exception)
            {
                EditorUtility.DisplayDialog("SAVE ERROR",
                "Cannot save changes while the localization file is open!\n\nPlease close the csv before attempting to save.",
                "OK");
            }
        }

        public void LoadLocalizationCSV(LocalizationBuilder localizationBuilder)
        {
            m_cachedLocalizationInfos[localizationBuilder].RefreshLocalizationEntries();
        }

        public int PageToolbar(int pageIndex, int maxNumPages)
        {
            if (pageIndex < 0)
            {
                pageIndex = 0;
            }
            if (pageIndex > maxNumPages - 1)
            {
                pageIndex = maxNumPages - 1;
            }

            if (maxNumPages > 1)
            {
                GUIContent label = new GUIContent(string.Format("(Page {0}/{1})", pageIndex + 1, maxNumPages));
                int toolbarInt = 1;
                GUIContent[] toolbarStrings = new GUIContent[] { new GUIContent("<--"), label, new GUIContent("-->") };
                toolbarInt = GUI.Toolbar(EditorGUILayout.GetControlRect(), toolbarInt, toolbarStrings);
                if (toolbarInt == 0)
                {
                    if (pageIndex > 0)
                    {
                        return --pageIndex;
                    }
                }
                if (toolbarInt == 2)
                {
                    if (pageIndex < maxNumPages - 1)
                    {
                        return ++pageIndex;
                    }
                }
            }

            return pageIndex;
        }

        public void FoldoutToolbar(CachedLocalizationInfo info)
        {
            using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
            {
                float maxFilterTypeWidth = ExtendedEditorGUI.GetDisplayNames(Enum.GetNames(typeof(FilterType))).Max(x => ToolbarFilterTypeStyle.CalcSize(new GUIContent(x)).x);
                if (m_activeLanguage == m_sourceLanguage)
                {
                    info.m_FilterType = FilterType.ShowAll;
                }
                else
                {
                    EditorGUI.BeginChangeCheck();
                    info.m_FilterType = (FilterType)EditorGUILayout.EnumPopup(info.m_FilterType, ToolbarFilterTypeStyle, GUILayout.Width(maxFilterTypeWidth));
                    if (EditorGUI.EndChangeCheck())
                    {
                        info.m_RegisteredEntryPageIndex = 0;
                    }
                }

                GUILayout.Space(4);

                GUI.SetNextControlName(m_searchControlName);
                info.m_SearchString = EditorGUILayout.TextField(info.m_SearchString, ToolbarSearchTextFieldStyle);
                if (info.m_SearchString.Length > 0)
                {
                    if (GUILayout.Button(GUIContent.none, ToolbarSearchCancelButtonStyle))
                    {
                        info.m_SearchString = "";
                        GUI.FocusControl(null);
                    }
                }
                else
                {
                    GUILayout.Box(GUIContent.none, ToolbarSearchCancelButtonEmptyStyle);
                }

                info.m_SearchType = (SearchType)EditorGUILayout.EnumFlagsField(info.m_SearchType, ToolbarSearchTypeStyle, GUILayout.Width(ToolbarSearchTypeStyle.fixedWidth));

                GUILayout.Space(4);

                Dictionary<string, bool> localizationEntryFoldouts = info.m_LocalizationEntryFoldoutDictionary;
                GUIContent expandAllLabel = new GUIContent("Expand");
                if (GUILayout.Button(expandAllLabel, ToolbarFoldStyle, GUILayout.Width(ToolbarFoldStyle.CalcSize(expandAllLabel).x)))
                {
                    foreach (string key in localizationEntryFoldouts.Keys.ToList())
                    {
                        localizationEntryFoldouts[key] = true;
                    }
                }

                GUIContent collapseAllLabel = new GUIContent("Collapse");
                if (GUILayout.Button(collapseAllLabel, ToolbarFoldStyle, GUILayout.Width(ToolbarFoldStyle.CalcSize(collapseAllLabel).x)))
                {
                    foreach (string key in localizationEntryFoldouts.Keys.ToList())
                    {
                        localizationEntryFoldouts[key] = false;
                    }
                }
            }
        }
        
        public void DrawRegisteredLocalizationEntries(LocalizationBuilder localizationBuilder)
        {
            if (m_cachedLocalizationInfos.ContainsKey(localizationBuilder))
            {
                CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];
                Dictionary<string, LocalizationEntry> localizationEntries = info.m_LocalizationEntryDictionary;
                Dictionary<string, bool> localizationEntryFoldouts = info.m_LocalizationEntryFoldoutDictionary;
                if (localizationEntries.Keys.Count > 0)
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        string[] keys = localizationEntries
                            .Where(x => 
                                    !Localizer.HasPrefix(x.Key, Localizer.UNREGISTERED_KEY_PREFIX) && 
                                    (info.m_FilterType == FilterType.ShowAll || (info.m_FilterType == FilterType.ShowNeedsTranslation && info.GetCachedNeedsTranslationUpdate(x.Key, m_activeLanguage))) && 
                                    (info.IsInSearch(x.Key, x.Value, m_activeLanguage, m_sourceLanguage)))
                            .Select(x => x.Key)
                            .ToArray();
                        int startingEntryIndex = info.m_RegisteredEntryPageIndex * NUM_ENTRIES_PER_PAGE;
                        int i = startingEntryIndex;

                        info.m_RegisteredEntryPageIndex = PageToolbar(info.m_RegisteredEntryPageIndex, (int)Math.Ceiling((float)keys.Length / NUM_ENTRIES_PER_PAGE));

                        string maxEntryNumberString = keys.Length.ToString();
                        int maxEntryNumberStringLength = maxEntryNumberString.Length;
                        float maxEntryNumberWidth = NumberStyle.CalcSize(new GUIContent(maxEntryNumberString)).x;

                        while (i >= 0 && i < keys.Length && i < (startingEntryIndex + NUM_ENTRIES_PER_PAGE))
                        {
                            string key = keys[i];
                            LocalizationEntry localizationEntry = localizationEntries[key];
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                EditorGUILayout.LabelField((i + 1).ToString("D" + maxEntryNumberStringLength), NumberStyle, GUILayout.Width(maxEntryNumberWidth), GUILayout.Height(36));
                                localizationEntryFoldouts[key] = RegisteredLocalizationEntryFoldout(localizationEntryFoldouts[key], localizationBuilder, localizationEntry, key);
                            }
                            i++;
                        }
                    }
                }
            }
        }

        public bool HasUnregisteredLocalizationEntries(LocalizationBuilder localizationBuilder)
        {
            CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];
            Dictionary<string, LocalizationEntry> localizationEntries = info.m_LocalizationEntryDictionary.Where(x => info.IsInSearch(x.Key, x.Value, m_activeLanguage, m_sourceLanguage)).ToDictionary(x => x.Key, x => x.Value);

            foreach (string localizationEntryKey in localizationEntries.Keys.ToList())
            {
                if (localizationEntries[localizationEntryKey] != null)
                {
                    if (Localizer.HasPrefix(localizationEntryKey, Localizer.UNREGISTERED_KEY_PREFIX))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void DrawUnregisteredLocalizationEntries(LocalizationBuilder localizationBuilder)
        {
            CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];
            Dictionary<string, LocalizationEntry> localizationEntries = info.m_LocalizationEntryDictionary.Where(x => info.IsInSearch(x.Key, x.Value, m_activeLanguage, m_sourceLanguage)).ToDictionary(x => x.Key, x => x.Value);
            Dictionary<string, bool> localizationEntryFoldouts = info.m_LocalizationEntryFoldoutDictionary;

            GUI.backgroundColor = m_errorColor;
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.white;

                foreach (string key in localizationEntries.Keys.ToList())
                {
                    if (localizationEntries[key] != null)
                    {
                        if (!localizationEntryFoldouts.ContainsKey(key))
                        {
                            localizationEntryFoldouts[key] = false;
                        }
                    }
                }

                List<string> orphanedKeys = new List<string>();
                List<string> orphanedKeyLabels = new List<string>();
                foreach (string key in localizationEntries.Keys)
                {
                    bool translationExists = localizationEntries[key].m_LocalizedDictionary.Any(x => x.Key != m_sourceLanguage && !string.IsNullOrEmpty(x.Value));
                    if (!Localizer.HasPrefix(key, Localizer.UNREGISTERED_KEY_PREFIX) && !translationExists)
                    {
                        orphanedKeys.Add(key);
                        string sourceString = localizationEntries[key].GetLocalizedString(m_sourceLanguage).Replace("/", "\\");
                        int maxLabelLength = 200;
                        sourceString = sourceString.Length > maxLabelLength ? sourceString.Substring(0, maxLabelLength) + "..." : sourceString;
                        orphanedKeyLabels.Add(sourceString + "/" + key);
                    }
                }
                orphanedKeys.Add("");
                orphanedKeyLabels.Add("");
                foreach (string key in localizationEntries.Keys)
                {
                    bool translationExists = localizationEntries[key].m_LocalizedDictionary.Any(x => x.Key != m_sourceLanguage && !string.IsNullOrEmpty(x.Value));
                    if (!Localizer.HasPrefix(key, Localizer.UNREGISTERED_KEY_PREFIX) && translationExists)
                    {
                        orphanedKeys.Add(key);
                        orphanedKeyLabels.Add(localizationEntries[key].GetLocalizedString(m_sourceLanguage) + "/" + key);
                    }
                }
                orphanedKeys.Insert(0, DELETE_KEY);
                orphanedKeyLabels.Insert(0, DELETE_KEY);

                if (GUILayout.Button("Auto-Map All Entries With Unregistered Keys"))
                {
                    foreach (string localizationEntryKey in localizationEntries.Keys.ToList())
                    {
                        if (Localizer.HasPrefix(localizationEntryKey, Localizer.UNREGISTERED_KEY_PREFIX))
                        {
                            foreach (string orphanedKey in localizationEntries.Keys.Where(x => Localizer.HasPrefix(x, Localizer.UNREGISTERED_KEY_PREFIX)).ToArray())
                            {
                                if (localizationEntries.ContainsKey(localizationEntryKey) && localizationEntries.ContainsKey(orphanedKey))
                                {
                                    if (LocalizationEntry.StringsAreEqual(m_sourceLanguage, localizationEntries[localizationEntryKey], localizationEntries[orphanedKey]))
                                    {
                                        localizationEntries[orphanedKey].m_LocalizedDictionary = localizationEntries[localizationEntryKey].m_LocalizedDictionary;
                                        localizationEntries.Remove(localizationEntryKey);
                                    }
                                }
                            }
                        }
                    }
                    m_cachedLocalizationInfos[localizationBuilder].m_HasUnsavedChanges = true;
                    GUIUtility.ExitGUI();
                }

                string[] keys = localizationEntries.Keys.Where(x => Localizer.HasPrefix(x, Localizer.UNREGISTERED_KEY_PREFIX)).ToArray();

                int startingEntryIndex = info.m_UnregisteredEntryPageIndex * NUM_ENTRIES_PER_PAGE;
                int i = startingEntryIndex;

                info.m_UnregisteredEntryPageIndex = PageToolbar(info.m_UnregisteredEntryPageIndex, (int)Math.Ceiling((float)keys.Length / NUM_ENTRIES_PER_PAGE));

                string maxEntryNumberString = keys.Length.ToString();
                int maxEntryNumberStringLength = maxEntryNumberString.Length;
                float maxEntryNumberWidth = NumberStyle.CalcSize(new GUIContent(maxEntryNumberString)).x;

                while (i < keys.Length && i < (startingEntryIndex + NUM_ENTRIES_PER_PAGE))
                {
                    string key = keys[i];
                    LocalizationEntry localizationEntry = localizationEntries[key];
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField((i + 1).ToString("D" + maxEntryNumberStringLength), NumberStyle, GUILayout.Width(maxEntryNumberWidth), GUILayout.Height(40));
                        localizationEntryFoldouts[key] = UnregisteredLocalizationEntryFoldout(localizationEntryFoldouts[key], localizationBuilder, localizationEntries[key], key, localizationEntries, orphanedKeys.ToArray(), orphanedKeyLabels.ToArray());
                    }
                    i++;
                }
            }
        }

        public static string[] GetAllLocalizableKeysInCombinedKey(string combinedLocalizationEntryKey)
        {
            List<string> localizableKeys = new List<string>();
            string[] localizationEntryKeys = LocalizationEntry.GetSplitKeys(combinedLocalizationEntryKey);
            foreach (string localizationEntryKey in localizationEntryKeys)
            {
                localizableKeys.Add(localizationEntryKey.Split('+')[0]);
            }
            return localizableKeys.ToArray();
        }

        public static string[] GetAllLocalizationEntryKeysInCombinedKey(string combinedLocalizationEntryKey)
        {
            return LocalizationEntry.GetSplitKeys(combinedLocalizationEntryKey);
        }

        public bool RegisteredLocalizationEntryFoldout(bool foldout, LocalizationBuilder localizationBuilder, LocalizationEntry localizationEntry, string localizationEntryKey)
        {
            string description = localizationEntry.m_Description;
            string sourceString = localizationEntry.GetLocalizedString(m_sourceLanguage);
            if (sourceString == null)
            {
                sourceString = "";
            }

            string translation = localizationEntry.GetLocalizedString(m_activeLanguage);
            if (translation == null)
            {
                translation = "";
            }

            string foldoutLabelPrefix = description;
            string foldoutLabel = sourceString;

            CachedLocalizationInfo info = m_cachedLocalizationInfos[localizationBuilder];

            if (info.GetCachedNeedsTranslationUpdate(localizationEntryKey, m_activeLanguage))
            {
                GUI.backgroundColor = m_errorColor;
            }

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.white;

                if (localizationBuilder != null)
                {
                    Rect foldoutRect = EditorGUILayout.GetControlRect();
                    EditorGUIUtility.labelWidth = foldoutRect.width;
                    foldout = EditorGUI.Foldout(foldoutRect, foldout, foldoutLabelPrefix, true, FoldoutDescriptionStyle);
                    EditorGUIUtility.labelWidth = 0;
                    if (foldoutLabel != "")
                    {
                        EditorGUI.indentLevel++;
                        if (foldoutRect.width > 1)
                        {
                            m_selectableLabelWidth = EditorGUI.IndentedRect(foldoutRect).width;
                        }
                        EditorGUILayout.SelectableLabel(foldoutLabel, SelectableLabelStyle, GUILayout.Height(SelectableLabelStyle.CalcHeight(new GUIContent(foldoutLabel), m_selectableLabelWidth)));
                        EditorGUI.indentLevel--;
                    }
                }

                EditorGUI.indentLevel++;
                if (foldout)
                {
                    if (localizationBuilder != null)
                    {
                        EditorGUILayout.Separator();
                        if (m_activeLanguage == m_sourceLanguage)
                        {
                            EditorGUILayout.HelpBox("[" + localizationEntryKey + "]", MessageType.None);
                        }
                        else
                        {
                            EditorGUI.BeginChangeCheck();
                            translation = EditorGUILayout.TextArea(translation, GUILayout.MinHeight(50));
                            if (EditorGUI.EndChangeCheck())
                            {
                                localizationEntry.m_LocalizedDictionary[m_activeLanguage] = translation;
                                info.m_HasUnsavedChanges = true;
                            }
                            EditorGUILayout.HelpBox(localizationEntryKey, MessageType.None);
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                GUILayout.FlexibleSpace();
                                if (GUILayout.Button(new GUIContent("Duplicate source text", "Duplicate original untranslated text into this box"), new GUIStyle(EditorStyles.miniButton)))
                                {
                                    GUI.FocusControl("");
                                    translation = sourceString;
                                    localizationEntry.m_LocalizedDictionary[m_activeLanguage] = translation;
                                    Repaint();
                                }
                            }
                        }
                        EditorGUILayout.Separator();
                    }
                }
                EditorGUI.indentLevel--;
            }
            return foldout;
        }

        public bool UnregisteredLocalizationEntryFoldout(bool foldout, LocalizationBuilder localizationBuilder, LocalizationEntry localizationEntry, string unregisteredKey, Dictionary<string, LocalizationEntry> localizationEntries, string[] orphanedKeys, string[] orphanedKeyLabels)
        {
            string description = localizationEntry.m_Description;
            string sourceString = localizationEntry.GetLocalizedString(m_sourceLanguage);

            string foldoutLabelPrefix = description;
            string foldoutLabel = sourceString;

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.white;

                if (localizationBuilder != null)
                {
                    Rect foldoutRect = EditorGUILayout.GetControlRect();
                    EditorGUIUtility.labelWidth = foldoutRect.width;
                    foldout = EditorGUI.Foldout(foldoutRect, foldout, foldoutLabelPrefix, true, FoldoutDescriptionStyle);
                    EditorGUIUtility.labelWidth = 0;
                    if (foldoutLabel != "")
                    {
                        EditorGUI.indentLevel++;
                        EditorGUILayout.SelectableLabel(foldoutLabel, SelectableLabelStyle, GUILayout.Height(SelectableLabelStyle.CalcHeight(new GUIContent(foldoutLabel), GUILayoutUtility.GetLastRect().width)));
                        EditorGUI.indentLevel--;
                    }
                }
                EditorGUI.indentLevel++;
                if (foldout)
                {
                    if (localizationBuilder != null)
                    {
                        EditorGUILayout.Separator();
                        EditorGUILayout.HelpBox("KEY NOT FOUND IN APPLICATION: [" + unregisteredKey + "]", MessageType.Info);
                        int selectedKeyIndex = -1;
                        EditorGUI.BeginChangeCheck();
                        selectedKeyIndex = EditorGUILayout.Popup("Reassign Key", selectedKeyIndex, orphanedKeyLabels.ToArray());
                        if (EditorGUI.EndChangeCheck())
                        {
                            string selectedKey = "";
                            if (selectedKeyIndex != -1)
                            {
                                selectedKey = orphanedKeys[selectedKeyIndex];
                            }
                            if (selectedKey == DELETE_KEY)
                            {
                                localizationEntries.Remove(unregisteredKey);
                            }
                            else if (selectedKey != "")
                            {
                                foreach (string language in localizationEntries[selectedKey].m_LocalizedDictionary.Keys.ToList())
                                {
                                    if (language != m_sourceLanguage)
                                    {
                                        string translation = localizationEntries[unregisteredKey].GetLocalizedString(language);
                                        if (!string.IsNullOrEmpty(translation) && !LocalizationEntry.StringsAreEqual(m_sourceLanguage, localizationEntries[unregisteredKey], localizationEntries[selectedKey]))
                                        {
                                            if (!Localizer.HasPrefix(translation, Localizer.OUTDATED_ENTRY_PREFIX + "(" + localizationEntries[selectedKey].GetLocalizedString(m_sourceLanguage) + ")"))
                                            {
                                                translation = Localizer.AddPrefix(translation, translation);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(translation) && string.IsNullOrEmpty(localizationEntries[selectedKey].GetLocalizedString(language)))
                                        {
                                            localizationEntries[selectedKey].m_LocalizedDictionary[language] = translation;
                                        }
                                    }
                                }
                                localizationEntries.Remove(unregisteredKey);
                            }
                            m_cachedLocalizationInfos[localizationBuilder].m_HasUnsavedChanges = true;
                        }
                    }
                }
                EditorGUI.indentLevel--;
            }
            return foldout;
        }
    }
}
#endif