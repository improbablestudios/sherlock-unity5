#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.Localization
{

    [CanEditMultipleObjects, CustomEditor(typeof(LocalizationBuilder), true)]
    public class LocalizationBuilderEditor : IdentifiableBehaviourEditor
    {
        public static readonly string k_fileLockedErrorMessage = "Cannot write to file while it is open. Please close the file before updating it";
        public static readonly string k_localizationFileNull = "Localization file does not exist";
        
        protected SerializedPropertyReorderableList m_localizedAssetGuidReorderableList;

        public static string LocalizationFileFolderEditorPreference
        {
            get
            {
                return EditorPrefs.GetString(LocalizationFileFolderEditorPreferenceKey, "Assets");
            }
            set
            {
                EditorPrefs.SetString(LocalizationFileFolderEditorPreferenceKey, value);
            }
        }

        protected static string LocalizationFileFolderEditorPreferenceKey
        {
            get
            {
                return typeof(LocalizationBuilder).AssemblyQualifiedName + "+LocalizationFileFolder";
            }
        }

        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            LocalizationBuilder t = target as LocalizationBuilder;

            if (property.name == "m_" + nameof(t.LocalizationFile))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    base.DrawProperty(property, label);

                    if (t.LocalizationFile == null)
                    {
                        GUIContent buttonLabel = new GUIContent("Create");
                        if (GUILayout.Button(buttonLabel, GUILayout.Width(GUI.skin.button.CalcSize(buttonLabel).x)))
                        {
                            ExportLocalizationFile(t);
                            GUIUtility.ExitGUI();
                        }
                    }
                    else
                    {
                        GUIContent updateButtonLabel = new GUIContent("Update");
                        if (GUILayout.Button(updateButtonLabel, GUILayout.Width(GUI.skin.button.CalcSize(updateButtonLabel).x)))
                        {
                            UpdateLocalizationFile(t);
                            GUIUtility.ExitGUI();
                        }
                        GUIContent validateButtonLabel = new GUIContent("Validate");
                        if (GUILayout.Button(validateButtonLabel, GUILayout.Width(GUI.skin.button.CalcSize(validateButtonLabel).x)))
                        {
                            ValidateLocalizationFile(t);
                            GUIUtility.ExitGUI();
                        }
                    }
                }
            }
            else if (property.name == "m_" + nameof(t.LocalizedAssetGuids))
            {
                if (t.DisplayLocalizedAssetGuidsAsObjectFields)
                {
                    if (m_localizedAssetGuidReorderableList == null)
                    {
                        m_localizedAssetGuidReorderableList = new SerializedPropertyReorderableList(ExtendedEditorGUI.AssetGuidField, (SerializedProperty p, GUIContent l) => EditorGUIUtility.singleLineHeight);
                    }
                    m_localizedAssetGuidReorderableList.Draw(property);
                }
                else
                {
                    base.DrawProperty(property, label);
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Add All Localizable Assets In Folder"))
                    {
                        t.LocalizedAssetGuids = t.LocalizedAssetGuids.Union(GetAllLocalizableAssetGuidsInFolder()).ToArray();
                        EditorUtility.SetDirty(t);
                    }

                    GUILayout.FlexibleSpace();
                }
            }
            else
            {
                base.DrawProperty(property, label, options);
            }
        }

        public virtual string[] GetAllLocalizableAssetGuidsInFolder()
        {
            List<string> localizedAssetGuids = new List<string>();

            string folder = EditorUtility.OpenFolderPanel("ILocalizable Folder", "", "");
            if (!string.IsNullOrEmpty(folder))
            {
                string[] paths = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories);
                foreach (string path in paths)
                {
                    string assetPath = path.Replace(Application.dataPath, "Assets");
                    string guid = AssetDatabase.AssetPathToGUID(assetPath);

                    ScriptableObject so = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);
                    if (so != null && so as ILocalizable != null)
                    {
                        if (!localizedAssetGuids.Contains(guid))
                        {
                            localizedAssetGuids.Add(guid);
                        }
                    }
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                    if (go != null && go.GetComponentsInChildren<ILocalizable>(true).Length > 0)
                    {
                        if (!localizedAssetGuids.Contains(guid))
                        {
                            localizedAssetGuids.Add(guid);
                        }
                    }
                }
            }
            return localizedAssetGuids.ToArray();
        }

        private static bool IsFileLocked(string path)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            FileInfo file = new FileInfo(path);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            return false;
        }

        private static void ReportLocalizationError(string errorMessage)
        {
            EditorUtility.DisplayDialog("Localization Builder Error", errorMessage, "OK");
        }

        public static void UpdateLocalizationFile(LocalizationBuilder localizationBuilder, ILocalizable localizable, bool storeAssetGuid)
        {
            if (localizationBuilder.LocalizationFile == null)
            {
                ReportLocalizationError(k_localizationFileNull);
                return;
            }

            string localizationFilePath = AssetDatabase.GetAssetPath(localizationBuilder.LocalizationFile);

            if (IsFileLocked(localizationFilePath))
            {
                ReportLocalizationError(k_fileLockedErrorMessage + ": " + localizationFilePath);
                return;
            }

            Dictionary<string, LocalizationEntry> localizationEntryDictionary = localizationBuilder.UpdateLocalizationEntry(localizable, storeAssetGuid);

            UpdateLocalizationFile(localizationBuilder, localizationFilePath, localizationEntryDictionary);
        }

        public static void UpdateLocalizationFile(LocalizationBuilder localizationBuilder)
        {
            UpdateLocalizationFile(localizationBuilder, AssetDatabase.GetAssetPath(localizationBuilder.LocalizationFile));
        }

        public static void UpdateLocalizationFile(LocalizationBuilder localizationBuilder, string localizationFilePath)
        {
            if (IsFileLocked(localizationFilePath))
            {
                ReportLocalizationError(k_fileLockedErrorMessage + ": " + localizationFilePath);
                return;
            }

            Dictionary<string, LocalizationEntry> localizationEntryDictionary = localizationBuilder.BuildLocalizationEntryDictionary();

            UpdateLocalizationFile(localizationBuilder, localizationFilePath, localizationEntryDictionary);
        }

        public static void UpdateLocalizationFile(LocalizationBuilder localizationBuilder, Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            UpdateLocalizationFile(localizationBuilder, AssetDatabase.GetAssetPath(localizationBuilder.LocalizationFile), localizationEntryDictionary);
        }

        public static void UpdateLocalizationFile(LocalizationBuilder localizationBuilder, string localizationFilePath, Dictionary<string, LocalizationEntry> localizationEntryDictionary)
        {
            if (localizationEntryDictionary != null)
            {
                string csvData = Localizer.ExportLocalizationEntryDictionaryToCsv(localizationEntryDictionary);
                try
                {
                    File.WriteAllText(localizationFilePath, csvData);
                    AssetDatabase.ImportAsset(localizationFilePath);

                    TextAsset textAsset = AssetDatabase.LoadAssetAtPath(localizationFilePath, typeof(TextAsset)) as TextAsset;
                    if (textAsset != null)
                    {
                        localizationBuilder.LocalizationFile = textAsset;
                    }

                    EditorUtility.DisplayDialog("Localization File Updated", "Exported " + LocalizationEntry.CountLocalizationEntriesInCsv(csvData) + " localization items.", "OK");
                }
                catch (IOException)
                {
                    ReportLocalizationError(k_fileLockedErrorMessage + ": " + localizationFilePath);
                }
            }
        }

        public virtual void ExportLocalizationFile(LocalizationBuilder localizationBuilder)
        {
            UpdateLocalizationFile(localizationBuilder, GetExportLocalizationFilePath(localizationBuilder.name));
        }

        public static string GetExportLocalizationFilePath(string name)
        {
            string path = EditorUtility.SaveFilePanelInProject("Export Localization File",
                                                               name + ".csv",
                                                               "csv",
                                                               "Please enter a filename to save the localization file to",
                                                               LocalizationFileFolderEditorPreference);
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }
            
            LocalizationFileFolderEditorPreference = Path.GetDirectoryName(path);

            return path;
        }

        public static void ValidateLocalizationFile(LocalizationBuilder localizationBuilder)
        {
            localizationBuilder.ValidateLocalizationEntryDictionary();
        }
    }

}

#endif