﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.Localization
{

    public class LocalizationManagerSettingsProvider : BaseAssetSettingsProvider<LocalizationManager>
    {
        public LocalizationManagerSettingsProvider() : base("Project/Managers/Localization Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new LocalizationManagerSettingsProvider();
        }
    }

    public class LocalizationManagerSettingsBuilder : BaseAssetSettingsBuilder<LocalizationManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif