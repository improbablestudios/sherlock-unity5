#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;
using UnityEngine.Video;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.Localization
{

    public class LocalizationMenuItems
    {
        [MenuItem("Tools/Localization/Localization Builder", false, 0)]
        [MenuItem("GameObject/Localization/Localization Builder", false, 0)]
        static void CreateLocalizationBuilder()
        {
            PrefabSpawner.SpawnPrefab<LocalizationBuilder>("~LocalizationBuilder", true);
        }

        [MenuItem("Tools/Localization/Localizable Text", false, 100)]
        [MenuItem("GameObject/Localization/Localizable Text", false, 100)]
        static void CreateLocalizableText()
        {
            PrefabSpawner.SpawnPrefab<LocalizableText>("~LocalizableText", true, typeof(Text));
        }

        [MenuItem("Tools/Localization/Localizable Sprite Renderer", false, 100)]
        [MenuItem("GameObject/Localization/Localizable Sprite Renderer", false, 100)]
        static void CreateLocalizableSpriteRenderer()
        {
            PrefabSpawner.SpawnPrefab<LocalizableSprite>("~LocalizableSpriteRenderer", true, typeof(SpriteRenderer));
        }
        
        [MenuItem("Tools/Localization/Localizable Audio Clip", false, 100)]
        [MenuItem("GameObject/Localization/Localizable Audio Clip", false, 100)]
        static void CreateLocalizableAudioClip()
        {
            PrefabSpawner.SpawnPrefab<LocalizableAudioClip>("~LocalizableAudioClip", true, typeof(AudioSource));
        }
        
        [MenuItem("Tools/Localization/Localizable Video", false, 100)]
        [MenuItem("GameObject/Localization/Localizable Video", false, 100)]
        static void CreateLocalizableVideo()
        {
            PrefabSpawner.SpawnPrefab<LocalizableVideoClip>("~LocalizableVideo", true, typeof(VideoPlayer));
        }
    }

}
#endif