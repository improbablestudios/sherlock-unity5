﻿using UnityEngine;

namespace ImprobableStudios.Localization
{
    
    public class LocalizableAudioClip : LocalizableBehaviour<AudioClip>
    {
        protected bool m_isPlaying;

        protected int m_timeSamples;

        protected AudioSource m_audioSource;

        public AudioSource AudioSource
        {
            get
            {
                if (m_audioSource == null)
                {
                    if (this != null)
                    {
                        m_audioSource = gameObject.GetComponent<AudioSource>();
                    }
                }
                return m_audioSource;
            }
        }

        public override string GetLocalizedMemberKey()
        {
            return "clip";
        }

        public override AudioClip GetValue()
        {
            if (AudioSource != null)
            {
                return AudioSource.clip;
            }

            return null;
        }
        
        public override void SetValue(AudioClip value)
        {
            if (AudioSource != null)
            {
                AudioSource.clip = value;
            }
        }

        //
        // ILocalizable implementation
        //

        public override void OnLocalizeComplete()
        {
            if (m_localizedDictionary != null && m_localizedDictionary.ContainsKey(LocalizationManager.Instance.ActiveLanguage))
            {
                AudioClip localizedAudioClip = m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage];

                if (AudioSource.clip != null)
                {
                    int previousTimeSamples = AudioSource.timeSamples;
                    bool wasPlaying = AudioSource.isPlaying;
                    AudioSource.clip = localizedAudioClip;
                    if (previousTimeSamples <= localizedAudioClip.samples)
                    {
                        // Set timeSamples before playing to prevent seek error
                        AudioSource.timeSamples = previousTimeSamples;
                    }
                    else
                    {
                        AudioSource.timeSamples = localizedAudioClip.samples;
                    }
                    if (wasPlaying)
                    {
                        AudioSource.Play();
                    }
                }
            }
        }
    }

}