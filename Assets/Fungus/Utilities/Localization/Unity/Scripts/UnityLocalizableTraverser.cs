﻿using ImprobableStudios.ReflectionUtilities;
using System;
using System.Reflection;

namespace ImprobableStudios.Localization
{

    public class UnityLocalizableTraverser : LocalizableTraverser
    {
        public override MemberInfo[] GetLocalizableMembers(Type objType)
        {
            return objType.GetUnitySerializedFields();
        }

        public override bool ShouldTraverseMembers(Type objType)
        {
            return base.ShouldTraverseMembers(objType) && objType.IsReflectableUnityType();
        }
    }

}