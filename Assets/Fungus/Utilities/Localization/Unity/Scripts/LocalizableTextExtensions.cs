﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.Localization
{

    public static class LocalizableTextExtensions
    {
        public static void RegisterLocalizableText(this GameObject gameObject, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = gameObject.GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = gameObject.AddComponent<LocalizableText>();
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
                localizableText.LocalizeSerializedValue = false;
            }
            localizableText.Register(localizedDictionary);
        }

        public static void RegisterLocalizableText(this TextMesh textMesh, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = textMesh.gameObject.GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = textMesh.gameObject.AddComponent<LocalizableText>();
                localizableText.LocalizeSerializedValue = false;
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
            }
            localizableText.Register(localizedDictionary);
        }

        public static void RegisterLocalizableText(this Text text, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = text.gameObject.GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = text.gameObject.AddComponent<LocalizableText>();
                localizableText.LocalizeSerializedValue = false;
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
            }
            localizableText.Register(localizedDictionary);
        }

        public static void RegisterLocalizableText(this InputField inputField, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = inputField.gameObject.GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = inputField.gameObject.AddComponent<LocalizableText>();
                localizableText.LocalizeSerializedValue = false;
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
            }
            localizableText.Register(localizedDictionary);
        }

        public static void RegisterLocalizableText(this ITextComponent textComponent, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = textComponent.GetComponent().GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = textComponent.GetComponent().gameObject.AddComponent<LocalizableText>();
                localizableText.LocalizeSerializedValue = false;
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
            }
            localizableText.Register(localizedDictionary);
        }

        public static void RegisterLocalizableText(this Component textComponent, Dictionary<string, string> localizedDictionary, bool append = false)
        {
            LocalizableText localizableText = textComponent.GetComponent<LocalizableText>();
            if (localizableText == null)
            {
                localizableText = textComponent.gameObject.AddComponent<LocalizableText>();
                localizableText.LocalizeSerializedValue = false;
            }
            if (append)
            {
                localizableText.LocalizedDictionary.AppendLocalizedDictionary(localizedDictionary);
            }
            localizableText.Register(localizedDictionary);
        }
    }

}