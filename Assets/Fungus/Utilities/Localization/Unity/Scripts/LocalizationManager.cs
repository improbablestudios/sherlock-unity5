﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Linq;
using System.Collections.Generic;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace ImprobableStudios.Localization
{

    [ExecuteInEditMode]
    public class LocalizationManager : SettingsScriptableObject<LocalizationManager>
    {
        [Tooltip("Currently active language")]
        [SerializeField]
        [FormerlySerializedAs("activeLanguage")]
        [LanguageField]
        protected string m_ActiveLanguage = Localizer.DEFAULT_LANGUAGE;

        [Tooltip("Source language")]
        [SerializeField]
        [FormerlySerializedAs("sourceLanguage")]
        [SelectNewLanguageField]
        protected string m_SourceLanguage = Localizer.DEFAULT_LANGUAGE;

        [Tooltip("Languages to translate to (Use official IETF language tag format when possible <Language Code>-<Region Code> (e.g. \"en-US\" for English as spoken in the United States) https://msdn.microsoft.com/en-us/library/cc233982.aspx)")]
        [SerializeField]
        [FormerlySerializedAs("languages")]
        [FormerlySerializedAs("m_Languages")]
        [SelectNewLanguageField]
        protected string[] m_TranslationLanguages = new string[] { };

        protected string[] m_languages;

        protected LocalizableTraverser m_localizableTraverser = new UnityLocalizableTraverser();

        protected LocalizationSerializer m_localizationSerializer = new UnityLocalizationSerializer();

        public string ActiveLanguage
        {
            get
            {
                return m_ActiveLanguage;
            }
            set
            {
                SetActiveLanguage(value);
            }
        }

        public string SourceLanguage
        {
            get
            {
                return m_SourceLanguage;
            }
            set
            {
                m_SourceLanguage = value;
                UpdateLanguages();
                Localizer.SourceLanguage = value;
            }
        }

        public string[] TranslationLanguages
        {
            get
            {
                return m_TranslationLanguages;
            }
            set
            {
                m_TranslationLanguages = value;
                UpdateLanguages();
                Localizer.TranslationLanguages = m_TranslationLanguages;
            }
        }

        public string[] Languages
        {
            get
            {
                if (m_languages == null)
                {
                    UpdateLanguages();
                }
                return m_languages;
            }
        }

        public LocalizableTraverser LocalizableTraverser
        {
            get
            {
                return m_localizableTraverser;
            }
            set
            {
                m_localizableTraverser = value;
                Localizer.LocalizableTraverser = value;
            }
        }

        public LocalizationSerializer LocalizationSerializer
        {
            get
            {
                return m_localizationSerializer;
            }
            set
            {
                m_localizationSerializer = value;
                Localizer.LocalizationSerializer = value;
            }
        }
        
        protected override void OnValidate()
        {
            base.OnValidate();

            UpdateLanguages();
            Localizer.TranslationLanguages = m_TranslationLanguages.Where(x => x != m_SourceLanguage && !string.IsNullOrEmpty(x)).ToArray();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Localizer.SourceLanguage = m_SourceLanguage;

            if (Application.isPlaying && m_ActiveLanguage != m_SourceLanguage)
            {
                SetActiveLanguage(m_ActiveLanguage);
            }
        }
        
        public void RegisterAllActiveLocalizables()
        {
            foreach (ILocalizable localizable in ResourceTracker.GetCachedInstanceObjectsOfType<ILocalizable>())
            {
                if ((localizable as UnityEngine.Object) != null)
                {
                    RegisterLocalizable(localizable);
                }
            }
            foreach (ILocalizable localizable in ResourceTracker.GetCachedAssetObjectsOfType<ILocalizable>())
            {
                if ((localizable as UnityEngine.Object) != null)
                {
                    RegisterLocalizable(localizable);
                }
            }
        }

        private void UpdateLanguages()
        {
            List<string> languages = new List<string>();
            languages.Add(m_SourceLanguage);
            languages.AddRange(m_TranslationLanguages);
            m_languages = languages.ToArray();
        }

        public void RegisterLocalizable(ILocalizable localizable)
        {
            Localizer.RegisterLocalizable(localizable, Application.isPlaying && m_ActiveLanguage != m_SourceLanguage);
        }

        public void UnregisterLocalizable(ILocalizable localizable)
        {
            Localizer.RemoveLocalizable(localizable.GetKey());
        }

        public void RegisterAllActiveLocalizationBuilders()
        {
            foreach (LocalizationBuilder localizationBuilder in FindObjectsOfType<LocalizationBuilder>())
            {
                RegisterLocalizationBuilder(localizationBuilder);
            }
        }

        public void RegisterLocalizationBuilder(LocalizationBuilder localizationBuilder)
        {
            Localizer.RegisterLocalizationEntryDictionary(localizationBuilder.LocalizationEntryDictionary);
        }

        protected virtual void CleanupMissingLocalizables()
        {
            foreach (KeyValuePair<string, ILocalizable> kvp in Localizer.LocalizableInstanceDictionary.Where(x => x.Value as UnityEngine.Object == null).ToList())
            {
                Localizer.RemoveLocalizable(kvp.Key);
            }
        }

        public virtual void SetupLocalizer()
        {
            Localizer.LocalizationSerializer = LocalizationSerializer;
            Localizer.LocalizableTraverser = LocalizableTraverser;
            Localizer.SourceLanguage = SourceLanguage;
            Localizer.TranslationLanguages = TranslationLanguages;
        }

        protected virtual void SetActiveLanguage(string activeLanguage)
        {
            if (Languages == null || Languages.Length == 0 || !Languages.Contains(activeLanguage))
            {
                Debug.LogError("Language (" + activeLanguage + ") does not exist in Languages", this);
                return;
            }

            m_ActiveLanguage = activeLanguage;

            if (Application.isPlaying)
            {
                CleanupMissingLocalizables();
                RegisterAllActiveLocalizables();
                RegisterAllActiveLocalizationBuilders();
            }

            SetupLocalizer();

            if (Application.isPlaying)
            {
                Localizer.SetLanguage(activeLanguage);
            }
        }
    }

}
