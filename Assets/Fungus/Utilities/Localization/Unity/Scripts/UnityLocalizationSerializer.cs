﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.Localization
{

    public class UnityLocalizationSerializer : LocalizationSerializer
    {
        public UnityLocalizationSerializer() : base()
        {
            AddDefaultConverters();
        }

        public UnityLocalizationSerializer(LocalizationConverter[] localizationConverters) : base(localizationConverters)
        {
        }

        protected override void AddDefaultConverters()
        {
            base.AddDefaultConverters();
            
            AddConverter(new UnityObjectLocalizationConverter());
            AssignDefaultConverter(typeof(UnityEngine.Object), typeof(UnityObjectLocalizationConverter));

            AddConverter(new SpriteLocalizationConverter());
            AssignDefaultConverter(typeof(Sprite), typeof(SpriteLocalizationConverter));
        }
    }

    public abstract class AssetLocalizationConverter : LocalizationConverter<UnityEngine.Object>
    {
        public override string GetAutoTranslatedString(string sourceString, string sourceLanguage, string targetLanguage)
        {
            if (sourceString != null)
            {
                sourceString = sourceString.Replace(sourceLanguage, targetLanguage);
                sourceString = sourceString.Replace(sourceLanguage.ToLower(), targetLanguage.ToLower());
                sourceString = sourceString.Replace(sourceLanguage.ToUpper(), targetLanguage.ToUpper());
                return sourceString;
            }
            return null;
        }
    }

    public class SpriteLocalizationConverter : ResourceLocalizationConverter<Sprite>
    {
    }

    public class UnityObjectLocalizationConverter : ResourceLocalizationConverter<UnityEngine.Object>
    {
    }

    public class ResourceLocalizationConverter<T> : AssetLocalizationConverter where T : UnityEngine.Object
    {
        protected override string ConvertObjectValueToString(UnityEngine.Object obj)
        {
            if (obj == null)
            {
                return null;
            }

            string assetPath = obj.name;
#if UNITY_EDITOR
            string resourcesFolderName = "Resources";
            assetPath = AssetDatabase.GetAssetPath(obj);
            if (!string.IsNullOrEmpty(assetPath))
            {
                assetPath = System.IO.Path.GetDirectoryName(assetPath) + "\\" + System.IO.Path.GetFileNameWithoutExtension(assetPath);
                int resourcsePathIndex = assetPath.LastIndexOf(resourcesFolderName);
                if (resourcsePathIndex >= 0)
                {
                    assetPath = assetPath.Substring(resourcsePathIndex + resourcesFolderName.Length + 1);
                }
            }
#endif
            return assetPath;
        }

        protected override UnityEngine.Object ConvertStringToObjectValue(string path)
        {
            return Resources.Load<T>(path);
        }

        public override bool StringIsValid(string str)
        {
            if (Resources.Load<T>(str) == null)
            {
                return false;
            }

            return true;
        }
    }

}