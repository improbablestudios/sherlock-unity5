﻿using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.Localization
{

    public static class LocalizableAudioClipExtensions
    {
        public static void RegisterLocalizableAudioClip(this GameObject gameObject, Dictionary<string, AudioClip> localizedDictionary)
        {
            LocalizableAudioClip localizableAudio = gameObject.GetComponent<LocalizableAudioClip>();
            if (localizableAudio == null)
            {
                localizableAudio = gameObject.AddComponent<LocalizableAudioClip>();
                localizableAudio.LocalizeSerializedValue = false;
            }
            localizableAudio.Register(localizedDictionary);
        }

        public static void RegisterLocalizableAudioClip(this AudioSource audioSource, Dictionary<string, AudioClip> localizedDictionary)
        {
            LocalizableAudioClip localizableAudio = audioSource.GetComponent<LocalizableAudioClip>();
            if (localizableAudio == null)
            {
                localizableAudio = audioSource.gameObject.AddComponent<LocalizableAudioClip>();
                localizableAudio.LocalizeSerializedValue = false;
            }
            localizableAudio.Register(localizedDictionary);
        }
    }

}