﻿using UnityEngine;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.Localization
{

    [DisallowMultipleComponent]
    public class LocalizableSprite : LocalizableBehaviour<Sprite>
    {
        public override string GetLocalizedMemberKey()
        {
            return "sprite";
        }

        public override Sprite GetValue()
        {
            return gameObject.GetSprite();
        }

        public override void SetValue(Sprite value)
        {
            gameObject.SetSprite(value);
        }
    }

}