﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.Localization
{

    public static class LocalizableSpriteExtensions
    {
        public static void RegisterLocalizableSprite(this GameObject gameObject, Dictionary<string, Sprite> localizedDictionary)
        {
            LocalizableSprite localizableSprite = gameObject.GetComponent<LocalizableSprite>();
            if (localizableSprite == null)
            {
                localizableSprite = gameObject.AddComponent<LocalizableSprite>();
                localizableSprite.LocalizeSerializedValue = false;
            }
            localizableSprite.Register(localizedDictionary);
        }

        public static void RegisterLocalizableSprite(this SpriteRenderer spriteRenderer, Dictionary<string, Sprite> localizedDictionary)
        {
            LocalizableSprite localizableSprite = spriteRenderer.GetComponent<LocalizableSprite>();
            if (localizableSprite == null)
            {
                localizableSprite = spriteRenderer.gameObject.AddComponent<LocalizableSprite>();
                localizableSprite.LocalizeSerializedValue = false;
            }
            localizableSprite.Register(localizedDictionary);
        }

        public static void RegisterLocalizableSprite(this Image image, Dictionary<string, Sprite> localizedDictionary)
        {
            LocalizableSprite localizableSprite = image.GetComponent<LocalizableSprite>();
            if (localizableSprite == null)
            {
                localizableSprite = image.gameObject.AddComponent<LocalizableSprite>();
                localizableSprite.LocalizeSerializedValue = false;
            }
            localizableSprite.Register(localizedDictionary);
        }

        public static void RegisterLocalizableSprite(this ISpriteComponent spriteComponent, Dictionary<string, Sprite> localizedDictionary)
        {
            LocalizableSprite localizableSprite = spriteComponent.GetComponent().GetComponent<LocalizableSprite>();
            if (localizableSprite == null)
            {
                localizableSprite = spriteComponent.GetComponent().gameObject.AddComponent<LocalizableSprite>();
                localizableSprite.LocalizeSerializedValue = false;
            }
            localizableSprite.Register(localizedDictionary);
        }

        public static void RegisterLocalizableSprite(this Component spriteComponent, Dictionary<string, Sprite> localizedDictionary)
        {
            LocalizableSprite localizableSprite = spriteComponent.GetComponent<LocalizableSprite>();
            if (localizableSprite == null)
            {
                localizableSprite = spriteComponent.gameObject.AddComponent<LocalizableSprite>();
                localizableSprite.LocalizeSerializedValue = false;
            }
            localizableSprite.Register(localizedDictionary);
        }
    }

}
