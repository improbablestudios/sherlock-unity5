﻿using System;
using UnityEngine;

namespace ImprobableStudios.Localization
{

#if UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
#endif
    public static class UnityLocalizationLogger
    {
        static UnityLocalizationLogger()
        {
            Localizer.OnWarning += Debug.LogWarning;
        }
    }

}