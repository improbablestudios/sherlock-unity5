﻿using UnityEngine;

namespace ImprobableStudios.Localization
{
    
    public class LanguageFieldAttribute : PropertyAttribute
    {
        public LanguageFieldAttribute()
        {
        }
    }

    public class SelectNewLanguageFieldAttribute : PropertyAttribute
    {
        public SelectNewLanguageFieldAttribute()
        {
        }
    }

}