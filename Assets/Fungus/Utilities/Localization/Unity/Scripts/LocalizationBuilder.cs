﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.Localization
{
    /// <summary>
    /// A component that builds a dictionary of localization entries from ILocalizable UnityEngine.Objects.
    /// This dictionary can be exported to a Localization File csv.
    /// </summary>
    [ExecuteInEditMode]
    public class LocalizationBuilder : IdentifiableBehaviour
    {
        [Tooltip("CSV file containing localization data which can be easily edited in a spreadsheet tool")]
        [SerializeField]
        protected TextAsset m_LocalizationFile;

        [Tooltip("Remove all entries with empty source text")]
        [SerializeField]
        protected bool m_RemoveEmptyEntries = true;

        [Tooltip("Combine all entires with identical source and description text into one line in the CSV")]
        [SerializeField]
        protected bool m_CombineIdenticalEntries = true;

#if UNITY_EDITOR
        [Tooltip("Display Localized Asset Guids as object fields so that it is possible to use the standard unity object picker")]
        [SerializeField]
        protected bool m_DisplayLocalizedAssetGuidsAsObjectFields = true;

        [Tooltip("Assets whose components and child components will be translated")]
        [SerializeField]
        protected string[] m_LocalizedAssetGuids = new string[0];
#endif

        private Dictionary<string, LocalizationEntry> m_localizationEntryDictionary;

        public TextAsset LocalizationFile
        {
            get
            {
                return m_LocalizationFile;
            }
            set
            {
                m_LocalizationFile = value;
            }
        }

        public bool RemoveEmptyEntries
        {
            get
            {
                return m_RemoveEmptyEntries;
            }
            set
            {
                m_RemoveEmptyEntries = value;
            }
        }

        public bool CombineIdenticalEntries
        {
            get
            {
                return m_CombineIdenticalEntries;
            }
            set
            {
                m_CombineIdenticalEntries = value;
            }
        }

#if UNITY_EDITOR
        public bool DisplayLocalizedAssetGuidsAsObjectFields
        {
            get
            {
                return m_DisplayLocalizedAssetGuidsAsObjectFields;
            }
            set
            {
                m_DisplayLocalizedAssetGuidsAsObjectFields = value;
            }
        }

        public string[] LocalizedAssetGuids
        {
            get
            {
                return m_LocalizedAssetGuids;
            }
            set
            {
                m_LocalizedAssetGuids = value;
            }
        }
#endif

        public Dictionary<string, LocalizationEntry> LocalizationEntryDictionary
        {
            get
            {
                if (m_localizationEntryDictionary == null)
                {
                    m_localizationEntryDictionary = GetLocalizationEntryDictionaryFromLocalizationFile();
                }
                return m_localizationEntryDictionary;
            }
        }

        protected override void OnValidate()
        {
            if (m_LocalizationFile != null)
            {
                LocalizationManager.Instance.RegisterLocalizationBuilder(this);
            }
        }

        protected override void Awake()
        {
            base.Awake();

            if (m_LocalizationFile != null)
            {
                LocalizationManager.Instance.RegisterLocalizationBuilder(this);
            }
        }

        public virtual Dictionary<string, LocalizationEntry> GetLocalizationEntryDictionaryFromLocalizationFile()
        {
            LocalizationManager.Instance.SetupLocalizer();

            try
            {
                if (m_LocalizationFile != null)
                {
                    Dictionary<string, LocalizationEntry> localizationEntryDictionary = new Dictionary<string, LocalizationEntry>();
                    Localizer.PopulateLocalizationEntryDictionaryWithCsvData(localizationEntryDictionary, m_LocalizationFile.text, false);
                    return localizationEntryDictionary;
                }
                else
                {
                    Debug.LogError("Localization Builder Error: No localization file specified");
                }
            }
            catch (LocalizationEntryError e)
            {
                Debug.LogWarning(e.Message);
            }

            return null;
        }

        public bool ValidateLocalizationEntryDictionary()
        {
            m_localizationEntryDictionary = GetLocalizationEntryDictionaryFromLocalizationFile();
            if (m_localizationEntryDictionary != null)
            {
                bool valid = true;

                string progressTitle = string.Format("Validating Localization Entry Dictionary '{0}'", name);

                int i = 0;

                foreach (KeyValuePair<string, LocalizationEntry> kvp in m_localizationEntryDictionary)
                {
#if UNITY_EDITOR
                    if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, string.Format("Validating '{0}'", kvp.Key), ((float)i / (float)LocalizationEntryDictionary.Count)))
                    {
                        return false;
                    }
#endif
                    try
                    {
                        string localizationEntryKey = kvp.Key;
                        LocalizationEntry localizationEntry = kvp.Value;
                        if (!Localizer.ValidateLocalizationEntry(localizationEntryKey, localizationEntry))
                        {
                            valid = false;
                        }
                    }
                    catch
                    {
                        valid = false;
                    }

                    i++;
                }

#if UNITY_EDITOR
                UnityEditor.EditorUtility.ClearProgressBar();
#endif
                return valid;
            }

            return false;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Updates a dictionary of localizable text items.
        /// </summary>
        /// <param name="localizable">The localizable to update in the Localization Entry Dictionary</param>
        /// <param name="addAssetGuid">Add the asset guid of this localizable to the m_LocalizedAssetGuids list</param>
        /// <returns>The new Localization Entry Dictionary</returns>
        public Dictionary<string, LocalizationEntry> UpdateLocalizationEntry(ILocalizable localizable, bool addAssetGuid)
        {
            LocalizationManager.Instance.SetupLocalizer();

            Dictionary<string, LocalizationEntry> localizationEntryDictionary = new Dictionary<string, LocalizationEntry>();

            if (addAssetGuid)
            {
                string assetPath = UnityEditor.AssetDatabase.GetAssetPath(localizable as UnityEngine.Object);
                if (assetPath != null)
                {
                    string guid = UnityEditor.AssetDatabase.AssetPathToGUID(assetPath);
                    if (!m_LocalizedAssetGuids.Contains(guid))
                    {
                        List<string> newLocalizedAssetGuidList = m_LocalizedAssetGuids.ToList();
                        newLocalizedAssetGuidList.Add(guid);
                        m_LocalizedAssetGuids = newLocalizedAssetGuidList.ToArray();
                    }
                }
            }

            if (m_LocalizationFile != null)
            {
                localizationEntryDictionary = GetLocalizationEntryDictionaryFromLocalizationFile();

                if (localizationEntryDictionary != null)
                {
                    Localizer.PopulateLocalizationEntryDictionaryWithApplicationData(localizationEntryDictionary, localizable);

                    string currentCsvData = m_LocalizationFile.text;
                    if (!string.IsNullOrEmpty(currentCsvData))
                    {
                        Localizer.PopulateLocalizationEntryDictionaryWithCsvData(localizationEntryDictionary, currentCsvData, true);
                    }

                    if (m_RemoveEmptyEntries)
                    {
                        Localizer.RemoveEmptyEntriesFromLocalizationEntryDictionary(localizationEntryDictionary);
                    }

                    if (m_CombineIdenticalEntries)
                    {
                        localizationEntryDictionary = Localizer.GetCombinedLocalizationEntryDictionary(localizationEntryDictionary);
                    }
                }
            }

            return localizationEntryDictionary;
        }

        /// <summary>
        /// Builds a dictionary of localizable text items.
        /// </summary>
        /// <returns>The Localization Entry Dictionary</returns>
        public Dictionary<string, LocalizationEntry> BuildLocalizationEntryDictionary()
        {
            LocalizationManager.Instance.SetupLocalizer();

            Dictionary<string, LocalizationEntry> localizationEntryDictionary = new Dictionary<string, LocalizationEntry>();
            Dictionary<string, string> localizableOrderDictionary = new Dictionary<string, string>();

            try
            {
                ILocalizable[] localizableChildren = gameObject.GetComponentsInChildren<ILocalizable>(true);

                string progressTitle = string.Format("Building Localization Entry Dictionary '{0}'", name);

                int step = 0;

                int totalSteps = localizableChildren.Length + m_LocalizedAssetGuids.Length + 4;

                for (int i = 0; i < localizableChildren.Length; i++)
                {
                    ILocalizable localizable = localizableChildren[i];

                    if (localizable as UnityEngine.Object != null)
                    {
                        if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, string.Format("Scanning Child '{0}'", (localizable as UnityEngine.Object).name), ((float)step / (float)totalSteps)))
                        {
                            return null;
                        }

                        localizableOrderDictionary[localizable.GetKey()] = localizable.GetLocalizableOrderedPriority();
                        Localizer.PopulateLocalizationEntryDictionaryWithApplicationData(localizationEntryDictionary, localizable);
                    }

                    step++;
                }

                for (int i = 0; i < m_LocalizedAssetGuids.Length; i++)
                {
                    string guid = m_LocalizedAssetGuids[i];
                    string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);

                    if (!string.IsNullOrEmpty(assetPath))
                    {
                        if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, string.Format("Scanning Asset '{0}'", Path.GetFileName(assetPath)), ((float)step / (float)totalSteps)))
                        {
                            return null;
                        }

                        UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
                        GameObject go = obj as GameObject;
                        if (obj as ILocalizable != null)
                        {
                            ILocalizable localizable = obj as ILocalizable;
                            localizableOrderDictionary[localizable.GetKey()] = localizable.GetLocalizableOrderedPriority();
                            Localizer.PopulateLocalizationEntryDictionaryWithApplicationData(localizationEntryDictionary, localizable);
                        }
                        else if (go != null)
                        {
                            foreach (ILocalizable localizable in go.GetComponentsInChildren<ILocalizable>(true))
                            {
                                if ((localizable as UnityEngine.Object) != null)
                                {
                                    localizableOrderDictionary[localizable.GetKey()] = localizable.GetLocalizableOrderedPriority();
                                    Localizer.PopulateLocalizationEntryDictionaryWithApplicationData(localizationEntryDictionary, localizable);
                                }
                            }
                        }
                        UnityEditor.EditorUtility.UnloadUnusedAssetsImmediate();
                        System.GC.Collect();
                    }

                    step++;
                }
                
                if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, "Processing CSV data", ((float)step / (float)totalSteps)))
                {
                    return null;
                }
                step++;

                if (m_LocalizationFile != null)
                {
                    string currentCsvData = m_LocalizationFile.text;

                    if (!string.IsNullOrEmpty(currentCsvData))
                    {
                        Localizer.PopulateLocalizationEntryDictionaryWithCsvData(localizationEntryDictionary, currentCsvData, true);
                    }
                }
                
                if (m_RemoveEmptyEntries)
                {
                    if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, "Removing empty entries", ((float)step / (float)totalSteps)))
                    {
                        return null;
                    }

                    Localizer.RemoveEmptyEntriesFromLocalizationEntryDictionary(localizationEntryDictionary);
                }
                step++;

                if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, "Sorting", ((float)step / (float)totalSteps)))
                {
                    return null;
                }
                step++;

                localizationEntryDictionary = Localizer.GetOrderedLocalizationEntryDictionary(localizationEntryDictionary, localizableOrderDictionary);

                if (m_CombineIdenticalEntries)
                {
                    if (UnityEditor.EditorUtility.DisplayCancelableProgressBar(progressTitle, "Combining identical entries", ((float)step / (float)totalSteps)))
                    {
                        return null;
                    }

                    localizationEntryDictionary = Localizer.GetCombinedLocalizationEntryDictionary(localizationEntryDictionary);
                }
                step++;
            }
            finally
            {
                UnityEditor.EditorUtility.ClearProgressBar();
            }

            UnityEditor.EditorUtility.ClearProgressBar();

            return localizationEntryDictionary;
        }
#endif
    }
}
