﻿using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.Localization
{
    
    public class LocalizableText : LocalizableBehaviour<string>
    {
        public override string GetLocalizedMemberKey()
        {
            return "text";
        }

        public override string GetValue()
        {
            return gameObject.GetText();
        }

        public override void SetValue(string value)
        {
            gameObject.SetText(value);
        }
    }

}
