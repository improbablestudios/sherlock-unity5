﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ImprobableStudios.BaseUtilities;
using UnityEngine.Serialization;

namespace ImprobableStudios.Localization
{

    [DisallowMultipleComponent]
    public abstract class LocalizableBehaviour<T> : LocalizableBehaviour, IExplicitLocalizable
    {
        public Dictionary<string, T> m_localizedDictionary;
        
        public Dictionary<string, T> LocalizedDictionary
        {
            get
            {
                return m_localizedDictionary;
            }
        }

        public abstract T GetValue();

        public abstract void SetValue(T value);

        public void Register(ILocalizable localizable, string key)
        {
            Register(localizable.SafeGetLocalizedDictionary<T>(key));
        }

        public void Register(Dictionary<string, T> localizedDictionary)
        {
            m_localizedDictionary = localizedDictionary;
        }

        public void RegisterAndSet(ILocalizable localizable, string key, T value)
        {
            Register(localizable, key);

            SetValue(value);
        }

        //
        // ILocalizable implementation
        //

        public override void OnLocalizeComplete()
        {
            if (m_localizedDictionary != null && m_localizedDictionary.ContainsKey(LocalizationManager.Instance.ActiveLanguage))
            {
                T localizedObj = m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage];
                SetValue(localizedObj);
            }
        }

        public override void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary)
        {
            string key = GetLocalizedMemberKey();
            if (!string.IsNullOrEmpty(key))
            {
                if (m_LocalizeSerializedValue)
                {
                    dictionary[key] = GetValue();
                }
            }
        }

        public override void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary)
        {
            string key = GetLocalizedMemberKey();
            if (!string.IsNullOrEmpty(key))
            {
                if (m_LocalizeSerializedValue)
                {
                    if (m_localizedDictionary == null)
                    {
                        if (dictionary.ContainsKey(key))
                        {
                            SetValue((T)dictionary[key]);
                        }
                    }
                }
            }
        }
    }


    [DisallowMultipleComponent]
    public abstract class LocalizableBehaviour : IdentifiableBehaviour, IExplicitLocalizable
    {
        [Tooltip("If true, the serialized value in the attached component will be localized, otherwise, it is treated as simply a placeholder and is not localized")]
        [SerializeField]
        [FormerlySerializedAs("m_LocalizeCurrentValue")]
        protected bool m_LocalizeSerializedValue = true;

        [Tooltip("An optional description that will appear in the Localization File csv")]
        [SerializeField]
        protected string m_Description;
        
        public bool LocalizeSerializedValue
        {
            get
            {
                return m_LocalizeSerializedValue;
            }
            set
            {
                m_LocalizeSerializedValue = value;
            }
        }

        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }
        
        public abstract string GetLocalizedMemberKey();

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return null;
        }

        public abstract void OnLocalizeComplete();

        public abstract void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary);

        public virtual void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary)
        {
            string key = GetLocalizedMemberKey();
            if (!string.IsNullOrEmpty(key))
            {
                if (m_LocalizeSerializedValue)
                {
                    dictionary[key] = m_Description;
                }
            }
        }

        public virtual void ExplicitlyGetConverters(Dictionary<string, Type> dictionary)
        {
        }

        public abstract void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary);
    }

}
