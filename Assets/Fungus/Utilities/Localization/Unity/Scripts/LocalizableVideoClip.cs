﻿using UnityEngine;
using UnityEngine.Video;

namespace ImprobableStudios.Localization
{

    public enum VideoLocalizationMethod
    {
        SwitchAudioTracks,
        SwitchVideoClip,
        SwitchUrl,
    }
    
    public class LocalizableVideoClip : LocalizableBehaviour<object>
    {
        public const string VIDEOCLIP_LOCALIZED_MEMBER_KEY = "clip";

        public const string URL_LOCALIZED_MEMBER_KEY = "url";

        [Tooltip("Determines whether the video clip will be localized by switching the whole clip with another one or just by switching the audio track")]
        [SerializeField]
        protected VideoLocalizationMethod m_VideoLocalizationMethod = VideoLocalizationMethod.SwitchAudioTracks;
        
        protected bool m_isPlaying;
        
        protected double m_time;

        protected VideoPlayer m_videoPlayer;

        public VideoPlayer VideoPlayer
        {
            get
            {
                if (m_videoPlayer == null)
                {
                    if (this != null)
                    {
                        m_videoPlayer = gameObject.GetComponent<VideoPlayer>();
                    }
                }
                return m_videoPlayer;
            }
        }

        public VideoLocalizationMethod VideoLocalizationMethod
        {
            get
            {
                return m_VideoLocalizationMethod;
            }
            set
            {
                m_VideoLocalizationMethod = value;
            }
        }
        
        public override string GetLocalizedMemberKey()
        {
            if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchVideoClip)
            {
                return VIDEOCLIP_LOCALIZED_MEMBER_KEY;
            }
            else if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchUrl)
            {
                return URL_LOCALIZED_MEMBER_KEY;
            }

            return null;
        }

        public override object GetValue()
        {
            if (VideoPlayer != null)
            {
                if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchVideoClip)
                {
                    return VideoPlayer.clip;
                }
                else if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchUrl)
                {
                    return VideoPlayer.url;
                }

            }

            return null;
        }

        public override void SetValue(object value)
        {
            if (VideoPlayer != null)
            {
                if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchVideoClip)
                {
                    VideoPlayer.clip = (VideoClip)value;
                }
                else if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchUrl)
                {
                    VideoPlayer.url = (string)value;
                }

            }
        }
        
        public void SwitchAudioTracksToActiveLanguage()
        {
            if (LocalizationManager.Instance.Languages.Length == VideoPlayer.controlledAudioTrackCount)
            {
                for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                {
                    int languageIndex = (VideoPlayer.controlledAudioTrackCount - 1) - i;
                    string language = LocalizationManager.Instance.Languages[languageIndex];
                    bool isActiveLanguage = language == LocalizationManager.Instance.ActiveLanguage;
                    if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.Direct)
                    {
                        VideoPlayer.SetDirectAudioMute(i, !isActiveLanguage);
                    }
                    else if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
                    {
                        AudioSource targetAudioSource = VideoPlayer.GetTargetAudioSource(i);
                        if (targetAudioSource != null)
                        {
                            targetAudioSource.mute = !isActiveLanguage;
                        }
                    }
                }
            }
        }

        //
        // ILocalizable implementation
        //
        
        public override void OnLocalizeComplete()
        {
            if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchAudioTracks)
            {
                VideoPlayer.Pause();
                SwitchAudioTracksToActiveLanguage();
                VideoPlayer.Play();
            }
            else if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchVideoClip)
            {
                if (m_localizedDictionary != null && m_localizedDictionary.ContainsKey(LocalizationManager.Instance.ActiveLanguage))
                {
                    VideoClip localizedVideoClip = m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage] as VideoClip;

                    if (localizedVideoClip != null)
                    {
                        if (VideoPlayer.clip != null)
                        {
                            double previousTime = VideoPlayer.time;
                            bool wasPlaying = VideoPlayer.isPlaying;
                            VideoPlayer.clip = localizedVideoClip;
                            double localizedClipLength = localizedVideoClip.length;
                            if (previousTime <= localizedClipLength)
                            {
                                // Set timeSamples before playing to prevent seek error
                                VideoPlayer.time = previousTime;
                            }
                            else
                            {
                                VideoPlayer.time = localizedClipLength;
                            }
                            if (wasPlaying)
                            {
                                VideoPlayer.Play();
                            }
                        }
                        else
                        {
                            VideoPlayer.clip = localizedVideoClip;
                        }
                    }
                }
            }
            else if (m_VideoLocalizationMethod == VideoLocalizationMethod.SwitchUrl)
            {
                if (m_localizedDictionary != null && m_localizedDictionary.ContainsKey(LocalizationManager.Instance.ActiveLanguage))
                {
                    string localizedUrl = m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage] as string;

                    if (localizedUrl != null)
                    {
                        if (VideoPlayer.url != null)
                        {
                            double previousTime = VideoPlayer.time;
                            bool wasPlaying = VideoPlayer.isPlaying;
                            VideoPlayer.url = localizedUrl;
                            VideoPlayer.time = previousTime;
                            if (wasPlaying)
                            {
                                VideoPlayer.Play();
                            }
                        }
                        else
                        {
                            VideoPlayer.url = localizedUrl;
                        }
                    }
                }
            }
        }
    }

}