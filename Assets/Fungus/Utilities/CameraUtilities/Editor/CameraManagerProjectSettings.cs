﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.CameraUtilities
{

    public class CameraManagerSettingsProvider : BaseAssetSettingsProvider<CameraManager>
    {
        public CameraManagerSettingsProvider() : base("Project/Managers/Camera Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new CameraManagerSettingsProvider();
        }
    }

    public class CameraManagerSettingsBuilder : BaseAssetSettingsBuilder<CameraManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif