#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.CameraUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(View), true)]
    public class ViewEditor : PersistentBehaviourEditor
    {
        Camera m_mainCamera;
        Vector3 m_originalCameraPosition;
        Vector3 m_originalCameraRotation;
        Vector3 m_originalCameraScale;
        float m_originalCameraFieldOfView;
        float m_originalCameraSize;

        protected override void OnEnable()
        {
            base.OnEnable();

            View t = target as View;

            m_mainCamera = Camera.main;

            if (m_mainCamera != null)
            {
                m_originalCameraPosition = m_mainCamera.transform.position;
                m_originalCameraRotation = m_mainCamera.transform.eulerAngles;
                m_originalCameraScale = m_mainCamera.transform.localScale;
                m_originalCameraFieldOfView = m_mainCamera.fieldOfView;
                m_originalCameraSize = m_mainCamera.orthographicSize;
            }

            if (m_mainCamera != null)
            {
                m_mainCamera.transform.position = t.transform.position;
                m_mainCamera.transform.eulerAngles = t.transform.eulerAngles;
                m_mainCamera.transform.localScale = t.transform.localScale;
                m_mainCamera.fieldOfView = t.FieldOfView;
                m_mainCamera.orthographicSize = t.OrthographicSize;
            }
        }

        protected virtual void OnDisable()
        {
            if (m_mainCamera != null)
            {
                m_mainCamera.transform.position = m_originalCameraPosition;
                m_mainCamera.transform.eulerAngles = m_originalCameraRotation;
                m_mainCamera.transform.localScale = m_originalCameraScale;
                m_mainCamera.fieldOfView = m_originalCameraFieldOfView;
                m_mainCamera.orthographicSize = m_originalCameraSize;
            }
        }

        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            View t = target as View;

            if (m_mainCamera != null)
            {
                m_mainCamera.transform.position = t.transform.position;
                m_mainCamera.transform.eulerAngles = t.transform.eulerAngles;
                m_mainCamera.transform.localScale = t.transform.localScale;
                m_mainCamera.fieldOfView = t.FieldOfView;
                m_mainCamera.orthographicSize = t.OrthographicSize;
            }
        }

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            View t = target as View;

            if (property.name == "m_" + nameof(t.Orthographic))
            {
                ExtendedEditorGUI.TogglePopupField(property, new GUIContent("Projection"), "Perspective", "Orthographic");
            }
            else if (property.name == "m_" + nameof(t.AspectRatio))
            {
                EditorGUI.BeginChangeCheck();
                base.DrawProperty(position, property, label);
                if (EditorGUI.EndChangeCheck())
                {
                    // Avoid divide by zero errors
                    if (property.vector2Value.y == 0)
                    {
                        property.vector2Value = new Vector2(property.vector2Value.x, 1f);
                    }
                    SceneView.RepaintAll();
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected virtual void OnSceneGUI()
        {
            View t = target as View;

            if (t.enabled)
            {
                if (t.Orthographic)
                {
                    EditViewBounds();
                }
            }

            SceneView.currentDrawingSceneView.AlignViewToObject(t.transform);
        }

        protected virtual void EditViewBounds()
        {
            View t = target as View;

            Vector3 pos = t.transform.position;

            float viewSize = CalculateLocalViewSize(t);

            Vector3[] handles = new Vector3[2];
            handles[0] = t.transform.TransformPoint(new Vector3(0, -viewSize, 0));
            handles[1] = t.transform.TransformPoint(new Vector3(0, viewSize, 0));

            Handles.color = Color.white;

            for (int i = 0; i < 2; ++i)
            {
                Vector3 newPos = Handles.FreeMoveHandle(handles[i],
                                                        Quaternion.identity,
                                                        HandleUtility.GetHandleSize(pos) * 0.1f,
                                                        Vector3.zero,
                                                        Handles.CubeHandleCap);
                if (newPos != handles[i])
                {
                    Undo.RecordObject(t, "Set View Size");
                    t.OrthographicSize = (newPos - pos).magnitude;
                    EditorUtility.SetDirty(t);
                    break;
                }
            }
        }

        // Calculate view size in local coordinates
        // Kinda expensive, but accurate and only called in editor.
        static float CalculateLocalViewSize(View view)
        {
            return view.transform.InverseTransformPoint(view.transform.position + new Vector3(0, view.OrthographicSize, 0)).magnitude;
        }
    }

}
#endif