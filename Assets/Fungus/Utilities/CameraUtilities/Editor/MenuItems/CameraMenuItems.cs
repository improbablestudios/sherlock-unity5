#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.CameraUtilities
{

    public class CameraMenuItems
    {
        [MenuItem("Tools/Camera/View", false, 10)]
        [MenuItem("GameObject/View", false, 10)]
        static void CreateView()
        {
            PrefabSpawner.SpawnPrefab<View>("~View", true);
        }

        /// <summary>
        /// Create a ForceCameraRatio Object in the current scene
        /// </summary>
        [MenuItem("Tools/Camera/Force Camera Ratio", false, 10)]
        [MenuItem("GameObject/Force Camera Ratio", false, 10)]
        public static void CreateForceCameraRatio()
        {
            PrefabSpawner.SpawnPrefab<View>("~ForceCameraRatio", true);
        }

        [MenuItem("Tools/Camera/Parallax Sprite Renderer", false, 10)]
        [MenuItem("GameObject/Parallax Sprite Renderer", false, 10)]
        static void CreateParallaxSpriteRenderer()
        {
            PrefabSpawner.SpawnPrefab<Parallax>("~ParallaxSpriteRenderer", true, typeof(SpriteRenderer));
        }
    }

}
#endif