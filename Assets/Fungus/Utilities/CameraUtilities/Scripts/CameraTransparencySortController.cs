﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.CameraUtilities
{
    
    [RequireComponent(typeof(Camera))]
    public class CameraTransparencySortController : PersistentBehaviour
    {
        [Tooltip("Transparent object sorting mode")]
        [SerializeField]
        protected TransparencySortMode m_TransparencySortMode;

        [Tooltip("An axis that describes the direction along which the distances of objects are measured for the purpose of sorting")]
        [SerializeField]
        protected Vector3 m_TransparencySortAxis;

        public TransparencySortMode TransparencySortMode
        {
            get
            {
                return m_TransparencySortMode;
            }
            set
            {
                m_TransparencySortMode = value;
            }
        }

        public Vector3 TransparencySortAxis
        {
            get
            {
                return m_TransparencySortAxis;
            }
            set
            {
                m_TransparencySortAxis = value;
            }
        }

        private Camera m_camera;

        public Camera Camera
        {
            get
            {
                if (m_camera == null)
                {
                    m_camera = GetComponent<Camera>();
                }
                return m_camera;
            }
            set
            {
                m_camera = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetTransparencySortingSettings();
        }
        
        void Start()
        {
            SetTransparencySortingSettings();
        }

        public void SetTransparencySortingSettings()
        {
            Camera.transparencySortMode = m_TransparencySortMode;
            Camera.transparencySortAxis = m_TransparencySortAxis;
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetTransparencySortingSettings();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TransparencySortAxis))
            {
                if (m_TransparencySortMode != TransparencySortMode.CustomAxis)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}
