using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using System.Collections.Generic;
using System;
using System.Linq;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ScriptableBehaviourUtilities;

namespace ImprobableStudios.CameraUtilities
{
    
    public class CameraManager : SettingsScriptableObject<CameraManager>
    {
        [Serializable]
        protected class CameraSwipeState
        {
            public bool m_Active;

            public View m_ViewA;

            public View m_ViewB;

            public float m_SpeedMultiplier = 1f;
        }

        [Serializable]
        protected class CameraFollowState
        {
            public GameObject m_FollowedGameObject;

            public Vector3 m_TargetLastPos;

            public Tweener m_FollowTween;
        }

        [Tooltip("Number of pixels per unit")]
        [Min(1)]
        [SerializeField]
        [FormerlySerializedAs("pixelsPerUnit")]
        protected int m_PixelsPerUnit = 32;

        [Tooltip("Number of pixels the screen height should be when at zoom level 1")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("screenPixelHeight")]
        protected int m_ScreenPixelHeight = 600;

        [Tooltip("Icon that will indicate to the user that they can swipe the camera by clicking and dragging")]
        [SerializeField]
        [FormerlySerializedAs("swipePanIcon")]
        protected Texture2D m_SwipePanIcon;

        [Tooltip("Position of continue and swipe icons in normalized screen space coords (e.g. (0,0) = top left, (1,1) = bottom right)")]
        [SerializeField]
        [FormerlySerializedAs("swipeIconPosition")]
        protected Vector2 m_SwipeIconPosition = new Vector2(1, 0);
        
        protected Vector3 m_previousMousePos;
        
        protected Dictionary<Camera, CameraSwipeState> m_cameraSwipeDictionary = new Dictionary<Camera, CameraSwipeState>();
        
        protected Dictionary<Camera, CameraFollowState> m_cameraFollowDictionary = new Dictionary<Camera, CameraFollowState>();

        private Canvas m_fadeCanvas;

        private Image m_fadeImage;

        public int PixelsPerUnit
        {
            get
            {
                return m_PixelsPerUnit;
            }
            set
            {
                m_PixelsPerUnit = value;
            }
        }

        public int ScreenPixelHeight
        {
            get
            {
                return m_ScreenPixelHeight;
            }
            set
            {
                m_ScreenPixelHeight = value;
            }
        }

        public Texture2D SwipePanIcon
        {
            get
            {
                return m_SwipePanIcon;
            }
            set
            {
                m_SwipePanIcon = value;
            }
        }

        public Vector2 SwipeIconPosition
        {
            get
            {
                return m_SwipeIconPosition;
            }
            set
            {
                m_SwipeIconPosition = value;
            }
        }

        protected Canvas FadeCanvas
        {
            get
            {
                if (m_fadeCanvas == null)
                {
                    GameObject fadeCanvasGO = PersistentBehaviour.InstantiateAndRegister(Resources.Load<GameObject>("~FadeCanvas"));
                    m_fadeCanvas = fadeCanvasGO.GetComponentInChildren<Canvas>(true);
                }
                return m_fadeCanvas;
            }
        }

        protected Image FadeImage
        {
            get
            {
                if (m_fadeImage == null)
                {
                    m_fadeImage = FadeCanvas.GetComponentInChildren<Image>(true);
                }
                return m_fadeImage;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            ScriptableBehaviourManager.OnUpdate += Update;
            ScriptableBehaviourManager.OnLateUpdate += LateUpdate;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            ScriptableBehaviourManager.OnUpdate -= Update;
            ScriptableBehaviourManager.OnLateUpdate -= LateUpdate;
        }

        protected virtual void Update()
        {
            foreach (Camera camera in m_cameraSwipeDictionary.Keys.ToList())
            {
                CameraSwipeState cameraSwipeInfo = m_cameraSwipeDictionary[camera];
                if (!cameraSwipeInfo.m_Active)
                {
                    return;
                }

                if (camera == null)
                {
                    Debug.LogWarning("Camera is null");
                    return;
                }

                Vector3 delta = Vector3.zero;

                if (Input.touchCount > 0)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Moved)
                    {
                        delta = Input.GetTouch(0).deltaPosition;
                    }
                }

                if (Input.GetMouseButtonDown(0))
                {
                    m_previousMousePos = Input.mousePosition;
                }
                else if (Input.GetMouseButton(0))
                {
                    delta = Input.mousePosition - m_previousMousePos;
                    m_previousMousePos = Input.mousePosition;
                }

                Vector3 cameraDelta = camera.ScreenToViewportPoint(delta);
                cameraDelta.x *= -2f * cameraSwipeInfo.m_SpeedMultiplier;
                cameraDelta.y *= -2f * cameraSwipeInfo.m_SpeedMultiplier;
                cameraDelta.z = 0f;

                Vector3 cameraPos = camera.transform.position;

                cameraPos += cameraDelta;

                camera.transform.position = CalcCameraPosition(cameraPos, cameraSwipeInfo.m_ViewA, cameraSwipeInfo.m_ViewB);
                camera.orthographicSize = CalcCameraSize(cameraPos, cameraSwipeInfo.m_ViewA, cameraSwipeInfo.m_ViewB);
            }
        }

        void LateUpdate()
        {
            foreach (KeyValuePair<Camera, CameraFollowState> kvp in m_cameraFollowDictionary)
            {
                Camera camera = kvp.Key;
                CameraFollowState cameraFollowState = kvp.Value;
                if (cameraFollowState.m_FollowedGameObject != null)
                {
                    Transform target = cameraFollowState.m_FollowedGameObject.transform;
                    if (cameraFollowState.m_TargetLastPos == target.position) return;
                    cameraFollowState.m_FollowTween.ChangeEndValue(target.position, true).Restart();
                    cameraFollowState.m_TargetLastPos = target.position;
                }
                else
                {
                    if (cameraFollowState.m_FollowTween != null)
                    {
                        cameraFollowState.m_FollowTween.Kill();
                        cameraFollowState.m_FollowTween = null;
                    }
                }
            }
        }

        /// <summary>
        /// Perform a fullscreen fade over a duration.
        /// </summary>
        /// <param name="fadeColor">The color to fade out the screen to</param>
        /// <param name="fadeDuration">The amount of time (in seconds) the fade will take</param>
        /// <param name="ease">The easing that will be used to perform the fade</param>
        /// <param name="sortingOrder">The sorting order of the canvas used to perform the fade</param>
        /// <param name="onComplete">Perform this action when the fade is complete</param>
        public virtual void Fade(Color fadeColor, float fadeDuration, Ease ease, int sortingOrder, UnityAction onComplete = null)
        {
            FadeCanvas.sortingOrder = sortingOrder;
            if (fadeDuration > 0)
            {
                FadeImage.color = new Color(fadeColor.r, fadeColor.g, fadeColor.b, FadeImage.color.a);
                FadeImage.DOColor(fadeColor, fadeDuration)
                    .SetEase(ease)
                    .OnComplete(
                        () =>
                        {
                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        });
            }
            else
            {
                FadeImage.color = fadeColor;
            }
        }

        /// <summary>
        /// Fade out, move camera to a view's position and then fade back in.
        /// </summary>
        /// <param name="camera">The camera that will be moved</param>
        /// <param name="view">The view to move the camera to</param>
        /// <param name="fadeColor">The color to fade out the screen to</param>
        /// <param name="fadeDuration">The amount of time (in seconds) the fade will take</param>
        /// <param name="ease">The easing that will be used to perform the fade</param>
        /// <param name="sortingOrder">The sorting order of the canvas used to perform the fade</param>
        /// <param name="onComplete">Perform this action when the move is complete</param>
        public virtual void FadeToView(Camera camera, View view, Color fadeColor, float fadeDuration, Ease ease, int sortingOrder, UnityAction onComplete = null)
        {
            if (m_cameraSwipeDictionary.ContainsKey(camera))
            {
                DeactivateCameraSwipeMode(camera);
            }

            float outDuration;
            float inDuration;

            outDuration = fadeDuration / 2f;
            inDuration = fadeDuration / 2f;

            // Fade out
            Fade(new Color(fadeColor.r, fadeColor.g, fadeColor.b, 1f), outDuration, ease, sortingOrder, delegate {

                // Snap to new view
                PanToView(camera, view, 0f, Ease.Linear, null);

                // Fade in
                Fade(new Color(fadeColor.r, fadeColor.g, fadeColor.b, 0f), inDuration, ease, sortingOrder, delegate {
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
            });
        }

        /// <summary>
        /// Fade out, move camera to a new position and then fade back in.
        /// </summary>
        /// <param name="camera">The camera that will be moved</param>
        /// <param name="position">The position to move the camera to</param>
        /// <param name="fadeColor">The color to fade out the screen to</param>
        /// <param name="fadeDuration">The amount of time (in seconds) the fade will take</param>
        /// <param name="ease">The easing that will be used to perform the fade</param>
        /// <param name="sortingOrder">The sorting order of the canvas used to perform the fade</param>
        /// <param name="onComplete">Perform this action when the move is complete</param>
        public virtual void FadeToPosition(Camera camera, Vector3 position, Color fadeColor, float fadeDuration, Ease ease, int sortingOrder, UnityAction onComplete = null)
        {
            DeactivateCameraSwipeMode(camera);

            float outDuration;
            float inDuration;

            outDuration = fadeDuration / 2f;
            inDuration = fadeDuration / 2f;

            // Fade out
            Fade(new Color(fadeColor.r, fadeColor.g, fadeColor.b, 1f), outDuration, ease, sortingOrder, delegate {

                // Snap to new view
                PanToPosition(camera, position, 0, Ease.Linear, onComplete);

                // Fade in
                Fade(new Color(fadeColor.r, fadeColor.g, fadeColor.b, 0f), inDuration, ease, sortingOrder, delegate {
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
            });
        }

        public virtual void PanToView(Camera camera, View view, float duration = 0f, Ease easeType = Ease.Unset, UnityAction onComplete = null)
        {
            StopFollowingWithCamera(camera);
            float targetSize = 0;
            if (view.Orthographic)
            {
                targetSize = view.OrthographicSize;
            }
            else
            {
                targetSize = view.FieldOfView;
            }
            PanToPosition(camera, view.transform.position, view.transform.eulerAngles, view.Orthographic, targetSize, view.NearClipPlane, view.FarClipPlane, duration, easeType, onComplete);
        }

        public virtual void PanToPosition(Camera camera, Vector3 targetPosition, float duration, Ease easeType, UnityAction onComplete)
        {
            camera.transform.DOMove(targetPosition, duration)
                .SetEase(easeType)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        /// <summary>
        /// Adjusts the properties of a camera over a period of time.
        /// </summary>
        /// <param name="camera">The camera that will be moved and/or changed</param>
        /// <param name="targetPosition">The position to move the camera to</param>
        /// <param name="targetRotation">The rotation to rotate the camera to</param>
        /// <param name="orthographic">Use an orthographic size when changing the camera's view size<paramref name="targetSize"/></param>
        /// <param name="targetSize">The size of the camera's view (When <paramref name="orthographic"/> is true, sets orthographic size. Otherwise, sets field of view.)</param>
        /// <param name="targetNearClipPlane">The target near clip plane of the camera</param>
        /// <param name="targetFarClipPlane">The target far clip plane of the camera</param>
        /// <param name="duration">The duration of the camera transition</param>
        /// <param name="easeType">The easing to use when adjusting the camera's properites over time</param>
        /// <param name="onComplete">Perform this action when the camera transition is complete</param>
        public virtual void PanToPosition(Camera camera, Vector3 targetPosition, Vector3 targetRotation, bool orthographic, float targetSize, float targetNearClipPlane, float targetFarClipPlane, float duration, Ease easeType, UnityAction onComplete = null)
        {
            StopFollowingWithCamera(camera);
            if (camera == null)
            {
                Debug.LogError("Camera is null");
                return;
            }

            DeactivateCameraSwipeMode(camera);

            if (duration <= 0)
            {
                camera.transform.position = targetPosition;
                camera.transform.eulerAngles = targetRotation;
                if (orthographic)
                {
                    camera.orthographicSize = targetSize;
                }
                else
                {
                    camera.fieldOfView = targetSize;
                }
                camera.nearClipPlane = targetNearClipPlane;
                camera.farClipPlane = targetFarClipPlane;
            }
            else
            {
                camera.transform.DOMove(targetPosition, duration)
                    .SetEase(easeType)
                    .OnComplete(
                        () =>
                        {
                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        });
                camera.transform.DORotate(targetRotation, duration).SetEase(easeType);
                if (orthographic)
                {
                    camera.DOOrthoSize(targetSize, duration).SetEase(easeType);
                }
                else
                {
                    camera.DOFieldOfView(targetSize, duration).SetEase(easeType);
                }
                camera.DONearClipPlane(targetNearClipPlane, duration).SetEase(easeType);
                camera.DOFarClipPlane(targetFarClipPlane, duration).SetEase(easeType);
            }
        }

        /// <summary>
        /// Moves the camera to <paramref name="viewA"/> 
        /// and then allows the player to control the camera position between <paramref name="viewA"/> and <paramref name="viewB"/> by swiping.
        /// </summary>
        /// <param name="camera">The camera that will be controlled by the player</param>
        /// <param name="viewA">The view that represents the minimum of the camera swipe range</param>
        /// <param name="viewB">The view that represents the maxiumum of the camera swipe range</param>
        /// <param name="swipeSpeedMultiplier">The speed with which the camera will move when the player swipes</param>
        /// <param name="duration">The time it will take for the camera to move to viewA and be ready to be controlled</param>
        /// <param name="ease">The easing to use when moving the camera to viewA</param>
        /// <param name="onComplete">Perform this action when the camera has moved to viewA and is ready to be controlled</param>
        public virtual void ActivateCameraSwipeMode(Camera camera, View viewA, View viewB, float swipeSpeedMultiplier, float duration, Ease ease, UnityAction onComplete = null)
        {
            if (camera == null)
            {
                Debug.LogWarning("Camera is null");
                return;
            }

            Vector3 cameraPos = camera.transform.position;

            Vector3 targetPosition = CalcCameraPosition(cameraPos, viewA, viewB);
            float targetSize = CalcCameraSize(cameraPos, viewA, viewB);

            PanToPosition(camera, targetPosition, Vector3.zero, true, targetSize, camera.nearClipPlane, camera.farClipPlane, duration, ease, delegate {

                if (!m_cameraSwipeDictionary.ContainsKey(camera))
                {
                    m_cameraSwipeDictionary[camera] = new CameraSwipeState();
                }

                CameraSwipeState cameraSwipeInfo = m_cameraSwipeDictionary[camera];

                cameraSwipeInfo.m_Active = true;
                cameraSwipeInfo.m_ViewA = viewA;
                cameraSwipeInfo.m_ViewB = viewB;
                cameraSwipeInfo.m_SpeedMultiplier = swipeSpeedMultiplier;

                if (onComplete != null)
                {
                    onComplete();
                }
            });
        }

        /// <summary>
        /// Don't allow the player to control this camera by swiping.
        /// </summary>
        /// <param name="camera">The camera that was controlled by the player</param>
        public virtual void DeactivateCameraSwipeMode(Camera camera)
        {
            if (m_cameraSwipeDictionary.ContainsKey(camera))
            {
                m_cameraSwipeDictionary.Remove(camera);
            }
        }

        /// <summary>
        /// Zooms the camera.
        /// </summary>
        /// <param name="camera">The camera to zoom</param>
        /// <param name="zoomLevel">The level of zoom</param>
        /// <param name="duration">The amount of time (in seconds) it takes to zoom</param>
        /// <param name="ease">The easing to use when zooming the camera</param>
        /// <param name="onComplete">Perform this action when the camera zoom is complete</param>
        public virtual void Zoom(Camera camera, float zoomLevel, float duration = 0f, Ease ease = Ease.InOutElastic, UnityAction onComplete = null)
        {
            float zoomedSize = CalculatePixelPerfectCameraSize(Camera.main, m_PixelsPerUnit, zoomLevel);
            Camera.main.DOOrthoSize(zoomedSize, duration)
                .SetEase(ease)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        private CameraFollowState GetCameraFollowState(Camera camera)
        {
            CameraFollowState state = null;
            if (m_cameraFollowDictionary.ContainsKey(camera))
            {
                state = m_cameraFollowDictionary[camera];
            }
            else
            {
                state = new CameraFollowState();
                m_cameraFollowDictionary[camera] = state;
            }

            return state;
        }

        private void Follow(Camera camera, GameObject targetObject)
        {
            CameraFollowState cameraFollowState = GetCameraFollowState(camera);
            
            cameraFollowState.m_TargetLastPos = targetObject.transform.position;

            cameraFollowState.m_FollowedGameObject = targetObject;

            if (cameraFollowState.m_FollowTween != null)
            {
                cameraFollowState.m_FollowTween.Kill();
                cameraFollowState.m_FollowTween = null;
            }
            cameraFollowState.m_FollowTween = camera.transform.DOMove(targetObject.transform.position, 0).SetAutoKill(false);

        }

        public virtual void StartFollowingWithCamera(Camera camera, GameObject targetObject, float duration = 0, Ease ease = Ease.InOutQuad, UnityAction onComplete = null)
        {
            StopFollowingWithCamera(camera);
            
            if (duration <= 0)
            {
                Follow(camera, targetObject);
                if (onComplete != null)
                {
                    onComplete();
                }
            }
            else
            {
                PanToPosition(
                    camera, 
                    targetObject.transform.position,
                    duration, 
                    ease,
                    () =>
                    {
                        Follow(camera, targetObject);
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
            }
        }

        public virtual void StopFollowingWithCamera(Camera camera)
        {
            if (m_cameraFollowDictionary.ContainsKey(camera))
            {
                CameraFollowState cameraFollowState = m_cameraFollowDictionary[camera];

                if (cameraFollowState.m_FollowTween != null)
                {
                    cameraFollowState.m_FollowTween.Kill();
                    cameraFollowState.m_FollowTween = null;
                }

                cameraFollowState.m_FollowedGameObject = null;

                m_cameraFollowDictionary.Remove(camera);
            }
        }
        
        // Clamp camera position to region defined by the two views
        protected virtual Vector3 CalcCameraPosition(Vector3 pos, View viewA, View viewB)
        {
            Vector3 safePos = pos;

            // Clamp camera position to region defined by the two views
            safePos.x = Mathf.Max(safePos.x, Mathf.Min(viewA.transform.position.x, viewB.transform.position.x));
            safePos.x = Mathf.Min(safePos.x, Mathf.Max(viewA.transform.position.x, viewB.transform.position.x));
            safePos.y = Mathf.Max(safePos.y, Mathf.Min(viewA.transform.position.y, viewB.transform.position.y));
            safePos.y = Mathf.Min(safePos.y, Mathf.Max(viewA.transform.position.y, viewB.transform.position.y));

            return safePos;
        }

        // Smoothly interpolate camera orthographic size based on relative position to two views
        protected virtual float CalcCameraSize(Vector3 pos, View viewA, View viewB)
        {
            // Get ray and point in same space
            Vector3 toViewB = viewB.transform.position - viewA.transform.position;
            Vector3 globalPos = pos - viewA.transform.position;

            // Normalize
            float distance = toViewB.magnitude;
            toViewB /= distance;
            globalPos /= distance;

            // Project point onto ray
            float t = Vector3.Dot(toViewB, globalPos);
            t = Mathf.Clamp01(t); // Not really necessary but no harm

            float cameraSize = Mathf.Lerp(viewA.OrthographicSize, viewB.OrthographicSize, t);

            return cameraSize;
        }

        public float CalculatePixelPerfectCameraSize(Camera camera, float assetsPixelsPerUnit, float zoomLevel)
        {
            return (m_ScreenPixelHeight / (zoomLevel * assetsPixelsPerUnit)) * 0.5f;
        }
    }
}