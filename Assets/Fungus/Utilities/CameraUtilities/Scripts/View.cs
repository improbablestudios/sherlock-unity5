using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.CameraUtilities
{
    /// <summary>
    /// Defines a camera view
    /// The position and rotation are specified using the game object's transform, so this class
    /// only needs to specify the ortographic view size.
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Camera/View")]
    public class View : PersistentBehaviour
    {
        [Tooltip("Is the camera orthographic or perspective?")]
        [SerializeField]
        [FormerlySerializedAs("orthographic")]
        protected bool m_Orthographic = true;

        [Tooltip("The orthographic size of the view")]
        [SerializeField]
        [FormerlySerializedAs("orthographicSize")]
        protected float m_OrthographicSize = 5f;

        [Tooltip("The field of view of the view")]
        [SerializeField]
        [FormerlySerializedAs("fieldOfView")]
        protected float m_FieldOfView = 60f;

        [Tooltip("The aspect ratio of the view (e.g. 4:3 aspect ratio = 1.333)")]
        [SerializeField]
        [FormerlySerializedAs("aspectRatio")]
        protected Vector2 m_AspectRatio = new Vector2(4, 3);

        [Tooltip("The near clip plane of the view")]
        [SerializeField]
        [FormerlySerializedAs("nearClipPlane")]
        protected float m_NearClipPlane = 0.3f;

        [Tooltip("The far clip plane of the view")]
        [SerializeField]
        [FormerlySerializedAs("farClipPlane")]
        protected float m_FarClipPlane = 1000f;

        [Tooltip("The color of this view's gizmos")]
        [SerializeField]
        [FormerlySerializedAs("gizmoColor")]
        protected Color m_GizmoColor = Color.green;
        
        public bool Orthographic
        {
            get
            {
                return m_Orthographic;
            }
            set
            {
                m_Orthographic = value;
            }
        }
        
        public float OrthographicSize
        {
            get
            {
                return m_OrthographicSize;
            }
            set
            {
                m_OrthographicSize = value;
            }
        }

        public float FieldOfView
        {
            get
            {
                return m_FieldOfView;
            }
            set
            {
                m_FieldOfView = value;
            }
        }

        public Vector2 AspectRatio
        {
            get
            {
                return m_AspectRatio;
            }
            set
            {
                m_AspectRatio = value;
            }
        }

        public float NearClipPlane
        {
            get
            {
                return m_NearClipPlane;
            }
            set
            {
                m_NearClipPlane = value;
            }
        }

        public float FarClipPlane
        {
            get
            {
                return m_FarClipPlane;
            }
            set
            {
                m_FarClipPlane = value;
            }
        }

        public Color GizmoColor
        {
            get
            {
                return m_GizmoColor;
            }
            set
            {
                m_GizmoColor = value;
            }
        }

        protected virtual void Update()
        {
            transform.localScale = new Vector3(1,1,1);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = m_GizmoColor;
            if (m_Orthographic)
            {
                float spread = m_FarClipPlane - m_NearClipPlane;
                float center = (m_FarClipPlane + m_NearClipPlane) * 0.5f;
                Gizmos.DrawWireCube(new Vector3(transform.position.x, transform.position.y, transform.position.z + center), new Vector3(m_OrthographicSize * 2 * (m_AspectRatio.x / m_AspectRatio.y), m_OrthographicSize * 2, spread));
            }
            else
            {
                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.DrawFrustum(transform.position, m_FieldOfView, m_NearClipPlane, m_FarClipPlane, (m_AspectRatio.x / m_AspectRatio.y));
            }
            Gizmos.color = Color.white;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_OrthographicSize))
            {
                if (!m_Orthographic)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_FieldOfView))
            {
                if (m_Orthographic)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}