﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.CameraUtilities
{
    /// <summary>
    /// Attach this component to a sprite object to apply a simple parallax scrolling effect.
    /// The horizontal and vertical parallax offset is calculated based on the distance from the camera to the position of the background sprite.
    /// The scale parallax is calculated based on the ratio of the camera size to the size of the background sprite.This gives a 'dolly zoom' effect.
    /// Accelerometer based parallax is also applied on devices that support it.
    /// </summary>
    public class Parallax : PersistentBehaviour
    {
        [Tooltip("The background sprite which this sprite is layered on top of (The position of this sprite is used to calculate the parallax offset)")]
        [SerializeField]
        [FormerlySerializedAs("backgroundSprite")]
        protected SpriteRenderer m_BackgroundSprite;
        
        [Tooltip("Scale factor for calculating the parallax offset")]
        [SerializeField]
        [FormerlySerializedAs("parallaxScale")]
        protected Vector2 m_ParallaxScale = new Vector2(0.25f, 0f);
        
        [Tooltip("Scale factor for calculating parallax offset based on device accelerometer tilt angle (Set this to 0 to disable the accelerometer parallax effect)")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("accelerometerScale")]
        protected float m_AccelerometerScale = 0.5f;

        protected Vector3 m_startPosition;

        protected Vector3 m_acceleration;

        protected Vector3 m_velocity;
        
        public SpriteRenderer BackgroundSprite
        {
            get
            {
                return m_BackgroundSprite;
            }
            set
            {
                m_BackgroundSprite = value;
            }
        }
        
        public Vector2 ParallaxScale
        {
            get
            {
                return m_ParallaxScale;
            }
            set
            {
                m_ParallaxScale = value;
            }
        }

        public float AccelerometerScale
        {
            get
            {
                return m_AccelerometerScale;
            }
            set
            {
                m_AccelerometerScale = value;
            }
        }

        protected virtual void Start()
        {
            // Store the starting position and scale of the sprite object
            m_startPosition = transform.position;

            // Default to using parent sprite as background
            if (m_BackgroundSprite == null)
            {
                m_BackgroundSprite = gameObject.GetComponentInParent<SpriteRenderer>();
            }
        }

        protected virtual void Update()
        {
            if (m_BackgroundSprite == null)
            {
                return;
            }

            Vector3 translation = Vector3.zero;

            // Apply parallax translation based on camera position relative to background sprite
            {
                Vector3 a = m_BackgroundSprite.bounds.center;
                Vector3 b = Camera.main.transform.position;
                translation = (a - b);
                translation.x *= m_ParallaxScale.x;
                translation.y *= m_ParallaxScale.y;
                translation.z = 0;

                // TODO: Limit parallax offset to just outside the bounds of the background sprite
            }

            // Apply parallax translation based on device accelerometer
            if (SystemInfo.supportsAccelerometer)
            {
                float maxParallaxScale = Mathf.Max(m_ParallaxScale.x, m_ParallaxScale.y);
                // The accelerometer data is quite noisy, so we apply smoothing to even it out.
                m_acceleration = Vector3.SmoothDamp(m_acceleration, Input.acceleration, ref m_velocity, 0.1f);
                // Assuming a 45 degree "neutral position" when holding a mobile device
                Vector3 accelerometerOffset = Quaternion.Euler(45, 0, 0) * m_acceleration * maxParallaxScale * m_AccelerometerScale;
                translation += accelerometerOffset;
            }

            transform.position = m_startPosition + translation;
        }
    }
}
