#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using UnityEditor.Animations;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    [CustomPropertyDrawer(typeof(TransitionStates), true)]
    public class TransitionStatesDrawer : PropertyDrawer
    {
        bool m_statesFoldout;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty statesProp = property.FindPropertyRelative("m_States");
            Rect pos = position;
            pos.height = EditorGUIUtility.singleLineHeight;
            m_statesFoldout = EditorGUI.Foldout(pos, m_statesFoldout, new GUIContent(statesProp.displayName, statesProp.tooltip));
            pos.y += EditorGUIUtility.singleLineHeight;
            pos.y += 2f;
            if (m_statesFoldout)
            {
                for (int i = 0; i < statesProp.arraySize; i++)
                {
                    SerializedProperty stateProp = statesProp.GetArrayElementAtIndex(i);
                    SerializedProperty keyProp = stateProp.FindPropertyRelative("m_" + nameof(TransitionState.Key));
                    SerializedProperty transitionProp = stateProp.FindPropertyRelative("m_" + nameof(TransitionState.Transition));
                    EditorGUI.PropertyField(pos, transitionProp, new GUIContent(keyProp.stringValue, transitionProp.tooltip), true);
                    pos.y += EditorGUIUtility.singleLineHeight;
                    pos.y += 2f;
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = 0f;

            height += EditorGUIUtility.singleLineHeight;
            height += 2f;
            if (m_statesFoldout)
            {
                SerializedProperty statesProp = property.FindPropertyRelative("m_States");
                for (int i = 0; i < statesProp.arraySize; i++)
                {
                    height += EditorGUIUtility.singleLineHeight;
                    height += 2f;
                }
            }
            return height;
        }
    }

}
#endif