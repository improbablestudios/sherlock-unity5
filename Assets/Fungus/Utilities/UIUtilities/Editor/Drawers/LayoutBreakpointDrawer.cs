#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.SerializedPropertyUtilities;
using System.Collections.Generic;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CustomPropertyDrawer(typeof(LayoutBreakpoint), true)]
    public class LayoutBreakpointDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            LayoutBreakpoint layoutBreakpoint = property.GetPropertyValue() as LayoutBreakpoint;

            SerializedProperty[] props = property.GetVisibleChildren(true);
            SerializedProperty modeProp = property.FindPropertyRelative("m_" + nameof(layoutBreakpoint.Mode));

            for (int i = 0; i < props.Length; i++)
            {
                SerializedProperty prop = props[i];

                if (prop.propertyPath == modeProp.propertyPath)
                {
                    EditorGUI.BeginChangeCheck();

                    EditorGUI.PropertyField(position, prop, label, true);
                    position.y += EditorGUI.GetPropertyHeight(prop, label, true);
                    position.y += 2f;

                    if (EditorGUI.EndChangeCheck())
                    {
                        if (modeProp.enumValueIndex == (int)LayoutBreakpointMode.ExplicitSize)
                        {
                            Component component = property.serializedObject.targetObject as Component;
                            if (component != null)
                            {
                                RectTransform rectTransform = component.GetComponent<RectTransform>();
                                if (rectTransform != null)
                                {
                                    property.serializedObject.ApplyModifiedProperties();
                                    ResponsiveListLayoutGroup responsiveListLayoutGroup = property.serializedObject.targetObject as ResponsiveListLayoutGroup;
                                    if (responsiveListLayoutGroup != null)
                                    {
                                        float size = responsiveListLayoutGroup.GetAxisTotalPreferredSize(layoutBreakpoint.GetAxis());
                                        layoutBreakpoint.SetAxisBreakpointSize(size);
                                    }
                                }
                                property.serializedObject.Update();
                            }
                        }
                    }
                }
                else if (modeProp.enumValueIndex == (int)LayoutBreakpointMode.ExplicitSize)
                {
                    GUIContent propLabel = new GUIContent(prop.displayName, prop.tooltip);
                    
                    EditorGUI.PropertyField(position, prop, propLabel, true);
                    position.y += EditorGUI.GetPropertyHeight(prop, propLabel, true);
                    position.y += 2f;
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            LayoutBreakpoint layoutBreakpoint = property.GetPropertyValue() as LayoutBreakpoint;

            SerializedProperty[] props = property.GetVisibleChildren(true);
            SerializedProperty modeProp = property.FindPropertyRelative("m_" + nameof(layoutBreakpoint.Mode));

            float height = 0f;

            for (int i = 0; i < props.Length; i++)
            {
                SerializedProperty prop = props[i];

                if (prop.propertyPath == modeProp.propertyPath || modeProp.enumValueIndex == (int)LayoutBreakpointMode.ExplicitSize)
                {
                    height += EditorGUI.GetPropertyHeight(prop, new GUIContent(prop.displayName, prop.tooltip));
                    height += 2f;
                }
            }

            return height;
        }
    }

}
#endif