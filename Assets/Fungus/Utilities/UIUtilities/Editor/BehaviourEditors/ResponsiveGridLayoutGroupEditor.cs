#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.UIUtilities
{

    [CustomEditor(typeof(ResponsiveGridLayoutGroup), true)]
    [CanEditMultipleObjects]
    public class ResponsiveGridLayoutGroupEditor : ResponsiveLayoutGroupEditor
    {
        public override bool IsPropertyVisible(SerializedProperty property)
        {
            ResponsiveGridLayoutGroup t = target as ResponsiveGridLayoutGroup;

            if (property.name == "m_" + nameof(t.RowCount))
            {
                return (t.GridConstraint == ResponsiveGridLayoutGroup.GridSizeConstraint.FixedRowCount);
            }
            else if (property.name == "m_" + nameof(t.ColumnCount))
            {
                return (t.GridConstraint == ResponsiveGridLayoutGroup.GridSizeConstraint.FixedColumnCount);
            }
            else if (property.name == "m_" + nameof(t.CellWidth))
            {
                return (t.CellWidthConstraint == ResponsiveGridLayoutGroup.CellSizeConstraint.Explicit);
            }
            else if (property.name == "m_" + nameof(t.CellHeight))
            {
                return (t.CellHeightConstraint == ResponsiveGridLayoutGroup.CellSizeConstraint.Explicit);
            }
            else
            {
                return base.IsPropertyVisible(property);
            }
        }

        public override int GetPropertyOrder(SerializedProperty property)
        {
            ResponsiveGridLayoutGroup t = target as ResponsiveGridLayoutGroup;

            if (property.name == "m_" + nameof(t.StartCorner))
            {
                return 3;
            }
            else if (property.name == "m_" + nameof(t.GridConstraint))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.CellWidthConstraint))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.CellHeightConstraint))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.RowCount))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ColumnCount))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.CellWidth))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.CellHeight))
            {
                return 5;
            }
            else
            {
                return base.GetPropertyOrder(property);
            }
        }
        
        public override int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            ResponsiveGridLayoutGroup t = target as ResponsiveGridLayoutGroup;

            if (property.name == "m_" + nameof(t.StartCorner))
            {
                return 0;
            }
            if (property.name == "m_" + nameof(t.ColumnCount))
            {
                return 1;
            }
            if (property.name == "m_" + nameof(t.RowCount))
            {
                return 1;
            }
            if (property.name == "m_" + nameof(t.CellWidth))
            {
                return 1;
            }
            if (property.name == "m_" + nameof(t.CellHeight))
            {
                return 1;
            }
            return base.GetPropertyIndentLevelOffset(property);
        }
    }

}

#endif