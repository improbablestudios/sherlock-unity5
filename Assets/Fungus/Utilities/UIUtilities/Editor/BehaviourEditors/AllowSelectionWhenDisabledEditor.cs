﻿#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{
    [CanEditMultipleObjects, CustomEditor(typeof(AllowSelectionWhenDisabled), true)]
    public class AllowSelectionWhenDisabledEditor : BaseEditor
    {
        public override void DrawNormalInspector()
        {
            EditorGUILayout.HelpBox("This component only works when using NavigationGroup, SelectionScroller, or Selectable.FindNearestSelectable. It will not work for Selectable.FindSelectable, Selectable.FindSelectableOnLeft, Selectable.FindSelectableOnRight, Selectable.FindSelectableOnDown, Selectable.FindSelectableOnUp, or when using the Automatic navigation mode.", MessageType.Info);
        }
    }
}
#endif