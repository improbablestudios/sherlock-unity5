#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;

namespace ImprobableStudios.UIUtilities
{

    [CustomEditor(typeof(AdaptiveLayout), true)]
    [CanEditMultipleObjects]
    public class AdaptiveLayoutEditor : BaseEditor
    {
        public override bool IsPropertyVisible(SerializedProperty property)
        {
            AdaptiveLayout t = target as AdaptiveLayout;

            if (property.propertyPath == "m_" + nameof(t.WidthBreakpoint) + "." + "m_" + nameof(t.WidthBreakpoint.Mode))
            {
                return t.WidthBreakpoint.Mode == LayoutBreakpointMode.ExplicitSize;
            }
            else if (property.propertyPath == "m_" + nameof(t.HeightBreakpoint) + "." + "m_" + nameof(t.HeightBreakpoint.Mode))
            {
                return t.HeightBreakpoint.Mode == LayoutBreakpointMode.ExplicitSize;
            }
            else
            {
                return base.IsPropertyVisible(property);
            }
        }

        public override int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            AdaptiveLayout t = target as AdaptiveLayout;

            if (property.name == "m_" + nameof(t.ElementsActiveWhenBelowWidthBreakpoint))
            {
                return 1;
            }
            else if (property.name == "m_" + nameof(t.ElementsActiveWhenAboveWidthBreakpoint))
            {
                return 1;
            }
            else if (property.name == "m_" + nameof(t.ElementsActiveWhenBelowHeightBreakpoint))
            {
                return 1;
            }
            else if (property.name == "m_" + nameof(t.ElementsActiveWhenAboveHeightBreakpoint))
            {
                return 1;
            }
            else
            {
                return base.GetPropertyIndentLevelOffset(property);
            }
        }
    }

}

#endif