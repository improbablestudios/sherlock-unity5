#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CustomEditor(typeof(ResponsiveListLayoutGroup), true)]
    [CanEditMultipleObjects]
    public class ResponsiveListLayoutGroupEditor : ResponsiveLayoutGroupEditor
    {
        private GUIContent m_controlsSizeLabel = new GUIContent("Control Child Size", "Returns true if the Layout Group controls the sizes of its children. Returns false if children control their own sizes.");
        private GUIContent m_forceExpandLabel = new GUIContent("Child Force Expand", "Whether to force the children to expand to take up all available space.");
        private GUIContent m_forceEqualLabel = new GUIContent("Child Force Equal", "Whether to force the children to be the same size as the largest child.");
        private Rect rect;
        
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ResponsiveListLayoutGroup t = target as ResponsiveListLayoutGroup;

            if (property.name == "m_" + nameof(t.ChildControlWidth))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect = position;
                rect = EditorGUI.PrefixLabel(rect, -1, m_controlsSizeLabel);
                rect.width = Mathf.Max(50, (rect.width - 4) / 3);
                EditorGUIUtility.labelWidth = 50;
                label.text = "Width";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.ChildControlHeight))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect.x += rect.width + 2;
                EditorGUIUtility.labelWidth = 50;
                label.text = "Height";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.ChildForceExpandWidth))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect = position;
                rect = EditorGUI.PrefixLabel(rect, -1, m_forceExpandLabel);
                rect.width = Mathf.Max(50, (rect.width - 4) / 3);
                EditorGUIUtility.labelWidth = 50;
                label.text = "Width";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.ChildForceExpandHeight))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect.x += rect.width + 2;
                EditorGUIUtility.labelWidth = 50;
                label.text = "Height";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.ChildForceEqualWidth))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect = position;
                rect = EditorGUI.PrefixLabel(rect, -1, m_forceEqualLabel);
                rect.width = Mathf.Max(50, (rect.width - 4) / 3);
                EditorGUIUtility.labelWidth = 50;
                label.text = "Width";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.ChildForceEqualHeight))
            {
                EditorGUI.BeginProperty(position, label, property);
                rect.x += rect.width + 2;
                EditorGUIUtility.labelWidth = 50;
                label.text = "Height";
                ToggleLeft(rect, property, label);
                EditorGUIUtility.labelWidth = 0;
                EditorGUI.EndProperty();
            }
            else
            {
                base.OnPropertyGUI(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ResponsiveListLayoutGroup t = target as ResponsiveListLayoutGroup;

            if (property.name == "m_" + nameof(t.ChildControlHeight))
            {
                return 0;
            }
            else if (property.name == "m_" + nameof(t.ChildForceExpandHeight))
            {
                return 0;
            }
            else if (property.name == "m_" + nameof(t.ChildForceEqualHeight))
            {
                return 0;
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        void ToggleLeft(Rect position, SerializedProperty property, GUIContent label)
        {
            bool toggle = property.boolValue;
            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;
            EditorGUI.BeginChangeCheck();
            int oldIndent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            toggle = EditorGUI.ToggleLeft(position, label, toggle);
            EditorGUI.indentLevel = oldIndent;
            if (EditorGUI.EndChangeCheck())
            {
                property.boolValue = property.hasMultipleDifferentValues ? true : !property.boolValue;
            }
            EditorGUI.showMixedValue = false;
        }

        public override bool IsPropertyVisible(SerializedProperty property)
        {
            ResponsiveListLayoutGroup t = target as ResponsiveListLayoutGroup;

            if (property.propertyPath == "m_" + nameof(t.WidthBreakpoint))
            {
                return t.StartAxis == ResponsiveLayoutGroup.Axis.Horizontal;
            }
            else if (property.propertyPath == "m_" + nameof(t.HeightBreakpoint))
            {
                return t.StartAxis == ResponsiveLayoutGroup.Axis.Vertical;
            }
            if (property.propertyPath == "m_" + nameof(t.HorizontalOrder))
            {
                return t.StartAxis == ResponsiveLayoutGroup.Axis.Horizontal;
            }
            else if (property.propertyPath == "m_" + nameof(t.VerticalOrder))
            {
                return t.StartAxis == ResponsiveLayoutGroup.Axis.Vertical;
            }
            else
            {
                return base.IsPropertyVisible(property);
            }
        }

        public override int GetPropertyOrder(SerializedProperty property)
        {
            ResponsiveListLayoutGroup t = target as ResponsiveListLayoutGroup;

            if (property.name == "m_" + nameof(t.HorizontalOrder))
            {
                return 4;
            }
            else if (property.name == "m_" + nameof(t.VerticalOrder))
            {
                return 4;
            }
            else if (property.propertyPath == "m_" + nameof(t.WidthBreakpoint))
            {
                return 5;
            }
            else if (property.propertyPath == "m_" + nameof(t.HeightBreakpoint))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildControlWidth))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildControlHeight))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildForceExpandWidth))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildForceExpandHeight))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildForceEqualWidth))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.ChildForceEqualHeight))
            {
                return 5;
            }
            else if (property.name == "m_" + nameof(t.LayoutPriority))
            {
                return 5;
            }
            else
            {
                return base.GetPropertyOrder(property);
            }
        }
    }

}

#endif