﻿#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(Counter), true)]
    public class CounterEditor : UIConstructEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Counter t = target as Counter;

            if (property.name == "m_" + nameof(t.Value))
            {
                EditorGUI.BeginProperty(position, label, property);
                if (t.HasMaxValue)
                {
                    if (t.WholeNumbers)
                    {
                        property.floatValue = EditorGUI.IntSlider(position, label, (int)Math.Round(property.floatValue), (int)Math.Round(t.MinValue), (int)Math.Round(t.MaxValue));
                    }
                    else
                    {
                        property.floatValue = EditorGUI.Slider(position, label, property.floatValue, t.MinValue, t.MaxValue);
                    }
                }
                else
                {
                    if (t.WholeNumbers)
                    {
                        property.floatValue = EditorGUI.IntField(position, label, (int)Math.Round(property.floatValue));
                    }
                    else
                    {
                        property.floatValue = EditorGUI.FloatField(position, label, property.floatValue);
                    }
                }
                EditorGUI.EndProperty();
            }
            else if (property.name == "m_" + nameof(t.MinValue))
            {
                if (t.WholeNumbers)
                {
                    property.floatValue = EditorGUI.IntField(position, label, (int)Math.Round(property.floatValue));
                }
                else
                {
                    property.floatValue = EditorGUI.FloatField(position, label, property.floatValue);
                }
            }
            else if (property.name == "m_" + nameof(t.MaxValue))
            {
                if (t.WholeNumbers)
                {
                    property.floatValue = EditorGUI.IntField(position, label, (int)Math.Round(property.floatValue));
                }
                else
                {
                    property.floatValue = EditorGUI.FloatField(position, label, property.floatValue);
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected override void PreviewTransitionButtons()
        {
            Counter t = target as Counter;

            base.PreviewTransitionButtons();

            if (GUILayout.Button(nameof(Counter.CounterTransitionStateType.CountDown)))
            {
                t.DecrementCounter(1);
            }
            if (GUILayout.Button(nameof(Counter.CounterTransitionStateType.CountUp)))
            {
                t.IncrementCounter(1);
            }
        }
    }

}
#endif