#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;


namespace ImprobableStudios.UIUtilities
{

    [CustomEditor(typeof(UITransitionEffect), true)]
    public class UITransitionEffectEditor : PersistentBehaviourEditor
    {
        AnimatorGenerator m_animatorGenerator = new AnimatorGenerator();

        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            UITransitionEffect t = target as UITransitionEffect;

            UITransitionEffect.TransitionType transition = t.Transition;

            Graphic[] targetGraphics = t.TargetGraphics;

            Animator animator = t.GetComponent<Animator>();

            if (property.name == "m_" + nameof(t.TransitionStates))
            {
                for (int i = 0; i < property.arraySize; i++)
                {
                    SerializedProperty transitionStateProp = property.GetArrayElementAtIndex(i);
                    UITransitionEffect.TransitionState transitionState = t.TransitionStates[i];
                    SerializedProperty nameProp = transitionStateProp.FindPropertyRelative("m_" + nameof(transitionState.Name));
                    SerializedProperty colorProp = transitionStateProp.FindPropertyRelative("m_" + nameof(transitionState.Color));
                    SerializedProperty spriteProp = transitionStateProp.FindPropertyRelative("m_" + nameof(transitionState.Sprite));
                    SerializedProperty animationProp = transitionStateProp.FindPropertyRelative("m_" + nameof(transitionState.Animation));
                    SerializedProperty textStyleProp = transitionStateProp.FindPropertyRelative("m_" + nameof(transitionState.TextStyle));

                    if (transition == UITransitionEffect.TransitionType.ColorTint || transition == UITransitionEffect.TransitionType.ColorChange)
                    {
                        DrawTransitionStateProperty(colorProp, nameProp);
                    }
                    if (transition == UITransitionEffect.TransitionType.SpriteSwap || transition == UITransitionEffect.TransitionType.SpriteChange)
                    {
                        DrawTransitionStateProperty(spriteProp, nameProp);
                    }
                    if (transition == UITransitionEffect.TransitionType.Animation)
                    {
                        DrawTransitionStateProperty(animationProp, nameProp);
                    }
                    if (transition == UITransitionEffect.TransitionType.TextStyle)
                    {
                        DrawTransitionStateProperty(textStyleProp, nameProp);
                    }
                }

                if (transition == UITransitionEffect.TransitionType.Animation)
                {
                    Rect controlRect = EditorGUILayout.GetControlRect(new GUILayoutOption[0]);
                    controlRect.xMin += EditorGUIUtility.labelWidth;
                    if (m_animatorGenerator.AutoGenerateAnimationButton(controlRect))
                    {
                        foreach (UITransitionEffect target in targets)
                        {
                            m_animatorGenerator.AutoGenerateAnimation(target.gameObject, target.TransitionStates.Select(x => x.Animation).ToArray());
                        }
                    }
                }
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }

        protected void DrawTransitionStateProperty(SerializedProperty prop, SerializedProperty nameProp)
        {
            DrawTransitionStateProperty(EditorGUILayout.GetControlRect(GUILayout.Height(EditorGUI.GetPropertyHeight(prop))), prop, nameProp);
        }

        protected void DrawTransitionStateProperty(Rect position, SerializedProperty prop, SerializedProperty nameProp)
        {
            EditorGUI.PropertyField(position, prop, new GUIContent(nameProp.stringValue + " " + prop.displayName, prop.tooltip));
        }
    }

}
#endif