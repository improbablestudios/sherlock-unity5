#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(UIConstruct), true)]
    public class UIConstructEditor : PersistentBehaviourEditor
    {
        AnimatorGenerator m_animatorGenerator = new AnimatorGenerator();

        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            using (new EditorGUILayout.HorizontalScope())
            {
                PreviewTransitionButtons();
            }
        }

        protected virtual void PreviewTransitionButtons()
        {
            UIConstruct t = target as UIConstruct;

            if (GUILayout.Button(nameof(UIConstruct.UITransitionStateType.Show)))
            {
                t.Show();
            }
            if (GUILayout.Button(nameof(UIConstruct.UITransitionStateType.Hide)))
            {
                t.Hide();
            }
            if (GUILayout.Button(nameof(UIConstruct.UITransitionStateType.Finish)))
            {
                t.Finish();
            }
        }

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            UIConstruct t = target as UIConstruct;

            Rect contentPosition = position;
            contentPosition.height = EditorGUIUtility.singleLineHeight;
            contentPosition.xMin += EditorGUIUtility.labelWidth;

            if (property.name == "m_" + nameof(t.SimpleTransitionTarget))
            {
            }
            else if (property.name == "m_" + nameof(t.SimpleTransitionStates))
            {
                SerializedProperty targetProp = GetProperty("m_" + nameof(t.SimpleTransitionTarget));
                base.DrawProperty(contentPosition, targetProp, GUIContent.none);

                base.DrawProperty(position, property, label);
            }
            else if (property.name == "m_" + nameof(t.AnimatedTransitionStates))
            {
                if (m_animatorGenerator == null)
                {
                    m_animatorGenerator = new UIConstructAnimatorGenerator(t.AnimatedTransitionStates.GetAnimatorStateName(nameof(UIConstruct.UITransitionStateType.Idle)));
                }
                if (m_animatorGenerator.AutoGenerateAnimationButton(contentPosition))
                {
                    foreach (UIConstruct target in targets)
                    {
                        m_animatorGenerator.AutoGenerateAnimation(target.gameObject, t.AnimatedTransitionStates.GetAnimatorStateNames());
                    }
                }

                base.DrawProperty(position, property, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            UIConstruct t = target as UIConstruct;

            if (property.name == "m_" + nameof(t.SimpleTransitionTarget))
            {
                return 0f;
            }

            return base.GetPropertyHeight(property, label);
        }
    }

}
#endif