﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.UI;

namespace ImprobableStudios.UIUtilities
{
    [CustomEditor(typeof(FocusedInputField), true)]
    public class FocusedInputFieldEditor : InputFieldEditor
    {
        public override void OnInspectorGUI()
        {
            FocusedInputField t = target as FocusedInputField;

            serializedObject.Update();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_" + nameof(t.SelectOnFocus)));

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_" + nameof(t.FocusOnSelect)));

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_" + nameof(t.UnfocusOnDeselect)));

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_" + nameof(t.SelectAllTextOnFocus)));

            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
        }
    }
}
#endif