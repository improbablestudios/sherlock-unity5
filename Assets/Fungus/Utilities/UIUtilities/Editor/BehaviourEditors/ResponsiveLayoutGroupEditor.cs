#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;

namespace ImprobableStudios.UIUtilities
{

    [CustomEditor(typeof(ResponsiveLayoutGroup), true)]
    [CanEditMultipleObjects]
    public class ResponsiveLayoutGroupEditor : BaseEditor
    {
        public override int GetPropertyOrder(SerializedProperty property)
        {
            ResponsiveLayoutGroup t = target as ResponsiveLayoutGroup;

            if (property.name == "m_" + nameof(t.Spacing))
            {
                return 1;
            }
            else if (property.name == "m_ChildAlignment")
            {
                return 2;
            }
            else if (property.name == "m_" + nameof(t.StartAxis))
            {
                return 4;
            }
            else
            {
                return base.GetPropertyOrder(property);
            }
        }
    }

}

#endif