﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.UIUtilities
{

    public class WriterManagerSettingsProvider : BaseAssetSettingsProvider<WriterManager>
    {
        public WriterManagerSettingsProvider() : base("Project/Managers/Writer Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new WriterManagerSettingsProvider();
        }
    }

    public class WriterManagerSettingsBuilder : BaseAssetSettingsBuilder<WriterManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif