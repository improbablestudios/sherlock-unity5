#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    internal static class MenuOptions
    {
        private const string kUILayerName = "UI";

        private const string kStandardSpritePath = "UI/Skin/UISprite.psd";

        private const string kBackgroundSpritePath = "UI/Skin/Background.psd";

        private const string kInputFieldBackgroundPath = "UI/Skin/InputFieldBackground.psd";

        private const string kKnobPath = "UI/Skin/Knob.psd";

        private const string kCheckmarkPath = "UI/Skin/Checkmark.psd";

        private const string kDropdownArrowPath = "UI/Skin/DropdownArrow.psd";

        private const string kMaskPath = "UI/Skin/UIMask.psd";

        private static DefaultControls.Resources s_StandardResources;

        private static DefaultControls.Resources GetStandardResources()
        {
            if (MenuOptions.s_StandardResources.standard == null)
            {
                MenuOptions.s_StandardResources.standard = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
                MenuOptions.s_StandardResources.background = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Background.psd");
                MenuOptions.s_StandardResources.inputField = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/InputFieldBackground.psd");
                MenuOptions.s_StandardResources.knob = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Knob.psd");
                MenuOptions.s_StandardResources.checkmark = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Checkmark.psd");
                MenuOptions.s_StandardResources.dropdown = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/DropdownArrow.psd");
                MenuOptions.s_StandardResources.mask = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UIMask.psd");
            }
            return MenuOptions.s_StandardResources;
        }

        private static void SetPositionVisibleinSceneView(RectTransform canvasRTransform, RectTransform itemTransform)
        {
            SceneView sceneView = SceneView.lastActiveSceneView;
            if (sceneView == null && SceneView.sceneViews.Count > 0)
            {
                sceneView = (SceneView.sceneViews[0] as SceneView);
            }
            if (!(sceneView == null) && !(sceneView.camera == null))
            {
                Camera camera = sceneView.camera;
                Vector3 zero = Vector3.zero;
                Vector2 vector;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRTransform, new Vector2((float)(camera.pixelWidth / 2), (float)(camera.pixelHeight / 2)), camera, out vector))
                {
                    vector.x += canvasRTransform.sizeDelta.x * canvasRTransform.pivot.x;
                    vector.y += canvasRTransform.sizeDelta.y * canvasRTransform.pivot.y;
                    vector.x = Mathf.Clamp(vector.x, 0f, canvasRTransform.sizeDelta.x);
                    vector.y = Mathf.Clamp(vector.y, 0f, canvasRTransform.sizeDelta.y);
                    zero.x = vector.x - canvasRTransform.sizeDelta.x * itemTransform.anchorMin.x;
                    zero.y = vector.y - canvasRTransform.sizeDelta.y * itemTransform.anchorMin.y;
                    Vector3 vector2;
                    vector2.x = canvasRTransform.sizeDelta.x * (0f - canvasRTransform.pivot.x) + itemTransform.sizeDelta.x * itemTransform.pivot.x;
                    vector2.y = canvasRTransform.sizeDelta.y * (0f - canvasRTransform.pivot.y) + itemTransform.sizeDelta.y * itemTransform.pivot.y;
                    Vector3 vector3;
                    vector3.x = canvasRTransform.sizeDelta.x * (1f - canvasRTransform.pivot.x) - itemTransform.sizeDelta.x * itemTransform.pivot.x;
                    vector3.y = canvasRTransform.sizeDelta.y * (1f - canvasRTransform.pivot.y) - itemTransform.sizeDelta.y * itemTransform.pivot.y;
                    zero.x = Mathf.Clamp(zero.x, vector2.x, vector3.x);
                    zero.y = Mathf.Clamp(zero.y, vector2.y, vector3.y);
                }
                itemTransform.anchoredPosition = zero;
                itemTransform.localRotation = Quaternion.identity;
                itemTransform.localScale = Vector3.one;
            }
        }

        public static void PlaceUIElementRoot(GameObject element, MenuCommand menuCommand)
        {
            GameObject gameObject = menuCommand.context as GameObject;
            if (gameObject == null || gameObject.GetComponentInParent<Canvas>() == null)
            {
                gameObject = MenuOptions.GetOrCreateCanvasGameObject();
            }
            string uniqueNameForSibling = GameObjectUtility.GetUniqueNameForSibling(gameObject.transform, element.name);
            element.name = uniqueNameForSibling;
            Undo.RegisterCreatedObjectUndo(element, "Create " + element.name);
            Undo.SetTransformParent(element.transform, gameObject.transform, "Parent " + element.name);
            GameObjectUtility.SetParentAndAlign(element, gameObject);
            if (gameObject != menuCommand.context)
            {
                MenuOptions.SetPositionVisibleinSceneView(gameObject.GetComponent<RectTransform>(), element.GetComponent<RectTransform>());
            }
            Selection.activeGameObject = element;
        }

        public static GameObject CreateNewUI()
        {
            GameObject gameObject = new GameObject("Canvas");
            gameObject.layer = LayerMask.NameToLayer("UI");
            Canvas canvas = gameObject.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            gameObject.AddComponent<CanvasScaler>();
            gameObject.AddComponent<GraphicRaycaster>();
            Undo.RegisterCreatedObjectUndo(gameObject, "Create " + gameObject.name);
            MenuOptions.CreateEventSystem(false);
            return gameObject;
        }
        
        private static void CreateEventSystem(bool select)
        {
            MenuOptions.CreateEventSystem(select, null);
        }

        private static void CreateEventSystem(bool select, GameObject parent)
        {
            EventSystem eventSystem = UnityEngine.Object.FindObjectOfType<EventSystem>();
            if (eventSystem == null)
            {
                GameObject gameObject = new GameObject("EventSystem");
                GameObjectUtility.SetParentAndAlign(gameObject, parent);
                eventSystem = gameObject.AddComponent<EventSystem>();
                gameObject.AddComponent<StandaloneInputModule>();
                Undo.RegisterCreatedObjectUndo(gameObject, "Create " + gameObject.name);
            }
            if (select && eventSystem != null)
            {
                Selection.activeGameObject = eventSystem.gameObject;
            }
        }

        public static GameObject GetOrCreateCanvasGameObject()
        {
            GameObject activeGameObject = Selection.activeGameObject;
            Canvas canvas = (!(activeGameObject != null)) ? null : activeGameObject.GetComponentInParent<Canvas>();
            GameObject result;
            if (canvas != null && canvas.gameObject.activeInHierarchy)
            {
                result = canvas.gameObject;
            }
            else
            {
                canvas = (UnityEngine.Object.FindObjectOfType(typeof(Canvas)) as Canvas);
                if (canvas != null && canvas.gameObject.activeInHierarchy)
                {
                    result = canvas.gameObject;
                }
                else
                {
                    result = MenuOptions.CreateNewUI();
                }
            }
            return result;
        }
    }

}
#endif