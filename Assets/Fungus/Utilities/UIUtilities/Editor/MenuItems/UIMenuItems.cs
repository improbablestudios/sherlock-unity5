#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    public class UIMenuItems
    {
        [MenuItem("Tools/UI/Toggle Option Menu", false, 10)]
        [MenuItem("GameObject/UI/Toggle Option Menu", false, 10)]
        static void CreateToggleOptionMenu()
        {
            PrefabSpawner.SpawnPrefab<ToggleOptionMenu>("~ToggleOptionMenu", true);
        }

        [MenuItem("Tools/UI/Button Option Menu", false, 10)]
        [MenuItem("GameObject/UI/Button Option Menu", false, 10)]
        static void CreateButtonOptionMenu()
        {
            PrefabSpawner.SpawnPrefab<ButtonOptionMenu>("~ButtonOptionMenu", true);
        }

        [MenuItem("Tools/UI/Dropdown Option Menu", false, 10)]
        [MenuItem("GameObject/UI/Dropdown Option Menu", false, 10)]
        static void CreateDropdownOptionMenu()
        {
            PrefabSpawner.SpawnPrefab<DropdownOptionMenu>("~DropdownOptionMenu", true);
        }

        [MenuItem("Tools/UI/Input Menu", false, 10)]
        [MenuItem("GameObject/UI/Input Menu", false, 10)]
        static void CreateInputMenu()
        {
            PrefabSpawner.SpawnPrefab<InputMenu>("~InputMenu", true);
        }

        [MenuItem("Tools/UI/Counter", false, 100)]
        [MenuItem("GameObject/UI/Counter", false, 100)]
        static void CreateCounter()
        {
            PrefabSpawner.SpawnPrefab<Counter>("~Counter", true);
        }

        [MenuItem("Tools/UI/Timer", false, 100)]
        [MenuItem("GameObject/UI/Timer", false, 100)]
        static void CreateTimer()
        {
            PrefabSpawner.SpawnPrefab<Timer>("~Timer", true);
        }

        [MenuItem("Tools/UI/Video Playback Controller", false, 200)]
        [MenuItem("GameObject/UI/Video Playback Controller", false, 200)]
        static void CreateVideoPlaybackController()
        {
            PrefabSpawner.SpawnPrefab<VideoPlaybackController>("~VideoPlaybackController", true);
        }

        [MenuItem("Tools/UI/Audio Playback Controller", false, 200)]
        [MenuItem("GameObject/UI/Audio Playback Controller", false, 200)]
        static void CreateAudioPlaybackController()
        {
            PrefabSpawner.SpawnPrefab<AudioPlaybackController>("~AudioPlaybackController", true);
        }

        [MenuItem("Tools/UI/Playable Playback Controller", false, 200)]
        [MenuItem("GameObject/UI/Playable Playback Controller", false, 200)]
        static void CreatePlayablePlaybackController()
        {
            PrefabSpawner.SpawnPrefab<PlayablePlaybackController>("~PlayablePlaybackController", true);
        }

        [MenuItem("Tools/UI/Paginated Text Box", false, 300)]
        [MenuItem("GameObject/UI/Paginated Text Box", false, 300)]
        static void CreatePaginatedTextBox()
        {
            PrefabSpawner.SpawnPrefab<PaginatedTextBox>("~PaginatedTextBox", true, typeof(Text));
        }

        [MenuItem("Tools/UI/Pie Chart", false, 400)]
        [MenuItem("GameObject/UI/Pie Chart", false, 400)]
        public static void AddText(MenuCommand menuCommand)
        {
            GameObject element = PrefabSpawner.SpawnPrefab<PieChart>("~PieChart", true).gameObject;
            MenuOptions.PlaceUIElementRoot(element, menuCommand);
        }
    }

}
#endif