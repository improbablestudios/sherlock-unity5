﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public class UIConstructAnimatorGenerator : AnimatorGenerator
    {
        protected string m_idleStateName;

        public UIConstructAnimatorGenerator(string idleStateName) : base()
        {
            m_idleStateName = idleStateName;
        }

        protected override void GenerateTriggerableTransition(string stateName, AnimatorController controller)
        {
            string idleStateName = m_idleStateName;

            if (!controller.parameters.Any(x => x.type == AnimatorControllerParameterType.Trigger && x.name == stateName))
            {
                controller.AddParameter(stateName, AnimatorControllerParameterType.Trigger);
            }
            AnimationClip animationClip = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(controller)).FirstOrDefault(x => x.name == stateName) as AnimationClip;
            if (animationClip == null)
            {
                animationClip = AnimatorController.AllocateAnimatorClip(stateName);
                AssetDatabase.AddObjectToAsset(animationClip, controller);
            }
            animationClip.wrapMode = WrapMode.Clamp;
            AnimationClipSettings clipSettings = AnimationUtility.GetAnimationClipSettings(animationClip);

            clipSettings.loopTime = (stateName == idleStateName);
            AnimationUtility.SetAnimationClipSettings(animationClip, clipSettings);

            AnimatorStateMachine stateMachine = controller.layers[0].stateMachine;
            AnimatorState destinationState = stateMachine.states.Select(x => x.state).FirstOrDefault(x => x.name == stateName);
            if (destinationState == null)
            {
                destinationState = stateMachine.AddState(stateName);
                destinationState.writeDefaultValues = false;
            }
            if (destinationState.motion != animationClip)
            {
                destinationState.motion = animationClip;
            }
            AnimatorStateTransition anyAnimatorStateTransition = stateMachine.anyStateTransitions.FirstOrDefault(x => x.destinationState == destinationState);
            if (anyAnimatorStateTransition == null)
            {
                anyAnimatorStateTransition = stateMachine.AddAnyStateTransition(destinationState);
            }
            anyAnimatorStateTransition.exitTime = 1f;
            anyAnimatorStateTransition.duration = 0f;
            if (!anyAnimatorStateTransition.conditions.Any(x => x.parameter == stateName))
            {
                anyAnimatorStateTransition.AddCondition(AnimatorConditionMode.If, 0f, stateName);
            }

            AnimatorState idleState = stateMachine.states.Select(x => x.state).FirstOrDefault(x => x.name == idleStateName);
            if (idleState != null)
            {
                if (destinationState != idleState)
                {
                    AnimatorStateTransition idleAnimatorStateTransition = destinationState.transitions.FirstOrDefault(x => x.destinationState == idleState);
                    if (idleAnimatorStateTransition == null)
                    {
                        idleAnimatorStateTransition = destinationState.AddTransition(idleState);
                    }
                    idleAnimatorStateTransition.hasExitTime = true;
                    idleAnimatorStateTransition.exitTime = 1f;
                    idleAnimatorStateTransition.duration = 0f;
                }
            }
        }
    }

}
#endif