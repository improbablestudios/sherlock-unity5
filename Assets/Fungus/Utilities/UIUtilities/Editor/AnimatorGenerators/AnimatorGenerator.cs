﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public class AnimatorGenerator
    {
        private const string k_buttonLabel = "Auto Generate Animation";

        public AnimatorGenerator()
        {
        }

        public bool AutoGenerateAnimationButton(Rect position)
        {
            return GUI.Button(position, k_buttonLabel, EditorStyles.miniButton);
        }

        public void AutoGenerateAnimation(GameObject target, string[] animatorStateNames)
        {
            Animator animator = target.GetComponent<Animator>();
            if (animator == null)
            {
                animator = target.gameObject.AddComponent<Animator>();
            }
            AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;
            if (animatorController == null)
            {
                AnimatorOverrideController animatorOverrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
                if (animatorOverrideController != null)
                {
                    while (animatorOverrideController != null && animatorOverrideController.runtimeAnimatorController != null && animatorController == null)
                    {
                        animatorController = animatorOverrideController.runtimeAnimatorController as AnimatorController;
                        animatorOverrideController = animatorOverrideController.runtimeAnimatorController as AnimatorOverrideController;
                    }
                }
            }
            if (animatorController == null)
            {
                animatorController = GenerateSelectableAnimatorContoller(target, animatorStateNames);
            }
            if (animatorController != null)
            {
                GenerateTriggerableTransitions(animatorController, animatorStateNames);
                animator.runtimeAnimatorController = animatorController;
            }
        }

        private AnimatorController GenerateSelectableAnimatorContoller(GameObject target, string[] animatorStateNames)
        {
            AnimatorController result;
            if (target == null)
            {
                result = null;
            }
            else
            {
                string saveControllerPath = GetSaveControllerPath(target);
                if (string.IsNullOrEmpty(saveControllerPath))
                {
                    result = null;
                }
                else
                {
                    AnimatorController controller = AnimatorController.CreateAnimatorControllerAtPath(saveControllerPath);
                    GenerateTriggerableTransitions(controller, animatorStateNames);
                    AssetDatabase.ImportAsset(saveControllerPath);
                    result = controller;
                }
            }
            return result;
        }

        private string GetSaveControllerPath(GameObject target)
        {
            string name = target.name;
            string message = string.Format("Create a new animator for the game object '{0}':", name);
            return EditorUtility.SaveFilePanelInProject("New Animation Contoller", name, "controller", message);
        }

        protected void GenerateTriggerableTransitions(AnimatorController controller, string[] animatorStateNames)
        {
            if (animatorStateNames != null)
            {
                foreach (string animationStateName in animatorStateNames)
                {
                    GenerateTriggerableTransition(animationStateName, controller);
                }
            }
        }

        protected virtual void GenerateTriggerableTransition(string stateName, AnimatorController controller)
        {
            if (!controller.parameters.Any(x => x.type == AnimatorControllerParameterType.Trigger && x.name == stateName))
            {
                controller.AddParameter(stateName, AnimatorControllerParameterType.Trigger);
            }
            AnimationClip animationClip = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(controller)).FirstOrDefault(x => x.name == stateName) as AnimationClip;
            if (animationClip == null)
            {
                animationClip = AnimatorController.AllocateAnimatorClip(stateName);
                AssetDatabase.AddObjectToAsset(animationClip, controller);
            }
            AnimatorStateMachine stateMachine = controller.layers[0].stateMachine;
            AnimatorState destinationState = stateMachine.states.Select(x => x.state).FirstOrDefault(x => x.name == stateName);
            if (destinationState == null)
            {
                destinationState = stateMachine.AddState(stateName);
            }
            if (destinationState.motion != animationClip)
            {
                destinationState.motion = animationClip;
            }
            AnimatorStateTransition animatorStateTransition = stateMachine.anyStateTransitions.FirstOrDefault(x => x.destinationState == destinationState);
            if (animatorStateTransition == null)
            {
                animatorStateTransition = stateMachine.AddAnyStateTransition(destinationState);
            }
            if (!animatorStateTransition.conditions.Any(x => x.parameter == stateName))
            {
                animatorStateTransition.AddCondition(AnimatorConditionMode.If, 0f, stateName);
            }
        }
    }

}
#endif