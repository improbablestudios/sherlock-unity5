﻿using UnityEngine.EventSystems;
using System.Reflection;

namespace ImprobableStudios.UIUtilities
{

    public static class PointerInputModuleExtensions
    {
        public static PointerEventData GetLastPointerEventData(this PointerInputModule standaloneInputModule)
        {
            return (PointerEventData)typeof(PointerInputModule).GetMethod("GetLastPointerEventData", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(standaloneInputModule, new object[] { -1 });
        }

    }

}
