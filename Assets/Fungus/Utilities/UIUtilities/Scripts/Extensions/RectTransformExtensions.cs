﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public static class RectTransformExtensions
    {
        public static List<RectTransform> GetLayoutRectChildren(this RectTransform rectTransform, List<RectTransform> cachedChildList = null)
        {
            if (cachedChildList == null)
            {
                cachedChildList = new List<RectTransform>();
            }
            else
            {
                cachedChildList.Clear();
            }

            List<Component> toIgnoreList = new List<Component>();
            for (int i = 0; i < rectTransform.childCount; i++)
            {
                var rect = rectTransform.GetChild(i) as RectTransform;
                if (rect == null || !rect.gameObject.activeInHierarchy)
                    continue;

                rect.GetComponents(typeof(ILayoutIgnorer), toIgnoreList);

                if (toIgnoreList.Count == 0)
                {
                    cachedChildList.Add(rect);
                    continue;
                }

                for (int j = 0; j < toIgnoreList.Count; j++)
                {
                    var ignorer = (ILayoutIgnorer)toIgnoreList[j];
                    if (!ignorer.ignoreLayout)
                    {
                        cachedChildList.Add(rect);
                        break;
                    }
                }
            }

            return cachedChildList;
        }

        public static float GetAxisTotalMinSize(this RectTransform rectTransform, int axis, Vector2 spacing, RectOffset padding, List<RectTransform> cachedChildList = null)
        {
            List<RectTransform> rectChildren = (cachedChildList != null) ? cachedChildList : rectTransform.GetLayoutRectChildren();
            if (rectChildren != null)
            {
                float minSize = 0;
                foreach (RectTransform child in rectChildren)
                {
                    minSize += child.GetAxisMinSize(axis);
                }
                minSize += spacing.GetAxisSpacing(axis) * (rectChildren.Count - 1);

                minSize += padding.GetAxisPadding(axis);

                return minSize;
            }

            return 0;
        }

        public static float GetAxisTotalPreferredSize(this LayoutGroup layoutGroup, int axis, Vector2 spacing, RectOffset padding, List<RectTransform> cachedChildList = null)
        {
            List<RectTransform> rectChildren = (cachedChildList != null) ? cachedChildList : layoutGroup.GetRectChildren();

            if (rectChildren != null)
            {
                ResponsiveListLayoutGroup responsiveListLayoutGroup = layoutGroup as ResponsiveListLayoutGroup;

                if ((responsiveListLayoutGroup != null && axis == 0 && responsiveListLayoutGroup.ChildForceEqualWidth)
                    || (responsiveListLayoutGroup != null && axis == 1 && responsiveListLayoutGroup.ChildForceEqualHeight))
                {
                    float largestPreferredSize = -1;

                    foreach (RectTransform child in rectChildren)
                    {
                        float flexible = LayoutUtility.GetFlexibleSize(child, axis);
                        if (flexible == 0)
                        {
                            float size = child.GetAxisPreferredSize(axis);
                            if (size > largestPreferredSize)
                            {
                                largestPreferredSize = size;
                            }
                        }
                    }
                    
                    float preferredSize = 0;

                    foreach (RectTransform child in rectChildren)
                    {
                        float flexible = LayoutUtility.GetFlexibleSize(child, axis);
                        if (flexible == 0)
                        {
                            preferredSize += largestPreferredSize;
                        }
                        else
                        {
                            float size = child.GetAxisPreferredSize(axis);
                            preferredSize += size;
                        }
                    }
                    
                    preferredSize += spacing.GetAxisSpacing(axis) * (rectChildren.Count - 1);

                    preferredSize += padding.GetAxisPadding(axis);

                    return preferredSize;
                }
                else
                {
                    float preferredSize = 0;

                    foreach (RectTransform child in rectChildren)
                    {
                        preferredSize += child.GetAxisPreferredSize(axis);
                    }

                    preferredSize += spacing.GetAxisSpacing(axis) * (rectChildren.Count - 1);

                    preferredSize += padding.GetAxisPadding(axis);

                    return preferredSize;
                }
            }

            return 0;
        }

        public static float GetAxisTotalMinSize(this LayoutGroup layoutGroup, int axis)
        {
            RectTransform rectTransform = layoutGroup.transform as RectTransform;

            if (rectTransform != null)
            {
                List<RectTransform> rectChildren = layoutGroup.GetRectChildren();
                Vector2 spacing = layoutGroup.GetSpacing();
                RectOffset padding = layoutGroup.GetPadding();
                return rectTransform.GetAxisTotalMinSize(axis, spacing, padding, rectChildren);
            }

            return 0;
        }

        public static float GetAxisTotalPreferredSize(this LayoutGroup layoutGroup, int axis)
        {
            RectTransform rectTransform = layoutGroup.transform as RectTransform;

            if (rectTransform != null)
            {
                List<RectTransform> rectChildren = layoutGroup.GetRectChildren();
                Vector2 spacing = layoutGroup.GetSpacing();
                RectOffset padding = layoutGroup.GetPadding();
                return layoutGroup.GetAxisTotalPreferredSize(axis, spacing, padding, rectChildren);
            }

            return 0;
        }

        public static float GetAxisSize(this RectTransform rectTransform, int axis)
        {
            return (axis == 0) ? rectTransform.rect.width : rectTransform.rect.height;
        }

        private static float GetAxisPreferredSize(this RectTransform rectTransform, int axis)
        {
            return (axis == 0) ? LayoutUtility.GetPreferredWidth(rectTransform) : LayoutUtility.GetPreferredHeight(rectTransform);
        }

        private static float GetAxisMinSize(this RectTransform rectTransform, int axis)
        {
            return (axis == 0) ? LayoutUtility.GetMinWidth(rectTransform) : LayoutUtility.GetMinHeight(rectTransform);
        }

        private static float GetAxisSpacing(this Vector2 spacing, int axis)
        {
            return (axis == 0) ? spacing.x : spacing.y;
        }

        private static float GetAxisPadding(this RectOffset padding, int axis)
        {
            return (axis == 0) ? padding.horizontal : padding.vertical;
        }

        private static List<RectTransform> GetRectChildren(this LayoutGroup layoutGroup)
        {
            if (layoutGroup != null)
            {
                ResponsiveListLayoutGroup responsiveListLayoutGroup = layoutGroup as ResponsiveListLayoutGroup;
                if (responsiveListLayoutGroup != null)
                {
                    return responsiveListLayoutGroup.RectChildren;
                }
                ResponsiveGridLayoutGroup responsiveGridLayoutGroup = layoutGroup as ResponsiveGridLayoutGroup;
                if (responsiveGridLayoutGroup != null)
                {
                    return responsiveGridLayoutGroup.RectChildren;
                }
                RectTransform rectTransform = layoutGroup.transform as RectTransform;
                if (rectTransform != null)
                {
                    return rectTransform.GetLayoutRectChildren(null);
                }
            }

            return null;
        }

        private static Vector2 GetSpacing(this LayoutGroup layoutGroup)
        {
            if (layoutGroup != null)
            {
                Vector2 spacing = Vector2.zero;
                HorizontalLayoutGroup horizontalLayoutGroup = layoutGroup as HorizontalLayoutGroup;
                if (horizontalLayoutGroup != null)
                {
                    spacing.x = horizontalLayoutGroup.spacing;
                }
                VerticalLayoutGroup verticalLayoutGroup = layoutGroup as VerticalLayoutGroup;
                if (verticalLayoutGroup != null)
                {
                    spacing.y = verticalLayoutGroup.spacing;
                }
                GridLayoutGroup gridLayoutGroup = layoutGroup as GridLayoutGroup;
                if (gridLayoutGroup != null)
                {
                    spacing = gridLayoutGroup.spacing;
                }
                ResponsiveListLayoutGroup responsiveListLayoutGroup = layoutGroup as ResponsiveListLayoutGroup;
                if (responsiveListLayoutGroup != null)
                {
                    spacing = responsiveListLayoutGroup.Spacing;
                }
                ResponsiveGridLayoutGroup responsiveGridLayoutGroup = layoutGroup as ResponsiveGridLayoutGroup;
                if (responsiveGridLayoutGroup != null)
                {
                    spacing = responsiveGridLayoutGroup.Spacing;
                }
                return spacing;
            }

            return Vector2.zero;
        }

        private static RectOffset GetPadding(this LayoutGroup layoutGroup)
        {
            if (layoutGroup != null)
            {
                return layoutGroup.padding;
            }

            return new RectOffset();
        }
    }

}