﻿using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public static class AnimationExtensions
    {
        public static KeyValuePair<AnimationClip, AnimationClip> GetAnimatorOverridePair(this AnimatorOverrideController animatorOverrideController, string originalClipName)
        {
            List<KeyValuePair<AnimationClip, AnimationClip>> animClips = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            animatorOverrideController.GetOverrides(animClips);
            foreach (KeyValuePair<AnimationClip, AnimationClip> pair in animClips)
            {
                if (pair.Key.name == originalClipName)
                {
                    return pair;
                }
            }
            return new KeyValuePair<AnimationClip, AnimationClip>();
        }

        public static bool SetAnimatorOverridePair(this AnimatorOverrideController animatorOverrideController, string originalClipName, AnimationClip animationClip)
        {
            List<KeyValuePair<AnimationClip, AnimationClip>> animClips = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            animatorOverrideController.GetOverrides(animClips);
            for (int i = 0; i < animClips.Count; i++)
            {
                KeyValuePair<AnimationClip, AnimationClip> pair = animClips[i];
                if (pair.Key.name == originalClipName)
                {
                    animClips[i] = new KeyValuePair<AnimationClip, AnimationClip>(pair.Key, animationClip);
                    animatorOverrideController.ApplyOverrides(animClips);
                    return true;
                }
            }
            return false;
        }

        public static bool UpdateAnimatorOverrides(this AnimatorOverrideController animatorOverrideController, Dictionary<string, AnimationClip> overridesToUpdate)
        {
            bool success = true;
            bool changed = false;
            List<KeyValuePair<AnimationClip, AnimationClip>> animClips = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            animatorOverrideController.GetOverrides(animClips);
            foreach (KeyValuePair<string, AnimationClip> kvp in overridesToUpdate)
            {
                int pairIndex = animClips.FindIndex(i => i.Key.name == kvp.Key);
                if (pairIndex < 0)
                {
                    Debug.LogError(string.Format("Could not find AnimationClip ({0}) in AnimatorOverrideController ({1})", kvp.Key, animatorOverrideController.name));
                    success = false;
                    continue;
                }
                KeyValuePair<AnimationClip, AnimationClip> pair = animClips[pairIndex];
                if (pair.Key != null)
                {
                    if (pair.Value != kvp.Value)
                    {
                        animClips[pairIndex] = new KeyValuePair<AnimationClip, AnimationClip>(pair.Key, kvp.Value);
                        changed = true;
                    }
                }
            }
            if (changed)
            {
                animatorOverrideController.ApplyOverrides(animClips);
            }
            return success;
        }
    }

}