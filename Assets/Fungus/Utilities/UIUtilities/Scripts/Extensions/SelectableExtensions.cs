﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace ImprobableStudios.UIUtilities
{
    
    public static class SelectableExtensions
    {
        public static void DoStateTransition(this Selectable selectable, int state, bool instant)
        {
            typeof(Selectable).GetMethod("DoStateTransition", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(selectable, new object[] { (int)state, instant });
        }

        public static bool HasSelection(this Selectable selectable)
        {
            return (bool)typeof(Selectable).GetProperty("hasSelection", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(selectable, null);
        }

        public static void SetSelection(this Selectable selectable, bool value)
        {
            typeof(Selectable).GetProperty("hasSelection", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(selectable, value);
        }

        public static int GetCurrentSelectionState(this Selectable selectable)
        {
            return (int)typeof(Selectable).GetProperty("currentSelectionState", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(selectable);
        }

        public static void SetToggleGroupWithoutNotify(this Toggle toggle, ToggleGroup newGroup, bool setMemberValue)
        {
            FieldInfo groupField = typeof(Toggle).GetField("m_Group", BindingFlags.Instance | BindingFlags.NonPublic);
            ToggleGroup currentGroup = groupField.GetValue(toggle) as ToggleGroup;
            if (currentGroup != null)
                currentGroup.UnregisterToggleWithoutNotify(toggle);
            if (setMemberValue)
                groupField.SetValue(toggle, newGroup);
            if (newGroup != null && toggle.IsActive())
                newGroup.RegisterToggleWithoutNotify(toggle);
            if (newGroup == null || !toggle.isOn || !toggle.IsActive())
                return;
        }

        public static void UnregisterToggleWithoutNotify(this ToggleGroup toggleGroup, Toggle toggle)
        {
            FieldInfo togglesField = typeof(ToggleGroup).GetField("m_Toggles", BindingFlags.Instance | BindingFlags.NonPublic);
            List<Toggle> toggles = togglesField.GetValue(toggleGroup) as List<Toggle>;
            if (toggles.Contains(toggle))
                toggles.Remove(toggle);
            if (toggleGroup.allowSwitchOff || toggleGroup.AnyTogglesOn() || toggles.Count == 0)
                return;
        }

        public static void RegisterToggleWithoutNotify(this ToggleGroup toggleGroup, Toggle toggle)
        {
            FieldInfo togglesField = typeof(ToggleGroup).GetField("m_Toggles", BindingFlags.Instance | BindingFlags.NonPublic);
            List<Toggle> toggles = togglesField.GetValue(toggleGroup) as List<Toggle>;
            if (!toggles.Contains(toggle))
                toggles.Add(toggle);
            if (toggleGroup.allowSwitchOff || toggleGroup.AnyTogglesOn())
                return;
        }

        public static Vector3 GetCenter(this RectTransform rectTransform)
        {
            Vector3[] worldCorners = new Vector3[4];
            rectTransform.GetWorldCorners(worldCorners);

            Vector3 points = Vector3.zero;
            foreach (Vector3 point in worldCorners)
            {
                points += point;
            }
            return (points / 4);
        }
        
        public static void UpdateSelectableNavigation(this Selectable selectable, Selectable[][] selectables, Navigation navigation)
        {
            if (navigation.mode != Navigation.Mode.None)
            {
                Navigation currentNav = selectable.navigation;

                if (currentNav.mode != Navigation.Mode.None)
                {
                    currentNav = new Navigation();
                    currentNav.mode = Navigation.Mode.Explicit;

                    for (int i = 0; i < selectables.Length; i++)
                    {
                        Selectable[] selectableArray = selectables[i];
                        if (navigation.mode == Navigation.Mode.Explicit)
                        {
                            if (currentNav.selectOnLeft == null)
                            {
                                currentNav.selectOnLeft = navigation.selectOnLeft;
                            }
                            if (currentNav.selectOnRight == null)
                            {
                                currentNav.selectOnRight = navigation.selectOnRight;
                            }
                            if (currentNav.selectOnUp == null)
                            {
                                currentNav.selectOnUp = navigation.selectOnUp;
                            }
                            if (currentNav.selectOnDown == null)
                            {
                                currentNav.selectOnDown = navigation.selectOnDown;
                            }
                        }
                        else if (navigation.mode == Navigation.Mode.Automatic)
                        {
                            if (currentNav.selectOnLeft == null)
                            {
                                currentNav.selectOnLeft = selectable.FindNearestSelectable(selectableArray, Vector3.left);
                            }
                            if (currentNav.selectOnRight == null)
                            {
                                currentNav.selectOnRight = selectable.FindNearestSelectable(selectableArray, Vector3.right);
                            }
                            if (currentNav.selectOnUp == null)
                            {
                                currentNav.selectOnUp = selectable.FindNearestSelectable(selectableArray, Vector3.up);
                            }
                            if (currentNav.selectOnDown == null)
                            {
                                currentNav.selectOnDown = selectable.FindNearestSelectable(selectableArray, Vector3.down);
                            }
                        }
                        else if (navigation.mode == Navigation.Mode.Vertical)
                        {
                            if (currentNav.selectOnUp == null)
                            {
                                currentNav.selectOnUp = selectable.FindNearestSelectable(selectableArray, Vector3.up);
                            }
                            if (currentNav.selectOnDown == null)
                            {
                                currentNav.selectOnDown = selectable.FindNearestSelectable(selectableArray, Vector3.down);
                            }
                            if (i == 0)
                            {
                                if (currentNav.selectOnLeft == null)
                                {
                                    currentNav.selectOnLeft = selectable.FindNearestSelectable(selectableArray, Vector3.left);
                                }
                                if (currentNav.selectOnRight == null)
                                {
                                    currentNav.selectOnRight = selectable.FindNearestSelectable(selectableArray, Vector3.right);
                                }
                            }
                        }
                        else if (navigation.mode == Navigation.Mode.Horizontal)
                        {
                            if (currentNav.selectOnLeft == null)
                            {
                                currentNav.selectOnLeft = selectable.FindNearestSelectable(selectableArray, Vector3.left);
                            }
                            if (currentNav.selectOnRight == null)
                            {
                                currentNav.selectOnRight = selectable.FindNearestSelectable(selectableArray, Vector3.right);
                            }
                            if (i == 0)
                            {
                                if (currentNav.selectOnUp == null)
                                {
                                    currentNav.selectOnUp = selectable.FindNearestSelectable(selectableArray, Vector3.up);
                                }
                                if (currentNav.selectOnDown == null)
                                {
                                    currentNav.selectOnDown = selectable.FindNearestSelectable(selectableArray, Vector3.down);
                                }
                            }
                        }
                    }

                    selectable.navigation = currentNav;
                }
            }
        }

        public static Selectable FindNearestSelectable(this Selectable selectable, Selectable[] targetSelectables, Vector3 dir)
        {
            dir = dir.normalized;
            Vector3 localDir = Quaternion.Inverse(selectable.transform.rotation) * dir;
            Vector3 pos = selectable.transform.TransformPoint(GetPointOnRectEdge(selectable.transform as RectTransform, localDir));
            float maxScore = Mathf.NegativeInfinity;
            Selectable bestPick = null;
            
            for (int i = 0; i < targetSelectables.Length; ++i)
            {
                Selectable sel = targetSelectables[i];
                bool includeNonInteractable = sel.gameObject.GetComponent<AllowSelectionWhenDisabled>() != null;
                if (sel == selectable || sel == null)
                    continue;

                if (!sel.isActiveAndEnabled || (!sel.IsInteractable() && !includeNonInteractable) || sel.navigation.mode == Navigation.Mode.None)
                    continue;

#if UNITY_EDITOR
                // Apart from runtime use, FindSelectable is used by custom editors to
                // draw arrows between different selectables. For scene view cameras,
                // only selectables in the same stage should be considered.
                if (Camera.current != null && !UnityEditor.SceneManagement.StageUtility.IsGameObjectRenderedByCamera(sel.gameObject, Camera.current))
                    continue;
#endif

                var selRect = sel.transform as RectTransform;
                Vector3 selCenter = selRect != null ? (Vector3)selRect.rect.center : Vector3.zero;
                Vector3 myVector = sel.transform.TransformPoint(selCenter) - pos;

                // Value that is the distance out along the direction.
                float dot = Vector3.Dot(dir, myVector);

                // Skip elements that are in the wrong direction or which have zero distance.
                // This also ensures that the scoring formula below will not have a division by zero error.
                if (dot <= 0)
                    continue;

                // This scoring function has two priorities:
                // - Score higher for positions that are closer.
                // - Score higher for positions that are located in the right direction.
                // This scoring function combines both of these criteria.
                // It can be seen as this:
                //   Dot (dir, myVector.normalized) / myVector.magnitude
                // The first part equals 1 if the direction of myVector is the same as dir, and 0 if it's orthogonal.
                // The second part scores lower the greater the distance is by dividing by the distance.
                // The formula below is equivalent but more optimized.
                //
                // If a given score is chosen, the positions that evaluate to that score will form a circle
                // that touches pos and whose center is located along dir. A way to visualize the resulting functionality is this:
                // From the position pos, blow up a circular balloon so it grows in the direction of dir.
                // The first Selectable whose center the circular balloon touches is the one that's chosen.
                float score = dot / myVector.sqrMagnitude;

                if (score > maxScore)
                {
                    maxScore = score;
                    bestPick = sel;
                }
            }
            return bestPick;
        }

        private static Vector3 GetPointOnRectEdge(RectTransform rect, Vector2 dir)
        {
            if (rect == null)
                return Vector3.zero;
            if (dir != Vector2.zero)
                dir /= Mathf.Max(Mathf.Abs(dir.x), Mathf.Abs(dir.y));
            dir = rect.rect.center + Vector2.Scale(rect.rect.size, dir * 0.5f);
            return dir;
        }
    }

}