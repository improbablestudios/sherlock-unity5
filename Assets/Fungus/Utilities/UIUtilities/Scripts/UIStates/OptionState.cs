﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class OptionState : MenuItemState
    {
        [SerializeField]
        protected string m_OptionText;

        [SerializeField]
        protected Dictionary<string, string> m_OptionLocalizedDictionary;

        [SerializeField]
        protected Sprite m_Icon;

        public string OptionText
        {
            get
            {
                return m_OptionText;
            }
        }

        public Dictionary<string, string> OptionLocalizedDictionary
        {
            get
            {
                return m_OptionLocalizedDictionary;
            }
        }

        public Sprite Icon
        {
            get
            {
                return m_Icon;
            }
        }

        public OptionState()
        {
        }

        public OptionState(
            Dictionary<string, string> optionLocalizedDictionary,
            string optionText,
            Sprite icon) : base()
        {
            m_OptionLocalizedDictionary = optionLocalizedDictionary;
            m_OptionText = optionText;
            m_Icon = icon;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", m_OptionText, m_Icon);
        }
    }


}