﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class InputState : MenuItemState
    {
        [SerializeField]
        protected string m_PlaceholderText;

        [SerializeField]
        protected Dictionary<string, string> m_PlaceholderLocalizedDictionary;

        [SerializeField]
        protected InputField.ContentType m_ContentType;

        [Min(0)]
        [SerializeField]
        protected int m_CharacterMinimum;

        [Min(0)]
        [SerializeField]
        protected int m_CharacterLimit = 0;

        public string PlaceholderText
        {
            get
            {
                return m_PlaceholderText;
            }
        }

        public Dictionary<string, string> PlaceholderLocalizedDictionary
        {
            get
            {
                return m_PlaceholderLocalizedDictionary;
            }
        }

        public InputField.ContentType ContentType
        {
            get
            {
                return m_ContentType;
            }
        }

        public int CharacterMinimum
        {
            get
            {
                return m_CharacterMinimum;
            }
        }

        public int CharacterLimit
        {
            get
            {
                return m_CharacterLimit;
            }
        }

        public InputState()
        {
        }
        
        public InputState(
            Dictionary<string, string> placeholderLocalizedDictionary,
            string placeholderText,
            InputField.ContentType contentType,
            int characterMinimum,
            int characterLimit) : base()
        {
            m_PlaceholderLocalizedDictionary = placeholderLocalizedDictionary;
            m_PlaceholderText = placeholderText;
            m_ContentType = contentType;
            m_CharacterMinimum = characterMinimum;
            m_CharacterLimit = characterLimit;
        }
    }


}