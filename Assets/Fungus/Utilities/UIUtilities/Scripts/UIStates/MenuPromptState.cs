﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class MenuPromptState
    {
        [SerializeField]
        protected string m_TitleText;

        [SerializeField]
        protected Dictionary<string, string> m_TitleLocalizedDictionary;

        [SerializeField]
        protected string m_MessageText;

        [SerializeField]
        protected Dictionary<string, string> m_MessageLocalizedDictionary;

        [SerializeField]
        protected Sprite m_Icon;

        public string TitleText
        {
            get
            {
                return m_TitleText;
            }
            set
            {
                m_TitleText = value;
            }
        }

        public Dictionary<string, string> TitleLocalizedDictionary
        {
            get
            {
                return m_TitleLocalizedDictionary;
            }
            set
            {
                m_TitleLocalizedDictionary = value;
            }
        }

        public string MessageText
        {
            get
            {
                return m_MessageText;
            }
            set
            {
                m_MessageText = value;
            }
        }

        public Dictionary<string, string> MessageLocalizedDictionary
        {
            get
            {
                return m_MessageLocalizedDictionary;
            }
            set
            {
                m_MessageLocalizedDictionary = value;
            }
        }

        public Sprite Icon
        {
            get
            {
                return m_Icon;
            }
            set
            {
                m_Icon = value;
            }
        }

        public MenuPromptState()
        {
        }

        public MenuPromptState(
            Dictionary<string, string> titleLocalizedDictionary,
            string titleText,
            Dictionary<string, string> messageLocalizedDictionary,
            string messageText,
            Sprite icon) : base()
        {
            m_TitleLocalizedDictionary = titleLocalizedDictionary;
            m_TitleText = titleText;
            m_MessageLocalizedDictionary = messageLocalizedDictionary;
            m_MessageText = messageText;
            m_Icon = icon;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", m_TitleText, m_MessageText, m_Icon);
        }
    }


}