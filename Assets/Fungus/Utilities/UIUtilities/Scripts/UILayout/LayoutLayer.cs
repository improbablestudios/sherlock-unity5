﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("Layout/Layout Layer", 152)]
    public class LayoutLayer : UIBehaviour, ILayoutSelfController, ILayoutIgnorer
    {
        private LayoutGroup m_parentLayoutGroup;

        public LayoutGroup ParentLayoutGroup
        {
            get
            {
                if (m_parentLayoutGroup == null && transform.parent != null)
                {
                    m_parentLayoutGroup = transform.parent.GetComponent<LayoutGroup>();
                }
                return m_parentLayoutGroup;
            }
        }

        public bool ignoreLayout => true;

        public virtual void SetLayoutHorizontal()
        {
            UpdateRectTransform();
        }

        public virtual void SetLayoutVertical()
        {
            UpdateRectTransform();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            UpdateRectTransform();
        }

        void UpdateRectTransform()
        {
            RectTransform rectTransform = GetComponent<RectTransform>();

            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
            if (ParentLayoutGroup != null)
            {
                rectTransform.sizeDelta = new Vector2(ParentLayoutGroup.preferredWidth, ParentLayoutGroup.preferredHeight);
            }
        }
    }

}