﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Layout/Responsive Grid Layout Group", 152)]
    public class ResponsiveGridLayoutGroup : ResponsiveLayoutGroup
    {
        public enum Corner
        {
            UpperLeft = 0,
            UpperRight = 1,
            LowerLeft = 2,
            LowerRight = 3
        }

        public enum GridSizeConstraint
        {
            Flexible = 0,
            FixedColumnCount = 1,
            FixedRowCount = 2
        }

        public enum CellSizeConstraint
        {
            Largest = 0,
            Explicit = 1,
        }

        [SerializeField]
        [Tooltip("Which corner should the first cell be placed in?")]
        protected Corner m_StartCorner = Corner.UpperLeft;

        [SerializeField]
        [Tooltip("Constrain the grid to a fixed number of rows or columns")]
        protected GridSizeConstraint m_GridConstraint;

        [SerializeField]
        [Tooltip("The number of columns")]
        [Min(1)]
        protected int m_ColumnCount = 1;

        [SerializeField]
        [Tooltip("The number of rows")]
        [Min(1)]
        protected int m_RowCount = 1;

        [SerializeField]
        [Tooltip("Constrain the grid to a fixed number of rows or columns")]
        protected CellSizeConstraint m_CellWidthConstraint;

        [SerializeField]
        [Tooltip("The width of a cell")]
        [Min(0)]
        protected float m_CellWidth = 100f;

        [SerializeField]
        [Tooltip("Constrain the grid to a fixed number of rows or columns")]
        protected CellSizeConstraint m_CellHeightConstraint;

        [SerializeField]
        [Tooltip("The width of a cell")]
        [Min(0)]
        protected float m_CellHeight = 100f;
        
        protected float m_minCellWidth;
        protected float m_minCellHeight;
        protected float m_preferredCellWidth;
        protected float m_preferredCellHeight;
        protected float m_flexibleCellWidth;
        protected float m_flexibleCellHeight;
        protected float m_widthMinMaxLerp;
        protected float m_heightMinMaxLerp;

        public Corner StartCorner
        {
            get
            {
                return m_StartCorner;
            }
            set
            {
                SetProperty(ref m_StartCorner, value);
            }
        }

        public GridSizeConstraint GridConstraint
        {
            get
            {
                return m_GridConstraint;
            }
            set
            {
                m_GridConstraint = value;
            }
        }

        public CellSizeConstraint CellWidthConstraint
        {
            get
            {
                return m_CellWidthConstraint;
            }
            set
            {
                m_CellWidthConstraint = value;
            }
        }

        public CellSizeConstraint CellHeightConstraint
        {
            get
            {
                return m_CellHeightConstraint;
            }
            set
            {
                m_CellHeightConstraint = value;
            }
        }

        public int RowCount
        {
            get
            {
                return m_RowCount;
            }
            set
            {
                m_RowCount = Math.Max(1, value);
            }
        }

        public int ColumnCount
        {
            get
            {
                return m_ColumnCount;
            }
            set
            {
                m_ColumnCount = Math.Max(1, value);
            }
        }
        
        public float CellWidth
        {
            get
            {
                return m_CellWidth;
            }
            set
            {
                m_CellWidth = Math.Max(0, value);
            }
        }

        public float CellHeight
        {
            get
            {
                return m_CellHeight;
            }
            set
            {
                m_CellHeight = Math.Max(0, value);
            }
        }

        public List<RectTransform> RectChildren
        {
            get
            {
                return rectChildren;
            }
        }
        
        protected virtual int GetPreferredColumnCount(float cellWidth, float cellHeight)
        {
            if (m_StartAxis == Axis.Horizontal)
            {
                if (m_GridConstraint == GridSizeConstraint.FixedColumnCount)
                {
                    return m_ColumnCount;
                }
                else if (m_GridConstraint == GridSizeConstraint.FixedRowCount)
                {
                    return Mathf.CeilToInt(rectChildren.Count / (float)m_RowCount - 0.001f);
                }
                else
                {
                    float width = rectTransform.rect.size.x;
                    int columnCount = Mathf.Max(1, Mathf.FloorToInt((width - padding.horizontal + m_Spacing.x + 0.001f) / (cellWidth + m_Spacing.x)));
                    return columnCount;
                }
            }
            else
            {
                if (m_GridConstraint == GridSizeConstraint.FixedRowCount)
                {
                    return Mathf.CeilToInt(rectChildren.Count / (float)m_RowCount - 0.001f);
                }
                else if (m_GridConstraint == GridSizeConstraint.FixedColumnCount)
                {
                    return m_ColumnCount;
                }
                else
                {
                    float height = rectTransform.rect.size.y;
                    int rowCount = Mathf.Max(1, Mathf.FloorToInt((height - padding.vertical + m_Spacing.y + 0.001f) / (cellHeight + m_Spacing.y)));
                    return Mathf.CeilToInt(rectChildren.Count / (float)rowCount);
                }
            }
        }
        
        protected virtual int GetPreferredRowCount(float cellWidth, float cellHeight)
        {
            if (m_StartAxis == Axis.Horizontal)
            {
                if (m_GridConstraint == GridSizeConstraint.FixedColumnCount)
                {
                    return Mathf.CeilToInt(rectChildren.Count / (float)m_ColumnCount - 0.001f);
                }
                else if (m_GridConstraint == GridSizeConstraint.FixedRowCount)
                {
                    return m_RowCount;
                }
                else
                {
                    float width = rectTransform.rect.size.x;
                    int columnCount = Mathf.Max(1, Mathf.FloorToInt((width - padding.horizontal + m_Spacing.x + 0.001f) / (cellWidth + m_Spacing.x)));
                    return Mathf.CeilToInt(rectChildren.Count / (float)columnCount);
                }
            }
            else
            {
                if (m_GridConstraint == GridSizeConstraint.FixedRowCount)
                {
                    return m_RowCount;
                }
                else if (m_GridConstraint == GridSizeConstraint.FixedColumnCount)
                {
                    return Mathf.CeilToInt(rectChildren.Count / (float)m_ColumnCount - 0.001f);
                }
                else
                {
                    float height = rectTransform.rect.size.y;
                    int rowCount = Mathf.Max(1, Mathf.FloorToInt((height - padding.vertical + m_Spacing.y + 0.001f) / (cellHeight + m_Spacing.y)));
                    return rowCount;
                }
            }
        }
        
        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();

            switch (m_CellWidthConstraint)
            {
                case CellSizeConstraint.Largest:
                    m_minCellWidth = GetLargestMinSize(true, (int)Axis.Horizontal);
                    m_preferredCellWidth = GetLargestPreferredSize(true, (int)Axis.Horizontal);
                    m_flexibleCellWidth = GetLargestFlexibleSize(true, (int)Axis.Horizontal);
                    break;
                case CellSizeConstraint.Explicit:
                    m_minCellWidth = m_preferredCellWidth = m_CellWidth;
                    m_flexibleCellWidth = -1;
                    break;
            }

            switch (m_CellHeightConstraint)
            {
                case CellSizeConstraint.Largest:
                    m_minCellHeight = GetLargestMinSize(true, (int)Axis.Vertical);
                    m_preferredCellHeight = GetLargestPreferredSize(true, (int)Axis.Vertical);
                    m_flexibleCellHeight = GetLargestFlexibleSize(true, (int)Axis.Vertical);
                    break;
                case CellSizeConstraint.Explicit:
                    m_minCellHeight = m_preferredCellHeight = m_CellHeight;
                    m_flexibleCellHeight = -1;
                    break;
            }
            
            float totalMin = 0;
            float totalPreferred = 0;
            float totalFlexible = 0;
            
            int preferredColumnCount = GetPreferredColumnCount(m_preferredCellWidth, m_preferredCellHeight);
            totalMin = padding.horizontal + (m_minCellWidth + m_Spacing.x) * preferredColumnCount - m_Spacing.x;
            totalPreferred = padding.horizontal + (m_preferredCellWidth + m_Spacing.x) * preferredColumnCount - m_Spacing.x;
            totalFlexible = m_flexibleCellWidth;
            
            SetLayoutInputForAxis(totalMin, totalPreferred, totalFlexible, 0);
        }

        public override void CalculateLayoutInputVertical()
        {
            switch (m_CellWidthConstraint)
            {
                case CellSizeConstraint.Largest:
                    m_minCellWidth = GetLargestMinSize(true, (int)Axis.Horizontal);
                    m_preferredCellWidth = GetLargestPreferredSize(true, (int)Axis.Horizontal);
                    m_flexibleCellWidth = GetLargestFlexibleSize(true, (int)Axis.Horizontal);
                    break;
                case CellSizeConstraint.Explicit:
                    m_minCellWidth = m_preferredCellWidth = m_CellWidth;
                    m_flexibleCellWidth = -1;
                    break;
            }

            switch (m_CellHeightConstraint)
            {
                case CellSizeConstraint.Largest:
                    m_minCellHeight = GetLargestMinSize(true, (int)Axis.Vertical);
                    m_preferredCellHeight = GetLargestPreferredSize(true, (int)Axis.Vertical);
                    m_flexibleCellHeight = GetLargestFlexibleSize(true, (int)Axis.Vertical);
                    break;
                case CellSizeConstraint.Explicit:
                    m_minCellHeight = m_preferredCellHeight = m_CellHeight;
                    m_flexibleCellHeight = -1;
                    break;
            }
            
            float totalMin = 0;
            float totalPreferred = 0;
            float totalFlexible = 0;
            
            int preferredRowCount = GetPreferredRowCount(m_preferredCellWidth, m_preferredCellHeight);
            totalMin = padding.vertical + (m_minCellHeight + m_Spacing.y) * preferredRowCount - m_Spacing.y;
            totalPreferred = padding.vertical + (m_preferredCellHeight + m_Spacing.y) * preferredRowCount - m_Spacing.y;
            totalFlexible = m_flexibleCellHeight;
            
            SetLayoutInputForAxis(totalMin, totalPreferred, totalFlexible, 1);
        }

        public override void SetLayoutHorizontal()
        {
            if (rectTransform.rect.width > 0)
            {
                m_widthMinMaxLerp = GetMinMaxLerp(0);

                float totalWidth = GetTotalSize(0, m_widthMinMaxLerp);
                float totalHeight = GetTotalSize(1, m_heightMinMaxLerp);
                float cellWidth = GetCellSize(m_minCellWidth, m_preferredCellWidth, m_widthMinMaxLerp);
                float cellHeight = GetCellSize(m_minCellHeight, m_preferredCellHeight, m_heightMinMaxLerp);

                int cornerX, cornerY, cellsPerMainAxis, actualCellCountX, actualCellCountY;
                GetActualCellCounts(padding, m_Spacing, totalWidth, totalHeight, cellWidth, cellHeight, out cornerX, out cornerY, out cellsPerMainAxis, out actualCellCountX, out actualCellCountY);

                if (GetTotalFlexibleSize(0) > 0)
                {
                    totalWidth = rectTransform.rect.size[0];
                    cellWidth = Math.Max(m_minCellWidth, (totalWidth - padding.horizontal - (actualCellCountX - 1) * m_Spacing.x) / actualCellCountX);
                }
                if (GetTotalFlexibleSize(1) > 0)
                {
                    totalHeight = rectTransform.rect.size[1];
                    cellHeight = Math.Max(m_minCellHeight, (totalHeight - padding.vertical - (actualCellCountY - 1) * m_Spacing.y) / actualCellCountY);
                }

                SetCellsAlongAxis(0, padding, m_Spacing, totalWidth, totalHeight, cellWidth, cellHeight);
            }
        }

        public override void SetLayoutVertical()
        {
            if (rectTransform.rect.height > 0)
            {
                m_heightMinMaxLerp = GetMinMaxLerp(1);

                float cellWidth = GetCellSize(m_minCellWidth, m_preferredCellWidth, m_widthMinMaxLerp);
                float cellHeight = GetCellSize(m_minCellHeight, m_preferredCellHeight, m_heightMinMaxLerp);
                float totalWidth = GetTotalSize(0, m_widthMinMaxLerp);
                float totalHeight = GetTotalSize(1, m_heightMinMaxLerp);

                int cornerX, cornerY, cellsPerMainAxis, actualCellCountX, actualCellCountY;
                GetActualCellCounts(padding, m_Spacing, totalWidth, totalHeight, cellWidth, cellHeight, out cornerX, out cornerY, out cellsPerMainAxis, out actualCellCountX, out actualCellCountY);

                if (GetTotalFlexibleSize(0) > 0)
                {
                    cellWidth = Math.Max(m_minCellWidth, (rectTransform.rect.size[0] - padding.horizontal - (actualCellCountX - 1) * m_Spacing.x) / actualCellCountX);
                    totalWidth = rectTransform.rect.size[0];
                }
                if (GetTotalFlexibleSize(1) > 0)
                {
                    cellHeight = Math.Max(m_minCellHeight, (rectTransform.rect.size[1] - padding.vertical - (actualCellCountY - 1) * m_Spacing.y) / actualCellCountY);
                    totalHeight = rectTransform.rect.size[1];
                }

                SetCellsAlongAxis(1, padding, m_Spacing, totalWidth, totalHeight, cellWidth, cellHeight);
            }
        }

        float GetMinMaxLerp(int axis)
        {
            return GetMinMaxLerp(rectTransform.rect.size[axis], GetTotalMinSize(axis), GetTotalPreferredSize(axis));
        }

        float GetMinMaxLerp(float size, float minSize, float preferredSize)
        {
            float minMaxLerp = 0;
            if (minSize != preferredSize)
            {
                minMaxLerp = Mathf.Clamp01((size - minSize) / (preferredSize - minSize));
            }

            return minMaxLerp;
        }
        
        float GetFlexibleMultiplier(float size, float flexibleSize, float preferredSize)
        {
            float itemFlexibleMultiplier = 0;
            if (size > preferredSize)
            {
                if (flexibleSize > 0)
                {
                    itemFlexibleMultiplier = (size - preferredSize) / flexibleSize;
                }
            }

            return itemFlexibleMultiplier;
        }
        
        float GetCellSize(float minSize, float preferredSize, float minMaxLerp)
        {
            return Mathf.Lerp(minSize, preferredSize, minMaxLerp);
        }
        
        float GetTotalSize(int axis, float minMaxLerp)
        {
            if (m_GridConstraint == GridSizeConstraint.Flexible)
            {
                float size = rectTransform.rect.size[axis];
                return size;
            }
            else
            {
                float totalMinSize = GetTotalMinSize(axis);
                float totalPreferredSize = GetTotalPreferredSize(axis);
                return Mathf.Lerp(totalMinSize, totalPreferredSize, minMaxLerp);
            }
        }
        
        private int GetCellCount(float padding, float spacing, float totalSize, float cellSize)
        {
            if (cellSize + spacing <= 0)
            {
                return int.MaxValue;
            }
            else
            {
                return Mathf.Max(1, Mathf.FloorToInt((totalSize - padding + spacing + 0.001f) / (cellSize + spacing)));
            }
        }

        private void GetActualCellCounts(RectOffset padding, Vector2 spacing, float totalWidth, float totalHeight, float cellWidth, float cellHeight, out int cornerX, out int cornerY, out int cellsPerMainAxis, out int actualCellCountX, out int actualCellCountY)
        {
            int cellCountX = GetCellCount(padding.horizontal, spacing.x, totalWidth, cellWidth);
            int cellCountY = GetCellCount(padding.vertical, spacing.y, totalHeight, cellHeight);

            cornerX = (int)m_StartCorner % 2;
            cornerY = (int)m_StartCorner / 2;

            if (m_StartAxis == Axis.Horizontal)
            {
                cellsPerMainAxis = cellCountX;
                actualCellCountX = Mathf.Clamp(cellCountX, 1, rectChildren.Count);
                actualCellCountY = Mathf.Clamp(cellCountY, 1, Mathf.CeilToInt(rectChildren.Count / (float)cellsPerMainAxis));
            }
            else
            {
                cellsPerMainAxis = cellCountY;
                actualCellCountY = Mathf.Clamp(cellCountY, 1, rectChildren.Count);
                actualCellCountX = Mathf.Clamp(cellCountX, 1, Mathf.CeilToInt(rectChildren.Count / (float)cellsPerMainAxis));
            }
        }

        private void SetCellsAlongAxis(int axis, RectOffset padding, Vector2 spacing, float totalWidth, float totalHeight, float cellWidth, float cellHeight)
        {
            // Normally a Layout Controller should only set horizontal values when invoked for the horizontal axis
            // and only vertical values when invoked for the vertical axis.
            // However, in this case we set both the horizontal and vertical position when invoked for the vertical axis.
            // Since we only set the horizontal position and not the size, it shouldn't affect children's layout,
            // and thus shouldn't break the rule that all horizontal layout must be calculated before all vertical layout.

            if (axis == 0)
            {
                // Only set the sizes when invoked for horizontal axis, not the positions.
                for (int i = 0; i < rectChildren.Count; i++)
                {
                    RectTransform rect = rectChildren[i];

                    m_Tracker.Add(this, rect,
                        DrivenTransformProperties.Anchors |
                        DrivenTransformProperties.AnchoredPosition |
                        DrivenTransformProperties.SizeDelta);

                    rect.anchorMin = Vector2.up;
                    rect.anchorMax = Vector2.up;
                    rect.sizeDelta = new Vector2(cellWidth, cellHeight);
                }
                return;
            }
            
            int cornerX, cornerY, cellsPerMainAxis, actualCellCountX, actualCellCountY;
            GetActualCellCounts(padding, m_Spacing, totalWidth, totalHeight, cellWidth, cellHeight, out cornerX, out cornerY, out cellsPerMainAxis, out actualCellCountX, out actualCellCountY);

            Vector2 requiredSpace = new Vector2(
                    actualCellCountX * cellWidth + (actualCellCountX - 1) * spacing.x,
                    actualCellCountY * cellHeight + (actualCellCountY - 1) * spacing.y
                    );
            Vector2 startOffset = new Vector2(
                    GetStartOffset(0, requiredSpace.x),
                    GetStartOffset(1, requiredSpace.y)
                    );

            for (int i = 0; i < rectChildren.Count; i++)
            {
                int positionX;
                int positionY;
                if (m_StartAxis == Axis.Horizontal)
                {
                    positionX = i % cellsPerMainAxis;
                    positionY = i / cellsPerMainAxis;
                }
                else
                {
                    positionX = i / cellsPerMainAxis;
                    positionY = i % cellsPerMainAxis;
                }

                if (cornerX == 1)
                    positionX = actualCellCountX - 1 - positionX;
                if (cornerY == 1)
                    positionY = actualCellCountY - 1 - positionY;

                SetChildAlongAxis(rectChildren[i], 0, startOffset.x + (cellWidth + spacing.x) * positionX, cellWidth);
                SetChildAlongAxis(rectChildren[i], 1, startOffset.y + (cellHeight + spacing.y) * positionY, cellHeight);
            }
        }
    }

}