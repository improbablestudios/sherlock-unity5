﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public interface IHasLayoutBreakpoint
    {
        bool HasAdjustedHorizontalLayout();
        bool HasAdjustedVerticalLayout();
        int GetLayoutPriority();
        UIBehaviour GetComponent();
    }

    public static class IHasLayoutBreakpointExtensions
    {
        public static IHasLayoutBreakpoint[] GetOrderedIHasLayoutBreakpoint(this Component component)
        {
            IHasLayoutBreakpoint[] hasLayoutBreakpoints = component.GetComponentsInParent<IHasLayoutBreakpoint>(true);
            return hasLayoutBreakpoints.OrderBy(x => x.GetLayoutPriority()).ThenBy(x => Array.IndexOf(hasLayoutBreakpoints, x)).ToArray();
        }

        public static bool AllHigherPrioritiesHaveAdjustedHorizontalLayout(this IHasLayoutBreakpoint hasLayoutBreakpoint, IHasLayoutBreakpoint[] orderedhasLayoutBreakpoints)
        {
            foreach (IHasLayoutBreakpoint ihlb in orderedhasLayoutBreakpoints)
            {
                if (ihlb.GetComponent().isActiveAndEnabled && ihlb.GetLayoutPriority() < hasLayoutBreakpoint.GetLayoutPriority() && !ihlb.HasAdjustedHorizontalLayout())
                {
                    return false;
                }
            }
            return true;
        }
        public static bool AllHigherPrioritiesHaveAdjustedVerticalLayout(this IHasLayoutBreakpoint hasLayoutBreakpoint, IHasLayoutBreakpoint[] orderedhasLayoutBreakpoints)
        {
            foreach (IHasLayoutBreakpoint ihlb in orderedhasLayoutBreakpoints)
            {
                if (ihlb != hasLayoutBreakpoint && ihlb.GetComponent().isActiveAndEnabled && ihlb.GetLayoutPriority() < hasLayoutBreakpoint.GetLayoutPriority() && !ihlb.HasAdjustedVerticalLayout())
                {
                    return false;
                }
            }
            return true;
        }
    }

    public enum LayoutBreakpointMode
    {
        None = 0,
        PreferredSize = 1,
        MinSize = 2,
        ExplicitSize = 3
    }

    public abstract class LayoutBreakpoint
    {
        [SerializeField]
        [Tooltip("Allow breakpoint to be defined automatically or explicitly, or set the breakpoint to none")]
        protected LayoutBreakpointMode m_Mode = LayoutBreakpointMode.None;

        protected bool m_wasLayoutAdjusted = false;

        public LayoutBreakpointMode Mode
        {
            get
            {
                return m_Mode;
            }
            set
            {
                m_Mode = value;
            }
        }

        public bool WasLayoutAdjusted
        {
            get
            {
                return m_wasLayoutAdjusted;
            }
        }

        public void RegisterLayoutAdjustment()
        {
            m_wasLayoutAdjusted = true;
        }

        public bool IsBelowBreakpoint(LayoutGroup layoutGroup)
        {
            int axis = GetAxis();

            RectTransform rectTransform = layoutGroup.transform as RectTransform;

            float size = rectTransform.GetAxisSize(axis);

            if (rectTransform != null)
            {
                if (m_Mode == LayoutBreakpointMode.ExplicitSize && size < GetAxisBreakpointSize())
                {
                    return true;
                }
                else
                {
                    float totalPreferredSize = layoutGroup.GetAxisTotalPreferredSize(axis);
                    float totalMinSize = layoutGroup.GetAxisTotalMinSize(axis);

                    if (m_Mode == LayoutBreakpointMode.PreferredSize && size < totalPreferredSize)
                    {
                        return true;
                    }
                    else if (m_Mode == LayoutBreakpointMode.MinSize && size < totalMinSize)
                    {
                        return true;
                    }
                }
            }

            m_wasLayoutAdjusted = false;
            return false;
        }

        public abstract int GetAxis();

        public abstract float GetAxisBreakpointSize();

        public abstract void SetAxisBreakpointSize(float size);
    }

    [Serializable]
    public class LayoutWidthBreakpoint : LayoutBreakpoint
    {
        [SerializeField]
        [Tooltip("The point at which this layout change")]
        [Min(0)]
        [ContextMenuItem("Set To Current Width", "SetWidthBreakpointToCurrentWidth")]
        protected float m_MinWidth;

        public float MinWidth
        {
            get
            {
                return m_MinWidth;
            }
            set
            {
                m_MinWidth = value;
            }
        }

        public override int GetAxis()
        {
            return 0;
        }

        public override float GetAxisBreakpointSize()
        {
            return m_MinWidth;
        }

        public override void SetAxisBreakpointSize(float size)
        {
            m_MinWidth = size;
        }
    }

    [Serializable]
    public class LayoutHeightBreakpoint : LayoutBreakpoint
    {
        [SerializeField]
        [Tooltip("The point at which this layout change")]
        [Min(0)]
        [ContextMenuItem("Set To Current Height", "SetHeightBreakpointToCurrentHeight")]
        protected float m_MinHeight;

        public float MinHeight
        {
            get
            {
                return m_MinHeight;
            }
            set
            {
                m_MinHeight = value;
            }
        }

        public override int GetAxis()
        {
            return 1;
        }

        public override float GetAxisBreakpointSize()
        {
            return m_MinHeight;
        }

        public override void SetAxisBreakpointSize(float size)
        {
            m_MinHeight = size;
        }
    }

}