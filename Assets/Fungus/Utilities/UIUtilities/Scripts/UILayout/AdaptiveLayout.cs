﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Layout/Adaptive Layout", 152)]
    public class AdaptiveLayout : UIBehaviour, IHasLayoutBreakpoint
    {
        [SerializeField]
        [Tooltip("The point at which this layout will activate and deactivate elements, " +
            "depending on whether the width of this rect transform is above or below the width breakpoint")]
        [ContextMenuItem("Set To Current Width", "SetWidthBreakpointToCurrentWidth")]
        protected LayoutWidthBreakpoint m_WidthBreakpoint = new LayoutWidthBreakpoint();

        [SerializeField]
        [Tooltip("These elements will be activated when the width of this rect transform is below the width breakpoint " +
            "and will be deactivated when the width of this rect transform is above the width breakpoint")]
        protected RectTransform[] m_ElementsActiveWhenBelowWidthBreakpoint = new RectTransform[0];

        [SerializeField]
        [Tooltip("These elements will be activated when the width of this rect transform is above the width breakpoint " +
            "and will be deactivated when the width of this rect transform is below the width breakpoint")]
        protected RectTransform[] m_ElementsActiveWhenAboveWidthBreakpoint = new RectTransform[0];

        [SerializeField]
        [Tooltip("The point at which this layout will activate and deactivate elements, " +
            "depending on whether the height of this rect transform is above or below the height breakpoint")]
        [ContextMenuItem("Set To Current Height", "SetHeightBreakpointToCurrentHeight")]
        protected LayoutHeightBreakpoint m_HeightBreakpoint = new LayoutHeightBreakpoint();

        [SerializeField]
        [Tooltip("These elements will be activated when the height of this rect transform is below the height breakpoint " +
            "and will be deactivated when the height of this rect transform is above the height breakpoint")]
        protected RectTransform[] m_ElementsActiveWhenBelowHeightBreakpoint = new RectTransform[0];

        [SerializeField]
        [Tooltip("These elements will be activated when the height of this rect transform is above the height breakpoint " +
            "and will be deactivated when the height of this rect transform is below the height breakpoint")]
        protected RectTransform[] m_ElementsActiveWhenAboveHeightBreakpoint = new RectTransform[0];

        [SerializeField]
        [Tooltip("The layout priority of this component")]
        protected int m_LayoutPriority = 1;

        private RectTransform m_rectTransform;

        private LayoutGroup m_layoutGroup;

        private List<RectTransform> m_rectChildren;

        private IHasLayoutBreakpoint[] m_orderedBreakpointLayoutChildren = new IHasLayoutBreakpoint[0];

        public LayoutWidthBreakpoint WidthBreakpoint
        {
            get
            {
                return m_WidthBreakpoint;
            }
            set
            {
                m_WidthBreakpoint = value;
            }
        }

        public LayoutHeightBreakpoint HeightBreakpoint
        {
            get
            {
                return m_HeightBreakpoint;
            }
            set
            {
                m_HeightBreakpoint = value;
            }
        }

        public RectTransform[] ElementsActiveWhenBelowWidthBreakpoint
        {
            get
            {
                return m_ElementsActiveWhenBelowWidthBreakpoint;
            }
            set
            {
                m_ElementsActiveWhenBelowWidthBreakpoint = value;
            }
        }

        public RectTransform[] ElementsActiveWhenAboveWidthBreakpoint
        {
            get
            {
                return m_ElementsActiveWhenAboveWidthBreakpoint;
            }
            set
            {
                m_ElementsActiveWhenAboveWidthBreakpoint = value;
            }
        }

        public RectTransform[] ElementsActiveWhenBelowHeightBreakpoint
        {
            get
            {
                return m_ElementsActiveWhenBelowHeightBreakpoint;
            }
            set
            {
                m_ElementsActiveWhenBelowHeightBreakpoint = value;
            }
        }

        public RectTransform[] ElementsActiveWhenAboveHeightBreakpoint
        {
            get
            {
                return m_ElementsActiveWhenAboveHeightBreakpoint;
            }
            set
            {
                m_ElementsActiveWhenAboveHeightBreakpoint = value;
            }
        }

        public int LayoutPriority
        {
            get
            {
                return m_LayoutPriority;
            }
            set
            {
                m_LayoutPriority = value;
            }
        }

        protected RectTransform RectTransform
        {
            get
            {
                if (m_rectTransform == null)
                {
                    m_rectTransform = GetComponent<RectTransform>();
                }
                return m_rectTransform;
            }
        }

        protected List<RectTransform> RectChildren
        {
            get
            {
                if (m_rectChildren == null)
                {
                    m_rectChildren = RectTransform.GetLayoutRectChildren(m_rectChildren);
                }
                return m_rectChildren;
            }
        }

        protected LayoutGroup LayoutGroup
        {
            get
            {
                if (m_layoutGroup == null)
                {
                    m_layoutGroup = GetComponentInChildren<LayoutGroup>(true);
                }
                return m_layoutGroup;
            }
        }

        protected IHasLayoutBreakpoint[] OrderedBreakpointLayoutChildren
        {
            get
            {
                if (m_orderedBreakpointLayoutChildren.Length == 0)
                {
                    m_orderedBreakpointLayoutChildren = this.GetOrderedIHasLayoutBreakpoint();
                }
                return m_orderedBreakpointLayoutChildren;
            }
        }
        
        protected virtual void LateUpdate()
        {
            m_rectChildren = RectTransform.GetLayoutRectChildren(m_rectChildren);

            ActivateAndDeactiveElementsAsNecessary();
        }
        
        public bool HasAdjustedHorizontalLayout()
        {
            return m_WidthBreakpoint.WasLayoutAdjusted;
        }

        public bool HasAdjustedVerticalLayout()
        {
            return m_HeightBreakpoint.WasLayoutAdjusted;
        }

        public int GetLayoutPriority()
        {
            return m_LayoutPriority;
        }

        public void SetWidthBreakpointToCurrentWidth()
        {
            m_WidthBreakpoint.MinWidth = RectTransform.rect.size.x;
        }

        public void SetHeightBreakpointToCurrentHeight()
        {
            m_HeightBreakpoint.MinHeight = RectTransform.rect.size.y;
        }
        
        public UIBehaviour GetComponent()
        {
            return this;
        }
        
        protected virtual void ActivateAndDeactiveElementsAsNecessary()
        {
            if (this.AllHigherPrioritiesHaveAdjustedHorizontalLayout(OrderedBreakpointLayoutChildren))
            {
                bool wasBelowWidthBreakpoint = m_WidthBreakpoint.IsBelowBreakpoint(LayoutGroup);

                if (wasBelowWidthBreakpoint != m_WidthBreakpoint.IsBelowBreakpoint(LayoutGroup))
                {
                    ActivateAndDeactiveWidthElements();
                }
            }

            if (this.AllHigherPrioritiesHaveAdjustedVerticalLayout(OrderedBreakpointLayoutChildren))
            {
                bool wasBelowHeightBreakpoint = m_HeightBreakpoint.IsBelowBreakpoint(LayoutGroup);

                if (wasBelowHeightBreakpoint != m_HeightBreakpoint.IsBelowBreakpoint(LayoutGroup))
                {
                    ActivateAndDeactiveHeightElements();
                }
            }
        }
        
        protected virtual void ActivateAndDeactiveWidthElements()
        {
            bool belowWidthBreakpoint = m_WidthBreakpoint.IsBelowBreakpoint(LayoutGroup);
            foreach (RectTransform rt in m_ElementsActiveWhenBelowWidthBreakpoint)
            {
                if (rt != null)
                {
                    rt.gameObject.SetActive(belowWidthBreakpoint);
                }
            }
            foreach (RectTransform rt in m_ElementsActiveWhenAboveWidthBreakpoint)
            {
                if (rt != null)
                {
                    rt.gameObject.SetActive(!belowWidthBreakpoint);
                }
            }

            if (belowWidthBreakpoint)
            {
                StartCoroutine(PerformActionOnNextFrame(m_WidthBreakpoint.RegisterLayoutAdjustment));
            }
        }

        protected virtual void ActivateAndDeactiveHeightElements()
        {
            bool belowHeightBreakpoint = m_HeightBreakpoint.IsBelowBreakpoint(LayoutGroup);
            foreach (RectTransform rt in m_ElementsActiveWhenBelowHeightBreakpoint)
            {
                if (rt != null)
                {
                    rt.gameObject.SetActive(belowHeightBreakpoint);
                }
            }
            foreach (RectTransform rt in m_ElementsActiveWhenAboveHeightBreakpoint)
            {
                if (rt != null)
                {
                    rt.gameObject.SetActive(!belowHeightBreakpoint);
                }
            }

            if (belowHeightBreakpoint)
            {
                StartCoroutine(PerformActionOnNextFrame(m_HeightBreakpoint.RegisterLayoutAdjustment));
            }
        }
        
        public IEnumerator PerformActionOnNextFrame(UnityAction onComplete)
        {
            yield return null;

            onComplete();
        }
    }

}
