﻿using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{
    
    public abstract class ResponsiveLayoutGroup : LayoutGroup
    {
        public enum Axis
        {
            Horizontal = 0,
            Vertical = 1
        }

        [SerializeField]
        [Tooltip("The spacing to use between layout elements")]
        protected Vector2 m_Spacing;

        [SerializeField]
        [Tooltip("Which axis should layout elements be placed along first?")]
        protected Axis m_StartAxis;
        
        public Vector2 Spacing
        {
            get
            {
                return m_Spacing;
            }
            set
            {
                SetProperty(ref m_Spacing, value);
            }
        }

        public Axis StartAxis
        {
            get
            {
                return m_StartAxis;
            }
            set
            {
                SetProperty(ref m_StartAxis, value);
            }
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();

            UnityEditor.EditorApplication.delayCall += Canvas.ForceUpdateCanvases;
        }
#endif

        protected RectTransform GetChildWithLargestNonFlexibleSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            GetLargestNonFlexibleSize(controlSize, axis, out largestChild);
            return largestChild;
        }
        
        protected float GetLargestNonFlexibleSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            return GetLargestNonFlexibleSize(controlSize, axis, out largestChild);
        }

        protected float GetLargestNonFlexibleSize(bool controlSize, int axis, out RectTransform largestChild)
        {
            float maxSize = 0;
            largestChild = null;
            foreach (RectTransform child in rectChildren)
            {
                if (LayoutUtility.GetFlexibleSize(child, axis) == 0)
                {
                    float size = 0;
                    if (controlSize)
                    {
                        size = Mathf.Max(LayoutUtility.GetMinSize(child, axis), LayoutUtility.GetPreferredSize(child, axis));
                    }
                    else
                    {
                        size = child.sizeDelta[axis];
                    }
                    if (size > maxSize)
                    {
                        maxSize = size;
                        largestChild = child;
                    }
                }
            }

            return maxSize;
        }
        
        protected RectTransform GetChildWithLargestPreferredSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            GetLargestPreferredSize(controlSize, axis, out largestChild);
            return largestChild;
        }

        protected float GetLargestPreferredSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            return GetLargestPreferredSize(controlSize, axis, out largestChild);
        }

        protected float GetLargestPreferredSize(bool controlSize, int axis, out RectTransform largestChild)
        {
            float maxSize = 0;
            largestChild = null;
            foreach (RectTransform child in rectChildren)
            {
                float size = 0;
                if (controlSize)
                {
                    size = LayoutUtility.GetPreferredSize(child, axis);
                }
                else
                {
                    size = child.sizeDelta[axis];
                }
                if (size > maxSize)
                {
                    maxSize = size;
                    largestChild = child;
                }
            }

            return maxSize;
        }

        protected RectTransform GetChildWithLargestMinSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            GetLargestMinSize(controlSize, axis, out largestChild);
            return largestChild;
        }

        protected float GetLargestMinSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            return GetLargestMinSize(controlSize, axis, out largestChild);
        }

        protected float GetLargestMinSize(bool controlSize, int axis, out RectTransform largestChild)
        {
            float maxSize = 0;
            largestChild = null;
            foreach (RectTransform child in rectChildren)
            {
                float size = 0;
                if (controlSize)
                {
                    size = LayoutUtility.GetMinSize(child, axis);
                }
                else
                {
                    size = child.sizeDelta[axis];
                }
                if (size > maxSize)
                {
                    maxSize = size;
                    largestChild = child;
                }
            }

            return maxSize;
        }

        protected RectTransform GetChildWithLargestFlexibleSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            GetLargestFlexibleSize(controlSize, axis, out largestChild);
            return largestChild;
        }

        protected float GetLargestFlexibleSize(bool controlSize, int axis)
        {
            RectTransform largestChild = null;
            return GetLargestFlexibleSize(controlSize, axis, out largestChild);
        }

        protected float GetLargestFlexibleSize(bool controlSize, int axis, out RectTransform largestChild)
        {
            float maxSize = 0;
            largestChild = null;
            foreach (RectTransform child in rectChildren)
            {
                float size = 0;
                if (controlSize)
                {
                    size = LayoutUtility.GetFlexibleSize(child, axis);
                }
                else
                {
                    size = child.sizeDelta[axis];
                }
                if (size > maxSize)
                {
                    maxSize = size;
                    largestChild = child;
                }
            }

            return maxSize;
        }
    }

}