﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Layout/Responsive List Layout Group", 152)]
    public class ResponsiveListLayoutGroup : ResponsiveLayoutGroup, IHasLayoutBreakpoint
    {
        public enum HorizontalItemOrder
        {
            LeftToRight = 0,
            RightToLeft = 1
        }

        public enum VerticalItemOrder
        {
            TopToBottom = 0,
            BottomToTop = 1
        }

        [SerializeField]
        [Tooltip("How should the layout elements be ordered in the horizontal direction?")]
        protected HorizontalItemOrder m_HorizontalOrder;

        [SerializeField]
        [Tooltip("How should the layout elements be ordered in the vertical direction?")]
        protected VerticalItemOrder m_VerticalOrder;
        
        [SerializeField]
        [Tooltip("If the width of the list is lower than the min width, the list will switch axis")]
        protected LayoutWidthBreakpoint m_WidthBreakpoint = new LayoutWidthBreakpoint();

        [SerializeField]
        [Tooltip("If the width of the list is lower than the min height, the list will switch axis")]
        protected LayoutHeightBreakpoint m_HeightBreakpoint = new LayoutHeightBreakpoint();

        [SerializeField]
        [Tooltip("Returns true if the Layout Group controls the widths of its children (Returns false if children control their own widths)")]
        protected bool m_ChildControlWidth = true;

        [SerializeField]
        [Tooltip("Returns true if the Layout Group controls the heights of its children (Returns false if children control their own heights)")]
        protected bool m_ChildControlHeight = true;

        [SerializeField]
        [Tooltip("Whether to force the children to expand to take up all available horizontal space")]
        protected bool m_ChildForceExpandWidth;

        [SerializeField]
        [Tooltip("Whether to force the children to expand to take up all available vertical space")]
        protected bool m_ChildForceExpandHeight;

        [SerializeField]
        [Tooltip("Whether to force all non-flexible children to be the same width as the widest non-flexible child")]
        protected bool m_ChildForceEqualWidth;

        [SerializeField]
        [Tooltip("Whether to force all non-flexible children to be the same height as the tallest non-flexible child")]
        protected bool m_ChildForceEqualHeight;

        [SerializeField]
        [Tooltip("The layout priority of this component")]
        protected int m_LayoutPriority = 1;

        private bool m_isVertical;
        
        private IHasLayoutBreakpoint[] m_orderedBreakpointLayoutChildren = new IHasLayoutBreakpoint[0];

        public HorizontalItemOrder HorizontalOrder
        {
            get
            {
                return m_HorizontalOrder;
            }
            set
            {
                m_HorizontalOrder = value;
            }
        }

        public VerticalItemOrder VerticalOrder
        {
            get
            {
                return m_VerticalOrder;
            }
            set
            {
                m_VerticalOrder = value;
            }
        }

        public LayoutWidthBreakpoint WidthBreakpoint
        {
            get
            {
                return m_WidthBreakpoint;
            }
            set
            {
                m_WidthBreakpoint = value;
            }
        }

        public LayoutHeightBreakpoint HeightBreakpoint
        {
            get
            {
                return m_HeightBreakpoint;
            }
            set
            {
                m_HeightBreakpoint = value;
            }
        }

        public bool ChildControlWidth
        {
            get
            {
                return m_ChildControlWidth;
            }
            set
            {
                SetProperty(ref m_ChildControlWidth, value);
            }
        }

        public bool ChildControlHeight
        {
            get
            {
                return m_ChildControlHeight;
            }
            set
            {
                SetProperty(ref m_ChildControlHeight, value);
            }
        }

        public bool ChildForceExpandWidth
        {
            get
            {
                return m_ChildForceExpandWidth;
            }
            set
            {
                SetProperty(ref m_ChildForceExpandWidth, value);
            }
        }

        public bool ChildForceExpandHeight
        {
            get
            {
                return m_ChildForceExpandHeight;
            }
            set
            {
                SetProperty(ref m_ChildForceExpandHeight, value);
            }
        }

        public bool ChildForceEqualWidth
        {
            get
            {
                return m_ChildForceEqualWidth;
            }
            set
            {
                SetProperty(ref m_ChildForceEqualWidth, value);
            }
        }

        public bool ChildForceEqualHeight
        {
            get
            {
                return m_ChildForceEqualHeight;
            }
            set
            {
                SetProperty(ref m_ChildForceEqualHeight, value);
            }
        }

        public int LayoutPriority
        {
            get
            {
                return m_LayoutPriority;
            }
            set
            {
                SetProperty(ref m_LayoutPriority, value);
            }
        }

        public List<RectTransform> RectChildren
        {
            get
            {
                return rectChildren;
            }
        }

        protected IHasLayoutBreakpoint[] OrderedBreakpointLayoutChildren
        {
            get
            {
                if (m_orderedBreakpointLayoutChildren.Length == 0)
                {
                    m_orderedBreakpointLayoutChildren = this.GetOrderedIHasLayoutBreakpoint();
                }
                return m_orderedBreakpointLayoutChildren;
            }
        }

        protected virtual void UpdateBreakpoints()
        {
            bool isVertical = m_StartAxis == Axis.Vertical;

            if (rectTransform.rect.width > 0 && rectTransform.rect.height > 0)
            {
                if (m_StartAxis == Axis.Vertical && m_HeightBreakpoint.Mode != LayoutBreakpointMode.None)
                {
                    if (this.AllHigherPrioritiesHaveAdjustedVerticalLayout(OrderedBreakpointLayoutChildren))
                    {
                        isVertical = IsVertical();
                    }
                }
                else if (m_StartAxis == Axis.Horizontal && m_WidthBreakpoint.Mode != LayoutBreakpointMode.None)
                {
                    if (this.AllHigherPrioritiesHaveAdjustedHorizontalLayout(OrderedBreakpointLayoutChildren))
                    {
                        isVertical = IsVertical();
                    }
                }
            }

            m_isVertical = isVertical;
        }

        public bool HasAdjustedHorizontalLayout()
        {
            return m_WidthBreakpoint.WasLayoutAdjusted;
        }

        public bool HasAdjustedVerticalLayout()
        {
            return m_HeightBreakpoint.WasLayoutAdjusted;
        }

        public int GetLayoutPriority()
        {
            return m_LayoutPriority;
        }

        public void SetWidthBreakpointToCurrentWidth()
        {
            m_WidthBreakpoint.MinWidth = rectTransform.rect.size.x;
        }

        public void SetHeightBreakpointToCurrentHeight()
        {
            m_HeightBreakpoint.MinHeight = rectTransform.rect.size.y;
        }
        
        public UIBehaviour GetComponent()
        {
            return this;
        }

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();

            if (m_StartAxis == Axis.Vertical)
            {
                UpdateBreakpoints();
            }

            CalcAlongAxis(0, m_isVertical);
        }

        public override void CalculateLayoutInputVertical()
        {
            if (m_StartAxis == Axis.Horizontal)
            {
                UpdateBreakpoints();
            }

            CalcAlongAxis(1, m_isVertical);
        }

        public override void SetLayoutHorizontal()
        {
            if (rectTransform.rect.width > 0)
            {
                SetChildrenAlongAxis(0, m_isVertical);
            }
        }

        public override void SetLayoutVertical()
        {
            if (rectTransform.rect.height > 0)
            {
                SetChildrenAlongAxis(1, m_isVertical);

                if (m_StartAxis == Axis.Horizontal)
                {
                    if (m_WidthBreakpoint.IsBelowBreakpoint(this))
                    {
                        StartCoroutine(PerformActionOnNextFrame(m_WidthBreakpoint.RegisterLayoutAdjustment));
                    }
                }
                else if (m_StartAxis == Axis.Vertical)
                {
                    if (m_HeightBreakpoint.IsBelowBreakpoint(this))
                    {
                        StartCoroutine(PerformActionOnNextFrame(m_HeightBreakpoint.RegisterLayoutAdjustment));
                    }
                }
            }
        }

        protected virtual bool IsVertical()
        {
            return ((m_StartAxis == Axis.Horizontal && (m_WidthBreakpoint.Mode == LayoutBreakpointMode.None || m_WidthBreakpoint.IsBelowBreakpoint(this))) ||
                    (m_StartAxis == Axis.Vertical && (m_HeightBreakpoint.Mode == LayoutBreakpointMode.None || !m_HeightBreakpoint.IsBelowBreakpoint(this))));
        }

        protected void CalcAlongAxis(int axis, bool isVertical)
        {
            float combinedPadding = (axis == 0 ? m_Padding.horizontal : m_Padding.vertical);
            bool controlSize = (axis == 0 ? m_ChildControlWidth : m_ChildControlHeight);
            bool childForceExpandSize = (axis == 0 ? m_ChildForceExpandWidth : m_ChildForceExpandHeight);
            bool childForceEqualSize = (axis == 0 ? m_ChildForceEqualWidth : m_ChildForceEqualHeight);
            float spacing = (axis == 0 ? m_Spacing.x : m_Spacing.y);

            float totalMin = combinedPadding;
            float totalPreferred = combinedPadding;
            float totalFlexible = 0;

            RectTransform largestChild = GetChildWithLargestNonFlexibleSize(controlSize, axis);

            bool alongOtherAxis = (isVertical ^ (axis == 1));
            for (int i = 0; i < rectChildren.Count; i++)
            {
                RectTransform child = rectChildren[i];
                float min, preferred, flexible;
                GetChildSizes(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, out min, out preferred, out flexible);

                if (alongOtherAxis)
                {
                    totalMin = Mathf.Max(min + combinedPadding, totalMin);
                    totalPreferred = Mathf.Max(preferred + combinedPadding, totalPreferred);
                    totalFlexible = Mathf.Max(flexible, totalFlexible);
                }
                else
                {
                    totalMin += min + spacing;
                    totalPreferred += preferred + spacing;

                    // Increment flexible size with element's flexible size.
                    totalFlexible += flexible;
                }
            }

            if (!alongOtherAxis && rectChildren.Count > 0)
            {
                totalMin -= spacing;
                totalPreferred -= spacing;
            }
            totalPreferred = Mathf.Max(totalMin, totalPreferred);

            SetLayoutInputForAxis(totalMin, totalPreferred, totalFlexible, axis);
        }

        protected void SetChildrenAlongAxis(int axis, bool isVertical)
        {
            float size = rectTransform.rect.size[axis];
            bool controlSize = (axis == 0 ? m_ChildControlWidth : m_ChildControlHeight);
            bool childForceExpandSize = (axis == 0 ? m_ChildForceExpandWidth : m_ChildForceExpandHeight);
            bool childForceEqualSize = (axis == 0 ? m_ChildForceEqualWidth : m_ChildForceEqualHeight);
            float alignmentOnAxis = GetAlignmentOnAxis(axis);
            float spacing = (axis == 0 ? m_Spacing.x : m_Spacing.y);

            RectTransform largestChild = GetChildWithLargestNonFlexibleSize(controlSize, axis);

            bool alongOtherAxis = (isVertical ^ (axis == 1));
            if (alongOtherAxis)
            {
                float innerSize = size - (axis == 0 ? padding.horizontal : padding.vertical);
                if ((axis == 0 && m_HorizontalOrder == HorizontalItemOrder.RightToLeft) ||
                    (axis == 1 && m_VerticalOrder == VerticalItemOrder.BottomToTop))
                {
                    for (int i = rectChildren.Count - 1; i >= 0; i--)
                    {
                        RectTransform child = rectChildren[i];
                        SetChildAlongOtherAxis(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, alignmentOnAxis, innerSize, size, spacing);
                    }
                }
                else
                {
                    for (int i = 0; i < rectChildren.Count; i++)
                    {
                        RectTransform child = rectChildren[i];
                        SetChildAlongOtherAxis(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, alignmentOnAxis, innerSize, size, spacing);
                    }
                }
            }
            else
            {
                float pos = (axis == 0 ? padding.left : padding.top);
                float startOffset = GetStartOffset(axis, GetTotalPreferredSize(axis) - (axis == 0 ? padding.horizontal : padding.vertical));
                if (GetTotalFlexibleSize(axis) == 0 && GetTotalPreferredSize(axis) < size)
                {
                    pos = startOffset;
                }

                float minMaxLerp = 0;
                if (GetTotalMinSize(axis) != GetTotalPreferredSize(axis))
                    minMaxLerp = Mathf.Clamp01((size - GetTotalMinSize(axis)) / (GetTotalPreferredSize(axis) - GetTotalMinSize(axis)));

                float itemFlexibleMultiplier = 0;
                if (size > GetTotalPreferredSize(axis))
                {
                    if (GetTotalFlexibleSize(axis) > 0)
                        itemFlexibleMultiplier = (size - GetTotalPreferredSize(axis)) / GetTotalFlexibleSize(axis);
                }

                if ((axis == 0 && m_HorizontalOrder == HorizontalItemOrder.RightToLeft) ||
                    (axis == 1 && m_VerticalOrder == VerticalItemOrder.BottomToTop))
                {
                    for (int i = rectChildren.Count - 1; i >= 0; i--)
                    {
                        RectTransform child = rectChildren[i];
                        SetChildAlongAxis(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, alignmentOnAxis, minMaxLerp, itemFlexibleMultiplier, spacing, ref pos);
                    }
                }
                else
                {
                    for (int i = 0; i < rectChildren.Count; i++)
                    {
                        RectTransform child = rectChildren[i];
                        SetChildAlongAxis(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, alignmentOnAxis, minMaxLerp, itemFlexibleMultiplier, spacing, ref pos);
                    }
                }
            }
        }

        protected void SetChildAlongAxis(RectTransform child, RectTransform largestChild, int axis, bool controlSize, bool childForceExpandSize, bool childForceEqualSize, float alignmentOnAxis, float minMaxLerp, float itemFlexibleMultiplier, float spacing, ref float pos)
        {
            float min, preferred, flexible;
            GetChildSizes(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, out min, out preferred, out flexible);

            float childSize = Mathf.Lerp(min, preferred, minMaxLerp);
            childSize += flexible * itemFlexibleMultiplier;

            if (controlSize)
            {
                SetChildAlongAxis(child, axis, pos, childSize);
            }
            else
            {
                float offsetInCell = (childSize - child.sizeDelta[axis]) * alignmentOnAxis;
                SetChildAlongAxis(child, axis, pos + offsetInCell);
            }

            pos += childSize + spacing;
        }

        protected void SetChildAlongOtherAxis(RectTransform child, RectTransform largestChild, int axis, bool controlSize, bool childForceExpandSize, bool childForceEqualSize, float alignmentOnAxis, float innerSize, float size, float spacing)
        {
            float min, preferred, flexible;
            GetChildSizes(child, largestChild, axis, controlSize, childForceExpandSize, childForceEqualSize, out min, out preferred, out flexible);

            float requiredSpace = Mathf.Clamp(innerSize, min, flexible > 0 ? size : preferred);
            float startOffset = GetStartOffset(axis, requiredSpace);
            if (controlSize)
            {
                SetChildAlongAxis(child, axis, startOffset, requiredSpace);
            }
            else
            {
                float offsetInCell = (requiredSpace - child.sizeDelta[axis]) * alignmentOnAxis;
                SetChildAlongAxis(child, axis, startOffset + offsetInCell);
            }
        }

        private void GetChildSizes(RectTransform child, RectTransform largestChild, int axis, bool controlSize, bool childForceExpand, bool childForceEqual,
            out float min, out float preferred, out float flexible)
        {
            flexible = LayoutUtility.GetFlexibleSize(child, axis);
            if (childForceExpand)
            {
                flexible = Mathf.Max(flexible, 1);
            }

            if (controlSize)
            {
                if (childForceEqual && childForceExpand)
                {
                    flexible = 0;
                    min = 0;
                    preferred = 1E+30F; // Max value preferred can be for a layout element
                }
                else if (childForceEqual && flexible == 0)
                {
                    min = LayoutUtility.GetMinSize(largestChild, axis);
                    preferred = LayoutUtility.GetPreferredSize(largestChild, axis);
                }
                else
                {
                    min = LayoutUtility.GetMinSize(child, axis);
                    preferred = LayoutUtility.GetPreferredSize(child, axis);
                }
            }
            else
            {
                if (childForceEqual && flexible == 0)
                {
                    min = largestChild.sizeDelta[axis];
                    preferred = min;
                }
                else
                {
                    min = child.sizeDelta[axis];
                    preferred = min;
                }
                if (!childForceExpand)
                {
                    flexible = 0;
                }
            }
        }

        public IEnumerator PerformActionOnNextFrame(UnityAction onComplete)
        {
            yield return null;

            onComplete();
        }
    }

}