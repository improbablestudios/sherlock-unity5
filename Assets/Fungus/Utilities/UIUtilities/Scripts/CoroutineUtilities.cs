﻿using System;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class WaitForFrames : CustomYieldInstruction
    {
        public int m_frames;
        private float m_startFrame;

        public WaitForFrames(int frames)
        {
            m_startFrame = Time.frameCount;
        }

        public override bool keepWaiting
        {
            get
            {
                return Time.frameCount - m_startFrame < m_frames;
            }
        }
    }

}