﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.UIUtilities
{

    public class WriterManager : SettingsScriptableObject<WriterManager>
    {
        [Tooltip("Determines how fast text will be written")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("writingSpeedMultiplier")]
        protected float m_WritingSpeedMultiplier = 1;

        [Tooltip("The custom tags that can be used to control how text is written")]
        [SerializeField]
        protected List<CustomTag> m_CustomTags = new List<CustomTag>();

        public float WritingSpeedMultiplier
        {
            get
            {
                return m_WritingSpeedMultiplier;
            }
            set
            {
                m_WritingSpeedMultiplier = value;
            }
        }

        public CustomTag[] CustomTags
        {
            get
            {
                return m_CustomTags.ToArray();
            }
            set
            {
                m_CustomTags = value.ToList();
            }
        }

        public static string GetTagHelpText()
        {
            string tagsText = TextTagParser.HELP_INFO;

            if (WriterManager.Instance.CustomTags.Length > 0)
            {
                tagsText += "\n\n\t-------- CUSTOM TAGS --------";
                foreach (CustomTag ct in WriterManager.Instance.CustomTags)
                {
                    if (ct != null)
                    {
                        string tagName = ct.name;
                        string tagStartSymbol = ct.TagStartSymbol;
                        string tagEndSymbol = ct.TagEndSymbol;
                        tagsText += "\n\t      " + tagStartSymbol + " " + tagName + " " + tagEndSymbol;
                    }
                }
            }
            tagsText += "\n";

            return tagsText;
        }

    }

}