﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Scroll On Select")]
    public class ScrollOnSelect : BaseOnEvent, ISelectHandler
    {
        protected override void Reset()
        {
            base.Reset();

            m_OnAxisEvent = true;
            m_OnPointerEvent = false;
            m_OnOtherEvent = true;
        }

        public void OnSelect(BaseEventData eventData)
        {
            if (ShouldHandleEvent(eventData))
            {
                SelectionScroller selectionScroller = gameObject.GetComponentInParent<SelectionScroller>();
                if (selectionScroller != null && selectionScroller.ShouldScrollOnSelect())
                {
                    RectTransform child = selectionScroller.GetChildContainingGameObject(gameObject);
                    selectionScroller.ScrollTo(child, false);
                }
            }
        }
    }

}
