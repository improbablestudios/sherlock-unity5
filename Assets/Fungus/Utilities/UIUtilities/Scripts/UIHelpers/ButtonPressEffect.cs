﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [DisallowMultipleComponent]
    [AddComponentMenu("UI/Helper/Button Press Effect")]
    public class ButtonPressEffect : PersistentBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [Tooltip("The amount to move this button down on press")]
        [SerializeField]
        [FormerlySerializedAs("moveChildrenDownAmount")]
        protected float m_MoveChildrenDownAmount;

        private bool m_pointerDown;

        private bool m_pointerInside;

        public float MoveChildrenDownAmount
        {
            get
            {
                return m_MoveChildrenDownAmount;
            }
            set
            {
                m_MoveChildrenDownAmount = value;
            }
        }

        protected void Down()
        {
            foreach (Transform child in transform)
            {
                RectTransform thisRT = this.GetComponent<RectTransform>();
                RectTransform childRT = child.GetComponent<RectTransform>();
                if (thisRT != null && childRT != null)
                {
                    childRT.anchoredPosition = new Vector2(childRT.anchoredPosition.x, childRT.anchoredPosition.y - m_MoveChildrenDownAmount);
                }
            }
        }

        protected void Up()
        {
            foreach (Transform child in transform)
            {
                RectTransform thisRT = this.GetComponent<RectTransform>();
                RectTransform childRT = child.GetComponent<RectTransform>();
                if (thisRT != null && childRT != null)
                {
                    childRT.anchoredPosition = new Vector2(childRT.anchoredPosition.x, childRT.anchoredPosition.y + m_MoveChildrenDownAmount);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (m_pointerInside)
            {
                Down();
            }
            m_pointerDown = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (m_pointerInside)
            {
                Up();
            }
            m_pointerDown = false;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (m_pointerDown)
            {
                Down();
            }
            m_pointerInside = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_pointerDown)
            {
                Up();
            }
            m_pointerInside = false;
        }
    }
}
