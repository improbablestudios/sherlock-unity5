﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{
    [RequireComponent(typeof(Selectable))]
    public class AllowSelectionWhenDisabled : MonoBehaviour
    {
        //Literally nothing. This component will need to exist on a selectable and then other classes can check for it.
    }
}