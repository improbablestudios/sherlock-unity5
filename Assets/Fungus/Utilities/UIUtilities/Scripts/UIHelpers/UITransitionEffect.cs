﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Linq;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityObjectUtilities;
using DG.Tweening;
using ImprobableStudios.TextStylePaletteUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    public abstract class UITransitionEffect : PersistentBehaviour
    {
        public enum TransitionType
        {
            None = 0,
            ColorTint = 1,
            SpriteSwap = 2,
            Animation = 3,
            ColorChange = 4,
            SpriteChange = 5,
            TextStyle = 6,
        }

        [Serializable]
        public class TransitionState
        {
            [Tooltip("The transition state name")]
            [SerializeField]
            protected string m_Name;

            [Tooltip("The color that will be applied to the Target Graphics when the state changes")]
            [SerializeField]
            protected Color m_Color = Color.white;

            [Tooltip("The sprite that will be applied to the Target Graphics when the state changes")]
            [SerializeField]
            protected Sprite m_Sprite;

            [Tooltip("The animation that will be applied when the state changes")]
            [SerializeField]
            protected string m_Animation;

            [Tooltip("The text style to use for the target graphic (if it is a Text graphic)")]
            [SerializeField]
            protected FontData m_TextStyle = FontData.defaultFontData;

            public string Name
            {
                get
                {
                    return m_Name;
                }
                set
                {
                    m_Name = value;
                }
            }

            public Color Color
            {
                get
                {
                    return m_Color;
                }
                set
                {
                    m_Color = value;
                }
            }

            public Sprite Sprite
            {
                get
                {
                    return m_Sprite;
                }
                set
                {
                    m_Sprite = value;
                }
            }

            public string Animation
            {
                get
                {
                    return m_Animation;
                }
                set
                {
                    m_Animation = value;
                }
            }

            public FontData TextStyle
            {
                get
                {
                    return m_TextStyle;
                }
                set
                {
                    m_TextStyle = value;
                }
            }

            public TransitionState(string name)
            {
                m_Name = name;
                m_Color = Color.white;
                m_Sprite = null;
                m_Animation = name;
                m_TextStyle = FontData.defaultFontData;
            }

            public TransitionState(string name, Color color)
            {
                m_Name = name;
                m_Color = color;
                m_Sprite = null;
                m_Animation = name;
                m_TextStyle = FontData.defaultFontData;
            }

            public TransitionState(string name, Color color, Sprite sprite)
            {
                m_Name = name;
                m_Color = color;
                m_Sprite = sprite;
                m_Animation = name;
                m_TextStyle = FontData.defaultFontData;
            }

            public TransitionState(string name, Color color, Sprite sprite, string animation)
            {
                m_Name = name;
                m_Color = color;
                m_Sprite = sprite;
                m_Animation = animation;
                m_TextStyle = FontData.defaultFontData;
            }

            public TransitionState(string name, Color color, Sprite sprite, string animation, FontData textStyle)
            {
                m_Name = name;
                m_Color = color;
                m_Sprite = sprite;
                m_Animation = animation;
                m_TextStyle = textStyle;
            }
        }

        private const string DEFAULT_STATE_NAME = "Normal";

        [Tooltip("The type of transition that will be applied when the state changes")]
        [SerializeField]
        protected TransitionType m_Transition = TransitionType.ColorTint;

        [Tooltip("The graphics that will be affected when the state changes")]
        [SerializeField]
        protected Graphic[] m_TargetGraphics = new Graphic[0];

        [Tooltip("Multiplier applied to colors (allows brightening greater then base color)")]
        [Range(1, 5)]
        [SerializeField]
        protected float m_ColorMultiplier = 1f;

        [Tooltip("How long a color transition should take")]
        [Min(0)]
        [SerializeField]
        protected float m_ColorFadeDuration = 0.1f;

        protected int m_currentState;

        private bool[] m_previousTargetActiveStates = new bool[0];

        public TransitionState[] TransitionStates
        {
            get
            {
                return GetTransitionStates();
            }
        }

        public TransitionType Transition
        {
            get
            {
                return m_Transition;
            }
            set
            {
                m_Transition = value;
                OnSetProperty();
            }
        }

        public float ColorMultiplier
        {
            get
            {
                return m_ColorMultiplier;
            }
            set
            {
                m_ColorMultiplier = value;
            }
        }

        public float ColorFadeDuration
        {
            get
            {
                return m_ColorFadeDuration;
            }
            set
            {
                m_ColorFadeDuration = value;
            }
        }

        public Graphic[] TargetGraphics
        {
            get
            {
                return m_TargetGraphics;
            }
            set
            {
                m_TargetGraphics = value;
                OnSetProperty();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Transition != TransitionType.None)
            {
                m_TargetGraphics = GetComponents<Graphic>();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            if (m_TargetGraphics.Length == 0)
            {
                m_TargetGraphics = GetComponents<Graphic>();
            }
        }

        protected PointerEventData GetLastPointerEventData()
        {
            if (EventSystem.current != null)
            {
                StandaloneInputModule standaloneInputModule = EventSystem.current.currentInputModule as StandaloneInputModule;
                if (standaloneInputModule != null)
                {
                    return standaloneInputModule.GetLastPointerEventData();
                }
            }
            return null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            EvaluateAndTransitionState(GetLastPointerEventData(), true);
        }

        protected override void OnDisable()
        {
            InstantClearState();
            base.OnDisable();
        }

        protected virtual void Update()
        {
            int state = GetCurrentState(GetLastPointerEventData());
            bool[] targetActiveStates = m_TargetGraphics.Select(x => x.isActiveAndEnabled).ToArray();
            if (m_currentState != state || targetActiveStates.SequenceEqual(m_previousTargetActiveStates))
            {
                DoStateTransition(state, false);
            }
            m_previousTargetActiveStates = targetActiveStates;
        }

        public abstract TransitionState[] GetTransitionStates();

        protected abstract int GetCurrentState(BaseEventData eventData);

        protected void OnSetProperty()
        {
            DoStateTransition(m_currentState, false);
        }

        protected virtual void InstantClearState()
        {
            TransitionType transition = m_Transition;
            Color normalColor = GetTransitionStates()[0].Color;
            Sprite normalSprite = GetTransitionStates()[0].Sprite;
            string normalTrigger = GetTransitionStates()[0].Animation;
            FontData normalTextStyle = GetTransitionStates()[0].TextStyle;

            foreach (Graphic targetGraphic in m_TargetGraphics)
            {
                switch (transition)
                {
                    case TransitionType.ColorTint:
                        StartColorTween(targetGraphic, normalColor, 0f, true);
                        break;
                    case TransitionType.ColorChange:
                        StartColorTween(targetGraphic, normalColor, 0f, false);
                        break;
                    case TransitionType.SpriteSwap:
                        DoSpriteSwap(targetGraphic, normalSprite, true);
                        break;
                    case TransitionType.SpriteChange:
                        DoSpriteSwap(targetGraphic, normalSprite, false);
                        break;
                    case TransitionType.Animation:
                        TriggerAnimation(normalTrigger);
                        break;
                    case TransitionType.TextStyle:
                        ChangeTextStyle(targetGraphic, normalTextStyle);
                        break;
                }
            }
        }

        protected virtual void DoStateTransition(int state, bool instant)
        {
            if (!Application.isPlaying)
            {
                instant = true;
            }

            m_currentState = state;

            if (gameObject.activeInHierarchy)
            {
                TransitionType transition = m_Transition;
                float colorMultiplier = m_ColorMultiplier;
                float fadeDuration = m_ColorFadeDuration;

                TransitionState transitionState = GetTransitionStates()[state];

                Color color = transitionState.Color;
                Sprite sprite = transitionState.Sprite;
                string triggername = transitionState.Animation;
                FontData textStyle = transitionState.TextStyle;

                foreach (Graphic targetGraphic in m_TargetGraphics)
                {
                    switch (transition)
                    {
                        case TransitionType.ColorTint:
                            StartColorTween(targetGraphic, color * colorMultiplier, (!instant) ? fadeDuration : 0f, true);
                            break;
                        case TransitionType.ColorChange:
                            StartColorTween(targetGraphic, color, (!instant) ? fadeDuration : 0f, false);
                            break;
                        case TransitionType.SpriteSwap:
                            DoSpriteSwap(targetGraphic, sprite, true);
                            break;
                        case TransitionType.SpriteChange:
                            DoSpriteSwap(targetGraphic, sprite, false);
                            break;
                        case TransitionType.Animation:
                            TriggerAnimation(triggername);
                            break;
                        case TransitionType.TextStyle:
                            ChangeTextStyle(targetGraphic, textStyle);
                            break;
                    }
                }
            }
        }

        protected void StartColorTween(Graphic targetGraphic, Color newColor, float fadeDuration, bool tintColor)
        {
            if (targetGraphic != null)
            {
                if (tintColor)
                {
                    if (fadeDuration <= 0)
                    {
                        targetGraphic.canvasRenderer.SetColor(newColor);
                    }
                    else
                    {
                        targetGraphic.CrossFadeColor(newColor, fadeDuration, true, true);
                    }
                }
                else
                {
                    if (fadeDuration <= 0)
                    {
                        targetGraphic.color = newColor;
                    }
                    else
                    {
                        targetGraphic.DOColor(newColor, fadeDuration);
                    }
                }
            }
        }

        protected void DoSpriteSwap(Graphic targetGraphic, Sprite newSprite, bool overrideSprite)
        {
            Image image = targetGraphic as Image;
            if (image != null)
            {
                if (overrideSprite)
                {
                    image.overrideSprite = newSprite;
                }
                else
                {
                    image.sprite = newSprite;
                }
            }
        }

        protected void TriggerAnimation(string triggername)
        {
            Animator animator = GetComponent<Animator>();
            if (m_Transition == TransitionType.Animation && animator != null && animator.isActiveAndEnabled && animator.hasBoundPlayables && !string.IsNullOrEmpty(triggername))
            {
                foreach (TransitionState transitionState in GetTransitionStates())
                {
                    animator.ResetTrigger(transitionState.Animation);
                }
                animator.SetTrigger(triggername);
            }
        }

        protected void ChangeTextStyle(Graphic targetGraphic, FontData newFontData)
        {
            Text text = targetGraphic as Text;
            if (text != null)
            {
                text.SetFontData(newFontData);
            }
        }

        public void EvaluateAndTransitionState(BaseEventData eventData, bool instant)
        {
            DoStateTransition(GetCurrentState(eventData), instant);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetGraphics))
            {
                TransitionType transition = m_Transition;
                Graphic[] targetGraphics = m_TargetGraphics;

                if (transition == TransitionType.ColorTint || transition == TransitionType.ColorChange)
                {
                    if (targetGraphics.Length == 0)
                    {
                        return "You must have a Graphic target in order to use a color transition.";
                    }
                }
                if (transition == TransitionType.SpriteSwap || transition == TransitionType.SpriteChange)
                {
                    if (targetGraphics.Length == 0 || !targetGraphics.Any(i => i is Image))
                    {
                        return "You must have a Image target in order to use a sprite transition.";
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);
            
            TransitionState[] transitionStates = GetTransitionStates();
            for (int i = 0; i < transitionStates.Length; i++)
            {
                TransitionState transitionState = transitionStates[i];
                if (string.IsNullOrEmpty(transitionState.Animation))
                {
                    transitionState.Animation = transitionState.Name;
                }
            }

            if (isActiveAndEnabled)
            {
                InstantClearState();
                DoStateTransition(m_currentState, true);
            }
        }

        public override int GetPropertyIndentLevelOffset(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetGraphics))
            {
                return 1;
            }
            if (propertyPath == "m_TransitionStates")
            {
                return 1;
            }
            if (propertyPath == nameof(m_ColorMultiplier))
            {
                return 1;
            }
            if (propertyPath == nameof(m_ColorFadeDuration))
            {
                return 1;
            }

            return base.GetPropertyIndentLevelOffset(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == "m_TransitionStates")
            {
                if (m_Transition == TransitionType.None)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetGraphics))
            {
                if (m_Transition != TransitionType.ColorTint && m_Transition != TransitionType.SpriteSwap && m_Transition != TransitionType.ColorChange && m_Transition != TransitionType.SpriteChange)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ColorMultiplier))
            {
                if (m_Transition != TransitionType.ColorTint)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ColorFadeDuration))
            {
                if (m_Transition != TransitionType.ColorTint && m_Transition != TransitionType.ColorChange)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_Transition))
            {
                return 1;
            }
            if (propertyPath == nameof(m_ColorMultiplier) || propertyPath == nameof(m_ColorFadeDuration))
            {
                return 2;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}