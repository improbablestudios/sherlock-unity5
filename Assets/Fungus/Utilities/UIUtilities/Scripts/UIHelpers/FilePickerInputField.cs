﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [AddComponentMenu("UI/File Picker Input Field")]
    [RequireComponent(typeof(InputField))]
    public class FilePickerInputField : PersistentBehaviour, IPointerClickHandler
    {
        [Tooltip("The title displayed in the file picker")]
        [SerializeField]
        [FormerlySerializedAs("filePickerTitle")]
        protected string m_FilePickerTitle = "";

        [Tooltip("The default directory to pick the file from")]
        [SerializeField]
        [FormerlySerializedAs("defaultDirectory")]
        protected string m_DefaultDirectory = "";

        [Tooltip("The file extension")]
        [SerializeField]
        [FormerlySerializedAs("extension")]
        protected string m_Extension = "";
        
        private InputField m_inputField;

        public string FilePickerTitle
        {
            get
            {
                return m_FilePickerTitle;
            }
            set
            {
                m_FilePickerTitle = value;
            }
        }

        public string DefaultDirectory
        {
            get
            {
                return m_DefaultDirectory;
            }
            set
            {
                m_DefaultDirectory = value;
            }
        }

        public string Extension
        {
            get
            {
                return m_Extension;
            }
            set
            {
                m_Extension = value;
            }
        }

        public string Path
        {
            get
            {
                return InputField.text;
            }
        }

        public InputField InputField
        {
            get
            {
                if (m_inputField == null)
                {
                    m_inputField = GetComponent<InputField>();
                }
                return m_inputField;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
#if UNITY_EDITOR
            string path = UnityEditor.EditorUtility.OpenFilePanel(m_FilePickerTitle, m_DefaultDirectory, m_Extension);
            if (!string.IsNullOrEmpty(path))
            {
                InputField.text = path;
            }
#endif
        }
    }

}
