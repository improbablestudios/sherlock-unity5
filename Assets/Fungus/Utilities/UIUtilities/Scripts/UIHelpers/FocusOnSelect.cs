﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Focus On Select")]
    public class FocusOnSelect : BaseOnEvent, ISelectHandler
    {
        [SerializeField]
        protected InputField m_InputField;

        public InputField InputField
        {
            get
            {
                return m_InputField;
            }
            set
            {
                m_InputField = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_InputField == null)
            {
                m_InputField = GetComponent<InputField>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_InputField == null)
            {
                m_InputField = GetComponent<InputField>();
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            if (ShouldHandleEvent(eventData))
            {
                InputField.ActivateInputField();
            }
        }
    }

}
