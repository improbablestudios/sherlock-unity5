﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System.Collections.Generic;
using System;
using System.Reflection;
using ImprobableStudios.UIUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(ScrollRect))]
    [AddComponentMenu("UI/Helper/Selection Scroller")]
    public class SelectionScroller : UIBehaviour
    {
        public enum ScrollMode
        {
            Reveal,
            Align,
        }

        [Tooltip("The mode used for scrolling (Align = scroll whenever a new child is selected, Reveal = scroll only when the selected child is out of the viewport bounds)")]
        [SerializeField]
        protected ScrollMode m_Mode = ScrollMode.Reveal;

        [Tooltip("The amount of time in seconds it takes to scroll to a selected option")]
        [Min(0)]
        [SerializeField]
        protected float m_ScrollDuration = 0.2f;

        [Tooltip("The button that when pressed will select the child above the currently selected child")]
        [SerializeField]
        protected Button m_ScrollUpButton;

        [Tooltip("The button that when pressed will select the child below the currently selected child")]
        [SerializeField]
        protected Button m_ScrollDownButton;

        [Tooltip("The button that when pressed will select the child to the left of the currently selected child")]
        [SerializeField]
        protected Button m_ScrollLeftButton;

        [Tooltip("The button that when pressed will select the child to the right of the currently selected child")]
        [SerializeField]
        protected Button m_ScrollRightButton;

        [Tooltip("Automatically child closest to focus point when dragging or scroll wheeling")]
        [SerializeField]
        protected bool m_AutoSelect = true;

        [Tooltip("Only show scroll buttons for the directions that contain children outside of the viewport bounds")]
        [SerializeField]
        protected bool m_AutoHideScrollButtons = true;

        [Tooltip("Determines how the scroller will align the focused child (If content has a layout group, use the layout group's alignment)")]
        [SerializeField]
        protected TextAnchor m_ChildAlignment = TextAnchor.MiddleCenter;

        [Tooltip("The padding around the content (If content has a layout group, use the layout group's padding)")]
        [SerializeField]
        protected RectOffset m_Padding = new RectOffset();

        protected bool m_originalInertia;

        protected float m_originalScrollSensitivity;
        
        protected Tween m_scrollTween;

        protected bool m_dragging;

        protected bool m_scrollWheeling;

        private ScrollRect m_scrollRect;

        private RectTransform m_content;

        private RectTransform m_viewport;

        private List<RectTransform> m_contentChildren;

        private List<Selectable> m_contentSelectables;

        private LayoutGroup m_contentLayoutGroup;

        public ScrollMode Mode
        {
            get
            {
                return m_Mode;
            }
            set
            {
                m_Mode = value;
            }
        }

        public float ScrollDuration
        {
            get
            {
                return m_ScrollDuration;
            }
            set
            {
                m_ScrollDuration = value;
            }
        }

        public Button ScrollUpButton
        {
            get
            {
                return m_ScrollUpButton;
            }
            set
            {
                m_ScrollUpButton = value;
            }
        }

        public Button ScrollDownButton
        {
            get
            {
                return m_ScrollDownButton;
            }
            set
            {
                m_ScrollDownButton = value;
            }
        }

        public Button ScrollLeftButton
        {
            get
            {
                return m_ScrollLeftButton;
            }
            set
            {
                m_ScrollLeftButton = value;
            }
        }

        public Button ScrollRightButton
        {
            get
            {
                return m_ScrollRightButton;
            }
            set
            {
                m_ScrollRightButton = value;
            }
        }

        public bool AutoSelect
        {
            get
            {
                return m_AutoSelect;
            }
            set
            {
                m_AutoSelect = value;
                SetupScrollControl();
            }
        }

        public bool AutoHideScrollButtons
        {
            get
            {
                return m_AutoHideScrollButtons;
            }
            set
            {
                m_AutoHideScrollButtons = value;
            }
        }

        public TextAnchor ChildAlignment
        {
            get
            {
                return m_ChildAlignment;
            }
            set
            {
                m_ChildAlignment = value;
            }
        }

        public RectOffset Padding
        {
            get
            {
                return m_Padding;
            }
            set
            {
                m_Padding = value;
            }
        }

        public ScrollRect ScrollRect
        {
            get
            {
                if (m_scrollRect == null)
                {
                    m_scrollRect = GetComponent<ScrollRect>();
                }
                return m_scrollRect;
            }
        }

        public RectTransform Content
        {
            get
            {
                if (m_content == null)
                {
                    m_content = ScrollRect.content;
                }
                return m_content;
            }
        }

        public RectTransform Viewport
        {
            get
            {
                if (m_viewport == null)
                {
                    m_viewport = GetViewport();
                }
                return m_viewport;
            }
        }

        public List<RectTransform> ContentChildren
        {
            get
            {
                if (m_contentChildren == null)
                {
                    if (Content != null)
                    {
                        m_contentChildren = GetContentChildren(Content);
                    }
                }

                return m_contentChildren;
            }
        }
        
        public LayoutGroup ContentLayoutGroup
        {
            get
            {
                if (m_contentLayoutGroup == null)
                {
                    if (Content != null)
                    {
                        m_contentLayoutGroup = Content.GetComponent<LayoutGroup>();
                    }
                }
                return m_contentLayoutGroup;
            }
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();

            if (ContentLayoutGroup != null)
            {
                m_ChildAlignment = ContentLayoutGroup.childAlignment;
                m_Padding = ContentLayoutGroup.padding;
            }
        }
#endif

        protected override void OnEnable()
        {
            base.OnEnable();

            m_originalInertia = ScrollRect.inertia;
            m_originalScrollSensitivity = ScrollRect.scrollSensitivity;

            if (m_ScrollUpButton != null)
            {
                m_ScrollUpButton.onClick.AddListener(OnScrollUpButtonClicked);
            }
            if (m_ScrollDownButton != null)
            {
                m_ScrollDownButton.onClick.AddListener(OnScrollDownButtonClicked);
            }
            if (m_ScrollLeftButton != null)
            {
                m_ScrollLeftButton.onClick.AddListener(OnScrollLeftButtonClicked);
            }
            if (m_ScrollRightButton != null)
            {
                m_ScrollRightButton.onClick.AddListener(OnScrollRightButtonClicked);
            }

            SetupScrollControl();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            ScrollRect.inertia = m_originalInertia;
            ScrollRect.scrollSensitivity = m_originalScrollSensitivity;

            if (m_ScrollUpButton != null)
            {
                m_ScrollUpButton.onClick.RemoveListener(OnScrollUpButtonClicked);
            }
            if (m_ScrollDownButton != null)
            {
                m_ScrollDownButton.onClick.RemoveListener(OnScrollDownButtonClicked);
            }
            if (m_ScrollLeftButton != null)
            {
                m_ScrollLeftButton.onClick.RemoveListener(OnScrollLeftButtonClicked);
            }
            if (m_ScrollRightButton != null)
            {
                m_ScrollRightButton.onClick.RemoveListener(OnScrollRightButtonClicked);
            }
        }

        protected override void Start()
        {
            base.Start();

            RectTransform selectedChild = GetSelectedChild();
            if (selectedChild != null)
            {
                ScrollTo(selectedChild, true);
            }
        }

        protected virtual void LateUpdate()
        {
            UpdateCache();
            UpdateScrollButtons();
            UpdateFocus();
            if (Viewport != null)
            {
                Viewport.pivot = new Vector2(0, 1);
            }
        }

        protected virtual void OnScrollUpButtonClicked()
        {
            OnScrollButtonClicked(GetNextUpSelectable());
        }

        protected virtual void OnScrollDownButtonClicked()
        {
            OnScrollButtonClicked(GetNextDownSelectable());
        }

        protected virtual void OnScrollLeftButtonClicked()
        {
            OnScrollButtonClicked(GetNextLeftSelectable());
        }

        protected virtual void OnScrollRightButtonClicked()
        {
            OnScrollButtonClicked(GetNextRightSelectable());
        }

        protected virtual void OnScrollButtonClicked(Selectable nextSelectable)
        {
            if (nextSelectable != null)
            {
                if (GetChildContainingGameObject(nextSelectable.gameObject) != null)
                {
                    EventSystem.current.SetSelectedGameObject(nextSelectable.gameObject);
                }
            }
        }

        protected virtual void UpdateCache()
        {
            m_viewport = GetViewport();

            if (ScrollRect.content != null)
            {
                if (ScrollRect.content != m_content)
                {
                    m_content = ScrollRect.content;
                    m_contentLayoutGroup = ScrollRect.content.GetComponent<LayoutGroup>();
                }
            }
            else
            {
                m_content = null;
                m_contentLayoutGroup = null;
            }

            if (Content != null)
            {
                m_contentChildren = GetContentChildren(Content);
            }
        }

        protected virtual void UpdateScrollButtons()
        {
            if (m_ScrollUpButton != null)
            {
                m_ScrollUpButton.gameObject.SetActive(AnyChildIsPositionedAboveViewport());
            }
            if (m_ScrollDownButton != null)
            {
                m_ScrollDownButton.gameObject.SetActive(AnyChildIsPositionedBelowViewport());
            }
            if (m_ScrollLeftButton != null)
            {
                m_ScrollLeftButton.gameObject.SetActive(AnyChildIsPositionedLeftOfViewport());
            }
            if (m_ScrollRightButton != null)
            {
                m_ScrollRightButton.gameObject.SetActive(AnyChildIsPositionedRightOfViewport());
            }
        }
        
        protected virtual void SetupScrollControl()
        {
            if (m_AutoSelect)
            {
                // Scroller is taking over scrolling duties
                ScrollRect.inertia = false;
                ScrollRect.scrollSensitivity = 0;
            }
            else
            {
                ScrollRect.inertia = m_originalInertia;
                ScrollRect.scrollSensitivity = m_originalScrollSensitivity;
            }
        }

        protected virtual void UpdateFocus()
        {
            if (Content != null)
            {
                SetupScrollControl();

                if (EventSystem.current != null)
                {
                    GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
                    PointerEventData lastPointerEventData = (EventSystem.current.currentInputModule as StandaloneInputModule).GetLastPointerEventData();
                    bool dragging = (lastPointerEventData != null) && lastPointerEventData.pointerDrag == ScrollRect.gameObject && !Mathf.Approximately(lastPointerEventData.delta.sqrMagnitude, 0.0f);
                    bool scrollWheeling = (lastPointerEventData != null) && !Mathf.Approximately(lastPointerEventData.scrollDelta.sqrMagnitude, 0.0f);
                    Selectable selectedSelectable = null;

                    if (selectedGameObject != null)
                    {
                        selectedSelectable = selectedGameObject.GetComponent<Selectable>();
                    }

                    if (dragging)
                    {
                        if (m_scrollTween != null)
                        {
                            m_scrollTween.Kill();
                            m_scrollTween = null;
                        }

                        if (m_AutoSelect)
                        {
                            RectTransform child = GetChildClosestToFocusPoint(Content.anchoredPosition);
                            SelectChild(child);
                        }
                    }

                    if (scrollWheeling)
                    {
                        Vector2 delta = lastPointerEventData.scrollDelta;
                        // Down is positive for scroll events, while in UI system up is positive.
                        delta.y *= -1;
                        if (ScrollRect.vertical && !ScrollRect.horizontal)
                        {
                            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                                delta.y = delta.x;
                            delta.x = 0;
                        }
                        if (ScrollRect.horizontal && !ScrollRect.vertical)
                        {
                            if (Mathf.Abs(delta.y) > Mathf.Abs(delta.x))
                                delta.x = delta.y;
                            delta.y = 0;
                        }
                        // Reverse y again to use delta for direction
                        delta.y *= -1;

                        if (selectedSelectable != null)
                        {
                            Selectable nextSelectable = FindNearestSelectable(selectedSelectable, delta);
                            if (nextSelectable != null)
                            {
                                RectTransform child = GetChildContainingGameObject(nextSelectable.gameObject);
                                if (child != null)
                                {
                                    SelectChild(child);
                                    ScrollTo(child, true);
                                }
                            }
                        }
                    }

                    m_dragging = dragging;
                    m_scrollWheeling = scrollWheeling;
                }
            }
        }

        public virtual bool ShouldScrollOnSelect()
        {
            return (!m_dragging && !m_scrollWheeling);
        }

        public virtual void SelectChild(RectTransform child)
        {
            if (child == null)
            {
                ChildDebugError();
                return;
            }

            Selectable childSelectable = child.GetComponentInChildren<Selectable>();

            if (childSelectable != null)
            {
                GameObject childSelectableGameObject = childSelectable.gameObject;
                
                EventSystem.current.SetSelectedGameObject(childSelectableGameObject);
            }
        }

        public virtual void ScrollTo(RectTransform child, bool instantly)
        {
            if (child == null)
            {
                ChildDebugError();
                return;
            }

            if (Content == null)
            {
                ScrollRectContentDebugError();
                return;
            }

            ScrollRect.StopMovement();

            Vector2 focusedContentPosition = GetFocusedContentPosition(child);

            if (instantly)
            {
                if (m_scrollTween != null && m_scrollTween.IsPlaying())
                {
                    m_scrollTween.Kill();
                    m_scrollTween = null;
                }

                Content.anchoredPosition = focusedContentPosition;
            }
            else
            {
                m_scrollTween = Content.DOAnchorPos(focusedContentPosition, m_ScrollDuration);
            }
        }

        protected virtual Vector2 GetFocusedContentPosition(RectTransform child)
        {
            Vector2 newAnchoredPosition = Content.anchoredPosition;
            
            if (child == null)
            {
                ChildDebugError();
                return newAnchoredPosition;
            }

            if (Content == null)
            {
                ScrollRectContentDebugError();
                return newAnchoredPosition;
            }
            
            if (ContentLayoutGroup != null)
            {
                m_ChildAlignment = ContentLayoutGroup.childAlignment;
                m_Padding = ContentLayoutGroup.padding;
            }

            if (ScrollRect.horizontal)
            {
                if (m_Mode == ScrollMode.Align)
                {
                    if (IsLeftAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.x = GetHorizontalLeftAlignedPosition(child, Content, Viewport) + m_Padding.left;
                    }
                    else if (IsRightAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.x = GetHorizontalRightAlignedPosition(child, Content, Viewport) - m_Padding.right;
                    }
                    else if (IsMiddleCenterAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.x = GetHorizontalCenterAlignedPosition(child, Content, Viewport);
                    }
                }
                else if (m_Mode == ScrollMode.Reveal)
                {
                    Bounds viewportBounds = GetViewportBounds(Viewport);
                    Bounds childBounds = GetChildBounds(child, Viewport);

                    if (ChildIsPositionedLeftOfViewport(childBounds, viewportBounds))
                    {
                        newAnchoredPosition.x = GetHorizontalLeftAlignedPosition(child, Content, Viewport) + m_Padding.left;
                    }
                    else if (ChildIsPositionedRightOfViewport(childBounds, viewportBounds))
                    {
                        newAnchoredPosition.x = GetHorizontalRightAlignedPosition(child, Content, Viewport) - m_Padding.right;
                    }
                }
            }

            if (ScrollRect.vertical)
            {
                if (m_Mode == ScrollMode.Align)
                {
                    if (IsUpperAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.y = GetVerticalUpperAlignedPosition(child, Content, Viewport) - m_Padding.top;
                    }
                    else if (IsLowerAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.y = GetVerticalLowerAlignedPosition(child, Content, Viewport) + m_Padding.bottom;
                    }
                    else if (IsMiddleCenterAligned(m_ChildAlignment))
                    {
                        newAnchoredPosition.y = GetVerticalMiddleAlignedPosition(child, Content, Viewport);
                    }
                }
                else if (m_Mode == ScrollMode.Reveal)
                {
                    Bounds viewportBounds = GetViewportBounds(Viewport);
                    Bounds childBounds = GetChildBounds(child, Viewport);

                    if (ChildIsPositionedAboveViewport(childBounds, viewportBounds))
                    {
                        newAnchoredPosition.y = GetVerticalUpperAlignedPosition(child, Content, Viewport) - m_Padding.top;
                    }
                    else if (ChildIsPositionedBelowViewport(childBounds, viewportBounds))
                    {
                        newAnchoredPosition.y = GetVerticalLowerAlignedPosition(child, Content, Viewport) + m_Padding.bottom;
                    }
                }
            }

            if (ScrollRect.movementType != ScrollRect.MovementType.Unrestricted)
            {
                newAnchoredPosition += CalculateOffset(ScrollRect, newAnchoredPosition - Content.anchoredPosition);
            }
            
            return newAnchoredPosition;
        }

        protected RectTransform GetViewport()
        {
            RectTransform rectTransform = ScrollRect.viewport;

            if (rectTransform == null)
            {
                rectTransform = (RectTransform)ScrollRect.transform;
            }

            return rectTransform;
        }

        public RectTransform GetChildContainingGameObject(GameObject gameObject)
        {
            if (gameObject == null)
            {
                GameObjectDebugError();
                return null;
            }

            foreach (RectTransform child in ContentChildren)
            {
                if (child.gameObject == gameObject)
                {
                    return child;
                }

                foreach (Transform grandChild in child.GetComponentsInChildren<Transform>())
                {
                    if (grandChild.gameObject == gameObject)
                    {
                        return child;
                    }
                }
            }

            return null;
        }

        public RectTransform GetSelectedChild()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject != null)
            {
                return GetChildContainingGameObject(selectedGameObject);
            }

            return null;
        }

        public RectTransform GetChildClosestToFocusPoint(Vector2 focusPoint)
        {
            float minDistance = float.MaxValue;
            RectTransform closestChild = null;
            foreach (RectTransform child in ContentChildren)
            {
                Vector2 focusedContentPosition = GetFocusedContentPosition(child);
                float distance = Vector2.Distance(focusPoint, focusedContentPosition);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestChild = child;
                }
            }
            return closestChild;
        }

        public bool AnyChildIsPositionedLeftOfViewport()
        {
            Bounds viewportBounds = GetViewportBounds(Viewport);

            foreach (RectTransform child in ContentChildren)
            {
                if (child != null)
                {
                    Bounds childBounds = GetChildBounds(child, Viewport);
                    if (ChildIsPositionedLeftOfViewport(childBounds, viewportBounds))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool AnyChildIsPositionedRightOfViewport()
        {
            Bounds viewportBounds = GetViewportBounds(Viewport);

            foreach (RectTransform child in ContentChildren)
            {
                if (child != null)
                {
                    Bounds childBounds = GetChildBounds(child, Viewport);
                    if (ChildIsPositionedRightOfViewport(childBounds, viewportBounds))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool AnyChildIsPositionedAboveViewport()
        {
            Bounds viewportBounds = GetViewportBounds(Viewport);

            foreach (RectTransform child in ContentChildren)
            {
                if (child != null)
                {
                    Bounds childBounds = GetChildBounds(child, Viewport);
                    if (ChildIsPositionedAboveViewport(childBounds, viewportBounds))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool AnyChildIsPositionedBelowViewport()
        {
            Bounds viewportBounds = GetViewportBounds(Viewport);

            foreach (RectTransform child in ContentChildren)
            {
                if (child != null)
                {
                    Bounds childBounds = GetChildBounds(child, Viewport);
                    if (ChildIsPositionedBelowViewport(childBounds, viewportBounds))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ChildIsPositionedLeftOfViewport(RectTransform child)
        {
            if (child == null)
            {
                return false;
            }

            Bounds childBounds = GetChildBounds(child, Viewport);
            Bounds viewportBounds = GetViewportBounds(Viewport);

            return ChildIsPositionedLeftOfViewport(childBounds, viewportBounds);
        }

        public bool ChildIsPositionedRightOfViewport(RectTransform child)
        {
            if (child == null)
            {
                return false;
            }

            Bounds childBounds = GetChildBounds(child, Viewport);
            Bounds viewportBounds = GetViewportBounds(Viewport);

            return ChildIsPositionedRightOfViewport(childBounds, viewportBounds);
        }

        public bool ChildIsPositionedAboveViewport(RectTransform child)
        {
            if (child == null)
            {
                return false;
            }

            Bounds childBounds = GetChildBounds(child, Viewport);
            Bounds viewportBounds = GetViewportBounds(Viewport);

            return ChildIsPositionedAboveViewport(childBounds, viewportBounds);
        }

        public bool ChildIsPositionedBelowViewport(RectTransform child)
        {
            if (child == null)
            {
                return false;
            }

            Bounds childBounds = GetChildBounds(child, Viewport);
            Bounds viewportBounds = GetViewportBounds(Viewport);

            return ChildIsPositionedBelowViewport(childBounds, viewportBounds);
        }

        private static bool ChildIsPositionedLeftOfViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return (childBounds.min.x < viewportBounds.min.x);
        }

        private static bool ChildIsPositionedRightOfViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return (childBounds.max.x > viewportBounds.max.x);
        }

        private static bool ChildIsPositionedAboveViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return (childBounds.max.y > viewportBounds.max.y);
        }

        private static bool ChildIsPositionedBelowViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return (childBounds.min.y < viewportBounds.min.y);
        }

        private static float DistanceBetweenLeftChildAndViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return Math.Abs(childBounds.max.x - viewportBounds.min.x);
        }

        private static float DistanceBetweenRightChildAndViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return Math.Abs(childBounds.min.x - viewportBounds.max.x);
        }

        private static float DistanceBetweenAboveChildAndViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return Math.Abs(childBounds.min.y - viewportBounds.max.y);
        }

        private static float DistanceBetweenBelowChildAndViewport(Bounds childBounds, Bounds viewportBounds)
        {
            return Math.Abs(childBounds.max.y - viewportBounds.min.y);
        }

        public Selectable FindNearestSelectable(Selectable selectable, Vector3 dir)
        {
            Selectable nextSelectableInScrollRect = selectable.FindNearestSelectable(GetContentSelectables(Content), dir);
            if (nextSelectableInScrollRect != null)
            {
                return nextSelectableInScrollRect;
            }

            return selectable.FindNearestSelectable(Selectable.allSelectablesArray, dir);
        }

        public Selectable GetNextLeftSelectable()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject != null)
            {
                Selectable selectable = selectedGameObject.GetComponent<Selectable>();
                if (selectable != null)
                {
                    Selectable nextSelectable = FindNearestSelectable(selectable, Vector3.left);
                    if (nextSelectable != null)
                    {
                        return nextSelectable;
                    }
                }
            }

            return null;
        }

        public Selectable GetNextRightSelectable()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject != null)
            {
                Selectable selectable = selectedGameObject.GetComponent<Selectable>();
                if (selectable != null)
                {
                    Selectable nextSelectable = FindNearestSelectable(selectable, Vector3.right);
                    if (nextSelectable != null)
                    {
                        return nextSelectable;
                    }
                }
            }

            return null;
        }

        public Selectable GetNextUpSelectable()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject != null)
            {
                Selectable selectable = selectedGameObject.GetComponent<Selectable>();
                if (selectable != null)
                {
                    Selectable nextSelectable = FindNearestSelectable(selectable, Vector3.up);
                    if (nextSelectable != null)
                    {
                        return nextSelectable;
                    }
                }
            }

            return null;
        }

        public Selectable GetNextDownSelectable()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject != null)
            {
                Selectable selectable = selectedGameObject.GetComponent<Selectable>();
                if (selectable != null)
                {
                    Selectable nextSelectable = FindNearestSelectable(selectable, Vector3.down);
                    if (nextSelectable != null)
                    {
                        return nextSelectable;
                    }
                }
            }

            return null;
        }

        public static bool IsMiddleCenterAligned(TextAnchor alignment)
        {
            return (alignment == TextAnchor.MiddleCenter);
        }

        public static bool IsLeftAligned(TextAnchor alignment)
        {
            return (alignment == TextAnchor.MiddleLeft ||
                    alignment == TextAnchor.UpperLeft ||
                    alignment == TextAnchor.LowerLeft);
        }

        public static bool IsRightAligned(TextAnchor alignment)
        {
            return (alignment == TextAnchor.MiddleRight ||
                    alignment == TextAnchor.UpperRight ||
                    alignment == TextAnchor.LowerRight);
        }

        public static bool IsUpperAligned(TextAnchor alignment)
        {
            return (alignment == TextAnchor.UpperCenter ||
                    alignment == TextAnchor.UpperLeft ||
                    alignment == TextAnchor.UpperRight);
        }

        public static bool IsLowerAligned(TextAnchor alignment)
        {
            return (alignment == TextAnchor.LowerCenter ||
                    alignment == TextAnchor.LowerLeft ||
                    alignment == TextAnchor.LowerRight);
        }

        protected static float GetHorizontalLeftAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }

            float childPivotOffset = child.rect.size.x * child.pivot.x;
            float childMaxOffset = child.anchoredPosition.x - childPivotOffset;

            float contentAnchoredPosition = -childMaxOffset;

            contentAnchoredPosition -= viewport.rect.size.x * content.anchorMin.x;
            contentAnchoredPosition += content.rect.size.x * content.pivot.x;

            return contentAnchoredPosition;
        }

        protected static float GetHorizontalRightAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }
            
            return GetHorizontalLeftAlignedPosition(child, content, viewport) + viewport.rect.size.x - child.rect.size.x;
        }

        protected static float GetHorizontalCenterAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }

            return GetHorizontalRightAlignedPosition(child, content, viewport) - (viewport.rect.size.x / 2) + (child.rect.size.x / 2);
        }

        protected static float GetVerticalUpperAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }

            float childPivotOffset = child.rect.size.y * (1 - child.pivot.y);
            float childMaxOffset = child.anchoredPosition.y + childPivotOffset;
            
            float contentAnchoredPosition = -childMaxOffset;

            contentAnchoredPosition += viewport.rect.size.y * (1 - content.anchorMax.y);
            contentAnchoredPosition -= content.rect.size.y * (1 - content.pivot.y);

            return contentAnchoredPosition;
        }

        protected static float GetVerticalLowerAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }

            return GetVerticalUpperAlignedPosition(child, content, viewport) - viewport.rect.size.y + child.rect.size.y;
        }

        protected static float GetVerticalMiddleAlignedPosition(RectTransform child, RectTransform content, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return 0;
            }

            if (content == null)
            {
                ContentDebugError();
                return 0;
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return 0;
            }

            return GetVerticalUpperAlignedPosition(child, content, viewport) - (viewport.rect.size.y / 2) + (child.rect.size.y / 2);
        }

        protected static float GetAnchor(float anchorMin, float anchorMax, float pivot)
        {
            return pivot * (anchorMax - anchorMin) + anchorMin;
        }

        protected static Bounds GetScrollRectViewBounds(ScrollRect scrollRect)
        {
            if (scrollRect == null)
            {
                ScrollRectDebugError();
                return new Bounds();
            }

            return (Bounds)typeof(ScrollRect).GetField("m_ViewBounds", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(scrollRect);
        }

        protected static Bounds GetScrollRectContentBounds(ScrollRect scrollRect)
        {
            if (scrollRect == null)
            {
                ScrollRectDebugError();
                return new Bounds();
            }

            return (Bounds)typeof(ScrollRect).GetField("m_ContentBounds", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(scrollRect);
        }

        protected static List<RectTransform> GetContentChildren(RectTransform content)
        {
            List<RectTransform> rectChildren = new List<RectTransform>();
            for (int i = 0; i < content.childCount; i++)
            {
                var rect = content.GetChild(i) as RectTransform;
                if (rect == null)
                {
                    continue;
                }

                rectChildren.Add(rect);
            }
            return rectChildren;
        }

        protected Selectable[] GetContentSelectables(RectTransform content)
        {
            return content.GetComponentsInChildren<Selectable>(true);
        }

        protected static Bounds GetViewportBounds(RectTransform viewport)
        {
            if (viewport == null)
            {
                ViewportDebugError();
                return new Bounds();
            }

            return new Bounds(viewport.rect.center, viewport.rect.size);
        }

        protected static Bounds GetChildBounds(RectTransform child, RectTransform viewport)
        {
            if (child == null)
            {
                ChildDebugError();
                return new Bounds();
            }

            if (viewport == null)
            {
                ViewportDebugError();
                return new Bounds();
            }

            Vector3[] corners = new Vector3[4];
            child.GetWorldCorners(corners);
            var viewWorldToLocalMatrix = viewport.worldToLocalMatrix;
            return InternalGetBounds(corners, ref viewWorldToLocalMatrix);
        }

        internal static Bounds InternalGetBounds(Vector3[] corners, ref Matrix4x4 viewWorldToLocalMatrix)
        {
            var vMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            var vMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (int j = 0; j < 4; j++)
            {
                Vector3 v = viewWorldToLocalMatrix.MultiplyPoint3x4(corners[j]);
                vMin = Vector3.Min(v, vMin);
                vMax = Vector3.Max(v, vMax);
            }

            var bounds = new Bounds(vMin, Vector3.zero);
            bounds.Encapsulate(vMax);
            return bounds;
        }

        protected static Vector2 CalculateOffset(ScrollRect scrollRect, Vector2 delta)
        {
            if (scrollRect == null)
            {
                ScrollRectDebugError();
                return Vector2.zero;
            }

            return InternalCalculateOffset(GetScrollRectViewBounds(scrollRect), GetScrollRectContentBounds(scrollRect), scrollRect.horizontal, scrollRect.vertical, scrollRect.movementType, delta);
        }

        internal static Vector2 InternalCalculateOffset(Bounds viewBounds, Bounds contentBounds, bool horizontal, bool vertical, ScrollRect.MovementType movementType, Vector2 delta)
        {
            Vector2 offset = Vector2.zero;
            if (movementType == ScrollRect.MovementType.Unrestricted)
                return offset;

            Vector2 min = contentBounds.min;
            Vector2 max = contentBounds.max;

            if (horizontal)
            {
                min.x += delta.x;
                max.x += delta.x;
                if (max.x < viewBounds.max.x)
                    offset.x = viewBounds.max.x - max.x;
                else if (min.x > viewBounds.min.x)
                    offset.x = viewBounds.min.x - min.x;
            }

            if (vertical)
            {
                min.y += delta.y;
                max.y += delta.y;
                if (max.y < viewBounds.max.y)
                    offset.y = viewBounds.max.y - max.y;
                else if (min.y > viewBounds.min.y)
                    offset.y = viewBounds.min.y - min.y;
            }

            return offset;
        }

        public bool CanScroll()
        {
            return CanScroll(ScrollRect);
        }

        public static bool CanScroll(ScrollRect scrollRect)
        {
            if (scrollRect == null)
            {
                ScrollRectDebugError();
                return false;
            }

            return InternalCanScroll(GetScrollRectViewBounds(scrollRect), GetScrollRectContentBounds(scrollRect), scrollRect.horizontal, scrollRect.vertical, scrollRect.movementType);
        }

        internal static bool InternalCanScroll(Bounds viewBounds, Bounds contentBounds, bool horizontal, bool vertical, ScrollRect.MovementType movementType)
        {
            if (movementType == ScrollRect.MovementType.Unrestricted)
            {
                return true;
            }
            
            if (horizontal)
            {
                if (contentBounds.size.x > viewBounds.size.x)
                {
                    return true;
                }
            }

            if (vertical)
            {
                if (contentBounds.size.x > viewBounds.size.x)
                {
                    return true;
                }
            }

            return false;
        }

        protected static void ChildDebugError()
        {
            Debug.LogError("child must not be null");
        }
        
        protected static void ScrollRectDebugError()
        {
            Debug.LogError("scrollRect must not be null");
        }

        protected static void ContentDebugError()
        {
            Debug.LogError("content must not be null");
        }

        protected static void ViewportDebugError()
        {
            Debug.LogError("viewport must not be null");
        }

        protected static void GameObjectDebugError()
        {
            Debug.LogError("gameObject must not be null");
        }

        protected static void StandaloneInputModuleDebugError()
        {
            Debug.LogError("standaloneInputModule must not be null");
        }

        protected static void ScrollRectContentDebugError()
        {
            Debug.LogError("ScrollRect must have content");
        }
    }

}
