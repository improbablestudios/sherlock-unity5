﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Select Last Selected")]
    public class SelectLastSelected : PersistentBehaviour
    {
        [Tooltip("If true, select the last selected selectable when the user presses up on this selectable")]
        [SerializeField]
        [FormerlySerializedAs("selectOnUp")]
        protected bool m_SelectOnUp;

        [Tooltip("If true, select the last selected selectable when the user presses down on this selectable")]
        [SerializeField]
        [FormerlySerializedAs("selectOnDown")]
        protected bool m_SelectOnDown;

        [Tooltip("If true, select the last selected selectable when the user presses left on this selectable")]
        [SerializeField]
        [FormerlySerializedAs("selectOnLeft")]
        protected bool m_SelectOnLeft;

        [Tooltip("If true, select the last selected selectable when the user presses right on this selectable")]
        [SerializeField]
        [FormerlySerializedAs("selectOnRight")]
        protected bool m_SelectOnRight;
        
        protected Selectable m_previouslySelected;

        private Selectable m_selectable;

        public bool SelectOnUp
        {
            get
            {
                return m_SelectOnUp;
            }
            set
            {
                m_SelectOnUp = value;
            }
        }
        
        public bool SelectOnDown
        {
            get
            {
                return m_SelectOnDown;
            }
            set
            {
                m_SelectOnDown = value;
            }
        }

        public bool SelectOnLeft
        {
            get
            {
                return m_SelectOnLeft;
            }
            set
            {
                m_SelectOnLeft = value;
            }
        }

        public bool SelectOnRight
        {
            get
            {
                return m_SelectOnRight;
            }
            set
            {
                m_SelectOnRight = value;
            }
        }

        public Selectable Selectable
        {
            get
            {
                if (m_selectable == null)
                {
                    m_selectable = GetComponent<Selectable>();
                }
                return m_selectable;
            }
        }

        protected virtual void Update()
        {
            if (EventSystem.current != null)
            {
                GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
                
                if (selectedGameObject != null)
                {
                    Selectable selectedSelectable = selectedGameObject.GetComponent<Selectable>();

                    if (m_previouslySelected != selectedSelectable && selectedSelectable != Selectable)
                    {
                        Navigation nav = Selectable.navigation;

                        nav.mode = Navigation.Mode.Explicit;

                        if (m_SelectOnDown)
                        {
                             nav.selectOnDown = selectedSelectable;
                        }
                        else if (m_SelectOnLeft)
                        {
                            nav.selectOnLeft = selectedSelectable;
                        }
                        else if (m_SelectOnRight)
                        {
                            nav.selectOnRight = selectedSelectable;
                        }
                        else if (m_SelectOnUp)
                        {
                            nav.selectOnUp = selectedSelectable;
                        }

                        Selectable.navigation = nav;

                        m_previouslySelected = selectedSelectable;
                    }
                }
            }
        }
    }

}