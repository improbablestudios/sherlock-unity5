﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{
    public class UIConstructController : PersistentBehaviour
    {
        [SerializeField]
        protected UIConstruct m_UI;

        public UIConstruct UI
        {
            get
            {
                return m_UI;
            }
            set
            {
                m_UI = value;
            }
        }

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return true;
        }

        protected override bool WillInitializePersistableFieldsOnAwake()
        {
            return false;
        }
    }
}
