﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Toggle On Select")]
    public class ToggleOnSelect : BaseOnEvent, ISelectHandler
    {
        [Tooltip("Invoke the toggle's OnValueChanged unity event on axis input events")]
        [SerializeField]
        protected bool m_InvokeListenersOnAxisEvent = true;

        [Tooltip("Invoke the toggle's OnValueChanged unity event on pointer input events")]
        [SerializeField]
        protected bool m_InvokeListenersOnPointerEvent = true;

        [Tooltip("Invoke the toggle's OnValueChanged unity event on events that are not axis input events or pointer input events")]
        [SerializeField]
        protected bool m_InvokeListenersOnOtherEvent = true;

        [SerializeField]
        protected Toggle m_Toggle;

        public bool InvokeListenersOnAxisEvent
        {
            get
            {
                return m_InvokeListenersOnAxisEvent;
            }
            set
            {
                m_InvokeListenersOnAxisEvent = value;
            }
        }

        public bool InvokeListenersOnPointerEvent
        {
            get
            {
                return m_InvokeListenersOnPointerEvent;
            }
            set
            {
                m_InvokeListenersOnPointerEvent = value;
            }
        }

        public bool InvokeListenersOnOtherEvent
        {
            get
            {
                return m_InvokeListenersOnOtherEvent;
            }
            set
            {
                m_InvokeListenersOnOtherEvent = value;
            }
        }

        public Toggle Toggle
        {
            get
            {
                return m_Toggle;
            }
            set
            {
                m_Toggle = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Toggle == null)
            {
                m_Toggle = GetComponent<Toggle>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Toggle == null)
            {
                m_Toggle = GetComponent<Toggle>();
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            if (ShouldHandleEvent(eventData))
            {
                if (ShouldInvokeListeners(eventData))
                {
                    Toggle.isOn = true;
                }
                else
                {
                    Toggle.SetIsOnWithoutNotify(true);
                }
            }
        }

        public bool ShouldInvokeListeners(BaseEventData eventData)
        {
            return (m_InvokeListenersOnAxisEvent && eventData is AxisEventData) ||
                (m_InvokeListenersOnPointerEvent && eventData is PointerEventData) ||
                (m_InvokeListenersOnOtherEvent && !(eventData is AxisEventData) && !(eventData is PointerEventData));
        }
    }

}
