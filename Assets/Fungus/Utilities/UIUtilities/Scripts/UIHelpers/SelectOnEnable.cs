﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Select On Enable")]
    public class SelectOnEnable : PersistentBehaviour
    {
        [Tooltip("The selectable to select")]
        [SerializeField]
        protected Selectable m_Selectable;

        [Tooltip("The priority used to determine which button with a select on enable component should be selected")]
        [SerializeField]
        [FormerlySerializedAs("priority")]
        protected int m_Priority = 1;
        
        public int Priority
        {
            get
            {
                return m_Priority;
            }
            set
            {
                m_Priority = value;
            }
        }

        public Selectable Selectable
        {
            get
            {
                return m_Selectable;
            }
            set
            {
                m_Selectable = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }
        
        protected override void Reset()
        {
            base.Reset();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            StartCoroutine(SelectAfterDelay());
        }

        private IEnumerator SelectAfterDelay()
        {
            yield return new WaitForEndOfFrame();

            if (EventSystem.current != null)
            {
                GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
                SelectOnEnable selectedSelectOnEnable = null;
                if (selectedGameObject != null)
                {
                    selectedSelectOnEnable = selectedGameObject.GetComponent<SelectOnEnable>();
                }
                if (selectedSelectOnEnable == null || !selectedSelectOnEnable.gameObject.activeInHierarchy || this.m_Priority <= selectedSelectOnEnable.m_Priority)
                {
                    if (selectedGameObject != gameObject)
                    {
                        EventSystem.current.SetSelectedGameObject(gameObject);
                    }
                }
            }
        }
    }

}