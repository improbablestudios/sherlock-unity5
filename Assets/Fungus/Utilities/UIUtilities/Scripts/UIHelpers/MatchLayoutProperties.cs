﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Layout/Match Layout Properties", 140)]
    [RequireComponent(typeof(RectTransform))]
    [ExecuteAlways]
    public class MatchLayoutProperties : UIBehaviour, ILayoutElement, IPersistableLoader
    {
        public enum MatchSource
        {
            RectSize,
            LayoutSize,
        }

        [SerializeField]
        protected RectTransform m_RectTransform;

        [SerializeField]
        protected MatchSource m_PreferredSizeSource;
        
        [SerializeField]
        protected bool m_MatchMinWidth = true;

        [SerializeField]
        protected bool m_MatchMinHeight = true;

        [SerializeField]
        protected bool m_MatchPreferredWidth = true;

        [SerializeField]
        protected bool m_MatchPreferredHeight = true;

        [SerializeField]
        protected bool m_MatchFlexibleWidth = true;

        [SerializeField]
        protected bool m_MatchFlexibleHeight = true;

        [SerializeField]
        protected int m_LayoutPriority = 1;

        public RectTransform RectTransform
        {
            get
            {
                return m_RectTransform;
            }
            set
            {
                m_RectTransform = value;
            }
        }

        public MatchSource PreferredSizeSource
        {
            get
            {
                return m_PreferredSizeSource;
            }
            set
            {
                m_PreferredSizeSource = value;
            }
        }

        public bool MatchMinWidth
        {
            get
            {
                return m_MatchMinWidth;
            }
            set
            {
                m_MatchMinWidth = value;
            }
        }

        public bool MatchMinHeight
        {
            get
            {
                return m_MatchMinHeight;
            }
            set
            {
                m_MatchMinHeight = value;
            }
        }

        public bool MatchPreferredWidth
        {
            get
            {
                return m_MatchPreferredWidth;
            }
            set
            {
                m_MatchPreferredWidth = value;
            }
        }

        public bool MatchPreferredHeight
        {
            get
            {
                return m_MatchPreferredHeight;
            }
            set
            {
                m_MatchPreferredHeight = value;
            }
        }

        public bool MatchFlexibleWidth
        {
            get
            {
                return m_MatchFlexibleWidth;
            }
            set
            {
                m_MatchFlexibleWidth = value;
            }
        }

        public bool MatchFlexibleHeight
        {
            get
            {
                return m_MatchFlexibleHeight;
            }
            set
            {
                m_MatchFlexibleHeight = value;
            }
        }

        public int LayoutPriority
        {
            get
            {
                return m_LayoutPriority;
            }
            set
            {
                m_LayoutPriority = value;
            }
        }

        public virtual float minWidth
        {
            get
            {
                if (m_MatchMinWidth)
                {
                    return GetLayoutProperty(RectTransform, x => x.minWidth, 0);
                }
                return -1;
            }
        }
        
        public virtual float minHeight
        {
            get
            {
                if (m_MatchMinHeight)
                {
                    return GetLayoutProperty(RectTransform, x => x.minHeight, 0);
                }
                return -1;
            }
        }
        
        public virtual float preferredWidth
        {
            get
            {
                if (m_MatchPreferredWidth)
                {
                    if (m_PreferredSizeSource == MatchSource.LayoutSize)
                    {
                        return GetLayoutProperty(RectTransform, x => x.preferredWidth, 0);
                    }
                    else if (m_PreferredSizeSource == MatchSource.RectSize)
                    {
                        if (RectTransform != null)
                        {
                            return RectTransform.rect.width;
                        }
                    }
                }
                return -1;
            }
        }
        
        public virtual float preferredHeight
        {
            get
            {
                if (m_MatchPreferredHeight)
                {
                    if (m_PreferredSizeSource == MatchSource.LayoutSize)
                    {
                        return GetLayoutProperty(RectTransform, x => x.preferredHeight, 0);
                    }
                    else if (m_PreferredSizeSource == MatchSource.RectSize)
                    {
                        return RectTransform.rect.height;
                    }
                }
                return -1;
            }
        }
        
        public virtual float flexibleWidth
        {
            get
            {
                if (m_MatchFlexibleWidth)
                {
                    return GetLayoutProperty(RectTransform, x => x.flexibleWidth, 0);
                }
                return -1;
            }
        }
        
        public virtual float flexibleHeight
        {
            get
            {
                if (m_MatchFlexibleHeight)
                {
                    return GetLayoutProperty(RectTransform, x => x.flexibleHeight, 0);
                }
                return -1;
            }
        }
        
        public virtual int layoutPriority
        {
            get
            {
                return m_LayoutPriority;
            }
            set
            {
                m_LayoutPriority = value;
            }
        }

        public virtual void CalculateLayoutInputHorizontal()
        {
        }

        public virtual void CalculateLayoutInputVertical()
        {
        }

        protected override void Awake()
        {
            base.Awake();

            this.LoadAndInitializePersistableFields();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SetDirty();
        }

        protected override void OnTransformParentChanged()
        {
            SetDirty();
        }

        protected override void OnDisable()
        {
            SetDirty();
            base.OnDisable();
        }

        protected override void OnDidApplyAnimationProperties()
        {
            SetDirty();
        }

        protected override void OnBeforeTransformParentChanged()
        {
            SetDirty();
        }
        
        protected void SetDirty()
        {
            if (!IsActive())
                return;
            LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
        }

        protected float GetLayoutProperty(RectTransform rect, System.Func<ILayoutElement, float> property, float defaultValue)
        {
            ILayoutElement dummy;
            return GetLayoutProperty(rect, property, defaultValue, out dummy);
        }

        protected float GetLayoutProperty(RectTransform rect, System.Func<ILayoutElement, float> property, float defaultValue, out ILayoutElement source)
        {
            source = null;
            if (rect == null)
                return 0;
            float min = defaultValue;
            int maxPriority = System.Int32.MinValue;
            Component[] components = rect.GetComponents(typeof(ILayoutElement));

            for (int i = 0; i < components.Length; i++)
            {
                var layoutComp = components[i] as ILayoutElement;
                if (layoutComp is Behaviour && !((Behaviour)layoutComp).enabled)
                    continue;

                int priority = layoutComp.layoutPriority;
                // If this layout components has lower priority than a previously used, ignore it.
                if (priority < maxPriority)
                    continue;
                float prop = property(layoutComp);
                // If this layout property is set to a negative value, it means it should be ignored.
                if (prop < 0)
                    continue;

                // If this layout component has higher priority than all previous ones,
                // overwrite with this one's value.
                if (priority > maxPriority)
                {
                    min = prop;
                    maxPriority = priority;
                    source = layoutComp;
                }
                // If the layout component has the same priority as a previously used,
                // use the largest of the values with the same priority.
                else if (prop > min)
                {
                    min = prop;
                    source = layoutComp;
                }
            }
            
            return min;
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();

            SetDirty();
        }

#endif
    }

}