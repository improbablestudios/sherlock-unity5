using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    public class CustomTag : IdentifiableScriptableObject, INestedAsset
    {
        [Tooltip("The start tag (e.g. {custom tag})")]
        [SerializeField]
        [FormerlySerializedAs("tagStartSymbol")]
        protected string m_TagStartSymbol;

        [Tooltip("The end tag (e.g. {/custom tag})")]
        [SerializeField]
        [FormerlySerializedAs("tagEndSymbol")]
        protected string m_TagEndSymbol;

        [Tooltip("Replace the start tag with this text")]
        [SerializeField]
        [FormerlySerializedAs("replaceTagStartWith")]
        protected string m_ReplaceTagStartWith;

        [Tooltip("Replace the end tag with this text")]
        [SerializeField]
        [FormerlySerializedAs("replaceTagEndWith")]
        protected string m_ReplaceTagEndWith;
        
        public string TagStartSymbol
        {
            get
            {
                return m_TagStartSymbol;
            }
            set
            {
                m_TagStartSymbol = value;
            }
        }
        
        public string TagEndSymbol
        {
            get
            {
                return m_TagEndSymbol;
            }
            set
            {
                m_TagEndSymbol = value;
            }
        }

        public string ReplaceTagStartWith
        {
            get
            {
                return m_ReplaceTagStartWith;
            }
            set
            {
                m_ReplaceTagStartWith = value;
            }
        }

        public string ReplaceTagEndWith
        {
            get
            {
                return m_ReplaceTagEndWith;
            }
            set
            {
                m_ReplaceTagEndWith = value;
            }
        }
    }
    
}