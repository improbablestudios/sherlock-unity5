﻿using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Helper/Show On Enable")]
    public class ShowOnEnable : PersistentBehaviour
    {
        [Tooltip("The ui to show when this object is enabled")]
        [SerializeField]
        protected UIConstruct m_UI;

        public UIConstruct UI
        {
            get
            {
                return m_UI;
            }
            set
            {
                m_UI = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_UI != null)
            {
                m_UI.Show();
            }
        }
    }

}