﻿using System.Collections;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{
    public class FocusedInputField : InputField
    {
        [Tooltip("Should this input field be selected when it becomes focused")]
        [SerializeField]
        protected bool m_SelectOnFocus;

        [Tooltip("Should this input field be focused when it becomes selected")]
        [SerializeField]
        protected bool m_FocusOnSelect;

        [Tooltip("Should this input field be unfocused when it becomes deselected")]
        [SerializeField]
        protected bool m_UnfocusOnDeselect;

        [Tooltip("Should all text in this input field be selected when it becomes focused")]
        [SerializeField]
        protected bool m_SelectAllTextOnFocus;

        public bool SelectOnFocus
        {
            get
            {
                return m_SelectOnFocus;
            }
            set
            {
                m_SelectOnFocus = value;
            }
        }

        public bool FocusOnSelect
        {
            get
            {
                return m_FocusOnSelect;
            }
            set
            {
                m_FocusOnSelect = value;
            }
        }

        public bool UnfocusOnDeselect
        {
            get
            {
                return m_UnfocusOnDeselect;
            }
            set
            {
                m_UnfocusOnDeselect = value;
            }
        }

        public bool SelectAllTextOnFocus
        {
            get
            {
                return m_SelectAllTextOnFocus;
            }
            set
            {
                m_SelectAllTextOnFocus = value;
            }
        }

        public override void OnSelect(BaseEventData eventData)
        {
            HasSelection = true;
            EvaluateAndTransitionToSelectionState(eventData);
            if (m_FocusOnSelect)
            {
                ActivateInputField();
            }
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            if (m_UnfocusOnDeselect)
            {
                DeactivateInputField();
            }
            HasSelection = false;
            EvaluateAndTransitionToSelectionState(eventData);
        }

        protected override void LateUpdate()
        {
            // Only activate if we are not already activated.
            if (ShouldActivateNextUpdate)
            {
                if (!isFocused)
                {
                    ActivateInputFieldInternal();
                    ShouldActivateNextUpdate = false;
                    return;
                }

                // Reset as we are already activated.
                ShouldActivateNextUpdate = false;
            }

            if (InPlaceEditing() || !isFocused)
                return;

            AssignPositioningIfNeeded();

            if (m_Keyboard == null || m_Keyboard.status != TouchScreenKeyboard.Status.Visible)
            {
                if (m_Keyboard != null)
                {
                    if (!readOnly)
                        text = m_Keyboard.text;

                    if (m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
                        WasCanceled = true;
                }

                OnDeselect(null);
                return;
            }

            string val = m_Keyboard.text;

            if (m_Text != val)
            {
                if (readOnly)
                {
                    m_Keyboard.text = m_Text;
                }
                else
                {
                    m_Text = "";

                    for (int i = 0; i < val.Length; ++i)
                    {
                        char c = val[i];

                        if (c == '\r' || (int)c == 3)
                            c = '\n';

                        if (onValidateInput != null)
                            c = onValidateInput(m_Text, m_Text.Length, c);
                        else if (characterValidation != CharacterValidation.None)
                            c = Validate(m_Text, m_Text.Length, c);

                        if (lineType == LineType.MultiLineSubmit && c == '\n')
                        {
                            m_Keyboard.text = m_Text;

                            OnDeselect(null);
                            return;
                        }

                        if (c != 0)
                            m_Text += c;
                    }

                    if (characterLimit > 0 && m_Text.Length > characterLimit)
                        m_Text = m_Text.Substring(0, characterLimit);

                    if (m_Keyboard.canGetSelection)
                    {
                        UpdateCaretFromKeyboard();
                    }
                    else
                    {
                        caretPositionInternal = caretSelectPositionInternal = m_Text.Length;
                    }

                    // Set keyboard text before updating label, as we might have changed it with validation
                    // and update label will take the old value from keyboard if we don't change it here
                    if (m_Text != val)
                        m_Keyboard.text = m_Text;

                    SendOnValueChangedAndUpdateLabel();
                }
            }
            else if (HideMobileInput && m_Keyboard.canSetSelection)
            {
                m_Keyboard.selection = new RangeInt(caretPositionInternal, caretSelectPositionInternal - caretPositionInternal);
            }
            else if (m_Keyboard.canGetSelection && !HideMobileInput)
            {
                UpdateCaretFromKeyboard();
            }


            if (m_Keyboard.status != TouchScreenKeyboard.Status.Visible)
            {
                if (m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
                    WasCanceled = true;

                OnDeselect(null);
            }
        }

        private void ActivateInputFieldInternal()
        {
            if (EventSystem.current == null)
                return;

            if (m_SelectOnFocus)
            {
                if (EventSystem.current.currentSelectedGameObject != gameObject)
                    EventSystem.current.SetSelectedGameObject(gameObject);
            }

            if (TouchScreenKeyboard.isSupported)
            {
                if (Input.touchSupported)
                {
                    TouchScreenKeyboard.hideInput = shouldHideMobileInput;
                }

                m_Keyboard = (inputType == InputType.Password) ?
                    TouchScreenKeyboard.Open(m_Text, keyboardType, false, multiLine, true, false, "", characterLimit) :
                    TouchScreenKeyboard.Open(m_Text, keyboardType, inputType == InputType.AutoCorrect, multiLine, false, false, "", characterLimit);

                // Mimics OnFocus but as mobile doesn't properly support select all
                // just set it to the end of the text (where it would move when typing starts)
                MoveTextEnd(false);
            }
            else
            {
                Input.imeCompositionMode = IMECompositionMode.On;
                if (m_SelectAllTextOnFocus)
                {
                    OnFocus();
                }
                else
                {
                    MoveTextEnd(false);
                }
            }

            AllowInput = true;
            OriginalText = text;
            WasCanceled = false;
            SetCaretVisible();
            UpdateLabel();
        }

        private PropertyInfo HasSelectionProperty
        {
            get
            {
                return typeof(Selectable).GetProperty("hasSelection", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private FieldInfo AllowInputField
        {
            get
            {
                return typeof(InputField).GetField("m_AllowInput", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private FieldInfo OriginalTextField
        {
            get
            {
                return typeof(InputField).GetField("m_OriginalText", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private FieldInfo WasCanceledField
        {
            get
            {
                return typeof(InputField).GetField("m_WasCanceled", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private FieldInfo ShouldActivateNextUpdateField
        {
            get
            {
                return typeof(InputField).GetField("m_ShouldActivateNextUpdate", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private FieldInfo HideMobileInputField
        {
            get
            {
                return typeof(InputField).GetField("m_HideMobileInput", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        private PropertyInfo InputProperty
        {
            get
            {
                return typeof(InputField).GetProperty("input", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            }
        }

        protected bool HasSelection
        {
            get
            {
                return (bool)HasSelectionProperty.GetValue(this);
            }
            set
            {
                HasSelectionProperty.SetValue(this, value);
            }
        }

        protected bool AllowInput
        {
            get
            {
                return (bool)AllowInputField.GetValue(this);
            }
            set
            {
                AllowInputField.SetValue(this, value);
            }
        }

        protected string OriginalText
        {
            get
            {
                return OriginalTextField.GetValue(this) as string;
            }
            set
            {
                OriginalTextField.SetValue(this, value);
            }
        }

        protected bool WasCanceled
        {
            get
            {
                return (bool)WasCanceledField.GetValue(this);
            }
            set
            {
                WasCanceledField.SetValue(this, value);
            }
        }

        protected bool ShouldActivateNextUpdate
        {
            get
            {
                return (bool)ShouldActivateNextUpdateField.GetValue(this);
            }
            set
            {
                ShouldActivateNextUpdateField.SetValue(this, value);
            }
        }

        protected bool HideMobileInput
        {
            get
            {
                return (bool)HideMobileInputField.GetValue(this);
            }
            set
            {
                HideMobileInputField.SetValue(this, value);
            }
        }

        protected BaseInput Input
        {
            get
            {
                return (BaseInput)InputProperty.GetValue(this);
            }
        }


        protected void EvaluateAndTransitionToSelectionState(BaseEventData eventData)
        {
            typeof(Selectable).GetMethod("EvaluateAndTransitionToSelectionState", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, new object[] { eventData } );
        }

        protected void SetCaretVisible()
        {
            typeof(InputField).GetMethod("SetCaretVisible", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null);
        }

        protected void SendOnValueChangedAndUpdateLabel()
        {
            typeof(InputField).GetMethod("SendOnValueChangedAndUpdateLabel", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null);
        }

        protected void AssignPositioningIfNeeded()
        {
            typeof(InputField).GetMethod("AssignPositioningIfNeeded", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null);
        }

        protected bool InPlaceEditing()
        {
            return (bool)typeof(InputField).GetMethod("InPlaceEditing", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null);
        }

        protected void UpdateCaretFromKeyboard()
        {
            typeof(InputField).GetMethod("UpdateCaretFromKeyboard", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(this, null);
        }
    }
}
