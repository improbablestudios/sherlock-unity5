﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ImprobableStudios.BaseUtilities;
using UnityEngine.Serialization;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Submit On Select")]
    public class SubmitOnSelect : BaseOnEvent, ISelectHandler
    {
        [Tooltip("The selectable to submit")]
        [SerializeField]
        protected Selectable m_Selectable;

        public Selectable Selectable
        {
            get
            {
                return m_Selectable;
            }
            set
            {
                m_Selectable = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }
        
        public void OnSelect(BaseEventData eventData)
        {
            if (ShouldHandleEvent(eventData))
            {
                ExecuteEvents.Execute(Selectable.gameObject, eventData, ExecuteEvents.submitHandler);
            }
        }
    }

}
