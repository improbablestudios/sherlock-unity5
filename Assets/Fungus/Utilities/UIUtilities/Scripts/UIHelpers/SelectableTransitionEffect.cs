﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Helper/Selectable Transition Effect")]
    [ExecuteInEditMode]
    [RequireComponent(typeof(Selectable))]
    public class SelectableTransitionEffect : UITransitionEffect, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler, IEventSystemHandler
    {
        protected enum SelectionState
        {
            Normal,
            Highlighted,
            Pressed,
            Selected,
            Disabled
        }
        
        [Tooltip("If true, apply the transition effects from the attached selectable to the target graphics specified here")]
        [SerializeField]
        protected bool m_CopySelectableTransitionEffects = true;

        [Tooltip("The type of transition that will be applied when the selection state changes")]
        [SerializeField]
        protected List<TransitionState> m_TransitionStates =
            new List<TransitionState>()
            {
                new TransitionState(((SelectionState)0).ToString(), new Color32(255, 255, 255, 255)),
                new TransitionState(((SelectionState)1).ToString(), new Color32(245, 245, 245, 255)),
                new TransitionState(((SelectionState)2).ToString(), new Color32(200, 200, 200, 255)),
                new TransitionState(((SelectionState)3).ToString(), new Color32(245, 245, 245, 255)),
                new TransitionState(((SelectionState)4).ToString(), new Color32(200, 200, 200, 128))
            };

        [Tooltip("If true, the highlight state takes priority over the disabled state")]
        [SerializeField]
        protected bool m_HighlightWhenDisabled = false;

        private bool m_GroupsAllowInteraction = true;

        private readonly List<CanvasGroup> m_CanvasGroupCache = new List<CanvasGroup>();

        private Selectable m_selectable;

        public ColorBlock Colors
        {
            get
            {
                ColorBlock colorBlock = Selectable.colors;
                colorBlock.normalColor = m_TransitionStates[0].Color;
                colorBlock.highlightedColor = m_TransitionStates[1].Color;
                colorBlock.pressedColor = m_TransitionStates[2].Color;
                colorBlock.selectedColor = m_TransitionStates[3].Color;
                colorBlock.disabledColor = m_TransitionStates[4].Color;
                return colorBlock;
            }
            set
            {
                m_TransitionStates[0].Color = value.normalColor;
                m_TransitionStates[1].Color = value.highlightedColor;
                m_TransitionStates[2].Color = value.pressedColor;
                m_TransitionStates[3].Color = value.selectedColor;
                m_TransitionStates[4].Color = value.disabledColor;
            }
        }

        public bool CopySelectableTransitionEffects
        {
            get
            {
                return m_CopySelectableTransitionEffects;
            }
            set
            {
                m_CopySelectableTransitionEffects = value;
                OnSetProperty();
            }
        }

        public Selectable Selectable
        {
            get
            {
                if (m_selectable == null)
                {
                    m_selectable = GetComponent<Selectable>();
                }
                return m_selectable;
            }
        }

        private bool IsPointerInside
        {
            get;
            set;
        }

        private bool IsPointerDown
        {
            get;
            set;
        }

        private bool HasSelection
        {
            get;
            set;
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            foreach (SelectionState state in Enum.GetValues(typeof(SelectionState)))
            {
                if (!m_TransitionStates.Any(x => x.Name == state.ToString()))
                {
                    Color color = GetColorFromColorBlock(ColorBlock.defaultColorBlock, (int)state);
                    m_TransitionStates.Add(new TransitionState(state.ToString(), color));
                }
            }
            m_TransitionStates = m_TransitionStates.OrderBy(x => Enum.Parse(typeof(SelectionState), x.Name)).ToList();
        }

        protected override void OnEnable()
        {
            HasSelection = (EventSystem.current != null && EventSystem.current.currentSelectedGameObject == gameObject);

            base.OnEnable();
        }

        protected virtual void OnCanvasGroupChanged()
        {
            bool flag = true;
            Transform transform = base.transform;
            while (transform != null)
            {
                transform.GetComponents<CanvasGroup>(m_CanvasGroupCache);
                bool flag2 = false;
                for (int i = 0; i < m_CanvasGroupCache.Count; i++)
                {
                    if (!m_CanvasGroupCache[i].interactable)
                    {
                        flag = false;
                        flag2 = true;
                    }
                    if (m_CanvasGroupCache[i].ignoreParentGroups)
                    {
                        flag2 = true;
                    }
                }
                if (flag2)
                {
                    break;
                }
                transform = transform.parent;
            }
            if (flag != m_GroupsAllowInteraction)
            {
                m_GroupsAllowInteraction = flag;
                OnSetProperty();
            }
        }

        private Color GetColorFromColorBlock(ColorBlock colorBlock, int state)
        {
            switch (state)
            {
                case 1:
                    return colorBlock.highlightedColor;
                case 2:
                    return colorBlock.pressedColor;
                case 3:
                    return colorBlock.selectedColor;
                case 4:
                    return colorBlock.disabledColor;
                default:
                    return colorBlock.normalColor;
            }
        }
        
        public override TransitionState[] GetTransitionStates()
        {
            return m_TransitionStates.ToArray();
        }

        protected override int GetCurrentState(BaseEventData eventData)
        {
            if (!IsInteractable())
            {
                if (m_HighlightWhenDisabled)
                {
                    if (HasSelection)
                    {
                        return (int)SelectionState.Selected;
                    }
                    if (IsHighlighted(eventData))
                    {
                        return (int)SelectionState.Highlighted;
                    }
                }
                return (int)SelectionState.Disabled;
            }

            else if (IsPressed())
            {
                return (int)SelectionState.Pressed;
            }
            else if (HasSelection)
            {
                return (int)SelectionState.Selected;
            }
            else if (IsHighlighted(eventData))
            {
                return (int)SelectionState.Highlighted;
            }

            return (int)SelectionState.Normal;
        }
        
        public virtual bool IsInteractable()
        {
            return m_GroupsAllowInteraction && Selectable.interactable;
        }
        
        protected override void InstantClearState()
        {
            IsPointerInside = false;
            IsPointerDown = false;
            HasSelection = false;

            base.InstantClearState();
        }
        
        public bool IsHighlighted(BaseEventData eventData)
        {
            bool result;
            if (!isActiveAndEnabled)
            {
                result = false;
            }
            else if (IsPressed())
            {
                result = false;
            }
            else
            {
                bool flag = HasSelection;
                if (eventData is PointerEventData)
                {
                    PointerEventData pointerEventData = eventData as PointerEventData;
                    flag |= ((IsPointerDown && !IsPointerInside && pointerEventData.pointerPress == base.gameObject) || (!IsPointerDown && IsPointerInside && pointerEventData.pointerPress == base.gameObject) || (!IsPointerDown && IsPointerInside && pointerEventData.pointerPress == null));
                }
                else
                {
                    flag |= IsPointerInside;
                }
                result = flag;
            }
            return result;
        }
        
        public bool IsPressed()
        {
            return isActiveAndEnabled && IsPointerInside && IsPointerDown;
        }
        
        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                IsPointerDown = true;
                EvaluateAndTransitionState(eventData, false);
            }
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                IsPointerDown = false;
                EvaluateAndTransitionState(eventData, false);
            }
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            IsPointerInside = true;
            EvaluateAndTransitionState(eventData, false);
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            IsPointerInside = false;
            EvaluateAndTransitionState(eventData, false);
        }

        public virtual void OnSelect(BaseEventData eventData)
        {
            HasSelection = true;
            EvaluateAndTransitionState(eventData, false);
        }

        public virtual void OnDeselect(BaseEventData eventData)
        {
            HasSelection = false;
            EvaluateAndTransitionState(eventData, false);
        }
        
        protected override void DoStateTransition(int state, bool instant)
        {
            Selectable.DoStateTransition(state, instant);

            base.DoStateTransition(state, instant);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Transition))
            {
                if (m_CopySelectableTransitionEffects)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TransitionStates))
            {
                if (m_CopySelectableTransitionEffects)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ColorMultiplier))
            {
                if (m_CopySelectableTransitionEffects)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ColorFadeDuration))
            {
                if (m_CopySelectableTransitionEffects)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_TransitionStates))
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}