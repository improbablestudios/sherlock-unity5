﻿using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.ColorPaletteUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(ToggleTransitionEffect))]
    [AddComponentMenu("UI/Helper/Toggle Transition Effect Color Controller")]
    public class ToggleTransitionEffectColorController : ColorController<ToggleTransitionEffect>
    {
        [Tooltip("The color swatch to use for the selectable's highlighted transition")]
        [SerializeField]
        [FormerlySerializedAs("m_OffPaletteColorName")]
        protected ColorSwatch m_OffColorSwatch;

        [Tooltip("The color swatch to use for the selectable's normal transition")]
        [SerializeField]
        [FormerlySerializedAs("m_OnPaletteColorName")]
        protected ColorSwatch m_OnColorSwatch;
        
        public ColorSwatch OffColorSwatch
        {
            get
            {
                return m_OffColorSwatch;
            }
            set
            {
                m_OffColorSwatch = value;
            }
        }

        public ColorSwatch OnColorSwatch
        {
            get
            {
                return m_OnColorSwatch;
            }
            set
            {
                m_OnColorSwatch = value;
            }
        }
        
        public override ColorSwatch[] GetColorSwatches()
        {
            return new ColorSwatch[] 
            {
                m_OffColorSwatch,
                m_OnColorSwatch
            };
        }

        public override void SetColorSwatches(ColorSwatch[] colorSwatches)
        {
            if (colorSwatches.Length > 0)
            {
                m_OffColorSwatch = colorSwatches[0];
            }
            if (colorSwatches.Length > 1)
            {
                m_OnColorSwatch = colorSwatches[1];
            }
        }

        public override Color[] GetColors()
        {
            return new Color[] 
            {
                Target.OffColor,
                Target.OnColor
            };
        }

        public override void SetColors(Color[] colors)
        {
            if (colors.Length > 0)
            {
                Target.OffColor = colors[0];
            }
            if (colors.Length > 1)
            {
                Target.OnColor = colors[1];
            }
        }
    }

}