﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Select On Hover")]
    public class SelectOnHover : PersistentBehaviour, IPointerEnterHandler
    {
        [Tooltip("The selectable to select")]
        [SerializeField]
        protected Selectable m_Selectable;

        [Tooltip("Ignore hover events when the pointer is being dragged")]
        [SerializeField]
        protected bool m_IgnoreDragging = true;

        public Selectable Selectable
        {
            get
            {
                return m_Selectable;
            }
            set
            {
                m_Selectable = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Selectable == null)
            {
                m_Selectable = GetComponent<Selectable>();
            }
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            // Only allow mouse events, ignore touch events
            if (eventData.pointerId < 0)
            {
                if (!m_IgnoreDragging || !eventData.dragging)
                {
                    EventSystem.current.SetSelectedGameObject(Selectable.gameObject, eventData);
                }
            }
        }
    }

}
