﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Toggle))]
    [AddComponentMenu("UI/Helper/Toggle Navigation Prioritizer")]
    [ExecuteAlways]
    public class ToggleNavigationPrioritizer : NavigationPrioritizer
    {
        [Tooltip("The navigation priority of this toggle when it is off")]
        [SerializeField]
        protected int m_OffPriority = 0;

        [Tooltip("The navigation priority of this toggle when it is on")]
        [SerializeField]
        protected int m_OnPriority = -1;

        private Toggle m_toggle;

        public int OffPriority
        {
            get
            {
                return m_OffPriority;
            }
            set
            {
                m_OffPriority = value;
            }
        }

        public int OnPriority
        {
            get
            {
                return m_OnPriority;
            }
            set
            {
                m_OnPriority = value;
            }
        }

        public Toggle Toggle
        {
            get
            {
                if (m_toggle == null)
                {
                    m_toggle = GetComponent<Toggle>();
                }
                return m_toggle;
            }
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();
            Toggle.onValueChanged.AddListener(OnValueChanged);
            m_Priority = (Toggle.isOn) ? m_OnPriority : m_OffPriority;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(bool on)
        {
            m_Priority = (on) ? m_OnPriority : m_OffPriority;
        }

        protected virtual void Update()
        {
            m_Priority = (Toggle.isOn) ? m_OnPriority : m_OffPriority;
        }

        public override bool IsPropertyEditable(string propertyPath)
        {
            if (propertyPath == nameof(m_Priority))
            {
                return false;
            }
            return base.IsPropertyEditable(propertyPath);
        }
    }

}