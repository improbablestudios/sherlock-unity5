﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.TextStylePaletteUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Selectable Transition Effect FontData Controller")]
    public class SelectableTransitionEffectFontDataController : TextStyleController<SelectableTransitionEffect>
    {
        [Tooltip("The color swatch to use for the selectable's normal transition")]
        [SerializeField]
        protected TextStyle m_NormalTextStyle;

        [Tooltip("The color swatch to use for the selectable's highlighted transition")]
        [SerializeField]
        protected TextStyle m_HighlightedTextStyle;

        [Tooltip("The color swatch to use for the selectable's pressed transition")]
        [SerializeField]
        protected TextStyle m_PressedTextStyle;

        [Tooltip("The color swatch to use for the selectable's disabled transition")]
        [SerializeField]
        protected TextStyle m_DisabledTextStyle;

        public TextStyle NormalTextStyle
        {
            get
            {
                return m_NormalTextStyle;
            }
            set
            {
                m_NormalTextStyle = value;
            }
        }

        public TextStyle HighlightedTextStyle
        {
            get
            {
                return m_HighlightedTextStyle;
            }
            set
            {
                m_HighlightedTextStyle = value;
            }
        }

        public TextStyle PressedTextStyle
        {
            get
            {
                return m_PressedTextStyle;
            }
            set
            {
                m_PressedTextStyle = value;
            }
        }

        public TextStyle DisabledTextStyle
        {
            get
            {
                return m_DisabledTextStyle;
            }
            set
            {
                m_DisabledTextStyle = value;
            }
        }

        public override TextStyle[] GetTextStyles()
        {
            return new TextStyle[]
            {
                m_NormalTextStyle,
                m_HighlightedTextStyle,
                m_PressedTextStyle,
                m_DisabledTextStyle
            };
        }

        public override void SetTextStyles(TextStyle[] textStyles)
        {
            if (textStyles.Length > 0)
            {
                m_NormalTextStyle = textStyles[0];
            }
            if (textStyles.Length > 1)
            {
                m_HighlightedTextStyle = textStyles[1];
            }
            if (textStyles.Length > 2)
            {
                m_PressedTextStyle = textStyles[2];
            }
            if (textStyles.Length > 3)
            {
                m_DisabledTextStyle = textStyles[3];
            }
        }

        public override FontData[] GetFontDatas()
        {
            FontData[] fontDatas = new FontData[5];
            fontDatas[0] = Target.TransitionStates[0].TextStyle;
            fontDatas[1] = Target.TransitionStates[1].TextStyle;
            fontDatas[2] = Target.TransitionStates[2].TextStyle;
            fontDatas[3] = Target.TransitionStates[3].TextStyle;
            fontDatas[4] = Target.TransitionStates[4].TextStyle;
            return fontDatas;
        }

        public override void SetFontDatas(FontData[] fontDatas)
        {
            if (fontDatas.Length > 0)
            {
                Target.TransitionStates[0].TextStyle = fontDatas[0];
            }
            if (fontDatas.Length > 1)
            {
                Target.TransitionStates[1].TextStyle = fontDatas[1];
            }
            if (fontDatas.Length > 2)
            {
                Target.TransitionStates[2].TextStyle = fontDatas[2];
            }
            if (fontDatas.Length > 3)
            {
                Target.TransitionStates[3].TextStyle = fontDatas[3];
            }
            if (fontDatas.Length > 4)
            {
                Target.TransitionStates[4].TextStyle = fontDatas[4];
            }
        }
    }

}