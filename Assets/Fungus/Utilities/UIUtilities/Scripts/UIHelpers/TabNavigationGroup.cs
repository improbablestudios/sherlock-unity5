﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Helper/Tab Navigation Group")]
    public class TabNavigationGroup : PersistentBehaviour
    {
        [Tooltip("The path to take when tabbing through ui components")]
        [SerializeField]
        [FormerlySerializedAs("navigationPath")]
        protected Selectable[] m_NavigationPath = new Selectable[0];
        
        public Selectable[] NavigationPath
        {
            get
            {
                return m_NavigationPath;
            }
            set
            {
                m_NavigationPath = value;
            }
        }

        public void Update()
        {
            if (EventSystem.current == null)
            {
                return;
            }

            GameObject currentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
            if (currentSelectedGameObject == null)
            {
                return;
            }

            Selectable currentSelectedSelectable = currentSelectedGameObject.GetComponent<Selectable>();
            if (currentSelectedSelectable == null)
            {
                return;
            }

            Selectable next = null;

            if (Input.GetKeyDown(KeyCode.Tab) && Input.GetKey(KeyCode.LeftShift))
            {
                next = GetPrevious(currentSelectedSelectable);
            }
            else if (Input.GetKeyDown(KeyCode.Tab))
            {
                next = GetNext(currentSelectedSelectable);
            }

            if (next != null)
            {
                next.Select();
            }
        }

        public Selectable GetPrevious(Selectable selectable)
        {
            if (m_NavigationPath.Length == 0)
            {
                return selectable;
            }
            int index = Array.IndexOf(m_NavigationPath, selectable);
            int previousIndex = m_NavigationPath.Length - 1;
            if (index - 1 >= 0)
            {
                previousIndex = index - 1;
            }
            return m_NavigationPath[previousIndex];
        }

        public Selectable GetNext(Selectable selectable)
        {
            if (m_NavigationPath.Length == 0)
            {
                return selectable;
            }
            int index = Array.IndexOf(m_NavigationPath, selectable);
            int nextIndex = 0;
            if (index + 1 < m_NavigationPath.Length)
            {
                nextIndex = index + 1;
            }
            return m_NavigationPath[nextIndex];
        }
    }

}
