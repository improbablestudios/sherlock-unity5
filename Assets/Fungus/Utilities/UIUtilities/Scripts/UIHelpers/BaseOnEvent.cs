﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.UIUtilities
{

    public abstract class BaseOnEvent : PersistentBehaviour
    {
        [Tooltip("Execute on axis input events")]
        [SerializeField]
        protected bool m_OnAxisEvent = true;

        [Tooltip("Execute on pointer input events")]
        [SerializeField]
        protected bool m_OnPointerEvent = true;

        [Tooltip("Execute on events that are not axis input events or pointer input events")]
        [SerializeField]
        protected bool m_OnOtherEvent = true;

        public bool OnAxisEvent
        {
            get
            {
                return m_OnAxisEvent;
            }
            set
            {
                m_OnAxisEvent = value;
            }
        }

        public bool OnPointerEvent
        {
            get
            {
                return m_OnPointerEvent;
            }
            set
            {
                m_OnPointerEvent = value;
            }
        }

        public bool OnOtherEvent
        {
            get
            {
                return m_OnOtherEvent;
            }
            set
            {
                m_OnOtherEvent = value;
            }
        }
        
        public bool ShouldHandleEvent(BaseEventData eventData)
        {
            return (m_OnAxisEvent && eventData is AxisEventData) ||
                (m_OnPointerEvent && eventData is PointerEventData) ||
                (m_OnOtherEvent && !(eventData is AxisEventData) && !(eventData is PointerEventData));
        }
    }

}
