﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Helper/Match Active State")]
    public class MatchActiveState : PersistentBehaviour
    {
        [Tooltip("The game object who's active state will be monitored")]
        [SerializeField]
        [FormerlySerializedAs("sourceGameObject")]
        [CheckNotNull]
        protected GameObject m_SourceObject;

        [Tooltip("The game object who's active state will be mirror the source's active state")]
        [SerializeField]
        [FormerlySerializedAs("targetGameObject")]
        [CheckNotNull]
        protected GameObject m_TargetObject;

        public GameObject SourceObject
        {
            get
            {
                return m_SourceObject;
            }
            set
            {
                m_SourceObject = value;
            }
        }
        
        public GameObject TargetObject
        {
            get
            {
                return m_TargetObject;
            }
            set
            {
                m_TargetObject = value;
            }
        }

        public void Update()
        {
            if (m_SourceObject != null && m_TargetObject != null)
            {
                m_TargetObject.SetActive(m_SourceObject.activeSelf);
            }
        }
    }

}
