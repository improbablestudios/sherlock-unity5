﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ImprobableStudios.InputUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [AddComponentMenu("UI/Helper/Navigation Group")]
    public class NavigationGroup : PersistentBehaviour
    {
        [Tooltip("Allows this group to be grouped with others of the same id")]
        [SerializeField]
        [FormerlySerializedAs("m_id")]
        protected int m_Id;

        [Tooltip("Determines how the buttons can be navigated")]
        [SerializeField]
        protected Navigation m_Navigation = Navigation.defaultNavigation;

        [Tooltip("Restore last selection when returning to this navigation group")]
        [SerializeField]
        protected bool m_RestoreLastSelection = true;

        private GameObject m_previousSelectedGameObject = null;

        private Selectable m_lastSelectedSelectable = null;

        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }

        public Selectable[] Selectables
        {
            get
            {
                return GetComponentsInChildren<Selectable>(true).Where(x => x.GetComponentInParent<NavigationGroup>() == this).ToArray();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            if (EventSystem.current != null)
            {
                ControllableInputModule controllableInputModule = EventSystem.current.currentInputModule as ControllableInputModule;
                if (controllableInputModule != null)
                {
                    controllableInputModule.OnBeforeMove.AddListener(OnBeforeMove);
                }
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (EventSystem.current != null)
            {
                ControllableInputModule controllableInputModule = EventSystem.current.currentInputModule as ControllableInputModule;
                if (controllableInputModule != null)
                {
                    controllableInputModule.OnBeforeMove.RemoveListener(OnBeforeMove);
                }
            }
        }

        protected virtual void Update()
        {
            if (EventSystem.current != null)
            {
                GameObject currentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
                if (m_previousSelectedGameObject != currentSelectedGameObject)
                {
                    OnSelectedGameObjectChanged(m_previousSelectedGameObject, currentSelectedGameObject);
                    m_previousSelectedGameObject = currentSelectedGameObject;
                }
            }
        }

        protected virtual void OnSelectedGameObjectChanged(GameObject previousSelectedGameObject, GameObject currentSelectedGameObject)
        {
            if (currentSelectedGameObject != null)
            {
                NavigationGroup selectedNavigationGroup = currentSelectedGameObject.GetComponentInParent<NavigationGroup>();
                if (selectedNavigationGroup == this)
                {
                    m_lastSelectedSelectable = currentSelectedGameObject.GetComponent<Selectable>();
                }
            }
        }


        protected virtual void OnBeforeMove(AxisEventData axisEventData)
        {
            GameObject selectedGameObject = axisEventData.selectedObject;
            if (selectedGameObject != null)
            {
                NavigationGroup selectedNavigationGroup = selectedGameObject.GetComponentInParent<NavigationGroup>();
                if (selectedNavigationGroup == this)
                {
                    if (selectedGameObject != null)
                    {
                        Selectable selectedSelectable = selectedGameObject.GetComponent<Selectable>();
                        if (selectedSelectable != null)
                        {
                            UpdateNavigation(selectedSelectable);
                            Selectable nextSelectable = GetNavigationSelectable(selectedSelectable.navigation, axisEventData.moveDir);

                            if (nextSelectable != null)
                            {
                                NavigationGroup nextNavigationGroup = nextSelectable.GetComponentInParent<NavigationGroup>();
                                if (nextNavigationGroup != null)
                                {
                                    if (nextNavigationGroup.Id != selectedNavigationGroup.Id)
                                    {
                                        if (nextNavigationGroup.m_RestoreLastSelection && nextNavigationGroup.m_lastSelectedSelectable != null)
                                        {
                                            selectedSelectable.navigation = SetNavigationSelectable(selectedSelectable.navigation, axisEventData.moveDir, nextNavigationGroup.m_lastSelectedSelectable);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        protected Selectable GetNavigationSelectable(Navigation navigation, MoveDirection moveDirection)
        {
            switch (moveDirection)
            {
                case MoveDirection.Left:
                    return navigation.selectOnLeft;
                case MoveDirection.Up:
                    return navigation.selectOnUp;
                case MoveDirection.Right:
                    return navigation.selectOnRight;
                case MoveDirection.Down:
                    return navigation.selectOnDown;
            }

            return null;
        }

        protected Navigation SetNavigationSelectable(Navigation navigation, MoveDirection moveDirection, Selectable selectable)
        {
            switch (moveDirection)
            {
                case MoveDirection.Left:
                    navigation.selectOnLeft = selectable;
                    break;
                case MoveDirection.Up:
                    navigation.selectOnUp = selectable;
                    break;
                case MoveDirection.Right:
                    navigation.selectOnRight = selectable;
                    break;
                case MoveDirection.Down:
                    navigation.selectOnDown = selectable;
                    break;
            }

            return navigation;
        }

        public void UpdateNavigation(Selectable selectedSelectable)
        {
            NavigationGroup[] navigationGroups = GetAllActive<NavigationGroup>();
            SortedDictionary<int, List<Selectable>> highestPrioritySelectablesInOtherGroups = new SortedDictionary<int, List<Selectable>>();
            List<Selectable> otherSelectablesInSameGroup = new List<Selectable>();
            foreach (IGrouping<int, NavigationGroup> group in navigationGroups.GroupBy(x => x.Id))
            {
                int id = group.Key;

                if (id == m_Id)
                {
                    Selectable[] selectables = group.SelectMany(x => x.Selectables).ToArray();
                    otherSelectablesInSameGroup.AddRange(selectables);
                }
                else
                {
                    NavigationGroup[] navigationGroupsWithSameId = group.Select(x => x).ToArray();
                    foreach (NavigationGroup ng in navigationGroupsWithSameId)
                    {
                        Selectable[] selectables = ng.Selectables;
                        List <Selectable> highestPrioritySelectables = new List<Selectable>();
                        int[] priorities = new int[selectables.Length];
                        int highestPriority = int.MaxValue;
                        for (int i = 0; i < selectables.Length; i++)
                        {
                            Selectable selectable = selectables[i];
                            NavigationPrioritizer navigationPrioritizer = selectable.GetComponent<NavigationPrioritizer>();
                            int priority = (navigationPrioritizer != null) ? navigationPrioritizer.Priority : int.MaxValue;
                            if (priority < highestPriority)
                            {
                                highestPriority = priority;
                            }
                            priorities[i] = priority;
                        }
                        for (int i = 0; i < selectables.Length; i++)
                        {
                            Selectable selectable = selectables[i];
                            int priority = priorities[i];
                            if (priority <= highestPriority)
                            {
                                highestPrioritySelectables.Add(selectable);
                            }
                        }
                        if (!highestPrioritySelectablesInOtherGroups.ContainsKey(highestPriority))
                        {
                            highestPrioritySelectablesInOtherGroups[highestPriority] = highestPrioritySelectables;
                        }
                        else
                        {
                            highestPrioritySelectablesInOtherGroups[highestPriority].AddRange(highestPrioritySelectables);
                        }
                    }
                }
            }
            Selectable[] selectableChildren = Selectables;
            otherSelectablesInSameGroup = otherSelectablesInSameGroup.Except(selectableChildren).ToList();

            List<Selectable[]> selectableLists = new List<Selectable[]>();
            selectableLists.Add(selectableChildren);
            selectableLists.Add(otherSelectablesInSameGroup.ToArray());
            selectableLists.AddRange(highestPrioritySelectablesInOtherGroups.Select(x => x.Value.ToArray()));
            selectableLists.Add(Selectable.allSelectablesArray);
            
            selectedSelectable.UpdateSelectableNavigation(selectableLists.ToArray(), m_Navigation);
        }
    }

}