﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Helper/Toggle Transition Effect")]
    [ExecuteInEditMode]
    [RequireComponent(typeof(Toggle))]
    public class ToggleTransitionEffect : UITransitionEffect
    {
        protected enum ToggleState
        {
            Off,
            On,
        }

        [Tooltip("The type of transition that will be applied when the toggle state changes")]
        [SerializeField]
        protected List<TransitionState> m_TransitionStates =
            new List<TransitionState>()
            {
                new TransitionState(((ToggleState)0).ToString(), new Color(1, 1, 1, 0)),
                new TransitionState(((ToggleState)1).ToString(), new Color(1, 1, 1, 1))
            };

        private Toggle m_toggle;

        public Toggle Toggle
        {
            get
            {
                if (m_toggle == null)
                {
                    m_toggle = GetComponent<Toggle>();
                }
                return m_toggle;
            }
        }

        public Color OffColor
        {
            get
            {
                return m_TransitionStates[0].Color;
            }
            set
            {
                m_TransitionStates[0].Color = value;
            }
        }

        public Color OnColor
        {
            get
            {
                return m_TransitionStates[1].Color;
            }
            set
            {
                m_TransitionStates[1].Color = value;
            }
        }
        
        protected override void Awake()
        {
            base.Awake();

            DoStateTransition(Toggle.isOn ? 1 : 0, false);
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            Toggle.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        public override TransitionState[] GetTransitionStates()
        {
            return m_TransitionStates.ToArray();
        }

        protected override int GetCurrentState(BaseEventData eventData)
        {
            return Toggle.isOn ? 1 : 0;
        }

        public virtual void OnValueChanged(bool on)
        {
            DoStateTransition(on ? 1 : 0, false);
        }

        protected override void DoStateTransition(int state, bool instant)
        {
            if (Toggle.graphic != null)
            {
                Toggle.graphic.CrossFadeAlpha(state, 0f, true);
            }

            base.DoStateTransition(state, instant);
        }
    }

}