﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(AspectRatioFitter))]
    public class AspectRatioScaler : PersistentBehaviour
    {
        private AspectRatioFitter m_aspectRatioFitter;
        
        private Canvas m_canvas;

        private CanvasScaler m_canvasScaler;

        public AspectRatioFitter AspectRatioFitter
        {
            get
            {
                if (m_aspectRatioFitter == null)
                {
                    m_aspectRatioFitter = GetComponent<AspectRatioFitter>();
                }
                return m_aspectRatioFitter;
            }
        }

        public Canvas Canvas
        {
            get
            {
                if (m_canvas == null)
                {
                    m_canvas = this.GetComponentInParent<Canvas>(true);
                }
                return m_canvas;
            }
        }

        public CanvasScaler CanvasScaler
        {
            get
            {
                if (m_canvasScaler == null)
                {
                    m_canvasScaler = Canvas.GetRequiredComponent<CanvasScaler>();
                }
                return m_canvasScaler;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            Update();
        }

        protected override void Awake()
        {
            base.Awake();

            Update();
        }

        protected virtual void Update()
        {
            AspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
            float currentAspectRatio = (float)Screen.width / (float)Screen.height;
            CanvasScaler.matchWidthOrHeight = currentAspectRatio < AspectRatioFitter.aspectRatio ? 0 : 1;
        }
    }

}
