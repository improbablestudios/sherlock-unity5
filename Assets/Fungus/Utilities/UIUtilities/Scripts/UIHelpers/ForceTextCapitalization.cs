﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    public enum Capitalization
    {
        Uppercase,
        Lowercase,
    }

    [ExecuteInEditMode]
    [RequireComponent(typeof(Text))]
    [AddComponentMenu("UI/Helper/Force Text Capitalization")]
    public class ForceTextCapitalization : PersistentBehaviour
    {
        [Tooltip("Determines if text is forced to be uppercase or lowercase")]
        [SerializeField]
        [FormerlySerializedAs("forceCapitalization")]
        protected Capitalization m_ForceCapitalization;

        private Text m_text;

        public Capitalization ForceCapitalization
        {
            get
            {
                return m_ForceCapitalization;
            }
            set
            {
                m_ForceCapitalization = value;
            }
        }

        public void Update()
        {
            if (m_text == null)
            {
                m_text = GetComponent<Text>();
            }
            m_text.text = Capitalize(m_text.text);
        }

        public string Capitalize(string stringToValidate)
        {
            if (m_ForceCapitalization == Capitalization.Uppercase)
            {
                stringToValidate = stringToValidate.ToUpper();
            }
            else if (m_ForceCapitalization == Capitalization.Lowercase)
            {
                stringToValidate = stringToValidate.ToLower();
            }

            return stringToValidate;
        }
    }

}
