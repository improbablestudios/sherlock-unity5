﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using DG.Tweening;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.CameraUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [AddComponentMenu("UI/Writer")]
    public class Writer : PersistentBehaviour
    {
        [Tooltip("The game object containing a text, input field or text mesh object to write to")]
        [SerializeField]
        [FormerlySerializedAs("targetTextObject")]
        protected GameObject m_TargetTextObject;

        [Tooltip("The game object to punch when the punch tags are displayed. If none is set, the main camera will shake instead")]
        [SerializeField]
        [FormerlySerializedAs("shakeObject")]
        protected GameObject m_ShakeObject;

        [Tooltip("The speed the writer should type (in characters per second)")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("writingSpeed")]
        protected float m_WritingSpeed = 60;

        [Tooltip("The pause duration for punctuation characters")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("punctuationPause")]
        protected float m_PunctuationPause = 0.25f;

        [Tooltip("If true, the writer will write one word at a time rather one character at a time")]
        [SerializeField]
        [FormerlySerializedAs("writeWholeWords")]
        protected bool m_WriteWholeWords = false;

        [Tooltip("If true, the writer will force the target text object to use Rich Text mode so text color and alpha appears correctly")]
        [SerializeField]
        [FormerlySerializedAs("forceRichText")]
        protected bool m_ForceRichText = true;

        [Tooltip("If true, when the player clicks while text is still writing the text will finish writing immediately")]
        [SerializeField]
        [FormerlySerializedAs("instantCompleteOnInput")]
        protected bool m_InstantCompleteOnInput = true;

        [Tooltip("This determines amount of time the writer will wait for user input before auto advancing. Set to 0 to advance instantly. Set to -1 to wait forever for user input")]
        [Min(-1)]
        [SerializeField]
        protected float m_AutoAdvanceDelay;
        
        protected UnityEvent m_onWriteFinished = new UnityEvent();
        
        protected UnityEvent m_onNextWriteFinished = new UnityEvent();
        
        protected string m_content;
        
        protected string m_previousText;
        
        protected bool m_continuePrevious;
        
        protected bool m_dontWaitFlag;
        
        protected bool m_isWaitingForInput;
        
        protected bool m_willClearOnInput;
        
        protected bool m_boldActive = false;
        
        protected bool m_italicActive = false;
        
        protected bool m_sizeActive = false;
        
        protected string m_sizeText = "";
        
        protected bool m_colorActive = false;
        
        protected string m_colorText = "";
        
        protected bool m_alphaActive = false;
        
        protected string m_alphaColorText = "";
        
        protected bool m_inputFlag;
        
        protected bool m_exitFlag;
        
        protected bool m_completeFlag;
        
        protected AudioClip m_currentAudioClip;
        
        protected float m_currentWritingSpeed;

        protected float m_currentPunctuationPause;
        
        protected float m_currentAutoAdvanceDelay;
        
        // Called when a user input event (e.g. a click) has been handled by the Writer
        protected UnityEvent m_onInputDetected = new UnityEvent();

        // Called when the Writer starts writing new text
        protected UnityEvent m_onStartWritng = new UnityEvent();

        // Called when the Writer has paused writing text (e.g. on a {wi} tag)
        protected UnityEvent m_onPauseWriting = new UnityEvent();

        // Called when the Writer has resumed writing text
        protected UnityEvent m_onResumeWriting = new UnityEvent();

        // Called when the Writer has finshed writing text
        protected UnityEvent m_onFinishedWriting = new UnityEvent();

        // Called every time the Writer writes a new character glyph
        protected UnityEvent m_onGlyph = new UnityEvent();
        
        private Text m_textUi;

        private InputField m_inputField;

        private TextMesh m_textMesh;

        private Coroutine m_writeCoroutine;
        
        public GameObject TargetTextObject
        {
            get
            {
                if (m_TargetTextObject == null)
                {
                    m_TargetTextObject = gameObject;
                }
                return m_TargetTextObject;
            }
            set
            {
                m_TargetTextObject = value;
                if (m_TargetTextObject != null)
                {
                    m_textUi = m_TargetTextObject.GetComponent<Text>();
                }
                else
                {
                    m_textUi = null;
                }
            }
        }

        public GameObject ShakeObject
        {
            get
            {
                return m_ShakeObject;
            }
            set
            {
                m_ShakeObject = value;
            }
        }
        
        public float WritingSpeed
        {
            get
            {
                return m_WritingSpeed;
            }
            set
            {
                m_WritingSpeed = value;
            }
        }
        
        public float PunctuationPause
        {
            get
            {
                return m_PunctuationPause;
            }
            set
            {
                m_PunctuationPause = value;
            }
        }
        
        public bool WriteWholeWords
        {
            get
            {
                return m_WriteWholeWords;
            }
            set
            {
                m_WriteWholeWords = value;
            }
        }
        
        public bool ForceRichText
        {
            get
            {
                return m_ForceRichText;
            }
            set
            {
                m_ForceRichText = value;
            }
        }
        
        public bool InstantCompleteOnInput
        {
            get
            {
                return m_InstantCompleteOnInput;
            }
            set
            {
                m_InstantCompleteOnInput = value;
            }
        }

        public float AutoAdvanceDelay
        {
            get
            {
                return m_AutoAdvanceDelay;
            }
            set
            {
                m_AutoAdvanceDelay = value;
            }
        }

        public UnityEvent OnWriteEnded
        {
            get
            {
                return m_onWriteFinished;
            }
        }

        public UnityEvent OnNextWriteEnded
        {
            get
            {
                return m_onNextWriteFinished;
            }
        }
        
        public bool IsWaitingForInput
        {
            get
            {
                return m_isWaitingForInput;
            }
        }

        public AudioClip CurrentAudioClip
        {
            get
            {
                return m_currentAudioClip;
            }
        }

        public UnityEvent OnInputDetected
        {
            get
            {
                return m_onInputDetected;
            }
        }
        
        public UnityEvent OnStartWriting
        {
            get
            {
                return m_onStartWritng;
            }
        }
        
        public UnityEvent OnPauseWriting
        {
            get
            {
                return m_onPauseWriting;
            }
        }
        
        public UnityEvent OnResumeWriting
        {
            get
            {
                return m_onResumeWriting;
            }
        }

        public UnityEvent OnFinishedWriting
        {
            get
            {
                return m_onFinishedWriting;
            }
        }

        public UnityEvent OnGlyph
        {
            get
            {
                return m_onGlyph;
            }
        }

        public Text TextUI
        {
            get
            {
                if (m_textUi == null)
                {
                    m_textUi = m_TargetTextObject.GetComponent<Text>();
                }
                return m_textUi;
            }
        }

        public InputField InputField
        {
            get
            {
                if (m_inputField == null)
                {
                    m_inputField = TargetTextObject.GetComponent<InputField>();
                }
                return m_inputField;
            }
        }

        public TextMesh TextMesh
        {
            get
            {
                if (m_textMesh == null)
                {
                    m_textMesh = TargetTextObject.GetComponent<TextMesh>();
                }
                return m_textMesh;
            }
        }

        public string Text 
        {
            get 
            {
                if (TextUI != null)
                {
                    return TextUI.text;
                }
                else if (InputField != null)
                {
                    return InputField.text;
                }
                else if (TextMesh != null)
                {
                    return TextMesh.text;
                }

                return "";
            }
            
            set 
            {
                if (TextUI != null)
                {
                    TextUI.text = value;
                }
                else if (InputField != null)
                {
                    InputField.text = value;
                }
                else if (TextMesh != null)
                {
                    TextMesh.text = value;
                }
            }
        }
        
        public Color Color
        {
            get
            {
                if (TextUI != null)
                {
                    return TextUI.color;
                }
                else if (InputField != null)
                {
                    return InputField.textComponent.color;
                }
                else if (TextMesh != null)
                {
                    return TextMesh.color;
                }
                return Color.white;
            }
            set
            {
                if (TextUI != null)
                {
                    TextUI.color = value;
                }
                else if (InputField != null)
                {
                    InputField.textComponent.color = value;
                }
                else if (TextMesh != null)
                {
                    TextMesh.color = value;
                }
            }
        }
        
        protected virtual void Start()
        {
            if (m_ForceRichText)
            {
                if (TextUI != null)
                {
                    TextUI.supportRichText = true;
                }

                // Input Field does not support rich text

                if (TextMesh != null)
                {
                    TextMesh.richText = true;
                }
            }
        }

        public virtual bool HasTextObject()
        {
            return (TextUI != null || InputField != null || TextMesh != null);
        }
        
        public virtual bool SupportsRichText()
        {
            if (TextUI != null)
            {
                return TextUI.supportRichText;
            }
            if (InputField != null)
            {
                return false;
            }
            if (TextMesh != null)
            {
                return TextMesh.richText;
            }
            return false;
        }
        
        protected virtual string OpenMarkup()
        {
            string openText = "";
            
            if (SupportsRichText())
            {
                if (m_sizeActive)
                {
                    openText += "<size=" + m_sizeText + ">";
                }
                if (m_colorActive)
                {
                    openText += "<color=" + m_colorText + ">"; 
                }
                if (m_alphaActive)
                {
                    openText += "<color=" + m_alphaColorText + ">";
                }
                if (m_boldActive)
                {
                    openText += "<b>"; 
                }
                if (m_italicActive)
                {
                    openText += "<i>"; 
                }            
            }
            
            return openText;
        }
        
        protected virtual string CloseMarkup()
        {
            string closeText = "";
            
            if (SupportsRichText())
            {
                if (m_italicActive)
                {
                    closeText += "</i>"; 
                }            
                if (m_boldActive)
                {
                    closeText += "</b>";
                }
                if (m_alphaActive)
                {
                    closeText += "</color>";
                }
                if (m_colorActive)
                {
                    closeText += "</color>"; 
                }
                if (m_sizeActive)
                {
                    closeText += "</size>";
                }
            }
            
            return closeText;        
        }

        public virtual void SetTextColor(Color textColor)
        {
            if (TextUI != null)
            {
                TextUI.color = textColor;
            }
            else if (InputField != null)
            {
                if (InputField.textComponent != null)
                {
                    InputField.textComponent.color = textColor;
                }
            }
            else if (TextMesh != null)
            {
                TextMesh.color = textColor;
            }
        }
        
        public virtual void SetTextAlpha(float textAlpha)
        {
            if (TextUI != null)
            {
                Color tempColor = TextUI.color;
                tempColor.a = textAlpha;
                TextUI.color = tempColor;
            }
            else if (InputField != null)
            {
                if (InputField.textComponent != null)
                {
                    Color tempColor = InputField.textComponent.color;
                    tempColor.a = textAlpha;
                    InputField.textComponent.color = tempColor;
                }
            }
            else if (TextMesh != null)
            {
                Color tempColor = TextMesh.color;
                tempColor.a = textAlpha;
                TextMesh.color = tempColor;
            }
        }

        public virtual void Stop()
        {
            if (IsWriting())
            {
                StopCoroutine(m_writeCoroutine);
                ResetControlMembers();
            }
        }

        public virtual void Finish()
        {
            if (IsWriting())
            {
                StopCoroutine(m_writeCoroutine);

                InvokeOnFinished();
                
                ResetControlMembers();

                InvokeOnWriteEndedEvent();
            }
        }

        public virtual void Exit()
        {
            if (IsWriting())
            {
                m_exitFlag = true;
            }
        }

        public virtual void Complete()
        {
            if (IsWriting())
            {
                m_completeFlag = true;
            }
        }

        public static bool WillMoveTruncatedTextToNextDialogBox(Text textUI, string text)
        {
            if (text == null || text == "")
            {
                return false;
            }
            TextTagParser tagParser = new TextTagParser();
            List<TextTagParser.Token> tokens = tagParser.Tokenize(text);
            string displayText = "";
            bool boldActive = false;
            bool italicActive = false;
            foreach (TextTagParser.Token token in tokens)
            {
                if(token.m_Type == TextTagParser.TokenType.Clear || token.m_Type == TextTagParser.TokenType.WaitForInputAndClear)
                {
                    displayText = "";
                }
                else if (token.m_Type == TextTagParser.TokenType.BoldStart)
                {
                    displayText += "<b>";
                    boldActive = true;
                }
                else if (token.m_Type == TextTagParser.TokenType.BoldEnd)
                {
                    displayText += "</b>";
                    boldActive = false;
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicStart)
                {
                    displayText += "<i>";
                    italicActive = true;
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicEnd)
                {
                    displayText += "</i>";
                    italicActive = false;
                }
                else if (token.m_Type == TextTagParser.TokenType.Words)
                {
                    displayText += token.m_ParamList[0];
                    if (boldActive)
                    {
                        displayText += "</b>";
                    }
                    if (italicActive)
                    {
                        displayText += "</i>";
                    }
                    if (textUI.GetTruncatedText(displayText) != "")
                    {
                        return true;
                    }
                    else
                    {
                        if (boldActive)
                        {
                            displayText += "<b>";
                        }
                        if (italicActive)
                        {
                            displayText += "<i>";
                        }
                    }
                }
            }
            return false;
        }

        protected virtual List<TextTagParser.Token> GetTruncatedTokens(Text textUI, List<TextTagParser.Token> tokens)
        {
            string displayText = "";
            List<TextTagParser.Token> splitTokens = new List<TextTagParser.Token>();
            bool boldActive = false;
            bool italicActive = false;
            foreach (TextTagParser.Token token in tokens)
            {
                if (token.m_Type == TextTagParser.TokenType.Clear || token.m_Type == TextTagParser.TokenType.WaitForInputAndClear)
                {
                    splitTokens.Add(token);
                    displayText = "";
                }
                else if (token.m_Type == TextTagParser.TokenType.BoldStart)
                {
                    splitTokens.Add(token);
                    displayText += "<b>";
                    boldActive = true;
                }
                else if (token.m_Type == TextTagParser.TokenType.BoldEnd)
                {
                    splitTokens.Add(token);
                    displayText += "</b>";
                    boldActive = false;
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicStart)
                {
                    splitTokens.Add(token);
                    displayText += "<i>";
                    italicActive = true;
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicEnd)
                {
                    splitTokens.Add(token);
                    displayText += "</i>";
                    italicActive = false;
                }
                else if (token.m_Type == TextTagParser.TokenType.Words)
                {
                    displayText += token.m_ParamList[0];
                    if (boldActive)
                    {
                        displayText += "</b>";
                    }
                    if (italicActive)
                    {
                        displayText += "</i>";
                    }
                    string truncatedText = TextExtensions.GetTruncatedText(textUI, displayText);
                    if (truncatedText != "")
                    {
                        TextTagParser.Token textStart = new TextTagParser.Token();
                        textStart.m_Type = TextTagParser.TokenType.Words;
                        textStart.m_ParamList.Add(token.m_ParamList[0].Remove(token.m_ParamList[0].LastIndexOf(truncatedText)));

                        TextTagParser.Token wc = new TextTagParser.Token();
                        wc.m_Type = TextTagParser.TokenType.WaitForInputAndClear;

                        TextTagParser.Token textEnd = new TextTagParser.Token();
                        textEnd.m_Type = TextTagParser.TokenType.Words;
                        textEnd.m_ParamList.Add(truncatedText);

                        splitTokens.Add(textStart);
                        splitTokens.Add(wc);
                        splitTokens.Add(textEnd);

                        if (boldActive)
                        {
                            displayText = "<b>";
                        }
                        if (italicActive)
                        {
                            displayText = "<i>";
                        }
                    }
                    else
                    {
                        if (boldActive)
                        {
                            displayText += "<b>";
                        }
                        if (italicActive)
                        {
                            displayText += "<i>";
                        }
                        splitTokens.Add(token);
                    }
                }
                else
                {
                    splitTokens.Add(token);
                }
                
            }

            return splitTokens;
        }

        public bool IsWriting()
        {
            return m_writeCoroutine != null;
        }

        public Coroutine Write(Dictionary<string, string> localizedDictionary, string content, bool continuePrevious, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (TargetTextObject != null)
            {
                localizedDictionary.FormatLocalizedDictionary(GetFormattedText);
                TargetTextObject.RegisterLocalizableText(localizedDictionary, continuePrevious);
            }

            return Write(content, continuePrevious, audioClip, onComplete);
        }
        
        public Coroutine Write(string content, bool continuePrevious, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (!HasTextObject())
            {
                return null;
            }

            ResetControlMembers();

            m_content = content;
            m_previousText = Text;
            m_continuePrevious = continuePrevious;
            m_currentAudioClip = audioClip;

            m_currentAutoAdvanceDelay = 0f;

            return WriteInternal(content, continuePrevious, audioClip, onComplete);
        }
        
        public Coroutine WriteAndWaitForInput(Dictionary<string, string> localizedDictionary, string content, bool continuePrevious, float autoAdvanceDelay, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (TargetTextObject != null)
            {
                localizedDictionary.FormatLocalizedDictionary(GetFormattedText);
                TargetTextObject.RegisterLocalizableText(localizedDictionary, continuePrevious);
            }
            
            return WriteAndWaitForInput(content, continuePrevious, autoAdvanceDelay, audioClip, onComplete);
        }

        public Coroutine WriteAndWaitForInput(string content, bool continuePrevious, float autoAdvanceDelay, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (!HasTextObject())
            {
                return null;
            }

            ResetControlMembers();

            m_content = content;
            m_previousText = Text;
            m_continuePrevious = continuePrevious;
            m_currentAudioClip = audioClip;

            m_currentAutoAdvanceDelay = autoAdvanceDelay;

            return WriteInternal(content, continuePrevious, audioClip, onComplete);
        }

        public Coroutine WriteInstantly(Dictionary<string, string> localizedDictionary, string content, bool continuePrevious, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (TargetTextObject != null)
            {
                localizedDictionary.FormatLocalizedDictionary(GetFormattedText);
                TargetTextObject.RegisterLocalizableText(localizedDictionary, continuePrevious);
            }

            return WriteInstantly(content, continuePrevious, audioClip, onComplete);
        }

        public Coroutine WriteInstantly(string content, bool continuePrevious, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            ResetControlMembers();

            m_content = content;
            m_previousText = Text;
            m_continuePrevious = continuePrevious;
            m_currentAudioClip = audioClip;
            
            m_completeFlag = true;
            m_dontWaitFlag = true;

            return WriteInternal(content, continuePrevious, audioClip, onComplete);
        }

        protected virtual Coroutine WriteInternal(string content, bool continuePrevious, AudioClip audioClip = null, UnityAction onComplete = null)
        {
            if (onComplete != null)
            {
                OnNextWriteEnded.AddPersistentListener(onComplete);
            }

            if (m_writeCoroutine != null)
            {
                StopCoroutine(m_writeCoroutine);
            }

            if (!continuePrevious)
            {
                Text = "";
            }

            // If this clip is null then WriterAudio will play the default sound effect (if any)
            InvokeOnStart();

            TextTagParser tagParser = new TextTagParser();
            List<TextTagParser.Token> tokens = tagParser.Tokenize(content);

            if (TextUI != null)
            {
                while (tokens.Count != GetTruncatedTokens(TextUI, tokens).Count)
                {
                    tokens = GetTruncatedTokens(TextUI, tokens);
                }
            }

            m_writeCoroutine = StartCoroutine(ProcessTokens(tokens));

            return m_writeCoroutine;
        }
        
        public void Rewrite()
        {
            Stop();

            if (m_continuePrevious)
            {
                Text = m_previousText;
            }

            if (gameObject.activeInHierarchy)
            {
                WriteAndWaitForInput(m_content, m_continuePrevious, m_currentAutoAdvanceDelay, m_currentAudioClip);
            }
            else
            {
                Writer tempWriter = new GameObject("~Writer", typeof(Writer)).GetComponent<Writer>();
                Dictionary<string, object> serializedValueDictionary = this.GetUnitySerializedValueDictionary();
                tempWriter.SetUnitySerializedValues(serializedValueDictionary);
                WriteAndWaitForInput(
                    m_content, 
                    m_continuePrevious, 
                    m_currentAutoAdvanceDelay, 
                    m_currentAudioClip, 
                    () => Destroy(tempWriter.gameObject));
            }
        }

        protected static bool TryGetSingleParam(List<string> paramList, int index, float defaultValue, ref float value) 
        {
            if (paramList.Count > index) 
            {
                string paramText = paramList[index];
                string valueText = paramText;
                if (paramText.StartsWith("*", StringComparison.Ordinal) || paramText.StartsWith("/", StringComparison.Ordinal) || paramText.StartsWith("+", StringComparison.Ordinal) || paramText.StartsWith("-", StringComparison.Ordinal))
                {
                    valueText = paramText.Substring(1);
                }
                float parsedValue;
                if (Single.TryParse(valueText, out parsedValue))
                {
                    if (paramText.StartsWith("*", StringComparison.Ordinal))
                    {
                        value *= parsedValue;
                    }
                    else if (paramText.StartsWith("/", StringComparison.Ordinal))
                    {
                        value /= parsedValue;
                    }
                    else if (paramText.StartsWith("+", StringComparison.Ordinal))
                    {
                        value += parsedValue;
                    }
                    else if (paramText.StartsWith("-", StringComparison.Ordinal))
                    {
                        value -= parsedValue;
                    }
                    else
                    {
                        value = parsedValue;
                    }
                    return true;
                }
            }
            value = defaultValue;
            return false;
        }

        protected virtual IEnumerator ProcessTokens(List<TextTagParser.Token> tokens)
        {
            foreach (TextTagParser.Token token in tokens)
            {
                switch (token.m_Type)
                {
                    case TextTagParser.TokenType.Words:
                        yield return DoWords(token.m_ParamList);
                        break;
                    
                    case TextTagParser.TokenType.BoldStart:
                        m_boldActive = true;
                        break;
                    
                    case TextTagParser.TokenType.BoldEnd:
                        m_boldActive = false;
                        break;
                    
                    case TextTagParser.TokenType.ItalicStart:
                        m_italicActive = true;
                        break;
                    
                    case TextTagParser.TokenType.ItalicEnd:
                        m_italicActive = false;
                        break;

                    case TextTagParser.TokenType.SizeStart:
                        if (CheckParamCount(token.m_ParamList, 1))
                        {
                            m_sizeActive = true;
                            m_sizeText = token.m_ParamList[0];
                        }
                        break;

                    case TextTagParser.TokenType.SizeEnd:
                        m_sizeActive = false;
                        break;

                    case TextTagParser.TokenType.ColorStart:
                        if (CheckParamCount(token.m_ParamList, 1)) 
                        {
                            m_colorActive = true;
                            m_colorText = token.m_ParamList[0];
                        }
                        break;
                    
                    case TextTagParser.TokenType.ColorEnd:
                        m_colorActive = false;
                        break;

                    case TextTagParser.TokenType.AlphaStart:
                        if (CheckParamCount(token.m_ParamList, 1))
                        {
                            m_alphaActive = true;
                            float alphaValue = 1f;
                            TryGetSingleParam(token.m_ParamList, 0, alphaValue, ref alphaValue);
                            Color textColor = Color;
                            textColor.a = alphaValue;
                            m_alphaColorText = "#" + ColorUtility.ToHtmlStringRGBA(textColor);
                        }
                        break;

                    case TextTagParser.TokenType.AlphaEnd:
                        m_alphaActive = false;
                        break;

                    case TextTagParser.TokenType.Wait:
                        if (!m_dontWaitFlag)
                        {
                            yield return DoWait(token.m_ParamList);
                        }
                        break;
                    
                    case TextTagParser.TokenType.WaitForInputNoClear:
                        if (!m_dontWaitFlag)
                        {
                            yield return DoWaitForInput(false);
                        }
                        break;
                    
                    case TextTagParser.TokenType.WaitForInputAndClear:
                        if (!m_dontWaitFlag)
                        {
                            yield return DoWaitForInput(true);
                        }
                        break;
                    
                    case TextTagParser.TokenType.WaitOnPunctuationStart:
                        TryGetSingleParam(token.m_ParamList, 0, m_PunctuationPause, ref m_currentPunctuationPause);
                        break;
                    
                    case TextTagParser.TokenType.WaitOnPunctuationEnd:
                        m_currentPunctuationPause = m_PunctuationPause;
                        break;
                    
                    case TextTagParser.TokenType.Clear:
                        Text = "";
                        break;
                    
                    case TextTagParser.TokenType.SpeedStart:
                        TryGetSingleParam(token.m_ParamList, 0, m_WritingSpeed, ref m_currentWritingSpeed);
                        break;
                    
                    case TextTagParser.TokenType.SpeedEnd:
                        m_currentWritingSpeed = m_WritingSpeed;
                        break;
                    
                    case TextTagParser.TokenType.Exit:
                        m_exitFlag = true;
                        break;
                    
                    case TextTagParser.TokenType.VerticalPunch: 
                        if (!m_completeFlag)
                        {
                            float vintensity = 10.0f;
                            float time = 0.5f;
                            TryGetSingleParam(token.m_ParamList, 0, vintensity, ref vintensity);
                            TryGetSingleParam(token.m_ParamList, 1, time, ref time);
                            Shake(new Vector3(0, vintensity, 0), time);
                        }
                        break;
                    
                    case TextTagParser.TokenType.HorizontalPunch:
                        if (!m_completeFlag)
                        {
                            float hintensity = 10.0f;
                            float time = 0.5f;
                            TryGetSingleParam(token.m_ParamList, 0, hintensity, ref hintensity);
                            TryGetSingleParam(token.m_ParamList, 1, time, ref time);
                            Shake(new Vector3(hintensity, 0, 0), time);
                        }
                        break;
                    
                    case TextTagParser.TokenType.Shake:
                        if (!m_completeFlag)
                        {
                            float intensity = 10.0f;
                            float time = 0.5f;
                            TryGetSingleParam(token.m_ParamList, 0, intensity, ref intensity);
                            TryGetSingleParam(token.m_ParamList, 1, time, ref time);
                            Shake(new Vector3(intensity, intensity, 0), time);
                        }
                        break;
                    
                    case TextTagParser.TokenType.Flash:
                        if (!m_completeFlag)
                        {
                            float flashDuration = 0.2f;
                            TryGetSingleParam(token.m_ParamList, 0, flashDuration, ref flashDuration);
                            Flash(flashDuration);
                        }
                        break;

                    case TextTagParser.TokenType.Audio:
                        if (!m_completeFlag)
                        {
                            AudioSource audioSource = null;
                            if (CheckParamCount(token.m_ParamList, 1))
                            {
                                audioSource = FindAudio(token.m_ParamList[0]);
                            }
                            if (audioSource != null)
                            {
                                audioSource.PlayOneShot(audioSource.clip);
                            }
                        }
                        break;
                    
                    case TextTagParser.TokenType.AudioLoop:
                        {
                            AudioSource audioSource = null;
                            if (CheckParamCount(token.m_ParamList, 1)) 
                            {
                                audioSource = FindAudio(token.m_ParamList[0]);
                            }
                            if (audioSource != null)
                            {
                                audioSource.Play();
                                audioSource.loop = true;
                            }
                        }
                        break;
                    
                    case TextTagParser.TokenType.AudioPause:
                        {
                            AudioSource audioSource = null;
                            if (CheckParamCount(token.m_ParamList, 1)) 
                            {
                                audioSource = FindAudio(token.m_ParamList[0]);
                            }
                            if (audioSource != null)
                            {
                                audioSource.Pause();
                            }
                        }
                        break;
                    
                    case TextTagParser.TokenType.AudioStop:
                        {
                            AudioSource audioSource = null;
                            if (CheckParamCount(token.m_ParamList, 1)) 
                            {
                                audioSource = FindAudio(token.m_ParamList[0]);
                            }
                            if (audioSource != null)
                            {
                                audioSource.Stop();
                            }
                        }
                        break;
                }
                
                if (m_exitFlag)
                {
                    break;
                }
            }

            InvokeOnFinished();

            if (!m_exitFlag && !m_dontWaitFlag)
            {
                if (m_currentAutoAdvanceDelay != 0)
                {
                    yield return DoWaitForInput(false);
                }
            }

            ResetControlMembers();

            InvokeOnWriteEndedEvent();
        }

        protected virtual void ResetControlMembers()
        {
            m_boldActive = false;
            m_italicActive = false;
            m_sizeActive = false;
            m_sizeText = "";
            m_colorActive = false;
            m_colorText = "";
            m_alphaActive = false;
            m_alphaColorText = "";
            m_currentPunctuationPause = m_PunctuationPause;
            m_currentWritingSpeed = m_WritingSpeed;
            m_currentAutoAdvanceDelay = m_AutoAdvanceDelay;

            m_inputFlag = false;
            m_exitFlag = false;
            m_completeFlag = false;
            m_isWaitingForInput = false;
            m_willClearOnInput = false;
            m_dontWaitFlag = false;

            m_writeCoroutine = null;
        }

        protected virtual IEnumerator DoWords(List<string> paramList)
        {
            if (!CheckParamCount(paramList, 1))
            {
                yield break;
            }

            string param = paramList[0];
            
            string startText = Text;
            string openText = OpenMarkup();
            string closeText = CloseMarkup();

            float timeAccumulator = Time.deltaTime;

            for (int i = 0; i < param.Length; ++i)
            {
                // Exit immediately if the exit flag has been set
                if (m_exitFlag)
                {
                    break;
                }

                string left = "";
                string right = "";

                PartitionString(m_WriteWholeWords, param, i, out left, out right);
                Text = ConcatenateString(startText, openText, closeText, left, right);

                InvokeOnGlyph();

                // No delay if user has clicked and Instant Complete is enabled
                if (m_completeFlag || (m_InstantCompleteOnInput && m_inputFlag))
                {
                    continue;
                }

                // Punctuation pause
                if (left.Length > 0 && 
                    right.Length > 0 &&
                    ((IsPunctuation(left.Substring(left.Length - 1)[0]) && right.Substring(0)[0] == '.') ||
                    (IsPunctuation(left.Substring(left.Length - 1)[0]) && Char.IsWhiteSpace(right.Substring(0)[0])) || 
                    (left.Length > 1 && left.Substring(left.Length - 2)[0] == '-' && left.Substring(left.Length - 1)[0] == '-')))
                {
                    yield return DoWait(m_currentPunctuationPause);
                }

                float multipliedWritingSpeed = m_currentWritingSpeed * WriterManager.Instance.WritingSpeedMultiplier;

                // Delay between characters
                if (multipliedWritingSpeed > 0f)
                {
                    if (timeAccumulator > 0f)
                    {
                        timeAccumulator -= 1f / multipliedWritingSpeed;
                    } 
                    else
                    {
                        yield return new WaitForSeconds(1f / multipliedWritingSpeed);
                    }
                }
            }
        }
        
        public string GetFormattedText(string tokenText)
        {
            TextTagParser tagParser = new TextTagParser();
            List<TextTagParser.Token> tokens = tagParser.Tokenize(tokenText);

            string displayText = "";

            foreach (TextTagParser.Token token in tokens)
            {
                if (token.m_Type == TextTagParser.TokenType.BoldStart)
                {
                    displayText += "<b>";
                }
                else if (token.m_Type == TextTagParser.TokenType.BoldEnd)
                {
                    displayText += "</b>";
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicStart)
                {
                    displayText += "<i>";
                }
                else if (token.m_Type == TextTagParser.TokenType.ItalicEnd)
                {
                    displayText += "</i>";
                }
                else if (token.m_Type == TextTagParser.TokenType.SizeStart)
                {
                    if (CheckParamCount(token.m_ParamList, 1))
                    {
                        string sizeText = token.m_ParamList[0];
                        displayText += "<size=" + sizeText + ">";
                    }
                }
                else if (token.m_Type == TextTagParser.TokenType.SizeEnd)
                {
                    displayText += "</size>";
                }
                else if (token.m_Type == TextTagParser.TokenType.ColorStart)
                {
                    if (CheckParamCount(token.m_ParamList, 1))
                    {
                        string colorText = token.m_ParamList[0];
                        displayText += "<color=" + colorText + ">";
                    }
                }
                else if (token.m_Type == TextTagParser.TokenType.ColorEnd)
                {
                    displayText += "</color>";
                }
                else if (token.m_Type == TextTagParser.TokenType.AlphaStart)
                {
                    if (CheckParamCount(token.m_ParamList, 1))
                    {
                        float alphaValue = 1f;
                        TryGetSingleParam(token.m_ParamList, 0, alphaValue, ref alphaValue);
                        Color textColor = Color;
                        textColor.a = alphaValue;
                        string alphaColorText = "#" + ColorUtility.ToHtmlStringRGBA(textColor);
                        displayText += "<color=" + alphaColorText + ">";
                    }
                }
                else if (token.m_Type == TextTagParser.TokenType.AlphaEnd)
                {
                    displayText += "</color>";
                }
                else if (token.m_Type == TextTagParser.TokenType.Words)
                {
                    displayText += token.m_ParamList[0];
                }
            }
            return displayText;
        }

        public static bool CheckParamCount(List<string> paramList, int count)
        {
            if (paramList == null)
            {
                Debug.LogError("paramList is null");
                return false;
            }
            if (paramList.Count != count)
            {
                Debug.LogError("There must be exactly " + paramList.Count + " parameters.");
                return false;
            }
            return true;
        }

        protected void PartitionString(bool wholeWords, string inputString, int i, out string left, out string right)
        {
            left = "";
            right = "";

            if (wholeWords)
            {
                // Look ahead to find next whitespace or end of string
                for (int j = i; j < inputString.Length; ++j)
                {
                    if (Char.IsWhiteSpace(inputString[j]) ||
                        j == inputString.Length - 1)
                    {
                        left = inputString.Substring(0, j + 1);
                        right = inputString.Substring(j + 1, inputString.Length - j - 1);
                        break;
                    }
                }
            }
            else
            {
                left = inputString.Substring(0, i + 1);
                right = inputString.Substring(i + 1);
            }
        }

        protected string ConcatenateString(string startText, string openText, string closeText, string leftText, string rightText)
        {
            string tempText = startText + openText + leftText + closeText;
            
            string hiddenColor = String.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", 1, 1, 1, 0);

            // Make right hand side text hidden
            if (SupportsRichText() &&
                rightText.Length > 0)
            {
                tempText += "<color=" + hiddenColor + ">" + rightText + "</color>";
            }

            return tempText;
        }
        
        protected virtual IEnumerator DoWait(List<string> paramList)
        {
            var param = "";
            if (paramList.Count == 1)
            {
                param = paramList[0];
            }

            float duration = 1f;
            if (!Single.TryParse(param, out duration))
            {
                duration = 1f;
            }
            
            yield return DoWait(duration);
        }

        protected virtual IEnumerator DoWait(float duration)
        {
            InvokeOnPause();

            float timeRemaining = duration;
            while (timeRemaining > 0f && !m_exitFlag && !m_dontWaitFlag && !(m_InstantCompleteOnInput && m_inputFlag))
            {
                timeRemaining -= Time.deltaTime;
                yield return null;
            }

            InvokeOnResume();
        }

        protected virtual IEnumerator DoWaitForInput(bool clear)
        {
            InvokeOnPause();

            m_isWaitingForInput = true;
            m_willClearOnInput = false;
            m_inputFlag = false;
            
            float timePassed = 0;
            while ((m_currentAutoAdvanceDelay < 0 || timePassed < m_currentAutoAdvanceDelay) && !m_exitFlag && !m_dontWaitFlag && !m_inputFlag)
            {
                timePassed += Time.deltaTime;
                yield return null;
            }
            
            m_isWaitingForInput = false;
            m_willClearOnInput = false;
            m_inputFlag = false;
            
            if (clear)
            {
                Text = "";
            }

            InvokeOnResume();
        }
        
        protected virtual bool IsPunctuation(char character)
        {
            return character == '.' || 
                character == '?' ||  
                    character == '!' || 
                    character == ',' ||
                    character == ':' ||
                    character == ';' ||
                    character == ')' ||
                    character == '"';
        }
        
        protected virtual void Shake(Vector3 axis, float time)
        {
            GameObject shakeObj = m_ShakeObject;
            if (shakeObj == null)
            {
                shakeObj = gameObject;
            }
            shakeObj.transform.DOShakePosition(time, axis, 30);
        }
        
        protected virtual void Flash(float duration)
        {
            CameraManager.Instance.Fade(new Color(1f,1f,1f,1f), duration, Ease.Linear, 100, delegate {
                CameraManager.Instance.Fade(new Color(1f,1f,1f,0f), duration, Ease.Linear, 100, null);
            });
        }
        
        protected virtual AudioSource FindAudio(string audioObjectName)
        {
            GameObject go = GameObject.Find(audioObjectName);
            if (go == null)
            {
                return null;
            }
            
            return go.GetComponent<AudioSource>();
        }

        private void InvokeOnWriteEndedEvent()
        {
            OnWriteEnded.Invoke();
            OnNextWriteEnded.Invoke();
            OnNextWriteEnded.RemoveAllPersistentAndNonPersistentListeners();
        }

        protected virtual void InvokeOnInput()
        {
            OnInputDetected.Invoke();
        }
        
        protected virtual void InvokeOnStart()
        {
            OnStartWriting.Invoke();
        }

        protected virtual void InvokeOnPause()
        {
            OnPauseWriting.Invoke();
        }

        protected virtual void InvokeOnResume()
        {
            OnResumeWriting.Invoke();
        }

        protected virtual void InvokeOnFinished()
        {
            OnFinishedWriting.Invoke();
        }

        protected virtual void InvokeOnGlyph()
        {
            OnGlyph.Invoke();
        }
        
        public virtual void Advance()
        {
            m_inputFlag = true;

            if (IsWriting())
            {
                InvokeOnInput();
            }
        }

        public virtual void AutoAdvanceWhenFinished(float delay)
        {
            m_currentAutoAdvanceDelay = delay;
        }

        public virtual void Skip()
        {
            m_completeFlag = true;
            m_dontWaitFlag = true;
        }
    }

}
