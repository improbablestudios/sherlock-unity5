﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Focus On Enable")]
    public class FocusOnEnable : PersistentBehaviour
    {
        [Tooltip("The input field to focus")]
        [SerializeField]
        protected InputField m_InputField;

        [Tooltip("The priority used to determine which input field with a focus on enable component should be focused")]
        [SerializeField]
        [FormerlySerializedAs("priority")]
        protected int m_Priority = 1;

        public int Priority
        {
            get
            {
                return m_Priority;
            }
            set
            {
                m_Priority = value;
            }
        }

        public InputField InputField
        {
            get
            {
                return m_InputField;
            }
            set
            {
                m_InputField = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_InputField == null)
            {
                m_InputField = GetComponent<InputField>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_InputField == null)
            {
                m_InputField = GetComponent<InputField>();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            StartCoroutine(FocusAfterDelay());
        }

        private IEnumerator FocusAfterDelay()
        {
            yield return new WaitForEndOfFrame();

            if (EventSystem.current != null)
            {
                FocusOnEnable focusedFocusOnEnable = null;
                foreach (FocusOnEnable focusOnEnable in GetAllActive<FocusOnEnable>())
                {
                    InputField inputField = focusOnEnable.InputField as InputField;
                    if (inputField != null && inputField.isFocused)
                    {
                        focusedFocusOnEnable = focusOnEnable;
                        break;
                    }
                }
                if (focusedFocusOnEnable == null || !focusedFocusOnEnable.gameObject.activeInHierarchy || this.m_Priority <= focusedFocusOnEnable.m_Priority)
                {
                    InputField.ActivateInputField();
                }
            }
        }
    }

}