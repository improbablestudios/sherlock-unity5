﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Audio;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    public enum WriterAudioMode
    {
        Beeps,            // Use short beep sound effects
        SoundEffect,      // Use a sound effect
        VoiceOver,        // Use voiceover audio clip that plays once
    }
    
    [AddComponentMenu("UI/Writer Audio")]
    [RequireComponent(typeof(Writer))]
    public class WriterAudio : PersistentBehaviour
    {
        [Tooltip("The type of sound effect to play when writing text")]
        [SerializeField]
        [FormerlySerializedAs("audioMode")]
        protected WriterAudioMode m_AudioMode = WriterAudioMode.Beeps;

        [Tooltip("The list of beeps to randomly select when playing beep sound effects (Will play maximum of one beep per character, with only one beep playing at a time)")]
        [SerializeField]
        [FormerlySerializedAs("beepSounds")]
        protected AudioClip[] m_BeepSounds = new AudioClip[0];

        [Tooltip("The default sound effect to play when writing text")]
        [SerializeField]
        [FormerlySerializedAs("soundEffect")]
        protected AudioClip m_SoundEffect;
        
        [Tooltip("If true, loop the sound effect")]
        [SerializeField]
        [FormerlySerializedAs("loop")]
        protected bool m_LoopSoundEffect = true;

        [Tooltip("The sound effect to play on user input (e.g. a click)")]
        [SerializeField]
        [FormerlySerializedAs("inputSound")]
        protected AudioClip m_InputSound;

        [Tooltip("The audio mixer group that the voice sound should output to")]
        [SerializeField]
        [FormerlySerializedAs("m_VoiceAudioMixerGroup")]
        protected AudioMixerGroup m_VoiceOutput;

        [Tooltip("The audio mixer group that the sound effect sound should output to")]
        [SerializeField]
        [FormerlySerializedAs("m_SoundAudioMixerGroup")]
        protected AudioMixerGroup m_SoundOutput;

        // When true, a beep will be played on every written character glyph
        protected bool m_playBeeps;

        // Time when current beep will have finished playing
        protected float m_nextBeepTime;

        // AudioSource to use for playing sound effects.
        private AudioSource m_soundAudioSource;

        // AudioSource to use for playing voices
        private AudioSource m_voiceAudioSource;

        private Writer m_writer;

        public bool LoopSoundEffect
        {
            get
            {
                return m_LoopSoundEffect;
            }
            set
            {
                m_LoopSoundEffect = value;
            }
        }
        
        public WriterAudioMode AudioMode
        {
            get
            {
                return m_AudioMode;
            }
            set
            {
                m_AudioMode = value;
            }
        }

        public AudioClip[] BeepSounds
        {
            get
            {
                return m_BeepSounds;
            }
            set
            {
                m_BeepSounds = value;
            }
        }

        public AudioClip SoundEffect
        {
            get
            {
                return m_SoundEffect;
            }
            set
            {
                m_SoundEffect = value;
            }
        }

        public AudioClip InputSound
        {
            get
            {
                return m_InputSound;
            }
            set
            {
                m_InputSound = value;
            }
        }

        public AudioMixerGroup VoiceOutput
        {
            get
            {
                return m_VoiceOutput;
            }
            set
            {
                m_VoiceOutput = value;
            }
        }

        public AudioMixerGroup SoundOutput
        {
            get
            {
                return m_SoundOutput;
            }
            set
            {
                m_SoundOutput = value;
            }
        }

        public Writer Writer
        {
            get
            {
                if (m_writer == null)
                {
                    m_writer = gameObject.GetRequiredComponent<Writer>();
                }
                return m_writer;
            }
        }

        public AudioSource SoundAudioSource
        {
            get
            {
                if (m_soundAudioSource == null)
                {
                    m_soundAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("SoundAudioSource");
                    m_soundAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_soundAudioSource.playOnAwake = false;
                }
                return m_soundAudioSource;
            }
        }

        public AudioSource VoiceAudioSource
        {
            get
            {
                if (m_voiceAudioSource == null)
                {
                    m_voiceAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("VoiceAudioSource");
                    m_voiceAudioSource.outputAudioMixerGroup = m_VoiceOutput;
                    m_voiceAudioSource.playOnAwake = false;
                }
                return m_voiceAudioSource;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Writer.OnInputDetected.AddListener(OnInput);
            Writer.OnStartWriting.AddListener(OnStart);
            Writer.OnPauseWriting.AddListener(OnPause);
            Writer.OnResumeWriting.AddListener(OnResume);
            Writer.OnFinishedWriting.AddListener(OnFinished);
            Writer.OnGlyph.AddListener(OnGlyph);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Writer.OnInputDetected.RemoveListener(OnInput);
            Writer.OnStartWriting.RemoveListener(OnStart);
            Writer.OnPauseWriting.RemoveListener(OnPause);
            Writer.OnResumeWriting.AddListener(OnResume);
            Writer.OnFinishedWriting.RemoveListener(OnFinished);
            Writer.OnGlyph.RemoveListener(OnGlyph);
        }
        
        public virtual void Play(AudioClip audioClip)
        {
            if ((m_AudioMode == WriterAudioMode.SoundEffect && audioClip == null && m_SoundEffect == null) ||
                (m_AudioMode == WriterAudioMode.Beeps && (audioClip == null && (m_BeepSounds.Length == 0 || m_BeepSounds[0] == null))) ||
                (m_AudioMode == WriterAudioMode.VoiceOver && audioClip == null))
            {
                return;
            }

            VoiceAudioSource.clip = audioClip;

            if (m_AudioMode == WriterAudioMode.Beeps)
            {
                // Use beeps defined in WriterAudio
                if (VoiceAudioSource.clip == null)
                {
                    if (m_BeepSounds.Length > 0)
                    {
                        VoiceAudioSource.clip = m_BeepSounds[0];
                    }
                }
                VoiceAudioSource.loop = false;
                m_playBeeps = true;
            }
            else if (m_AudioMode == WriterAudioMode.SoundEffect)
            {
                // Use sound effects defined in WriterAudio
                if (VoiceAudioSource.clip == null)
                {
                    VoiceAudioSource.clip = m_SoundEffect;
                }
                VoiceAudioSource.loop = m_LoopSoundEffect;
                PlayVoiceAudioSource();
            }
            else if (m_AudioMode == WriterAudioMode.VoiceOver)
            {
                VoiceAudioSource.loop = false;
                PlayVoiceAudioSource();
            }
        }

        public virtual void Pause()
        {
            SoundAudioSource.Pause();
        }

        public virtual void Stop()
        {
            m_playBeeps = false;
            SoundAudioSource.Stop();
            VoiceAudioSource.Stop();
        }

        public virtual void Resume()
        {
            SoundAudioSource.UnPause();
        }

        protected virtual void OnInput()
        {
            if (m_InputSound != null)
            {
                SoundAudioSource.clip = m_InputSound;
                SoundAudioSource.Play();
            }
        }

        protected virtual void OnStart()
        {
            Play(Writer.CurrentAudioClip);
        }

        protected virtual void OnPause()
        {
            Pause();
        }

        protected virtual void OnResume()
        {
            Resume();
        }

        protected virtual void OnFinished()
        {
            Stop();
        }

        protected virtual void OnGlyph()
        {
            if (m_playBeeps && m_BeepSounds.Length > 0)
            {
                if (!VoiceAudioSource.isPlaying)
                {
                    if (m_nextBeepTime < Time.realtimeSinceStartup)
                    {
                        if (VoiceAudioSource.clip == null)
                        {
                            VoiceAudioSource.clip = m_BeepSounds[UnityEngine.Random.Range(0, m_BeepSounds.Length - 1)];
                        }
                        VoiceAudioSource.loop = false;
                        PlayVoiceAudioSource();

                        float extend = VoiceAudioSource.clip.length;

                        m_nextBeepTime = Time.realtimeSinceStartup + extend;
                    }
                }
            }
        }

        protected virtual void PlayVoiceAudioSource()
        {
            VoiceAudioSource.volume = 1f;
            VoiceAudioSource.Play();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_BeepSounds))
            {
                if (m_AudioMode != WriterAudioMode.Beeps)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_SoundEffect))
            {
                if (m_AudioMode != WriterAudioMode.SoundEffect)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_LoopSoundEffect))
            {
                if (m_AudioMode != WriterAudioMode.SoundEffect)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

    }

}