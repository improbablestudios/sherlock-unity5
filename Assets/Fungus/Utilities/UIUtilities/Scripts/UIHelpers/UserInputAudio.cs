﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [AddComponentMenu("UI/Helper/User Input Audio")]
    public class UserInputAudio : PersistentBehaviour, ISelectHandler, ISubmitHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        [Tooltip("The sound that plays when the any selectable on this game object is selected")]
        [SerializeField]
        [FormerlySerializedAs("selectedSound")]
        protected AudioClip m_SelectedSound;

        [Tooltip("The sound that plays when this game object is hovered on")]
        [SerializeField]
        protected AudioClip m_HoverOnSound;

        [Tooltip("The sound that plays when this game object is hovered off")]
        [SerializeField]
        protected AudioClip m_HoverOffSound;

        [Tooltip("The sound that plays when this game object is pressed down")]
        [SerializeField]
        protected AudioClip m_PointerDownSound;

        [Tooltip("The sound that plays when this game object is pressed up")]
        [SerializeField]
        protected AudioClip m_PointerUpSound;

        [Tooltip("The sound that plays when this game object is clicked")]
        [SerializeField]
        protected AudioClip m_ClickedSound;
        
        [Tooltip("The sound that plays when the user types on the keyboard while this game object is selected")]
        [SerializeField]
        protected AudioClip m_TypeSound;

        [Tooltip("The audio mixer group that the sounds output to")]
        [SerializeField]
        [FormerlySerializedAs("m_AudioMixerGroup")]
        protected AudioMixerGroup m_SoundOutput;

        private AudioSource m_selectedAudioSource;

        private AudioSource m_hoverOnAudioSource;

        private AudioSource m_hoverOffAudioSource;

        private AudioSource m_pointerDownAudioSource;

        private AudioSource m_pointerUpAudioSource;

        private AudioSource m_clickedAudioSource;

        private AudioSource m_typeAudioSource;

        public AudioClip SelectedSound
        {
            get
            {
                return m_SelectedSound;
            }
            set
            {
                m_SelectedSound = value;
            }
        }

        public AudioClip HoverOnSound
        {
            get
            {
                return m_HoverOnSound;
            }
            set
            {
                m_HoverOnSound = value;
            }
        }

        public AudioClip HoverOffSound
        {
            get
            {
                return m_HoverOffSound;
            }
            set
            {
                m_HoverOffSound = value;
            }
        }

        public AudioClip PointerDownSound
        {
            get
            {
                return m_PointerDownSound;
            }
            set
            {
                m_PointerDownSound = value;
            }
        }

        public AudioClip PointerUpSound
        {
            get
            {
                return m_PointerUpSound;
            }
            set
            {
                m_PointerUpSound = value;
            }
        }

        public AudioClip ClickedSound
        {
            get
            {
                return m_ClickedSound;
            }
            set
            {
                m_ClickedSound = value;
            }
        }

        public AudioClip TypeSound
        {
            get
            {
                return m_TypeSound;
            }
            set
            {
                m_TypeSound = value;
            }
        }
        
        public AudioMixerGroup SoundOutput
        {
            get
            {
                return m_SoundOutput;
            }
            set
            {
                m_SoundOutput = value;
            }
        }

        protected AudioSource SelectedAudioSource
        {
            get
            {
                if (m_selectedAudioSource == null)
                {
                    m_selectedAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("SelectedAudioSource");
                    m_selectedAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_selectedAudioSource.playOnAwake = false;
                }
                return m_selectedAudioSource;
            }
        }

        protected AudioSource HoverOnAudioSource
        {
            get
            {
                if (m_hoverOnAudioSource == null)
                {
                    m_hoverOnAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("HoverOnAudioSource");
                    m_hoverOnAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_hoverOnAudioSource.playOnAwake = false;
                }
                return m_hoverOnAudioSource;
            }
        }

        protected AudioSource HoverOffAudioSource
        {
            get
            {
                if (m_hoverOffAudioSource == null)
                {
                    m_hoverOffAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("HoverOffAudioSource");
                    m_hoverOffAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_hoverOffAudioSource.playOnAwake = false;
                }
                return m_hoverOffAudioSource;
            }
        }
        
        protected AudioSource PointerDownAudioSource
        {
            get
            {
                if (m_pointerDownAudioSource == null)
                {
                    m_pointerDownAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("PointerDownAudioSource");
                    m_pointerDownAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_pointerDownAudioSource.playOnAwake = false;
                }
                return m_pointerDownAudioSource;
            }
        }

        protected AudioSource PointerUpAudioSource
        {
            get
            {
                if (m_pointerUpAudioSource == null)
                {
                    m_pointerUpAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("PointerUpAudioSource");
                    m_pointerUpAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_pointerUpAudioSource.playOnAwake = false;
                }
                return m_pointerUpAudioSource;
            }
        }

        protected AudioSource ClickedAudioSource
        {
            get
            {
                if (m_clickedAudioSource == null)
                {
                    m_clickedAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("ClickedAudioSource");
                    m_clickedAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_clickedAudioSource.playOnAwake = false;
                }
                return m_clickedAudioSource;
            }
        }

        protected AudioSource TypeAudioSource
        {
            get
            {
                if (m_typeAudioSource == null)
                {
                    m_typeAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("TypeAudioSource");
                    m_typeAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_typeAudioSource.playOnAwake = false;
                }
                return m_typeAudioSource;
            }
        }

        protected virtual void Update()
        {
            if (m_TypeSound != null)
            {
                if (Event.current != null && Event.current.isKey)
                {
                    PlayTypeSound();
                }
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            PlaySelectedSound();
        }
        
        public void OnSubmit(BaseEventData eventData)
        {
            PlayClickedSound();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            PlayHoverOnSound();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            PlayHoverOffSound();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            PlayPointerDownSound();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            PlayPointerUpSound();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (GetComponent<Selectable>() == null)
            {
                PlayClickedSound();
            }
        }

        private void PlaySelectedSound()
        {
            PlaySound(SelectedAudioSource, SelectedSound);
        }

        private void PlayHoverOnSound()
        {
            PlaySound(HoverOnAudioSource, HoverOnSound);
        }

        private void PlayHoverOffSound()
        {
            PlaySound(HoverOffAudioSource, HoverOffSound);
        }

        private void PlayPointerDownSound()
        {
            PlaySound(PointerDownAudioSource, PointerDownSound);
        }

        private void PlayPointerUpSound()
        {
            PlaySound(PointerUpAudioSource, PointerUpSound);
        }

        private void PlayClickedSound()
        {
            PlaySound(ClickedAudioSource, ClickedSound);
        }

        private void PlayTypeSound()
        {
            PlaySound(TypeAudioSource, TypeSound);
        }

        private void PlaySound(AudioSource audioSource, AudioClip audioClip)
        {
            if (audioSource.isActiveAndEnabled)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
            }
        }
    }

}