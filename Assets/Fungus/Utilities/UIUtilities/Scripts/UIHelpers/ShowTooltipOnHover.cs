﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ImprobableStudios.Localization;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [AddComponentMenu("UI/Helper/Show Tooltip On Hover")]
    public class ShowTooltipOnHover : PersistentBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler, ILocalizable
    {
        [Tooltip("The tooltip to use to display the toolip text and icon")]
        [ObjectPopup(true)]
        [SerializeField]
        [CheckNotNull]
        protected Tooltip m_Tooltip;

        [Tooltip("The text to display in the tooltip")]
        [Localize]
        [SerializeField]
        protected string m_TooltipText;

        [Tooltip("The icon to display in the tooltip")]
        [SerializeField]
        protected Sprite m_TooltipIcon;

        [Tooltip("The position of the tooltip")]
        [SerializeField]
        protected Vector3 m_TooltipPosition;

        [Tooltip("The scale of the tooltip")]
        [SerializeField]
        protected Vector3 m_TooltipScale = Vector3.one;
        
        public Tooltip Tooltip
        {
            get
            {
                return m_Tooltip;
            }
            set
            {
                m_Tooltip = value;
            }
        }
        
        public string TooltipText
        {
            get
            {
                return m_TooltipText;
            }
            set
            {
                m_TooltipText = value;
            }
        }

        public Sprite TooltipIcon
        {
            get
            {
                return m_TooltipIcon;
            }
            set
            {
                m_TooltipIcon = value;
            }
        }

        public Vector3 TooltipPosition
        {
            get
            {
                return m_TooltipPosition;
            }
            set
            {
                m_TooltipPosition = value;
            }
        }

        public Vector3 TooltipScale
        {
            get
            {
                return m_TooltipScale;
            }
            set
            {
                m_TooltipScale = value;
            }
        }

        protected override void Awake()
        {
            m_Tooltip = InstantiateAndRegister(m_Tooltip);
            m_Tooltip.gameObject.SetActive(false);
            m_Tooltip.transform.SetParent(this.transform);
        }

        public void ShowTooltip()
        {
            if (m_Tooltip.gameObject.activeSelf)
            {
                return;
            }

            RectTransform tooltipRectTransform = m_Tooltip.GetComponent<RectTransform>();

            if (tooltipRectTransform != null)
            {
                tooltipRectTransform.anchoredPosition = m_TooltipPosition;
            }
            else
            {
                m_Tooltip.transform.localPosition = m_TooltipPosition;
            }

            m_Tooltip.transform.localScale = m_TooltipScale;

            if (m_Tooltip.m_TooltipText != null)
            {
                if (!string.IsNullOrEmpty(m_TooltipText))
                {
                    m_Tooltip.m_TooltipText.text = m_TooltipText;
                    m_Tooltip.m_TooltipText.gameObject.SetActive(true);
                }
                else
                {
                    m_Tooltip.m_TooltipText.gameObject.SetActive(false);
                }
            }

            if (m_Tooltip.m_TooltipIcon != null)
            {
                if (m_TooltipIcon != null)
                {
                    m_Tooltip.m_TooltipIcon.sprite = m_TooltipIcon;
                    m_Tooltip.m_TooltipIcon.gameObject.SetActive(true);
                }
                else
                {
                    m_Tooltip.m_TooltipIcon.gameObject.SetActive(false);
                }
            }

            m_Tooltip.Show();
        }

        public void HideTooltip()
        {
            if (!m_Tooltip.gameObject.activeSelf)
            {
                return;
            }

            m_Tooltip.Hide();
        }

        public void OnMouseEnter()
        {
            if (gameObject.GetComponent<Selectable>() == null)
            {
                ShowTooltip();
            }
        }

        public void OnMouseExit()
        {
            if (gameObject.GetComponent<Selectable>() == null)
            {
                HideTooltip();
            }
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (gameObject.GetComponent<Selectable>() != null)
            {
                ShowTooltip();
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (gameObject.GetComponent<Selectable>() != null)
            {
                HideTooltip();
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            if (gameObject.GetComponent<Selectable>() != null)
            {
                ShowTooltip();
            }
        }

        public void OnDeselect(BaseEventData eventData)
        {
            if (gameObject.GetComponent<Selectable>() != null)
            {
                HideTooltip();
            }
        }

        // ILocalizable Implementation
        
        public string GetLocalizableOrderedPriority()
        {
            return null;
        }

        public void OnLocalizeComplete()
        {
        }
    }

}
