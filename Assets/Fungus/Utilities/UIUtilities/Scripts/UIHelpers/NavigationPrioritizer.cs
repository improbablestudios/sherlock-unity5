﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Navigation Prioritizer")]
    public class NavigationPrioritizer : PersistentBehaviour
    {
        [Tooltip("When navigating between navigation groups, navigation will favor the selectables with the highest navigation priority within their parent navigation group")]
        [SerializeField]
        [FormerlySerializedAs("priority")]
        protected int m_Priority = 1;

        private Selectable m_selectable;

        public int Priority
        {
            get
            {
                return m_Priority;
            }
            set
            {
                m_Priority = value;
            }
        }

        public Selectable Selectable
        {
            get
            {
                if (m_selectable == null)
                {
                    m_selectable = GetComponent<Selectable>();
                }
                return m_selectable;
            }
        }
    }

}