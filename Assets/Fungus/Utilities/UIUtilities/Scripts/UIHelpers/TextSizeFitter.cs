﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("UI/Helper/Text Size Fitter")]
    public class TextSizeFitter : PersistentBehaviour
    {
        [Tooltip("The rect transform will be resized automatically whenever the text changes")]
        [SerializeField]
        protected bool m_FitAutomatically = true;

        [Tooltip("The text that will be used to determine the size of the rect transform attached to this game object")]
        [SerializeField]
        protected Text m_Text;

        [Tooltip("If true, allow the rect transform to expand or shrink horizontally to fit the text")]
        [SerializeField]
        [FormerlySerializedAs("m_ExpandWidth")]
        protected bool m_FitWidth = true;

        [Tooltip("Ensure the rect transform is at least this width")]
        [Min(0)]
        [SerializeField]
        protected float m_MinWidth;

        [Tooltip("Ensure the rect transform is at most this width")]
        [Min(0)]
        [SerializeField]
        protected float m_MaxWidth = 2000f;

        [Tooltip("The amount of horizontal padding around the text")]
        [SerializeField]
        protected float m_PaddingWidth = 20f;

        [Tooltip("If true, allow the rect transform to expand or shrink vertically to fit the text")]
        [SerializeField]
        [FormerlySerializedAs("m_ExpandHeight")]
        protected bool m_FitHeight;

        [Tooltip("Ensure the rect transform is at least this height")]
        [SerializeField]
        [Min(0)]
        protected float m_MinHeight;

        [Tooltip("Ensure the rect transform is at most this height")]
        [SerializeField]
        [Min(0)]
        protected float m_MaxHeight = 2000f;

        [Tooltip("The amount of vertical padding around the text")]
        [SerializeField]
        protected float m_PaddingHeight = 20f;

        [Tooltip("If true, don't pad the text when it is empty")]
        [SerializeField]
        protected bool m_DontUsePaddingWhenEmpty;

        [Tooltip("If greater than 0, animate the rect transform expanding or shrinking to fit the text")]
        [Min(0)]
        [SerializeField]
        protected float m_AnimationDuration;

        [Tooltip("The ease type used for the rect transform animation")]
        [SerializeField]
        protected Ease m_AnimationEase = Ease.InOutQuad;

        private Tweener m_widthTween;

        private Tweener m_heightTween;

        private RectTransform m_rectTransform;

        private LayoutElement m_layoutElement;

        public Text Text
        {
            get
            {
                return m_Text;
            }
            set
            {
                m_Text = value;
            }
        }
        
        public bool FitWidth
        {
            get
            {
                return m_FitWidth;
            }
            set
            {
                m_FitWidth = value;
            }
        }

        public float MinWidth
        {
            get
            {
                return m_MinWidth;
            }
            set
            {
                m_MinWidth = value;
            }
        }

        public float MaxWidth
        {
            get
            {
                return m_MaxWidth;
            }
            set
            {
                m_MaxWidth = value;
            }
        }

        public float PaddingWidth
        {
            get
            {
                return m_PaddingWidth;
            }
            set
            {
                m_PaddingWidth = value;
            }
        }

        public bool FitHeight
        {
            get
            {
                return m_FitHeight;
            }
            set
            {
                m_FitHeight = value;
            }
        }

        public float MinHeight
        {
            get
            {
                return m_MinHeight;
            }
            set
            {
                m_MinHeight = value;
            }
        }

        public float MaxHeight
        {
            get
            {
                return m_MaxHeight;
            }
            set
            {
                m_MaxHeight = value;
            }
        }

        public float PaddingHeight
        {
            get
            {
                return m_PaddingHeight;
            }
            set
            {
                m_PaddingHeight = value;
            }
        }

        public bool DontUsePaddingWhenEmpty
        {
            get
            {
                return m_DontUsePaddingWhenEmpty;
            }
            set
            {
                m_DontUsePaddingWhenEmpty = value;
            }
        }

        public float AnimationDuration
        {
            get
            {
                return m_AnimationDuration;
            }
            set
            {
                m_AnimationDuration = value;
            }
        }

        public Ease AnimationEase
        {
            get
            {
                return m_AnimationEase;
            }
            set
            {
                m_AnimationEase = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            m_MaxWidth = Math.Max(m_MinWidth, m_MaxWidth);
            m_MaxHeight = Math.Max(m_MinHeight, m_MaxHeight);
        }

        protected override void Awake()
        {
            base.Awake();
            m_rectTransform = gameObject.GetComponent<RectTransform>();
            m_layoutElement = gameObject.GetComponent<LayoutElement>();
        }

        protected void OnBecameVisible()
        {
            if (m_FitAutomatically)
            {
                Fit();
            }
        }

        private Vector2 GetTargetSize()
        {
            if (m_rectTransform == null)
            {
                m_rectTransform = gameObject.GetComponent<RectTransform>();
            }
            if (m_layoutElement == null)
            {
                m_layoutElement = gameObject.GetComponent<LayoutElement>();
            }

            float width = m_rectTransform.sizeDelta.x;
            float height = m_rectTransform.sizeDelta.y;
            
            if (m_Text != null)
            {
                if (m_FitWidth)
                {
                    float extraWidth = 0;
                    if (m_Text.transform.parent != null)
                    {
                        HorizontalLayoutGroup horizontalLayoutGroup = m_Text.transform.parent.GetComponent<HorizontalLayoutGroup>();
                        if (horizontalLayoutGroup != null && Array.IndexOf(GetComponentsInChildren<HorizontalLayoutGroup>(true), horizontalLayoutGroup) >= 0)
                        {
                            foreach (Transform t in horizontalLayoutGroup.transform)
                            {
                                if (t.gameObject.activeSelf && t != m_Text.transform)
                                {
                                    RectTransform rt = t.GetComponent<RectTransform>();
                                    extraWidth += rt.sizeDelta.x + horizontalLayoutGroup.spacing;
                                }
                            }
                        }
                    }
                    float padding = 0;
                    if (!string.IsNullOrEmpty(m_Text.text) || !m_DontUsePaddingWhenEmpty)
                    {
                        padding = m_PaddingWidth * 2;
                    }
                    width = Mathf.Clamp(m_Text.preferredWidth + extraWidth + padding, m_MinWidth, m_MaxWidth);
                }


                if (m_FitHeight)
                {
                    float extraHeight = 0;
                    if (m_Text.transform.parent != null)
                    {
                        VerticalLayoutGroup verticalLayoutGroup = m_Text.transform.parent.GetComponent<VerticalLayoutGroup>();
                        if (verticalLayoutGroup != null && Array.IndexOf(GetComponentsInChildren<VerticalLayoutGroup>(true), verticalLayoutGroup) >= 0)
                        {
                            foreach (Transform t in verticalLayoutGroup.transform)
                            {
                                if (t.gameObject.activeSelf && t != m_Text.transform)
                                {
                                    RectTransform rt = t.GetComponent<RectTransform>();
                                    extraHeight += rt.sizeDelta.y + verticalLayoutGroup.spacing;
                                }
                            }
                        }
                    }
                    float padding = 0;
                    if (!string.IsNullOrEmpty(m_Text.text) || !m_DontUsePaddingWhenEmpty)
                    {
                        padding = m_PaddingHeight * 2;
                    }
                    height = Mathf.Clamp(m_Text.preferredHeight + extraHeight + padding, m_MinHeight, m_MaxHeight);
                }

                return new Vector2(width, height);
            }

            return Vector2.zero;
        }

        void Update()
        {
            if (m_FitAutomatically)
            {
                Fit();
            }
        }

        public void Fit(UnityAction onComplete = null)
        {
            Vector2 newTargetSize = GetTargetSize();

            if (m_rectTransform == null)
            {
                m_rectTransform = gameObject.GetComponent<RectTransform>();
            }
            if (m_layoutElement == null)
            {
                m_layoutElement = gameObject.GetComponent<LayoutElement>();
            }

            if (m_rectTransform.sizeDelta != newTargetSize &&
                (m_FitWidth || m_FitHeight) &&
                (m_widthTween == null || !m_widthTween.IsPlaying()) &&
                (m_heightTween == null || !m_heightTween.IsPlaying()))
            {
                if (m_layoutElement != null)
                {
                    if (Application.isPlaying && m_AnimationDuration > 0)
                    {
                        if (m_FitWidth && m_FitHeight)
                        {
                            m_widthTween = DOPreferredWidth(newTargetSize.x)
                                .OnComplete(
                                    () =>
                                    {
                                        DOPreferredHeight()
                                            .OnComplete(
                                                () =>
                                                {
                                                    if (onComplete != null)
                                                    {
                                                        onComplete();
                                                    }
                                                });
                                    });
                        }

                        if (m_FitWidth && !m_FitHeight)
                        {
                            m_widthTween = DOPreferredWidth(newTargetSize.x)
                                .OnComplete(
                                    () =>
                                    {
                                        if (onComplete != null)
                                        {
                                            onComplete();
                                        }
                                    });
                        }

                        if (m_FitHeight && !m_FitWidth)
                        {
                            m_heightTween = DOPreferredHeight(newTargetSize.y)
                                .OnComplete(
                                    () =>
                                    {
                                        if (onComplete != null)
                                        {
                                            onComplete();
                                        }
                                    });
                        }
                    }
                    else
                    {
                        m_layoutElement.preferredWidth = newTargetSize.x;
                        m_layoutElement.preferredHeight = newTargetSize.y;
                        LayoutRebuilder.MarkLayoutForRebuild(m_rectTransform);
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    }
                }
                else
                {
                    if (Application.isPlaying && m_AnimationDuration > 0)
                    {
                        if (m_FitWidth && m_FitHeight)
                        {
                            m_widthTween = DOSizeDeltaWidth(newTargetSize.x)
                                .OnComplete(
                                    () =>
                                    {
                                        m_heightTween = DOSizeDeltaHeight()
                                            .OnComplete(
                                                () =>
                                                {
                                                    if (onComplete != null)
                                                    {
                                                        onComplete();
                                                    }
                                                });
                                    });
                        }

                        if (m_FitWidth && !m_FitHeight)
                        {
                            m_widthTween = DOSizeDeltaWidth(newTargetSize.x)
                                .OnComplete(
                                    () =>
                                    {
                                        if (onComplete != null)
                                        {
                                            onComplete();
                                        }
                                    });
                        }

                        if (m_FitHeight && !m_FitWidth)
                        {
                            m_heightTween = DOSizeDeltaHeight(newTargetSize.y)
                                .OnComplete(
                                    () =>
                                    {
                                        if (onComplete != null)
                                        {
                                            onComplete();
                                        }
                                    });
                        }
                    }
                    else
                    {
                        m_rectTransform.sizeDelta = newTargetSize;
                        LayoutRebuilder.MarkLayoutForRebuild(m_rectTransform);
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    }
                }
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            }
        }

        public void StopFitting()
        {
            if (m_heightTween != null && m_heightTween.IsPlaying())
            {
                m_heightTween.Kill();
            }
            if (m_widthTween != null && m_widthTween.IsPlaying())
            {
                m_widthTween.Kill();
            }
        }

        private Tweener DOPreferredWidth(float width)
        {
            return m_layoutElement.DOPreferredSize(new Vector2(width, m_layoutElement.preferredHeight), m_AnimationDuration).SetEase(m_AnimationEase);
        }
        
        private Tweener DOPreferredHeight()
        {
            return DOPreferredHeight(GetTargetSize().y);
        }

        private Tweener DOPreferredHeight(float height)
        {
            return m_layoutElement.DOPreferredSize(new Vector2(m_layoutElement.preferredWidth, height), m_AnimationDuration).SetEase(m_AnimationEase);
        }

        private Tweener DOSizeDeltaWidth(float width)
        {
            return m_rectTransform.DOSizeDelta(new Vector2(width, m_rectTransform.sizeDelta.y), m_AnimationDuration).SetEase(m_AnimationEase);
        }

        private Tweener DOSizeDeltaHeight()
        {
            return DOSizeDeltaHeight(GetTargetSize().y);
        }

        private Tweener DOSizeDeltaHeight(float height)
        {
            return m_rectTransform.DOSizeDelta(new Vector2(m_rectTransform.sizeDelta.x, height), m_AnimationDuration).SetEase(m_AnimationEase);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_MinWidth))
            {
                if (!m_FitWidth)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxWidth))
            {
                if (!m_FitWidth)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_PaddingWidth))
            {
                if (!m_FitWidth)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MinHeight))
            {
                if (!m_FitHeight)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxHeight))
            {
                if (!m_FitHeight)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_PaddingHeight))
            {
                if (!m_FitHeight)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AnimationEase))
            {
                if (m_AnimationDuration <= 0)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyIndentLevelOffset(string propertyPath)
        {
            if (propertyPath == "m_" + nameof(MinWidth))
            {
                return 1;
            }
            if (propertyPath == "m_" + nameof(MaxWidth))
            {
                return 1;
            }
            if (propertyPath == "m_" + nameof(PaddingWidth))
            {
                return 1;
            }
            if (propertyPath == "m_" + nameof(MinHeight))
            {
                return 1;
            }
            if (propertyPath == "m_" + nameof(MaxHeight))
            {
                return 1;
            }
            if (propertyPath == "m_" + nameof(PaddingHeight))
            {
                return 1;
            }
            return base.GetPropertyIndentLevelOffset(propertyPath);
        }
    }

}