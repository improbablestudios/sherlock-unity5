﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(InputField))]
    [AddComponentMenu("UI/Helper/Force Input Field Capitalization")]
    public class ForceInputFieldCapitalization : PersistentBehaviour
    {
        [Tooltip("Determines if input field text is forced to be uppercase or lowercase")]
        [SerializeField]
        [FormerlySerializedAs("forceCapitalization")]
        protected Capitalization m_ForceCapitalization;

        private InputField m_inputField;

        public Capitalization ForceCapitalization
        {
            get
            {
                return m_ForceCapitalization;
            }
            set
            {
                m_ForceCapitalization = value;
            }
        }

        public InputField InputField
        {
            get
            {
                if (m_inputField == null)
                {
                    m_inputField = GetComponent<InputField>();
                }
                return m_inputField;
            }
        }

        public void Start()
        {
            InputField.onValidateInput += (string input, int charIndex, char addedChar) => Capitalize(addedChar);
        }

        private char Capitalize(char charToValidate)
        {
            string stringCharToValidate = charToValidate.ToString();
            bool charIsLowercase = stringCharToValidate == stringCharToValidate.ToLower();
            if (m_ForceCapitalization == Capitalization.Uppercase)
            {
                if (charIsLowercase)
                {
                    charToValidate = stringCharToValidate.ToUpper().ToCharArray()[0];
                }
            }
            else if (m_ForceCapitalization == Capitalization.Lowercase)
            {
                if (!charIsLowercase)
                {
                    charToValidate = stringCharToValidate.ToLower().ToCharArray()[0];
                }
            }

            return charToValidate;
        }
    }

}
