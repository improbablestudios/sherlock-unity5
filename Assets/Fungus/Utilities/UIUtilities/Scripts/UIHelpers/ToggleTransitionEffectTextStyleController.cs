﻿using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.TextStylePaletteUtilities;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(ToggleTransitionEffect))]
    [AddComponentMenu("UI/Helper/Toggle Transition Effect FontData Controller")]
    public class ToggleTransitionEffectTextStyleController : TextStyleController<ToggleTransitionEffect>
    {
        [Tooltip("The color swatch to use for the selectable's highlighted transition")]
        [SerializeField]
        [FormerlySerializedAs("m_OffPaletteFontDataName")]
        protected TextStyle m_OffTextStyle;

        [Tooltip("The color swatch to use for the selectable's normal transition")]
        [SerializeField]
        [FormerlySerializedAs("m_OnPaletteFontDataName")]
        protected TextStyle m_OnTextStyle;

        public TextStyle OffTextStyle
        {
            get
            {
                return m_OffTextStyle;
            }
            set
            {
                m_OffTextStyle = value;
            }
        }

        public TextStyle OnTextStyle
        {
            get
            {
                return m_OnTextStyle;
            }
            set
            {
                m_OnTextStyle = value;
            }
        }

        public override TextStyle[] GetTextStyles()
        {
            return new TextStyle[]
            {
                m_OffTextStyle,
                m_OnTextStyle
            };
        }

        public override void SetTextStyles(TextStyle[] colorSwatches)
        {
            if (colorSwatches.Length > 0)
            {
                m_OffTextStyle = colorSwatches[0];
            }
            if (colorSwatches.Length > 1)
            {
                m_OnTextStyle = colorSwatches[1];
            }
        }

        public override FontData[] GetFontDatas()
        {
            return new FontData[]
            {
                Target.TransitionStates[0].TextStyle,
                Target.TransitionStates[1].TextStyle,
            };
        }

        public override void SetFontDatas(FontData[] fontDatas)
        {
            if (fontDatas.Length > 0)
            {
                Target.TransitionStates[0].TextStyle = fontDatas[0];
            }
            if (fontDatas.Length > 1)
            {
                Target.TransitionStates[1].TextStyle = fontDatas[1];
            }
        }
    }

}