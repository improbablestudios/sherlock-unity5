﻿using UnityEngine;
using System;
using UnityEngine.Playables;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(PlayableDirector))]
    [AddComponentMenu("UI/Playback/Playable Playback Controller")]
    public class PlayablePlaybackController : PlaybackController
    {
        #region Private Fields
        private PlayableDirector m_playableDirector;
        #endregion

        #region Public Properties
        public PlayableDirector PlayableDirector
        {
            get
            {
                if (m_playableDirector == null)
                {
                    m_playableDirector = GetComponent<PlayableDirector>();
                }
                return m_playableDirector;
            }
        }
        #endregion

        #region Public Methods
        public void Setup(PlayableAsset playableAsset)
        {
            PlayableDirector.playableAsset = playableAsset;
            PlayableDirector.RebuildGraph();

            if (CanPlay())
            {
                Setup();
            }
        }

        public void Play(PlayableAsset playableAsset)
        {
            PlayableDirector.playableAsset = playableAsset;
            PlayableDirector.RebuildGraph();

            if (CanPlay())
            {
                Play();
                OnPlaybackStarted(PlayableDirector);
            }
        }
        #endregion

        #region MonoBehaviour Methods
        protected override void Update()
        {
            base.Update();

            if (!IsFinished && !WasSkipped)
            {
                if (PlayableDirector.playableGraph.IsValid() && PlayableDirector.time >= PlayableDirector.duration)
                {
                    OnPlaybackFinished(PlayableDirector);
                }
            }
        }
        #endregion

        #region Override Methods
        protected override UnityEngine.Object GetPlaybackSource()
        {
            return PlayableDirector;
        }

        protected override bool CanPlay()
        {
            return PlayableDirector.playableGraph.IsValid();
        }

        protected override bool IsPrepared()
        {
            return true;
        }

        protected override bool IsPlaying()
        {
            return PlayableDirector.playableGraph.IsValid() && PlayableDirector.playableGraph.IsPlaying();
        }

        protected override void DoSetupTargets()
        {
        }

        protected override float DoGetVolume()
        {
            if (m_AudioSource != null)
            {
                return m_AudioSource.volume;
            }

            return 1f;
        }

        protected override void DoSetVolume(float volume)
        {
            if (m_AudioSource != null)
            {
                m_AudioSource.volume = volume;
            }
        }

        protected override bool DoGetMuteState()
        {
            if (m_AudioSource != null)
            {
                return m_AudioSource.mute;
            }

            return false;
        }

        protected override void DoSetMuteState(bool mute)
        {
            if (m_AudioSource != null)
            {
                m_AudioSource.mute = mute;
            }
        }

        protected override float DoGetMaxPlaybackTime()
        {
            return (float)PlayableDirector.duration;
        }

        protected override float DoGetCurrentPlaybackTime()
        {
            return (float)PlayableDirector.time;
        }

        protected override void DoSetCurrentPlaybackTime(float time)
        {
            PlayableDirector.time = time;
        }

        protected override void DoSetPlayOnAwake(bool playOnAwake)
        {
            PlayableDirector.playOnAwake = playOnAwake;
        }

        protected override void DoClear()
        {
        }

        protected override void DoPreparePlayback()
        {
        }

        protected override void DoStartPlayback()
        {
            PlayableDirector.Play();
        }

        protected override void DoUpdatePlayback()
        {
            PlayableDirector.Evaluate();
        }

        protected override void DoPausePlayback()
        {
            PlayableDirector.Pause();
        }

        protected override void DoResumePlayback()
        {
            PlayableDirector.Resume();
        }

        protected override void DoStopPlayback()
        {
            PlayableDirector.Stop();
        }

        protected override void SubscribeToOnPrepared(Action<UnityEngine.Object> action)
        {
        }

        protected override void SubscribeToOnStarted(Action<UnityEngine.Object> action)
        {
            PlayableDirector.played += (PlayableDirector playableDirector) => action(playableDirector);
        }

        protected override void SubscribeToOnFinished(Action<UnityEngine.Object> action)
        {
        }

        protected override void UnsubscribeFromOnPrepared(Action<UnityEngine.Object> action)
        {
        }

        protected override void UnsubscribeFromOnStarted(Action<UnityEngine.Object> action)
        {
            PlayableDirector.played -= (PlayableDirector playableDirector) => action(playableDirector);
        }

        protected override void UnsubscribeFromOnFinished(Action<UnityEngine.Object> action)
        {
        }
        #endregion
    }

}