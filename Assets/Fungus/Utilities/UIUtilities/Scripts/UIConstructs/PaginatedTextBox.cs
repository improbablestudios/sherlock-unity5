﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Paginated Text Box")]
    public class PaginatedTextBox : UIConstruct, ITextComponent
    {
        [Tooltip("The text content to populate the text ui with")]
        [TextArea]
        [SerializeField]
        [FormerlySerializedAs("textContent")]
        [FormerlySerializedAs("m_TextContent")]
        protected string m_Content = "";

        [Tooltip("The text ui used to display the text content")]
        [SerializeField]
        [FormerlySerializedAs("textUI")]
        [FormerlySerializedAs("m_TextUi")]
        [CheckNotNull]
        protected Text m_Text;

        [Tooltip("The button that changes the current page displayed to the previous page")]
        [SerializeField]
        [FormerlySerializedAs("previousPageButton")]
        protected Button m_PreviousPageButton;

        [Tooltip("The button that changes the current page displayed to the next page")]
        [SerializeField]
        [FormerlySerializedAs("nextPageButton")]
        protected Button m_NextPageButton;

        [Tooltip("The text ui used to display the page number")]
        [SerializeField]
        [FormerlySerializedAs("pageNumberText")]
        protected Text m_PageNumberText;

        [Tooltip("The current page to display")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("currentPage")]
        protected int m_CurrentPage = 0;

        protected List<string> m_pages = new List<string>();
        
        public string Content
        {
            get
            {
                return m_Content;
            }
            set
            {
                m_Content = value;
            }
        }
        
        public Text Text
        {
            get
            {
                return m_Text;
            }
            set
            {
                m_Text = value;
            }
        }

        public Button PreviousPageButton
        {
            get
            {
                return m_PreviousPageButton;
            }
            set
            {
                m_PreviousPageButton = value;
            }
        }

        public Button NextPageButton
        {
            get
            {
                return m_NextPageButton;
            }
            set
            {
                m_NextPageButton = value;
            }
        }

        public Text PageNumberText
        {
            get
            {
                return m_PageNumberText;
            }
            set
            {
                m_PageNumberText = value;
            }
        }

        public int CurrentPage
        {
            get
            {
                return m_CurrentPage;
            }
            set
            {
                m_CurrentPage = value;
            }
        }

        public string[] Pages
        {
            get
            {
                return m_pages.ToArray();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_PreviousPageButton != null)
            {
                m_PreviousPageButton.onClick.AddListener(OnPreviousPage);
            }
            if (m_NextPageButton != null)
            {
                m_NextPageButton.onClick.AddListener(OnNextPage);
            }
            Update();
        }

        protected override void Update()
        {
            base.Update();

            string plainText = TextExtensions.RemoveUnityTags(m_Content);

            m_pages.Clear();

            SplitIntoPages(plainText, m_pages);

            if (m_pages.Count == 0)
            {
                m_CurrentPage = 0;
            }
            else if (m_CurrentPage < 0)
            {
                m_CurrentPage = 0;
            }
            else if (m_CurrentPage > m_pages.Count - 1)
            {
                m_CurrentPage = m_pages.Count - 1;
            }

            if (m_PreviousPageButton != null)
            {
                if (m_CurrentPage <= 0)
                {
                    m_PreviousPageButton.gameObject.SetActive(false);
                }
                else
                {
                    m_PreviousPageButton.gameObject.SetActive(true);
                }
            }

            if (m_NextPageButton != null)
            {
                if (m_CurrentPage >= m_pages.Count - 1)
                {
                    m_NextPageButton.gameObject.SetActive(false);
                }
                else
                {
                    m_NextPageButton.gameObject.SetActive(true);
                }
            }

            string pageText = "";
            if (m_CurrentPage >= 0 && m_CurrentPage <= m_pages.Count - 1)
            {
                pageText = m_pages[m_CurrentPage];
            }
            if (m_Text != null)
            {
                m_Text.text = pageText;
            }

            if (m_PageNumberText != null)
            {
                m_PageNumberText.text = (m_CurrentPage + 1) + "/" + m_pages.Count;
            }
        }

        void SplitIntoPages(string stringText, List<string> pages)
        {
            string page = stringText;
            string leftoverText = "";
            if (m_Text != null)
            {
                leftoverText = TextExtensions.GetTruncatedText(m_Text, stringText);
            }
            if (leftoverText != "")
            {
                page = stringText.Remove(stringText.LastIndexOf(leftoverText));
            }
            if (page != "")
            {
                pages.Add(page);
            }
            if (leftoverText != "")
            {
                SplitIntoPages(leftoverText, pages);
            }
        }

        void OnPreviousPage()
        {
            if (m_CurrentPage <= 0)
            {
                return;
            }
            else
            {
                m_CurrentPage--;
            }
        }

        void OnNextPage()
        {
            if (m_CurrentPage >= m_pages.Count - 1)
            {
                return;
            }
            else
            {
                m_CurrentPage++;
            }
        }

        // ITextComponent Implementation

        public string GetString()
        {
            return Content;
        }

        public void SetString(string value)
        {
            Content = value;
        }

        public Component GetComponent()
        {
            return this;
        }
    }

}
