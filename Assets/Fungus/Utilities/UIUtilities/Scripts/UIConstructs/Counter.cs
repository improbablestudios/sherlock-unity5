﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.Localization.SmartFormat;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class CounterWarning
    {
        [FormerlySerializedAs("warningTriggerValue")]
        public float m_WarningTriggerValue;

        [Localize]
        [FormerlySerializedAs("warningMessage")]
        public string m_WarningMessage = "";

        [FormerlySerializedAs("warningColor")]
        public Color m_WarningColor = Color.red;

        [FormerlySerializedAs("warningSound")]
        public AudioClip m_WarningSound;

        [FormerlySerializedAs("loopWarningSound")]
        public bool m_LoopWarningSound;

        [FormerlySerializedAs("warningSoundPitch")]
        public float m_WarningSoundPitch = 1f;

        protected bool m_valueWasReached = false;

        public bool ValueWasReached
        {
            get
            {
                return m_valueWasReached;
            }
            set
            {
                m_valueWasReached = value;
            }
        }

        public CounterWarning()
        {
        }

        public CounterWarning(float displayValue, string warningMessage, Color warningColor, AudioClip warningSound, bool loopWarningSound, float warningSoundPitch)
        {
            m_WarningTriggerValue = displayValue;
            m_WarningMessage = warningMessage;
            m_WarningColor = warningColor;
            m_WarningSound = warningSound;
            m_LoopWarningSound = loopWarningSound;
            m_WarningSoundPitch = warningSoundPitch;
        }

        public CounterWarning(float displayValue, string warningMessage)
        {
            m_WarningTriggerValue = displayValue;
            m_WarningMessage = warningMessage;
        }

        public void ResetTracking()
        {
            m_valueWasReached = false;
        }
    }

    [AddComponentMenu("UI/Counter")]
    public class Counter : UIConstruct, ILocalizable
    {
        public enum CounterTransitionStateType
        {
            CountDown,
            CountUp,
        }

        private static Counter s_defaultCounter;

        [Tooltip("Setup this counter as soon as it's enabled")]
        [SerializeField]
        protected bool m_SetupOnEnable = true;

        [Tooltip("Only allow the counter to finish if it is not currently hidden")]
        [SerializeField]
        protected bool m_OnlyFinishWhenVisible;

        [Tooltip("Reset counter after it has finished counting")]
        [SerializeField]
        protected bool m_ResetAfterFinished;

        [Tooltip("If true, the value will change instantly. Otherwise, it will animate")]
        [SerializeField]
        protected bool m_CountInstantly;

        [Tooltip("If true, this counter will count down, otherwise, it will count up")]
        [SerializeField]
        [FormerlySerializedAs("countdown")]
        protected bool m_Countdown = true;

        [Tooltip("Ensure value is always a whole number")]
        [SerializeField]
        protected bool m_WholeNumbers;

        [Tooltip("If true, this counter will count up to the max value and then stop")]
        [SerializeField]
        protected bool m_HasMaxValue = true;

        [Tooltip("The text ui that is used to display info and warnings")]
        [FormerlySerializedAs("infoText")]
        [SerializeField]
        protected Text m_InfoText;

        [Tooltip("The text ui that is used to display the current counter value")]
        [FormerlySerializedAs("valueText")]
        [SerializeField]
        protected Text m_ValueText;

        [Tooltip("The scroll rect that will scroll as the counter counts up or down")]
        [SerializeField]
        protected ScrollRect m_ValueScroll;

        [Tooltip("The texts that will be set when doing a scrolling count transition.")]
        [SerializeField]
        protected List<Text> m_ScrollingValueTexts = new List<Text>();

        [Tooltip("The text ui that is used to display the min counter value")]
        [FormerlySerializedAs("valueText")]
        [SerializeField]
        protected Text m_MinValueText;

        [Tooltip("The text ui that is used to display the max counter value")]
        [FormerlySerializedAs("valueText")]
        [SerializeField]
        protected Text m_MaxValueText;

        [Tooltip("The slider that will fill as the counter counts up and unfill as the counter goes down")]
        [FormerlySerializedAs("valueSlider")]
        [SerializeField]
        protected Slider m_ValueSlider;

        [Tooltip("The format in which the current value will be displayed (e.g. 'you have earned {0} out of {1} points'. ({0} = current value, {1} = max value, {2} = min value))")]
        [Localize]
        [SerializeField]
        [FormerlySerializedAs("valueFormat")]
        protected string m_ValueFormat = "{0}/{1}";

        [Tooltip("The format in which the current value will be displayed (e.g. 'you have earned {0} out of {1} points'. ({0} = current value, {1} = max value, {2} = min value))")]
        [Localize]
        [SerializeField]
        [FormerlySerializedAs("valueFormat")]
        protected string m_MinValueFormat = "Min: {2}";

        [Tooltip("The format in which the current value will be displayed (e.g. 'you have earned {0} out of {1} points'. ({0} = current value, {1} = max value, {2} = min value))")]
        [Localize]
        [SerializeField]
        [FormerlySerializedAs("valueFormat")]
        protected string m_MaxValueFormat = "Max: {1}";

        [Tooltip("The min value the counter will count down to or count up from")]
        [SerializeField]
        protected float m_MinValue = 0;

        [Tooltip("The max value the counter will count down from or count up to")]
        [SerializeField]
        [FormerlySerializedAs("maxValue")]
        protected float m_MaxValue = 10;

        [Tooltip("The current value of the counter")]
        [SerializeField]
        protected float m_Value;

        [Tooltip("The warnings that appear when the counter reaches certain values")]
        [FormerlySerializedAs("warnings")]
        [SerializeField]
        protected CounterWarning[] m_Warnings = new CounterWarning[0];

        [Tooltip("The graphics that will be colored when a warning is active")]
        [SerializeField]
        [FormerlySerializedAs("coloredWarningGraphics")]
        protected Graphic[] m_ColoredWarningGraphics = new Graphic[0];

        [SerializeField]
        protected UnityEvent m_OnSet = new UnityEvent();

        protected UnityEvent m_OnNextSet = new UnityEvent();

        protected Dictionary<string, string> m_localizedCounterFormatDictionary;

        protected bool m_needsReset = true;

        protected bool m_neverUpdated = true;

        protected float m_transitionEndDisplayedValue;

        protected float m_transitionStartDisplayedValue;

        protected float m_transitionMaxDisplayedValue;

        private AudioSource m_audioSource;

        public static Counter DefaultCounter
        {
            get
            {
                if (s_defaultCounter == null)
                {
                    s_defaultCounter = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<Counter>("~Counter"));
                }
                return s_defaultCounter;
            }
        }

        public bool SetupOnEnable
        {
            get
            {
                return m_SetupOnEnable;
            }
            set
            {
                m_SetupOnEnable = value;
            }
        }

        public bool OnlyFinishWhenVisible
        {
            get
            {
                return m_OnlyFinishWhenVisible;
            }
            set
            {
                m_OnlyFinishWhenVisible = value;
                UpdateCounter();
            }
        }

        public bool ResetAfterFinished
        {
            get
            {
                return m_ResetAfterFinished;
            }
            set
            {
                m_ResetAfterFinished = value;
                UpdateCounter();
            }
        }

        public bool CountInstantly
        {
            get
            {
                return m_CountInstantly;
            }
            set
            {
                m_CountInstantly = value;
                UpdateCounter();
            }
        }

        public bool Countdown
        {
            get
            {
                return m_Countdown;
            }
            set
            {
                m_Countdown = value;
                UpdateCounter();
            }
        }

        public bool WholeNumbers
        {
            get
            {
                return m_WholeNumbers;
            }
            set
            {
                m_WholeNumbers = value;
                UpdateCounter();
            }
        }

        public bool HasMaxValue
        {
            get
            {
                return m_HasMaxValue;
            }
            set
            {
                m_HasMaxValue = value;
                UpdateCounter();
            }
        }

        public Text InfoText
        {
            get
            {
                return m_InfoText;
            }
            set
            {
                m_InfoText = value;
                UpdateCounter();
            }
        }

        public Text ValueText
        {
            get
            {
                return m_ValueText;
            }
            set
            {
                m_ValueText = value;
                UpdateCounter();
            }
        }

        public ScrollRect ValueScroll
        {
            get
            {
                return m_ValueScroll;
            }
            set
            {
                m_ValueScroll = value;
            }
        }

        public List<Text> ScrollingValueTexts
        {
            get
            {
                return m_ScrollingValueTexts;
            }
            set
            {
                m_ScrollingValueTexts = value;
                UpdateCounter();
            }
        }

        public Text MinValueText
        {
            get
            {
                return m_MinValueText;
            }
            set
            {
                m_MinValueText = value;
                UpdateCounter();
            }
        }

        public Text MaxValueText
        {
            get
            {
                return m_MaxValueText;
            }
            set
            {
                m_MaxValueText = value;
                UpdateCounter();
            }
        }

        public Slider ValueSlider
        {
            get
            {
                return m_ValueSlider;
            }
            set
            {
                m_ValueSlider = value;
                UpdateCounter();
            }
        }

        public string ValueFormat
        {
            get
            {
                return m_ValueFormat;
            }
            set
            {
                m_ValueFormat = value;
                UpdateCounter();
            }
        }

        public string MinValueFormat
        {
            get
            {
                return m_MinValueFormat;
            }
            set
            {
                m_MinValueFormat = value;
                UpdateCounter();
            }
        }

        public string MaxValueFormat
        {
            get
            {
                return m_MaxValueFormat;
            }
            set
            {
                m_MaxValueFormat = value;
                UpdateCounter();
            }
        }

        public float MinValue
        {
            get
            {
                return m_MinValue;
            }
            set
            {
                m_MinValue = value;
                UpdateCounter();
            }
        }

        public float MaxValue
        {
            get
            {
                return m_MaxValue;
            }
            set
            {
                m_MaxValue = value;
                UpdateCounter();
            }
        }

        public float Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                SetCounterValue(value);
            }
        }

        public CounterWarning[] Warnings
        {
            get
            {
                return m_Warnings;
            }
            set
            {
                m_Warnings = value;
                UpdateCounter();
            }
        }

        public Graphic[] ColoredWarningGraphics
        {
            get
            {
                return m_ColoredWarningGraphics;
            }
            set
            {
                m_ColoredWarningGraphics = value;
                UpdateCounter();
            }
        }

        public UnityEvent OnSet
        {
            get
            {
                return m_OnSet;
            }
        }

        public UnityEvent OnNextSet
        {
            get
            {
                return m_OnNextSet;
            }
        }

        public AudioSource AudioSource
        {
            get
            {
                if (m_audioSource == null)
                {
                    m_audioSource = gameObject.GetRequiredComponent<AudioSource>();
                    m_audioSource.playOnAwake = false;
                }
                return m_audioSource;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);

            m_transitionStartDisplayedValue = GetDisplayedValue();

            if (m_SetupOnEnable)
            {
                UpdateCounter();
            }
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            if (m_localizedCounterFormatDictionary != null)
            {
                m_ValueFormat = m_localizedCounterFormatDictionary[LocalizationManager.Instance.ActiveLanguage];
            }

            UpdateCounter();
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            UpdateCounter();
        }

        protected override List<string> GetDefaultTransitionStates()
        {
            List<string> states = base.GetDefaultTransitionStates();
            states.AddRange(Enum.GetNames(typeof(CounterTransitionStateType)));
            return states;
        }

        protected override SimpleTransition CreateDefaultSimpleTransition(string state)
        {
            if (state == nameof(CounterTransitionStateType.CountDown) || state == nameof(CounterTransitionStateType.CountUp))
            {
                return UITransition.CreateTransition<SimpleCountTransition>(state);
            }
            return base.CreateDefaultSimpleTransition(state);
        }

        protected override SimpleTransition SetupDefaultSimpleTransition(string state, SimpleTransition transition)
        {
            SimpleCountTransition simpleCountTransition = transition as SimpleCountTransition;
            if (simpleCountTransition != null)
            {
                simpleCountTransition.SetTextCountTarget(m_ValueText);
                simpleCountTransition.SetSliderCountTarget(m_ValueSlider);
                simpleCountTransition.SetScrollCountTarget(m_ValueScroll);
                simpleCountTransition.TextCountTransition.SetValueStringFormatter(GetFormattedValueString);
                return simpleCountTransition;
            }
            return base.SetupDefaultSimpleTransition(state, transition);
        }

        protected void DoTextCountDownTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountDown));
            if (countTransition != null)
            {
                SimpleTextCountTransition textCountTranstition = countTransition.TextCountTransition;
                if (textCountTranstition != null)
                {
                    textCountTranstition.Play();
                }
            }
        }

        protected void DoTextCountUpTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountUp));
            if (countTransition != null)
            {
                SimpleTextCountTransition componentTransition = countTransition.TextCountTransition;
                if (componentTransition != null)
                {
                    componentTransition.Play();
                }
            }
        }

        protected void DoSliderCountDownTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountDown));
            if (countTransition != null)
            {
                SimpleSliderCountTransition componentTransition = countTransition.SliderCountTransition;
                if (componentTransition != null)
                {
                    componentTransition.Play();
                }
            }
        }

        protected void DoSliderCountUpTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountUp));
            if (countTransition != null)
            {
                SimpleSliderCountTransition componentTransition = countTransition.SliderCountTransition;
                if (componentTransition != null)
                {
                    componentTransition.Play();
                }
            }
        }

        protected void DoScrollCountDownTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountDown));
            if (countTransition != null)
            {
                SimpleScrollCountTransition componentTransition = countTransition.ScrollCountTransition;
                if (componentTransition != null)
                {
                    if (m_ValueScroll != null)
                    {
                        SetupScrollingValueTexts(m_MinValue, m_MaxValue);
                    }

                    componentTransition.Play();
                }
            }
        }

        protected void DoScrollCountUpTransition()
        {
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(nameof(CounterTransitionStateType.CountUp));
            if (countTransition != null)
            {
                SimpleScrollCountTransition componentTransition = countTransition.ScrollCountTransition;
                if (componentTransition != null)
                {
                    if (m_ValueScroll != null)
                    {
                        SetupScrollingValueTexts(m_MinValue, m_MaxValue);
                    }

                    componentTransition.Play();
                }
            }
        }

        protected void SetValueTextToTransitionStartValue()
        {
            SetTextToValue(m_ValueText, m_transitionStartDisplayedValue);

            if (m_ValueScroll == null)
            {
                TeardownScrollingValueTexts();
            }
        }

        protected void SetValueTextToTransitionEndValue()
        {
            SetTextToValue(m_ValueText, m_transitionEndDisplayedValue);

            if (m_ValueScroll == null)
            {
                TeardownScrollingValueTexts();
            }
        }

        protected void SetValueSliderToTransitionStartValue()
        {
            SetSliderToValue(m_ValueSlider, m_transitionStartDisplayedValue);
        }

        protected void SetValueSliderToTransitionEndValue()
        {
            SetSliderToValue(m_ValueSlider, m_transitionEndDisplayedValue);
        }

        protected void SetValueScrollToTransitionStartValue()
        {
            if (m_ValueScroll != null)
            {
                SetupScrollingValueTexts(m_transitionStartDisplayedValue, m_transitionMaxDisplayedValue);
            }

            SetScrollToValue(m_ValueScroll, m_transitionStartDisplayedValue, m_transitionMaxDisplayedValue);
        }

        protected void SetValueScrollToTransitionEndValue()
        {
            if (m_ValueScroll != null)
            {
                SetupScrollingValueTexts(m_transitionEndDisplayedValue, m_transitionMaxDisplayedValue);
            }

            SetScrollToValue(m_ValueScroll, m_transitionEndDisplayedValue, m_transitionMaxDisplayedValue);
        }

        protected void SetTextToValue(Text text, float value)
        {
            string valueTextString = GetFormattedValueString(value, m_ValueFormat);

            if (text != null)
            {
                text.text = valueTextString;
            }
        }

        protected void SetSliderToValue(Slider slider, float value)
        {
            if (slider != null)
            {
                slider.value = value;
            }
        }

        protected void SetScrollToValue(ScrollRect scrollRect, float value, float maxValue)
        {
            if (scrollRect != null)
            {
                float normalizedValue = value / maxValue;
                scrollRect.normalizedPosition = Vector2.one - new Vector2(normalizedValue, normalizedValue);
            }
        }

        protected void SetupScrollingValueTexts(float startDisplayedValue, float endDisplayedValue)
        {
            Text valueTextTemplate = m_ValueText;

            if (m_ValueText != null)
            {
                if (!m_ScrollingValueTexts.Contains(m_ValueText))
                {
                    m_ValueText.gameObject.SetActive(false);
                }
            }

            int textIndex = 0;
            for (int displayedValue = (int)startDisplayedValue; displayedValue < endDisplayedValue + 1; displayedValue++)
            {
                Text valueText = null;

                if (textIndex < m_ScrollingValueTexts.Count)
                {
                    valueText = m_ScrollingValueTexts[textIndex];
                }

                if (valueText == null && valueTextTemplate != null)
                {
                    valueText = InstantiateAndRegister(valueTextTemplate);
                    valueText.transform.SetParent(valueTextTemplate.transform.parent, false);
                    valueText.gameObject.name = "ScrollingValueText " + (textIndex + 1);
                }

                if (valueText != null)
                {
                    valueText.text = GetFormattedValueString(displayedValue, m_ValueFormat);
                    if (textIndex < m_ScrollingValueTexts.Count)
                    {
                        m_ScrollingValueTexts[textIndex] = valueText;
                    }
                    else
                    {
                        m_ScrollingValueTexts.Add(valueText);
                    }
                    valueText.gameObject.SetActive(true);
                }
                textIndex++;
            }

            int numberOfValueTextsNeeded = (int)(endDisplayedValue - startDisplayedValue) + 1;

            for (int i = 0; i < m_ScrollingValueTexts.Count; i++)
            {
                if (i >= numberOfValueTextsNeeded)
                {
                    if (m_ScrollingValueTexts[i] != null)
                    {
                        m_ScrollingValueTexts[i].gameObject.SetActive(false);
                    }
                }
            }
        }

        protected void TeardownScrollingValueTexts()
        {
            if (m_ValueText != null)
            {
                m_ValueText.gameObject.SetActive(true);
            }

            for (int i = 0; i < m_ScrollingValueTexts.Count; i++)
            {
                if (m_ScrollingValueTexts[i] != null)
                {
                    m_ScrollingValueTexts[i].gameObject.SetActive(false);
                }
            }
        }

        public void IncrementCounter(float amount)
        {
            SetCounterValue(m_Value + amount);
        }

        public void DecrementCounter(float amount)
        {
            SetCounterValue(m_Value - amount);
        }

        protected virtual void UpdateCounter()
        {
            if (m_Value < m_MinValue)
            {
                m_Value = m_MinValue;
            }

            if (CounterHasMaxValue() && m_Value > m_MaxValue)
            {
                m_Value = m_MaxValue;
            }

            if (m_WholeNumbers)
            {
                m_Value = (float)Math.Round(m_Value);
                m_MinValue = (float)Math.Round(m_MinValue);
                m_MaxValue = (float)Math.Round(m_MaxValue);
            }

            float displayedValue = GetDisplayedValue(m_Value);
            float minDisplayedValue = GetDisplayedValue(m_MinValue);
            float maxDisplayedValue = GetDisplayedValue(m_MaxValue);
            string valueTextString = GetFormattedValueString(displayedValue, m_ValueFormat);
            float valueSliderFloat = displayedValue;

            if (m_ValueSlider != null)
            {
                if (CounterHasMaxValue())
                {
                    m_ValueSlider.maxValue = m_MaxValue;
                }
            }

            if (m_CountInstantly || m_neverUpdated || !Application.isPlaying)
            {
                if (m_ValueText != null)
                {
                    if (m_ValueScroll == null)
                    {
                        TeardownScrollingValueTexts();
                    }

                    m_ValueText.text = valueTextString;
                }
                if (m_ValueSlider != null)
                {
                    m_ValueSlider.value = valueSliderFloat;
                }
            }
            else
            {
                float startDisplayedValue = m_transitionEndDisplayedValue;
                TransitionState activeCountTransitionState = null;
                TransitionStates transitionStates = GetTransitionStates();
                if (transitionStates != null)
                {
                    activeCountTransitionState = transitionStates.GetActiveTransitionStates().FirstOrDefault(x => x.Key == nameof(CounterTransitionStateType.CountUp) || x.Key == nameof(CounterTransitionStateType.CountDown));
                }
                string countTransitionState = null;
                if (startDisplayedValue > displayedValue)
                {
                    countTransitionState = nameof(CounterTransitionStateType.CountDown);
                }
                else if (startDisplayedValue < displayedValue)
                {
                    countTransitionState = nameof(CounterTransitionStateType.CountUp);
                }

                if (countTransitionState != null)
                {
                    UnityAction doCountTransition = () => DoCounterTransition(countTransitionState, startDisplayedValue, displayedValue, minDisplayedValue, maxDisplayedValue);

                    if (activeCountTransitionState != null)
                    {
                        activeCountTransitionState.Transition.SetOnComplete(doCountTransition);
                    }
                    else
                    {
                        doCountTransition?.Invoke();
                    }
                }
            }

            if (m_MinValueText != null)
            {
                m_MinValueText.text = GetFormattedValueString(displayedValue, m_MinValueFormat);
            }

            if (m_MaxValueText != null)
            {
                m_MaxValueText.text = GetFormattedValueString(displayedValue, m_MaxValueFormat);
            }

            for (int i = 0; i < m_Warnings.Length; i++)
            {
                CounterWarning counterWarning = m_Warnings[i];
                if (!counterWarning.ValueWasReached &&
                    ((m_Countdown && displayedValue <= counterWarning.m_WarningTriggerValue) || (!m_Countdown && displayedValue >= counterWarning.m_WarningTriggerValue)))
                {
                    counterWarning.ValueWasReached = true;
                    if (m_InfoText != null)
                    {
                        m_InfoText.gameObject.SetActive(true);
                        m_InfoText.RegisterLocalizableText(this.SafeGetLocalizedDictionary(x => x.m_Warnings[i].m_WarningMessage));
                        m_InfoText.text = m_Warnings[i].m_WarningMessage;
                    }
                    foreach (Graphic graphic in m_ColoredWarningGraphics)
                    {
                        graphic.CrossFadeColor(counterWarning.m_WarningColor, 0.1f, false, false);
                    }
                    if (isActiveAndEnabled)
                    {
                        if (counterWarning.m_WarningSound != null)
                        {
                            AudioSource.clip = counterWarning.m_WarningSound;
                            AudioSource.loop = counterWarning.m_LoopWarningSound;
                            AudioSource.Play();
                        }
                    }
                    AudioSource.pitch = counterWarning.m_WarningSoundPitch;
                }
            }

            if (CounterHasMaxValue() && m_Value >= m_MaxValue)
            {
                if (!m_OnlyFinishWhenVisible || IsDoingShowTransitionOrFullyShown)
                {
                    OnCounterFinished();
                }
            }

            m_neverUpdated = false;
        }

        private void DoCounterTransition(string countTransitionState, float startDisplayedValue, float endDisplayedValue, float minDisplayedValue, float maxDisplayedValue)
        {
            if (CounterHasMaxValue())
            {
                if (m_ValueScroll != null)
                {
                    SetupScrollingValueTexts(minDisplayedValue, maxDisplayedValue);
                }
            }
            else
            {
                if (m_ValueScroll == null)
                {
                    TeardownScrollingValueTexts();
                }
            }

            m_transitionStartDisplayedValue = startDisplayedValue;
            m_transitionEndDisplayedValue = endDisplayedValue;
            m_transitionMaxDisplayedValue = maxDisplayedValue;
            SimpleCountTransition countTransition = m_SimpleTransitionStates.GetTransition<SimpleCountTransition>(countTransitionState);
            if (countTransition != null)
            {
                countTransition.SetStartValue(startDisplayedValue);
                countTransition.SetEndValue(endDisplayedValue);
                countTransition.SetMaxValue(maxDisplayedValue);
            }
            UITransition transition = DoTransition(countTransitionState, false);
            if (transition != null)
            {
                transition.AddOnComplete(SetValueTextToTransitionEndValue);
            }
            else
            {
                SetValueTextToTransitionEndValue();
            }
        }

        public bool CounterHasMaxValue()
        {
            return m_Countdown || m_HasMaxValue;
        }

        public float GetValue()
        {
            return m_Value;
        }

        public float GetDisplayedValue()
        {
            if (m_Countdown)
            {
                return m_MaxValue - m_Value;
            }

            return m_Value;
        }

        public float GetDisplayedValue(float value)
        {
            if (m_Countdown)
            {
                return m_MaxValue - value;
            }

            return value;
        }

        public virtual string GetFormattedValueString(float value)
        {
            return GetFormattedValueString(value, m_ValueFormat);
        }

        public virtual string GetFormattedValueString(float value, string valueFormat)
        {
            object arg0 = value;
            object arg1 = m_MaxValue;
            object arg2 = m_MinValue;

            if (m_WholeNumbers)
            {
                arg0 = (int)Math.Round((float)arg0);
                arg1 = (int)Math.Round((float)arg1);
                arg2 = (int)Math.Round((float)arg2);
            }

            return Smart.Format(valueFormat, arg0, arg1, arg2);
        }

        public void HideAndClearCounter(UnityAction onComplete = null)
        {
            Hide(
                () =>
                {
                    ClearCounter();
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
        }

        public virtual void ClearCounter()
        {
            ResetCounter();
            SetCounterInfoText("");
            ClearCounterWarnings();
        }

        public virtual void RaiseCounter(float targetValue, bool countdown, UnityAction onShowComplete = null, UnityAction onCounterComplete = null)
        {
            SetupRaisedCounterUI(targetValue, countdown, onShowComplete, onCounterComplete);
        }

        protected virtual void SetupRaisedCounterUI(float targetValue, bool countdown, UnityAction onShowComplete = null, UnityAction onCounterComplete = null)
        {
            m_MaxValue = targetValue;
            m_Countdown = countdown;

            Show(onShowComplete);
            ResetCounter();

            if (onCounterComplete != null)
            {
                OnNextFinished.AddPersistentListener(onCounterComplete);
                OnNextFinished.AddPersistentListener(ResetCounter);
            }
        }

        public virtual void ResetCounter()
        {
            m_needsReset = false;

            SetCounterValue(m_Value);

            foreach (CounterWarning valuerWarning in m_Warnings)
            {
                valuerWarning.ResetTracking();
            }
        }

        public void SetCounterValueToMax()
        {
            SetCounterValue(m_MaxValue);
        }

        public virtual void SetCounterFormat(Dictionary<string, string> localizedDictionary, string text)
        {
            SetCounterFormat(text);

            m_localizedCounterFormatDictionary = localizedDictionary;
        }

        public void SetCounterFormat(string valueFormat)
        {
            LocalizationManager.Instance.RegisterLocalizable(this);

            this.m_ValueFormat = valueFormat;

            UpdateCounter();
        }

        public void SetCounterValue(float value)
        {
            m_Value = value;

            UpdateCounter();

            InvokeCounterSetEvent();
        }

        public void SetCounterHasMax(bool hasMaxValue)
        {
            m_HasMaxValue = hasMaxValue;
            UpdateCounter();
        }

        public void SetCounterMin(float minValue)
        {
            m_MinValue = minValue;
            UpdateCounter();
        }

        public void SetCounterMax(float maxValue)
        {
            m_MaxValue = maxValue;
            UpdateCounter();
        }

        public void SetCounterWholeNumbers(bool wholeNumbers)
        {
            m_WholeNumbers = wholeNumbers;
            UpdateCounter();
        }

        public void SetCounterDirection(bool countdown)
        {
            m_Countdown = countdown;
            UpdateCounter();
        }

        public void SetCounterInfoText(string text)
        {
            if (m_InfoText != null)
            {
                if (!string.IsNullOrEmpty(text))
                {
                    m_InfoText.text = text;
                    m_InfoText.gameObject.SetActive(true);
                }
                else
                {
                    m_InfoText.gameObject.SetActive(false);
                }
            }
        }

        public void SetCounterInfoText(Dictionary<string, string> localizedDictionary, string text)
        {
            if (m_InfoText != null)
            {
                m_InfoText.RegisterLocalizableText(localizedDictionary);
                SetCounterInfoText(text);
            }
        }

        public void ClearCounterWarnings()
        {
            m_Warnings = new CounterWarning[0];
            UpdateCounter();
        }

        public void SetCounterWarnings(CounterWarning[] newCounterWarnings)
        {
            m_Warnings = newCounterWarnings;
            UpdateCounter();
        }

        protected virtual void OnCounterFinished()
        {
            UnityAction onComplete = () =>
            {
                if (m_ResetAfterFinished)
                {
                    ResetCounter();
                }
            };

            Finish(onComplete);
        }

        protected IEnumerator DelayedCallback(float delay, UnityAction onComplete)
        {
            yield return new WaitForSeconds(delay);

            onComplete();
        }

        private void InvokeCounterSetEvent()
        {
            OnSet.Invoke();
            OnNextSet.Invoke();
            OnNextSet.RemoveAllPersistentAndNonPersistentListeners();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_HasMaxValue))
            {
                if (m_Countdown)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxValueText))
            {
                if (!CounterHasMaxValue())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxValueFormat))
            {
                if (!CounterHasMaxValue())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxValue))
            {
                if (!CounterHasMaxValue())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ValueSlider))
            {
                if (!CounterHasMaxValue())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ColoredWarningGraphics))
            {
                if (m_Warnings == null || m_Warnings.Length == 0)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ValueFormat))
            {
                if (m_ValueText == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MinValueFormat))
            {
                if (m_MinValueText == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MaxValueFormat))
            {
                if (m_MaxValueText == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_HideAfterFinishedDelay))
            {
                if (!m_HideAfterFinished)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return null;
        }

        public virtual void OnLocalizeComplete()
        {
            if (m_localizedCounterFormatDictionary != null && m_localizedCounterFormatDictionary.ContainsKey(LocalizationManager.Instance.ActiveLanguage))
            {
                string localizedText = m_localizedCounterFormatDictionary[LocalizationManager.Instance.ActiveLanguage];
                m_ValueFormat = localizedText;
            }
        }
    }

}
