﻿using ImprobableStudios.BaseUtilities;
using System;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.VectorUtilities;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Pie Chart Slice")]
    public class PieChartSlice : PersistentBehaviour, ICanvasRaycastFilter
    {
        [Tooltip("The image used to display the slice's circle sprite")]
        [SerializeField]
        protected Image m_Image;

        [Tooltip("The overlay panel which will be centered in the middle of the slice")]
        [SerializeField]
        protected RectTransform m_OverlayRoot;

        [Tooltip("The text used to display the slice label")]
        [SerializeField]
        protected Text m_LabelText;

        [Tooltip("The text used to display the slice value")]
        [SerializeField]
        protected Text m_ValueText;

        public Image Image
        {
            get
            {
                return m_Image;
            }
            set
            {
                m_Image = value;
            }
        }

        public RectTransform OverlayRoot
        {
            get
            {
                return m_OverlayRoot;
            }
            set
            {
                m_OverlayRoot = value;
            }
        }

        public Text LabelText
        {
            get
            {
                return m_LabelText;
            }
            set
            {
                m_LabelText = value;
            }
        }

        public Text ValueText
        {
            get
            {
                return m_ValueText;
            }
            set
            {
                m_ValueText = value;
            }
        }

        public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            Image image = m_Image;
            
            Vector3 center = image.rectTransform.GetCenter();
            float fillAmount = image.fillAmount;
            float fillAngle = ((fillAmount * 360f));
            float zRotation = image.transform.eulerAngles.z;
            
            if (image.fillOrigin == (int)Image.Origin360.Top)
            {
                zRotation += 90f;
            }
            else if (image.fillOrigin == (int)Image.Origin360.Right)
            {
                zRotation += 0f;
            }
            else if (image.fillOrigin == (int)Image.Origin360.Bottom)
            {
                zRotation += 270f;
            }
            else if (image.fillOrigin == (int)Image.Origin360.Left)
            {
                zRotation += 180f;
            }

            float startAngle = 0;
            float endAngle = 0;
            if (image.fillClockwise)
            {
                startAngle = zRotation - fillAngle;
                endAngle = zRotation;
            }
            else
            {
                startAngle = zRotation;
                endAngle = zRotation + fillAngle;
            }

            float xRadius = (image.rectTransform.rect.width / 2f);
            float yRadius = (image.rectTransform.rect.height / 2f);
            
            Vector3 cursorWorldPosition;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(image.rectTransform, screenPoint, eventCamera, out cursorWorldPosition);

            if (!Math3d.IsPointInArc(cursorWorldPosition, center, xRadius, startAngle, endAngle) || !Math3d.IsPointInArc(cursorWorldPosition, center, yRadius, startAngle, endAngle))
            {
                return false;
            }
            
            return true;
        }
    }
    
    [Serializable]
    public class PieChartSliceInfo
    {
        [Tooltip("The label for this slice")]
        [SerializeField]
        protected string m_Label;

        [Tooltip("The value of this slice")]
        [Min(0)]
        [SerializeField]
        protected float m_Value;

        [Tooltip("The color of this slice")]
        [SerializeField]
        protected Color m_Color;

        [Tooltip("The sprite to use for this slice")]
        [SerializeField]
        protected Sprite m_CircleSprite;

        public string Label
        {
            get
            {
                return m_Label;
            }
            set
            {
                m_Label = value;
            }
        }

        public float Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                m_Value = value;
            }
        }

        public Color Color
        {
            get
            {
                return m_Color;
            }
            set
            {
                m_Color = value;
            }
        }

        public Sprite CircleSprite
        {
            get
            {
                return m_CircleSprite;
            }
            set
            {
                m_CircleSprite = value;
            }
        }

        public PieChartSliceInfo()
        {
            m_Label = GetDefaultLabel();
            m_Value = GetDefaultValue();
            m_Color = GetDefaultColor();
            m_CircleSprite = GetDefaultCircleSprite();
        }

        public PieChartSliceInfo(float value)
        {
            m_Label = GetDefaultLabel();
            m_Value = value;
            m_Color = GetDefaultColor();
            m_CircleSprite = GetDefaultCircleSprite();
        }

        public PieChartSliceInfo(string label)
        {
            m_Label = label;
            m_Value = GetDefaultValue();
            m_Color = GetDefaultColor();
            m_CircleSprite = GetDefaultCircleSprite();
        }

        public PieChartSliceInfo(string label, float value)
        {
            m_Label = label;
            m_Value = value;
            m_Color = GetDefaultColor();
            m_CircleSprite = GetDefaultCircleSprite();
        }

        public PieChartSliceInfo(string label, float value, Color color)
        {
            m_Label = label;
            m_Value = value;
            m_Color = color;
            m_CircleSprite = GetDefaultCircleSprite();
        }

        public PieChartSliceInfo(string label, float value, Color color, Sprite circleSprite)
        {
            m_Label = label;
            m_Value = value;
            m_Color = color;
            m_CircleSprite = circleSprite;
        }

        protected virtual string GetDefaultLabel()
        {
            return "";
        }

        protected virtual float GetDefaultValue()
        {
            return 1f;
        }

        protected virtual Color GetDefaultColor()
        {
            System.Random random = new System.Random();
            return new Color((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble(), 1);
        }

        protected virtual Sprite GetDefaultCircleSprite()
        {
            return null;
        }
    }

}