﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.InputUtilities;
using System;

namespace ImprobableStudios.UIUtilities
{

    public abstract class PlaybackController : UIConstruct
    {
        #region Serialized Fields
        [Tooltip("Determines whether playback will start automatically on awake")]
        [SerializeField]
        protected bool m_PlayOnAwake = true;

        [Tooltip("Determines whether the play/pause button will be selected by default")]
        [SerializeField]
        protected bool m_SelectPlayPauseButtonByDefault = true;

        [Tooltip("Determines whether the playback can controlled by user input")]
        [SerializeField]
        protected bool m_AllowPlaybackControl = true;

        [Tooltip("Determines whether the playback can be skipped")]
        [SerializeField]
        protected bool m_AllowSkipping = true;

        [Tooltip("Determines whether the playback can be scrubbed")]
        [SerializeField]
        protected bool m_AllowScrubbing = true;

        [Tooltip("Determines whether the replay button is only accessible after playback has finished")]
        [SerializeField]
        protected bool m_AllowReplayingOnlyAfterFinished = false;

        [Tooltip("The panel which contains controls the user can use to control playback")]
        [SerializeField]
        protected PlaybackControlPanel m_PlaybackControlPanel;

        [Tooltip("The amount of time in seconds the skip forward and skip back buttons will skip")]
        [Min(0)]
        [SerializeField]
        protected float m_StepAmount = 10f;

        [Tooltip("The format used to display the current playback time and playback length")]
        [SerializeField]
        protected string m_ProgressTimeFormat = "{0:mm:ss}";

        [SerializeField]
        protected float m_PlaybackControlPanelVisibleDuration = 3f;

        [Tooltip("The audio source that will be used by default to playback the audio.")]
        [SerializeField]
        protected AudioSource m_AudioSource;
        #endregion

        #region Protected Fields
        protected UnityEvent m_OnNextPrepared = new UnityEvent();

        protected UnityEvent m_OnPlayed = new UnityEvent();

        protected UnityEvent m_OnReset = new UnityEvent();

        protected UnityEvent m_OnReplayed = new UnityEvent();

        protected UnityEvent m_OnStarted = new UnityEvent();

        protected UnityEvent m_OnPaused = new UnityEvent();

        protected UnityEvent m_OnResumed = new UnityEvent();

        protected UnityEvent m_OnSkipped = new UnityEvent();

        protected UnityEvent m_OnStopped = new UnityEvent();
        #endregion

        #region Private Fields
        private bool m_started;

        private bool m_finished;

        private bool m_skipped;

        private bool m_wasPlayingWhenScrubbed;

        private bool m_isSeeking;

        private float m_hideControlsTime;
        #endregion

        #region Public Properties
        public bool PlayOnAwake
        {
            get
            {
                return m_PlayOnAwake;
            }
            set
            {
                m_PlayOnAwake = value;
            }
        }

        public bool AllowPlaybackControl
        {
            get
            {
                return m_AllowPlaybackControl;
            }
            set
            {
                m_AllowPlaybackControl = value;
            }
        }

        public bool AllowSkipping
        {
            get
            {
                return m_AllowPlaybackControl && m_AllowSkipping;
            }
            set
            {
                m_AllowSkipping = value;
            }
        }

        public bool AllowScrubbing
        {
            get
            {
                return m_AllowPlaybackControl && m_AllowScrubbing;
            }
            set
            {
                m_AllowScrubbing = value;
            }
        }

        public bool AllowReplayingOnlyAfterFinished
        {
            get
            {
                return m_AllowPlaybackControl && m_AllowReplayingOnlyAfterFinished;
            }
            set
            {
                m_AllowReplayingOnlyAfterFinished = value;
            }
        }

        public PlaybackControlPanel PlaybackControlPanel
        {
            get
            {
                return m_PlaybackControlPanel;
            }
            set
            {
                m_PlaybackControlPanel = value;
                Refresh();
            }
        }

        public float StepAmount
        {
            get
            {
                return m_StepAmount;
            }
            set
            {
                m_StepAmount = value;
                Refresh();
            }
        }

        public string ProgressTimeFormat
        {
            get
            {
                return m_ProgressTimeFormat;
            }
            set
            {
                m_ProgressTimeFormat = value;
                Refresh();
            }
        }

        public AudioSource AudioSource
        {
            get
            {
                return m_AudioSource;
            }
            set
            {
                m_AudioSource = value;
                Refresh();
            }
        }

        public UnityEvent OnNextPrepared
        {
            get
            {
                return m_OnNextPrepared;
            }
        }

        public UnityEvent OnPlayed
        {
            get
            {
                return m_OnPlayed;
            }
        }

        public UnityEvent OnReset
        {
            get
            {
                return m_OnReset;
            }
        }

        public UnityEvent OnReplayed
        {
            get
            {
                return m_OnReplayed;
            }
        }

        public UnityEvent OnStarted
        {
            get
            {
                return m_OnStarted;
            }
        }

        public UnityEvent OnPaused
        {
            get
            {
                return m_OnPaused;
            }
        }

        public UnityEvent OnResumed
        {
            get
            {
                return m_OnResumed;
            }
        }

        public UnityEvent OnSkipped
        {
            get
            {
                return m_OnSkipped;
            }
        }

        public UnityEvent OnStopped
        {
            get
            {
                return m_OnStopped;
            }
        }

        public bool IsFinished
        {
            get
            {
                return m_finished;
            }
        }

        public bool WasStarted
        {
            get
            {
                return m_started;
            }
        }

        public bool WasSkipped
        {
            get
            {
                return m_skipped;
            }
        }

        public bool AreControlsHidden
        {
            get
            {
                if (m_PlaybackControlPanelVisibleDuration >= 0)
                {
                    return m_hideControlsTime <= 0;
                }
                return false;
            }
        }
        #endregion

        #region MonoBehaviour Methods
        protected override void Awake()
        {
            base.Awake();

            DoSetupTargets();

            if (CanPlay())
            {
                if (m_PlayOnAwake)
                {
                    PlayAfterPrepared(true);
                }
                else
                {
                    InitializePlayback();
                }
            }

        }

        protected override void OnEnable()
        {
            base.OnEnable();

            m_hideControlsTime = m_PlaybackControlPanelVisibleDuration;

            if (EventSystem.current != null)
            {
                if (m_SelectPlayPauseButtonByDefault)
                {
                    SelectPlayPauseButton();
                }
            }

            AddListeners();

            DoClear();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            RemoveListeners();
        }

        protected override void Update()
        {
            base.Update();

            if (m_PlaybackControlPanel != null)
            {
                if (Input.anyKey)
                {
                    ShowControlPanel();
                }
                else
                {
                    if (IsPlaying())
                    {
                        m_hideControlsTime -= Time.deltaTime;
                        if (m_hideControlsTime <= 0 && !ControlPanelIsDoingHideTransitionOrFullyHidden())
                        {
                            if (m_PlaybackControlPanelVisibleDuration >= 0)
                            {
                                HideControlPanel();
                            }
                        }
                    }
                }

                if (m_PlaybackControlPanel.PlayButton != null)
                {
                    if (m_AllowPlaybackControl && IsPrepared() && !IsPlaying())
                    {
                        bool wasNotActive = !m_PlaybackControlPanel.PlayButton.gameObject.activeSelf;
                        m_PlaybackControlPanel.PlayButton.gameObject.SetActive(true);
                        if (EventSystem.current != null)
                        {
                            if (m_SelectPlayPauseButtonByDefault && EventSystem.current.currentSelectedGameObject == m_PlaybackControlPanel.PauseButton.gameObject)
                            {
                                if (wasNotActive)
                                {
                                    EventSystem.current.SetSelectedGameObject(m_PlaybackControlPanel.PlayButton.gameObject);
                                }
                            }
                        }
                    }
                    else
                    {
                        m_PlaybackControlPanel.PlayButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.PauseButton != null)
                {
                    if (IsPrepared() && IsPlaying())
                    {
                        bool wasNotActive = !m_PlaybackControlPanel.PauseButton.gameObject.activeSelf;
                        m_PlaybackControlPanel.PauseButton.gameObject.SetActive(true);
                        if (EventSystem.current != null)
                        {
                            if (m_SelectPlayPauseButtonByDefault && EventSystem.current.currentSelectedGameObject == m_PlaybackControlPanel.PlayButton.gameObject)
                            {
                                if (wasNotActive)
                                {
                                    EventSystem.current.SetSelectedGameObject(m_PlaybackControlPanel.PauseButton.gameObject);
                                }
                            }
                        }
                    }
                    else
                    {
                        m_PlaybackControlPanel.PauseButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.StepForwardButton != null)
                {
                    if (m_AllowPlaybackControl && m_AllowSkipping)
                    {
                        m_PlaybackControlPanel.StepForwardButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        m_PlaybackControlPanel.StepForwardButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.StepBackwardButton != null)
                {
                    if (m_AllowPlaybackControl && m_AllowSkipping)
                    {
                        m_PlaybackControlPanel.StepBackwardButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        m_PlaybackControlPanel.StepBackwardButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.ReplayButton != null)
                {
                    if (m_AllowPlaybackControl && IsPrepared() && (!m_AllowReplayingOnlyAfterFinished || m_finished))
                    {
                        m_PlaybackControlPanel.ReplayButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        m_PlaybackControlPanel.ReplayButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.SkipButton != null)
                {
                    if (m_AllowPlaybackControl && IsPrepared())
                    {
                        m_PlaybackControlPanel.SkipButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        m_PlaybackControlPanel.SkipButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    if (m_AllowPlaybackControl && IsPrepared())
                    {
                        m_PlaybackControlPanel.StopButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        m_PlaybackControlPanel.StopButton.gameObject.SetActive(false);
                    }
                }
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    RefreshProgressBar();
                }
            }

            UpdateProgressBar();
            UpdateVolumeBar();
            UpdateProgressText();
            UpdateMuteToggle();
        }
        #endregion

        #region Public Methods
        public float GetVolume()
        {
            return DoGetVolume();
        }

        public void SetVolume(float volume)
        {
            DoSetVolume(volume);
        }

        public bool GetMuteState()
        {
            return DoGetMuteState();
        }

        public void SetMuteState(bool mute)
        {
            DoSetMuteState(mute);
        }

        public float GetMaxPlaybackTime()
        {
            return DoGetMaxPlaybackTime();
        }

        public float GetCurrentPlaybackTime()
        {
            return DoGetCurrentPlaybackTime();
        }

        public void SetCurrentPlaybackTime(float time)
        {
            time = Math.Max(0, time);
            time = Math.Min(GetMaxPlaybackTime(), time);

            DoSetCurrentPlaybackTime(time);
        }

        public void SetPlayOnAwake(bool playOnAwake)
        {
            DoSetPlayOnAwake(playOnAwake);
        }

        public void Clear()
        {
            DoClear();
        }

        public void ResetPlayback()
        {
            SetCurrentPlaybackTime(0);

            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.gameObject.SetActive(false);
                }
            }

            DoPausePlayback();

            ShowControlPanel();

            m_OnReset.Invoke();
        }

        public void ReplayPlayback()
        {
            m_started = true;
            m_finished = false;
            m_skipped = false;

            SetCurrentPlaybackTime(0);
            DoStartPlayback();

            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.gameObject.SetActive(true);
                }
            }

            m_OnReplayed.Invoke();
        }

        public void StartPlayback()
        {
            m_started = true;
            m_finished = false;
            m_skipped = false;

            PlayAfterPrepared(true);

            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.gameObject.SetActive(true);
                }
            }

            m_OnStarted.Invoke();
        }

        public void PausePlayback()
        {
            DoPausePlayback();

            ShowControlPanel();

            m_OnPaused.Invoke();
        }

        public void ResumePlayback()
        {
            if (!m_started)
            {
                StartPlayback();
                return;
            }

            if (IsPrepared())
            {
                DoResumePlayback();
            }
            else
            {
                PlayAfterPrepared(false);
            }

            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.gameObject.SetActive(true);
                }
            }

            m_OnResumed.Invoke();
        }

        public void StepForwardPlayback()
        {
            float value = GetCurrentPlaybackTime() + m_StepAmount;

            SetCurrentPlaybackTime(value);

            if (value > 0)
            {
                m_started = true;
            }

            if (m_wasPlayingWhenScrubbed)
            {
                ResumePlayback();
            }
            else
            {
                SetProgressBarTime(value);
            }
        }

        public void StepBackwardPlayback()
        {
            float value = GetCurrentPlaybackTime() - m_StepAmount;

            SetCurrentPlaybackTime(value);

            if (value > 0)
            {
                m_started = true;
            }

            if (m_wasPlayingWhenScrubbed)
            {
                ResumePlayback();
            }
            else
            {
                SetProgressBarTime((float)value);
            }
        }

        public void SkipPlayback()
        {
            m_started = false;
            m_skipped = true;

            SetCurrentPlaybackTime(GetMaxPlaybackTime());

            DoClear();

            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.gameObject.SetActive(false);
                }
            }

            m_OnSkipped.Invoke();
        }

        public void StopPlayback()
        {
            m_started = false;

            DoStopPlayback();

            DoClear();

            m_OnStopped.Invoke();
        }
        #endregion

        #region Protected Methods
        protected void Setup()
        {
            InitializePlayback();

            DoClear();

            Refresh();
        }

        protected void Play(UnityAction onComplete = null)
        {
            if (onComplete != null)
            {
                m_OnNextFinished.AddPersistentListener(onComplete);
            }

            StartPlayback();

            Refresh();
        }

        protected void Refresh()
        {
            DoSetPlayOnAwake(false);

            RefreshProgressBar();
            RefreshVolumeBar();

            if (Application.isPlaying)
            {
                DoSetupTargets();

                RemoveListeners();

                AddListeners();
            }
        }
        #endregion

        #region Listener Methods
        protected virtual void OnPlaybackPrepared(UnityEngine.Object source)
        {
            if (source == GetPlaybackSource())
            {
                m_OnNextPrepared.Invoke();
                m_OnNextPrepared.RemoveAllPersistentAndNonPersistentListeners();
            }
        }

        protected virtual void OnPlaybackStarted(UnityEngine.Object source)
        {
            if (source == GetPlaybackSource())
            {
                m_OnPlayed.Invoke();
            }
        }

        protected virtual void OnPlaybackFinished(UnityEngine.Object source)
        {
            if (source == GetPlaybackSource())
            {
                m_finished = true;
                OnPlaybackFinished();
            }
        }

        protected virtual void OnPointerDownOnProgressBar()
        {
            if (IsPlaying())
            {
                m_wasPlayingWhenScrubbed = true;
            }

            m_isSeeking = true;
            DoPausePlayback();
        }

        protected virtual void OnPointerUpOnProgressBar()
        {
            if (AllowScrubbing)
            {
                if (m_PlaybackControlPanel != null)
                {
                    if (m_PlaybackControlPanel.ProgressBar != null)
                    {
                        float value = m_PlaybackControlPanel.ProgressBar.value;

                        SetCurrentPlaybackTime(value);

                        if (m_PlaybackControlPanel.ProgressBar.value > 0)
                        {
                            m_started = true;
                        }

                        if (m_wasPlayingWhenScrubbed)
                        {
                            ResumePlayback();
                        }
                        else
                        {
                            SetProgressBarTime(value);
                        }

                        m_isSeeking = false;
                    }
                }
            }
        }

        protected virtual void OnVolumeBarValueChanged(float value)
        {
            DoSetVolume(value);
        }

        protected virtual void OnVolumeMuteToggleValueChanged(bool on)
        {
            DoSetMuteState(!on);
        }
        #endregion

        #region Abstract Methods
        protected abstract UnityEngine.Object GetPlaybackSource();

        protected abstract bool CanPlay();

        protected abstract bool IsPrepared();

        protected abstract bool IsPlaying();

        protected abstract void DoSetupTargets();

        protected abstract float DoGetVolume();

        protected abstract void DoSetVolume(float volume);

        protected abstract bool DoGetMuteState();

        protected abstract void DoSetMuteState(bool mute);

        protected abstract float DoGetMaxPlaybackTime();

        protected abstract float DoGetCurrentPlaybackTime();

        protected abstract void DoSetCurrentPlaybackTime(float time);

        protected abstract void DoSetPlayOnAwake(bool playOnAwake);

        protected abstract void DoClear();

        protected abstract void DoPreparePlayback();

        protected abstract void DoStartPlayback();

        protected abstract void DoUpdatePlayback();

        protected abstract void DoPausePlayback();

        protected abstract void DoResumePlayback();

        protected abstract void DoStopPlayback();

        protected abstract void SubscribeToOnPrepared(Action<UnityEngine.Object> action);

        protected abstract void SubscribeToOnStarted(Action<UnityEngine.Object> action);

        protected abstract void SubscribeToOnFinished(Action<UnityEngine.Object> action);

        protected abstract void UnsubscribeFromOnPrepared(Action<UnityEngine.Object> action);

        protected abstract void UnsubscribeFromOnStarted(Action<UnityEngine.Object> action);

        protected abstract void UnsubscribeFromOnFinished(Action<UnityEngine.Object> action);
        #endregion

        #region Virtual Methods
        protected virtual void AddListeners()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.PlayButton != null)
                {
                    m_PlaybackControlPanel.PlayButton.onClick.AddListener(ResumePlayback);
                }
                if (m_PlaybackControlPanel.PauseButton != null)
                {
                    m_PlaybackControlPanel.PauseButton.onClick.AddListener(PausePlayback);
                }
                if (m_PlaybackControlPanel.ReplayButton != null)
                {
                    m_PlaybackControlPanel.ReplayButton.onClick.AddListener(ReplayPlayback);
                }
                if (m_PlaybackControlPanel.StepForwardButton != null)
                {
                    m_PlaybackControlPanel.StepForwardButton.onClick.AddListener(StepForwardPlayback);
                }
                if (m_PlaybackControlPanel.StepBackwardButton != null)
                {
                    m_PlaybackControlPanel.StepBackwardButton.onClick.AddListener(StepBackwardPlayback);
                }
                if (m_PlaybackControlPanel.SkipButton != null)
                {
                    m_PlaybackControlPanel.SkipButton.onClick.AddListener(SkipPlayback);
                }
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.onClick.AddListener(StopPlayback);
                }
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    PointerDownDetector pointerDownDetector = m_PlaybackControlPanel.ProgressBar.GetRequiredComponent<PointerDownDetector>();
                    PointerUpDetector pointerUpDetector = m_PlaybackControlPanel.ProgressBar.GetRequiredComponent<PointerUpDetector>();
                    pointerDownDetector.OnInputDetected.AddListener(OnPointerDownOnProgressBar);
                    pointerUpDetector.OnInputDetected.AddListener(OnPointerUpOnProgressBar);
                    m_PlaybackControlPanel.ProgressBar.onValueChanged.AddListener(SetCurrentPlaybackTime);
                }
                if (m_PlaybackControlPanel.VolumeBar != null)
                {
                    m_PlaybackControlPanel.VolumeBar.onValueChanged.AddListener(OnVolumeBarValueChanged);
                }
                if (m_PlaybackControlPanel.VolumeMuteToggle != null)
                {
                    m_PlaybackControlPanel.VolumeMuteToggle.onValueChanged.AddListener(OnVolumeMuteToggleValueChanged);
                }
            }

            SubscribeToOnPrepared(OnPlaybackPrepared);
            SubscribeToOnStarted(OnPlaybackStarted);
            SubscribeToOnFinished(OnPlaybackFinished);
        }

        protected virtual void RemoveListeners()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.PlayButton != null)
                {
                    m_PlaybackControlPanel.PlayButton.onClick.RemoveListener(ResumePlayback);
                }
                if (m_PlaybackControlPanel.PauseButton != null)
                {
                    m_PlaybackControlPanel.PauseButton.onClick.RemoveListener(PausePlayback);
                }
                if (m_PlaybackControlPanel.ReplayButton != null)
                {
                    m_PlaybackControlPanel.ReplayButton.onClick.RemoveListener(ReplayPlayback);
                }
                if (m_PlaybackControlPanel.StepForwardButton != null)
                {
                    m_PlaybackControlPanel.StepForwardButton.onClick.RemoveListener(StepForwardPlayback);
                }
                if (m_PlaybackControlPanel.StepBackwardButton != null)
                {
                    m_PlaybackControlPanel.StepBackwardButton.onClick.RemoveListener(StepBackwardPlayback);
                }
                if (m_PlaybackControlPanel.SkipButton != null)
                {
                    m_PlaybackControlPanel.SkipButton.onClick.RemoveListener(SkipPlayback);
                }
                if (m_PlaybackControlPanel.StopButton != null)
                {
                    m_PlaybackControlPanel.StopButton.onClick.RemoveListener(StopPlayback);
                }
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    PointerDownDetector pointerDownDetector = m_PlaybackControlPanel.ProgressBar.GetRequiredComponent<PointerDownDetector>();
                    PointerUpDetector pointerUpDetector = m_PlaybackControlPanel.ProgressBar.GetRequiredComponent<PointerUpDetector>();
                    pointerDownDetector.OnInputDetected.RemoveListener(OnPointerDownOnProgressBar);
                    pointerUpDetector.OnInputDetected.RemoveListener(OnPointerUpOnProgressBar);
                    m_PlaybackControlPanel.ProgressBar.onValueChanged.RemoveListener(SetCurrentPlaybackTime);
                }
                if (m_PlaybackControlPanel.VolumeBar != null)
                {
                    m_PlaybackControlPanel.VolumeBar.onValueChanged.RemoveListener(OnVolumeBarValueChanged);
                }
                if (m_PlaybackControlPanel.VolumeMuteToggle != null)
                {
                    m_PlaybackControlPanel.VolumeMuteToggle.onValueChanged.RemoveListener(OnVolumeMuteToggleValueChanged);
                }
            }

            UnsubscribeFromOnPrepared(OnPlaybackPrepared);
            UnsubscribeFromOnStarted(OnPlaybackStarted);
            UnsubscribeFromOnFinished(OnPlaybackFinished);
        }

        protected virtual void DoShowFirstFrame()
        {
            DoStartPlayback();
            DoUpdatePlayback();
            DoPausePlayback();
        }
        #endregion

        #region Private Methods
        private void SelectPlayPauseButton()
        {
            if (PlaybackControlPanel.PlayButton != null && PlaybackControlPanel.PlayButton.gameObject.activeSelf)
            {
                EventSystem.current.SetSelectedGameObject(m_PlaybackControlPanel.PlayButton.gameObject);
            }
            if (PlaybackControlPanel.PauseButton != null && PlaybackControlPanel.PauseButton.gameObject.activeSelf)
            {
                EventSystem.current.SetSelectedGameObject(m_PlaybackControlPanel.PauseButton.gameObject);
            }
        }

        private bool ControlPanelIsDoingHideTransitionOrFullyHidden()
        {
            if (m_PlaybackControlPanel != null)
            {
                return m_PlaybackControlPanel.IsDoingHideTransitionOrFullyHidden;
            }
            return true;
        }

        private void ShowControlPanel()
        {
            if (m_PlaybackControlPanel != null)
            {
                m_hideControlsTime = m_PlaybackControlPanelVisibleDuration;
                m_PlaybackControlPanel.Show();
                if (EventSystem.current != null)
                {
                    if (m_SelectPlayPauseButtonByDefault && EventSystem.current.currentSelectedGameObject == null)
                    {
                        SelectPlayPauseButton();
                    }
                }
            }
        }

        private void HideControlPanel()
        {
            if (m_PlaybackControlPanel != null)
            {
                m_PlaybackControlPanel.Hide(ClearControlPanelSelection);
            }
        }

        private void ClearControlPanelSelection()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.HasSelection())
                {
                    EventSystem.current.SetSelectedGameObject(null);
                }
            }
        }

        private void InitializePlayback()
        {
            m_started = false;

            if (CanPlay())
            {
                PerformActionAfterPrepared(DoShowFirstFrame);
            }
        }

        private void RefreshProgressBar()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    bool canScrub = AllowScrubbing && !AreControlsHidden;
                    m_PlaybackControlPanel.ProgressBar.interactable = canScrub;

                    m_PlaybackControlPanel.ProgressBar.minValue = 0;
                    m_PlaybackControlPanel.ProgressBar.maxValue = GetMaxPlaybackTime();
                }
            }
        }

        private void RefreshVolumeBar()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.VolumeBar != null)
                {
                    m_PlaybackControlPanel.VolumeBar.minValue = 0f;
                    m_PlaybackControlPanel.VolumeBar.maxValue = 1f;
                }
            }
        }

        private void UpdateVolumeBar()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.VolumeBar != null)
                {
                    m_PlaybackControlPanel.VolumeBar.SetValueWithoutNotify(DoGetVolume());
                }
            }
        }

        private void UpdateMuteToggle()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.VolumeMuteToggle != null)
                {
                    bool mute = DoGetMuteState();
                    if (DoGetVolume() == 0)
                    {
                        mute = true;
                    }
                    m_PlaybackControlPanel.VolumeMuteToggle.SetIsOnWithoutNotify(!mute);
                }
            }
        }

        private void UpdateProgressBar()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    m_PlaybackControlPanel.ProgressBar.maxValue = GetMaxPlaybackTime();

                    if (!m_isSeeking)
                    {
                        float time  = GetCurrentPlaybackTime();
                        m_PlaybackControlPanel.ProgressBar.SetValueWithoutNotify(time);
                    }
                }
            }
        }

        private void SetProgressBarTime(float time)
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.ProgressBar != null)
                {
                    m_PlaybackControlPanel.ProgressBar.SetValueWithoutNotify((float)time);
                }
            }
        }

        private void UpdateProgressText()
        {
            if (m_PlaybackControlPanel != null)
            {
                if (m_PlaybackControlPanel.ProgressText != null)
                {
                    float current = GetCurrentPlaybackTime();
                    float max = GetMaxPlaybackTime();
                    m_PlaybackControlPanel.ProgressText.text = Timer.GetFormattedTimeString(current, m_ProgressTimeFormat) + "/" + Timer.GetFormattedTimeString(max, m_ProgressTimeFormat);
                }
            }
        }

        private void PlayAfterPrepared(bool fromBeginning)
        {
            if (fromBeginning)
            {
                PerformActionAfterPrepared(PlayFromBeginning);
            }
            else
            {
                PerformActionAfterPrepared(Resume);
            }
        }

        private void PlayFromBeginning()
        {
            DoStartPlayback();
        }

        private void Resume()
        {
            DoResumePlayback();
        }

        private void PerformActionAfterPrepared(UnityAction onPrepared)
        {
            if (IsPrepared())
            {
                onPrepared.Invoke();
            }
            else
            {
                DoPreparePlayback();
                m_OnNextPrepared.AddPersistentListener(onPrepared);
            }
        }

        private void OnPlaybackFinished()
        {
            Finish();
        }
        #endregion

        #region Editor Methods
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            Refresh();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_PlaybackControlPanel))
            {
                if (!m_AllowPlaybackControl)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
        #endregion
    }

}