﻿using System;
using UnityEngine;
using UnityEngine.Events;
using ImprobableStudios.Localization.SmartFormat;
using ImprobableStudios.BaseUtilities;
using UnityEngine.Serialization;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Timer")]
    public class Timer : Counter
    {
        private static Timer s_defaultTimer;

        [Tooltip("If true, the timer will start when it is enabled")]
        [SerializeField]
        [FormerlySerializedAs("m_StartTimerOnEnable")]
        protected bool m_StartCountingOnEnable;

        [Tooltip("If true, the timer will be unaffected by time scale")]
        [SerializeField]
        protected bool m_UseUnscaledTime;
        
        protected bool m_isCounting;

        public static Timer DefaultTimer
        {
            get
            {
                if (s_defaultTimer == null)
                {
                    s_defaultTimer = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<Timer>("~Timer"));
                }
                return s_defaultTimer;
            }
        }

        public bool StartCountingOnEnable
        {
            get
            {
                return m_StartCountingOnEnable;
            }
            set
            {
                m_StartCountingOnEnable = value;
            }
        }
        
        public bool UseUnscaledTime
        {
            get
            {
                return m_UseUnscaledTime;
            }
            set
            {
                m_UseUnscaledTime = value;
            }
        }

        public bool IsCounting
        {
            get
            {
                return m_isCounting;
            }
        }
        
        protected override void Reset()
        {
            base.Reset();

            m_ValueFormat = "{0:HH:mm:ss}/{1:HH:mm:ss}";
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_StartCountingOnEnable)
            {
                StartTimer();
            }
        }

        protected override void Update()
        {
            base.Update();

            if (m_isCounting)
            {
                IncrementCounter(GetCurrentDeltaTime());
            }
        }
        
        protected float GetCurrentDeltaTime()
        {
            if (m_UseUnscaledTime)
            {
                return Time.unscaledDeltaTime;
            }

            return Time.deltaTime;
        }

        public void StartTimer()
        {
            gameObject.SetActive(true);

            if (m_needsReset)
            {
                ResetCounter();
            }

            m_isCounting = true;
        }

        public void StopTimer()
        {
            m_isCounting = false;
        }

        protected override void SetupRaisedCounterUI(float targetValue, bool countdown, UnityAction onShowComplete = null, UnityAction onCounterComplete = null)
        {
            base.SetupRaisedCounterUI(targetValue, countdown, onShowComplete, onCounterComplete);

            StartTimer();
        }

        public override void ClearCounter()
        {
            StopTimer();

            base.ClearCounter();
        }

        public override void ResetCounter()
        {
            StopTimer();

            base.ResetCounter();
        }

        protected override void OnCounterFinished()
        {
            StopTimer();

            base.OnCounterFinished();
        }

        protected override void HideUI(bool instantly, bool disableOnHidden, UnityAction onKill, UnityAction onComplete)
        {
            if (disableOnHidden)
            {
                StopTimer();
            }

            base.HideUI(instantly, disableOnHidden, onKill, onComplete);
        }

        public override string GetFormattedValueString(float value, string valueFormat)
        {
            float valueToFormat = value;
            if (m_Countdown && value > 0 && value < m_MaxValue)
            {
                valueToFormat++;
            }
            if (valueToFormat < 0)
            {
                valueToFormat = Math.Abs(valueToFormat);
                valueFormat = valueFormat.Replace("{0:", "-{0:");
            }
            long valueTicks = (long)(valueToFormat * 10000 * 1000);
            DateTime valueDateTime = new DateTime(valueTicks);

            float maxValueToFormat = m_MaxValue;
            if (maxValueToFormat < 0)
            {
                maxValueToFormat = Math.Abs(maxValueToFormat);
                valueFormat = valueFormat.Replace("{1:", "-{1:");
            }
            long maxValueTicks = (long)(maxValueToFormat * 10000 * 1000);
            DateTime maxValueDateTime = new DateTime(maxValueTicks);

            return Smart.Format(valueFormat, valueDateTime, maxValueDateTime);
        }

        public static string GetFormattedTimeString(float value, string valueFormat = "{0:HH:mm:ss}")
        {
            float valueToFormat = value;
            if (valueToFormat < 0)
            {
                valueToFormat = Math.Abs(valueToFormat);
                valueFormat = valueFormat.Replace("{0:", "-{0:");
            }
            long valueTicks = (long)(valueToFormat * 10000 * 1000);
            if (valueTicks >= DateTime.MinValue.Ticks && valueTicks <= DateTime.MaxValue.Ticks)
            {
                DateTime valueDateTime = new DateTime(valueTicks);
                return Smart.Format(valueFormat, valueDateTime);
            }
            return valueFormat;
        }
    }

}
