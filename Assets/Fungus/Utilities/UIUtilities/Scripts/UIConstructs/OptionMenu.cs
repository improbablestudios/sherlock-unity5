﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ImprobableStudios.UIUtilities
{

    public abstract class OptionMenu : Menu<OptionState>
    {
        [Serializable]
        public class OptionEvent : UnityEvent<int>
        {
        }
        
        [SerializeField]
        protected OptionEvent m_OnSubmit = new OptionEvent();

        private static OptionMenu s_defaultOptionMenu;

        public OptionEvent OnSubmit
        {
            get
            {
                return m_OnSubmit;
            }
        }

        public static OptionMenu DefaultOptionMenu
        {
            get
            {
                if (s_defaultOptionMenu == null)
                {
                    s_defaultOptionMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<OptionMenu>("~ToggleOptionMenu"));
                }
                return s_defaultOptionMenu;
            }
        }
        
        public abstract bool CanDisplayIcon();
        
        private List<int> m_selectedOptionIndices = new List<int>();

        public abstract void SetupSelectedOptionIndices();

        public void AddSelectedOptionIndex(int index)
        {
            if (!m_selectedOptionIndices.Contains(index))
            {
                m_selectedOptionIndices.Add(index);
            }
        }

        public void RemoveSelectedOptionIndex(int index)
        {
            if (m_selectedOptionIndices.Contains(index))
            {
                m_selectedOptionIndices.Remove(index);
            }
        }

        public void ClearSelectedOptionIndices()
        {
            m_selectedOptionIndices.Clear();
        }

        public void SetSelectedOptionIndex(int index)
        {
            ClearSelectedOptionIndices();
            AddSelectedOptionIndex(index);
        }

        public void SetSelectedOptionIndices(int[] indices)
        {
            ClearSelectedOptionIndices();
            foreach (int index in indices)
            {
                AddSelectedOptionIndex(index);
            }
        }

        protected override void OnSetupMenu()
        {
            base.OnSetupMenu();
            SetupSelectedOptionIndices();
        }

        protected override void OnSubmitted()
        {
            OptionEvent optionEvent = m_OnSubmit;
            int[] indices = m_selectedOptionIndices.ToArray();
            
            if (m_ClearOnSubmit)
            {
                m_OnSubmit = new OptionEvent();
                ClearSelectedOptionIndices();
            }
            
            foreach (int index in indices)
            {
                optionEvent.Invoke(index);
            }
        }

        protected override void OnCanceled()
        {
            base.OnCanceled();
            
            if (m_ClearOnCancel)
            {
                m_OnSubmit.RemoveAllListeners();
                ClearSelectedOptionIndices();
            }
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_OnSubmit))
            {
                return 60;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}