﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(VideoPlayer))]
    [AddComponentMenu("UI/Playback/Video Playback Controller")]
    public class VideoPlaybackController : PlaybackController
    {
        #region Serialized Fields
        [Tooltip("The toggle whose value will reflect the current fullscreen state of the video and can be used to set the current fullscreen state of the video")]
        [SerializeField]
        protected Toggle m_FullscreenToggle;

        [Tooltip("The panel that will be resized when fullscreen is toggled on or off")]
        [SerializeField]
        protected RectTransform m_ResizePanel;

        [Tooltip("The panel that specifies the space the video panel should occupy when fullscreened")]
        [SerializeField]
        protected RectTransform m_FullscreenedPanel;

        [Tooltip("The panel that specifies the space the video panel should occupy when windowed")]
        [SerializeField]
        protected RectTransform m_WindowedPanel;

        private VideoPlayer m_videoPlayer;
        #endregion

        #region Public Properties
        public Toggle FullscreenToggle
        {
            get
            {
                return m_FullscreenToggle;
            }
            set
            {
                m_FullscreenToggle = value;
                Refresh();
            }
        }

        public RectTransform ResizePanel
        {
            get
            {
                return m_ResizePanel;
            }
            set
            {
                m_ResizePanel = value;
                Refresh();
            }
        }

        public RectTransform FullscreenedPanel
        {
            get
            {
                return m_FullscreenedPanel;
            }
            set
            {
                m_FullscreenedPanel = value;
                Refresh();
            }
        }

        public RectTransform WindowedPanel
        {
            get
            {
                return m_WindowedPanel;
            }
            set
            {
                m_WindowedPanel = value;
                Refresh();
            }
        }

        public VideoPlayer VideoPlayer
        {
            get
            {
                if (m_videoPlayer == null)
                {
                    m_videoPlayer = GetComponent<VideoPlayer>();
                }
                return m_videoPlayer;
            }
        }
        #endregion

        #region Public Methods
        public void Setup(VideoClip videoClip)
        {
            if (videoClip != null)
            {
                if (VideoPlayer != null)
                {
                    VideoPlayer.source = VideoSource.VideoClip;
                    VideoPlayer.clip = videoClip;
                }

                Setup();
            }
        }

        public void Play(VideoClip videoClip, UnityAction onComplete = null)
        {
            if (videoClip != null)
            {
                if (VideoPlayer != null)
                {
                    VideoPlayer.source = VideoSource.VideoClip;
                    VideoPlayer.clip = videoClip;
                }

                Play(onComplete);
            }
        }

        public void Setup(string url, ushort controlledAudioTrackCount)
        {
            if (!string.IsNullOrEmpty(url))
            {
                if (VideoPlayer != null)
                {
                    VideoPlayer.source = VideoSource.Url;
                    VideoPlayer.url = url;
                    VideoPlayer.controlledAudioTrackCount = controlledAudioTrackCount;
                }

                Setup();
            }
        }

        public void Play(string url, ushort controlledAudioTrackCount, UnityAction onComplete = null)
        {
            if (!string.IsNullOrEmpty(url))
            {
                if (VideoPlayer != null)
                {
                    VideoPlayer.source = VideoSource.Url;
                    VideoPlayer.url = url;
                    VideoPlayer.controlledAudioTrackCount = controlledAudioTrackCount;
                }

                Play(onComplete);
            }
        }
        #endregion

        #region Virtual Methods
        protected override void AddListeners()
        {
            base.AddListeners();

            if (m_FullscreenToggle != null)
            {
                m_FullscreenToggle.onValueChanged.AddListener(OnFullscreenToggleValueChanged);
            }
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();

            if (m_FullscreenToggle != null)
            {
                m_FullscreenToggle.onValueChanged.RemoveListener(OnFullscreenToggleValueChanged);
            }
        }

        #endregion

        #region Private Methods
        private void SetFullscreenState(bool fullscreen)
        {
            if (m_ResizePanel != null)
            {
                if (fullscreen)
                {
                    if (m_FullscreenedPanel != null)
                    {
                        MatchRectTransformSize(m_ResizePanel, m_FullscreenedPanel);
                    }
                }
                else
                {
                    if (m_WindowedPanel != null)
                    {
                        MatchRectTransformSize(m_ResizePanel, m_WindowedPanel);
                    }
                }
            }
        }
        
        private void MatchRectTransformSize(RectTransform rectTransform, RectTransform targetRectTransform)
        {
            rectTransform.SetParent(targetRectTransform);
            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
        }
        #endregion

        #region Listener Methods
        protected virtual void OnFullscreenToggleValueChanged(bool on)
        {
            SetFullscreenState(on);
        }
        #endregion

        #region Override Methods
        protected override UnityEngine.Object GetPlaybackSource()
        {
            return VideoPlayer;
        }

        protected override bool CanPlay()
        {
            if (VideoPlayer != null)
            {
                return (VideoPlayer.source == VideoSource.VideoClip && VideoPlayer.clip != null) ||
                (VideoPlayer.source == VideoSource.Url && !string.IsNullOrEmpty(VideoPlayer.url));
            }
            return false;
        }

        protected override bool IsPrepared()
        {
            if (VideoPlayer != null)
            {
                return VideoPlayer.isPrepared;
            }
            return false;
        }

        protected override bool IsPlaying()
        {
            if (VideoPlayer != null)
            {
                return VideoPlayer.isPlaying;
            }
            return false;
        }

        protected override void DoSetupTargets()
        {
            SetTargetAudioSourceForAllTracks();
            SetTargetCamera();
        }

        protected override float DoGetVolume()
        {
            if (VideoPlayer != null)
            {
                for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                {
                    if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.Direct)
                    {
                        return VideoPlayer.GetDirectAudioVolume(i);
                    }
                    else if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
                    {
                        AudioSource audioSource = VideoPlayer.GetTargetAudioSource(i);
                        if (audioSource != null)
                        {
                            return audioSource.volume;
                        }
                    }
                }
            }

            return 1f;
        }

        protected override void DoSetVolume(float volume)
        {
            if (VideoPlayer != null)
            {
                for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                {
                    if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.Direct)
                    {
                        VideoPlayer.SetDirectAudioVolume(i, volume);
                    }
                    else if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
                    {
                        AudioSource audioSource = VideoPlayer.GetTargetAudioSource(i);
                        if (audioSource != null)
                        {
                            audioSource.volume = volume;
                        }
                    }
                }
            }
        }

        protected override bool DoGetMuteState()
        {
            if (VideoPlayer != null)
            {
                for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                {
                    if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.Direct)
                    {
                        return VideoPlayer.GetDirectAudioMute(i);
                    }
                    else if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
                    {
                        AudioSource audioSource = VideoPlayer.GetTargetAudioSource(i);
                        if (audioSource != null)
                        {
                            return audioSource.mute;
                        }
                    }
                }
            }

            return false;
        }

        protected override void DoSetMuteState(bool mute)
        {
            if (VideoPlayer != null)
            {
                for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                {
                    if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.Direct)
                    {
                        VideoPlayer.SetDirectAudioMute(i, mute);
                    }
                    else if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
                    {
                        AudioSource audioSource = VideoPlayer.GetTargetAudioSource(i);
                        audioSource.mute = mute;
                    }
                }
            }
        }

        protected override float DoGetMaxPlaybackTime()
        {
            if (VideoPlayer != null)
            {
                if (VideoPlayer.source == VideoSource.VideoClip && VideoPlayer.clip != null)
                {
                    return (float)VideoPlayer.clip.length;
                }
                else if (VideoPlayer.source == VideoSource.Url && !string.IsNullOrEmpty(VideoPlayer.url) && VideoPlayer.frameCount != 0)
                {
                    return (float)VideoPlayer.frameCount / VideoPlayer.frameRate;
                }
            }
            return 0;
        }

        protected override float DoGetCurrentPlaybackTime()
        {
            if (VideoPlayer != null)
            {
                return (float)VideoPlayer.time;
            }
            return 0f;
        }

        protected override void DoSetCurrentPlaybackTime(float time)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.time = time;
            }
        }

        protected override void DoSetPlayOnAwake(bool playOnAwake)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.playOnAwake = playOnAwake;
            }
        }

        protected override void DoClear()
        {
            if (VideoPlayer != null)
            {
                if (VideoPlayer.renderMode == VideoRenderMode.RenderTexture)
                {
                    RenderTexture rt = RenderTexture.active;
                    RenderTexture.active = VideoPlayer.targetTexture;
                    GL.Clear(true, true, Color.clear);
                    RenderTexture.active = rt;
                }
            }
        }

        protected override void DoPreparePlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.Prepare();
            }
        }

        protected override void DoStartPlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.Stop();
                VideoPlayer.Play();
            }
        }

        protected override void DoUpdatePlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.StepForward();
            }
        }

        protected override void DoPausePlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.Pause();
            }
        }

        protected override void DoResumePlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.Play();
            }
        }

        protected override void DoStopPlayback()
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.Stop();
            }
        }

        protected override void SubscribeToOnPrepared(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.prepareCompleted += (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }

        protected override void SubscribeToOnStarted(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.started += (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }

        protected override void SubscribeToOnFinished(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.loopPointReached += (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }

        protected override void UnsubscribeFromOnPrepared(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.prepareCompleted -= (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }

        protected override void UnsubscribeFromOnStarted(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.started -= (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }

        protected override void UnsubscribeFromOnFinished(Action<UnityEngine.Object> action)
        {
            if (VideoPlayer != null)
            {
                VideoPlayer.loopPointReached -= (VideoPlayer videoPlayer) => action(videoPlayer);
            }
        }
        #endregion

        #region Private Methods
        private void SetTargetAudioSourceForAllTracks()
        {
            if (VideoPlayer != null)
            {
                if (VideoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource && m_AudioSource != null)
                {
                    for (ushort i = 0; i < VideoPlayer.controlledAudioTrackCount; i++)
                    {
                        if (VideoPlayer.GetTargetAudioSource(i) == null)
                        {
                            AudioSource trackAudioSource = m_AudioSource;
                            if (i > 0)
                            {
                                trackAudioSource = Instantiate(m_AudioSource, m_AudioSource.transform.parent);
                            }
                            VideoPlayer.SetTargetAudioSource(i, trackAudioSource);
                        }
                    }
                }
            }
        }

        private void SetTargetCamera()
        {
            if (VideoPlayer != null)
            {
                if (VideoPlayer.renderMode == VideoRenderMode.CameraNearPlane || VideoPlayer.renderMode == VideoRenderMode.CameraFarPlane)
                {
                    if (VideoPlayer.targetCamera == null && Camera.main != null)
                    {
                        VideoPlayer.targetCamera = Camera.main;
                    }
                }
            }
        }
        #endregion
    }

}