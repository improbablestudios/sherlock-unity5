﻿using System.Collections.Generic;
using System.Linq;
using ImprobableStudios.UIUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public abstract class ListOptionMenu<T> : OptionMenu where T : OptionUI
    {
        [Tooltip("The ui used to create new options. If none is specified, new options will be created using the first option in the Option UI list")]
        [SerializeField]
        protected T m_TemplateOptionUI;

        [Tooltip("The ui used to represent options.")]
        [SerializeField]
        protected List<T> m_OptionUI = new List<T>();
        
        public T TemplateOptionUI
        {
            get
            {
                return m_TemplateOptionUI;
            }
            set
            {
                m_TemplateOptionUI = value;
                InvokePropertyChanged(nameof(m_TemplateOptionUI));
            }
        }

        public T[] OptionUI
        {
            get
            {
                return m_OptionUI.ToArray();
            }
            set
            {
                m_OptionUI = value.ToList();
                InvokePropertyChanged(nameof(m_OptionUI));
            }
        }

        public T[] ActiveOptions
        {
            get
            {
                return m_OptionUI.Where(x => x.gameObject.activeSelf).ToArray();
            }
        }

        protected override void RebuildMenuItems()
        {
            base.RebuildMenuItems();
            for (int i = m_MenuItems.Count; i < m_OptionUI.Count; i++)
            {
                DestroyMenuItem(i, null);
            }
        }

        protected override void PopulateMenu()
        {
            T[] options = GetComponentsInChildren<T>(true);
            for (int i = 0; i < options.Length; i++)
            {
                T option = options[i];
                if (i < m_OptionUI.Count)
                {
                    m_OptionUI[i] = option;
                }
                else
                {
                    m_OptionUI.Add(option);
                }
            }

            if (m_OptionUI.Count > 0)
            {
                m_TemplateOptionUI = m_OptionUI[0];
            }
        }

        public virtual T GetTemplateOptionUI()
        {
            if (m_TemplateOptionUI != null)
            {
                return m_TemplateOptionUI;
            }
            foreach (T option in m_OptionUI)
            {
                if (option != null)
                {
                    return option;
                }
            }

            return null;
        }
        
        protected override void OnInstantiate()
        {
            base.OnInstantiate();

            if (Application.isPlaying)
            {
                T templateOption = GetTemplateOptionUI();
                if (templateOption != null && !m_OptionUI.Contains(templateOption))
                {
                    templateOption.gameObject.SetActive(false);
                }
            }
        }
        
        public int GetFirstInactiveOptionIndex()
        {
            for (int i = 0; i < m_OptionUI.Count; i++)
            {
                T option = m_OptionUI[i];
                if (option != null && !option.IsDoingShowTransitionOrFullyShown)
                {
                    return i;
                }
            }

            return -1;
        }

        public int GetFirstActiveOptionIndex()
        {
            for (int i = 0; i < m_OptionUI.Count; i++)
            {
                T option = m_OptionUI[i];
                if (option != null && option.gameObject.activeSelf)
                {
                    return i;
                }
            }

            return -1;
        }
        
        protected override void CreateMenuItems(OptionState[] optionStates, UnityAction onComplete = null)
        {
            for (int i = 0; i < optionStates.Length; i++)
            {
                OptionState menuItemState = optionStates[i];
                UnityAction callback = null;
                if (i == m_MenuItems.Count - 1)
                {
                    callback = onComplete;
                }
                CreateMenuItem(menuItemState, i, callback);
            }
        }

        protected override void CreateMenuItem(OptionState optionState, int displayIndex, UnityAction onComplete)
        {
            T optionUI = null;
            T optionUITemplate = GetTemplateOptionUI();
            
            if (displayIndex >= 0)
            {
                for (int i = 0; i <= displayIndex; i++)
                {
                    if (i < m_OptionUI.Count)
                    {
                        optionUI = m_OptionUI[i];
                    }
                    else
                    {
                        optionUI = null;
                    }
                    
                    if (optionUI == null && optionUITemplate != null)
                    {
                        optionUI = InstantiateAndRegister(optionUITemplate);
                        optionUI.transform.SetParent(optionUITemplate.transform.parent, false);
                        optionUI.gameObject.name = "Option " + (i + 1);
                    }

                    if (optionUI != null)
                    {
                        if (i < m_OptionUI.Count)
                        {
                            m_OptionUI[i] = optionUI;
                        }
                        else
                        {
                            m_OptionUI.Add(optionUI);
                        }
                    }
                }
            }
            
            if (optionUI != null)
            {
                optionUI.ShowOption(displayIndex, optionState, 0, onComplete);
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            }
        }

        protected override void DestroyMenuItems(UnityAction onComplete = null)
        {
            for (int i = 0; i < m_OptionUI.Count; i++)
            {
                UnityAction callback = null;
                if (i == m_OptionUI.Count - 1)
                {
                    callback = onComplete;
                }
                DestroyMenuItem(i, callback);
            }
        }

        protected override void DestroyMenuItem(int index, UnityAction onComplete)
        {
            if (index >= 0 && index < m_OptionUI.Count)
            {
                if (m_OptionUI[index] != null)
                {
                    m_OptionUI[index].gameObject.SetActive(false);
                }
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        protected override void OnBeforeShowMenuItems()
        {
            base.OnBeforeShowMenuItems();

            T templateOption = GetTemplateOptionUI();
            if (templateOption != null)
            {
                if (!m_OptionUI.Contains(templateOption))
                {
                    templateOption.gameObject.SetActive(false);
                }
            }
        }
        
        public override bool CanDisplayIcon()
        {
            return (GetTemplateOptionUI() != null && GetTemplateOptionUI().IconImage != null);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_TemplateOptionUI) || propertyPath == nameof(m_OptionUI))
            {
                return base.GetPropertyOrder(nameof(m_MenuItems)) - 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}