﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Events;
using System;

namespace ImprobableStudios.UIUtilities
{

    [RequireComponent(typeof(AudioSource))]
    [AddComponentMenu("UI/Playback/Audio Playback Controller")]
    public class AudioPlaybackController : PlaybackController
    {
        #region Public Methods
        public void Setup(AudioClip audioClip)
        {
            if (audioClip != null)
            {
                if (AudioSource != null)
                {
                    AudioSource.clip = audioClip;
                }

                Setup();
            }
        }

        public void Play(AudioClip audioClip, UnityAction onComplete = null)
        {
            if (audioClip != null)
            {
                if (AudioSource != null)
                {
                    AudioSource.clip = audioClip;
                }

                Play(onComplete);
            }
        }
        #endregion


        #region Override Methods
        protected override UnityEngine.Object GetPlaybackSource()
        {
            return AudioSource;
        }

        protected override bool CanPlay()
        {
            return AudioSource.clip != null;
        }

        protected override bool IsPrepared()
        {
            return true;
        }

        protected override bool IsPlaying()
        {
            return AudioSource.isPlaying;
        }

        protected override void DoSetupTargets()
        {
        }

        protected override float DoGetVolume()
        {
            if (m_AudioSource != null)
            {
                return m_AudioSource.volume;
            }

            return 1f;
        }

        protected override void DoSetVolume(float volume)
        {
            if (m_AudioSource != null)
            {
                m_AudioSource.volume = volume;
            }
        }

        protected override bool DoGetMuteState()
        {
            if (m_AudioSource != null)
            {
                return m_AudioSource.mute;
            }

            return false;
        }

        protected override void DoSetMuteState(bool mute)
        {
            if (m_AudioSource != null)
            {
                m_AudioSource.mute = mute;
            }
        }

        protected override float DoGetMaxPlaybackTime()
        {
            if (AudioSource.clip != null)
            {
                return AudioSource.clip.length;
            }
            return 0f;
        }

        protected override float DoGetCurrentPlaybackTime()
        {
            return AudioSource.time;
        }

        protected override void DoSetCurrentPlaybackTime(float time)
        {
            AudioSource.time = time;
        }

        protected override void DoSetPlayOnAwake(bool playOnAwake)
        {
            AudioSource.playOnAwake = playOnAwake;
        }

        protected override void DoClear()
        {
        }

        protected override void DoPreparePlayback()
        {
        }

        protected override void DoStartPlayback()
        {
            AudioSource.Play();
        }

        protected override void DoUpdatePlayback()
        {
        }

        protected override void DoPausePlayback()
        {
            AudioSource.Pause();
        }

        protected override void DoResumePlayback()
        {
            AudioSource.UnPause();
        }

        protected override void DoStopPlayback()
        {
            AudioSource.Stop();
        }

        protected override void SubscribeToOnPrepared(Action<UnityEngine.Object> action)
        {
        }

        protected override void SubscribeToOnStarted(Action<UnityEngine.Object> action)
        {
        }

        protected override void SubscribeToOnFinished(Action<UnityEngine.Object> action)
        {
        }

        protected override void UnsubscribeFromOnPrepared(Action<UnityEngine.Object> action)
        {
        }

        protected override void UnsubscribeFromOnStarted(Action<UnityEngine.Object> action)
        {
        }

        protected override void UnsubscribeFromOnFinished(Action<UnityEngine.Object> action)
        {
        }
        #endregion
    }

}