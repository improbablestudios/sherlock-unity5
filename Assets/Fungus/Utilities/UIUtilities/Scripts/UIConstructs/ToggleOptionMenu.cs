﻿using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Menu/Toggle Option Menu")]
    public class ToggleOptionMenu : ListOptionMenu<ToggleOptionUI>
    {
        private static ToggleOptionMenu s_defaultToggleOptionMenu;
        
        [Tooltip("The toggles that will be toggled on when the menu is shown")]
        [SerializeField]
        protected Toggle[] m_ToggleByDefault;

        protected ToggleOptionUI m_lastToggledOn;

        public static ToggleOptionMenu DefaultToggleOptionMenu
        {
            get
            {
                if (s_defaultToggleOptionMenu == null)
                {
                    s_defaultToggleOptionMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<ToggleOptionMenu>("~ToggleOptionMenu"));
                }
                return s_defaultToggleOptionMenu;
            }
        }
        
        public Toggle[] ToggleByDefault
        {
            get
            {
                return m_ToggleByDefault;
            }
            set
            {
                m_ToggleByDefault = value;
                InvokePropertyChanged(nameof(m_ToggleByDefault));
            }
        }
        
        protected override void SetupMenuItems()
        {
            foreach (ToggleOptionUI toggleOption in m_OptionUI)
            {
                if (toggleOption != null)
                {
                    if (toggleOption.Toggle != null && !m_ToggleByDefault.Contains(toggleOption.Toggle))
                    {
                        toggleOption.Toggle.SetIsOnWithoutNotify(false);
                    }
                }
            }
        }

        protected override void SetupSelection()
        {
            base.SetupSelection();
            
            foreach (Toggle toggle in m_ToggleByDefault)
            {
                if (toggle != null)
                {
                    toggle.SetIsOnWithoutNotify(true);
                }
            }
        }

        public override void SetupSelectedOptionIndices()
        {
            List<int> selectedIndices = new List<int>();

            for (int i = 0; i < m_OptionUI.Count; i++)
            {
                ToggleOptionUI option = m_OptionUI[i];
                if (option.Toggle.gameObject.activeSelf && option.Toggle.isOn)
                {
                    selectedIndices.Add(i);
                }
            }

            SetSelectedOptionIndices(selectedIndices.ToArray());
        }

        protected override void DestroyMenuItems(UnityAction onComplete = null)
        {
            SetupSelectedOptionIndices();
            base.DestroyMenuItems(onComplete);
        }

        protected override void DestroyMenuItem(int index, UnityAction onComplete)
        {
            base.DestroyMenuItem(index, onComplete);

            if (index >= 0 && index < m_OptionUI.Count)
            {
                if (m_OptionUI[index] != null)
                {
                    m_OptionUI[index].Toggle.SetIsOnWithoutNotify(false);
                }
            }
        }
    }

}