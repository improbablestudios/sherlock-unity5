﻿using ImprobableStudios.BaseUtilities;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Menu/Input Menu")]
    public class InputMenu : Menu<InputState>
    {
        [Serializable]
        public class InputEvent : UnityEvent<string>
        {
        }

        private static InputMenu s_defaultInputMenu;

        [Tooltip("The ui that will be used to take input")]
        [SerializeField]
        [CheckNotNull]
        protected InputField m_InputField;

        [SerializeField]
        protected InputEvent m_OnSubmit = new InputEvent();

        protected string m_input;

        public static InputMenu DefaultInputMenu
        {
            get
            {
                if (s_defaultInputMenu == null)
                {
                    s_defaultInputMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<InputMenu>("~InputMenu"));
                }
                return s_defaultInputMenu;
            }
        }

        public InputField InputField
        {
            get
            {
                return m_InputField;
            }
            set
            {
                m_InputField = value;
                InvokePropertyChanged(nameof(m_InputField));
            }
        }

        public InputEvent OnSubmit
        {
            get
            {
                return m_OnSubmit;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            m_InputField.onValueChanged.AddListener(OnValueChanged);
            m_InputField.onEndEdit.AddListener(OnEndEdit);
            if (m_SubmitButton != null)
            {
                m_SubmitButton.onClick.AddListener(OnSubmitButtonClicked);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_InputField.onValueChanged.RemoveListener(OnValueChanged);
            m_InputField.onEndEdit.RemoveListener(OnEndEdit);
            if (m_SubmitButton != null)
            {
                m_SubmitButton.onClick.RemoveListener(OnSubmitButtonClicked);
            }
        }

        protected virtual void OnValueChanged(string value)
        {
            m_input = value;

            SetupSubmitButton();
        }

        protected virtual void OnEndEdit(string value)
        {
            if (m_MenuItems.Count > 0)
            {
                InputState inputState = m_MenuItems[0];

                if (m_InputField.text.Length >= inputState.CharacterMinimum)
                {
                    if (m_SubmitButton == null)
                    {
                        SubmitMenu();
                    }
                }
            }
        }

        protected override void OnSubmitted()
        {
            InputEvent inputEvent = m_OnSubmit;
            string input = m_InputField.text;

            if (m_ClearOnSubmit)
            {
                m_OnSubmit = new InputEvent();
            }

            if (inputEvent != null)
            {
                inputEvent.Invoke(input);
            }
        }
        
        protected override void OnCanceled()
        {
            base.OnCanceled();

            if (m_ClearOnCancel)
            {
                m_OnSubmit.RemoveAllListeners();
            }
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            m_InputField.text = m_input;
        }

        protected override void CreateMenuItems(InputState[] inputStates, UnityAction onComplete = null)
        {
            for (int i = 0; i < inputStates.Length; i++)
            {
                InputState menuItemState = inputStates[i];
                UnityAction callback = null;
                if (i == m_MenuItems.Count - 1)
                {
                    callback = onComplete;
                }
                CreateMenuItem(menuItemState, i, callback);
            }
        }

        protected override void CreateMenuItem(InputState inputState, int displayIndex, UnityAction onComplete)
        {
            if (inputState != null)
            {
                Text placeholderText = m_InputField.placeholder as Text;
                if (placeholderText != null)
                {
                    placeholderText.text = inputState.PlaceholderText;
                }
                m_InputField.text = "";
                m_InputField.contentType = inputState.ContentType;
                m_InputField.characterLimit = inputState.CharacterLimit;
                m_InputField.gameObject.SetActive(true);
            }
            else
            {
                m_InputField.gameObject.SetActive(false);
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        protected override void DestroyMenuItems(UnityAction onComplete = null)
        {
            DestroyMenuItem(0, onComplete);
        }

        protected override void DestroyMenuItem(int inputStateIndex, UnityAction onComplete)
        {
            m_InputField.text = "";

            if (onComplete != null)
            {
                onComplete();
            }
        }

        protected override void OnBeforeShowMenuItems()
        {
            base.OnBeforeShowMenuItems();

            m_InputField.text = "";
        }

        protected override void SetupMenuItems()
        {
            m_InputField.ActivateInputField();
            SetupSubmitButton();
        }

        protected void SetupSubmitButton()
        {
            if (m_MenuItems.Count > 0)
            {
                InputState inputState = m_MenuItems[0];

                if (m_SubmitButton != null)
                {
                    if (m_InputField.text.Length < inputState.CharacterMinimum)
                    {
                        m_SubmitButton.interactable = false;
                        return;
                    }
                }
            }

            m_SubmitButton.interactable = true;
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_OnSubmit))
            {
                return 60;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}