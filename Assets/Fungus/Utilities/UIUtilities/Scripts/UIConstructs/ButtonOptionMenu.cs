﻿using ImprobableStudios.BaseUtilities;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Menu/Button Option Menu")]
    public class ButtonOptionMenu : ListOptionMenu<ButtonOptionUI>
    {
        private static ButtonOptionMenu s_defaultButtonOptionMenu;
        
        public static ButtonOptionMenu DefaultButtonOptionMenu
        {
            get
            {
                if (s_defaultButtonOptionMenu == null)
                {
                    s_defaultButtonOptionMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<ButtonOptionMenu>("~ButtonOptionMenu"));
                }
                return s_defaultButtonOptionMenu;
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_SubmitButton))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override void SetupSelectedOptionIndices()
        {
            ClearSelectedOptionIndices();
        }
    }

}