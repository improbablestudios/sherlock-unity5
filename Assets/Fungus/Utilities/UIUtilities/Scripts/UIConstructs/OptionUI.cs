﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.Localization;

namespace ImprobableStudios.UIUtilities
{
    
    [DisallowMultipleComponent]
    public abstract class OptionUI : UIConstruct
    {
        [Tooltip("The image used to display the option icon")]
        [SerializeField]
        [FormerlySerializedAs("iconImage")]
        protected Text m_OptionText;

        [Tooltip("The image used to display the option icon")]
        [SerializeField]
        [FormerlySerializedAs("iconImage")]
        protected Image m_IconImage;
        
        protected int m_optionIndex;

        public Text OptionText
        {
            get
            {
                return m_OptionText;
            }
            set
            {
                m_OptionText = value;
            }
        }

        public Image IconImage
        {
            get
            {
                return m_IconImage;
            }
            set
            {
                m_IconImage = value;
            }
        }

        public void ShowOption(int optionIndex, OptionState optionState, float delay, UnityAction onShown = null)
        {
            SetupOption(optionIndex,optionState);
            Show(delay, onShown);
        }

        protected void SetupOption(int optionIndex, OptionState optionState)
        {
            m_optionIndex = optionIndex;
            SetOptionText(optionState.OptionLocalizedDictionary, optionState.OptionText);
            SetIconImage(optionState.Icon);
        }
        
        private void SetOptionText(Dictionary<string, string> optionLocalizedDictionary, string optionText)
        {
            if (m_OptionText != null)
            {
                m_OptionText.RegisterLocalizableText(optionLocalizedDictionary);
            }
            SetOptionText(optionText);
        }

        private void SetOptionText(string optionText)
        {
            if (m_OptionText != null)
            {
                if (!string.IsNullOrEmpty(optionText))
                {
                    m_OptionText.text = optionText;
                    m_OptionText.gameObject.SetActive(true);
                }
                else
                {
                    m_OptionText.gameObject.SetActive(false);
                }
            }
        }

        private void SetIconImage(Sprite icon)
        {
            if (m_IconImage != null)
            {
                if (icon != null)
                {
                    m_IconImage.sprite = icon;
                    m_IconImage.gameObject.SetActive(true);
                }
                else
                {
                    m_IconImage.gameObject.SetActive(false);
                }
            }
        }

        public abstract Selectable GetOptionSelectable();
    }

}
