﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [DisallowMultipleComponent]
    [AddComponentMenu("UI/Menu Option/Toggle Option UI")]
    public class ToggleOptionUI : OptionUI
    {
        [Tooltip("The toggle the player must toggle on to choose this option")]
        [SerializeField]
        [CheckNotNull]
        protected Toggle m_Toggle;
        
        public Toggle Toggle
        {
            get
            {
                return m_Toggle;
            }
            set
            {
                m_Toggle = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            m_Toggle = GetComponent<Toggle>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            m_Toggle.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            m_Toggle.onValueChanged.RemoveListener(OnValueChanged);
        }
        
        protected virtual void OnValueChanged(bool on)
        {
            ToggleOptionMenu menu = GetComponentInParent<ToggleOptionMenu>();

            if (menu != null)
            {
                menu.SetupSelectedOptionIndices();
                if (on)
                {
                    if (menu.SubmitButton == null)
                    {
                        menu.SubmitMenu();
                    }
                }
            }
        }

        public override Selectable GetOptionSelectable()
        {
            return m_Toggle;
        }
    }

}
