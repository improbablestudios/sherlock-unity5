﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{

    [DisallowMultipleComponent]
    [AddComponentMenu("UI/Menu Option/Button Option UI")]
    public class ButtonOptionUI : OptionUI
    {
        [Tooltip("The button the player must click to choose this option")]
        [SerializeField]
        [CheckNotNull]
        protected Button m_Button;

        public Button Button
        {
            get
            {
                return m_Button;
            }
            set
            {
                m_Button = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            m_Button = GetComponent<Button>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            m_Button.onClick.AddListener(OnClick);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_Button.onClick.RemoveListener(OnClick);
        }

        protected virtual void OnClick()
        {
            OptionMenu menu = GetComponentInParent<OptionMenu>();
            if (menu != null)
            {
                menu.SetSelectedOptionIndex(m_optionIndex);
                menu.SubmitMenu();
            }
        }

        public override Selectable GetOptionSelectable()
        {
            return m_Button;
        }
    }

}
