﻿using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public class Tooltip : UIConstruct
    {
        public Text m_TooltipText;

        public Image m_TooltipIcon;
    }

}
