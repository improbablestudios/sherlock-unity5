﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.InputUtilities;
using ImprobableStudios.Localization;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public abstract class Menu<T> : Menu where T : MenuItemState, new()
    {
        [Tooltip("The list of possible options.")]
        [SerializeField]
        protected List<T> m_MenuItems = new List<T>();

        public T[] MenuItems
        {
            get
            {
                return m_MenuItems.ToArray();
            }
            set
            {
                m_MenuItems = value.ToList();
                InvokePropertyChanged(nameof(m_MenuItems));
            }
        }

        protected abstract void CreateMenuItems(T[] menuItemStates, UnityAction onComplete = null);

        protected abstract void CreateMenuItem(T menuItemState, int displayIndex, UnityAction onComplete);

        protected abstract void DestroyMenuItems(UnityAction onComplete = null);

        protected abstract void DestroyMenuItem(int menuItemIndex, UnityAction onComplete);

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            if (propertyPath == nameof(m_MenuPrompt))
            {
                SetPrompt(m_MenuPrompt);
            }

            if (propertyPath == nameof(m_MenuItems))
            {
                RebuildMenuItems();
            }
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            SetPrompt(m_MenuPrompt);

            RebuildMenuItems();
        }

        protected virtual void RebuildMenuItems()
        {
            CreateMenuItems(m_MenuItems.ToArray());
        }
        
        public virtual void DisplayMenuItems(T[] menuItemStates)
        {
            m_MenuItems = menuItemStates.ToList();
            RebuildMenuItems();
        }

        public virtual int DisplayMenuItem(T menuItemState)
        {
            int displayIndex = GetNextAvailableDisplayIndex();
            DisplayMenuItem(menuItemState, displayIndex);
            return displayIndex;
        }

        public virtual void DisplayMenuItem(T menuItemState, int displayIndex)
        {
            OnBeforeShowMenuItems();

            if (Application.isPlaying)
            {
                Show();
            }

            CreateMenuItemAndState(menuItemState, displayIndex, SetupMenu);
        }
        
        public virtual int GetNextAvailableDisplayIndex()
        {
            return m_MenuItems.Count;
        }

        protected virtual void OnBeforeShowMenuItems()
        {
        }

        protected override void OnSetupMenu()
        {
            SetupMenuItems();
        }

        protected virtual void SetupMenuItems()
        {
        }

        protected override void HideMenuUI(UnityAction onComplete = null)
        {
            DestroyMenuItems(onComplete);
        }

        protected override void OnCleared()
        {
            DestroyStates();
        }

        protected virtual void CreateMenuItemsAndStates(T[] menuItemStates, UnityAction onComplete = null)
        {
            CreateStates(menuItemStates);
            CreateMenuItems(menuItemStates, onComplete);
        }

        protected void CreateMenuItemAndState(T menuItemState, int displayIndex, UnityAction onComplete)
        {
            CreateState(menuItemState, displayIndex);
            CreateMenuItem(menuItemState, displayIndex, onComplete);
        }

        protected virtual void CreateStates(T[] menuItemStates)
        {
            for (int i = 0; i < menuItemStates.Length; i++)
            {
                T menuItemState = menuItemStates[i];
                CreateState(menuItemState, i);
            }
        }

        protected virtual void CreateState(T menuItemState, int menuItemIndex)
        {
            if (menuItemIndex < m_MenuItems.Count)
            {
                m_MenuItems[menuItemIndex] = menuItemState;
            }
            else
            {
                int i = m_MenuItems.Count;
                while (i < menuItemIndex)
                {
                    m_MenuItems.Add(null);
                    i++;
                }
                m_MenuItems.Add(menuItemState);
            }
        }

        protected void DestroyMenuItemsAndStates(UnityAction onComplete = null)
        {
            DestroyStates();
            DestroyMenuItems(onComplete);
        }

        protected void DestroyMenuItemAndState(int menuItemIndex, UnityAction onComplete)
        {
            DestroyState(menuItemIndex);
            DestroyMenuItem(menuItemIndex, onComplete);
        }

        protected virtual void DestroyStates()
        {
            while (m_MenuItems.Count > 0)
            {
                DestroyState(0);
            }
        }

        protected virtual void DestroyState(int menuItemIndex)
        {
            m_MenuItems.RemoveAt(menuItemIndex);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_MenuItems))
            {
                return 50;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

    public abstract class Menu : UIConstruct, ISubmitGenerator, ICancelGenerator
    {
        public enum ExecutionTiming
        {
            AfterTransitionOut,
            BeforeTransitionOut,
        }

        private static Menu s_defaultMenu;

        [Tooltip("Setup this menu as soon as it's enabled")]
        [SerializeField]
        protected bool m_SetupOnEnable = true;

        [Tooltip("The text ui used to display the menu title")]
        [SerializeField]
        protected Text m_TitleText;

        [Tooltip("The text ui used to display the menu message")]
        [SerializeField]
        protected Text m_MessageText;

        [Tooltip("The image ui used to display the menu icon")]
        [SerializeField]
        protected Image m_IconImage;

        [Tooltip("The button used to submit")]
        [SerializeField]
        protected Button m_SubmitButton;

        [Tooltip("The button used to cancel")]
        [SerializeField]
        protected Button m_CancelButton;

        [Tooltip("The selectable that will be selected by default when the menu is shown")]
        [SerializeField]
        protected Selectable m_SelectByDefault;

        [Tooltip("If true, the menu will hide itself after the user submits it")]
        [SerializeField]
        protected bool m_HideOnSubmit = true;

        [Tooltip("If true, the menu will hide itself after the user cancels it")]
        [SerializeField]
        protected bool m_HideOnCancel = true;

        [Tooltip("If true, the menu will reset and remove all OnSubmitted listeners when it is submitted")]
        [SerializeField]
        protected bool m_ClearOnSubmit = true;

        [Tooltip("If true, the menu will reset and remove all OnSubmitted listeners when it is canceled")]
        [SerializeField]
        protected bool m_ClearOnCancel = true;

        [Tooltip("Determines whether submit and cancel events are executed before hiding the menu or after hiding the menu")]
        [SerializeField]
        protected ExecutionTiming m_EventExecutionTiming;

        [Tooltip("The event that executes when the menu is canceled")]
        [SerializeField]
        protected UnityEvent m_OnCancel;

        [Tooltip("The prompt for this menu.")]
        [SerializeField]
        protected MenuPromptState m_MenuPrompt = new MenuPromptState();

        public bool SetupOnEnable
        {
            get
            {
                return m_SetupOnEnable;
            }
            set
            {
                m_SetupOnEnable = value;
            }
        }

        public Text TitleText
        {
            get
            {
                return m_TitleText;
            }
            set
            {
                m_TitleText = value;
                InvokePropertyChanged(nameof(m_TitleText));
            }
        }

        public Text MessageText
        {
            get
            {
                return m_MessageText;
            }
            set
            {
                m_MessageText = value;
                InvokePropertyChanged(nameof(m_MessageText));
            }
        }

        public Image IconImage
        {
            get
            {
                return m_IconImage;
            }
            set
            {
                m_IconImage = value;
                InvokePropertyChanged(nameof(m_IconImage));
            }
        }

        public Button SubmitButton
        {
            get
            {
                return m_SubmitButton;
            }
            set
            {
                m_SubmitButton = value;
                InvokePropertyChanged(nameof(m_SubmitButton));
            }
        }

        public Button CancelButton
        {
            get
            {
                return m_CancelButton;
            }
            set
            {
                m_CancelButton = value;
                InvokePropertyChanged(nameof(m_CancelButton));
            }
        }

        public Selectable SelectByDefault
        {
            get
            {
                return m_SelectByDefault;
            }
            set
            {
                m_SelectByDefault = value;
                InvokePropertyChanged(nameof(m_SelectByDefault));
            }
        }

        public bool HideOnSubmit
        {
            get
            {
                return m_HideOnSubmit;
            }
            set
            {
                m_HideOnSubmit = value;
                InvokePropertyChanged(nameof(m_HideOnSubmit));
            }
        }

        public bool HideOnCancel
        {
            get
            {
                return m_HideOnCancel;
            }
            set
            {
                m_HideOnCancel = value;
                InvokePropertyChanged(nameof(m_HideOnCancel));
            }
        }

        public bool ClearOnSubmit
        {
            get
            {
                return m_ClearOnSubmit;
            }
            set
            {
                m_ClearOnSubmit = value;
                InvokePropertyChanged(nameof(m_ClearOnSubmit));
            }
        }

        public bool ClearOnCancel
        {
            get
            {
                return m_ClearOnCancel;
            }
            set
            {
                m_ClearOnCancel = value;
                InvokePropertyChanged(nameof(m_ClearOnCancel));
            }
        }

        public ExecutionTiming EventExecutionTiming
        {
            get
            {
                return m_EventExecutionTiming;
            }
            set
            {
                m_EventExecutionTiming = value;
                InvokePropertyChanged(nameof(m_EventExecutionTiming));
            }
        }

        public UnityEvent OnCancel
        {
            get
            {
                return m_OnCancel;
            }
        }

        public MenuPromptState MenuPrompt
        {
            get
            {
                return m_MenuPrompt;
            }
            set
            {
                m_MenuPrompt = value;
                InvokePropertyChanged(nameof(m_MenuPrompt));
            }
        }

        public static Menu DefaultMenu
        {
            get
            {
                if (s_defaultMenu == null)
                {
                    s_defaultMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<Menu>("~ToggleOptionMenu"));
                }
                return s_defaultMenu;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            PopulateMenu();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_SubmitButton != null)
            {
                m_SubmitButton.onClick.AddListener(OnSubmitButtonClicked);
            }
            if (m_CancelButton != null)
            {
                m_CancelButton.onClick.AddListener(OnCancelButtonClicked);
            }
            if (m_SetupOnEnable)
            {
                RebuildUI();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_SubmitButton != null)
            {
                m_SubmitButton.onClick.RemoveListener(OnSubmitButtonClicked);
            }
            if (m_CancelButton != null)
            {
                m_CancelButton.onClick.RemoveListener(OnCancelButtonClicked);
            }
        }

        protected override void OnInstantiate()
        {
            base.OnInstantiate();

            if (Application.isPlaying)
            {
                if (m_TitleText != null)
                {
                    m_TitleText.gameObject.SetActive(false);
                }
                if (m_MessageText != null)
                {
                    m_MessageText.gameObject.SetActive(false);
                }
                if (m_IconImage != null)
                {
                    m_IconImage.gameObject.SetActive(false);
                }
            }
        }

        protected override void OnBeforeActivate()
        {
            base.OnBeforeActivate();

            SavePreviousSelection();

            SetupSelection();
        }

        protected abstract void HideMenuUI(UnityAction onComplete = null);

        protected abstract void OnCleared();

        protected abstract void OnSubmitted();

        protected virtual void PopulateMenu()
        {
        }

        protected virtual void OnCancelButtonClicked()
        {
            CancelMenu();
        }

        protected virtual void OnSubmitButtonClicked()
        {
            SubmitMenu();
        }

        public void SetPrompt(MenuPromptState menuPromptState)
        {
            m_MenuPrompt = menuPromptState;
            if (menuPromptState != null)
            {
                SetTitleText(menuPromptState.TitleLocalizedDictionary, menuPromptState.TitleText);
                SetMessageText(menuPromptState.MessageLocalizedDictionary, menuPromptState.MessageText);
                SetIcon(menuPromptState.Icon);
            }
        }

        public void SetTitleText(Dictionary<string, string> localizedDictionary, string text)
        {
            if (m_MenuPrompt == null)
            {
                m_MenuPrompt = new MenuPromptState();
            }
            m_MenuPrompt.TitleLocalizedDictionary = localizedDictionary;

            if (m_TitleText != null)
            {
                m_TitleText.RegisterLocalizableText(localizedDictionary);
            }
            SetTitleText(text);
        }

        public void SetTitleText(string text)
        {
            if (Application.isPlaying)
            {
                Show();
            }

            if (m_MenuPrompt == null)
            {
                m_MenuPrompt = new MenuPromptState();
            }
            m_MenuPrompt.TitleText = text;

            if (m_TitleText != null)
            {
                if (!string.IsNullOrEmpty(text))
                {
                    m_TitleText.text = text;
                    m_TitleText.gameObject.SetActive(true);
                }
                else
                {
                    m_TitleText.gameObject.SetActive(false);
                }
            }
        }

        public void SetMessageText(Dictionary<string, string> localizedDictionary, string text)
        {
            if (m_MenuPrompt == null)
            {
                m_MenuPrompt = new MenuPromptState();
            }
            m_MenuPrompt.MessageLocalizedDictionary = localizedDictionary;

            if (m_MessageText != null)
            {
                m_MessageText.RegisterLocalizableText(localizedDictionary);
            }
            SetMessageText(text);
        }

        public void SetMessageText(string text)
        {
            if (Application.isPlaying)
            {
                Show();
            }

            if (m_MenuPrompt == null)
            {
                m_MenuPrompt = new MenuPromptState();
            }
            m_MenuPrompt.MessageText = text;

            if (m_MessageText != null)
            {
                if (!string.IsNullOrEmpty(text))
                {
                    m_MessageText.text = text;
                    m_MessageText.gameObject.SetActive(true);
                }
                else
                {
                    m_MessageText.gameObject.SetActive(false);
                }
            }
        }

        public void SetIcon(Sprite icon)
        {
            if (Application.isPlaying)
            {
                Show();
            }

            if (m_MenuPrompt == null)
            {
                m_MenuPrompt = new MenuPromptState();
            }
            m_MenuPrompt.Icon = icon;

            if (m_IconImage != null)
            {
                if (icon != null)
                {
                    m_IconImage.sprite = icon;
                    m_IconImage.gameObject.SetActive(true);
                }
                else
                {
                    m_IconImage.gameObject.SetActive(false);
                }
            }
        }

        public void CancelMenu(UnityAction onComplete = null)
        {
            if (m_EventExecutionTiming == ExecutionTiming.BeforeTransitionOut)
            {
                DoCancel();
            }

            if (m_HideOnCancel)
            {
                Hide(
                    () =>
                    {
                        if (m_ClearOnCancel)
                        {
                            HideMenuUI(
                                () =>
                                {
                                    if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                                    {
                                        DoCancel();
                                    }

                                    if (onComplete != null)
                                    {
                                        onComplete();
                                    }
                                });
                        }
                        else
                        {
                            if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                            {
                                DoCancel();
                            }

                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        }
                    });
            }
            else
            {
                if (m_ClearOnCancel)
                {
                    HideMenuUI(
                        () =>
                        {
                            if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                            {
                                DoCancel();
                            }

                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        });
                }
                else
                {
                    if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                    {
                        DoCancel();
                    }

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                }
            }
        }

        public void ClearMenu(UnityAction onComplete = null)
        {
            OnCleared();
            HideMenuUI(onComplete);
        }

        protected void DoCancel()
        {
            if (m_ClearOnCancel)
            {
                OnCleared();
            }

            OnCanceled();

            BaseEventData baseEvent = new BaseEventData(EventSystem.current);
            ExecuteEvents.Execute(gameObject, baseEvent, ExecuteEvents.cancelHandler);

            if (HasSelection())
            {
                RestorePreviousSelection();
            }
        }

        protected virtual void OnCanceled()
        {
            if (m_OnCancel != null)
            {
                m_OnCancel.Invoke();
            }
            m_OnCancel.RemoveAllListeners();
        }

        public void SubmitMenu(UnityAction onComplete = null)
        {
            if (m_EventExecutionTiming == ExecutionTiming.BeforeTransitionOut)
            {
                DoSubmit();
            }

            if (m_HideOnSubmit)
            {
                Hide(
                    () =>
                    {
                        if (m_ClearOnSubmit)
                        {
                            HideMenuUI(
                                () =>
                                {
                                    if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                                    {
                                        DoSubmit();
                                    }

                                    if (onComplete != null)
                                    {
                                        onComplete();
                                    }
                                });
                        }
                        else
                        {
                            if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                            {
                                DoSubmit();
                            }

                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        }
                    });
            }
            else
            {
                if (m_ClearOnSubmit)
                {
                    HideMenuUI(
                        () =>
                        {
                            if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                            {
                                DoSubmit();
                            }

                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        });
                }
                else
                {
                    if (m_EventExecutionTiming == ExecutionTiming.AfterTransitionOut)
                    {
                        DoSubmit();
                    }

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                }
            }
        }

        protected void DoSubmit()
        {
            if (m_ClearOnSubmit)
            {
                OnCleared();
            }

            OnSubmitted();

            BaseEventData baseEvent = new BaseEventData(EventSystem.current);
            ExecuteEvents.Execute(gameObject, baseEvent, ExecuteEvents.submitHandler);

            if (HasSelection())
            {
                RestorePreviousSelection();
            }
        }

        public void SetupMenu()
        {
            if (isActiveAndEnabled)
            {
                StartCoroutine(PerformActionAtEndOfFrame(SetupSelection));
            }

            OnSetupMenu();
        }

        protected virtual void SetupSelection()
        {
            if (m_SelectByDefault != null)
            {
                if (EventSystem.current != null)
                {
                    EventSystem.current.SetSelectedGameObject(m_SelectByDefault.gameObject);
                }
            }
        }
        
        protected virtual void OnSetupMenu()
        {
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_EventExecutionTiming))
            {
                return 60;
            }
            if (propertyPath == nameof(m_OnCancel))
            {
                return 60;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}
