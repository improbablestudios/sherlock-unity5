﻿using ImprobableStudios.Localization.SmartFormat;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.UIUtilities
{
    
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("UI/Pie Chart")]
    public class PieChart : PersistentBehaviour
    {
        private static Sprite s_defaultCircleSprite;

        [Tooltip("The slice template that will be used to create slices")]
        [SerializeField]
        private PieChartSlice m_SliceTemplate;

        [Tooltip("Determines whether label text is shown on slices")]
        [SerializeField]
        private bool m_ShowSliceLabelText = true;

        [Tooltip("Determines whether value text is shown on slices")]
        [SerializeField]
        private bool m_ShowSliceValueText = true;

        [Tooltip("Determines whether the value text is shown as a percentage")]
        [SerializeField]
        private bool m_ShowSliceValueAsPercentage = true;

        [Tooltip("Determines how the value text will be formatted when displayed as a percentage")]
        [SerializeField]
        private string m_PercentValueFormat = "{0:P0}";

        [Tooltip("Determines how the value text will be formatted when displayed as a number")]
        [SerializeField]
        private string m_NumericValueFormat = "{0:F2}";

        [Tooltip("The slices that make up this pie chart")]
        [SerializeField]
        protected PieChartSliceInfo[] m_Slices = new PieChartSliceInfo[0];

        protected List<PieChartSlice> m_Items = new List<PieChartSlice>();
        
        public static Sprite DefaultCircleSprite
        {
            get
            {
                if (s_defaultCircleSprite == null)
                {
                    s_defaultCircleSprite = Resources.Load<Sprite>("~Circle");
                }
                return s_defaultCircleSprite;
            }
        }

        public PieChartSlice SliceTemplate
        {
            get
            {
                return m_SliceTemplate;
            }
            set
            {
                m_SliceTemplate = value;
                CreateChart();
            }
        }
        
        public bool ShowSliceLabelText
        {
            get
            {
                return m_ShowSliceLabelText;
            }
            set
            {
                m_ShowSliceLabelText = value;
                UpdateSlices();
            }
        }

        public bool ShowSliceValueText
        {
            get
            {
                return m_ShowSliceValueText;
            }
            set
            {
                m_ShowSliceValueText = value;
                UpdateSlices();
            }
        }

        public bool ShowSliceValueAsPercentage
        {
            get
            {
                return m_ShowSliceValueAsPercentage;
            }
            set
            {
                m_ShowSliceValueAsPercentage = value;
                UpdateSlices();
            }
        }

        public string PercentValueFormat
        {
            get
            {
                return m_PercentValueFormat;
            }
            set
            {
                m_PercentValueFormat = value;
                UpdateSlices();
            }
        }

        public string NumericValueFormat
        {
            get
            {
                return m_NumericValueFormat;
            }
            set
            {
                m_NumericValueFormat = value;
                UpdateSlices();
            }
        }

        public PieChartSliceInfo[] Slices
        {
            get
            {
                return m_Slices;
            }
            set
            {
                m_Slices = value;
                CreateChart();
            }
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            CreateChart();
        }

        public void CreateChart()
        {
            if (!IsSliceTemplateValid())
            {
                return;
            }

            m_SliceTemplate.gameObject.SetActive(false);

            if (Application.isPlaying)
            {
                foreach (PieChartSlice slice in GetComponentsInChildren<PieChartSlice>())
                {
                    Destroy(slice.gameObject);
                }
                m_Items.Clear();
                CreateSlices();
                UpdateSlices();
            }
            else
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += 
                    () =>
                    {
                        if (this != null)
                        {
                            foreach (PieChartSlice slice in GetComponentsInChildren<PieChartSlice>())
                            {
                                DestroyImmediate(slice.gameObject);
                            }
                            m_Items.Clear();
                            CreateSlices();
                            UpdateSlices();
                        }
                    };
#endif
            }
        }

        public float GetTotal()
        {
            float total = 0f;
            for (int i = 0; i < m_Slices.Length; i++)
            {
                total += m_Slices[i].Value;
            }
            return total;
        }

        protected virtual void CreateSlices()
        {
            float total = GetTotal();

            m_SliceTemplate.gameObject.SetActive(true);
            
            for (int i = 0; i < m_Slices.Length; i++)
            {
                PieChartSliceInfo info = m_Slices[i];
                PieChartSlice slice = CreateAndAddSlice(m_SliceTemplate, info, total, m_Items);
                slice.transform.SetParent(m_SliceTemplate.transform.parent, false);
            }
            m_SliceTemplate.gameObject.SetActive(false);
        }

        private PieChartSlice CreateAndAddSlice(PieChartSlice sliceTemplate, PieChartSliceInfo info, float total, List<PieChartSlice> slices)
        {
            PieChartSlice slice = InstantiateAndRegister(sliceTemplate);
            slice.transform.SetParent(sliceTemplate.transform.parent, false);
            slice.gameObject.name = "Slice " + slices.Count + ((info.Label == null) ? "" : (": " + info.Label));
            slices.Add(slice);
            return slice;
        }

        public void SetSliceValue(int sliceIndex, float value)
        {
            if (sliceIndex >= 0 && sliceIndex < m_Slices.Length)
            {
                PieChartSliceInfo info = m_Slices[sliceIndex];
                info.Value = value;
                UpdateSlices();
            }
        }

        public void UpdateSlices()
        {
            float total = GetTotal();

            if (m_Items.Count == m_Slices.Length)
            {
                float zRotation = 0f;
                for (int i = 0; i < m_Slices.Length; i++)
                {
                    PieChartSliceInfo info = m_Slices[i];
                    PieChartSlice slice = m_Items[i];
                    Image sliceImage = slice.Image;
                    RectTransform sliceOverlayRoot = slice.OverlayRoot;

                    SetupSlice(slice, info, total);

                    float fillAmount = info.Value / total;
                    sliceImage.fillAmount = fillAmount;

                    Vector3 rotation = sliceImage.transform.eulerAngles;
                    rotation.z = zRotation;
                    sliceImage.transform.eulerAngles = rotation;

                    float multiplier = 360f;
                    if (sliceImage.fillClockwise)
                    {
                        multiplier = -multiplier;
                    }

                    Vector3 center = sliceImage.rectTransform.GetCenter();
                    float minAngle = zRotation;
                    if (sliceImage.fillOrigin == (int)Image.Origin360.Top)
                    {
                        minAngle += 90f;
                    }
                    else if (sliceImage.fillOrigin == (int)Image.Origin360.Right)
                    {
                        minAngle += 0f;
                    }
                    else if (sliceImage.fillOrigin == (int)Image.Origin360.Bottom)
                    {
                        minAngle += 270f;
                    }
                    else if (sliceImage.fillOrigin == (int)Image.Origin360.Left)
                    {
                        minAngle += 180f;
                    }
                    float fillAngle = ((fillAmount * multiplier));
                    float centerAngle = minAngle + (fillAngle / 2f);
                    float xRadius = (sliceImage.rectTransform.rect.width / 2f);
                    float yRadius = (sliceImage.rectTransform.rect.height / 2f);
                    float sliceCenterX = ((sliceImage.transform.lossyScale.x * xRadius) / 2f) * Mathf.Cos(centerAngle * Mathf.Deg2Rad);
                    float sliceCenterY = ((sliceImage.transform.lossyScale.y * yRadius) / 2f) * Mathf.Sin(centerAngle * Mathf.Deg2Rad);
                    Vector3 sliceCenter = center + new Vector3(sliceCenterX, sliceCenterY, 0);

                    sliceOverlayRoot.position = sliceCenter;
                    sliceOverlayRoot.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, xRadius);
                    sliceOverlayRoot.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, yRadius);

                    zRotation += fillAmount * multiplier;
                }
            }
        }

        protected virtual void SetupSlice(PieChartSlice slice, PieChartSliceInfo info, float total)
        {
            slice.gameObject.SetActive(true);
            if (slice.LabelText != null)
            {
                slice.LabelText.text = info.Label;
                slice.LabelText.gameObject.SetActive(m_ShowSliceLabelText && !string.IsNullOrEmpty(info.Label));
            }
            if (slice.ValueText != null)
            {
                string valueString = "";
                if (m_ShowSliceValueAsPercentage)
                {
                    float percent = info.Value / total;
                    valueString = Smart.Format(m_PercentValueFormat, percent);
                }
                else
                {
                    valueString = Smart.Format(m_NumericValueFormat, info.Value);
                }
                slice.ValueText.text = valueString;
                slice.ValueText.gameObject.SetActive(m_ShowSliceValueText);
            }
            if (slice.Image != null)
            {
                slice.Image.sprite = info.CircleSprite;
                if (info.CircleSprite == null)
                {
                    slice.Image.sprite = DefaultCircleSprite;
                }
                slice.Image.color = info.Color;
                slice.Image.type = Image.Type.Filled;
                slice.Image.fillMethod = Image.FillMethod.Radial360;
            }
        }

        protected virtual bool IsSliceTemplateValid()
        {
            if (m_SliceTemplate == null)
            {
                Debug.LogError("The slice template is not assigned. The template needs to be assigned and must have a PieChartSlice component attached to represent the slice.", this);
                return false;
            }
            else
            {
                if (!(m_SliceTemplate.transform is RectTransform))
                {
                    Debug.LogError("The slice template is not valid. The PieChartSlice component must have a RectTransform attached.", m_SliceTemplate);
                    return false;
                }
                else if (m_SliceTemplate.Image == null)
                {
                    Debug.LogError("The slice template is not valid. The PieChartSlice component must have an Image assigned.", m_SliceTemplate);
                    return false;
                }
                else if (m_SliceTemplate.LabelText == null)
                {
                    Debug.LogError("The slice template is not valid. The PieChartSlice component must have a Label Text assigned.", m_SliceTemplate);
                    return false;
                }
                else if (m_SliceTemplate.ValueText == null)
                {
                    Debug.LogError("The slice template is not valid. The PieChartSlice component must have a Value Text assigned.", m_SliceTemplate);
                    return false;
                }

                float fakeTotal = 3f;
                float fakeValue = 1f;
                SetupSlice(m_SliceTemplate, new PieChartSliceInfo("Slice", fakeValue, Color.white, null), fakeTotal);
                m_SliceTemplate.Image.fillAmount = fakeValue / fakeTotal;

                return true;
            }
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            CreateChart();
        }
    }

}
