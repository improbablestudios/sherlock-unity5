﻿using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Menu/Dropdown Option Menu")]
    public class DropdownOptionMenu : OptionMenu
    {
        [Tooltip("The ui that will be used to display options")]
        [SerializeField]
        [CheckNotNull]
        [FormerlySerializedAs("m_DropdownOptionUI")]
        protected Dropdown m_Dropdown;

        public Dropdown Dropdown
        {
            get
            {
                return m_Dropdown;
            }
            set
            {
                m_Dropdown = value;
                InvokePropertyChanged(nameof(m_Dropdown));
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            SetSelectedOptionIndex(Dropdown.value);

            m_Dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_Dropdown.onValueChanged.RemoveListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(int value)
        {
            SetSelectedOptionIndex(value);
            if (m_SubmitButton == null)
            {
                SubmitMenu();
            }
        }

        protected override void RebuildMenuItems()
        {
            base.RebuildMenuItems();
            for (int i = m_MenuItems.Count; i < Dropdown.options.Count; i++)
            {
                DestroyMenuItem(i, null);
            }
        }

        protected override void CreateMenuItems(OptionState[] optionStates, UnityAction onComplete = null)
        {
            for (int i = 0; i < optionStates.Length; i++)
            {
                OptionState menuItemState = optionStates[i];
                UnityAction callback = null;
                if (i == m_MenuItems.Count - 1)
                {
                    callback = onComplete;
                }
                CreateMenuItem(menuItemState, i, callback);
            }
        }

        protected override void CreateMenuItem(OptionState optionState, int displayIndex, UnityAction onComplete)
        {
            Dropdown.OptionData optionData = null;

            if (optionState != null)
            {
                optionData = new Dropdown.OptionData(optionState.OptionText, optionState.Icon);
            }

            if (displayIndex < m_Dropdown.options.Count)
            {
                m_Dropdown.options[displayIndex] = optionData;
            }
            else
            {
                int i = m_Dropdown.options.Count;
                while (i < displayIndex)
                {
                    m_Dropdown.options.Add(null);
                    i++;
                }
                m_Dropdown.options.Add(optionData);
            }

            m_Dropdown.RefreshShownValue();

            if (onComplete != null)
            {
                onComplete();
            }
        }

        protected override void DestroyMenuItems(UnityAction onComplete = null)
        {
            for (int i = 0; i < m_Dropdown.options.Count; i++)
            {
                UnityAction callback = null;
                if (i == m_Dropdown.options.Count - 1)
                {
                    callback = onComplete;
                }
                DestroyMenuItem(i, callback);
            }
        }

        protected override void DestroyMenuItem(int index, UnityAction onComplete)
        {
            if (index >= 0 && index < m_Dropdown.options.Count)
            {
                m_Dropdown.options.RemoveAt(index);
            }
            m_Dropdown.RefreshShownValue();

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public override bool CanDisplayIcon()
        {
            return (m_Dropdown != null && m_Dropdown.itemImage != null);
        }
        
        public override void SetupSelectedOptionIndices()
        {
            SetSelectedOptionIndex(Dropdown.value);
        }
    }

}