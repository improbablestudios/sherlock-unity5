﻿using DG.Tweening;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.UnityObjectUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    public class UIConstruct : PersistentBehaviour
    {
        public enum UITransitionStateType
        {
            Idle,
            Show,
            Finish,
            Hide,
        }

        public enum UITransitionSubState
        {
            Idle,
            IsDoingShowTransition,
            IsDoingHideTransition,
            IsDoingFinishTransition,
        }

        [Tooltip("If true, the ui will hide after it is finished performing its action")]
        [SerializeField]
        protected bool m_HideAfterFinished;

        [Tooltip("If true, hiding the ui will also disable it")]
        [SerializeField]
        protected bool m_DisableOnHidden = true;

        [Tooltip("The amount of time between the ui finishing and the ui being hidden")]
        [SerializeField]
        protected float m_HideAfterFinishedDelay;

        [Tooltip("The type of transition that will be applied to the ui when the state changes")]
        [SerializeField]
        protected UITransitionType m_Transition = UITransitionType.Simple;

        [Tooltip("The rect transform that will be faded or moved when transitioning this ui")]
        [SerializeField]
        protected RectTransform m_SimpleTransitionTarget;

        [Tooltip("The Simple Transition States for this ui")]
        [SerializeField]
        protected SimpleTransitionStates m_SimpleTransitionStates = new SimpleTransitionStates();

        [Tooltip("The Animated Transition States for this ui")]
        [SerializeField]
        protected AnimatedTransitionStates m_AnimatedTransitionStates = new AnimatedTransitionStates();

        [Tooltip("Called when the ui has finished its show transition")]
        [SerializeField]
        protected UnityEvent m_TransitionInCompleted = new UnityEvent();

        [Tooltip("Called when the ui has started its hide transition")]
        [SerializeField]
        protected UnityEvent m_TransitionOutStarted = new UnityEvent();

        [SerializeField]
        protected UnityEvent m_OnFinished = new UnityEvent();

        protected UnityEvent m_OnShown = new UnityEvent();

        protected UnityEvent m_OnHidden = new UnityEvent();

        protected UnityEvent m_OnNextShown = new UnityEvent();

        protected UnityEvent m_OnNextHidden = new UnityEvent();

        protected UnityEvent m_OnNextFinished = new UnityEvent();

        protected GameObject m_previouslySelectedGameObject;

        protected bool m_parentIsBlockingUserInput;

        protected bool m_blockedUserInput;

        protected Dictionary<Selectable, bool> m_childSelectableEnabledStates;

        protected Dictionary<Toggle, ToggleGroup> m_childSelectableToggleGroups;

        private Dictionary<string, Tween> m_delayTweens = new Dictionary<string, Tween>();

        private string[] m_currentChainedTransitions;

        private UITransitionSubState m_currentTransitionSubState;

        private Canvas m_uiCanvas;

        private CanvasGroup m_uiCanvasGroup;

        private Animator m_uiAnimator;

        public bool HideAfterFinished
        {
            get
            {
                return m_HideAfterFinished;
            }
            set
            {
                m_HideAfterFinished = value;
            }
        }

        public bool DisableOnHidden
        {
            get
            {
                return m_DisableOnHidden;
            }
            set
            {
                m_DisableOnHidden = value;
            }
        }

        public float HideAfterFinishedDelay
        {
            get
            {
                return m_HideAfterFinishedDelay;
            }
            set
            {
                m_HideAfterFinishedDelay = value;
            }
        }

        public UITransitionType Transition
        {
            get
            {
                return m_Transition;
            }
            set
            {
                m_Transition = value;
            }
        }

        public RectTransform SimpleTransitionTarget
        {
            get
            {
                return m_SimpleTransitionTarget;
            }
            set
            {
                m_SimpleTransitionTarget = value;
            }
        }

        public SimpleTransitionStates SimpleTransitionStates
        {
            get
            {
                return m_SimpleTransitionStates;
            }
            set
            {
                m_SimpleTransitionStates = value;
            }
        }

        public AnimatedTransitionStates AnimatedTransitionStates
        {
            get
            {
                return m_AnimatedTransitionStates;
            }
            set
            {
                m_AnimatedTransitionStates = value;
            }
        }

        public UnityEvent TransitionInCompleted
        {
            get
            {
                return m_TransitionInCompleted;
            }
        }

        public UnityEvent TransitionOutStarted
        {
            get
            {
                return m_TransitionOutStarted;
            }
        }

        public UnityEvent OnShown
        {
            get
            {
                return m_OnShown;
            }
        }

        public UnityEvent OnHidden
        {
            get
            {
                return m_OnHidden;
            }
        }

        public UnityEvent OnFinished
        {
            get
            {
                return m_OnFinished;
            }
        }

        public UnityEvent OnNextShown
        {
            get
            {
                return m_OnNextShown;
            }
        }

        public UnityEvent OnNextHidden
        {
            get
            {
                return m_OnNextHidden;
            }
        }

        public UnityEvent OnNextFinished
        {
            get
            {
                return m_OnNextFinished;
            }
        }

        public UITransitionSubState CurrentTransitionSubState
        {
            get
            {
                return m_currentTransitionSubState;
            }
            private set
            {
                m_currentTransitionSubState = value;
            }
        }

        public bool IsDoingShowTransition
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingShowTransition;
            }
        }

        public bool IsFullyShown
        {
            get
            {
                return !IsDoingShowTransition && !IsDoingHideTransition && DoesNotNeedToTransition(nameof(UITransitionStateType.Show)) && gameObject.activeSelf;
            }
        }

        public bool IsDoingShowTransitionOrFullyShown
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingShowTransition
                    || IsFullyShown;
            }
        }

        public bool IsDoingHideTransition
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingHideTransition;
            }
        }

        public bool IsFullyHidden
        {
            get
            {
                return !IsDoingHideTransition && !IsDoingShowTransition && DoesNotNeedToTransition(nameof(UITransitionStateType.Hide));
            }
        }

        public bool IsDoingHideTransitionOrFullyHidden
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingHideTransition
                    || IsFullyHidden;
            }
        }

        public bool IsDoingFinishTransition
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingFinishTransition;
            }
        }

        public bool IsFullyFinished
        {
            get
            {
                return !IsDoingFinishTransition;
            }
        }

        public bool IsDoingFinishTransitionOrFullyFinished
        {
            get
            {
                return CurrentTransitionSubState == UITransitionSubState.IsDoingFinishTransition
                    || IsFullyFinished;
            }
        }

        public Canvas UICanvas
        {
            get
            {
                if (m_uiCanvas == null)
                {
                    m_uiCanvas = gameObject.GetComponentInChildren<Canvas>(true);
                }
                return m_uiCanvas;
            }
        }

        public Animator UIAnimator
        {
            get
            {
                if (m_uiAnimator == null)
                {
                    m_uiAnimator = gameObject.GetRequiredComponent<Animator>();
                }
                return m_uiAnimator;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_SimpleTransitionTarget == null)
            {
                m_SimpleTransitionTarget = gameObject.transform as RectTransform;
            }
            if (m_SimpleTransitionTarget == null)
            {
                m_SimpleTransitionTarget = GetComponentInChildren<RectTransform>(true);
            }

            SetupTransitions();
        }

        protected override void Awake()
        {
            base.Awake();

            CurrentTransitionSubState = UITransitionSubState.Idle; // First time this UI has been activated, so it is not in the process of showing or hiding

            if (Application.isPlaying)
            {
                SetupTransitions();
                CreateNeccessaryRuntimeTransitions();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (Application.isPlaying)
            {
                SetupNeccessaryRuntimeTransitions();
            }

            Canvas.ForceUpdateCanvases();

            CurrentTransitionSubState = UITransitionSubState.Idle;  // Something enabled this UI so it is no longer in the process of showing or hiding
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            CurrentTransitionSubState = UITransitionSubState.Idle;  // Something disabled this UI so it is no longer in the process of showing or hiding
        }

        protected virtual void Update()
        {
        }

        protected virtual void OnTransitionInCompleted()
        {
        }

        protected virtual void OnTransitionOutStarted()
        {
        }

        protected virtual void OnBeforeActivate()
        {
        }

        public virtual void RebuildUI()
        {
            foreach (UITransition transition in GetTransitions())
            {
                transition.Resume();
            }
        }

        protected void SetupTransitions()
        {
            List<string> defaultStates = GetDefaultTransitionStates();
            foreach (string state in defaultStates)
            {
                if (!string.IsNullOrEmpty(state))
                {
                    if (!m_SimpleTransitionStates.IsValidState(state))
                    {
                        m_SimpleTransitionStates[state] = null;
                    }
                    if (!m_AnimatedTransitionStates.IsValidState(state))
                    {
                        m_AnimatedTransitionStates[state] = null;
                    }
                }
            }
            foreach (string state in defaultStates)
            {
                if (!string.IsNullOrEmpty(state))
                {
                    AnimatedTransition transition = m_AnimatedTransitionStates[state];
                    if (transition != null)
                    {
                        string idleStateName = m_AnimatedTransitionStates.GetAnimatorStateName(nameof(UITransitionStateType.Idle));
                        if (!string.IsNullOrEmpty(idleStateName))
                        {
                            transition.SetIdleStateName(idleStateName);
                        }
                    }
                }
            }
            foreach (string state in m_SimpleTransitionStates.GetStates())
            {
                if (!defaultStates.Contains(state))
                {
                    m_SimpleTransitionStates.RemoveStates(state);
                }
            }
            foreach (string state in m_AnimatedTransitionStates.GetStates())
            {
                if (!defaultStates.Contains(state))
                {
                    m_AnimatedTransitionStates.RemoveStates(state);
                }
            }
        }

        protected void CreateNeccessaryRuntimeTransitions()
        {
            List<string> defaultStates = GetDefaultTransitionStates();

            foreach (string state in defaultStates)
            {
                CreateNeccessaryRuntimeTransition(state);
            }
        }

        protected void CreateNeccessaryRuntimeTransition(string state)
        {
            if (!string.IsNullOrEmpty(state))
            {
                if (m_SimpleTransitionStates[state] == null)
                {
                    m_SimpleTransitionStates[state] = CreateDefaultSimpleTransition(state);
                }
                else
                {
                    m_SimpleTransitionStates[state] = m_SimpleTransitionStates[state].Clone();
                }
                if (m_AnimatedTransitionStates[state] == null)
                {
                    m_AnimatedTransitionStates[state] = CreateDefaultAnimatedTransition(state);
                }
                else
                {
                    m_AnimatedTransitionStates[state] = m_AnimatedTransitionStates[state].Clone();
                }
            }
        }

        protected void SetupNeccessaryRuntimeTransitions()
        {
            List<string> defaultStates = GetDefaultTransitionStates();

            foreach (string state in defaultStates)
            {
                if (!string.IsNullOrEmpty(state))
                {
                    if (m_SimpleTransitionStates[state] != null)
                    {
                        SetupDefaultSimpleTransition(state, m_SimpleTransitionStates[state]);
                    }
                }
            }
            foreach (string state in defaultStates)
            {
                if (!string.IsNullOrEmpty(state))
                {
                    if (m_AnimatedTransitionStates[state] != null)
                    {
                        SetupDefaultAnimatedTransition(state, m_AnimatedTransitionStates[state]);
                    }
                }
            }
        }

        protected virtual List<string> GetDefaultTransitionStates()
        {
            List<string> states = new List<string>();
            states.AddRange(Enum.GetNames(typeof(UITransitionStateType)));
            return states;
        }

        protected virtual SimpleTransition CreateDefaultSimpleTransition(string state)
        {
            if (state == nameof(UITransitionStateType.Idle))
            {
                return UITransition.CreateTransition<SimpleIdleTransition>(state);
            }
            else if (state == nameof(UITransitionStateType.Show))
            {
                return UITransition.CreateTransition<SimpleShowTransition>(state);
            }
            else if (state == nameof(UITransitionStateType.Hide))
            {
                return UITransition.CreateTransition<SimpleHideTransition>(state);
            }
            else if (state == nameof(UITransitionStateType.Finish))
            {
                return UITransition.CreateTransition<SimpleFinishTransition>(state);
            }
            return null;
        }

        protected AnimatedTransition CreateDefaultAnimatedTransition(string state)
        {
            return UITransition.CreateTransition<AnimatedTransition>(state);
        }

        protected virtual SimpleTransition SetupDefaultSimpleTransition(string state, SimpleTransition transition)
        {
            SimpleComponentTransition<RectTransform> rectTransformTransition = transition as SimpleComponentTransition<RectTransform>;
            if (rectTransformTransition != null)
            {
                if (m_SimpleTransitionTarget != null)
                {
                    rectTransformTransition.Target = m_SimpleTransitionTarget;
                }
            }
            SimpleComponentTransition<CanvasGroup> canvasGroupTransition = transition as SimpleComponentTransition<CanvasGroup>;
            if (canvasGroupTransition != null)
            {
                if (m_SimpleTransitionTarget != null)
                {
                    CanvasGroup canvasGroup = m_SimpleTransitionTarget.GetComponent<CanvasGroup>();
                    if (canvasGroup != null)
                    {
                        canvasGroupTransition.Target = canvasGroup;
                    }
                }
            }
            SimpleShowTransition simpleShowTransition = transition as SimpleShowTransition;
            if (simpleShowTransition != null)
            {
                return simpleShowTransition.SetFadeTarget(m_SimpleTransitionTarget.GetRequiredComponent<CanvasGroup>()).SetMoveTarget(m_SimpleTransitionTarget);
            }
            SimpleHideTransition simpleHideTransition = transition as SimpleHideTransition;
            if (simpleHideTransition != null)
            {
                return simpleHideTransition.SetFadeTarget(m_SimpleTransitionTarget.GetRequiredComponent<CanvasGroup>()).SetMoveTarget(m_SimpleTransitionTarget);
            }
            return transition;
        }

        protected AnimatedTransition SetupDefaultAnimatedTransition(string state, AnimatedTransition transition)
        {
            return transition.SetMonoBehaviour(this).SetAnimator(UIAnimator).SetTransitionStateName(state);
        }

        protected void TransitionToState(string state, float transitionDelay, bool instantly, bool actionOnFinished, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            m_currentChainedTransitions = new string[] { state };
            if (transitionDelay > 0) // Do transition after a delay
            {
                m_delayTweens[state] = DOVirtual.DelayedCall(transitionDelay, () => TransitionInternal(state, instantly, actionOnFinished, onKillOrComplete, onComplete));
            }
            else // Do transition
            {
                TransitionInternal(state, instantly, actionOnFinished, onKillOrComplete, onComplete);
            }
        }

        protected void TransitionToStates(string[] states, float transitionDelay, float stateChangeDelay, bool instantly, bool actionOnFinished, UnityAction onComplete = null)
        {
            m_currentChainedTransitions = states;
            TransitionToStatesRecursively(-1, states, transitionDelay, stateChangeDelay, instantly, actionOnFinished, null, onComplete);
        }

        private void TransitionToStatesRecursively(int index, string[] states, float transitionDelay, float stateChangeDelay, bool instantly, bool actionOnFinished, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            index++;

            float delay = transitionDelay; // use the transition delay for first state
            if (index > 0)
            {
                delay = stateChangeDelay; // use state change delay between each state.
            }

            if (index < states.Length)
            {
                TransitionToState(states[index], delay, instantly, actionOnFinished, null, () => TransitionToStatesRecursively(index, states, transitionDelay, stateChangeDelay, instantly, actionOnFinished, onKillOrComplete, onComplete));
            }
        }

        protected virtual void TransitionInternal(string state, bool instantly, bool actionOnFinished, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            if (state == nameof(UITransitionStateType.Show))
            {
                ShowInternal(instantly, onKillOrComplete, onComplete);
            }
            else if (state == nameof(UITransitionStateType.Hide))
            {
                HideInternal(instantly, actionOnFinished, onKillOrComplete, onComplete);
            }
            else if (state == nameof(UITransitionStateType.Finish))
            {
                FinishInternal(instantly, actionOnFinished, onKillOrComplete, onComplete);
            }
        }

        public virtual void KillAllOtherActiveTransitions(string currentState, UnityAction onKill = null)
        {
            bool killHandled = false;
            TransitionStates transitionStates = GetTransitionStates();
            if (transitionStates != null)
            {
                foreach (UITransition transition in transitionStates.GetActiveTransitions())
                {
                    if (currentState != transition.Key)
                    {
                        transition.Kill(onKill);
                        killHandled = true;
                    }
                }
            }
            foreach (string state in m_currentChainedTransitions)
            {
                if (currentState != state)
                {
                    if (m_delayTweens.TryGetValue(state, out Tween delayTween))
                    {
                        if (delayTween != null && delayTween.IsPlaying())
                        {
                            delayTween.Kill();
                        }
                    }
                }
            }
            if (!killHandled)
            {
                onKill?.Invoke();
            }
        }

        protected UITransition DoTransition(string state, bool instantly)
        {
            UITransition transition = GetTransition(state);

            if (transition != null)
            {
                if (instantly)
                {
                    transition.SkipToEnd();
                }
                else
                {
                    transition.Play();
                }
            }

            return transition;
        }

        public UITransition GetTransition(string state)
        {
            TransitionStates transitionStates = GetTransitionStates();
            if (transitionStates != null)
            {
                UITransition transition = transitionStates[state];
                if (transition == null)
                {
                    CreateNeccessaryRuntimeTransition(state);
                    transition = transitionStates[state];
                }
                return transition;
            }
            return null;
        }

        public TransitionStates GetTransitionStates()
        {
            if (m_Transition == UITransitionType.Simple)
            {
                return m_SimpleTransitionStates;
            }
            else if (m_Transition == UITransitionType.Animation)
            {
                return m_AnimatedTransitionStates;
            }
            return null;
        }

        public UITransition[] GetTransitions()
        {
            UITransition[] transitions = null;

            if (m_Transition == UITransitionType.Simple)
            {
                return m_SimpleTransitionStates.GetTransitions();
            }
            else if (m_Transition == UITransitionType.Animation)
            {
                return m_AnimatedTransitionStates.GetTransitions();
            }

            return transitions;
        }

        protected bool DoesNotNeedToTransition(string state)
        {
            UITransition transition = GetTransition(state);

            if (transition != null)
            {
                SimpleTransition simpleTransition = transition as SimpleTransition;
                if (simpleTransition != null)
                {
                    return simpleTransition.DoesNotNeedToTransition();
                }
            }

            return false;
        }

        public void Raise(float duration, UnityAction onComplete = null)
        {
            Raise(duration, m_DisableOnHidden, onComplete);
        }

        public void Raise(float duration, bool disableOnHidden, UnityAction onComplete = null)
        {
            TransitionToStates(new string[] { nameof(UITransitionStateType.Show), nameof(UITransitionStateType.Hide) }, 0, duration, false, disableOnHidden, onComplete);
        }

        public void Show()
        {
            Show(null);
        }

        public void Show(UnityAction onComplete)
        {
            Show(0f, onComplete);
        }

        public void Show(float transitionDelay, UnityAction onComplete = null)
        {
            if (!Application.isPlaying)
            {
                gameObject.SetActive(true);
            }

            SetPrimaryActive(this);

            TransitionToState(nameof(UITransitionStateType.Show), transitionDelay, false, false, onComplete);
        }

        public void ShowInstantly(UnityAction onComplete = null)
        {
            if (!Application.isPlaying)
            {
                gameObject.SetActive(true);
            }

            SetPrimaryActive(this);

            TransitionToState(nameof(UITransitionStateType.Show), 0, true, false, onComplete);
        }

        private void ShowInternal(bool instantly, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            if (!Application.isPlaying || IsFullyShown) // Just activate without doing Show transition and trigger onComplete
            {
                if (!IsPartOfPrefabAsset()) // Don't change prefab assets
                {
                    gameObject.SetActive(true);
                }

                CurrentTransitionSubState = UITransitionSubState.Idle;  // We are not doing a Show transition

                onKillOrComplete?.Invoke();
            }
            else if (IsDoingShowTransition) // UI is already in the process of being shown, just trigger onComplete when it's done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextShown.AddPersistentListener(onKillOrComplete);
                }
            }
            else // Activate UI, do Show transition, and trigger onComplete when it is done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextShown.AddPersistentListener(onKillOrComplete);
                }

                OnBeforeActivate();

                BlockUserInput();

                gameObject.SetActive(true);

                KillAllOtherActiveTransitions(nameof(UITransitionStateType.Show), () => ShowUI(instantly, OnStoppedShowing, () => { OnStoppedShowing(); onComplete?.Invoke(); }));

                CurrentTransitionSubState = UITransitionSubState.IsDoingShowTransition;  // Started showing
            }
        }

        private void OnStoppedShowing()
        {
            UnBlockUserInput();

            CurrentTransitionSubState = UITransitionSubState.Idle; // Done showing

            m_TransitionInCompleted.Invoke();
            OnTransitionInCompleted();

            m_OnShown.Invoke();

            m_OnNextShown.Invoke();
            m_OnNextShown.RemoveAllPersistentAndNonPersistentListeners();
        }

        protected virtual void ShowUI(bool instantly, UnityAction onKill, UnityAction onComplete)
        {
            UITransition transition = DoTransition(nameof(UITransitionStateType.Show), instantly);
            if (transition != null)
            {
                transition.SetOnKill(onKill).SetOnComplete(onComplete);
            }
            else
            {
                onComplete?.Invoke();
            }
        }

        public void Hide()
        {
            Hide(null);
        }

        public void Hide(UnityAction onComplete)
        {
            Hide(m_DisableOnHidden, onComplete);
        }

        public void Hide(bool disableOnHidden, UnityAction onComplete = null)
        {
            Hide(0f, disableOnHidden, onComplete);
        }

        public void Hide(float transitionDelay, UnityAction onComplete = null)
        {
            if (!Application.isPlaying)
            {
                if (m_DisableOnHidden)
                {
                    gameObject.SetActive(false);
                }
            }

            TransitionToState(nameof(UITransitionStateType.Hide), transitionDelay, false, m_DisableOnHidden, onComplete);
        }

        public void Hide(float transitionDelay, bool disableOnHidden, UnityAction onComplete = null)
        {
            if (!Application.isPlaying)
            {
                if (disableOnHidden)
                {
                    gameObject.SetActive(false);
                }
            }

            TransitionToState(nameof(UITransitionStateType.Hide), transitionDelay, false, disableOnHidden, onComplete);
        }

        public void HideInstantly(bool disableOnHidden, UnityAction onComplete = null)
        {
            TransitionToState(nameof(UITransitionStateType.Hide), 0f, true, disableOnHidden, onComplete);
        }

        private void HideInternal(bool instantly, bool disableOnHidden, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            if (!Application.isPlaying || IsFullyHidden)  // Just (optionally) dectivate without doing Hide transition and trigger onComplete
            {
                if (!IsPartOfPrefabAsset()) // Don't change prefab assets
                {
                    if (disableOnHidden)
                    {
                        gameObject.SetActive(false);
                    }
                }

                CurrentTransitionSubState = UITransitionSubState.Idle;  // We are not doing a Hide transition

                onKillOrComplete?.Invoke();
            }
            else if (IsDoingHideTransition) // UI is already in the process of being hidden, just trigger onComplete when it's done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextHidden.AddPersistentListener(onKillOrComplete);
                }
            }
            else // Do Hide transition, (optionally) deactivate UI, and trigger onComplete when it is done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextHidden.AddPersistentListener(onKillOrComplete);
                }

                BlockUserInput();

                m_TransitionOutStarted.Invoke();
                OnTransitionOutStarted();

                KillAllOtherActiveTransitions(nameof(UITransitionStateType.Hide), () => HideUI(instantly, disableOnHidden, () => OnStoppedHiding(disableOnHidden), () => { OnStoppedHiding(disableOnHidden); onComplete?.Invoke(); }));

                CurrentTransitionSubState = UITransitionSubState.IsDoingHideTransition; // Started hiding
            }
        }

        private void OnStoppedHiding(bool disableOnHidden)
        {
            CurrentTransitionSubState = UITransitionSubState.Idle; // Done hiding

            if (disableOnHidden)
            {
                this.gameObject.SetActive(false);
                UnBlockUserInput();
            }

            m_OnHidden.Invoke();

            m_OnNextHidden.Invoke();
            m_OnNextHidden.RemoveAllPersistentAndNonPersistentListeners();
        }

        protected virtual void HideUI(bool instantly, bool disableOnHidden, UnityAction onKill, UnityAction onComplete)
        {
            UITransition transition = DoTransition(nameof(UITransitionStateType.Hide), instantly);
            if (transition != null)
            {
                transition.SetOnKill(onKill).SetOnComplete(onComplete);
            }
            else
            {
                onComplete?.Invoke();
            }
        }

        public void Finish()
        {
            Finish(null);
        }

        public void Finish(UnityAction onComplete)
        {
            Finish(0f, m_HideAfterFinished, onComplete);
        }

        public void Finish(bool hideAfterFinished, UnityAction onComplete)
        {
            Finish(0f, hideAfterFinished, onComplete);
        }

        public void Finish(float transitionDelay, bool hideAfterFinished, UnityAction onComplete = null)
        {
            bool instantly = false;
            if (hideAfterFinished)
            {
                TransitionToStates(new string[] { nameof(UITransitionStateType.Finish), nameof(UITransitionStateType.Hide) }, transitionDelay, m_HideAfterFinishedDelay, instantly, m_DisableOnHidden, onComplete);
            }
            else
            {
                TransitionToState(nameof(UITransitionStateType.Finish), transitionDelay, instantly, m_DisableOnHidden, onComplete);
            }
        }

        public void FinishInstantly(bool hideAfterFinished, UnityAction onComplete = null)
        {
            float transitionDelay = 0f;
            bool instantly = true;
            if (hideAfterFinished)
            {
                TransitionToStates(new string[] { nameof(UITransitionStateType.Finish), nameof(UITransitionStateType.Hide) }, transitionDelay, m_HideAfterFinishedDelay, instantly, m_DisableOnHidden, onComplete);
            }
            else
            {
                TransitionToState(nameof(UITransitionStateType.Finish), transitionDelay, instantly, m_DisableOnHidden, onComplete);
            }
        }

        private void FinishInternal(bool instantly, bool hideAfterFinished, UnityAction onKillOrComplete = null, UnityAction onComplete = null)
        {
            if (!Application.isPlaying || IsFullyFinished)  // Just (optionally) dectivate without doing Finish transition and trigger onComplete
            {
                CurrentTransitionSubState = UITransitionSubState.Idle;  // We are not doing a Finish transition

                OnStoppedFinishing();
                onKillOrComplete?.Invoke();
            }
            else if (IsDoingFinishTransition) // UI is already in the process of being finished, just trigger onComplete when it's done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextFinished.AddPersistentListener(onKillOrComplete);
                }
            }
            else // Do Finish transition, and trigger onComplete when it is done.
            {
                if (onKillOrComplete != null)
                {
                    m_OnNextFinished.AddPersistentListener(onKillOrComplete);
                }

                BlockUserInput();

                KillAllOtherActiveTransitions(nameof(UITransitionStateType.Finish), () => FinishUI(instantly, OnStoppedFinishing, () => { OnStoppedFinishing(); onComplete?.Invoke(); }));

                CurrentTransitionSubState = UITransitionSubState.IsDoingFinishTransition; // Started finishing
            }
        }

        private void OnStoppedFinishing()
        {
            CurrentTransitionSubState = UITransitionSubState.Idle; // Done finishing

            UnBlockUserInput();

            m_OnFinished.Invoke();

            m_OnNextFinished.Invoke();
            m_OnNextFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        protected virtual void FinishUI(bool instantly, UnityAction onKill, UnityAction onComplete)
        {
            UITransition transition = DoTransition(nameof(UITransitionStateType.Finish), instantly);
            if (transition != null)
            {
                transition.SetOnKill(onKill).SetOnComplete(onComplete);
            }
            else
            {
                onComplete?.Invoke();
            }
        }

        public void BlockUserInput()
        {
            if (!m_blockedUserInput && !m_parentIsBlockingUserInput)
            {
                m_blockedUserInput = true;
                UIConstruct[] uiChildren = GetComponentsInChildren<UIConstruct>(true);
                foreach (UIConstruct uiConstruct in uiChildren)
                {
                    if (uiConstruct != this)
                    {
                        uiConstruct.m_parentIsBlockingUserInput = true;
                    }
                }
                Selectable[] selectableChildren = GetComponentsInChildren<Selectable>(true);
                m_childSelectableToggleGroups = new Dictionary<Toggle, ToggleGroup>();
                for (int i = 0; i < selectableChildren.Length; i++)
                {
                    Selectable selectable = selectableChildren[i];
                    Toggle toggle = selectable as Toggle;
                    if (toggle != null)
                    {
                        m_childSelectableToggleGroups[toggle] = toggle.group;
                        toggle.group = null;
                    }
                }
                m_childSelectableEnabledStates = new Dictionary<Selectable, bool>();
                for (int i = 0; i < selectableChildren.Length; i++)
                {
                    Selectable selectable = selectableChildren[i];
                    m_childSelectableEnabledStates[selectable] = selectable.enabled;
                    selectable.enabled = false;
                }
            }
        }

        public void UnBlockUserInput()
        {
            if (m_blockedUserInput && !m_parentIsBlockingUserInput)
            {
                m_blockedUserInput = false;
                UIConstruct[] uiChildren = GetComponentsInChildren<UIConstruct>(true);
                foreach (UIConstruct uiConstruct in uiChildren)
                {
                    if (uiConstruct != this)
                    {
                        uiConstruct.m_parentIsBlockingUserInput = false;
                    }
                }
                if (m_childSelectableEnabledStates != null)
                {
                    foreach (KeyValuePair<Selectable, bool> kvp in m_childSelectableEnabledStates)
                    {
                        Selectable selectable = kvp.Key;
                        selectable.enabled = kvp.Value;
                    }
                    foreach (KeyValuePair<Toggle, ToggleGroup> kvp in m_childSelectableToggleGroups)
                    {
                        Toggle toggle = kvp.Key;
                        toggle.SetToggleGroupWithoutNotify(kvp.Value, true);
                    }
                }
            }
        }

        protected void SavePreviousSelection()
        {
            if (EventSystem.current != null)
            {
                m_previouslySelectedGameObject = EventSystem.current.currentSelectedGameObject;
            }
        }

        protected void RestorePreviousSelection()
        {
            if (EventSystem.current != null)
            {
                if (m_previouslySelectedGameObject != null && m_previouslySelectedGameObject.activeInHierarchy)
                {
                    EventSystem.current.SetSelectedGameObject(m_previouslySelectedGameObject);
                }
            }
        }

        public virtual bool HasSelection()
        {
            if (EventSystem.current != null)
            {
                GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
                if (selectedGameObject != null)
                {
                    return GetComponentsInChildren<Selectable>().Any(x => x.gameObject == selectedGameObject);
                }
            }
            return false;
        }

        public virtual bool ContainsSelectable(Selectable selectable, bool includeInactive)
        {
            return GetComponentsInChildren<Selectable>(includeInactive).Contains(selectable);
        }

        public IEnumerator PerformActionAtEndOfFrame(UnityAction onComplete)
        {
            yield return new WaitForEndOfFrame();

            onComplete();
        }

        public IEnumerator PerformActionOnNextFrame(UnityAction onComplete)
        {
            yield return null;

            onComplete();
        }

        public IEnumerator PerformActionAtEndOfNextFrame(UnityAction onComplete)
        {
            yield return null;

            yield return new WaitForEndOfFrame();

            onComplete();
        }

        public IEnumerator PerformActionOnFrameDelay(int frames, UnityAction onComplete)
        {
            yield return new WaitForFrames(frames);

            onComplete();
        }

        public IEnumerator PerformActionAtEndOfFrameDelay(int frames, UnityAction onComplete)
        {
            yield return new WaitForFrames(frames);

            yield return new WaitForEndOfFrame();

            onComplete();
        }

        protected IEnumerator PerformActionAfterDelay(float delay, UnityAction onComplete)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            onComplete?.Invoke();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_SimpleTransitionTarget))
            {
                if (m_Transition != UITransitionType.Simple)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_SimpleTransitionStates))
            {
                if (m_Transition != UITransitionType.Simple)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AnimatedTransitionStates))
            {
                if (m_Transition != UITransitionType.Animation)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return int.MaxValue;
            }

            return base.GetPropertyOrder(propertyPath);
        }

        public override int GetPropertyIndentLevelOffset(string propertyPath)
        {
            if (propertyPath == nameof(m_SimpleTransitionTarget))
            {
                return 1;
            }
            if (propertyPath == nameof(m_SimpleTransitionStates))
            {
                return 1;
            }
            if (propertyPath == nameof(m_AnimatedTransitionStates))
            {
                return 1;
            }

            return base.GetPropertyIndentLevelOffset(propertyPath);
        }
    }
}
