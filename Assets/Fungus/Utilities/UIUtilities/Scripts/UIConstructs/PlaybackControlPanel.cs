﻿using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [AddComponentMenu("UI/Playback/Playback Control Panel")]
    public class PlaybackControlPanel : UIConstruct
    {
        [Tooltip("The button that can be used to replay playback")]
        [SerializeField]
        protected Button m_ReplayButton;

        [Tooltip("The button that can be used to play playback")]
        [SerializeField]
        protected Button m_PlayButton;

        [Tooltip("The button that can be used to pause playback")]
        [SerializeField]
        protected Button m_PauseButton;

        [Tooltip("The button that can be used to step forward playback a few seconds")]
        [SerializeField]
        protected Button m_StepForwardButton;

        [Tooltip("The button that can be used to step backward playback a few seconds")]
        [SerializeField]
        protected Button m_StepBackwardButton;

        [Tooltip("The button that can be used to skip playback entirely")]
        [SerializeField]
        protected Button m_SkipButton;

        [Tooltip("The button that can be used to stop playback")]
        [SerializeField]
        protected Button m_StopButton;

        [Tooltip("The slider whose fill value will reflect the current time of playback and can be used to set the current time of playback")]
        [SerializeField]
        protected Slider m_ProgressBar;

        [Tooltip("The text which will display the current playback time and length of playback")]
        [SerializeField]
        protected Text m_ProgressText;

        [Tooltip("The slider whose fill value will reflect the current volume and can be used to set the current volume")]
        [SerializeField]
        protected Slider m_VolumeBar;

        [Tooltip("The toggle whose value will reflect the current mute state and can be used to set the current mute state")]
        [SerializeField]
        protected Toggle m_VolumeMuteToggle;

        public Button ReplayButton
        {
            get
            {
                return m_ReplayButton;
            }
            set
            {
                m_ReplayButton = value;
            }
        }

        public Button PlayButton
        {
            get
            {
                return m_PlayButton;
            }
            set
            {
                m_PlayButton = value;
            }
        }

        public Button PauseButton
        {
            get
            {
                return m_PauseButton;
            }
            set
            {
                m_PauseButton = value;
            }
        }

        public Button StepForwardButton
        {
            get
            {
                return m_StepForwardButton;
            }
            set
            {
                m_StepForwardButton = value;
            }
        }

        public Button StepBackwardButton
        {
            get
            {
                return m_StepBackwardButton;
            }
            set
            {
                m_StepBackwardButton = value;
            }
        }

        public Button SkipButton
        {
            get
            {
                return m_SkipButton;
            }
            set
            {
                m_SkipButton = value;
            }
        }

        public Button StopButton
        {
            get
            {
                return m_StopButton;
            }
            set
            {
                m_StopButton = value;
            }
        }

        public Slider ProgressBar
        {
            get
            {
                return m_ProgressBar;
            }
            set
            {
                m_ProgressBar = value;
            }
        }

        public Text ProgressText
        {
            get
            {
                return m_ProgressText;
            }
            set
            {
                m_ProgressText = value;
            }
        }

        public Slider VolumeBar
        {
            get
            {
                return m_VolumeBar;
            }
            set
            {
                m_VolumeBar = value;
            }
        }

        public Toggle VolumeMuteToggle
        {
            get
            {
                return m_VolumeMuteToggle;
            }
            set
            {
                m_VolumeMuteToggle = value;
            }
        }
    }
}
