using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ImprobableStudios.UIUtilities
{

    public class TextTagParser
    {
        public const string HELP_INFO = 
            "\t{b} Bold Text {/b}\n" +
            "\t{i} Italic Text {/i}\n" +
            "\t{size=80} Text Size{/size}\n" +
            "\t{color=red}, {color=#add8e6ff} Color Text (color){/color}\n" +
            "\t{alpha=0.5} Transparent Text (0-1){/alpha}\n" +
            "\n" +
            "\t{cps}, {cps=60}, {cps=*0.5}  Writing speed (chars per sec){/cps}\n" +
            "\t{w}, {w=0.5} Wait (seconds)\n" +
            "\t{wi} Wait for input\n" +
            "\t{wc} Wait for input and clear\n" +
            "\t{wp}, {wp=0.5} Wait on punctuation (seconds){/wp}\n" +
            "\t{c} Clear\n" +
            "\t{x} Exit, advance to the next command without waiting for input\n" +
            "\n" +
            "\t{vpunch=10,0.5} Vertically punch screen (intensity,time)\n" +
            "\t{hpunch=10,0.5} Horizontally punch screen (intensity,time)\n" +
            "\t{shake=10,0.5} Shake screen (intensity,time)\n" +
            "\t{flash=0.5} Flash screen (duration)\n" +
            "\n" +
            "\t{audio=AudioObjectName} Play Audio Once\n" +
            "\t{audioloop=AudioObjectName} Play Audio Loop\n" +
            "\t{audiopause=AudioObjectName} Pause Audio\n" +
            "\t{audiostop=AudioObjectName} Stop Audio\n" +
            "\n" +
            "\t{$VarName} Substitute variable";

        public static readonly Regex BALANCED_BRACKET_REGEX = new Regex(@"
                                {                      # First '{'
                                    (?:                 
                                    [^{}]               # Match all non-braces
                                    |
                                    (?<open> { )       # Match '{', and capture into 'open'
                                    |
                                    (?<-open> } )      # Match '}', and delete the 'open' capture
                                    )+
                                    (?(open)(?!))       # Fails if 'open' stack isn't empty!

                                }                      # Last '}'
                                ", RegexOptions.IgnorePatternWhitespace);

        public enum TokenType
        {
            Invalid,
            Words,                    // A string of words
            BoldStart,                // {b}
            BoldEnd,                // {/b}
            ItalicStart,            // {i}
            ItalicEnd,                // {/i}
            SizeStart,                // {size=80}
            SizeEnd,                // {/size}
            ColorStart,                // {color=red}
            ColorEnd,                // {/color}
            AlphaStart,                // {alpha=0.5}
            AlphaEnd,                // {/alpha}
            Wait,                     // {w}, {w=0.5}
            WaitForInputNoClear,     // {wi}
            WaitForInputAndClear,     // {wc}
            WaitOnPunctuationStart, // {wp}, {wp=0.5}
            WaitOnPunctuationEnd,   // {/wp}
            Clear,                     // {c}
            SpeedStart,             // {cps}, {cps=60}, {cps=*0.5}
            SpeedEnd,                 // {/cps}
            Exit,                     // {x}
            VerticalPunch,           // {vpunch=0.5}
            HorizontalPunch,        // {hpunch=0.5}
            Shake,                    // {shake=0.5}
            Flash,                    // {flash=0.5}
            Audio,                    // {audio=Sound}
            AudioLoop,                // {audioloop=Sound}
            AudioPause,                // {audiopause=Sound}
            AudioStop                // {audiostop=Sound}
        }

        public class Token
        {
            public TokenType m_Type = TokenType.Invalid;
            public List<string> m_ParamList = new List<string>();

            public override string ToString()
            {
                return m_Type + "(" + string.Join(",", m_ParamList) + ")";
            }
        }
        
        public virtual List<Token> Tokenize(string storyText)
        {
            List<Token> tokens = new List<Token>();

            storyText = ReplaceCustomTags(storyText);

            Match m = BALANCED_BRACKET_REGEX.Match(storyText);   // m is the first match

            int position = 0;
            while (m.Success)
            {
                // Get bit leading up to tag
                string preText = storyText.Substring(position, m.Index - position);
                string tagText = m.Value;

                if (preText != "")
                {
                    AddWordsToken(tokens, preText);
                }
                AddTagToken(tokens, tagText);

                position = m.Index + tagText.Length;
                m = m.NextMatch();
            }

            if (position < storyText.Length)
            {
                string postText = storyText.Substring(position, storyText.Length - position);
                if (postText.Length > 0)
                {
                    AddWordsToken(tokens, postText);
                }
            }

            // Remove all leading whitespace & newlines after a {c} or {wc} tag
            // These characters are usually added for legibility when editing, but are not 
            // desireable when viewing the text in game.
            bool trimLeading = false;
            foreach (Token token in tokens)
            {
                if (trimLeading &&
                    token.m_Type == TokenType.Words)
                {
                    token.m_ParamList[0] = token.m_ParamList[0].TrimStart(' ', '\t', '\r', '\n');
                }

                if (token.m_Type == TokenType.Clear ||
                    token.m_Type == TokenType.WaitForInputAndClear)
                {
                    trimLeading = true;
                }
                else
                {
                    trimLeading = false;
                }
            }

            return tokens;
        }

        protected virtual void AddWordsToken(List<Token> tokenList, string words)
        {
            Token token = new Token();
            token.m_Type = TokenType.Words;
            token.m_ParamList = new List<string>();
            token.m_ParamList.Add(words);
            tokenList.Add(token);
        }

        protected virtual void AddTagToken(List<Token> tokenList, string tagText)
        {
            Token token = GetTagToken(tagText);

            if (token.m_Type != TokenType.Invalid)
            {
                tokenList.Add(token);
            }
        }

        protected static List<string> ExtractParameters(string input)
        {
            List<string> paramsList = new List<string>();
            int index = input.IndexOf('=');
            if (index == -1)
            {
                return paramsList;
            }

            string paramsStr = input.Substring(index + 1);
            var splits = paramsStr.Split(',');
            foreach (var p in splits)
            {
                paramsList.Add(p.Trim());
            }
            return paramsList;
        }

        protected static string ReplaceCustomTags(string text)
        {
            foreach (CustomTag ct in WriterManager.Instance.CustomTags)
            {
                if (ct.TagStartSymbol != "" && ct.ReplaceTagStartWith != "")
                {
                    text = text.Replace(ct.TagStartSymbol, ct.ReplaceTagStartWith);
                }
                if (ct.TagEndSymbol != "" && ct.ReplaceTagEndWith != "")
                {
                    text = text.Replace(ct.TagEndSymbol, ct.ReplaceTagEndWith);
                }
            }
            return text;
        }

        public static Token GetTagToken(string tagText)
        {
            TokenType type = TokenType.Invalid;
            string tag = tagText.Substring(1, tagText.Length - 2);
            List<string> parameters = ExtractParameters(tag);

            if (tagText.Length >= 3 &&
                tagText.Substring(0, 1) == "{" &&
                tagText.Substring(tagText.Length - 1, 1) == "}")
            {
                if (tag == "b")
                {
                    type = TokenType.BoldStart;
                }
                else if (tag == "/b")
                {
                    type = TokenType.BoldEnd;
                }
                else if (tag == "i")
                {
                    type = TokenType.ItalicStart;
                }
                else if (tag == "/i")
                {
                    type = TokenType.ItalicEnd;
                }
                else if (tag.StartsWith("color=", StringComparison.Ordinal))
                {
                    type = TokenType.ColorStart;
                }
                else if (tag == "/color")
                {
                    type = TokenType.ColorEnd;
                }
                else if (tag.StartsWith("alpha=", StringComparison.Ordinal))
                {
                    type = TokenType.AlphaStart;
                }
                else if (tag == "/alpha")
                {
                    type = TokenType.AlphaEnd;
                }
                else if (tag == "wi")
                {
                    type = TokenType.WaitForInputNoClear;
                }
                if (tag == "wc")
                {
                    type = TokenType.WaitForInputAndClear;
                }
                else if (tag.StartsWith("wp=", StringComparison.Ordinal))
                {
                    type = TokenType.WaitOnPunctuationStart;
                }
                else if (tag == "wp")
                {
                    type = TokenType.WaitOnPunctuationStart;
                }
                else if (tag == "/wp")
                {
                    type = TokenType.WaitOnPunctuationEnd;
                }
                else if (tag.StartsWith("w=", StringComparison.Ordinal))
                {
                    type = TokenType.Wait;
                }
                else if (tag == "w")
                {
                    type = TokenType.Wait;
                }
                else if (tag == "c")
                {
                    type = TokenType.Clear;
                }
                else if (tag.StartsWith("cps=", StringComparison.Ordinal))
                {
                    type = TokenType.SpeedStart;
                }
                else if (tag == "cps")
                {
                    type = TokenType.SpeedStart;
                }
                else if (tag == "/cps")
                {
                    type = TokenType.SpeedEnd;
                }
                else if (tag == "x")
                {
                    type = TokenType.Exit;
                }
                else if (tag.StartsWith("vpunch", StringComparison.Ordinal) ||
                         tag.StartsWith("vpunch=", StringComparison.Ordinal))
                {
                    type = TokenType.VerticalPunch;
                }
                else if (tag.StartsWith("hpunch", StringComparison.Ordinal) ||
                         tag.StartsWith("hpunch=", StringComparison.Ordinal))
                {
                    type = TokenType.HorizontalPunch;
                }
                else if (tag.StartsWith("shake", StringComparison.Ordinal) ||
                         tag.StartsWith("shake=", StringComparison.Ordinal))
                {
                    type = TokenType.Shake;
                }
                else if (tag.StartsWith("flash", StringComparison.Ordinal) ||
                         tag.StartsWith("flash=", StringComparison.Ordinal))
                {
                    type = TokenType.Flash;
                }
                else if (tag.StartsWith("audio=", StringComparison.Ordinal))
                {
                    type = TokenType.Audio;
                }
                else if (tag.StartsWith("audioloop=", StringComparison.Ordinal))
                {
                    type = TokenType.AudioLoop;
                }
                else if (tag.StartsWith("audiopause=", StringComparison.Ordinal))
                {
                    type = TokenType.AudioPause;
                }
                else if (tag.StartsWith("audiostop=", StringComparison.Ordinal))
                {
                    type = TokenType.AudioStop;
                }
            }

            Token token = new Token();
            token.m_Type = type;
            token.m_ParamList = parameters;

            return token;
        }

        public static string[] FindInvalidTokens(string text)
        {
            List<string> tokenTexts = new List<string>();
            
            text = ReplaceCustomTags(text);

            Match m = BALANCED_BRACKET_REGEX.Match(text);   // m is the first match

            while (m.Success)
            {
                string tagText = m.Value;

                Token token = GetTagToken(tagText);

                if (token.m_Type == TokenType.Invalid)
                {
                    tokenTexts.Add(tagText);
                }

                m = m.NextMatch();
            }

            return tokenTexts.ToArray();
        }
    }

}