﻿using UnityEngine;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.UIUtilities
{

    public abstract class TransitionState
    {
        [SerializeField]
        protected string m_Key;

        public UITransition Transition
        {
            get
            {
                return GetTransition();
            }
            set
            {
                SetTransition(value);
            }
        }

        public string Key
        {
            get
            {
                return m_Key;
            }
        }

        public TransitionState(string key, UITransition transition)
        {
            SetTransition(transition);
            m_Key = key;
        }

        public abstract UITransition GetTransition();

        public abstract void SetTransition(UITransition uiTransition);

        public override string ToString()
        {
            return Key + " (" + GetType() + ")";
        }
    }

    public class TransitionState<T> : TransitionState where T : UITransition
    {
        [ObjectPopup("<Default>")]
        [SerializeField]
        protected T m_Transition;

        public new T Transition
        {
            get
            {
                return m_Transition;
            }
            set
            {
                m_Transition = value;
            }
        }

        public TransitionState(string key, T transition) : base(key, transition)
        {
        }

        public override UITransition GetTransition()
        {
            return m_Transition;
        }

        public override void SetTransition(UITransition uiTransition)
        {
            m_Transition = uiTransition as T;
        }
    }

}
