﻿using DG.Tweening;
using UnityEngine.Events;

namespace ImprobableStudios.UIUtilities
{

    public abstract class SimpleTransition : UITransition
    {
        protected Tween m_transitionTween;

        public Tween GetActiveTween()
        {
            return m_transitionTween;
        }

        public sealed override void Play()
        {
            m_transitionTween = PlayInternal();
            if (m_transitionTween != null)
            {
                m_transitionTween.OnComplete(InvokeOnComplete);
                m_transitionTween.OnKill(InvokeOnKill);
                m_transitionTween.OnUpdate(SaveTransitionState);
            }
        }

        public sealed override void SkipToStart(UnityAction onSkip = null)
        {
            SkipToStartInternal();
            onSkip?.Invoke();
        }

        public sealed override void SkipToEnd(UnityAction onSkip = null)
        {
            SkipToEndInternal();
            InvokeOnComplete();
            onSkip?.Invoke();
        }

        public abstract bool DoesNotNeedToTransition();

        protected abstract Tween PlayInternal();

        protected abstract void SkipToStartInternal();

        protected abstract void SkipToEndInternal();

        public sealed override bool IsDoingTransition()
        {
            return m_transitionTween != null && m_transitionTween.IsPlaying();
        }

        public sealed override void Complete(UnityAction onComplete = null)
        {
            if (m_transitionTween != null)
            {
                if (m_transitionTween.IsPlaying())
                {
                    m_transitionTween.Complete();
                }
            }
            else
            {
                if (!DoesNotNeedToTransition())
                {
                    SkipToEnd();
                }
            }
            onComplete?.Invoke();
        }

        public sealed override void Kill(UnityAction onKill = null)
        {
            if (m_transitionTween != null)
            {
                if (m_transitionTween.IsPlaying())
                {
                    m_transitionTween.Kill();
                }
            }
            else
            {
                if (!DoesNotNeedToTransition())
                {
                    InvokeOnKill();
                }
            }
            onKill?.Invoke();
        }

        public sealed override void Resume(UnityAction onResume = null)
        {
            if (m_transitionTween != null)
            {
                m_transitionTween.Goto(m_ElapsedTime, true);
            }
            else
            {
                SkipToEnd();
            }
            onResume?.Invoke();
        }

        protected sealed override void SaveTransitionState()
        {
            if (m_transitionTween != null)
            {
                m_ElapsedTime = m_transitionTween.Elapsed(true);
            }
        }
    }

}
