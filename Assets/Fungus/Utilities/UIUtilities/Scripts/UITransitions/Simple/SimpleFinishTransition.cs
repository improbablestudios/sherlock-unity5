﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Finish Transition", menuName = "UI/Transition/Simple/Finish", order = 0)]
    public class SimpleFinishTransition : SimpleComponentTransition<Image>
    {
        [SerializeField]
        protected Sprite m_DoingIcon;

        [SerializeField]
        protected Sprite m_DoneIcon;

        protected override Tween PlayInternal()
        {
            if (m_Target != null)
            {
                m_Target.sprite = m_DoneIcon;
            }
            return null;
        }

        protected override void SkipToEndInternal()
        {
            if (m_Target != null)
            {
                m_Target.sprite = m_DoneIcon;
            }
        }

        protected override void SkipToStartInternal()
        {
            if (m_Target != null)
            {
                m_Target.sprite = m_DoingIcon;
            }
        }

        public override bool DoesNotNeedToTransition()
        {
            if (m_Target != null)
            {
                return m_Target.sprite == m_DoneIcon;
            }
            return false;
        }
    }

}
