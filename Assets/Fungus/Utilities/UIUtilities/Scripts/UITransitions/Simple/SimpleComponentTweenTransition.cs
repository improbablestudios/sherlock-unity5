﻿using DG.Tweening;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public abstract class SimpleComponentTweenTransition<T> : SimpleComponentTransition<T> where T : Component
    {
        [SerializeField]
        protected float m_Duration = 0.2f;

        [SerializeField]
        protected bool m_SpeedBased = false;

        [SerializeField]
        protected Ease m_Ease = Ease.InOutQuad;

        public float Duration
        {
            get
            {
                return m_Duration;
            }
            set
            {
                m_Duration = value;
            }
        }

        public bool SpeedBased
        {
            get
            {
                return m_SpeedBased;
            }
            set
            {
                m_SpeedBased = value;
            }
        }

        public Ease Ease
        {
            get
            {
                return m_Ease;
            }
            set
            {
                m_Ease = value;
            }
        }

        protected sealed override Tween PlayInternal()
        {
            return GetTransitionTween().SetSpeedBased(m_SpeedBased).SetEase(m_Ease).SetTarget(m_Target);
        }

        protected abstract Tween GetTransitionTween();
    }

}
