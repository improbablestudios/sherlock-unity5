﻿using DG.Tweening;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public abstract class SimpleComponentTransition<T> : SimpleTransition where T : Component
    {
        [SerializeField]
        protected T m_Target;

        public T Target
        {
            get
            {
                return m_Target;
            }
            set
            {
                m_Target = value;
            }
        }
    }

}
