﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    public abstract class SimpleFloatCountTransition<T> : SimpleComponentTweenTransition<T>
        where T : Component
    {
        [SerializeField]
        protected float m_StartValue = 0;

        [SerializeField]
        protected float m_EndValue = 10;

        public float StartValue
        {
            get
            {
                return m_StartValue;
            }
            set
            {
                m_StartValue = value;
            }
        }

        public float EndValue
        {
            get
            {
                return m_EndValue;
            }
            set
            {
                m_EndValue = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            m_Ease = Ease.Linear;
            m_SpeedBased = true;
        }

        protected override Tween GetTransitionTween()
        {
            DOGetter<float> getter = () => TransitionGetter();
            DOSetter<float> setter = (float v) => TransitionSetter(v);

            return DOTween.To(new FloatPlugin(), getter, setter, (int)m_EndValue, m_Duration);
        }

        protected virtual float TransitionGetter()
        {
            return m_StartValue;
        }

        protected abstract void TransitionSetter(float value);

        protected override void SkipToStartInternal()
        {
            TransitionSetter(m_StartValue);
        }

        protected override void SkipToEndInternal()
        {
            TransitionSetter(m_EndValue);
        }

        public override bool DoesNotNeedToTransition()
        {
            return m_StartValue == m_EndValue;
        }
    }

}