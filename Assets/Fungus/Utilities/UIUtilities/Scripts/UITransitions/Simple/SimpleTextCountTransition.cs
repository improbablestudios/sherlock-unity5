﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Text Count Transition", menuName = "UI/Transition/Simple/Count/Text", order = 0)]
    public class SimpleTextCountTransition : SimpleFloatCountTransition<Text>
    {
        protected Func<float, string> m_valueStringFormatter;

        protected override void TransitionSetter(float value)
        {
            if (m_Target != null)
            {
                string valueString = value.ToString();
                if (m_valueStringFormatter != null)
                {
                    valueString = m_valueStringFormatter(value);
                }
                m_Target.text = valueString;
            }
        }

        public SimpleTextCountTransition SetValueStringFormatter(Func<float, string> valueStringFormatter)
        {
            m_valueStringFormatter = valueStringFormatter;
            return this;
        }
    }

}
