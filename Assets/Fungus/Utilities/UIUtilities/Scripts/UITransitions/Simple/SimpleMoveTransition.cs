﻿using DG.Tweening;
using System;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Move Transition", menuName = "UI/Transition/Simple/Move", order = 0)]
    public class SimpleMoveTransition : SimpleComponentTweenTransition<RectTransform>
    {
        [SerializeField]
        protected Vector2 m_StartAnchoredPosition = Vector2.zero;

        [SerializeField]
        protected Vector2 m_EndAnchoredPosition = Vector2.zero;

        public Vector2 StartAnchoredPosition
        {
            get
            {
                return m_StartAnchoredPosition;
            }
            set
            {
                m_StartAnchoredPosition = value;
            }
        }

        public Vector2 EndAnchoredPosition
        {
            get
            {
                return m_EndAnchoredPosition;
            }
            set
            {
                m_EndAnchoredPosition = value;
            }
        }

        protected override Tween GetTransitionTween()
        {
            if (m_Target != null)
            {
                m_Target.anchoredPosition = m_StartAnchoredPosition;
                return m_Target.DOAnchorPos(m_EndAnchoredPosition, m_Duration);
            }
            return null;
        }

        protected override void SkipToStartInternal()
        {
            if (m_Target != null)
            {
                m_Target.anchoredPosition = m_StartAnchoredPosition;
            }
        }

        protected override void SkipToEndInternal()
        {
            if (m_Target != null)
            {
                m_Target.anchoredPosition = m_EndAnchoredPosition;
            }
        }

        public override bool DoesNotNeedToTransition()
        {
            if (m_Target != null)
            {
                return m_Target.anchoredPosition == m_EndAnchoredPosition;
            }
            return false;
        }
    }

}
