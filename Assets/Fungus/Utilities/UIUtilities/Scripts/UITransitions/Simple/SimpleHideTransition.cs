﻿using DG.Tweening;
using ImprobableStudios.SearchPopupUtilities;
using System;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Hide Transition", menuName = "UI/Transition/Simple/Hide", order = 0)]
    public class SimpleHideTransition : SimpleTransition
    {
        [ObjectPopup("<Default>")]
        [SerializeField]
        protected SimpleFadeTransition m_FadeTransition;

        [ObjectPopup("<Default>")]
        [SerializeField]
        protected SimpleMoveTransition m_MoveTransition;

        public SimpleFadeTransition FadeTransition
        {
            get
            {
                ValidateFadeTransition();
                return m_FadeTransition;
            }
            set
            {
                m_FadeTransition = value;
            }
        }

        public SimpleMoveTransition MoveTransition
        {
            get
            {
                ValidateMoveTransition();
                return m_MoveTransition;
            }
            set
            {
                m_MoveTransition = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (Application.isPlaying)
            {
                ValidateFadeTransition();
                ValidateMoveTransition();
            }
        }

        private void ValidateFadeTransition()
        {
            if (m_FadeTransition == null)
            {
                m_FadeTransition = CreateTransition<SimpleFadeTransition>();
                m_FadeTransition.StartAlpha = 1f;
                m_FadeTransition.EndAlpha = 0f;
            }
        }

        private void ValidateMoveTransition()
        {
            if (m_MoveTransition == null)
            {
                m_MoveTransition = CreateTransition<SimpleMoveTransition>();
                m_MoveTransition.StartAnchoredPosition = Vector2.zero;
                m_MoveTransition.EndAnchoredPosition = Vector2.zero;
            }
        }

        public SimpleHideTransition SetFadeTarget(CanvasGroup canvasGroup)
        {
            FadeTransition.Target = canvasGroup;
            return this;
        }

        public SimpleHideTransition SetMoveTarget(RectTransform rectTransform)
        {
            MoveTransition.Target = rectTransform;
            return this;
        }

        protected override Tween PlayInternal()
        {
            Sequence sequence = DOTween.Sequence();

            FadeTransition.Play();
            sequence.Join(m_FadeTransition.GetActiveTween());

            MoveTransition.Play();
            sequence.Join(m_MoveTransition.GetActiveTween());

            return sequence;
        }

        protected override void SkipToStartInternal()
        {
            FadeTransition.SkipToStart();
            MoveTransition.SkipToStart();
        }

        protected override void SkipToEndInternal()
        {
            FadeTransition.SkipToEnd();
            MoveTransition.SkipToEnd();
        }

        public override bool DoesNotNeedToTransition()
        {
            return FadeTransition.DoesNotNeedToTransition()
                && MoveTransition.DoesNotNeedToTransition();
        }
    }

}
