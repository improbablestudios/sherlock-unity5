﻿using System;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class SimpleTransitionStates : TransitionStates<SimpleTransitionState>
    {
        public new SimpleTransition this[string key]
        {
            get
            {
                return GetTransition(key) as SimpleTransition;
            }
            set
            {
                SetTransition(key, value as SimpleTransition);
            }
        }

        public new SimpleTransition[] GetTransitions()
        {
            return base.GetTransitions().Cast<SimpleTransition>().ToArray();
        }

        protected override TransitionState CreateTransitionState(string key, UITransition transition)
        {
            return new SimpleTransitionState(key, transition as SimpleTransition);
        }
    }

}
