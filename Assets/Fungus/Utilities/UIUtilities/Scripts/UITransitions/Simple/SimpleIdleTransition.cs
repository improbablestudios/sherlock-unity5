﻿using DG.Tweening;
using System;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Idle Transition", menuName = "UI/Transition/Simple/Idle", order = 0)]
    public class SimpleIdleTransition : SimpleTransition
    {
        public override bool DoesNotNeedToTransition()
        {
            return false;
        }

        protected override Tween PlayInternal()
        {
            return null;
        }

        protected override void SkipToEndInternal()
        {
        }

        protected override void SkipToStartInternal()
        {
        }
    }

}
