﻿using System;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class SimpleTransitionState : TransitionState<SimpleTransition>
    {
        public SimpleTransitionState(string key, SimpleTransition transition) : base(key, transition)
        {
        }
    }

}
