﻿using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Slider Count Transition", menuName = "UI/Transition/Simple/Count/Slider", order = 0)]
    public class SimpleSliderCountTransition : SimpleFloatCountTransition<Slider>
    {
        protected override void Reset()
        {
            base.Reset();

            m_Duration = 1f;
        }

        protected override void TransitionSetter(float value)
        {
            if (m_Target != null)
            {
                m_Target.value = value;
            }
        }
    }

}
