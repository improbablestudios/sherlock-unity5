﻿using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Scroll Count Transition", menuName = "UI/Transition/Simple/Count/Scroll", order = 0)]
    public class SimpleScrollCountTransition : SimpleFloatCountTransition<ScrollRect>
    {
        [SerializeField]
        protected float m_MaxValue = 10;

        public float MaxValue
        {
            get
            {
                return m_MaxValue;
            }
            set
            {
                m_MaxValue = value;
            }
        }

        protected override void TransitionSetter(float value)
        {
            if (m_Target != null)
            {
                float normalizedValue = value / m_MaxValue;
                m_Target.normalizedPosition = Vector2.one - new Vector2(normalizedValue, normalizedValue);
            }
        }
    }

}
