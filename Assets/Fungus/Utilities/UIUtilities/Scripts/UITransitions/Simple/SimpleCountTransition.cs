﻿using DG.Tweening;
using ImprobableStudios.SearchPopupUtilities;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Count Transition", menuName = "UI/Transition/Simple/Count", order = 0)]
    public class SimpleCountTransition : SimpleTransition
    {
        [ObjectPopup("<Default>")]
        [SerializeField]
        protected SimpleTextCountTransition m_TextCountTransition;

        [ObjectPopup("<Default>")]
        [SerializeField]
        protected SimpleSliderCountTransition m_SliderCountTransition;

        [ObjectPopup("<Default>")]
        [SerializeField]
        protected SimpleScrollCountTransition m_ScrollCountTransition;

        public SimpleTextCountTransition TextCountTransition
        {
            get
            {
                ValidateTextCountTransition();
                return m_TextCountTransition;
            }
            set
            {
                m_TextCountTransition = value;
            }
        }

        public SimpleSliderCountTransition SliderCountTransition
        {
            get
            {
                ValidateSliderCountTransition();
                return m_SliderCountTransition;
            }
            set
            {
                m_SliderCountTransition = value;
            }
        }

        public SimpleScrollCountTransition ScrollCountTransition
        {
            get
            {
                ValidateScrollCountTransition();
                return m_ScrollCountTransition;
            }
            set
            {
                m_ScrollCountTransition = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (Application.isPlaying)
            {
                ValidateTextCountTransition();
                ValidateSliderCountTransition();
                ValidateScrollCountTransition();
            }
        }

        private void ValidateTextCountTransition()
        {
            if (m_TextCountTransition == null)
            {
                m_TextCountTransition = CreateTransition<SimpleTextCountTransition>();
            }
        }

        private void ValidateSliderCountTransition()
        {
            if (m_SliderCountTransition == null)
            {
                m_SliderCountTransition = CreateTransition<SimpleSliderCountTransition>();
            }
        }

        private void ValidateScrollCountTransition()
        {
            if (m_ScrollCountTransition == null)
            {
                m_ScrollCountTransition = CreateTransition<SimpleScrollCountTransition>();
            }
        }

        public SimpleCountTransition SetTextCountTarget(Text text)
        {
            TextCountTransition.Target = text;
            return this;
        }

        public SimpleCountTransition SetSliderCountTarget(Slider slider)
        {
            SliderCountTransition.Target = slider;
            return this;
        }

        public SimpleCountTransition SetScrollCountTarget(ScrollRect scrollRect)
        {
            ScrollCountTransition.Target = scrollRect;
            return this;
        }

        public SimpleCountTransition SetStartValue(float startValue)
        {
            TextCountTransition.StartValue = startValue;
            SliderCountTransition.StartValue = startValue;
            ScrollCountTransition.StartValue = startValue;
            return this;
        }

        public SimpleCountTransition SetEndValue(float endValue)
        {
            TextCountTransition.EndValue = endValue;
            SliderCountTransition.EndValue = endValue;
            ScrollCountTransition.EndValue = endValue;
            return this;
        }

        public SimpleCountTransition SetMaxValue(float maxValue)
        {
            ScrollCountTransition.MaxValue = maxValue;
            return this;
        }

        protected override Tween PlayInternal()
        {
            Sequence sequence = DOTween.Sequence();

            TextCountTransition.Play();
            sequence.Join(TextCountTransition.GetActiveTween());

            SliderCountTransition.Play();
            sequence.Join(SliderCountTransition.GetActiveTween());

            ScrollCountTransition.Play();
            sequence.Join(ScrollCountTransition.GetActiveTween());

            return sequence;
        }

        protected override void SkipToStartInternal()
        {
            TextCountTransition.SkipToStart();
            SliderCountTransition.SkipToStart();
            ScrollCountTransition.SkipToStart();
        }

        protected override void SkipToEndInternal()
        {
            TextCountTransition.SkipToEnd();
            SliderCountTransition.SkipToEnd();
            ScrollCountTransition.SkipToEnd();
        }

        public override bool DoesNotNeedToTransition()
        {
            return TextCountTransition.DoesNotNeedToTransition()
                && SliderCountTransition.DoesNotNeedToTransition()
                && ScrollCountTransition.DoesNotNeedToTransition();
        }
    }

}
