﻿using DG.Tweening;
using System;
using UnityEngine;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Fade Transition", menuName = "UI/Transition/Simple/Fade", order = 0)]
    public class SimpleFadeTransition : SimpleComponentTweenTransition<CanvasGroup>
    {
        [SerializeField]
        protected float m_StartAlpha = 0f;

        [SerializeField]
        protected float m_EndAlpha = 1f;

        public float StartAlpha
        {
            get
            {
                return m_StartAlpha;
            }
            set
            {
                m_StartAlpha = value;
            }
        }

        public float EndAlpha
        {
            get
            {
                return m_EndAlpha;
            }
            set
            {
                m_EndAlpha = value;
            }
        }

        protected override Tween GetTransitionTween()
        {
            if (m_Target != null)
            {
                m_Target.alpha = m_StartAlpha;
                return m_Target.DOFade(m_EndAlpha, m_Duration);
            }
            return null;
        }

        protected override void SkipToStartInternal()
        {
            if (m_Target != null)
            {
                m_Target.alpha = m_StartAlpha;
            }
        }

        protected override void SkipToEndInternal()
        {
            if (m_Target != null)
            {
                m_Target.alpha = m_EndAlpha;
            }
        }

        public override bool DoesNotNeedToTransition()
        {
            if (m_Target != null)
            {
                return m_Target.alpha == m_EndAlpha;
            }
            return false;
        }
    }

}
