﻿using System;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class AnimatedTransitionStates : TransitionStates<AnimatedTransitionState>
    {
        public new AnimatedTransition this[string key]
        {
            get
            {
                return GetTransition(key) as AnimatedTransition;
            }
            set
            {
                SetTransition(key, value as AnimatedTransition);
            }
        }

        public new AnimatedTransition[] GetTransitions()
        {
            return base.GetTransitions().Cast<AnimatedTransition>().ToArray();
        }

        public string GetAnimatorStateName(string key)
        {
            return m_States.Where(x => x.Key == key).Select(x => GetAnimatorStateName(x.Transition)).FirstOrDefault();
        }

        public string[] GetAnimatorStateNames()
        {
            return m_States.Select(x => GetAnimatorStateName(x.Transition)).ToArray();
        }

        private string GetAnimatorStateName(UITransition transition)
        {
            if (transition != null)
            {
                AnimatedTransition animatedTransition = transition as AnimatedTransition;
                if (animatedTransition != null)
                {
                    return animatedTransition.AnimatorStateName;
                }
            }
            return null;
        }

        protected override TransitionState CreateTransitionState(string key, UITransition transition)
        {
            return new AnimatedTransitionState(key, transition as AnimatedTransition);
        }
    }

}
