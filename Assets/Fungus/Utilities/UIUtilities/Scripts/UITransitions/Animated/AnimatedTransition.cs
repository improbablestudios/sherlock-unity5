﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.UIUtilities
{

    [CreateAssetMenu(fileName = "Animated Transition", menuName = "UI/Transition/Animated", order = 0)]
    public class AnimatedTransition : UITransition
    {
        [SerializeField]
        protected string m_AnimatorStateName;

        [SerializeField]
        protected float m_Speed = 1f;

        protected MonoBehaviour m_monoBehaviour;

        protected Animator m_animator;

        protected string m_idleStateName;

        protected Coroutine m_transitionCoroutine;

        protected bool m_isDoingTransition;

        protected int m_previousAnimatorStateHash = -1;

        protected int m_currentAnimatorStateHash = -1;

        public string AnimatorStateName
        {
            get
            {
                return m_AnimatorStateName;
            }
            set
            {
                m_AnimatorStateName = value;
            }
        }

        public float Speed
        {
            get
            {
                return m_Speed;
            }
            set
            {
                m_Speed = value;
            }
        }

        public AnimatedTransition SetTransitionStateName(string transitionStateName)
        {
            m_AnimatorStateName = transitionStateName ?? throw new ArgumentNullException(nameof(transitionStateName));
            return this;
        }

        public AnimatedTransition SetIdleStateName(string idleStateName)
        {
            m_idleStateName = idleStateName ?? throw new ArgumentNullException(nameof(idleStateName));
            return this;
        }

        public AnimatedTransition SetMonoBehaviour(MonoBehaviour monoBehaviour)
        {
            m_monoBehaviour = monoBehaviour ?? throw new ArgumentNullException(nameof(monoBehaviour));
            return this;
        }

        public AnimatedTransition SetAnimator(Animator animator)
        {
            m_animator = animator ?? throw new ArgumentNullException(nameof(animator));
            return this;
        }

        public sealed override void Play()
        {
            m_monoBehaviour.gameObject.SetActive(true);
            m_transitionCoroutine = m_monoBehaviour.StartCoroutine(DoTransitionInternal(m_animator, m_AnimatorStateName, m_idleStateName, m_Speed, InvokeOnComplete));
        }

        protected IEnumerator DoTransitionInternal(Animator animator, string transitionStateName, string idleStateName, float speed, UnityAction onComplete = null)
        {
            m_isDoingTransition = true;

            animator.speed = speed;

            while (!animator.GetCurrentAnimatorStateInfo(0).IsName(idleStateName))
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();

            animator.Play(transitionStateName, 0, 0f);

            yield return new WaitForEndOfFrame();

            while (animator.GetCurrentAnimatorStateInfo(0).IsName(transitionStateName))
            {
                yield return null;
            }

            animator.Play(transitionStateName, 0, 1f);
            animator.Update(0f);

            yield return null;

            yield return new WaitForEndOfFrame();

            m_isDoingTransition = false;

            onComplete?.Invoke();
        }

        public sealed override void SkipToStart(UnityAction onSkip = null)
        {
            m_monoBehaviour.gameObject.SetActive(true);
            m_monoBehaviour.StartCoroutine(SkipToStartInternal(m_animator, m_AnimatorStateName, onSkip));
        }

        protected IEnumerator SkipToStartInternal(Animator animator, string transitionStateName, UnityAction onComplete = null)
        {
            return SkipToInternal(0f, animator, transitionStateName, onComplete);
        }

        public sealed override void SkipToEnd(UnityAction onSkip = null)
        {
            m_monoBehaviour.gameObject.SetActive(true);
            m_monoBehaviour.StartCoroutine(SkipToEndInternal(m_animator, m_AnimatorStateName, onSkip));
        }

        protected IEnumerator SkipToEndInternal(Animator animator, string transitionStateName, UnityAction onComplete = null)
        {
            return SkipToInternal(1f, animator, transitionStateName, onComplete);
        }

        protected IEnumerator SkipToInternal(float normalizedTime, Animator animator, string transitionStateName, UnityAction onComplete = null)
        {
            if (animator.isInitialized)
            {
                animator.speed = 1f;

                animator.Play(transitionStateName, 0, normalizedTime);
                animator.Update(0f);

                yield return null;

                yield return new WaitForEndOfFrame();
            }

            onComplete?.Invoke();
        }

        public sealed override bool IsDoingTransition()
        {
            return m_isDoingTransition;
        }

        public sealed override void Complete(UnityAction onComplete = null)
        {
            if (m_transitionCoroutine != null)
            {
                m_monoBehaviour.StopCoroutine(m_transitionCoroutine);
                m_transitionCoroutine = null;
            }
            m_isDoingTransition = false;

            SkipToEnd(onComplete);
        }

        public sealed override void Kill(UnityAction onKill = null)
        {
            if (m_transitionCoroutine != null)
            {
                m_monoBehaviour.StopCoroutine(m_transitionCoroutine);
                m_transitionCoroutine = null;
            }
            m_isDoingTransition = false;

            InvokeOnKill();
            onKill?.Invoke();
        }

        public sealed override void Resume(UnityAction onResume = null)
        {
            Play();
            onResume?.Invoke();
        }

        protected sealed override void SaveTransitionState()
        {
            m_ElapsedTime = m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            int currentAnimatorStateHash = m_animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
            if (currentAnimatorStateHash != m_currentAnimatorStateHash)
            {
                m_previousAnimatorStateHash = m_currentAnimatorStateHash;
                m_currentAnimatorStateHash = currentAnimatorStateHash;
            }
        }
    }

}
