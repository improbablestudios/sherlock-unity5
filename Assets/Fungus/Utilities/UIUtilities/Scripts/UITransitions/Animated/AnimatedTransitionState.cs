﻿using System;

namespace ImprobableStudios.UIUtilities
{

    [Serializable]
    public class AnimatedTransitionState : TransitionState<AnimatedTransition>
    {
        public AnimatedTransitionState(string key, AnimatedTransition transition) : base(key, transition)
        {
        }
    }

}
