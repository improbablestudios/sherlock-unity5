﻿using ImprobableStudios.BaseUtilities;
using UnityEngine.Events;

namespace ImprobableStudios.UIUtilities
{

    public enum UITransitionType
    {
        None,
        Simple,
        Animation,
    }

    public abstract class UITransition : IdentifiableScriptableObject
    {
        private UnityEvent m_OnComplete = new UnityEvent();

        private UnityEvent m_OnKill = new UnityEvent();

        protected float m_ElapsedTime;

        public abstract void Play();

        public abstract void SkipToStart(UnityAction onSkip = null);

        public abstract void SkipToEnd(UnityAction onSkip = null);

        public abstract bool IsDoingTransition();

        public abstract void Resume(UnityAction onResume = null);

        public abstract void Complete(UnityAction onComplete = null);

        public abstract void Kill(UnityAction onKill = null);

        public UITransition SetOnComplete(UnityAction onComplete)
        {
            m_OnComplete?.RemoveAllListeners();
            m_OnComplete?.AddListener(onComplete);
            return this;
        }

        public UITransition SetOnKill(UnityAction onKill)
        {
            m_OnKill?.RemoveAllListeners();
            m_OnKill?.AddListener(onKill);
            return this;
        }

        public UITransition AddOnComplete(UnityAction onComplete)
        {
            m_OnComplete?.AddListener(onComplete);
            return this;
        }

        public UITransition AddOnKill(UnityAction onKill)
        {
            m_OnKill?.AddListener(onKill);
            return this;
        }

        public UITransition RemoveOnComplete(UnityAction onComplete)
        {
            m_OnComplete?.RemoveListener(onComplete);
            return this;
        }

        public UITransition RemoveOnKill(UnityAction onKill)
        {
            m_OnKill?.RemoveListener(onKill);
            return this;
        }

        public UITransition RemoveAllOnComplete()
        {
            m_OnComplete?.RemoveAllListeners();
            return this;
        }

        public UITransition RemoveAllOnKill()
        {
            m_OnKill?.RemoveAllListeners();
            return this;
        }

        protected void InvokeOnComplete()
        {
            m_OnComplete?.Invoke();
            m_OnComplete?.RemoveAllListeners();
        }

        protected void InvokeOnKill()
        {
            m_OnKill?.Invoke();
            m_OnKill?.RemoveAllListeners();
        }

        protected abstract void SaveTransitionState();

        public static T CreateTransition<T>() where T : UITransition
        {
            T transition = CreateInstance<T>();
            transition.Reset();
            return transition;
        }

        public static T CreateTransition<T>(string state) where T : UITransition
        {
            T transition = CreateInstance<T>();
            transition.name = state;
            transition.Reset();
            return transition;
        }
    }

}
