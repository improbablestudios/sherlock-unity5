﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ImprobableStudios.UIUtilities
{

    public abstract class TransitionStates
    {
        public UITransition this[string key]
        {
            get
            {
                return GetTransition(key);
            }
            set
            {
                SetTransition(key, value);
            }
        }

        protected abstract TransitionState[] GetTransitionStates();

        protected abstract void AddTransitionState(TransitionState transitionState);

        protected abstract void RemoveTransitionState(TransitionState transitionState);

        protected abstract TransitionState CreateTransitionState(string key, UITransition transition);

        public T GetTransition<T>() where T : UITransition
        {
            foreach (TransitionState state in GetTransitionStates())
            {
                if (state != null && state.Transition is T)
                {
                    return state.Transition as T;
                }
            }
            return null;
        }

        public T GetTransition<T>(string key) where T : UITransition
        {
            foreach (TransitionState state in GetTransitionStates())
            {
                if (state != null && state.Key == key && state.Transition is T)
                {
                    return state.Transition as T;
                }
            }
            return null;
        }
        public UITransition[] GetTransitions()
        {
            return GetTransitionStates().Select(x => x.Transition).ToArray();
        }

        public void RemoveStates(string key)
        {
            foreach (TransitionState state in GetTransitionStates())
            {
                if (state != null && state.Key == key)
                {
                    RemoveTransitionState(state);
                }
            }
        }

        public string[] GetStates()
        {
            return GetTransitionStates().Select(x => x.Key).ToArray();
        }

        public TransitionState[] GetActiveTransitionStates()
        {
            return GetTransitionStates().Where(x => x.Transition != null && x.Transition.IsDoingTransition()).ToArray();
        }

        public string[] GetActiveStates()
        {
            return GetTransitionStates().Where(x => x.Transition != null && x.Transition.IsDoingTransition()).Select(x => x.Key).ToArray();
        }

        public UITransition[] GetActiveTransitions()
        {
            return GetTransitionStates().Where(x => x.Transition != null && x.Transition.IsDoingTransition()).Select(x => x.Transition).ToArray();
        }

        public bool IsStateActive(string key)
        {
            return GetTransitionStates().Any(x => x.Key == key && x.Transition != null && x.Transition.IsDoingTransition());
        }

        public bool IsValidState(string key)
        {
            return GetTransitionStates().Any(x => x.Key == key);
        }

        protected UITransition GetTransition(string key)
        {
            foreach (TransitionState state in GetTransitionStates())
            {
                if (state != null && state.Key == key)
                {
                    return state.Transition;
                }
            }
            return null;
        }

        protected void SetTransition(string key, UITransition transition)
        {
            TransitionState state = GetTransitionStates().FirstOrDefault(x => x.Key == key);
            if (state != null)
            {
                state.Transition = transition;
            }
            else
            {
                AddTransitionState(CreateTransitionState(key, transition));
            }
        }
    }

    public abstract class TransitionStates<T> : TransitionStates where T : TransitionState
    {
        [SerializeField]
        protected List<T> m_States = new List<T>();

        protected sealed override TransitionState[] GetTransitionStates()
        {
            return m_States.ToArray();
        }

        protected sealed override void AddTransitionState(TransitionState transitionState)
        {
            if (transitionState is T)
            {
                m_States.Add(transitionState as T);
            }
        }

        protected sealed override void RemoveTransitionState(TransitionState transitionState)
        {
            if (transitionState is T)
            {
                m_States.Remove(transitionState as T);
            }
        }
    }

}