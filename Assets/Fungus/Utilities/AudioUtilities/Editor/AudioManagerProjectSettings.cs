﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.AudioUtilities
{

    public class AudioManagerSettingsProvider : BaseAssetSettingsProvider<AudioManager>
    {
        public AudioManagerSettingsProvider() : base("Project/Managers/Audio Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new AudioManagerSettingsProvider();
        }
    }

    public class AudioManagerSettingsBuilder : BaseAssetSettingsBuilder<AudioManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif