#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using UnityEngine;
using System;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.AudioUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(AudioManager), true)]
    public class AudioManagerEditor : IdentifiableScriptableObjectEditor
    {
        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            AudioManager t = serializedObject.targetObject as AudioManager;

            if (property.name == "m_" + nameof(t.AudioMixer))
            {
                base.DrawProperty(property, label);

                if (t.AudioMixer == null)
                {
                    EditorGUILayout.HelpBox("An audio mixer is required to control volume and pitch", MessageType.Error);
                }
            }
            else if (property.name == "m_" + nameof(t.AudioMixerSettings))
            {
                base.DrawProperty(property, label, options);

                List<string> errors = new List<string>();
                foreach (AudioMixerGroupInfo amgi in t.AudioMixerSettings)
                {
                    float value = 0f;
                    if (t.AudioMixer != null && !t.AudioMixer.GetFloat(amgi.m_VolumeParameter, out value))
                    {
                        errors.Add(string.Format("Exposed parameter name ({0}) does not exist in Audio Mixer", amgi.m_VolumeParameter));
                    }
                }
                if (errors.Count > 0)
                {
                    EditorGUILayout.HelpBox(string.Join("\n", errors.ToArray()), MessageType.Error);
                    if (GUILayout.Button("Open Audio Mixer"))
                    {
                        Type audioMixerWindowType = typeof(EditorWindow).Assembly.GetType("UnityEditor.AudioMixerWindow");
                        EditorWindow.GetWindow(audioMixerWindowType);
                    }
                }
                if (GUILayout.Button("Update Audio Mixer Settings"))
                {
                    t.UpdateAudioMixerSettings();
                }
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }
    }
}
#endif