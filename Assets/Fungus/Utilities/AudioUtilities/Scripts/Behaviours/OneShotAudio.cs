﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

namespace ImprobableStudios.AudioUtilities

{
    public class OneShotAudio : PersistentBehaviour
    {
        public static AudioSource Play(AudioClip clip, AudioMixerGroup audioMixerGroup = null, UnityAction onComplete = null)
        {
            OneShotAudio oneShotAudio = PersistentBehaviour.InstantiateAndRegister(Resources.Load<OneShotAudio>("~OneShotAudio"));
            oneShotAudio.PlayInternal(clip, audioMixerGroup, onComplete);
            return oneShotAudio.GetComponent<AudioSource>();
        }

        public void PlayInternal(AudioClip clip, AudioMixerGroup audioMixerGroup = null, UnityAction onComplete = null)
        {
            AudioSource audioSource = this.GetComponent<AudioSource>();
            audioSource.outputAudioMixerGroup = audioMixerGroup;
            audioSource.PlayOneShot(clip, 1f);
            StartCoroutine(DestroyWhenDone(clip.length, onComplete));
        }

        private IEnumerator DestroyWhenDone(float time, UnityAction onComplete)
        {
            yield return new WaitForSeconds(time);
            if (onComplete != null)
            {
                onComplete();
            }
            Destroy(this.gameObject);
        }
    }
}