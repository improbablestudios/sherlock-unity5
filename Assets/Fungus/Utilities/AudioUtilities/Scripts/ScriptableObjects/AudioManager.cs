﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;
using UnityEngine.Audio;
using ImprobableStudios.Localization;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ScriptableBehaviourUtilities;

namespace ImprobableStudios.AudioUtilities
{

    [Serializable]
    public class AudioMixerGroupInfo
    {
        public AudioMixerGroup m_AudioMixerGroup;
        public string m_VolumeParameter;
        public string m_PitchParameter;
        [NonSerialized]
        public float m_CurrentVolume = 1;

        public AudioMixerGroupInfo()
        {
        }

        public AudioMixerGroupInfo(AudioMixerGroup audioMixerGroup)
        {
            m_AudioMixerGroup = audioMixerGroup;
            m_VolumeParameter = audioMixerGroup.name + "Volume";
            m_PitchParameter = audioMixerGroup.name + "Pitch";
        }
    }

    public class AudioManager : SettingsScriptableObject<AudioManager>
    {
        [Tooltip("The audio mixer that is used to control all audio")]
        [SerializeField]
        protected AudioMixer m_AudioMixer;

        [Tooltip("The audio mixer groups and parameters that are used to control them")]
        [SerializeField]
        protected AudioMixerGroupInfo[] m_AudioMixerSettings = new AudioMixerGroupInfo[0];
        
        protected HashSet<AudioSource> m_playedAudioSources = new HashSet<AudioSource>();
        
        protected List<AudioClip> m_playedAudioClips = new List<AudioClip>();

        public AudioMixer AudioMixer
        {
            get
            {
                return m_AudioMixer;
            }
            set
            {
                m_AudioMixer = value;
            }
        }

        public AudioMixerGroupInfo[] AudioMixerSettings
        {
            get
            {
                return m_AudioMixerSettings;
            }
            set
            {
                m_AudioMixerSettings = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            UpdateAudioMixerSettings();
        }

        public void UpdateAudioMixerSettings()
        {
            if (m_AudioMixer == null)
            {
                m_AudioMixerSettings = new AudioMixerGroupInfo[0];
                return;
            }

            List<AudioMixerGroupInfo> newAudioMixerSettings = m_AudioMixerSettings.ToList();

            foreach (AudioMixerGroup audioMixerGroup in m_AudioMixer.FindMatchingGroups(""))
            {
                if (GetAudioMixerGroupInfo(audioMixerGroup) == null)
                {
                    newAudioMixerSettings.Add(new AudioMixerGroupInfo(audioMixerGroup));
                }
            }

            newAudioMixerSettings.RemoveAll(i => i.m_AudioMixerGroup == null);

            m_AudioMixerSettings = newAudioMixerSettings.ToArray();
        }

        public void UpdateAudioMixer()
        {
            foreach (AudioMixerGroupInfo amgi in m_AudioMixerSettings)
            {
                SetLinearVolumeOfAudioMixerGroup(amgi.m_AudioMixerGroup, amgi.m_CurrentVolume);
            }
        }

        public AudioMixerGroup GetMasterAudioMixerGroup()
        {
            return m_AudioMixer.FindMatchingGroups("Master")[0];
        }

        public AudioMixerGroupInfo GetAudioMixerGroupInfo(AudioMixerGroup audioMixerGroup)
        {
            for (int i = 0; i < m_AudioMixerSettings.Length; i++)
            {
                AudioMixerGroupInfo amgi = m_AudioMixerSettings[i];
                if (amgi.m_AudioMixerGroup == audioMixerGroup)
                {
                    return amgi;
                }
            }
            return null;
        }

        public AudioSource[] GetAllPlayedAudioSourcesInAudioMixerGroup(AudioMixerGroup audioMixerGroup)
        {
            List<AudioSource> audioSourcesInAudioMixerGroup = new List<AudioSource>();
            foreach (AudioSource a in m_playedAudioSources)
            {
                if (a != null && a.outputAudioMixerGroup == audioMixerGroup)
                {
                    audioSourcesInAudioMixerGroup.Add(a);
                }
            }
            return audioSourcesInAudioMixerGroup.ToArray();
        }

        public AudioSource[] GetAllPlayingAudioSourcesInAudioMixerGroup(AudioMixerGroup audioMixerGroup)
        {
            List<AudioSource> audioSourcesInAudioMixerGroup = new List<AudioSource>();
            foreach (AudioSource a in m_playedAudioSources)
            {
                if (a != null && a.outputAudioMixerGroup == audioMixerGroup && a.isPlaying)
                {
                    audioSourcesInAudioMixerGroup.Add(a);
                }
            }
            return audioSourcesInAudioMixerGroup.ToArray();
        }

        public AudioSource[] GetAllPlayingAudioSources()
        {
            List<AudioSource> audioSourcesInAudioMixerGroup = new List<AudioSource>();
            foreach (AudioSource a in m_playedAudioSources)
            {
                if (a != null && a.isPlaying)
                {
                    audioSourcesInAudioMixerGroup.Add(a);
                }
            }
            return audioSourcesInAudioMixerGroup.ToArray();
        }

        public AudioSource[] GetAllPlayedAudioSources()
        {
            return m_playedAudioSources.ToArray();
        }

        public void Play(AudioClip audioClip, AudioMixerGroup audioMixerGroup, UnityAction onComplete = null)
        {
            AudioSource oneShotAudioSource = OneShotAudio.Play(audioClip, audioMixerGroup, onComplete);

            m_playedAudioSources.Add(oneShotAudioSource);
        }

        public void Play(Dictionary<string, AudioClip> localizedDictionary, AudioClip audioClip, AudioSource audioSource, UnityAction onComplete = null)
        {
            audioSource.RegisterLocalizableAudioClip(localizedDictionary);
            Play(audioClip, audioSource, onComplete);
        }

        public void Play(AudioClip audioClip, AudioSource audioSource, UnityAction onComplete = null)
        {
            audioSource.clip = audioClip;
            Play(audioSource, audioSource.loop, 0f, onComplete);
        }

        public void Play(AudioClip audioClip, AudioSource audioSource, bool loop, float fadeDuration, UnityAction onComplete = null)
        {
            audioSource.clip = audioClip;
            Play(audioSource, loop, fadeDuration, onComplete);
        }

        public void Play(AudioSource audioSource, bool loop = false, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            m_playedAudioSources.Add(audioSource);

            float volume = audioSource.volume;
            audioSource.loop = loop;

            if (fadeDuration <= 0f)
            {
                PlaySoundWithCallback(audioSource, volume, onComplete);
            }
            else
            {
                audioSource.volume = 0;
                audioSource.Play();
                audioSource.DOFade(volume, fadeDuration)
                    .OnComplete(
                        () =>
                        {
                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        });
            }
        }

        public void Pause(AudioSource audioSource, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                audioSource.Pause();
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            float originalVolume = audioSource.volume;
            audioSource.DOFade(0, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        audioSource.Pause();
                        audioSource.volume = originalVolume;
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void PauseAll(float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayingAudioSources();
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onPause = null;
                if (i + 1 >= audioSources.Length)
                {
                    onPause = onComplete;
                }
                Pause(a, fadeDuration, onPause);
            }
        }

        public void PauseAllInAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayingAudioSourcesInAudioMixerGroup(audioMixerGroup);
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onPause = null;
                if (i + 1 >= audioSources.Length)
                {
                    onPause = onComplete;
                }
                Pause(a, fadeDuration, onPause);
            }
        }

        public void UnPause(AudioSource audioSource, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                audioSource.UnPause();
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            float volume = audioSource.volume;
            audioSource.volume = 0;
            audioSource.UnPause();
            audioSource.DOFade(volume, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void UnPauseAll(float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayedAudioSources();
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onUnPause = null;
                if (i + 1 >= audioSources.Length)
                {
                    onUnPause = onComplete;
                }
                UnPause(a, fadeDuration, onUnPause);
            }
        }

        public void UnPauseAllInAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayedAudioSourcesInAudioMixerGroup(audioMixerGroup);
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onUnPause = null;
                if (i + 1 >= audioSources.Length)
                {
                    onUnPause = onComplete;
                }
                UnPause(a, fadeDuration, onUnPause);
            }
        }

        public void Stop(AudioSource audioSource, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                audioSource.Stop();
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            float originalVolume = audioSource.volume;
            audioSource.DOFade(0, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        audioSource.Stop();
                        audioSource.volume = originalVolume;
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void StopAll(float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayingAudioSources();
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onStop = null;
                if (i + 1 >= audioSources.Length)
                {
                    onStop = onComplete;
                }
                Stop(a, fadeDuration, onStop);
            }
        }

        public void StopAllInAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioSource[] audioSources = GetAllPlayingAudioSourcesInAudioMixerGroup(audioMixerGroup);
            for (int i = 0; i < audioSources.Length; i++)
            {
                AudioSource a = audioSources[i];
                UnityAction onStop = null;
                if (i + 1 >= audioSources.Length)
                {
                    onStop = onComplete;
                }
                Stop(a, fadeDuration, onStop);
            }
        }

        public void ChangeVolume(AudioSource audioSource, float volume, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                audioSource.volume = volume;
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            audioSource.DOFade(volume, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void ChangeVolumeOfAll(float volume, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioMixerGroup audioMixerGroup = GetMasterAudioMixerGroup();
            ChangeFloatOfAudioMixerGroup(GetAudioMixerGroupInfo(audioMixerGroup).m_VolumeParameter, ConvertLinearVolumeToDecibel(volume), fadeDuration, onComplete);
        }

        public void ChangeVolumeOfAudioMixerGroup(AudioMixerGroup audioMixerGroup, float volume, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            ChangeFloatOfAudioMixerGroup(GetAudioMixerGroupInfo(audioMixerGroup).m_VolumeParameter, ConvertLinearVolumeToDecibel(volume), fadeDuration, onComplete);
        }

        public float GetLinearVolumeOfAudioMixerGroup(AudioMixerGroup audioMixerGroup)
        {
            float currentValue = 0f;
            m_AudioMixer.GetFloat(GetAudioMixerGroupInfo(audioMixerGroup).m_VolumeParameter, out currentValue);
            return ConvertDecibelVolumeToLinear(currentValue);
        }

        public void SetLinearVolumeOfAudioMixerGroup(AudioMixerGroup audioMixerGroup, float value)
        {
            AudioMixerGroupInfo amgi = GetAudioMixerGroupInfo(audioMixerGroup);
            m_AudioMixer.SetFloat(amgi.m_VolumeParameter, ConvertLinearVolumeToDecibel(value));
        }

        private float ConvertLinearVolumeToDecibel(float linear)
        {
            float dB;

            if (linear != 0)
            {
                dB = 20.0f * Mathf.Log10(linear);
            }
            else
            {
                dB = -144.0f;
            }

            return dB;
        }

        private float ConvertDecibelVolumeToLinear(float dB)
        {
            float linear = Mathf.Pow(10.0f, dB / 20.0f);

            return linear;
        }

        public void ChangePitch(AudioSource audioSource, float pitch, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                audioSource.pitch = pitch;
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            audioSource.DOPitch(pitch, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void ChangePitchOfAll(float pitch, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            AudioMixerGroup audioMixerGroup = GetMasterAudioMixerGroup();
            ChangeFloatOfAudioMixerGroup(GetAudioMixerGroupInfo(audioMixerGroup).m_PitchParameter, pitch, fadeDuration, onComplete);
        }

        public void ChangePitchOfAudioMixerGroup(AudioMixerGroup audioMixerGroup, float pitch, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            ChangeFloatOfAudioMixerGroup(GetAudioMixerGroupInfo(audioMixerGroup).m_PitchParameter, pitch, fadeDuration, onComplete);
        }

        private void ChangeFloatOfAudioMixerGroup(string exposedParameterName, float value, float fadeDuration = 0f, UnityAction onComplete = null)
        {
            if (fadeDuration <= 0)
            {
                m_AudioMixer.SetFloat(exposedParameterName, value);
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }

            m_AudioMixer.DOSetFloat(exposedParameterName, value, fadeDuration)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void PlaySoundWithCallback(AudioSource audioSource, float volume, UnityAction callback)
        {
            audioSource.volume = volume;

            // Set timeSamples before playing to prevent seek error
            audioSource.timeSamples = 0;
            audioSource.Play();

            if (audioSource.clip != null)
            {
                m_playedAudioClips.Add(audioSource.clip);
            }
            else
            {
                if (callback != null)
                {
                    callback();
                }
            }
        }
    }

}