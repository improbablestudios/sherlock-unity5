﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SceneUtilities
{

    public class SceneLoadingManager : SettingsScriptableObject<SceneLoadingManager>
    {
        [Tooltip("The color to fade out the screen with when loading")]
        [SerializeField]
        [FormerlySerializedAs("loadingOverlayColor")]
        [FormerlySerializedAs("m_LoadingOverlayColor")]
        protected Color m_OverlayColor = Color.black;

        [Tooltip("The duration of the screen fade when loading")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("loadingOverlayFadeDuration")]
        [FormerlySerializedAs("m_LoadingOverlayFadeDuration")]
        protected float m_OverlayFadeDuration = 0.5f;

        [Tooltip("The sorting order of the canvas that will be used to fade in and out the scene")]
        [SerializeField]
        protected int m_OverlaySortingOrder = short.MaxValue;

        [Tooltip("The loading scene to use when loading a new scene")]
        [SerializeField]
        [FormerlySerializedAs("loadingSceneName")]
        protected string m_LoadingSceneName = "";

        [Tooltip("The miniumum amount of time the loading scene should be shown")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("minLoadingSceneDuration")]
        protected float m_MinLoadingSceneDuration = 1.5f;
        
        [Tooltip("The event that is executed when scenes have been loaded")]
        [SerializeField]
        protected UnityEvent m_OnSceneLoaded = new UnityEvent();

        [Tooltip("The event that is executed when scenes have been loaded and the loading overlay is hidden")]
        [SerializeField]
        protected UnityEvent m_OnOverlayHidden = new UnityEvent();

        protected UnityEvent m_OnNextSceneLoaded = new UnityEvent();

        protected UnityEvent m_OnNextOverlayHidden = new UnityEvent();

        public Color OverlayColor
        {
            get
            {
                return m_OverlayColor;
            }
            set
            {
                m_OverlayColor = value;
            }
        }
        
        public float OverlayFadeDuration
        {
            get
            {
                return m_OverlayFadeDuration;
            }
            set
            {
                m_OverlayFadeDuration = value;
            }
        }

        protected int OverlaySortingOrder
        {
            get
            {
                return m_OverlaySortingOrder;
            }
            set
            {
                m_OverlaySortingOrder = value;
            }
        }

        public string LoadingSceneName
        {
            get
            {
                return m_LoadingSceneName;
            }
            set
            {
                m_LoadingSceneName = value;
            }
        }

        public float MinLoadingSceneDuration
        {
            get
            {
                return m_MinLoadingSceneDuration;
            }
            set
            {
                m_MinLoadingSceneDuration = value;
            }
        }

        public UnityEvent OnSceneLoaded
        {
            get
            {
                return m_OnSceneLoaded;
            }
        }

        public UnityEvent OnOverlayHidden
        {
            get
            {
                return m_OnOverlayHidden;
            }
        }

        public UnityEvent OnNextSceneLoaded
        {
            get
            {
                return m_OnNextSceneLoaded;
            }
        }

        public UnityEvent OnNextOverlayHidden
        {
            get
            {
                return m_OnNextOverlayHidden;
            }
        }

        public bool IsLoading
        {
            get
            {
                return SceneLoader.Instance != null;
            }
        }

        public void LoadScene(string sceneNameToLoad, bool additive, bool showLoadingScreen)
        {
            string loadingSceneName = null;
            if (showLoadingScreen)
            {
                loadingSceneName = m_LoadingSceneName;
            }

            SetupSceneLoader();
            SceneLoader.LoadScenes(new string[] { sceneNameToLoad }, null, additive, loadingSceneName, m_MinLoadingSceneDuration, InvokeOnSceneLoaded, InvokeOnOverlayHidden);
        }

        public void LoadScene(int sceneIndexToLoad, bool additive, bool showLoadingScreen)
        {
            string loadingSceneName = null;
            if (showLoadingScreen)
            {
                loadingSceneName = m_LoadingSceneName;
            }

            SetupSceneLoader();
            SceneLoader.LoadScenes(null, new int[] { sceneIndexToLoad }, additive, loadingSceneName, m_MinLoadingSceneDuration, InvokeOnSceneLoaded, InvokeOnOverlayHidden);
        }

        public void LoadScenes(string[] sceneNamesToLoad, bool additive, bool showLoadingScreen)
        {
            string loadingSceneName = null;
            if (showLoadingScreen)
            {
                loadingSceneName = m_LoadingSceneName;
            }

            SetupSceneLoader();
            SceneLoader.LoadScenes(sceneNamesToLoad, null, additive,loadingSceneName, m_MinLoadingSceneDuration, InvokeOnSceneLoaded, InvokeOnOverlayHidden);
        }

        public void LoadScenes(int[] sceneIndicesToLoad, bool additive, bool showLoadingScreen)
        {
            string loadingSceneName = null;
            if (showLoadingScreen)
            {
                loadingSceneName = m_LoadingSceneName;
            }

            SetupSceneLoader();
            SceneLoader.LoadScenes(null, sceneIndicesToLoad, additive, loadingSceneName, m_MinLoadingSceneDuration, InvokeOnSceneLoaded, InvokeOnOverlayHidden);
        }

        protected void InvokeOnSceneLoaded()
        {
            m_OnSceneLoaded.Invoke();
            m_OnNextSceneLoaded.Invoke();
            m_OnNextSceneLoaded.RemoveAllListeners();
        }

        protected void InvokeOnOverlayHidden()
        {
            m_OnOverlayHidden.Invoke();
            m_OnNextOverlayHidden.Invoke();
            m_OnNextOverlayHidden.RemoveAllListeners();
        }

        protected void SetupSceneLoader()
        {
            SceneLoader.CreateSceneLoaderInstance();
            SceneLoader.Instance.OverlayColor = m_OverlayColor;
            SceneLoader.Instance.OverlayFadeDuration = m_OverlayFadeDuration;
            SceneLoader.Instance.OverlaySortingOrder = m_OverlaySortingOrder;
        }
    }

}
