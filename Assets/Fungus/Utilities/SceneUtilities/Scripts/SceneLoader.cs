﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SceneUtilities
{

    public class SceneLoader : PersistentBehaviour
    {
        private static SceneLoader s_instance;

        [Tooltip("The color to fade out the screen with when loading")]
        [SerializeField]
        protected Color m_OverlayColor = Color.black;

        [Tooltip("The duration of the screen fade when loading")]
        [Min(0)]
        [SerializeField]
        protected float m_OverlayFadeDuration = 0.5f;

        [Tooltip("The sorting order of the canvas that will be used to fade in and out the scene")]
        [SerializeField]
        protected int m_OverlaySortingOrder = short.MaxValue;

        protected Canvas m_loadingOverlayCanvas;

        protected Image m_loadingOverlayImage;

        public static SceneLoader Instance
        {
            get
            {
                return s_instance;
            }
        }

        public Color OverlayColor
        {
            get
            {
                return m_OverlayColor;
            }
            set
            {
                m_OverlayColor = value;
            }
        }

        public float OverlayFadeDuration
        {
            get
            {
                return m_OverlayFadeDuration;
            }
            set
            {
                m_OverlayFadeDuration = value;
            }
        }

        public int OverlaySortingOrder
        {
            get
            {
                return m_OverlaySortingOrder;
            }
            set
            {
                m_OverlaySortingOrder = value;
            }
        }

        public Canvas LoadingOverlayCanvas
        {
            get
            {
                if (m_loadingOverlayCanvas == null)
                {
                    GameObject fadeCanvasGO = PersistentBehaviour.InstantiateAndRegister(Resources.Load<GameObject>("~FadeCanvas"));
                    m_loadingOverlayCanvas = fadeCanvasGO.GetComponentInChildren<Canvas>(true);
                    fadeCanvasGO.transform.SetParent(this.transform);
                }
                return m_loadingOverlayCanvas;
            }
        }

        public Image LoadingOverlayImage
        {
            get
            {
                if (m_loadingOverlayImage == null)
                {
                    m_loadingOverlayImage = LoadingOverlayCanvas.GetComponentInChildren<Image>(true);
                }
                return m_loadingOverlayImage;
            }
        }

        public static void CreateSceneLoaderInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameObject(typeof(SceneLoader).Name).AddComponent<SceneLoader>();
                DontDestroyOnLoad(s_instance.gameObject);
            }
        }

        public static void LoadScenes(string[] sceneNamesToLoad, int[] sceneIndicesToLoad, bool additive, string loadingSceneName, float minLoadingDuration, UnityAction onScenesLoaded = null, UnityAction onComplete = null)
        {
            s_instance.StartCoroutine(s_instance.LoadScenesAsync(sceneNamesToLoad, sceneIndicesToLoad, additive, loadingSceneName, minLoadingDuration, onScenesLoaded, onComplete));
        }

        protected virtual IEnumerator LoadScenesAsync(string[] sceneNamesToLoad, int[] sceneIndicesToLoad, bool additive, string loadingSceneName, float minLoadingSceneDuration, UnityAction onScenesLoaded = null, UnityAction onComplete = null)
        {
            bool loadingSceneSpecified = !string.IsNullOrEmpty(loadingSceneName);

            LoadSceneMode specifiedLoadSceneMode = additive ? LoadSceneMode.Additive : LoadSceneMode.Single;
            LoadSceneMode loadingSceneLoadSceneMode = specifiedLoadSceneMode;

            LoadingOverlayCanvas.sortingOrder = m_OverlaySortingOrder;
            LoadingOverlayImage.color = new Color(m_OverlayColor.r, m_OverlayColor.g, m_OverlayColor.b, 0f);

            yield return StartCoroutine(Fade(1, m_OverlayFadeDuration));

            if (!additive)
            {
                DOTween.KillAll();
            }

            // Load Loading Screen
            if (loadingSceneSpecified)
            {
                yield return SceneManager.LoadSceneAsync(loadingSceneName, loadingSceneLoadSceneMode);

                yield return StartCoroutine(Fade(0, m_OverlayFadeDuration));
            }
            else
            {
                if (!additive)
                {
                    for (int i = 0; i < SceneManager.sceneCount; i++)
                    {
                        yield return SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
                    }
                }
            }

            // Load New Scenes By Name
            if (sceneNamesToLoad != null)
            {
                foreach (string sceneNameToLoad in sceneNamesToLoad)
                {
                    yield return SceneManager.LoadSceneAsync(sceneNameToLoad, LoadSceneMode.Additive);
                }

                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneNamesToLoad[0]));
            }
            // Load New Scenes By Index
            if (sceneIndicesToLoad != null)
            {
                foreach (int sceneIndexToLoad in sceneIndicesToLoad)
                {
                    yield return SceneManager.LoadSceneAsync(sceneIndexToLoad, LoadSceneMode.Additive);
                }

                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndicesToLoad[0]));
            }

            onScenesLoaded?.Invoke();

            // Unload Loading Screen
            if (loadingSceneSpecified)
            {
                yield return new WaitForSeconds(minLoadingSceneDuration);

                yield return StartCoroutine(Fade(1, m_OverlayFadeDuration));

                Scene loadingScene = SceneManager.GetSceneByName(loadingSceneName);

                if (loadingScene.IsValid())
                {
                    yield return SceneManager.UnloadSceneAsync(loadingSceneName);
                }
            }

            // Fade to new screen
            yield return StartCoroutine(Fade(0, m_OverlayFadeDuration));

            // Destroy this SceneLoader object when the scene is done loading
            Destroy(this.gameObject);

            onComplete?.Invoke();
        }

        public IEnumerator Fade(float endAlpha, float duration)
        {
            float alpha = LoadingOverlayImage.color.a;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / duration;
                Color spriteColor = LoadingOverlayImage.color;
                spriteColor.a = Mathf.Lerp(alpha, endAlpha, t);
                LoadingOverlayImage.color = spriteColor;
                yield return null;
            }
        }
    }

}
