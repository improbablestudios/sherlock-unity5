﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SceneUtilities
{

    public enum SceneStatus
    {
        WIP,
        TEST,
        FINAL,
    }

    [Serializable]
    public class SceneOption
    {
        [Tooltip("The scene path")]
        [FormerlySerializedAs("path")]
        [FormerlySerializedAs("m_Path")]
        public string m_ScenePath;

        [Tooltip("The scene name")]
        [FormerlySerializedAs("name")]
        [FormerlySerializedAs("m_Name")]
        public string m_SceneName;

        [Tooltip("The scene index")]
        [FormerlySerializedAs("sceneIndex")]
        public int m_SceneIndex;

        [Tooltip("The scene status")]
        [FormerlySerializedAs("status")]
        [FormerlySerializedAs("m_Status")]
        public SceneStatus m_SceneStatus;

        public string OptionText
        {
            get
            {
                return "[" + (int)m_SceneStatus + ". " + m_SceneStatus.ToString() + "] " + m_SceneName;
            }
        }
    }

    [AddComponentMenu("UI/Scene Selector")]
    public class SceneSelector : PersistentBehaviour
    {
        [Tooltip("The dropdown used to select a scene to load")]
        [SerializeField]
        [FormerlySerializedAs("dropdown")]
        protected Dropdown m_Dropdown;

        [Tooltip("The different scene options")]
        [SerializeField]
        [FormerlySerializedAs("sceneOptions")]
        protected List<SceneOption> m_SceneOptions = new List<SceneOption>();
        
        public Dropdown Dropdown
        {
            get
            {
                return m_Dropdown;
            }
            set
            {
                m_Dropdown = value;
            }
        }
        
        public List<SceneOption> SceneOptions
        {
            get
            {
                return m_SceneOptions;
            }
            set
            {
                m_SceneOptions = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Dropdown == null)
            {
                m_Dropdown = GetComponent<Dropdown>();
            }
        }

        protected virtual void Start()
        {
            SortSceneOptions();
            PopulateDropdown();
            m_Dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(int index)
        {
            int sceneIndex = m_SceneOptions[index].m_SceneIndex;
            SceneLoadingManager.Instance.LoadScene(sceneIndex, false, true);
        }

        public void SortSceneOptions()
        {
            m_SceneOptions = m_SceneOptions.OrderByDescending(s => s.m_SceneStatus).ThenBy(n => n.m_SceneName).ToList();
            int indexOfMainSceneOption = m_SceneOptions.FindIndex(i => i.m_SceneIndex == 0);
            if (indexOfMainSceneOption >= 0)
            {
                SceneOption mainSceneOption = m_SceneOptions[indexOfMainSceneOption];
                m_SceneOptions[indexOfMainSceneOption] = m_SceneOptions[0];
                m_SceneOptions[0] = mainSceneOption;
            }
        }

        public void PopulateDropdown()
        {
            List<Dropdown.OptionData> dropdownOptions = new List<Dropdown.OptionData>();
            foreach (SceneOption sceneOption in m_SceneOptions)
            {
                Dropdown.OptionData option = new Dropdown.OptionData();
                option.text = sceneOption.OptionText;
                dropdownOptions.Add(option);
            }

            m_Dropdown.options.Clear();
            m_Dropdown.AddOptions(dropdownOptions);
            m_Dropdown.value = m_SceneOptions.FindIndex(i => i.m_SceneIndex == SceneManager.GetActiveScene().buildIndex);
        }
    }

}