#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System;

namespace ImprobableStudios.SceneUtilities
{
    
    [InitializeOnLoad]
    public static class EditorSceneMemoryManager
    {
        static EditorSceneMemoryManager()
        {
            EditorSceneManager.sceneOpened += OnSceneOpened;
        }

        static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            GarbageCollect();
        }
        
        static void GarbageCollect()
        {
            EditorUtility.UnloadUnusedAssetsImmediate();
            GC.Collect();
        }
    }

}
#endif