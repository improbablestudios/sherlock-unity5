#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SceneUtilities
{

    public class BuildUtilities
    {
        public static EditorBuildSettingsScene[] GetScenesInBuild(bool onlyEnabled)
        {
            if (onlyEnabled)
            {
                return (from scene in EditorBuildSettings.scenes where scene.enabled select scene).ToArray();
            }
            else
            {
                return EditorBuildSettings.scenes;
            }
        }

        public static void AddSceneToBuild(string path)
        {
            List<EditorBuildSettingsScene> scenes = EditorBuildSettings.scenes.ToList();
            bool sceneIsInBuild = false;
            foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            {
                if (scene.path == path)
                {
                    sceneIsInBuild = true;
                }
            }
            if (!sceneIsInBuild)
            {
                EditorBuildSettingsScene newScene = new EditorBuildSettingsScene(path, true);
                scenes.Add(newScene);
                EditorBuildSettings.scenes = scenes.ToArray();
            }
        }

        public static void RemoveSceneFromBuild(string path)
        {
            List<EditorBuildSettingsScene> scenes = EditorBuildSettings.scenes.ToList();
            bool sceneIsInBuild = false;
            foreach (EditorBuildSettingsScene scene in scenes)
            {
                if (scene.path == path)
                {
                    sceneIsInBuild = true;
                }
            }
            if (sceneIsInBuild)
            {
                scenes.RemoveAll(i => (i.path == path));
                EditorBuildSettings.scenes = scenes.ToArray();
            }
        }

        public static string GetSceneName(EditorBuildSettingsScene scene)
        {
            return GetSceneName(scene.path);
        }

        public static string GetSceneName(string scenePath)
        {
            string name = scenePath.Substring(scenePath.LastIndexOf('/') + 1);
            name = name.Substring(0, name.Length - 6);
            return name;
        }

        public static string[] GetScenePaths(bool onlyEnabled)
        {
            List<string> sceneList = new List<string>();
            foreach (UnityEditor.EditorBuildSettingsScene scene in GetScenesInBuild(onlyEnabled))
            {
                if (scene.enabled)
                {
                    sceneList.Add(scene.path);
                }
            }
            return sceneList.ToArray();
        }

        public static void UpdateSceneSelectors()
        {
            foreach (SceneSelector sceneSelector in Resources.FindObjectsOfTypeAll<SceneSelector>())
            {
                UpdateSceneSelectorOptions(sceneSelector);
                EditorUtility.SetDirty(sceneSelector);
            }
        }

        public static void UpdateSceneSelectorOptions(SceneSelector sceneSelector)
        {
            EditorBuildSettingsScene[] scenesInBuild = GetScenesInBuild(true);
            List<SceneOption> currentSceneOptions = sceneSelector.SceneOptions.ToList();
            List<SceneOption> newSceneOptions = new List<SceneOption>();
            for (int i = 0; i < scenesInBuild.Length; i++)
            {
                EditorBuildSettingsScene scene = scenesInBuild[i];
                string name = GetSceneName(scene);
                SceneOption correspondingSceneOption = currentSceneOptions.Find(x => x.m_ScenePath == scene.path);
                if (correspondingSceneOption == null)
                {
                    SceneOption sceneOption = new SceneOption();
                    sceneOption.m_ScenePath = scene.path;
                    sceneOption.m_SceneName = name;
                    sceneOption.m_SceneIndex = i;
                    newSceneOptions.Add(sceneOption);
                }
                else if (newSceneOptions.Find(x => x.m_ScenePath == scene.path) == null)
                {
                    correspondingSceneOption.m_ScenePath = scene.path;
                    correspondingSceneOption.m_SceneName = name;
                    correspondingSceneOption.m_SceneIndex = i;
                    newSceneOptions.Add(correspondingSceneOption);
                }
            }
            sceneSelector.SceneOptions = newSceneOptions;
            sceneSelector.SortSceneOptions();
            sceneSelector.PopulateDropdown();
        }
    }

}
#endif