#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SceneUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(SceneSelector), true)]
    public class SceneSelectorEditor : PersistentBehaviourEditor
    {
        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            SceneSelector t = target as SceneSelector;

            if (property.name == "m_" + nameof(t.SceneOptions))
            {
                EditorGUI.BeginChangeCheck();

                base.DrawProperty(property, label, options);

                if (GUILayout.Button("Update Scenes") || EditorGUI.EndChangeCheck())
                {
                    BuildUtilities.UpdateSceneSelectorOptions(t);
                    EditorUtility.SetDirty(t);
                }
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }
    }

}
#endif