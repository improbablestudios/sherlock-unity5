﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.SceneUtilities
{

    public class SceneLoadingManagerSettingsProvider : BaseAssetSettingsProvider<SceneLoadingManager>
    {
        public SceneLoadingManagerSettingsProvider() : base("Project/Managers/Scene Loading Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new SceneLoadingManagerSettingsProvider();
        }
    }

    public class SceneLoadingManagerSettingsBuilder : BaseAssetSettingsBuilder<SceneLoadingManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif