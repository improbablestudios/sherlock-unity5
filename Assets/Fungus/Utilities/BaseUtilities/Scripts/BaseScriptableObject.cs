﻿using UnityEngine;
using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.BaseUtilities
{
    
    public abstract class BaseScriptableObject : ScriptableObject
    {
        public delegate void PropertyChangedEventHandler();

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private static Dictionary<Type, List<BaseScriptableObject>> s_activeInstances = new Dictionary<Type, List<BaseScriptableObject>>();

        private string[] m_errors;

        protected virtual void OnValidate()
        {
        }

        protected virtual void OnEnable()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged += OnPlayModeStateChange;
#endif
            AddToActiveList();
        }

        protected virtual void OnDisable()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged -= OnPlayModeStateChange;
#endif
            RemoveFromActiveList();
        }

        protected virtual void OnDestroy()
        {
            ReleaseReferences();
        }
        
#if UNITY_EDITOR
        protected virtual void OnPlayModeStateChange(UnityEditor.PlayModeStateChange playModeState)
        {
            if (playModeState == UnityEditor.PlayModeStateChange.ExitingEditMode || playModeState == UnityEditor.PlayModeStateChange.EnteredEditMode)
            {
                ResetState();
            }
        }
#endif

        protected virtual void ResetState()
        {
        }

        protected virtual void OnPropertyChanged(string propertyPath)
        {
        }

        private void AddToActiveList()
        {
            Type type = GetType();

            if (s_activeInstances.ContainsKey(type) && s_activeInstances[type] != null)
            {
                foreach (Type t in type.GetInheritanceHierarchy(typeof(BaseScriptableObject)))
                {
                    s_activeInstances[t].Add(this);
                }
                foreach (Type t in type.GetInterfaces())
                {
                    s_activeInstances[t].Add(this);
                }
            }
            else
            {
                foreach (Type t in type.GetInheritanceHierarchy(typeof(BaseScriptableObject)))
                {
                    s_activeInstances[t] = new List<BaseScriptableObject>() { this };
                }
                foreach (Type t in type.GetInterfaces())
                {
                    s_activeInstances[t] = new List<BaseScriptableObject>() { this };
                }
            }
        }

        private void RemoveFromActiveList()
        {
            Type type = GetType();

            if (s_activeInstances.ContainsKey(type) && s_activeInstances[type] != null)
            {
                foreach (Type t in type.GetInheritanceHierarchy(typeof(BaseScriptableObject)))
                {
                    s_activeInstances[t].Remove(this);
                }
                foreach (Type t in type.GetInterfaces())
                {
                    s_activeInstances[t].Remove(this);
                }
            }
            else
            {
                foreach (Type t in type.GetInheritanceHierarchy(typeof(BaseScriptableObject)))
                {
                    s_activeInstances[t] = new List<BaseScriptableObject>();
                }
                foreach (Type t in type.GetInterfaces())
                {
                    s_activeInstances[t] = new List<BaseScriptableObject>();
                }
            }
        }

        /// <summary>
        /// Releases all references so that they can be garbage collected.
        /// </summary>
        public void ReleaseReferences()
        {
            foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                Type fieldType = field.FieldType;
                if (typeof(IList).IsAssignableFrom(fieldType))
                {
                    IList list = field.GetValue(this) as IList;
                    if (list != null)
                    {
                        list.Clear();
                    }
                }

                if (typeof(IDictionary).IsAssignableFrom(fieldType))
                {
                    IDictionary dictionary = field.GetValue(this) as IDictionary;
                    if (dictionary != null)
                    {
                        dictionary.Clear();
                    }
                }

                if (!fieldType.IsPrimitive)
                {
                    field.SetValue(this, null);
                }
            }
        }

        public static T[] GetAllActive<T>() where T : BaseScriptableObject
        {
            Type type = typeof(T);

            if (s_activeInstances.ContainsKey(type))
            {
                return s_activeInstances[type].Cast<T>().ToArray();
            }
            return new T[0];
        }

        public static void SetPrimaryActive<T>(T baseBehaviour) where T : BaseScriptableObject
        {
            SetPrimaryActive(typeof(T), baseBehaviour);
        }

        public static void SetPrimaryActive(Type type, BaseScriptableObject baseBehaviour)
        {
            if (s_activeInstances.ContainsKey(type))
            {
                MoveItemToFront(s_activeInstances[type], baseBehaviour);
            }
            else
            {
                s_activeInstances[type] = new List<BaseScriptableObject>() { baseBehaviour };
            }
        }

        public static T GetPrimaryActive<T>() where T : BaseScriptableObject
        {
            Type type = typeof(T);

            return GetPrimaryActive(type) as T;
        }

        public static BaseScriptableObject GetPrimaryActive(Type type)
        {
            if (s_activeInstances.ContainsKey(type) && s_activeInstances[type].Count > 0)
            {
                return s_activeInstances[type][0];
            }

            return null;
        }

        private static void MoveItemToFront<T>(List<T> list, T item)
        {
            int index = list.IndexOf(item);
            if (index >= 0)
            {
                MoveItemAtIndexToFront(list, index);
            }
            else
            {
                list.Insert(0, item);
            }
        }

        private static void MoveItemAtIndexToFront<T>(List<T> list, int index)
        {
            if (index >= 0 && index < list.Count)
            {
                T item = list[index];
                list.RemoveAt(index);
                list.Insert(0, item);
            }
        }

        /// <summary>
        /// Returns a string specifying the error for the specified property.
        /// Otherwise, if there is no error, returns null.
        /// </summary>
        /// <param name="propertyPath">The property path that represents a serialized field</param>
        /// <returns>If the property has an error, return the error message. Otherwise, return null</returns>
        public virtual string GetPropertyError(string propertyPath)
        {
            FieldInfo field = GetType().GetField(propertyPath, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            if (field != null)
            {
                string error = GetFieldErrorString(field);
                if (error != null)
                {
                    return error;
                }
            }

            return null;
        }
        
        public virtual void InvokePropertyChanged(string propertyPath)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () => HandleOnPropertyChanged(propertyPath);
#else
            HandleOnPropertyChanged(propertyPath);
#endif
        }

        private void HandleOnPropertyChanged(string propertyPath)
        {
            CacheErrors();
            PropertyChanged();
            if (this != null)
            {
                OnPropertyChanged(propertyPath);
            }
        }

        /// <summary>
        /// Returns true if the specified property should be displayed in the inspector.
        /// This is useful for hiding certain properties based on the value of another property.
        /// </summary>
        /// <param name="propertyPath">The property path that represents a serialized field</param>
        /// <returns>True, if the property should be visible in the inspector. Otherwise, false.</returns>
        public virtual bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == "m_Script")
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns true if the specified property can be edited in the inspector.
        /// This is useful for making certain properties uneditable based on the value of another property.
        /// </summary>
        /// <param name="propertyPath">The property path that represents a serialized field</param>
        /// <returns>True, if the property should be editable in the inspector. Otherwise, false.</returns>
        public virtual bool IsPropertyEditable(string propertyPath)
        {
            if (propertyPath == "m_Script")
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a number that represents where this property should appear in the inspector (lower number = top, higher number = bottom, 0 = default position)
        /// This is useful for reordering the properties in the inspector (like when moving the properties of base classes to the end of a child class)
        /// </summary>
        /// <param name="propertyPath">The property path that represents a serialized field</param>
        /// <returns>The priority of the property (lower number = top, higher number = bottom, 0 = default position)</returns>
        public virtual int GetPropertyOrder(string propertyPath)
        {
            return 0;
        }

        /// <summary>
        /// Returns the indent level offset for the specified property.
        /// This is useful for grouping certain properties.
        /// </summary>
        /// <param name="propertyPath">The property path that represents a serialized field</param>
        /// <returns>The indent level of the property (0 = default indent)</returns>
        public virtual int GetPropertyIndentLevel(string propertyPath)
        {
            return 0;
        }

        public virtual void FixReferences()
        {
        }

        public void CacheErrors()
        {
            m_errors = GetErrors();
        }

        public virtual string[] GetCachedErrors()
        {
            if (m_errors == null)
            {
                m_errors = GetErrors();
            }

            return m_errors;
        }

        public virtual string GetFirstCachedError()
        {
            if (m_errors == null)
            {
                m_errors = GetErrors();
            }

            if (m_errors.Length > 0)
            {
                return m_errors[0];
            }

            return null;
        }

        public virtual string[] GetErrors()
        {
            List<string> errors = new List<string>();
            foreach (FieldInfo field in GetType().GetUnitySerializedFields())
            {
                string error = null;

                error = GetPropertyError(field.Name);
                if (error != null)
                {
                    errors.Add(error);
                }
            }
            return errors.ToArray();
        }

        public virtual string GetFirstError()
        {
            foreach (FieldInfo field in GetType().GetUnitySerializedFields())
            {
                string error = null;

                error = GetPropertyError(field.Name);
                if (error != null)
                {
                    return error;
                }
            }
            return null;
        }

        public string GetFieldErrorString(FieldInfo field)
        {
            return GetFieldErrorString(field, "{0}");
        }

        public string GetFieldErrorString(FieldInfo field, string errorInfoFormat)
        {
            string[] errorMessages = GetFieldErrors(field);

            if (errorMessages.Length > 0)
            {
                string[] nonEmptyErrorMessages = errorMessages.Where(i => !string.IsNullOrEmpty(i)).ToArray();
                if (nonEmptyErrorMessages.Length > 0)
                {
                    return string.Format(errorInfoFormat, string.Join(",", nonEmptyErrorMessages));
                }
                else
                {
                    return "";
                }
            }

            return null;
        }

        protected virtual string[] GetFieldErrors(FieldInfo field)
        {
            CheckNotNullAttribute checkNotNullAttribute = field.GetAttribute<CheckNotNullAttribute>(false);
            CheckNotEmptyAttribute checkNotEmptyAttribute = field.GetAttribute<CheckNotEmptyAttribute>(false);

            List<string> errorMessages = new List<string>();

            if (checkNotNullAttribute != null)
            {
                string errorMessage = checkNotNullAttribute.GetError(field, this);
                if (errorMessage != null)
                {
                    errorMessages.Add(errorMessage);
                }
            }

            if (checkNotEmptyAttribute != null)
            {
                string errorMessage = checkNotEmptyAttribute.GetError(field, this);
                if (errorMessage != null)
                {
                    errorMessages.Add(errorMessage);
                }
            }

            return errorMessages.ToArray();
        }
    }
}