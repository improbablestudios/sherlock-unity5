﻿using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{
    public static class PersistableLoaderExtensions
    {
        public static void LoadAndInitializePersistableFields(this IPersistableLoader persistableLoader)
        {
            LoadPersistableFields(persistableLoader);
            InitializePersistableFields(persistableLoader);
        }

        public static void LoadPersistableFields(this IPersistableLoader persistableLoader)
        {
            LoadFields(typeof(UnityEngine.Object), LoadPersistableField, persistableLoader);
        }

        public static void InitializePersistableFields(this IPersistableLoader persistableLoader)
        {
            persistableLoader.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object dObj, object[] p, MemberInfo dMem) => MemberShouldBeLoaded(md, mp, hc, m, mt, dObj, p, dMem, typeof(UnityEngine.Object)),
                InitializePersistableField,
                false);
        }

        public static void LoadPersistableField<T>(this T persistableLoader, Expression<Func<T, object>> memberAccess) where T : IPersistableLoader
        {
            MemberInfo member = memberAccess.GetMember();
            LoadPersistableField(-1, "", false, member, member.GetMemberType(), persistableLoader, null, null);
        }

        public static void InitializePersistableField<T>(this T persistableLoader, Expression<Func<T, object>> memberAccess) where T : IPersistableLoader
        {
            MemberInfo member = memberAccess.GetMember();
            InitializePersistableField(-1, "", false, member, member.GetMemberType(), persistableLoader, null, null);
        }

        private static void LoadPersistableField(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            UnityEngine.Object unityEngineObj = member.GetMemberValue(declaringObj, propertyParameters) as UnityEngine.Object;
            if (unityEngineObj != null)
            {
                UnityEngine.Object loadedUnityEngineObj = LoadPersistentInstance(unityEngineObj);
                member.SetMemberValue(declaringObj, loadedUnityEngineObj, propertyParameters);
            }
        }

        private static void InitializePersistableField(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            UnityEngine.Object unityEngineObj = member.GetMemberValue(declaringObj, propertyParameters) as UnityEngine.Object;
            if (unityEngineObj != null)
            {
                IPersistable persistable = GetPersistableFromObject(unityEngineObj);
                if (persistable != null)
                {
                    persistable.Initialize();
                }
            }
        }

        private static bool MemberShouldBeLoaded(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Type typeToLoad)
        {
            DontInstantiateAttribute dontInstantiateAttribute = member.GetCustomAttributes(true).ToList().OfType<DontInstantiateAttribute>().FirstOrDefault();
            if (dontInstantiateAttribute != null)
            {
                return false;
            }
            return UnityReflectionExtensions.MemberShouldBeLoaded(memberDepth, memberPath, hasChildren, member, memberType, declaringObj, propertyParameters, declaringMember, typeToLoad);
        }

        public static void LoadFields(Type typeToLoad, MemberAction onLoadField, object childObj)
        {
            childObj.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                (int md, string mp, bool hc, MemberInfo m, Type mt, object dObj, object[] p, MemberInfo dMem) => MemberShouldBeLoaded(md, mp, hc, m, mt, dObj, p, dMem, typeToLoad),
                onLoadField,
                false);
        }
        
        public static T LoadPersistentInstance<T>(T obj) where T : UnityEngine.Object
        {
            return LoadPersistentInstance(obj as UnityEngine.Object) as T;
        }

        public static UnityEngine.Object LoadPersistentInstance(UnityEngine.Object obj)
        {
#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return obj;
            }
#endif
            if (obj == null)
            {
                return null;
            }

            IPersistable persistable = null;

            Component c = obj as Component;
            if (c != null)
            {
                if (c.gameObject.scene.IsValid())
                {
                    return obj;
                }
                else
                {
                    if (c is IPersistable)
                    {
                        persistable = c as IPersistable;
                    }
                    else
                    {
                        persistable = c.GetComponent<IPersistable>();
                    }
                }
            }

            GameObject go = obj as GameObject;
            if (go != null)
            {
                if (go.scene.IsValid())
                {
                    return obj;
                }
                else
                {
                    persistable = go.GetComponent<IPersistable>();
                }
            }

            ScriptableObject so = obj as ScriptableObject;
            if (so != null)
            {
                if (so is IPersistable)
                {
                    persistable = so as IPersistable;
                }
                else
                {
                    return obj;
                }
            }
            
            if (persistable == null)
            {
                return obj;
            }

            UnityEngine.Object persistentSource = persistable as UnityEngine.Object;
            UnityEngine.Object persistentInstance = persistable.GetPersistentInstance() as UnityEngine.Object;
            
            if (persistentInstance == null)
            {
                if (persistable.IsSource())
                {
                    persistentInstance = persistentSource;
                }
                else
                {
                    persistentInstance = ResourceTracker.GetCachedInstanceObject(obj.GetType(), persistentSource);
                    if (persistentInstance == null) // Not found in scene
                    {
                        if (persistable != null)
                        {
                            persistentInstance = persistable.Instantiate() as UnityEngine.Object;
                        }
                    }
                }

                persistable.SetPersistentInstance(persistentInstance as IPersistable);
            }

            return GetObjectFromPersistable(obj, persistentSource, persistentInstance);
        }
        
        public static UnityEngine.Object GetObjectFromPersistable(UnityEngine.Object sourceObj, UnityEngine.Object persistentSource, UnityEngine.Object peristentInstance)
        {
            if (sourceObj == null)
            {
                return null;
            }

            if (peristentInstance == null)
            {
                return null;
            }

            if (sourceObj is IPersistable)
            {
                return peristentInstance as UnityEngine.Object;
            }
            else if (sourceObj is Component)
            {
                Type type = sourceObj.GetType();

                Component persistentSourceComponent = persistentSource as Component;
                Component persistentInstanceComponent = peristentInstance as Component;

                if (persistentSourceComponent != null && persistentInstanceComponent != null)
                {
                    Component[] componentsOfSameTypeOnSource = persistentSourceComponent.GetComponents(type);
                    Component[] componentsOfSameTypeOnTarget = persistentInstanceComponent.GetComponents(type);

                    if (componentsOfSameTypeOnSource.Length == componentsOfSameTypeOnTarget.Length)
                    {
                        int index = Array.IndexOf(componentsOfSameTypeOnSource, sourceObj);
                        if (index >= 0)
                        {
                            return componentsOfSameTypeOnTarget[index];
                        }
                    }

                    return persistentInstanceComponent.GetComponent(sourceObj.GetType());
                }
            }
            else if (sourceObj is GameObject)
            {
                Component persistentInstanceComponent = peristentInstance as Component;
                if (persistentInstanceComponent != null)
                {
                    return persistentInstanceComponent.gameObject;
                }
            }
            return null;
        }
        
        public static IPersistable GetPersistableFromObject(UnityEngine.Object obj)
        {
            if (obj == null)
            {
                return null;
            }

            IPersistable persistable = obj as IPersistable;
            if (persistable != null)
            {
                return persistable;
            }

            Component component = obj as Component;
            if (component != null)
            {
                return component.GetComponent<IPersistable>();
            }

            GameObject gameObject = obj as GameObject;
            if (gameObject != null)
            {
                return gameObject.GetComponent<IPersistable>();
            }

            return null;
        }
    }
}
