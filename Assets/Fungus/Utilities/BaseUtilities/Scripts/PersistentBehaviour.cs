﻿using System;
using UnityEngine;
using System.Linq;
using ImprobableStudios.ResourceTrackingUtilities;

namespace ImprobableStudios.BaseUtilities
{

    [AddComponentMenu("Base Utilities/Persistent Behaviour")]
    public class PersistentBehaviour : IdentifiableBehaviour, IPersistable, IPersistableLoader
    {
        protected static Transform s_persistentBehavioursParent;

        protected IPersistable m_persistentInstance;

        protected GameObject m_prefabRoot;

        protected GameObject m_instanceRoot;
        
        protected bool m_rootInitialized;

        protected bool m_isInitialized;
        
        public Transform PersistentBehaviourInstancesRoot
        {
            get
            {
                if (transform.root.GetComponent<DontDestroyOnLoad>() != null)
                {
                    return PersistentBehaviourInstancesApplicationRoot;
                }
                else
                {
                    return PersistentBehaviourInstancesSceneRoot;
                }
            }
        }

        public static Transform PersistentBehaviourInstancesApplicationRoot
        {
            get
            {
                if (s_persistentBehavioursParent == null)
                {
                    s_persistentBehavioursParent = new GameObject("PersistentBehaviours (Application)").transform;
                    DontDestroyOnLoad(s_persistentBehavioursParent);
                }
                return s_persistentBehavioursParent;
            }
        }

        public static Transform PersistentBehaviourInstancesSceneRoot
        {
            get
            {
                if (s_persistentBehavioursParent == null)
                {
                    s_persistentBehavioursParent = new GameObject("PersistentBehaviours (Scene)").transform;
                }
                return s_persistentBehavioursParent;
            }
        }

        public IPersistable PersistentInstance
        {
            get
            {
                return m_persistentInstance;
            }
        }

        protected override void Awake()
        {
            base.Awake();

            if (WillLoadPersistableFieldsOnAwake())
            {
                this.LoadPersistableFields();
                if (WillInitializePersistableFieldsOnAwake())
                {
                    this.InitializePersistableFields();
                }
            }
        }

        protected override void OnDestroy()
        {
            ResourceTracker.RemoveFromCache(this);

            base.OnDestroy();
        }

        protected virtual void OnInstantiate()
        {
        }

        protected virtual void OnInitialize()
        {
        }

        public IPersistable Instantiate()
        {
            GameObject persistentBehaviourPrefabRoot = transform.root.gameObject;

            Transform typeParent = GetTypeParent(persistentBehaviourPrefabRoot);

            // Save current prefab active state
            bool isActive = persistentBehaviourPrefabRoot.activeSelf;

            // Ensure that instance starts inactive in scene
            persistentBehaviourPrefabRoot.SetActive(false);

            GameObject persistentBehaviourRootInstance = Instantiate(persistentBehaviourPrefabRoot, typeParent);
            persistentBehaviourRootInstance.name = persistentBehaviourPrefabRoot.name;

            PersistentBehaviour persistentBehaviourInstance = null;
            
            foreach (Component c in persistentBehaviourRootInstance.GetComponentsInChildren<Component>(true))
            {
                Transform t = c as Transform;
                if (t != null)
                {
                    ResourceTracker.AddToCache(t.gameObject);
                }

                ResourceTracker.AddToCache(c);

                PersistentBehaviour pb = c as PersistentBehaviour;
                if (pb != null)
                {
                    pb.OnInstantiate();
                    pb.m_prefabRoot = persistentBehaviourPrefabRoot;
                    pb.m_instanceRoot = persistentBehaviourRootInstance;

                    if (m_Key == pb.Key)
                    {
                        persistentBehaviourInstance = pb;
                    }
                }
            }

            // Revert prefab active state
            persistentBehaviourPrefabRoot.SetActive(isActive);

            return persistentBehaviourInstance;
        }

        public Transform GetTypeParent(GameObject root)
        {
            Type type = null;

            PersistentBehaviour persistentBehaviour = root.GetComponent<PersistentBehaviour>();
            if (persistentBehaviour != null)
            {
                type = persistentBehaviour.GetType();
            }
            else
            {
                type = root.GetComponents<Component>().Select(i => i.GetType()).First(i => !typeof(Transform).IsAssignableFrom(i));
            }

            if (type == null)
            {
                type = typeof(Transform);
            }

            string parentName = type.Name;

            Transform typeParentTransform = PersistentBehaviourInstancesRoot.Find(parentName);

            if (typeParentTransform == null)
            {
                typeParentTransform = new GameObject(parentName).transform;
                typeParentTransform.transform.parent = PersistentBehaviourInstancesRoot;
            }

            return typeParentTransform;
        }

        public static T InstantiateAndRegister<T>(T original, Vector3 position, Quaternion rotation) where T : UnityEngine.Object
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            T newObj = Instantiate(original, position, rotation);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static T InstantiateAndRegister<T>(T original, Transform parent, bool worldPositionStays) where T : UnityEngine.Object
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            T newObj = Instantiate(original, parent, worldPositionStays);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static T InstantiateAndRegister<T>(T original, Transform parent) where T : UnityEngine.Object
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            T newObj = Instantiate(original, parent);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static T InstantiateAndRegister<T>(T original, Vector3 position, Quaternion rotation, Transform parent) where T : UnityEngine.Object
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            T newObj = Instantiate(original, position, rotation, parent);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static T InstantiateAndRegister<T>(T original) where T : UnityEngine.Object
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            T newObj = Instantiate(original);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static UnityEngine.Object InstantiateAndRegister(UnityEngine.Object original, Vector3 position, Quaternion rotation, Transform parent)
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            UnityEngine.Object newObj = Instantiate(original, position, rotation, parent);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static UnityEngine.Object InstantiateAndRegister(UnityEngine.Object original, Transform parent)
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            UnityEngine.Object newObj = Instantiate(original, parent);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static UnityEngine.Object InstantiateAndRegister(UnityEngine.Object original)
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            UnityEngine.Object newObj = Instantiate(original);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static UnityEngine.Object InstantiateAndRegister(UnityEngine.Object original, Vector3 position, Quaternion rotation)
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            UnityEngine.Object newObj = Instantiate(original, position, rotation);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }

        public static UnityEngine.Object InstantiateAndRegister(UnityEngine.Object original, Transform parent, bool instantiateInWorldSpace)
        {
            bool isActive = original.GetGameObject().activeSelf;
            original.GetGameObject().SetActive(false);
            UnityEngine.Object newObj = Instantiate(original, parent, instantiateInWorldSpace);
            Register(newObj);
            original.GetGameObject().SetActive(isActive);
            newObj.GetGameObject().SetActive(isActive);
            return newObj;
        }
        
        public static void Register(UnityEngine.Object obj)
        {
            foreach (Component c in obj.GetGameObject().GetComponentsInChildren<Component>(true))
            {
                Transform t = c as Transform;
                if (t != null)
                {
                    ResourceTracker.AddToCache(t.gameObject);
                }

                ResourceTracker.AddToCache(c);

                PersistentBehaviour pb = obj as PersistentBehaviour;
                if (pb != null)
                {
                    pb.OnInstantiate();
                }
            }
        }
        
        public void Initialize()
        {
            if (!m_rootInitialized)
            {
                if (m_prefabRoot != null && m_instanceRoot != null)
                {
                    m_instanceRoot.SetActive(m_prefabRoot.activeSelf);
                    foreach (PersistentBehaviour pb in m_instanceRoot.transform.GetComponentsInChildren<PersistentBehaviour>(true))
                    {
                        pb.m_rootInitialized = true;
                    }
                }
                m_rootInitialized = true;
            }

            if (!m_isInitialized)
            {
                OnInitialize();
                m_isInitialized = true;
            }
        }

        public bool IsSource()
        {
            return gameObject.scene.IsValid();
        }

        public IPersistable GetPersistentInstance()
        {
            return m_persistentInstance;
        }

        public void SetPersistentInstance(IPersistable persistentInstance)
        {
            m_persistentInstance = persistentInstance;
        }

        public override string ToString()
        {
            return name;
        }
        
        protected virtual bool WillLoadPersistableFieldsOnAwake()
        {
            return false;
        }

        protected virtual bool WillInitializePersistableFieldsOnAwake()
        {
            return true;
        }
    }

}