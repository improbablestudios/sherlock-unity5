﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.BaseUtilities
{

    public class PreviewManager : PersistentBehaviour
    {
        private static PreviewManager s_instance;
        
        protected static PreviewManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    PreviewManager[] instances = Resources.FindObjectsOfTypeAll<PreviewManager>();
                    foreach (PreviewManager clipboard in instances)
                    {
                        s_instance = instances[0];
                        break;
                    }
                }
                if (s_instance == null)
                {
                    s_instance = new GameObject("~PreviewManager").AddComponent<PreviewManager>();
                    s_instance.gameObject.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
                    s_instance.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
                }
                return s_instance;
            }
        }

        PreviewManager()
        {
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        
        protected void OnSceneWasLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            DestroyImmediate(this);
        }

        private void DestroyPreviewObjects()
        {
#if UNITY_EDITOR
            if (this != null)
            {
                foreach (Transform child in GetComponentsInChildren<Transform>(true))
                {
                    if (child != this.transform)
                    {
                        try
                        {
                            if (child != null)
                            {
                                DestroyImmediate(child.gameObject);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
#endif
        }

#if UNITY_EDITOR
        public static T GetPreviewObject<T>(T previewObject, T sourceObject) where T : UnityEngine.Object
        {
            if (s_instance == null)
            {
                s_instance = FindObjectOfType<PreviewManager>();
            }

            if (sourceObject != null)
            {
                if (previewObject == null || (PrefabUtility.GetCorrespondingObjectFromSource(previewObject) != null && PrefabUtility.GetCorrespondingObjectFromSource(previewObject) != sourceObject))
                {
                    ResourceTracker.CacheLoadedObjects();
                    previewObject = ResourceTracker.GetCachedInstanceObject(typeof(T), sourceObject) as T;
                    if (previewObject != null && previewObject.GetGameObject().GetComponentInParent<PreviewManager>(true) == null)
                    {
                        return null;
                    }
                    if (previewObject == null)
                    {
                        PreviewManager preview = PreviewManager.Instance;
                        previewObject = PrefabUtility.InstantiatePrefab(sourceObject) as T;
                        ResourceTracker.AddToCache(previewObject);
                        Transform previewTransform = null;
                        GameObject previewGameObject = previewObject as GameObject;
                        Component previewComponent = previewObject as Component;
                        if (previewGameObject != null)
                        {
                            previewTransform = previewGameObject.transform;
                        }
                        if (previewComponent != null)
                        {
                            previewTransform = previewComponent.transform;
                        }
                        foreach (Transform child in previewTransform.root.GetComponentsInChildren<Transform>(true))
                        {
                            child.gameObject.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
                        }
                        previewTransform.root.SetParent(preview.transform);
                    }
                }
            }
            return previewObject;
        }
#endif

        public bool IsPreviewObject(UnityEngine.Object obj)
        {
            if (PreviewManager.Instance != null)
            {
                if (obj.GetGameObject() != null && obj.GetGameObject().transform.IsChildOf(PreviewManager.Instance.transform))
                {
                    return true;
                }
            }

            return false;
        }
    }

}