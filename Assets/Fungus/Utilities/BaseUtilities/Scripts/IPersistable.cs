﻿namespace ImprobableStudios.BaseUtilities
{

    public interface IPersistable
    {
        bool IsSource();
        string GetKey();
        IPersistable GetPersistentInstance();
        void SetPersistentInstance(IPersistable persistentInstance);
        IPersistable Instantiate();
        void Initialize();
    }

}