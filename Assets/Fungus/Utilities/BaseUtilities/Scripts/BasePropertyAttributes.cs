﻿using ImprobableStudios.ReflectionUtilities;
using System.Reflection;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    public abstract class CheckFieldAttribute : PropertyAttribute
    {
        public CheckFieldAttribute()
        {
        }

        public CheckFieldAttribute(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        protected string ErrorMessage
        {
            get;
            set;
        }

        public abstract string GetError(FieldInfo field, object parentObj);
    }

    public class CheckNotNullAttribute : CheckFieldAttribute
    {
        public CheckNotNullAttribute() : base()
        {
        }

        public CheckNotNullAttribute(string errorMessage) : base(errorMessage)
        {
        }

        public override string GetError(FieldInfo field, object parentObj)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }

            if (field.IsMemberValueNull(parentObj))
            {
                return GetDefaultErrorMessage(field.Name);
            }

            return null;
        }

        public static string GetDefaultErrorMessage(string fieldName)
        {
            string errorFormat = "No {0} selected";
#if UNITY_EDITOR
            return string.Format(errorFormat, UnityEditor.ObjectNames.NicifyVariableName(fieldName));
#else
            return string.Format(errorFormat, fieldName);
#endif
        }
    }

    public class CheckNotEmptyAttribute : CheckFieldAttribute
    {
        public CheckNotEmptyAttribute() : base()
        {
        }

        public CheckNotEmptyAttribute(string errorMessage) : base(errorMessage)
        {
        }

        public override string GetError(FieldInfo field, object parentObj)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }

            if (field.IsMemberValueEmpty(parentObj))
            {
                return GetDefaultErrorMessage(field.Name);
            }

            return null;
        }

        public static string GetDefaultErrorMessage(string fieldName)
        {
            string errorFormat = "{0} is empty";
#if UNITY_EDITOR
            return string.Format(errorFormat, UnityEditor.ObjectNames.NicifyVariableName(fieldName));
#else
            return string.Format(errorFormat, fieldName);
#endif
        }
    }

}