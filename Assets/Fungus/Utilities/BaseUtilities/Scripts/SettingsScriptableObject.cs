﻿using System;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    public class SettingsScriptableObject<T> : IdentifiableScriptableObject
        where T : SettingsScriptableObject<T>
    {
        private static object s_lock = new object();

        private static T s_instance;

        public static T Instance
        {
            get
            {
                lock (s_lock)
                {
#if UNITY_EDITOR
                    if (s_instance == null)
                    {
                        s_instance = ImprobableStudios.ProjectSettingsUtilities.AssetSettingsConfig.GetActiveSettings<T>();
                    }
#else
                    if (s_instance == null)
                    {
                        s_instance = FindObjectOfType<T>();
                    }
#endif
                    if (s_instance == null)
                    {
                        s_instance = CreateInstance<T>();
                    }

                    return s_instance;
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

#if UNITY_EDITOR
            ImprobableStudios.ProjectSettingsUtilities.AssetSettingsConfig.OnActiveSettingsChanged += SetInstance;
#else
            s_instance = this as T;
#endif
        }

        protected override void OnDisable()
        {
            base.OnDisable();

#if UNITY_EDITOR
            ImprobableStudios.ProjectSettingsUtilities.AssetSettingsConfig.OnActiveSettingsChanged -= SetInstance;
#endif
        }

        private void SetInstance(Type type, ScriptableObject instance)
        {
            if (type.IsAssignableFrom(typeof(T)))
            {
                s_instance = instance as T;
            }
        }
    }

}
