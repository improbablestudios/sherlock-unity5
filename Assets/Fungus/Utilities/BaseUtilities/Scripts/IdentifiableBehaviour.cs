﻿using ImprobableStudios.ResourceTrackingUtilities;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    [AddComponentMenu("Base Utilities/Identifiable Behaviour")]
    public class IdentifiableBehaviour : BaseBehaviour, IIdentifiable
    {
        [Tooltip("A unique key that represents an instance of this monobehaviour")]
        [SerializeField]
        protected string m_Key = "";

        public string Key
        {
            get
            {
                return m_Key;
            }
        }

        protected virtual void Reset()
        {
            RegenerateKey();

            ResourceTracker.AddAllToCache(this);
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            RegenerateKey();

            ResourceTracker.AddToCache(this);
        }

        protected virtual void Awake()
        {
            ResourceTracker.AddAllToCache(this);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            ResourceTracker.AddAllToCache(this);
        }

        public virtual string GetKey()
        {
            if (string.IsNullOrEmpty(m_Key))
            {
                RegenerateKey();
            }

            return m_Key;
        }

        public void RegenerateKey()
        {
#if UNITY_EDITOR
            if (this != null)
            {
                if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
                {
                    string key = "";
                    if (this.TryGetUniqueKey(out key))
                    {
                        if (m_Key != key)
                        {
                            m_Key = key;
                            RecordChanges();
                        }
                    }
                }
            }
#endif
        }

        public virtual string GetName()
        {
            return name;
        }

        public override string ToString()
        {
            return GetName();
        }

        public virtual Texture2D GetIcon()
        {
            return null;
        }

        public virtual GUIStyle GetStyle()
        {
            return null;
        }

        public static IdentifiableBehaviour GetIdentifiableBehaviourFromObject(UnityEngine.Object obj)
        {
            IdentifiableBehaviour identifiableBehaviour = obj as IdentifiableBehaviour;
            Component component = obj as Component;
            GameObject gameObject = obj as GameObject;
            if (identifiableBehaviour != null)
            {
                return identifiableBehaviour;
            }
            else if (component != null)
            {
                IdentifiableBehaviour attachedIdentifiableBehaviour = component.GetComponent<IdentifiableBehaviour>();
                if (attachedIdentifiableBehaviour != null)
                {
                    return attachedIdentifiableBehaviour;
                }
                else
                {
                    return null;
                }
            }
            else if (gameObject != null)
            {
                return gameObject.GetComponent<IdentifiableBehaviour>();
            }
            return null;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Key))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override bool IsPropertyEditable(string propertyPath)
        {
            if (propertyPath == nameof(m_Key))
            {
                return false;
            }
            return true;
        }
    }

}