﻿using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Awake()
        {
            if (transform == transform.root)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }

}