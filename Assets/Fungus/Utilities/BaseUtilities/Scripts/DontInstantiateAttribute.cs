﻿using System;

namespace ImprobableStudios.BaseUtilities
{

    ///<summary>
    /// Use this attribute on fields in a PersistentBehaviour class to prevent 
    /// the instantiatiation of the asset reference on Awake or when directly calling LoadPersistentBehaviours().
    /// This is useful for storing asset objects that you only want to reference the values of 
    /// or if you want to handle the instantiation of those objects yourself.
    ///</summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class DontInstantiateAttribute : Attribute
    {
        public DontInstantiateAttribute()
        {
        }
    }

}