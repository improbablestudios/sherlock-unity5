﻿#if UNITY_EDITOR
using ImprobableStudios.SearchProjectUtilities;
using System;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    public class ValidateObjectWindow : SearchProjectWindow
    {
        [MenuItem("Search/Scan For Errors", false, 10)]
        public static void Init()
        {
            ValidateObjectWindow window = EditorWindow.GetWindow<ValidateObjectWindow>("Validate");
            window.Show();
        }

        protected override void OnSearchGUI()
        {
        }

        protected override bool IsSearchValid()
        {
            return true;
        }

        protected override string GetHierarchySearchString()
        {
            return null;
        }

        protected override SearchableEditorWindow.SearchMode GetHierarchySearchMode()
        {
            return SearchableEditorWindow.SearchMode.All;
        }
        
        protected override bool IsSearchMatch(UnityEngine.Object obj)
        {
            BaseBehaviour baseBehaviour = obj as BaseBehaviour;
            BaseScriptableObject baseScriptableObject = obj as BaseScriptableObject;
            if (baseBehaviour != null)
            {
                return !string.IsNullOrEmpty(baseBehaviour.GetFirstError());
            }
            if (baseScriptableObject != null)
            {
                return !string.IsNullOrEmpty(baseScriptableObject.GetFirstError());
            }
            return false;
        }

        protected override GUIContent GetSearchButtonLabel()
        {
            return new GUIContent("Scan For Errors");
        }

        protected override string GetNoneFoundMessage()
        {
            return "No errors found";
        }
    }

}
#endif