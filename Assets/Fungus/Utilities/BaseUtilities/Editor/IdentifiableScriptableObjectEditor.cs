﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(IdentifiableScriptableObject), true)]
    public class IdentifiableScriptableObjectEditor : BaseScriptableObjectEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            IdentifiableScriptableObject t = target as IdentifiableScriptableObject;

            if (property.name == "m_" + nameof(t.Key))
            {
                bool enabled = GUI.enabled;
                GUI.enabled = false;
                EditorGUI.PropertyField(position, property, label, true);
                GUI.enabled = enabled;

                EditorGUILayout.Separator();
                EditorGUILayout.LabelField("", EditorStyles.numberField, GUILayout.Height(3f));
                EditorGUILayout.Separator();
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif