﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(BaseScriptableObject), true)]
    public class BaseScriptableObjectEditor : BaseEditor
    {
        public sealed override void OnPropertyGUI(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            EditorGUI.BeginChangeCheck();

            DrawProperty(property, label);

            if (EditorGUI.EndChangeCheck())
            {
                InvokePropertyChanged(property);
            }
        }

        public sealed override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();

            DrawProperty(position, property, label);

            if (EditorGUI.EndChangeCheck())
            {
                InvokePropertyChanged(property);
            }
        }
        
        public override bool IsPropertyVisible(SerializedProperty property)
        {
            BaseScriptableObject t = target as BaseScriptableObject;

            if (!t.IsPropertyVisible(property.propertyPath))
            {
                return false;
            }

            return base.IsPropertyVisible(property);
        }

        public override bool IsPropertyEditable(SerializedProperty property)
        {
            BaseScriptableObject t = target as BaseScriptableObject;

            if(!t.IsPropertyEditable(property.propertyPath))
            {
                return false;
            }

            return base.IsPropertyEditable(property);
        }

        public override int GetPropertyOrder(SerializedProperty property)
        {
            BaseScriptableObject t = target as BaseScriptableObject;

            return t.GetPropertyOrder(property.propertyPath);
        }

        public override int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            BaseScriptableObject t = target as BaseScriptableObject;

            return t.GetPropertyIndentLevel(property.propertyPath);
        }

        public override string GetPropertyError(SerializedProperty property)
        {
            BaseScriptableObject t = target as BaseScriptableObject;

            return t.GetPropertyError(property.propertyPath);
        }
        

        protected void InvokePropertyChanged(SerializedProperty property)
        {
            foreach (BaseScriptableObject t in targets)
            {
                if (serializedObject.targetObject != null)
                {
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.Update();
                    t.InvokePropertyChanged(property.propertyPath);
                    serializedObject.ApplyModifiedProperties();
                    serializedObject.Update();
                }
            }
        }

        protected virtual void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            base.OnPropertyGUI(property, label);
        }

        protected virtual void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            base.OnPropertyGUI(position, property, label);
        }
        
        public virtual void DrawPreview()
        {
        }

    }
}

#endif