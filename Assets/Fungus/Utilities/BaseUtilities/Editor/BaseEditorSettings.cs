﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{
    public static class BaseEditorSettings
    {
        private const string k_keyPrefix = "baseeditor";

        private const string k_settingsPreferencesKey = "Preferences/Inspector";

        public static bool UseReorderableLists
        {
            get;
            private set;
        }

        public static bool GroupUnityEvents
        {
            get;
            private set;
        }

        static BaseEditorSettings()
        {
            UseReorderableLists = EditorPrefs.GetBool($"{k_keyPrefix}.{nameof(UseReorderableLists)}", true);
            GroupUnityEvents = EditorPrefs.GetBool($"{k_keyPrefix}.{nameof(GroupUnityEvents)}", true);
        }

        private static void Save()
        {
            EditorPrefs.SetBool($"{k_keyPrefix}.{nameof(UseReorderableLists)}", UseReorderableLists);
            EditorPrefs.SetBool($"{k_keyPrefix}.{nameof(GroupUnityEvents)}", GroupUnityEvents);
        }

        [SettingsProvider]
        private static SettingsProvider CreateSearchSettings()
        {
            SettingsProvider settings = new SettingsProvider(k_settingsPreferencesKey, SettingsScope.User)
            {
                keywords = new[] { "inspector", "editor", "list", "event" },
                guiHandler = searchContext =>
                {
                    EditorGUIUtility.labelWidth = 500;
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(10);
                        GUILayout.BeginVertical();
                        {
                            GUILayout.Space(10);
                            EditorGUI.BeginChangeCheck();
                            {
                                UseReorderableLists = EditorGUILayout.Toggle(new GUIContent(ObjectNames.NicifyVariableName(nameof(UseReorderableLists))), UseReorderableLists);
                                GroupUnityEvents = EditorGUILayout.Toggle(new GUIContent(ObjectNames.NicifyVariableName(nameof(GroupUnityEvents))), GroupUnityEvents);
                            }
                            if (EditorGUI.EndChangeCheck())
                            {
                                Save();
                            }
                        }
                        GUILayout.EndVertical();
                    }
                    GUILayout.EndHorizontal();
                }
            };
            return settings;
        }
    }

}
#endif