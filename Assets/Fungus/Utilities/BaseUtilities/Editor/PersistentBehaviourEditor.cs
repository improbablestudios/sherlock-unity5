﻿#if UNITY_EDITOR
using UnityEditor;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(PersistentBehaviour), true)]
    public class PersistentBehaviourEditor : IdentifiableBehaviourEditor
    {
    }

}

#endif