﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{
    [CanEditMultipleObjects, CustomEditor(typeof(SettingsScriptableObject<>), true)]
    public class SettingsScriptableObjectEditor : IdentifiableScriptableObjectEditor
    {
        protected override bool IsMainAssetReorderableList(SerializedProperty property)
        {
            Type type = GetAssetListItemType(property);
            if (type != null && typeof(ScriptableObject).IsAssignableFrom(type))
            {
                return true;
            }

            return base.IsMainAssetReorderableList(property);
        }
    }
}
#endif