﻿#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    public class PrefabSpawner : MonoBehaviour
    {
        public static T SpawnPrefab<T>(string prefabName, bool disconnectPrefab, params Type[] otherComponentTypes) where T : UnityEngine.Object
        {
            return SpawnPrefab(prefabName, prefabName, disconnectPrefab, typeof(T), otherComponentTypes) as T;
        }

        public static UnityEngine.Object SpawnPrefab(string prefabName, bool disconnectPrefab, Type type, params Type[] otherComponentTypes)
        {
            return SpawnPrefab(prefabName, prefabName, disconnectPrefab, type, otherComponentTypes);
        }

        public static UnityEngine.Object SpawnPrefab(string prefabName, string instanceName, bool disconnectPrefab, Type type, params Type[] otherComponentTypes)
        {
            GameObject prefab = Resources.Load<GameObject>(prefabName);

            GameObject go = null;

            if (prefab != null)
            {
                go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                Resources.UnloadUnusedAssets();

                go.name = instanceName;

                GameObject selectedGameObject = Selection.activeGameObject as GameObject;
                if (selectedGameObject != null && selectedGameObject.scene.IsValid())
                {
                    go.transform.SetParent(selectedGameObject.transform);
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localEulerAngles = Vector3.zero;
                    go.transform.localScale = Vector3.zero;
                }

                if (disconnectPrefab)
                {
                    PrefabUtility.UnpackPrefabInstance(go, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                }
            }

            if (go == null)
            {
                if (typeof(Component).IsAssignableFrom(type))
                {
                    go = new GameObject(instanceName, type);
                }
                else
                {
                    go = new GameObject(instanceName);
                }

                foreach (Type componentType in otherComponentTypes)
                {
                    go.AddComponent(componentType);
                }
            }

            if (go == null)
            {
                return null;
            }

            SceneView view = SceneView.lastActiveSceneView;
            if (view != null)
            {
                Camera sceneCam = view.camera;
                Vector3 pos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
                pos.z = 0f;
                go.transform.position = pos;
            }

            Selection.activeGameObject = go;

            go.transform.position = Vector3.zero;

            Undo.RegisterCreatedObjectUndo(go, "Create Object");

            if (typeof(Component).IsAssignableFrom(type))
            {
                return go.GetComponent(type);
            }
            return go;
        }

        public static UnityEngine.Object SavePrefab(string prefabName, string instanceName, string targetAssetPath, bool deleteInstance, Type type, params Type[] componentTypes)
        {
            GameObject prefab = Resources.Load<GameObject>(prefabName);

            GameObject go = null;

            if (prefab != null)
            {
                go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
                go.name = instanceName;
            }

            if (go == null)
            {
                if (componentTypes.Length > 0 && componentTypes.All(i => typeof(Component).IsAssignableFrom(i)))
                {
                    go = new GameObject(instanceName, componentTypes);
                }
                else if (typeof(Component).IsAssignableFrom(type))
                {
                    go = new GameObject(instanceName, type);
                }
                else
                {
                    go = new GameObject(instanceName);
                }
            }

            if (go == null)
            {
                return null;
            }

            go.transform.position = Vector3.zero;

            GameObject savedPrefab = PrefabUtility.SaveAsPrefabAsset(go, targetAssetPath);

            if (deleteInstance)
            {
                Undo.DestroyObjectImmediate(go);
            }

            Selection.activeGameObject = savedPrefab;

            Undo.RegisterCreatedObjectUndo(savedPrefab, "Create Object");

            if (typeof(Component).IsAssignableFrom(type))
            {
                return savedPrefab.GetComponent(type);
            }
            return savedPrefab;
        }
    }

}
#endif