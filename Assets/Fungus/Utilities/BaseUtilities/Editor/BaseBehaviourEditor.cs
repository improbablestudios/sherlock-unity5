﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(BaseBehaviour), true)]
    public class BaseBehaviourEditor : BaseEditor
    {
        public sealed override void OnPropertyGUI(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            EditorGUI.BeginChangeCheck();

            DrawProperty(property, label);

            if (EditorGUI.EndChangeCheck())
            {
                InvokePropertyChanged(property);
            }
        }

        public sealed override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();

            DrawProperty(position, property, label);

            if (EditorGUI.EndChangeCheck())
            {
                InvokePropertyChanged(property);
            }
        }

        public override bool IsPropertyVisible(SerializedProperty property)
        {
            BaseBehaviour t = target as BaseBehaviour;

            if (!t.IsPropertyVisible(property.propertyPath))
            {
                return false;
            }

            return base.IsPropertyVisible(property);
        }

        public override bool IsPropertyEditable(SerializedProperty property)
        {
            BaseBehaviour t = target as BaseBehaviour;

            if (!t.IsPropertyEditable(property.propertyPath))
            {
                return false;
            }

            return base.IsPropertyEditable(property);
        }

        public override int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            BaseBehaviour t = target as BaseBehaviour;

            return t.GetPropertyIndentLevelOffset(property.propertyPath);
        }

        public override int GetPropertyOrder(SerializedProperty property)
        {
            BaseBehaviour t = target as BaseBehaviour;

            return t.GetPropertyOrder(property.propertyPath);
        }

        public override string GetPropertyError(SerializedProperty property)
        {
            BaseBehaviour t = target as BaseBehaviour;

            return t.GetPropertyError(property.propertyPath);
        }
        
        protected void InvokePropertyChanged(SerializedProperty property)
        {
            foreach (BaseBehaviour t in targets)
            {
                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
                t.InvokePropertyChanged(property.propertyPath);
                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
            }
        }

        protected virtual void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            base.OnPropertyGUI(property, label);
        }

        protected virtual void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            base.OnPropertyGUI(position, property, label);
        }

        public virtual void DrawPreview()
        {
        }

    }
}

#endif