﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{
    class SetupMenuItems
    {
        [MenuItem("Tools/Setup", false, -2000)]
        [MenuItem("GameObject/Setup", false, -2000)]
        static void CreateSetup()
        {
            GameObject setup = GameObject.Find("Setup");
            if (setup != null)
            {
                EditorGUIUtility.PingObject(setup);
                Selection.activeGameObject = setup.gameObject;
            }
            else
            {
                GameObject sourceSetup = Resources.Load<GameObject>("Setup");
                if (sourceSetup != null)
                {
                    setup = PrefabUtility.InstantiatePrefab(sourceSetup) as GameObject;
                    setup.name = sourceSetup.name;
                    EditorGUIUtility.PingObject(setup);
                    Selection.activeGameObject = setup.gameObject;
                }
                else
                {
                    EditorUtility.DisplayDialog("Setup Not Found", "No prefab named 'Setup' could be found a Resources folder", "OK");
                }
            }
        }

    }
}
#endif