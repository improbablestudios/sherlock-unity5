#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.BaseWindowUtilities;
using ImprobableStudios.ReorderableListUtilities;
using System;
using ImprobableStudios.ResourceTrackingUtilities;
using System.Linq;
using System.Reflection;
using UnityEditor.Presets;
using UnityEngine.Events;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(UnityEngine.Object), true)]
    public class BaseEditor : Editor
    {
        public delegate bool ProcessGameObject(GameObject gameObject, bool processChildren);

        protected SerializedPropertyCache m_serializedPropertyCache;

        protected SerializedPropertyReorderableListCache m_reorderableListCache;

        protected ObjectReferenceEditorCache m_objectReferenceEditorCache = new ObjectReferenceEditorCache();

        protected Preset m_preset;

        protected bool m_eventsFoldout;

        protected virtual void InitializeReorderableListCache()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache();
            m_reorderableListCache.IsPropertyVisibleCallback = IsPropertyVisible;
            m_reorderableListCache.IsPropertyEditableCallback = IsPropertyEditable;
            m_reorderableListCache.GetPropertyIndentLevelOffsetCallback = GetPropertyIndentLevelOffset;
        }

        private static Texture2D s_errorIcon;

        protected static Texture2D ErrorIcon
        {
            get
            {
                if (s_errorIcon == null)
                {
                    s_errorIcon = EditorGUIUtility.FindTexture("console.erroricon.sml");
                }
                return s_errorIcon;
            }
        }

        protected GUIStyle ProcessorBoxStyle
        {
            get
            {
                GUIStyle boxStyle = new GUIStyle(EditorStyles.helpBox);
                boxStyle.padding = new RectOffset(20, 20, 20, 20);
                return boxStyle;
            }
        }

        protected GUIStyle ProcessorLabelStyle
        {
            get
            {
                GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
                labelStyle.alignment = TextAnchor.MiddleCenter;
                return labelStyle;
            }
        }

        protected virtual void OnEnable()
        {
            try
            {
                m_serializedPropertyCache = new SerializedPropertyCache(serializedObject, GetPropertyOrder);
            }
            catch
            {
                DestroyImmediate(this);
            }
        }

        public override sealed void OnInspectorGUI()
        {
            EditorGUIUtility.wideMode = EditorGUIUtility.currentViewWidth > 300;

            serializedObject.SetInspectorMode(GetInspectorMode());

            if (GetInspectorMode() == InspectorMode.Normal)
            {
                DrawNormalInspector();
            }
            else
            {
                DrawDebugInspector();
            }
        }

        public virtual void DrawNormalInspector()
        {
            serializedObject.Update();


            bool isAnyUnityEventIsVisible = false;
            foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
            {
                if (IsPropertyVisible(property))
                {
                    bool isUnityEvent = property.type == nameof(UnityEvent);
                    if (isUnityEvent)
                    {
                        isAnyUnityEventIsVisible = true;
                    }
                    if (!BaseEditorSettings.GroupUnityEvents || !isUnityEvent)
                    {
                        DrawNormalInspectorProperty(property, (SerializedProperty p) => OnPropertyGUI(property));
                    }
                }
            }

            if (BaseEditorSettings.GroupUnityEvents && isAnyUnityEventIsVisible)
            {
                m_eventsFoldout = EditorGUILayout.Foldout(m_eventsFoldout, "Events", true);
                if (m_eventsFoldout)
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        if (property.type == nameof(UnityEvent))
                        {
                            DrawNormalInspectorProperty(property, (SerializedProperty p) => OnPropertyGUI(property));
                        }
                    }
                }
            }

            if (serializedObject.targetObject != null)
            {
                serializedObject.ApplyModifiedProperties();
            }
        }

        public virtual void DrawDebugInspector()
        {
            serializedObject.Update();

            SerializedProperty property = serializedObject.GetIterator();
            bool expanded = true;
            while (property.NextVisible(expanded))
            {
                using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath))
                {
                    EditorGUILayout.PropertyField(property, true);
                }
                expanded = false;
            }

            serializedObject.ApplyModifiedProperties();
        }

        public void OnInspectorGUI(Rect position)
        {
            if (serializedObject.targetObject != null)
            {
                EditorGUIUtility.wideMode = position.width > 300;

                serializedObject.SetInspectorMode(GetInspectorMode());

                if (GetInspectorMode() == InspectorMode.Normal)
                {
                    DrawNormalInspector(position);
                }
                else
                {
                    DrawDebugInspector(position);
                }
            }
        }

        public void DrawNormalInspector(Rect position)
        {
            float margin = 2f;

            serializedObject.Update();

            bool isAnyUnityEventIsVisible = false;
            foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
            {
                if (IsPropertyVisible(property))
                {
                    bool isUnityEvent = property.type == nameof(UnityEvent);
                    if (isUnityEvent)
                    {
                        isAnyUnityEventIsVisible = true;
                    }
                    if (!BaseEditorSettings.GroupUnityEvents || !isUnityEvent)
                    {
                        DrawNormalInspectorProperty(property,
                            (SerializedProperty p) =>
                            {
                                position.height = GetPropertyHeight(property, GetDefaultPropertyLabel(property));
                                OnPropertyGUI(position, property);
                                position.y += position.height + margin;
                            });
                    }
                }
            }

            if (BaseEditorSettings.GroupUnityEvents && isAnyUnityEventIsVisible)
            {
                position.height = EditorGUIUtility.singleLineHeight;
                m_eventsFoldout = EditorGUI.Foldout(position, m_eventsFoldout, "Events", true);
                position.y += position.height + margin;
                if (m_eventsFoldout)
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        if (property.type == nameof(UnityEvent))
                        {
                            DrawNormalInspectorProperty(property,
                            (SerializedProperty p) =>
                            {
                                position.height = GetPropertyHeight(property, GetDefaultPropertyLabel(property));
                                OnPropertyGUI(position, property);
                                position.y += position.height + margin;
                            });
                        }
                    }
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawNormalInspectorProperty(SerializedProperty property, Action<SerializedProperty> onDrawProperty)
        {
            if (IsPropertyVisible(property))
            {
                int indentLevel = EditorGUI.indentLevel;
                EditorGUI.indentLevel += GetPropertyIndentLevelOffset(property);

                bool enabled = GUI.enabled;
                if (!IsPropertyEditable(property))
                {
                    GUI.enabled = false;
                }
                onDrawProperty(property);

                EditorGUI.indentLevel = indentLevel;

                GUI.enabled = enabled;
            }
        }

        public void DrawDebugInspector(Rect position)
        {
            float margin = 2f;

            serializedObject.Update();

            SerializedProperty property = serializedObject.GetIterator();
            bool enterChildren = true;
            while (property.NextVisible(enterChildren))
            {
                using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath))
                {
                    position.height = EditorGUI.GetPropertyHeight(property, GetDefaultPropertyLabel(property));
                    EditorGUILayout.PropertyField(property, true);
                    position.y += position.height + margin;
                }
                enterChildren = false;
            }

            serializedObject.ApplyModifiedProperties();
        }

        public float GetInspectorHeight()
        {
            if (serializedObject.targetObject != null)
            {
                serializedObject.SetInspectorMode(GetInspectorMode());

                if (GetInspectorMode() == InspectorMode.Normal)
                {
                    return GetNormalInspectorHeight();
                }
                else
                {
                    return GetDebugInspectorHeight();
                }
            }
            return 0;
        }

        public float GetNormalInspectorHeight()
        {
            float height = 0f;
            float margin = 2f;

            bool isAnyUnityEventIsVisible = false;
            foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
            {
                if (IsPropertyVisible(property))
                {
                    bool isUnityEvent = property.type == nameof(UnityEvent);
                    if (isUnityEvent)
                    {
                        isAnyUnityEventIsVisible = true;
                    }
                    if (!BaseEditorSettings.GroupUnityEvents || !isUnityEvent)
                    {
                        height += GetPropertyHeight(property, GetDefaultPropertyLabel(property));
                        height += margin;
                    }
                }
            }

            if (BaseEditorSettings.GroupUnityEvents && isAnyUnityEventIsVisible)
            {
                height += EditorGUIUtility.singleLineHeight;
                height += margin;
                if (m_eventsFoldout)
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        if (IsPropertyVisible(property))
                        {
                            if (property.type == nameof(UnityEvent))
                            {
                                height += GetPropertyHeight(property, GetDefaultPropertyLabel(property));
                                height += margin;
                            }
                        }
                    }
                }
            }

            return height;
        }

        public float GetDebugInspectorHeight()
        {
            float height = 0f;
            float margin = 2f;

            SerializedProperty property = serializedObject.GetIterator();
            bool enterChildren = true;
            while (property.NextVisible(enterChildren))
            {
                using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath))
                {
                    height += EditorGUI.GetPropertyHeight(property, true);
                    height += margin;
                }
                enterChildren = false;
            }

            return height;
        }

        public InspectorMode GetInspectorMode()
        {
            EditorWindow window = EditorWindowUtilities.GetCurrentEditorWindow();

            if (window != null)
            {
                if (EditorWindowUtilities.GetInspectorWindowType().IsAssignableFrom(window.GetType()))
                {
                    return EditorWindowUtilities.GetInspectorWindowMode(window);
                }
                else
                {
                    BaseWindow baseWindow = window as BaseWindow;
                    if (baseWindow != null)
                    {
                        return baseWindow.GetInspectorMode();
                    }
                }
            }

            return InspectorMode.Normal;
        }

        public void OnPropertyGUI(SerializedProperty property)
        {
            OnPropertyGUI(property, GetDefaultPropertyLabel(property));
        }

        public void OnPropertyGUI(Rect position, SerializedProperty property)
        {
            OnPropertyGUI(position, property, GetDefaultPropertyLabel(property));
        }

        public SerializedProperty GetProperty(string propertyPath)
        {
            return m_serializedPropertyCache[serializedObject].FirstOrDefault(x => x.propertyPath == propertyPath);
        }

        public virtual GUIContent GetDefaultPropertyLabel(SerializedProperty property)
        {
            return GetLabelWithErrors(property, new GUIContent(property.displayName, property.GetPropertyTooltip()));
        }

        public virtual Rect GetPropertyControlRect(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            return EditorGUILayout.GetControlRect(!string.IsNullOrEmpty(label.text), GetPropertyHeight(property, label), options);
        }

        public virtual void OnPropertyGUI(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            OnPropertyGUI(GetPropertyControlRect(property, label, options), property, label);
        }

        public virtual void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            RectOffset padding = GetPropertyPadding(property);

            position.xMin += padding.left;
            position.xMax -= padding.right;
            position.yMin += padding.top;
            position.yMax -= padding.bottom;

            if (m_reorderableListCache == null)
            {
                InitializeReorderableListCache();
            }

            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (IsNestedAssetReorderableList(property))
                {
                    InitializeNestedAssetReorderableList(property);
                }
                else if (IsMainAssetReorderableList(property))
                {
                    InitializeMainAssetReorderableList(property);
                }
                m_reorderableListCache[property].Draw(position, property, label);
            }
            else
            {
                PropertyField(position, property, label);
            }
        }

        private void PropertyField(Rect position, SerializedProperty property, GUIContent label)
        {
            ExtendedEditorGUI.PropertyField(position, property, label, true, IsPropertyVisible, IsPropertyEditable, GetPropertyIndentLevelOffset, ChildPropertyField, GetChildPropertyHeight);
        }

        protected virtual bool ChildPropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
                return false;
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (IsNestedAssetReorderableList(property))
                {
                    InitializeNestedAssetReorderableList(property);
                }
                else if (IsMainAssetReorderableList(property))
                {
                    InitializeMainAssetReorderableList(property);
                }
                SerializedPropertyReorderableList reorderableList = m_reorderableListCache[property];
                position.height = reorderableList.GetHeight(property, label);
                reorderableList.Draw(position, property, label);
                return false;
            }
            else
            {
                return EditorGUI.PropertyField(position, property, label, includeChildren);
            }
        }

        public virtual float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (m_reorderableListCache == null)
            {
                InitializeReorderableListCache();
            }

            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
                return 0f;
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (IsNestedAssetReorderableList(property))
                {
                    InitializeNestedAssetReorderableList(property);
                }
                else if (IsMainAssetReorderableList(property))
                {
                    InitializeMainAssetReorderableList(property);
                }
                return m_reorderableListCache[property].GetHeight(property, label);
            }
            else
            {
                return ExtendedEditorGUI.GetPropertyHeight(property, label, true, IsPropertyVisible, GetChildPropertyHeight);
            }
        }

        protected virtual float GetChildPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
                return 0f;
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (IsNestedAssetReorderableList(property))
                {
                    InitializeNestedAssetReorderableList(property);
                }
                else if (IsMainAssetReorderableList(property))
                {
                    InitializeMainAssetReorderableList(property);
                }
                SerializedPropertyReorderableList reorderableList = m_reorderableListCache[property];
                return reorderableList.GetHeight(property, label);
            }
            else
            {
                return EditorGUI.GetPropertyHeight(property, label, includeChildren);
            }
        }

        public virtual RectOffset GetPropertyPadding(SerializedProperty property)
        {
            return new RectOffset();
        }

        public virtual bool IsPropertyVisible(SerializedProperty property)
        {
            if (property.name == "m_Script")
            {
                return false;
            }
            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
                return false;
            }
            return true;
        }

        public virtual bool IsPropertyEditable(SerializedProperty property)
        {
            if (property.name == "m_Script")
            {
                return false;
            }
            return true;
        }

        public virtual int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            return 0;
        }

        public virtual int GetPropertyOrder(SerializedProperty property)
        {
            return 0;
        }

        public virtual string GetPropertyError(SerializedProperty property)
        {
            return null;
        }

        public virtual GUIContent GetLabelWithErrors(SerializedProperty property, GUIContent label)
        {
            string error = GetPropertyError(property);
            if (error != null)
            {
                string text = label.text;

                string tooltip = label.tooltip;
                if (!string.IsNullOrEmpty(error))
                {
                    if (!string.IsNullOrEmpty(tooltip))
                    {
                        tooltip += " ";
                    }
                    tooltip += string.Format("[{0}]", error);
                }

                return new GUIContent(text, ErrorIcon, tooltip);
            }

            return label;
        }

        protected virtual bool IsMainAssetReorderableList(SerializedProperty property)
        {
            return false;
        }

        protected virtual bool IsNestedAssetReorderableList(SerializedProperty property)
        {
            Type type = GetAssetListItemType(property);
            if (type != null && typeof(ScriptableObject).IsAssignableFrom(type))
            {
                if (typeof(INestedAsset).IsAssignableFrom(type))
                {
                    return true;
                }
            }

            return false;
        }

        protected Type GetAssetListItemType(SerializedProperty property)
        {
            return property.GetPropertyType().GetItemType();
        }

        protected void InitializeMainAssetReorderableList(SerializedProperty property)
        {
            if (m_reorderableListCache == null)
            {
                InitializeReorderableListCache();
            }
            if (!m_reorderableListCache.ContainsKey(property))
            {
                MainAssetReorderableList reorderableList = new MainAssetReorderableList(ReorderableListFlags.DisableCollapsingElements);
                reorderableList.EditorExpandedElementDrawerCallback = EditorElementDrawer;
                reorderableList.EditorExpandedElementHeightCallback = EditorElementHeight;
                reorderableList.EditorCollapsedElementDrawerCallback = EditorElementDrawer;
                reorderableList.EditorCollapsedElementHeightCallback = EditorElementHeight;
                reorderableList.EditorIsPropertyVisibleCallback = EditorIsPropertyVisible;
                reorderableList.EditorIsPropertyEditableCallback = EditorIsPropertyEditable;
                reorderableList.EditorGetPropertyIndentLevelOffsetCallback = EditorGetPropertyIndentLevelOffset;
                m_reorderableListCache[property] = reorderableList;
            }
        }

        protected void InitializeNestedAssetReorderableList(SerializedProperty property)
        {
            if (m_reorderableListCache == null)
            {
                InitializeReorderableListCache();
            }
            if (!m_reorderableListCache.ContainsKey(property))
            {
                NestedAssetReorderableList reorderableList = new NestedAssetReorderableList(ReorderableListFlags.DisableCollapsingElements);
                reorderableList.EditorExpandedElementDrawerCallback = EditorElementDrawer;
                reorderableList.EditorExpandedElementHeightCallback = EditorElementHeight;
                reorderableList.EditorCollapsedElementDrawerCallback = EditorElementDrawer;
                reorderableList.EditorCollapsedElementHeightCallback = EditorElementHeight;
                reorderableList.EditorIsPropertyVisibleCallback = EditorIsPropertyVisible;
                reorderableList.EditorIsPropertyEditableCallback = EditorIsPropertyEditable;
                reorderableList.EditorGetPropertyIndentLevelOffsetCallback = EditorGetPropertyIndentLevelOffset;
                m_reorderableListCache[property] = reorderableList;
            }
        }

        protected void EditorElementDrawer(Rect position, UnityEngine.Object obj)
        {
            if (obj != null)
            {
                Editor editor = m_objectReferenceEditorCache[obj];
                if (editor != null)
                {
                    BaseEditor baseEditor = editor as BaseEditor;
                    baseEditor.OnInspectorGUI(position);
                }
            }
        }

        protected float EditorElementHeight(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                Editor editor = m_objectReferenceEditorCache[obj];
                if (editor != null)
                {
                    BaseEditor baseEditor = editor as BaseEditor;
                    return baseEditor.GetInspectorHeight();
                }
            }
            return 0f;
        }

        protected bool EditorIsPropertyVisible(UnityEngine.Object obj, SerializedProperty property)
        {
            if (obj != null)
            {
                Editor editor = m_objectReferenceEditorCache[obj];
                if (editor != null)
                {
                    BaseEditor baseEditor = editor as BaseEditor;
                    return baseEditor.IsPropertyVisible(property);
                }
            }
            return true;
        }

        protected bool EditorIsPropertyEditable(UnityEngine.Object obj, SerializedProperty property)
        {
            if (obj != null)
            {
                Editor editor = m_objectReferenceEditorCache[obj];
                if (editor != null)
                {
                    BaseEditor baseEditor = editor as BaseEditor;
                    return baseEditor.IsPropertyEditable(property);
                }
            }
            return true;
        }

        protected int EditorGetPropertyIndentLevelOffset(UnityEngine.Object obj, SerializedProperty property)
        {
            if (obj != null)
            {
                Editor editor = m_objectReferenceEditorCache[obj];
                if (editor != null)
                {
                    BaseEditor baseEditor = editor as BaseEditor;
                    return baseEditor.GetPropertyIndentLevelOffset(property);
                }
            }
            return 0;
        }

        public void DrawProcessGameObjectsGUI(string title, string processTitle, string dialogErrorTitle, string dialogErrorMessage, string dialogErrorOk, ProcessGameObject processGameObject)
        {
            using (new EditorGUILayout.VerticalScope())
            {
                EditorGUILayout.LabelField(title, ProcessorLabelStyle);
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("Selection"))
                    {
                        if (!ProcessGameObjectsInSelection(processGameObject, processTitle))
                        {
                            EditorUtility.DisplayDialog(dialogErrorTitle, dialogErrorMessage, dialogErrorOk);
                        }
                    }

                    if (GUILayout.Button("Scene"))
                    {
                        if (!ProcessGameObjectsInScene(processGameObject, processTitle))
                        {
                            EditorUtility.DisplayDialog(dialogErrorTitle, dialogErrorMessage, dialogErrorOk);
                        }
                    }

                    if (GUILayout.Button("Assets"))
                    {
                        if (!ProcessRootGameObjectsInAssets(processGameObject, processTitle))
                        {
                            EditorUtility.DisplayDialog(dialogErrorTitle, dialogErrorMessage, dialogErrorOk);
                        }
                    }
                }
            }
        }

        public bool ProcessGameObjectsInSelection(ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            bool found = false;

            GameObject[] gameObjects = Selection.gameObjects.ToArray();

            GameObject[] prefabs = gameObjects.Where(x => !x.scene.IsValid()).ToArray();
            GameObject[] instances = gameObjects.Except(prefabs).ToArray();

            if (ProcessGameObjects(prefabs, processGameObject, true, title, infoFormat))
            {
                found = true;
            }
            if (ProcessGameObjects(instances, processGameObject, false, title, infoFormat))
            {
                found = true;
            }

            return found;
        }

        public bool ProcessGameObjectsInScene(ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = ResourceTracker.FindLoadedInstanceGameObjects();
            return ProcessGameObjects(gameObjects, processGameObject, false, title, infoFormat);
        }

        public bool ProcessGameObjectsInAssets(ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = AssetDatabaseUtilities.FindGameObjects();
            return ProcessGameObjects(gameObjects, processGameObject, false, title, infoFormat);
        }

        public bool ProcessRootGameObjectsInScene(ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = ResourceTracker.FindLoadedInstanceRoots();
            return ProcessGameObjects(gameObjects, processGameObject, true, title, infoFormat);
        }

        public bool ProcessRootGameObjectsInAssets(ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = AssetDatabaseUtilities.FindPrefabs();
            return ProcessGameObjects(gameObjects, processGameObject, true, title, infoFormat);
        }

        public bool ProcessGameObjectsWithComponentInSelection(Type type, ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            bool found = false;

            GameObject[] gameObjects = Selection.gameObjects.Where(x => x.GetComponent(type) != null).ToArray();

            GameObject[] prefabs = gameObjects.Where(x => !x.scene.IsValid()).ToArray();
            GameObject[] instances = gameObjects.Except(prefabs).ToArray();

            if (ProcessGameObjects(prefabs, processGameObject, true, title, infoFormat))
            {
                found = true;
            }
            if (ProcessGameObjects(instances, processGameObject, false, title, infoFormat))
            {
                found = true;
            }

            return found;
        }

        public bool ProcessGameObjectsWithComponentInScene(Type type, ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = ResourceTracker.FindLoadedInstanceGameObjectsWithComponentType(type);
            return ProcessGameObjects(gameObjects, processGameObject, false, title, infoFormat);
        }

        public bool ProcessGameObjectsWithComponentInAssets(Type type, ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = AssetDatabaseUtilities.FindGameObjectsWithComponent(type);
            return ProcessGameObjects(gameObjects, processGameObject, false, title, infoFormat);
        }

        public bool ProcessRootGameObjectsWithChildComponentInScene(Type type, ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = ResourceTracker.FindLoadedInstanceRootsWithComponentType(type);
            return ProcessGameObjects(gameObjects, processGameObject, true, title, infoFormat);
        }

        public bool ProcessRootGameObjectsWithChildComponentInAssets(Type type, ProcessGameObject processGameObject, string title, string infoFormat = "Processing '{0}'")
        {
            GameObject[] gameObjects = AssetDatabaseUtilities.FindPrefabsWithComponent(type);
            return ProcessGameObjects(gameObjects, processGameObject, true, title, infoFormat);
        }

        public bool ProcessGameObjects(GameObject[] gameObjects, ProcessGameObject processGameObject, bool processChildren, string title, string infoFormat = "Processing '{0}'")
        {
            bool found = false;

            for (int i = 0; i < gameObjects.Length; i++)
            {
                GameObject gameObject = gameObjects[i];

                if (EditorUtility.DisplayCancelableProgressBar(title, string.Format(infoFormat, gameObject.name), (float)i / gameObjects.Length))
                {
                    break;
                }

                if (gameObject.scene.IsValid())
                {
                    if (ProcessGameObjectInScene(gameObject, processGameObject, processChildren))
                    {
                        found = true;
                    }
                }
                else
                {
                    if (ProcessGameObjectInAssets(gameObject, processGameObject, processChildren))
                    {
                        found = true;
                    }
                }
            }

            EditorUtility.ClearProgressBar();

            return found;
        }

        public bool ProcessGameObjectInScene(GameObject instance, ProcessGameObject processGameObject, bool processChildren)
        {
            bool found = false;

            try
            {
                if (ResourceTracker.IsValidInstance(instance))
                {
                    if (processGameObject(instance, processChildren))
                    {
                        found = true;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return found;
        }

        public bool ProcessGameObjectInAssets(GameObject prefab, ProcessGameObject processGameObject, bool processChildren)
        {
            bool found = false;

            try
            {
                if (ResourceTracker.IsObjectSavedInBuild(prefab))
                {
                    string prefabAssetPath = AssetDatabase.GetAssetPath(prefab);

                    GameObject loadedPrefab = PrefabUtility.LoadPrefabContents(prefabAssetPath);

                    if (processGameObject(loadedPrefab, processChildren))
                    {
                        found = true;
                    }

                    PrefabUtility.SaveAsPrefabAsset(loadedPrefab, prefabAssetPath);
                    PrefabUtility.UnloadPrefabContents(loadedPrefab);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return found;
        }

        protected override void OnHeaderGUI()
        {
            DrawHeaderGUI(GetTargetTitle(), 0f);
        }

        protected virtual Rect DrawHeaderGUI(string header, float leftMargin)
        {
            GUILayout.BeginHorizontal(new GUIStyle("In BigTitle"), new GUILayoutOption[0]);
            GUILayout.Space(38f);
            GUILayout.BeginVertical();
            GUILayout.Space(19f);
            GUILayout.BeginHorizontal();
            if ((double)leftMargin > 0.0)
                GUILayout.Space(leftMargin);
            if ((bool)(this))
                this.OnHeaderControlsGUI();
            else
                EditorGUILayout.GetControlRect();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            Rect lastRect = GUILayoutUtility.GetLastRect();
            Rect r = new Rect(lastRect.x + leftMargin, lastRect.y, lastRect.width - leftMargin, lastRect.height);
            Rect rect1 = new Rect(r.x + 6f, r.y + 6f, 32f, 32f);
            if ((bool)(this))
            {
                this.OnHeaderIconGUI(rect1);
            }
            else
            {
                GUIStyle centerStyle = new GUIStyle();
                centerStyle.alignment = TextAnchor.MiddleCenter;
                GUI.Label(rect1, (Texture)AssetPreview.GetMiniTypeThumbnail(typeof(UnityEngine.Object)), centerStyle);
            }
            if ((bool)(this))
            {
                this.DrawPostIconContent(rect1);
            }
            Rect rect2 = new Rect(r.x + 44f, r.y + 6f, (float)((double)r.width - 44.0 - 38.0 - 4.0), 16f);
            if ((bool)(this))
            {
                this.OnHeaderTitleGUI(rect2, header);
            }
            else
            {
                GUI.Label(rect2, header, EditorStyles.largeLabel);
            }
            if ((bool)(this))
            {
                r.y += 5;
                DrawHeaderHelpAndSettingsGUI(r);
            }
            Event current = Event.current;
            if (this != null && current.type == EventType.MouseDown && (current.button == 1 && r.Contains(current.mousePosition)))
            {
                ShowSettingsContextMenu(new Rect(current.mousePosition.x, current.mousePosition.y, 0.0f, 0.0f));
                current.Use();
            }
            return lastRect;
        }

        protected virtual string GetTargetTitle()
        {
            return (string)typeof(Editor).GetProperty("targetTitle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(this, null);
        }

        protected virtual void OnHeaderControlsGUI()
        {
            typeof(Editor).GetMethod("OnHeaderControlsGUI", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(this, null);
        }

        protected virtual void OnHeaderIconGUI(Rect iconRect)
        {
            typeof(Editor).GetMethod("OnHeaderIconGUI", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(this, new object[] { iconRect });
        }

        protected virtual void DrawPostIconContent(Rect iconRect)
        {
            typeof(Editor).GetMethod("DrawPostIconContent", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(Rect) }, null ).Invoke(this, new object[] { iconRect });
        }

        protected virtual void OnHeaderTitleGUI(Rect iconRect, string header)
        {
            typeof(Editor).GetMethod("OnHeaderTitleGUI", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(this, new object[] { iconRect, header });
        }

        protected virtual void DrawHeaderHelpAndSettingsGUI()
        {
            Vector2 size = GetHeaderHelpAndSettingsSize();
            DrawHeaderHelpAndSettingsGUI(EditorGUILayout.GetControlRect(GUILayout.Width(size.x), GUILayout.Height(size.y)));
        }
        
        protected virtual void DrawHeaderHelpAndSettingsGUI(Rect position)
        {
            Vector2 settingsSize = GetSettingsButtonSize();
            Vector2 presetSize = GetPresetButtonSize();
            Vector2 helpSize = GetHelpButtonSize();

            Rect pos = position;
            pos.xMin = pos.xMax;

            if (WillDrawSettingsButton())
            {
                pos.xMax = pos.xMin;
                pos.xMin = pos.xMax - settingsSize.x;
                DrawSettingsButton(pos);
            }
            
            if (WillDrawPresetButton())
            {
                pos.xMax = pos.xMin;
                pos.xMin = pos.xMax - presetSize.x;
                DrawPresetButton(pos);
            }

            if (WillDrawHelpButton())
            {
                pos.xMax = pos.xMin;
                pos.xMin = pos.xMax - helpSize.x;
                DrawHelpButton(pos);
            }
        }

        protected virtual bool WillDrawSettingsButton()
        {
            return true;
        }

        protected virtual GUIStyle GetSettingsButtonStyle()
        {
            return new GUIStyle("IconButton");
        }

        protected virtual GUIContent GetSettingsButtonContent()
        {
            return EditorGUIUtility.IconContent(EditorGUIUtility.isProSkin ? "d__Popup" : "_Popup");
        }

        protected virtual void DrawSettingsButton(Rect position)
        {
            GUIStyle buttonStyle = GetSettingsButtonStyle();
            GUIContent content = GetSettingsButtonContent();
            if (EditorGUI.DropdownButton(position, content, FocusType.Passive, buttonStyle))
            {
                ShowSettingsContextMenu(position);
            }
        }

        protected virtual Vector2 GetSettingsButtonSize()
        {
            GUIStyle buttonStyle = GetSettingsButtonStyle();
            GUIContent content = GetSettingsButtonContent();
            return buttonStyle.CalcSize(content);
        }

        protected virtual bool WillDrawPresetButton()
        {
            return true;
        }

        protected virtual GUIStyle GetPresetButtonStyle()
        {
            return new GUIStyle("IconButton");
        }

        protected virtual GUIContent GetPresetButtonContent()
        {
            return EditorGUIUtility.IconContent(EditorGUIUtility.isProSkin ? "d_Preset.Context" : "Preset.Context");
        }

        protected virtual void DrawPresetButton(Rect position)
        {
            GUIStyle buttonStyle = GetPresetButtonStyle();
            GUIContent content = GetPresetButtonContent();
            if (EditorGUI.DropdownButton(position, content, FocusType.Passive, buttonStyle))
            {
                ShowPresetsMenu();
            }
        }

        protected virtual Vector2 GetPresetButtonSize()
        {
            GUIStyle buttonStyle = GetPresetButtonStyle();
            GUIContent content = GetPresetButtonContent();
            return buttonStyle.CalcSize(content);
        }

        protected virtual bool WillDrawHelpButton()
        {
            return true;
        }

        protected virtual GUIStyle GetHelpButtonStyle()
        {
            return new GUIStyle("IconButton");
        }

        protected virtual GUIContent GetHelpButtonContent()
        {
            return EditorGUIUtility.IconContent("_Help");
        }

        protected virtual void DrawHelpButton(Rect position)
        {
            GUIStyle buttonStyle = GetHelpButtonStyle();
            GUIContent content = GetHelpButtonContent();
            if (EditorGUI.DropdownButton(position, content, FocusType.Passive, buttonStyle))
            {
                ShowHelpMenu();
            }
        }

        protected virtual Vector2 GetHelpButtonSize()
        {
            GUIStyle buttonStyle = GetHelpButtonStyle();
            GUIContent content = GetHelpButtonContent();
            return buttonStyle.CalcSize(content);
        }

        protected Vector2 GetHeaderHelpAndSettingsSize()
        {
            Vector2 settingsSize = (WillDrawSettingsButton()) ? GetSettingsButtonSize() : Vector2.zero;
            Vector2 presetSize = (WillDrawPresetButton()) ? GetPresetButtonSize() : Vector2.zero;
            Vector2 helpSize = (WillDrawHelpButton()) ? GetHelpButtonSize() : Vector2.zero;

            return new Vector2(settingsSize.x + presetSize.x + helpSize.x, new[] { settingsSize.y, presetSize.y, helpSize.y }.Max());
        }

        protected virtual void ShowSettingsContextMenu(Rect position)
        {
            ExtendedEditorGUI.DisplayObjectContextMenu(position, targets, 0);
        }

        protected virtual void ShowPresetsMenu()
        {
            PresetSelector.ShowSelector(targets, m_preset, true);
        }

        protected virtual void ShowHelpMenu()
        {
            if (Help.HasHelpForObject(target))
            {
                Help.ShowHelpForObject(target);
            }
            else
            {
                Help.BrowseURL("http://forum.unity3d.com/search.php");
            }
        }
    }

}
#endif
