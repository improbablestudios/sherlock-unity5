﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.BaseUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(IdentifiableBehaviour), true)]
    public class IdentifiableBehaviourEditor : BaseBehaviourEditor
    {
        public override bool IsPropertyVisible(SerializedProperty property)
        {
            IdentifiableBehaviour t = target as IdentifiableBehaviour;

            if (property.name == "m_" + nameof(t.Key))
            {
                return false;
            }

            return base.IsPropertyVisible(property);
        }

        public override bool IsPropertyEditable(SerializedProperty property)
        {
            IdentifiableBehaviour t = target as IdentifiableBehaviour;

            if (property.name == "m_" + nameof(t.Key))
            {
                return false;
            }

            return base.IsPropertyEditable(property);
        }
    }

}



#endif