#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.PathfindingUtilities
{

    public class PathfindingMenuItems
    {
        [MenuItem("Tools/Pathfinding/Path Renderer", false, 10)]
        [MenuItem("GameObject/Pathfinding/Path Renderer", false, 10)]
        static void CreatePathRenderer()
        {
            PrefabSpawner.SpawnPrefab<PathRenderer>("~PathRenderer", true);
        }

        [MenuItem("Tools/Pathfinding/Path Follower", false, 10)]
        [MenuItem("GameObject/Pathfinding/Path Follower", false, 10)]
        static void CreatePathFollower()
        {
            PrefabSpawner.SpawnPrefab<PathFollower>("~PathFollower", true);
        }
    }

}
#endif