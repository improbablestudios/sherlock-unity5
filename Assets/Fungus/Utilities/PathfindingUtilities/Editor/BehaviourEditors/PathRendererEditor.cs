#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.PathfindingUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(PathRenderer), true)]
    public class PathRendererEditor : PersistentBehaviourEditor
    {
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();
            
            PathRenderer t = target as PathRenderer;

            if (GUILayout.Button("Update Path"))
            {
                if (AstarPath.active != null && AstarPath.active.graphs != null && AstarPath.active.graphs.Length > 0)
                {
                    t.ForceSearchPath();
                }
            }
        }
    }

}


#endif