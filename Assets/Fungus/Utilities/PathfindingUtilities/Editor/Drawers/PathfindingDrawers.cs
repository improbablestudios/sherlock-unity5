#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;
using Pathfinding;
using System;
using ImprobableStudios.SerializedPropertyUtilities;

namespace ImprobableStudios.PathfindingUtilities
{
    [CustomPropertyDrawer(typeof(PathfindingGraphMaskAttribute))]
    public class PathfindingGraphMaskDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            int mask = property.intValue;
            if (AstarPath.active != null)
            {
                AstarData data = AstarPath.active.data;
                data.Awake();
                string[] displayedOptions = data.graphs.Select(i => i.name).ToArray();
                EditorGUI.BeginChangeCheck();
                mask = EditorGUILayout.MaskField(new GUIContent(property.displayName, property.tooltip), mask, displayedOptions);
                if (EditorGUI.EndChangeCheck())
                {
                    property.intValue = mask;
                }
            }
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(int) };
        }
    }
}

#endif