﻿using UnityEngine;

namespace ImprobableStudios.PathfindingUtilities
{

    public class PathfindingGraphMaskAttribute : PropertyAttribute
    {
        public PathfindingGraphMaskAttribute()
        {
        }
    }

}
