﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.UnityObjectUtilities;
using Pathfinding;
using Pathfinding.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.PathfindingUtilities
{

    [RequireComponent(typeof(Seeker))]
    [AddComponentMenu("Pathfinding/Path Follower")]
    public class PathFollower : PersistentBehaviour
    {
        [Tooltip("The target to move towards")]
        [SerializeField]
        [CheckNotNull]
        protected Transform m_Target;

        [Tooltip("The graphs that will be scanned for paths")]
        [PathfindingGraphMask]
        [SerializeField]
        protected int m_Graphs;

        [Tooltip("Enables or disables searching for paths (Setting this to false does not stop any active path requests from being calculated or stop it from continuing to follow the current path)")]
        [SerializeField]
        protected bool m_CanSearch = true;

        [Tooltip("Determines how often it will search for new paths (If you have fast moving targets or AIs, you might want to set it to a lower value. The value is in seconds between path requests)")]
        [Min(0)]
        [SerializeField]
        protected float m_RepathRate = 0.5f;

        [Tooltip("Enables or disables movement")]
        [SerializeField]
        protected bool m_CanMove = true;

        [Tooltip("Speed in world units")]
        [Min(0)]
        [SerializeField]
        protected float m_Speed = 4;

        [Tooltip("If true, the AI will rotate to face the movement direction")]
        [SerializeField]
        protected bool m_EnableRotation = true;

        [Tooltip("If true, rotation will only be done along the Z axis so that the Y axis is the forward direction of the character (This is useful for 2D games in which one often want to have the Y axis as the forward direction to get sprites and 2D colliders to work properly)")]
        [SerializeField]
        protected bool m_RotateIn2D = false;

        [Tooltip("How quickly to rotate")]
        [Min(0)]
        [SerializeField]
        protected float m_RotationSpeed = 30;

        [Tooltip("If true, some interpolation will be done when a new path has been calculated (This is used to avoid short distance teleportation)")]
        [SerializeField]
        protected bool m_InterpolatePathSwitches = true;

        [Tooltip("How quickly to interpolate to the new path")]
        [Min(0)]
        [SerializeField]
        protected float m_SwitchPathInterpolationSpeed = 10;

        protected UnityEvent m_OnPathUpdated = new UnityEvent();

        protected UnityEvent m_OnNextPathUpdated = new UnityEvent();

        /// <summary>
        /// Cached Transform component
        /// </summary>
        protected Transform m_transform;

        /// <summary>
        /// Time when the last path request was sent
        /// </summary>
        protected float m_lastRepath = -9999;

        /// <summary>
        /// Current path which is followed
        /// </summary>
        protected ABPath m_path;

        /// <summary>
        /// True if the end-of-path is reached.
        /// (<see cref="OnTargetReached"/>)
        /// </summary>
        public bool m_targetReached
        {
            get;
            private set;
        }

        /// <summary>
        /// Only when the previous path has been returned should be search for a new path
        /// </summary>
        protected bool m_canSearchAgain = true;

        /// <summary>
        /// When a new path was returned, the AI was moving along this ray.
        /// Used to smoothly interpolate between the previous movement and the movement along the new path.
        /// The speed is equal to movement direction.
        /// </summary>
        protected Vector3 m_previousMovementOrigin;

        protected Vector3 m_previousMovementDirection;

        protected float m_previousMovementStartTime = -9999;

        protected PathInterpolator m_interpolator = new PathInterpolator();

        /// <summary>
        /// Holds if the Start function has been run.
        /// Used to test if coroutines should be started in OnEnable to prevent calculating paths
        /// in the awake stage (or rather before start on frame 0).
        /// </summary>
        private bool m_startHasRun = false;

        private Seeker m_Seeker;

        public Transform Target
        {
            get
            {
                return m_Target;
            }
            set
            {
                m_Target = value;
                ForceSearchPath();
            }
        }

        public int Graphs
        {
            get
            {
                return m_Graphs;
            }
            set
            {
                m_Graphs = value;
                ForceSearchPath();
            }
        }

        public bool CanSearch
        {
            get
            {
                return m_CanSearch;
            }
            set
            {
                m_CanSearch = value;
            }
        }

        public float RepathRate
        {
            get
            {
                return m_RepathRate;
            }
            set
            {
                m_RepathRate = value;
            }
        }

        public bool CanMove
        {
            get
            {
                return m_CanMove;
            }
            set
            {
                m_CanMove = value;
            }
        }

        protected float Speed
        {
            get
            {
                return m_Speed;
            }
            set
            {
                m_Speed = value;
            }
        }

        protected bool EnableRotation
        {
            get
            {
                return m_EnableRotation;
            }
            set
            {
                m_EnableRotation = value;
            }
        }

        protected bool RotateIn2D
        {
            get
            {
                return m_RotateIn2D;
            }
            set
            {
                m_RotateIn2D = value;
            }
        }

        protected float RotationSpeed
        {
            get
            {
                return m_RotationSpeed;
            }
            set
            {
                m_RotationSpeed = value;
            }
        }

        protected bool InterpolatePathSwitches
        {
            get
            {
                return m_InterpolatePathSwitches;
            }
            set
            {
                m_InterpolatePathSwitches = value;
            }
        }

        protected float SwitchPathInterpolationSpeed
        {
            get
            {
                return m_SwitchPathInterpolationSpeed;
            }
            set
            {
                m_SwitchPathInterpolationSpeed = value;
            }
        }

        public UnityEvent OnPathUpdated
        {
            get
            {
                return m_OnPathUpdated;
            }
        }

        public UnityEvent OnNextPathUpdated
        {
            get
            {
                return m_OnNextPathUpdated;
            }
        }

        public Seeker Seeker
        {
            get
            {
                if (m_Seeker == null)
                {
                    m_Seeker = this.GetRequiredComponent<Seeker>();
                }
                return m_Seeker;
            }
            set
            {
                m_Seeker = value;
                ForceSearchPath();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            m_Seeker = this.GetRequiredComponent<Seeker>();
        }

        /// <summary>
        /// Initializes reference variables.
        /// If you override this function you should in most cases call base.Awake () at the start of it.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            //This is a simple optimization, cache the transform component lookup
            m_transform = transform;

            m_Seeker = GetComponent<Seeker>();

            // Tell the StartEndModifier to ask for our exact position when post processing the path This
            // is important if we are using prediction and requesting a path from some point slightly ahead
            // of us since then the start point in the path request may be far from our position when the
            // path has been calculated. This is also good because if a long path is requested, it may take
            // a few frames for it to be calculated so we could have moved some distance during that time
            m_Seeker.startEndModifier.adjustStartPoint = () => m_transform.position;
        }

        /// <summary>
        /// Starts searching for paths.
        /// If you override this function you should in most cases call base.Start () at the start of it.
        /// (<see cref="Init"/>)
        /// (<see cref="RepeatTrySearchPath"/>)
        /// </summary>
        protected virtual void Start()
        {
            m_startHasRun = true;
            Init();
        }

        /// <summary>
        /// Called when the component is enabled
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            // Make sure we receive callbacks when paths complete
            m_Seeker.pathCallback += OnPathComplete;
            AstarPath.OnGraphsUpdated += OnGraphsUpdated;
            Init();
        }

        void Init()
        {
            if (m_startHasRun)
            {
                m_lastRepath = float.NegativeInfinity;
                StartCoroutine(RepeatTrySearchPath());
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            // Abort any calculations in progress
            if (m_Seeker != null)
            {
                m_Seeker.CancelCurrentPathRequest();
            }
            m_canSearchAgain = true;

            // Release the current path so that it can be pooled
            if (m_path != null)
            {
                m_path.Release(this);
            }
            m_path = null;

            // Make sure we no longer receive callbacks when paths complete
            m_Seeker.pathCallback -= OnPathComplete;
            AstarPath.OnGraphsUpdated -= OnGraphsUpdated;
        }

        /// <summary>
        /// Tries to search for a path every <see cref="RepathRate"/> seconds.
        /// (<see cref="TrySearchPath"/>)
        /// </summary>
        /// <returns></returns>
        protected IEnumerator RepeatTrySearchPath()
        {
            while (m_CanSearch && m_RepathRate >= 0)
            {
                float v = TrySearchPath();
                yield return new WaitForSeconds(v);
            }
        }

        /// <summary>
        /// Tries to search for a path.
        /// Will search for a new path if there was a sufficient time since the last repath and both
        /// <see cref="m_canSearchAgain"/> and <see cref="m_CanSearch"/> are true and there is a target.
        /// </summary>
        /// <returns>The time to wait until calling this function again (based on <see cref="RepathRate"/>)</returns>
        public float TrySearchPath()
        {
            if (Time.time - m_lastRepath >= m_RepathRate && m_canSearchAgain && m_CanSearch && m_Target != null)
            {
                SearchPath();
                return m_RepathRate;
            }
            else
            {
                return Mathf.Max(0, m_RepathRate - (Time.time - m_lastRepath));
            }
        }

        /// <summary>
        /// Requests a path to the target.
        /// Some inheriting classes will prevent the path from being requested immediately when
        /// this function is called, for example when the AI is currently traversing a special path segment
        /// in which case it is usually a bad idea to search for a new path.
        /// </summary>
        public virtual void SearchPath()
        {
            ForceSearchPath();
        }

        /// <summary>
        /// Requests a path to the target for the active graph.
        /// Bypasses 'is-it-a-good-time-to-request-a-path' checks.
        /// </summary>
        /// <param name="onPathUpdated">Perform this action when the path is finished updating</param>
        public virtual void ForceSearchPath(UnityAction onPathUpdated = null)
        {
            if (onPathUpdated != null)
            {
                OnNextPathUpdated.AddPersistentListener(onPathUpdated);
            }

            StartPath();
        }

        /// <summary>
        /// Requests a path to the target for all graphs.
        /// Bypasses 'is-it-a-good-time-to-request-a-path' checks.
        /// </summary>
        /// <param name="onPathUpdated">Perform this action when the path is finished updating</param>
        public virtual void ForceScanGraphs(UnityAction onPathUpdated = null)
        {
            if (onPathUpdated != null)
            {
                OnNextPathUpdated.AddPersistentListener(onPathUpdated);
            }

            List<NavGraph> graphs = new List<NavGraph>();
            for (int i = 0; i < AstarPath.active.graphs.Length; i++)
            {
                NavGraph graph = AstarPath.active.graphs[i];

                // Check if this graph should be searched
                if (graph == null || !SuitableGraph(i, m_Graphs))
                {
                    continue;
                }

                graphs.Add(graph);
            }

            AstarPath.active.Scan(graphs.ToArray());
        }

        public virtual bool SuitableGraph(int graphIndex, int graphMask)
        {
            return ((graphMask >> graphIndex) & 1) != 0;
        }

        public void StartPath()
        {
            if (m_Target == null)
            {
                throw new System.InvalidOperationException("Target is null");
            }

            m_lastRepath = Time.time;
            // This is where we should search to
            var targetPosition = m_Target.position;
            var currentPosition = GetFeetPosition();

            // If we are following a path, start searching from the node we will
            // reach next this can prevent odd turns right at the start of the path
            if (m_interpolator.valid)
            {
                var prevDist = m_interpolator.distance;
                // Move to the end of the current segment
                m_interpolator.MoveToSegment(m_interpolator.segmentIndex, 1);
                currentPosition = m_interpolator.position;
                // Move back to the original position
                m_interpolator.distance = prevDist;
            }

            m_canSearchAgain = false;

            // We should search from the current position
            m_Seeker.StartPath(currentPosition, targetPosition, null, m_Graphs);
        }

        public void OnGraphsUpdated(AstarPath script)
        {
            StartPath();
        }

        /// <summary>
        /// The end of the path has been reached.
        /// If you want custom logic for when the AI has reached it's destination
        /// add it here.
        /// You can also create a new script which inherits from this one
        /// and override the function in that script.
        /// </summary>
        public virtual void OnTargetReached()
        {
        }

        /// <summary>
        /// Called when a requested path has finished calculation.
        /// A path is first requested by <see cref="SearchPath"/>, it is then calculated, probably in the same or the next frame.
        /// Finally it is returned to the seeker which forwards it to this function.
        /// </summary>
        /// <param name="path">The path</param>
        public virtual void OnPathComplete(Path path)
        {
            ABPath p = path as ABPath;

            if (p == null)
            {
                throw new System.Exception("This function only handles ABPaths, do not use special path types");
            }
            m_canSearchAgain = true;

            // Increase the reference count on the path.
            // This is used for path pooling
            p.Claim(this);

            // Path couldn't be calculated of some reason.
            // More info in p.errorLog (debug string)
            if (p.error)
            {
                p.Release(this);
                return;
            }

            if (m_InterpolatePathSwitches)
            {
                ConfigurePathSwitchInterpolation();
            }

            // Release the previous path
            // This is used for path pooling.
            // Note that this will invalidate the interpolator
            // since the vectorPath list will be pooled.
            if (m_path != null)
            {
                m_path.Release(this);
            }

            // Replace the old path
            m_path = p;
            m_targetReached = false;

            // Just for the rest of the code to work, if there
            // is only one waypoint in the path add another one
            if (m_path.vectorPath != null && m_path.vectorPath.Count == 1)
            {
                m_path.vectorPath.Insert(0, GetFeetPosition());
            }

            // Reset some variables
            ConfigureNewPath();
        }

        protected virtual void ConfigurePathSwitchInterpolation()
        {
            bool reachedEndOfPreviousPath = m_interpolator.valid && m_interpolator.remainingDistance < 0.0001f;

            if (m_interpolator.valid && !reachedEndOfPreviousPath)
            {
                m_previousMovementOrigin = m_interpolator.position;
                m_previousMovementDirection = m_interpolator.tangent.normalized * m_interpolator.remainingDistance;
                m_previousMovementStartTime = Time.time;
            }
            else
            {
                m_previousMovementOrigin = Vector3.zero;
                m_previousMovementDirection = Vector3.zero;
                m_previousMovementStartTime = -9999;
            }
        }

        public virtual Vector3 GetFeetPosition()
        {
            return m_transform.position;
        }

        /// <summary>
        /// Finds the closest point on the current path and configures <see cref="m_interpolator"/>.
        /// </summary>
        protected virtual void ConfigureNewPath()
        {
            var hadValidPath = m_interpolator.valid;
            var prevTangent = hadValidPath ? m_interpolator.tangent : Vector3.zero;

            m_interpolator.SetPath(m_path.vectorPath);
            m_interpolator.MoveToClosestPoint(GetFeetPosition());

            if (m_InterpolatePathSwitches && m_SwitchPathInterpolationSpeed > 0.01f && hadValidPath)
            {
                var correctionFactor = Mathf.Max(-Vector3.Dot(prevTangent.normalized, m_interpolator.tangent.normalized), 0);
                m_interpolator.distance -= m_Speed * correctionFactor * (1f / m_SwitchPathInterpolationSpeed);
            }
        }

        protected virtual void Update()
        {
            if (m_CanMove)
            {
                Vector3 direction;
                Vector3 nextPos = CalculateNextPosition(out direction);

                // Rotate unless we are really close to the target
                if (m_EnableRotation && direction != Vector3.zero)
                {
                    if (m_RotateIn2D)
                    {
                        float angle = Mathf.Atan2(direction.x, -direction.y) * Mathf.Rad2Deg + 180;
                        Vector3 euler = m_transform.eulerAngles;
                        euler.z = Mathf.LerpAngle(euler.z, angle, Time.deltaTime * m_RotationSpeed);
                        m_transform.eulerAngles = euler;
                    }
                    else
                    {
                        Quaternion rot = m_transform.rotation;
                        Quaternion desiredRot = Quaternion.LookRotation(direction);

                        m_transform.rotation = Quaternion.Slerp(rot, desiredRot, Time.deltaTime * m_RotationSpeed);
                    }
                }

                m_transform.position = nextPos;
            }
        }

        /// <summary>
        /// Calculate the AI's next position (one frame in the future).
        /// </summary>
        /// <param name="direction">The tangent of the segment the AI is currently traversing (not normalized)</param>
        /// <returns>The AI's next position</returns>
        protected virtual Vector3 CalculateNextPosition(out Vector3 direction)
        {
            if (!m_interpolator.valid)
            {
                direction = Vector3.zero;
                return m_transform.position;
            }

            m_interpolator.distance += Time.deltaTime * m_Speed;
            if (m_interpolator.remainingDistance < 0.0001f && !m_targetReached)
            {
                m_targetReached = true;
                OnTargetReached();
            }

            direction = m_interpolator.tangent;
            float alpha = m_SwitchPathInterpolationSpeed * (Time.time - m_previousMovementStartTime);
            if (m_InterpolatePathSwitches && alpha < 1f)
            {
                // Find the approximate position we would be at if we
                // would have continued to follow the previous path
                Vector3 positionAlongPreviousPath = m_previousMovementOrigin + Vector3.ClampMagnitude(m_previousMovementDirection, m_Speed * (Time.time - m_previousMovementStartTime));

                // Interpolate between the position on the current path and the position
                // we would have had if we would have continued along the previous path.
                return Vector3.Lerp(positionAlongPreviousPath, m_interpolator.position, alpha);
            }
            else
            {
                return m_interpolator.position;
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_RepathRate))
            {
                if (!m_CanSearch)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_RotateIn2D))
            {
                if (!m_EnableRotation)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_RotationSpeed))
            {
                if (!m_EnableRotation)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}