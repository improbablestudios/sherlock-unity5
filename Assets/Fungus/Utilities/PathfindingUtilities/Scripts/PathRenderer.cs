﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.PathfindingUtilities
{

    [RequireComponent(typeof(LineRenderer))]
    [RequireComponent(typeof(Seeker))]
    [AddComponentMenu("Pathfinding/Path Renderer")]
    public class PathRenderer : PersistentBehaviour
    {
        [Tooltip("The graphs that will be scanned for paths")]
        [PathfindingGraphMask]
        [SerializeField]
        protected int m_Graphs;

        [Tooltip("The start position of the path")]
        [SerializeField]
        protected Vector3 m_StartPosition;

        [Tooltip("The end position of the path")]
        [SerializeField]
        protected Vector3 m_EndPosition;

        [Tooltip("Enables or disables searching for paths (Setting this to false does not stop any active path requests from being calculated or stop it from continuing to follow the current path)")]
        [SerializeField]
        protected bool m_CanSearch = true;

        [Tooltip("Determines how often (in seconds) it will search for new paths (If you have fast moving targets or AIs, you might want to set it to a lower value)")]
        [SerializeField]
        protected float m_RepathRate = 0.5F;

        protected UnityEvent m_OnPathUpdated = new UnityEvent();

        protected UnityEvent m_OnNextPathUpdated = new UnityEvent();

        /// <summary>
        /// Time when the last path request was sent
        /// </summary>
        protected float m_lastRepath = -9999;

        /// <summary>
        /// Current path which is followed
        /// </summary>
        protected ABPath m_path;

        /// <summary>
        /// Only when the previous path has been returned should we search for a new path
        /// </summary>
        protected bool m_canSearchAgain = true;

        /// <summary>
        /// Holds if the Start function has been run.
        /// Used to test if coroutines should be started in OnEnable to prevent calculating paths
        /// in the awake stage(or rather before start on frame 0).
        /// </summary>
        private bool m_startHasRun = false;

        private LineRenderer m_LineRenderer;

        private Seeker m_Seeker;

        public Vector3 StartPosition
        {
            get
            {
                return m_StartPosition;
            }
            set
            {
                m_StartPosition = value;
                ForceSearchPath();
            }
        }

        public Vector3 EndPosition
        {
            get
            {
                return m_EndPosition;
            }
            set
            {
                m_EndPosition = value;
                ForceSearchPath();
            }
        }

        public int Graphs
        {
            get
            {
                return m_Graphs;
            }
            set
            {
                m_Graphs = value;
                ForceSearchPath();
            }
        }

        public bool CanSearch
        {
            get
            {
                return m_CanSearch;
            }
            set
            {
                m_CanSearch = value;
            }
        }

        public float RepathRate
        {
            get
            {
                return m_RepathRate;
            }
            set
            {
                m_RepathRate = value;
            }
        }

        public Vector3[] CalculatedPath
        {
            get
            {
                return m_path.vectorPath.ToArray();
            }
        }

        public UnityEvent OnPathUpdated
        {
            get
            {
                return m_OnPathUpdated;
            }
        }

        public UnityEvent OnNextPathUpdated
        {
            get
            {
                return m_OnNextPathUpdated;
            }
        }

        public LineRenderer LineRenderer
        {
            get
            {
                if (m_LineRenderer == null)
                {
                    m_LineRenderer = this.GetRequiredComponent<LineRenderer>();
                }
                return m_LineRenderer;
            }
            set
            {
                m_LineRenderer = value;
                ForceSearchPath();
            }
        }

        public Seeker Seeker
        {
            get
            {
                if (m_Seeker == null)
                {
                    m_Seeker = this.GetRequiredComponent<Seeker>();
                }
                return m_Seeker;
            }
            set
            {
                m_Seeker = value;
                ForceSearchPath();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            LineRenderer = this.GetRequiredComponent<LineRenderer>();
            Seeker = this.GetRequiredComponent<Seeker>();
        }

        /// <summary>
        /// Starts searching for paths.
        /// If you override this function you should in most cases call base.Start () at the start of it.
        /// (<see cref="Init"/>)
        /// (<see cref="RepeatTrySearchPath"/>)
        /// </summary>
        protected virtual void Start()
        {
            m_startHasRun = true;
            Init();
        }

        /// <summary>
        /// Called when the component is enabled
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            // Make sure we receive callbacks when paths complete
            Seeker.pathCallback += OnPathComplete;
            AstarPath.OnGraphsUpdated += OnGraphsUpdated;
            Init();
        }

        void Init()
        {
            if (m_startHasRun)
            {
                m_lastRepath = float.NegativeInfinity;
                StartCoroutine(RepeatTrySearchPath());
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            // Abort any calculations in progress
            if (Seeker != null)
            {
                Seeker.CancelCurrentPathRequest();
            }
            m_canSearchAgain = true;

            // Release the current path so that it can be pooled
            if (m_path != null)
            {
                m_path.Release(this);
            }
            m_path = null;

            // Make sure we no longer receive callbacks when paths complete
            m_Seeker.pathCallback -= OnPathComplete;
            AstarPath.OnGraphsUpdated -= OnGraphsUpdated;
        }

        /// <summary>
        /// Tries to search for a path every #repathRate seconds.
        /// <see cref="TrySearchPath"/>
        /// </summary>
        /// <returns>The IEnumerator used to pass this function into a coroutine</returns>
        protected IEnumerator RepeatTrySearchPath()
        {
            while (m_CanSearch && m_RepathRate >= 0)
            {
                float v = TrySearchPath();
                yield return new WaitForSeconds(v);
            }
        }

        /// <summary>
        /// Tries to search for a path.
        /// Will search for a new path if there was a sufficient time since the last repath and both
        /// <see cref="m_canSearchAgain"/> and <see cref="m_CanSearch"/> are true and there is a target.
        /// </summary>
        /// <returns>The time to wait until calling this function again (based on <see cref="m_RepathRate"/>)</returns>
        public float TrySearchPath()
        {
            if (Time.time - m_lastRepath >= m_RepathRate && m_canSearchAgain && m_CanSearch)
            {
                SearchPath(StartPosition, EndPosition);
                return m_RepathRate;
            }
            else
            {
                return Mathf.Max(0, m_RepathRate - (Time.time - m_lastRepath));
            }
        }

        /// <summary>
        /// Requests a path to the target.
        /// Some inheriting classes will prevent the path from being requested immediately when
        /// this function is called, for example when the AI is currently traversing a special path segment
        /// in which case it is usually a bad idea to search for a new path.
        /// </summary>
        /// <param name="startPosition">The starting point of the path</param>
        /// <param name="endPosition">The ending point of the path</param>
        public virtual void SearchPath(Vector3 startPosition, Vector3 endPosition)
        {
            m_StartPosition = startPosition;
            m_EndPosition = endPosition;
            ForceSearchPath();
        }

        /// <summary>
        /// Requests a path to the target in the active graph.
        /// Bypasses 'is-it-a-good-time-to-request-a-path' checks.
        /// </summary>
        /// <param name="onPathUpdated">Perform this action when the path is finished updating</param>
        public virtual void ForceSearchPath(UnityAction onPathUpdated = null)
        {
            if (onPathUpdated != null)
            {
                OnNextPathUpdated.AddPersistentListener(onPathUpdated);
            }

            StartPath();
        }

        /// <summary>
        /// Requests a path to the target in all graphs.
        /// Bypasses 'is-it-a-good-time-to-request-a-path' checks.
        /// </summary>
        public virtual void ForceScanGraphs()
        {
            List<NavGraph> graphs = new List<NavGraph>();
            for (int i = 0; i < AstarPath.active.graphs.Length; i++)
            {
                NavGraph graph = AstarPath.active.graphs[i];

                // Check if this graph should be searched
                if (graph == null || !SuitableGraph(i, m_Graphs))
                {
                    continue;
                }

                graphs.Add(graph);
            }

            AstarPath.active.Scan(graphs.ToArray());
        }

        public virtual bool SuitableGraph(int graphIndex, int graphMask)
        {
            return ((graphMask >> graphIndex) & 1) != 0;
        }

        private void StartPath()
        {
            m_lastRepath = Time.time;

            m_canSearchAgain = false;

            Seeker.StartPath(StartPosition, EndPosition, null, Graphs);
        }

        public void OnGraphsUpdated(AstarPath script)
        {
            StartPath();
        }

        /// <summary>
        /// Called when a requested path has finished calculation.
        /// A path is first requested by #SearchPath, it is then calculated, probably in the same or the next frame.
        /// Finally it is returned to the seeker which forwards it to this function.
        /// </summary>
        /// <param name="path">The path</param>
        public virtual void OnPathComplete(Path path)
        {
            ABPath p = path as ABPath;

            if (p == null)
            {
                throw new System.Exception("This function only handles ABPaths, do not use special path types");
            }
            m_canSearchAgain = true;

            // Increase the reference count on the path.
            // This is used for path pooling
            p.Claim(this);

            // Path couldn't be calculated of some reason.
            // More info in p.errorLog (debug string)
            if (p.error)
            {
                p.Release(this);
                return;
            }

            DrawPathLine(p);

            // Release the previous path
            // This is used for path pooling.
            // Note that this will invalidate the interpolator
            // since the vectorPath list will be pooled.
            if (m_path != null)
            {
                m_path.Release(this);
            }
            // Replace the old path
            m_path = p;

            InvokePathUpdated();
        }

        private void InvokePathUpdated()
        {
            OnPathUpdated.Invoke();
            OnNextPathUpdated.Invoke();
            OnNextPathUpdated.RemoveAllPersistentAndNonPersistentListeners();
        }

        public float GetDistanceFromPathEndToEndPosition()
        {
            if (m_path != null)
            {
                if (m_path.vectorPath.Count > 0)
                {
                    return (Vector3.Distance(m_path.endPoint, m_path.originalEndPoint));
                }
            }
            return -1;
        }

        void DrawPathLine(ABPath p)
        {
            if (p.vectorPath != null)
            {
                LineRenderer.positionCount = p.vectorPath.Count;
                LineRenderer.SetPositions(p.vectorPath.ToArray());
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_RepathRate))
            {
                if (!m_CanSearch)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}

