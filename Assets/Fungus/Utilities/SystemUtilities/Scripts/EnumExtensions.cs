﻿using System;

namespace ImprobableStudios.SystemUtilities
{

    public static class EnumExtensions
    {
        public static bool HasFlag(this Enum variable, Enum value)
        {
            if (variable == null)
            {
                return false;
            }

            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            
            if (!Enum.IsDefined(variable.GetType(), value))
            {
                throw new ArgumentException(string.Format(
                    "Enumeration type mismatch.  The flag is of type '{0}', was expecting '{1}'.",
                    value.GetType(), variable.GetType()));
            }
            
            try
            {
                ulong num = Convert.ToUInt64(value);
                ulong num2 = Convert.ToUInt64(variable);
                return (num2 & num) == num;
            }
            catch (OverflowException)
            {
                long num = Convert.ToInt64(value);
                long num2 = Convert.ToInt64(variable);
                return (num2 & num) == num;
            }
        }
    }

}