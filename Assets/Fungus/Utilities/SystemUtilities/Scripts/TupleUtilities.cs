﻿using System;
using UnityEngine.Serialization;

namespace ImprobableStudios.SystemUtilities
{

    [Serializable]
    public class Tuple<T, U>
    {
        [FormerlySerializedAs("first")]
        public T m_First;

        [FormerlySerializedAs("second")]
        public U m_Second;
        
        public Tuple(T first, U second)
        {
            this.m_First = first;
            this.m_Second = second;
        }

        public override int GetHashCode()
        {
            return m_First.GetHashCode() ^ m_Second.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return Equals((Tuple<T, U>)obj);
        }

        public bool Equals(Tuple<T, U> other)
        {
            return other.m_First.Equals(m_First) && other.m_Second.Equals(m_Second);
        }
    }

}
