﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UnityObjectUtilities
{

    public static class SpriteExtensions
    {
        public static Component GetSpriteComponent(this GameObject go)
        {
            Image image = go.GetComponent<Image>();
            SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
            ISpriteComponent spriteComponent = go.GetComponent<ISpriteComponent>();

            if (image != null)
            {
                return image;
            }
            else if (spriteRenderer != null)
            {
                return spriteRenderer;
            }
            else if (spriteComponent != null)
            {
                return spriteComponent.GetComponent();
            }

            return null;
        }

        public static Component[] GetSpriteComponents(this GameObject go)
        {
            List<Component> spriteComponents = new List<Component>();
            spriteComponents.AddRange(go.GetComponents<Image>());
            spriteComponents.AddRange(go.GetComponents<SpriteRenderer>());
            spriteComponents.AddRange(go.GetComponents<ISpriteComponent>().Select(i => i.GetComponent()));
            return spriteComponents.ToArray();
        }

        public static Component[] GetSpriteComponentsInChildren(this GameObject go, bool includeInactive = false)
        {
            List<Component> spriteComponents = new List<Component>();
            spriteComponents.AddRange(go.GetComponentsInChildren<Image>(includeInactive));
            spriteComponents.AddRange(go.GetComponentsInChildren<SpriteRenderer>(includeInactive));
            spriteComponents.AddRange(go.GetComponentsInChildren<ISpriteComponent>(includeInactive).Select(i => i.GetComponent()));
            return spriteComponents.ToArray();
        }

        public static Sprite GetSprite(this GameObject go)
        {
            Image image = go.GetComponent<Image>();
            SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
            ISpriteComponent spriteComponent = go.GetComponent<ISpriteComponent>();

            if (image != null)
            {
                return image.sprite;
            }
            else if (spriteRenderer != null)
            {
                return spriteRenderer.sprite;
            }
            else if (spriteComponent != null)
            {
                return spriteComponent.GetSprite();
            }

            return null;
        }

        public static void SetSprite(this GameObject go, Sprite sprite)
        {
            Image image = go.GetComponent<Image>();
            SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
            ISpriteComponent spriteComponent = go.GetComponent<ISpriteComponent>();

            if (image != null)
            {
                image.sprite = sprite;
            }
            else if (spriteRenderer != null)
            {
                spriteRenderer.sprite = sprite;
            }
            else if (spriteComponent != null)
            {
                spriteComponent.SetSprite(sprite);
            }
        }

        public static Sprite GetSprite(this Component c)
        {
            Image image = c as Image;
            SpriteRenderer spriteRenderer = c as SpriteRenderer;
            ISpriteComponent spriteComponent = c as ISpriteComponent;

            if (image != null)
            {
                return image.sprite;
            }
            else if (spriteRenderer != null)
            {
                return spriteRenderer.sprite;
            }
            else if (spriteComponent != null)
            {
                return spriteComponent.GetSprite();
            }

            return null;
        }

        public static void SetSprite(this Component c, Sprite sprite)
        {
            Image image = c as Image;
            SpriteRenderer spriteRenderer = c as SpriteRenderer;
            ISpriteComponent spriteComponent = c as ISpriteComponent;

            if (image != null)
            {
                image.sprite = sprite;
            }
            else if (spriteRenderer != null)
            {
                spriteRenderer.sprite = sprite;
            }
            else if (spriteComponent != null)
            {
                spriteComponent.SetSprite(sprite);
            }
        }

        public static Vector2 GetCenter(this SpriteRenderer spriteRenderer)
        {
            return spriteRenderer.transform.TransformPoint(spriteRenderer.sprite.bounds.center);
        }
    }

}
