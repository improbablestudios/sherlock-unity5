﻿using UnityEngine;

namespace ImprobableStudios.UnityObjectUtilities
{

    public interface ISpriteComponent
    {
        Sprite GetSprite();
        void SetSprite(Sprite value);
        Component GetComponent();
    }

}
