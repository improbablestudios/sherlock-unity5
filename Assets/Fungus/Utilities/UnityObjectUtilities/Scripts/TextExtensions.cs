﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.UnityObjectUtilities
{

    public static class TextExtensions
    {
        public static Component GetTextComponent(this GameObject go)
        {
            Text text = go.GetComponent<Text>();
            InputField inputField = go.GetComponent<InputField>();
            TextMesh textMesh = go.GetComponent<TextMesh>();
            ITextComponent textComponent = go.GetComponent<ITextComponent>();

            if (text != null)
            {
                return text;
            }
            else if (inputField != null)
            {
                return inputField;
            }
            else if (textMesh != null)
            {
                return textMesh;
            }
            else if (textComponent != null)
            {
                return textComponent.GetComponent();
            }

            return null;
        }

        public static Component[] GetTextComponents(this GameObject go)
        {
            List<Component> textComponents = new List<Component>();
            textComponents.AddRange(go.GetComponents<Text>());
            textComponents.AddRange(go.GetComponents<InputField>());
            textComponents.AddRange(go.GetComponents<TextMesh>());
            textComponents.AddRange(go.GetComponents<ITextComponent>().Select(i => i.GetComponent()));
            return textComponents.ToArray();
        }

        public static Component[] GetTextComponentsInChildren(this GameObject go, bool includeInactive = false)
        {
            List<Component> textComponents = new List<Component>();
            textComponents.AddRange(go.GetComponentsInChildren<Text>(includeInactive));
            textComponents.AddRange(go.GetComponentsInChildren<InputField>(includeInactive));
            textComponents.AddRange(go.GetComponentsInChildren<TextMesh>(includeInactive));
            textComponents.AddRange(go.GetComponentsInChildren<ITextComponent>(includeInactive).Select(i => i.GetComponent()));
            return textComponents.ToArray();
        }

        public static string GetText(this GameObject go)
        {
            Text uiText = go.GetComponent<Text>();
            InputField inputField = go.GetComponent<InputField>();
            TextMesh textMesh = go.GetComponent<TextMesh>();
            ITextComponent textComponent = go.GetComponent<ITextComponent>();

            if (uiText != null)
            {
                return uiText.text;
            }
            else if (inputField != null)
            {
                return inputField.text;
            }
            else if (textMesh != null)
            {
                return textMesh.text;
            }
            else if (textComponent != null)
            {
                return textComponent.GetString();
            }

            return "";
        }

        public static void SetText(this GameObject go, string text)
        {
            Text uiText = go.GetComponent<Text>();
            InputField inputField = go.GetComponent<InputField>();
            TextMesh textMesh = go.GetComponent<TextMesh>();
            ITextComponent textComponent = go.GetComponent<ITextComponent>();

            if (uiText != null)
            {
                uiText.text = text;
            }
            else if (inputField != null)
            {
                inputField.text = text;
            }
            else if (textMesh != null)
            {
                textMesh.text = text;
            }
            else if (textComponent != null)
            {
                textComponent.SetString(text);
            }
        }

        public static string GetText(this Component c)
        {
            Text uiText = c as Text;
            InputField inputField = c as InputField;
            TextMesh textMesh = c as TextMesh;
            ITextComponent textComponent = c as ITextComponent;

            if (uiText != null)
            {
                return uiText.text;
            }
            else if (inputField != null)
            {
                return inputField.text;
            }
            else if (textMesh != null)
            {
                return textMesh.text;
            }
            else if (textComponent != null)
            {
                return textComponent.GetString();
            }

            return "";
        }

        public static void SetText(this Component c, string text)
        {
            Text uiText = c as Text;
            InputField inputField = c as InputField;
            TextMesh textMesh = c as TextMesh;
            ITextComponent textComponent = c as ITextComponent;

            if (uiText != null)
            {
                uiText.text = text;
            }
            else if (inputField != null)
            {
                inputField.text = text;
            }
            else if (textMesh != null)
            {
                textMesh.text = text;
            }
            else if (textComponent != null)
            {
                textComponent.SetString(text);
            }
        }

        public static string RemoveUnityTags(this string text)
        {
            return text
                .RemoveUnityTag("b")
                .RemoveUnityTag("i")
                .RemoveUnityTag("size")
                .RemoveUnityTag("color");
        }

        public static string RemoveUnityTag(this string text, string tag)
        {
            Regex startRegex = new Regex("<" + tag + ".*?>");
            foreach (Match match in startRegex.Matches(text))
            {
                text = text.Replace(match.Value, "");
            }
            
            Regex endRegex = new Regex("</" + tag + ">");
            foreach (Match match in endRegex.Matches(text))
            {
                text = text.Replace(match.Value, "");
            }
            return text;
        }

        public static string ReplaceBraceTagsWithBracketTags(this string text)
        {
            return text
                .ReplaceBraceTagWithBracketTag("b")
                .ReplaceBraceTagWithBracketTag("i")
                .ReplaceBraceTagWithBracketTag("size")
                .ReplaceBraceTagWithBracketTag("color");
        }

        public static string ReplaceBraceTagWithBracketTag(this string text, string tag)
        {
            Regex startRegex = new Regex("{" + tag + ".*?}");
            foreach (Match match in startRegex.Matches(text))
            {
                string unityTag = match.Value.Replace("{", "<").Replace("}", ">");
                text = text.Replace(match.Value, unityTag);
            }

            Regex endRegex = new Regex("{/" + tag + "}");
            foreach (Match match in endRegex.Matches(text))
            {
                string unityTag = match.Value.Replace("{", "<").Replace("}", ">");
                text = text.Replace(match.Value, unityTag);
            }

            return text;
        }
        
        public static TextGenerator CloneTextGenerator(this Text textUI, string stringText)
        {
            TextGenerator generator = new TextGenerator();
            TextGenerationSettings settings = new TextGenerationSettings();
            settings = textUI.GetGenerationSettings(textUI.rectTransform.rect.size);
            generator.Populate(stringText, settings);
            return generator;
        }

        public static string GetTruncatedText(this Text textUI, string stringText)
        {
            TextGenerator generator = CloneTextGenerator(textUI, stringText);
            int characterCountVisible = generator.characterCountVisible;
            if (characterCountVisible >= 0 && characterCountVisible < stringText.Length)
            {
                string visibleText = stringText.Remove(characterCountVisible);
                int spaceBeforeTruncationPos = visibleText.LastIndexOf(" ");
                int breakBeforeTruncationPos = visibleText.LastIndexOf("\n");
                int safeTruncationPos = Math.Max(spaceBeforeTruncationPos, breakBeforeTruncationPos);
                string truncatedText = stringText.Substring(characterCountVisible);
                if (spaceBeforeTruncationPos >= 0)
                {
                    truncatedText = stringText.Substring(safeTruncationPos).TrimStart();
                }
                return RemoveUnityTags(truncatedText);
            }
            return "";
        }
    }

}