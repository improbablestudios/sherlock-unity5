﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.UnityObjectUtilities
{

    public static class GameObjectExtensions
    {
        // For performance reasons, it is recommended to not use any of the following GetComponent functions every frame.
        // Instead, cache the result in a member variable at startup.

        public static T GetComponentInParent<T>(this Component target, bool includeInactive = false) where T : Component
        {
            return GetComponentInParent<T>(target.gameObject, includeInactive);
        }

        public static T GetComponentInParent<T>(this GameObject target, bool includeInactive = false) where T : Component
        {
            T[] parents = target.GetComponentsInParent<T>(includeInactive);
            T parent = null;
            if (parents.Length > 0)
            {
                parent = parents[0];
            }
            return parent;
        }

        public static T GetRequiredChildComponent<T>(this Component target, string childName) where T : Component
        {
            return GetRequiredChildComponent<T>(target.gameObject, childName);
        }

        public static T GetRequiredChildComponent<T>(this GameObject target, string childName) where T : Component
        {
            foreach (T c in target.GetComponentsInChildren<T>(true))
            {
                if (c.gameObject != target && c.name == childName)
                {
                    return c;
                }
            }

            T component = new GameObject(childName).AddComponent<T>();
            component.transform.SetParent(target.transform);
            component.transform.localPosition = Vector3.zero;
            component.transform.localEulerAngles = Vector3.zero;
            component.transform.localScale = Vector3.one;

            return component;
        }

        public static T GetRequiredComponent<T>(this Component target) where T : Component
        {
            return GetRequiredComponent<T>(target.gameObject);
        }

        public static T GetRequiredComponent<T>(this GameObject target) where T : Component
        {
            T component = target.GetComponent<T>();
            if (component == null)
            {
                component = target.AddComponent<T>();
            }

            return component;
        }

        public static T GetComponent<T>(this UnityEngine.Object obj) where T : Component
        {
            return GetComponent(obj, typeof(T)) as T;
        }
        
        public static Component GetComponent(this UnityEngine.Object obj, Type type)
        {
            if (obj == null)
            {
                return null;
            }
            Component componentObj = obj as Component;
            GameObject gameObjectObj = obj as GameObject;
            if (type.IsAssignableFrom(obj.GetType()))
            {
                return componentObj;
            }
            else if (componentObj != null)
            {
                Component attachedComponent = componentObj.GetComponent(type);
                if (attachedComponent != null)
                {
                    return attachedComponent;
                }
                else
                {
                    return null;
                }
            }
            else if (gameObjectObj != null)
            {
                return gameObjectObj.GetComponent(type);
            }
            return null;
        }

        public static GameObject FindGrandChild(this GameObject parent, string name)
        {
            return FindGrandChild(parent.transform, name).gameObject;
        }

        public static Transform FindGrandChild(this Transform parent, string name)
        {
            var result = parent.Find(name);
            if (result != null)
                return result;
            foreach (Transform child in parent)
            {
                result = FindGrandChild(child, name);
                if (result != null)
                    return result;
            }
            return null;
        }

#if UNITY_EDITOR
        public static bool IsParentOrSelfSelected(this Component component)
        {
            return IsParentOrSelfSelected(component.transform);
        }

        public static bool IsParentOrSelfSelected(this GameObject gameObject)
        {
            return IsParentOrSelfSelected(gameObject.transform);
        }

        public static bool IsParentOrSelfSelected(this Transform transform)
        {
            foreach (Transform selectedGameObject in UnityEditor.Selection.transforms)
            {
                if (selectedGameObject == transform)
                {
                    return true;
                }
                foreach (Transform child in selectedGameObject.GetComponentsInChildren<Transform>())
                {
                    if (child == transform)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
#endif
    }

}