﻿using UnityEngine;

namespace ImprobableStudios.UnityObjectUtilities
{

    public interface ITextComponent
    {
        string GetString();
        void SetString(string value);
        Component GetComponent();
    }

}
