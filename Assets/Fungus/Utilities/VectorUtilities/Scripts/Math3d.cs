﻿using System;
using UnityEngine;

namespace ImprobableStudios.VectorUtilities
{

    public class Math3d
    {
        public const float PI_OVER_TWO = 1.570796326794897f;

        /// <summary>
        /// Increase or decrease the length of vector by size
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Vector3 AddVectorLength(Vector3 vector, float size)
        {
            //get the vector length
            float magnitude = Vector3.Magnitude(vector);

            //calculate new vector length
            float newMagnitude = magnitude + size;

            //calculate the ratio of the new length to the old length
            float scale = newMagnitude / magnitude;

            //scale the vector
            return vector * scale;
        }

        /// <summary>
        /// Create a vector of direction "vector" with length "size"
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Vector3 SetVectorLength(Vector3 vector, float size)
        {
            //normalize the vector
            Vector3 vectorNormalized = Vector3.Normalize(vector);

            //scale the vector
            return vectorNormalized *= size;
        }

        /// <summary>
        /// Find the line of intersection between two planes.The planes are defined by a normal and a point on that plane.
        /// The outputs are a point on the line and a vector which indicates it's direction. If the planes are not parallel, 
        /// the function outputs true, otherwise false.
        /// </summary>
        /// <param name="linePoint"></param>
        /// <param name="lineVec"></param>
        /// <param name="plane1Normal"></param>
        /// <param name="plane1Position"></param>
        /// <param name="plane2Normal"></param>
        /// <param name="plane2Position"></param>
        /// <returns></returns>
        public static bool PlanePlaneIntersection(out Vector3 linePoint, out Vector3 lineVec, Vector3 plane1Normal, Vector3 plane1Position, Vector3 plane2Normal, Vector3 plane2Position)
        {
            linePoint = Vector3.zero;
            lineVec = Vector3.zero;

            //We can get the direction of the line of intersection of the two planes by calculating the 
            //cross product of the normals of the two planes. Note that this is just a direction and the line
            //is not fixed in space yet. We need a point for that to go with the line vector.
            lineVec = Vector3.Cross(plane1Normal, plane2Normal);

            //Next is to calculate a point on the line to fix it's position in space. This is done by finding a vector from
            //the plane2 location, moving parallel to it's plane, and intersecting plane1. To prevent rounding
            //errors, this vector also has to be perpendicular to lineDirection. To get this vector, calculate
            //the cross product of the normal of plane2 and the lineDirection.		
            Vector3 ldir = Vector3.Cross(plane2Normal, lineVec);

            float denominator = Vector3.Dot(plane1Normal, ldir);

            //Prevent divide by zero and rounding errors by requiring about 5 degrees angle between the planes.
            if (Mathf.Abs(denominator) > 0.006f)
            {

                Vector3 plane1ToPlane2 = plane1Position - plane2Position;
                float t = Vector3.Dot(plane1Normal, plane1ToPlane2) / denominator;
                linePoint = plane2Position + t * ldir;

                return true;
            }

            //output not valid
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the intersection between a line and a plane. 
        /// If the line and plane are not parallel, the function outputs true, otherwise false.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="linePoint"></param>
        /// <param name="lineVec"></param>
        /// <param name="planeNormal"></param>
        /// <param name="planePoint"></param>
        /// <returns></returns>
        public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint)
        {
            float length;
            float dotNumerator;
            float dotDenominator;
            Vector3 vector;
            intersection = Vector3.zero;

            //calculate the distance between the linePoint and the line-plane intersection point
            dotNumerator = Vector3.Dot((planePoint - linePoint), planeNormal);
            dotDenominator = Vector3.Dot(lineVec, planeNormal);

            //line and plane are not parallel
            if (dotDenominator != 0.0f)
            {
                length = dotNumerator / dotDenominator;

                //create a vector from the linePoint to the intersection point
                vector = SetVectorLength(lineVec, length);

                //get the coordinates of the line-plane intersection point
                intersection = linePoint + vector;

                return true;
            }

            //output not valid
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Calculate the intersection point of two lines.Returns true if lines intersect, otherwise false.
        /// Note that in 3d, two lines do not intersect most of the time. So if the two lines are not in the 
        /// same plane, use ClosestPointsOnTwoLines() instead.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="linePoint1"></param>
        /// <param name="lineVec1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="lineVec2"></param>
        /// <returns></returns>
        public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {
            Vector3 lineVec3 = linePoint2 - linePoint1;
            Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
            Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

            float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

            //is coplanar, and not parrallel
            if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
            {
                float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
                intersection = linePoint1 + (lineVec1 * s);
                return true;
            }
            else
            {
                intersection = Vector3.zero;
                return false;
            }
        }

        /// <summary>
        /// Two non-parallel lines which may or may not touch each other have a point on each line which are closest
        /// to each other. This function finds those two points. If the lines are not parallel, the function 
        /// </summary>
        /// <param name="closestPointLine1"></param>
        /// <param name="closestPointLine2"></param>
        /// <param name="linePoint1"></param>
        /// <param name="lineVec1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="lineVec2"></param>
        /// <returns></returns>
        public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {
            closestPointLine1 = Vector3.zero;
            closestPointLine2 = Vector3.zero;

            float a = Vector3.Dot(lineVec1, lineVec1);
            float b = Vector3.Dot(lineVec1, lineVec2);
            float e = Vector3.Dot(lineVec2, lineVec2);

            float d = a * e - b * b;

            //lines are not parallel
            if (d != 0.0f)
            {

                Vector3 r = linePoint1 - linePoint2;
                float c = Vector3.Dot(lineVec1, r);
                float f = Vector3.Dot(lineVec2, r);

                float s = (b * f - c * e) / d;
                float t = (a * f - c * b) / d;

                closestPointLine1 = linePoint1 + lineVec1 * s;
                closestPointLine2 = linePoint2 + lineVec2 * t;

                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function returns a point which is a projection from a point to a line.
        /// The line is regarded infinite. If the line is finite, use ProjectPointOnLineSegment() instead.
        /// </summary>
        /// <param name="linePoint"></param>
        /// <param name="lineVec"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
        {
            //get vector from point on line to point in space
            Vector3 linePointToPoint = point - linePoint;

            float t = Vector3.Dot(linePointToPoint, lineVec);

            return linePoint + lineVec * t;
        }

        /// <summary>
        /// This function returns a point which is a projection from a point to a line segment.
        /// If the projected point lies outside of the line segment, the projected point will 
        /// be clamped to the appropriate line edge.
        /// If the line is infinite instead of a segment, use ProjectPointOnLine() instead.
        /// </summary>
        /// <param name="linePoint1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {
            Vector3 vector = linePoint2 - linePoint1;

            Vector3 projectedPoint = ProjectPointOnLine(linePoint1, vector.normalized, point);

            int side = PointOnWhichSideOfLineSegment(linePoint1, linePoint2, projectedPoint);

            //The projected point is on the line segment
            if (side == 0)
            {
                return projectedPoint;
            }

            if (side == 1)
            {

                return linePoint1;
            }

            if (side == 2)
            {
                return linePoint2;
            }

            //output is invalid
            return Vector3.zero;
        }

        /// <summary>
        /// This function returns a point which is a projection from a point to a plane.
        /// </summary>
        /// <param name="planeNormal"></param>
        /// <param name="planePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            float distance;
            Vector3 translationVector;

            //First calculate the distance from the point to the plane:
            distance = SignedDistancePlanePoint(planeNormal, planePoint, point);

            //Reverse the sign of the distance
            distance *= -1;

            //Get a translation vector
            translationVector = SetVectorLength(planeNormal, distance);

            //Translate the point to form a projection
            return point + translationVector;
        }

        /// <summary>
        /// Projects a vector onto a plane. The output is not normalized.
        /// </summary>
        /// <param name="planeNormal"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector3 ProjectVectorOnPlane(Vector3 planeNormal, Vector3 vector)
        {
            return vector - (Vector3.Dot(vector, planeNormal) * planeNormal);
        }

        /// <summary>
        /// Get the shortest distance between a point and a plane. The output is signed so it holds information
        /// as to which side of the plane normal the point is.
        /// </summary>
        /// <param name="planeNormal"></param>
        /// <param name="planePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static float SignedDistancePlanePoint(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            return Vector3.Dot(planeNormal, (point - planePoint));
        }

        /// <summary>
        /// This function calculates a signed (+ or - sign instead of being ambiguous) dot product. It is basically used
        /// to figure out whether a vector is positioned to the left or right of another vector. The way this is done is
        /// by calculating a vector perpendicular to one of the vectors and using that as a reference. This is because
        /// the result of a dot product only has signed information when an angle is transitioning between more or less
        /// than 90 degrees.
        /// </summary>
        /// <param name="vectorA"></param>
        /// <param name="vectorB"></param>
        /// <param name="normal"></param>
        /// <returns></returns>
        public static float SignedDotProduct(Vector3 vectorA, Vector3 vectorB, Vector3 normal)
        {
            Vector3 perpVector;
            float dot;

            //Use the geometry object normal and one of the input vectors to calculate the perpendicular vector
            perpVector = Vector3.Cross(normal, vectorA);

            //Now calculate the dot product between the perpendicular vector (perpVector) and the other input vector
            dot = Vector3.Dot(perpVector, vectorB);

            return dot;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="referenceVector"></param>
        /// <param name="otherVector"></param>
        /// <param name="normal"></param>
        /// <returns></returns>
        public static float SignedVectorAngle(Vector3 referenceVector, Vector3 otherVector, Vector3 normal)
        {
            Vector3 perpVector;
            float angle;

            //Use the geometry object normal and one of the input vectors to calculate the perpendicular vector
            perpVector = Vector3.Cross(normal, referenceVector);

            //Now calculate the dot product between the perpendicular vector (perpVector) and the other input vector
            angle = Vector3.Angle(referenceVector, otherVector);
            angle *= Mathf.Sign(Vector3.Dot(perpVector, otherVector));

            return angle;
        }

        /// <summary>
        /// Calculate the angle between a vector and a plane.The plane is made by a normal vector.
        /// Output is in radians.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="normal"></param>
        /// <returns></returns>
        public static float AngleVectorPlane(Vector3 vector, Vector3 normal)
        {
            float dot;
            float angle;

            //calculate the the dot product between the two input vectors. This gives the cosine between the two vectors
            dot = Vector3.Dot(vector, normal);

            //this is in radians
            angle = (float)Math.Acos(dot);

            return PI_OVER_TWO - angle; //90 degrees - angle
        }

        //
        /// <summary>
        /// Calculate the dot product as an angle.
        /// </summary>
        /// <param name="vec1">first vector</param>
        /// <param name="vec2">second vector</param>
        /// <returns></returns>
        public static float DotProductAngle(Vector3 vec1, Vector3 vec2)
        {
            vec1 = vec1.normalized;
            vec2 = vec2.normalized;

            double dot;
            double angle;

            //get the dot product
            dot = Vector3.Dot(vec1, vec2);

            //Clamp to prevent NaN error. Shouldn't need this in the first place, but there could be a rounding error issue.
            if (dot < -1.0f)
            {
                dot = -1.0f;
            }
            if (dot > 1.0f)
            {
                dot = 1.0f;
            }

            //Calculate the angle. The output is in radians
            //This step can be skipped for optimization...
            angle = Math.Acos(dot);

            return (float)angle;
        }

        /// <summary>
        /// This function finds out on which side of a line segment the point is located.
        /// The point is assumed to be on a line created by linePoint1 and linePoint2. If the point is not on
        /// the line segment, project it on the line using ProjectPointOnLine() first.
        /// Returns 0 if point is on the line segment.
        /// Returns 1 if point is outside of the line segment and located on the side of linePoint1.
        /// Returns 2 if point is outside of the line segment and located on the side of linePoint2.
        /// </summary>
        /// <param name="linePoint1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {

            Vector3 lineVec = linePoint2 - linePoint1;
            Vector3 pointVec = point - linePoint1;

            float dot = Vector3.Dot(pointVec, lineVec);

            //point is on side of linePoint2, compared to linePoint1
            if (dot > 0)
            {
                //point is on the line segment
                if (pointVec.magnitude <= lineVec.magnitude)
                {
                    return 0;
                }
                //point is not on the line segment and it is on the side of linePoint2
                else
                {
                    return 2;
                }
            }
            //Point is not on side of linePoint2, compared to linePoint1.
            //Point is not on the line segment and it is on the side of linePoint1.
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Returns the pixel distance from a point to a line.
        /// </summary>
        /// <param name="linePoint1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static float PointDistanceToLine(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {
            Vector3 projectedPoint = ProjectPointOnLineSegment(linePoint1, linePoint1, point);

            projectedPoint = new Vector3(projectedPoint.x, projectedPoint.y, 0f);

            Vector3 vector = projectedPoint - point;
            return vector.magnitude;
        }

        /// <summary>
        /// Returns true if a line segment (made up of linePoint1 and linePoint2) is fully or partially in a rectangle
        /// made up of RectA to RectD. The line segment is assumed to be on the same plane as the rectangle. If the line is 
        /// not on the plane, use ProjectPointOnPlane() on linePoint1 and linePoint2 first.
        /// </summary>
        /// <param name="linePoint1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="rectA">Bottom-Left point</param>
        /// <param name="rectC">Top-Right point</param>
        /// <param name="rectB">Top-Left point</param>
        /// <param name="rectD">Bottom-Right point</param>
        /// <returns></returns>
        public static bool IsLineInRectangle(Vector3 linePoint1, Vector3 linePoint2, Vector3 rectA, Vector3 rectB, Vector3 rectC, Vector3 rectD)
        {
            bool pointAInside = false;
            bool pointBInside = false;

            pointAInside = IsPointInRectangle(linePoint1, rectA, rectC, rectB, rectD);

            if (!pointAInside)
            {
                pointBInside = IsPointInRectangle(linePoint2, rectA, rectC, rectB, rectD);
            }

            //none of the points are inside, so check if a line is crossing
            if (!pointAInside && !pointBInside)
            {
                if (AreLineSegmentsCrossing(linePoint1, linePoint2, rectA, rectB))
                {
                    return true;
                }
                if (AreLineSegmentsCrossing(linePoint1, linePoint2, rectB, rectC))
                {
                    return true;
                }
                if (AreLineSegmentsCrossing(linePoint1, linePoint2, rectC, rectD))
                {
                    return true;
                }
                if (AreLineSegmentsCrossing(linePoint1, linePoint2, rectD, rectA))
                {
                    return true;
                }
                return false;
            }

            else
            {
                return true;
            }
        }

        /// <summary>
        /// Returns true if "point" is in a rectangle mad up of RectA to RectD. The line point is assumed to be on the same 
        /// plane as the rectangle. If the point is not on the plane, use ProjectPointOnPlane() first.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="rectA">Bottom-Left point</param>
        /// <param name="rectC">Top-Right point</param>
        /// <param name="rectB">Top-Left point</param>
        /// <param name="rectD">Bottom-Right point</param>
        /// <returns></returns>
        public static bool IsPointInRectangle(Vector3 point, Vector3 rectA, Vector3 rectC, Vector3 rectB, Vector3 rectD)
        {
            Vector3 vector;
            Vector3 linePoint;

            //get the center of the rectangle
            vector = rectC - rectA;
            float size = -(vector.magnitude / 2f);
            vector = AddVectorLength(vector, size);
            Vector3 middle = rectA + vector;

            Vector3 xVector = rectB - rectA;
            float width = xVector.magnitude / 2f;

            Vector3 yVector = rectD - rectA;
            float height = yVector.magnitude / 2f;

            linePoint = ProjectPointOnLine(middle, xVector.normalized, point);
            vector = linePoint - point;
            float yDistance = vector.magnitude;

            linePoint = ProjectPointOnLine(middle, yVector.normalized, point);
            vector = linePoint - point;
            float xDistance = vector.magnitude;

            if ((xDistance <= width) && (yDistance <= height))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if line segment made up of pointA1 and pointA2 is crossing line segment made up of
        /// pointB1 and pointB2. The two lines are assumed to be in the same plane.
        /// </summary>
        /// <param name="pointA1"></param>
        /// <param name="pointA2"></param>
        /// <param name="pointB1"></param>
        /// <param name="pointB2"></param>
        /// <returns></returns>
        public static bool AreLineSegmentsCrossing(Vector3 pointA1, Vector3 pointA2, Vector3 pointB1, Vector3 pointB2)
        {
            Vector3 closestPointA;
            Vector3 closestPointB;
            int sideA;
            int sideB;

            Vector3 lineVecA = pointA2 - pointA1;
            Vector3 lineVecB = pointB2 - pointB1;

            bool valid = ClosestPointsOnTwoLines(out closestPointA, out closestPointB, pointA1, lineVecA.normalized, pointB1, lineVecB.normalized);

            //lines are not parallel
            if (valid)
            {
                sideA = PointOnWhichSideOfLineSegment(pointA1, pointA2, closestPointA);
                sideB = PointOnWhichSideOfLineSegment(pointB1, pointB2, closestPointB);

                if ((sideA == 0) && (sideB == 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //lines are parallel
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deterimines if a point is inside a circle
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static bool IsPointInCircle(Vector2 point, Vector2 center, float radius)
        {
            return (Math.Pow((point.x - center.x), 2) + Math.Pow((point.y - center.y), 2) <= Math.Pow(radius, 2));
        }

        /// <summary>
        /// Deterimines if a point is inside a arc
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="point"></param>
        /// <param name="startAngle"></param>
        /// <param name="endAngle"></param>
        /// <returns></returns>
        public static bool IsPointInArc(Vector2 point, Vector2 center, float radius, float startAngle, float endAngle)
        {
            if (!IsPointInCircle(point, center, radius))
                return false;

            float normalizedStartAngle = NormalizeAngle(startAngle);
            float normalizedEndAngle = NormalizeAngle(endAngle);
            float pointAngle = (float)Math.Atan2(point.y - center.y, point.x - center.x) * Mathf.Rad2Deg;
            float normalizedPointAngle = NormalizeAngle(pointAngle);

            if (normalizedStartAngle < normalizedEndAngle)
            {
                return normalizedStartAngle <= normalizedPointAngle && normalizedPointAngle <= normalizedEndAngle;
            }
            else
            {
                return !(normalizedEndAngle <= normalizedPointAngle && normalizedPointAngle <= normalizedStartAngle);
            }
        }

        /// <summary>
        /// Normalizes an angle so that it is between 0 and 360
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float NormalizeAngle(float angle)
        {
            angle = (angle % 360);
            if (angle < 0)
            {
                angle = 360 + angle;
            }
            return angle;
        }
    }

}