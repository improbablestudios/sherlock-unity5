﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    public abstract class TextStyleController<T> : TextStyleController where T : Component
    {
        [SerializeField]
        [Tooltip("The component who's text style will be controlled")]
        protected T m_Target;

        public new T Target
        {
            get
            {
                if (m_Target == null)
                {
                    m_Target = GetComponent<T>();
                }
                return m_Target as T;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Target == null)
            {
                m_Target = GetComponent<T>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Target == null)
            {
                m_Target = GetComponent<T>();
            }
        }

        protected sealed override Type GetControlledComponentType()
        {
            return typeof(T);
        }

        protected sealed override Component GetTarget()
        {
            return m_Target;
        }
    }

    public abstract class TextStyleController : PersistentBehaviour
    {
        [Tooltip("Determines whether the textStyle will be automatically set on awake")]
        [SerializeField]
        protected bool m_ApplyTextStyleOnAwake = true;

        [Tooltip("Determines whether the component will use the font size of the text style")]
        [SerializeField]
        protected bool m_UseFontSize = true;

        public bool ApplyTextStyleOnAwake
        {
            get
            {
                return m_ApplyTextStyleOnAwake;
            }
            set
            {
                m_ApplyTextStyleOnAwake = value;
            }
        }

        public bool UseFontSize
        {
            get
            {
                return m_UseFontSize;
            }
            set
            {
                m_UseFontSize = value;
            }
        }

        public Component Target
        {
            get
            {
                return GetTarget();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            InitializeTextStyles();
        }

        protected override void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
            {
                if (m_ApplyTextStyleOnAwake)
                {
                    ApplyTextStylePalette();
                }
            }
        }

        protected abstract Type GetControlledComponentType();

        protected abstract Component GetTarget();

        public abstract TextStyle[] GetTextStyles();

        public abstract void SetTextStyles(TextStyle[] textStyles);

        public abstract FontData[] GetFontDatas();

        public abstract void SetFontDatas(FontData[] textStyles);

        public string[] GetTextStyleNames()
        {
            return GetTextStyles().Select(x => (x != null) ? x.name : null).ToArray();
        }

        public void InitializeTextStyles()
        {
            FontData[] fontDatas = GetFontDatas();
            TextStyle[] textStyle = new TextStyle[fontDatas.Length];
            for (int i = 0; i < fontDatas.Length; i++)
            {
                FontData fontData = fontDatas[i];
                TextStyle matchingTextStyle = TextStylePaletteManager.Instance.GetActiveTextStyle(fontData);
                if (matchingTextStyle != null)
                {
                    textStyle[i] = matchingTextStyle;
                }
                else
                {
                    textStyle[i] = null;
                }
            }
            SetTextStyles(textStyle);
        }
        
        public void ApplyTextStylePalette()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(Target, "Apply TextStyle");
            }
#endif
            FontData[] currentFontDatas = GetFontDatas();
            TextStyle[] matchingTextStyles = TextStylePaletteManager.Instance.GetActiveTextStyles(GetTextStyleNames());
            FontData[] matchingFontDatas = new FontData[currentFontDatas.Length];
            for (int i = 0; i < matchingFontDatas.Length; i++)
            {
                FontData currentFontData = currentFontDatas[i];
                TextStyle matchingTextStyle = matchingTextStyles[i];
                if (matchingTextStyle != null)
                {
                    FontData newFontData = matchingTextStyle.FontData;

                    if (!m_UseFontSize)
                    {
                        newFontData.fontSize = currentFontData.fontSize;
                    }

                    matchingFontDatas[i] = newFontData;
                }
                else
                {
                    matchingFontDatas[i] = currentFontData;
                }
            }

            SetFontDatas(matchingFontDatas);

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(Target, "Apply TextStyle");
                UnityEditor.EditorUtility.SetDirty(Target);
            }
#endif
        }

        public static Type[] GetControllerTypes()
        {
            return typeof(TextStyleController).FindAllInstantiableDerivedTypes().ToArray();
        }

        public static Type[] GetControlledTypes()
        {
            return GetTextStyleControllerTypeDictionary().Keys.ToArray();
        }

        public static Dictionary<Type, Type> GetTextStyleControllerTypeDictionary()
        {
            Dictionary<Type, Type> types = new Dictionary<Type, Type>();
            Type[] textStyleControllerComponentTypes = GetControllerTypes();
            foreach (Type controllerType in textStyleControllerComponentTypes)
            {
                Type controlledType = GetControlledType(controllerType);
                types[controlledType] = controllerType;
            }
            return types;
        }

        public static Type GetControllerType(Type controlledType)
        {
            foreach (Type controllerType in GetControllerTypes())
            {
                if (controlledType == GetControlledType(controllerType))
                {
                    return controllerType;
                }
            }

            return null;
        }
        public static Type GetControlledType(Type controllerType)
        {
            Type baseGenericType = controllerType.GetBaseGenericType();
            if (baseGenericType != null)
            {
                Type[] genericArguments = baseGenericType.GetGenericArguments();
                if (genericArguments != null && genericArguments.Length > 0)
                {
                    return genericArguments[0];
                }
            }

            return null;
        }
    }

}