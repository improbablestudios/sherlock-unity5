﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    [RequireComponent(typeof(Text))]
    [AddComponentMenu("UI/Helper/UI Text Style Controller")]
    public class UITextStyleController : TextStyleController<Text>
    {
        [Tooltip("The textStyle swatch to use for this graphic")]
        [SerializeField]
        protected TextStyle m_TextStyle;
        
        public TextStyle TextStyle
        {
            get
            {
                return m_TextStyle;
            }
            set
            {
                m_TextStyle = value;
            }
        }
        
        public override TextStyle[] GetTextStyles()
        {
            return new TextStyle[] { m_TextStyle };
        }

        public override void SetTextStyles(TextStyle[] textStyleSwatches)
        {
            if (textStyleSwatches.Length > 0)
            {
                m_TextStyle = textStyleSwatches[0];
            }
        }

        public override FontData[] GetFontDatas()
        {
            return new FontData[] { Target.GetFontData() };
        }

        public override void SetFontDatas(FontData[] fontStyles)
        {
            if (fontStyles.Length > 0)
            {
                FontData newFontData = fontStyles[0];

                if (!m_UseFontSize)
                {
                    newFontData.fontSize = Target.fontSize;
                }

                Target.SetFontData(newFontData);
            }
        }
    }

}