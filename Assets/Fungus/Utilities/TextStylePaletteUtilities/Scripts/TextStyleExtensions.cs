﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    public static class TextStyleExtensions
    {
        public static FontData GetFontData(this Text text)
        {
            FontData fontData = new FontData();

            fontData.alignByGeometry = text.alignByGeometry;
            fontData.alignment = text.alignment;
            fontData.bestFit = text.resizeTextForBestFit;
            fontData.font = text.font;
            fontData.fontSize = text.fontSize;
            fontData.fontStyle = text.fontStyle;
            fontData.horizontalOverflow = text.horizontalOverflow;
            fontData.lineSpacing = text.lineSpacing;
            fontData.maxSize = text.resizeTextMaxSize;
            fontData.minSize = text.resizeTextMinSize;
            fontData.richText = text.supportRichText;
            fontData.verticalOverflow = text.verticalOverflow;

            return fontData;
        }

        public static void SetFontData(this Text text, FontData fontData)
        {
            text.alignByGeometry = fontData.alignByGeometry;
            text.alignment = fontData.alignment;
            text.resizeTextForBestFit = fontData.bestFit;
            text.font = fontData.font;
            text.fontSize = fontData.fontSize;
            text.fontStyle = fontData.fontStyle;
            text.horizontalOverflow = fontData.horizontalOverflow;
            text.lineSpacing = fontData.lineSpacing;
            text.resizeTextMaxSize = fontData.maxSize;
            text.resizeTextMinSize = fontData.minSize;
            text.supportRichText = fontData.richText;
            text.verticalOverflow = fontData.verticalOverflow;
        }

        public static bool Compare(this FontData fontData1, FontData fontData2)
        {
            return fontData1.alignByGeometry == fontData2.alignByGeometry
                || fontData1.alignment == fontData2.alignment
                || fontData1.bestFit == fontData2.bestFit
                || fontData1.font == fontData2.font
                || fontData1.fontSize == fontData2.fontSize
                || fontData1.fontStyle == fontData2.fontStyle
                || fontData1.horizontalOverflow == fontData2.horizontalOverflow
                || fontData1.lineSpacing == fontData2.lineSpacing
                || fontData1.maxSize == fontData2.maxSize
                || fontData1.minSize == fontData2.minSize
                || fontData1.richText == fontData2.richText
                || fontData1.verticalOverflow == fontData2.verticalOverflow;
        }
    }

}