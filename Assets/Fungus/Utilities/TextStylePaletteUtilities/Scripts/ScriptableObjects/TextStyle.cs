﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.TextStylePaletteUtilities
{
    
    public class TextStyle : IdentifiableScriptableObject, INestedAsset
    {
        public const string DEFAULT_TEXT_STYLE_NAME = "TextStyle";

        [SerializeField]
        protected FontData m_FontData = FontData.defaultFontData;

        public FontData FontData
        {
            get
            {
                return m_FontData;
            }
            set
            {
                m_FontData = value;
            }
        }

        public override GUIStyle GetStyle()
        {
#if UNITY_EDITOR
            if (m_FontData != null)
            {
                GUIStyle style = new GUIStyle("PR Label");

                style.font = m_FontData.font;
                style.fontStyle = m_FontData.fontStyle;

                return style;
            }
            return null;
#else
            return null;
#endif
        }
    }

}