﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    [CreateAssetMenu]
    public class TextStylePalette : IdentifiableScriptableObject
    {
        [SerializeField]
        [Tooltip("The different textStyles in this textStyle palette")]
        protected List<TextStyle> m_TextStyles = new List<TextStyle>();
        
        public TextStyle[] TextStyles
        {
            get
            {
                return m_TextStyles.ToArray();
            }
            set
            {
                m_TextStyles = value.ToList();
            }
        }
        
        public virtual string GetUniqueTextStyleSwatchName(TextStyle textStyleSwatch, string newName)
        {
            string[] names = m_TextStyles.Where(x => x != null).Select(x => x.name).ToArray();
            string defaultName = TextStyle.DEFAULT_TEXT_STYLE_NAME;
            int ignoreIndex = m_TextStyles.IndexOf(textStyleSwatch);

            return names.GetUniqueName(newName, defaultName, ignoreIndex);
        }
    }

}