﻿using System;
using System.Linq;
using UnityEngine;
using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using UnityEngine.UI;

namespace ImprobableStudios.TextStylePaletteUtilities
{
    
    public class TextStylePaletteManager : SettingsScriptableObject<TextStylePaletteManager>
    {
        [Tooltip("The name of the current text style palette to use. (The text style names in the active text style palette must match the text style names in the default text style palette)")]
        [SerializeField]
        protected TextStylePalette m_ActiveTextStylePalette;

        [Tooltip("The possible text style palettes")]
        [SerializeField]
        protected List<TextStylePalette> m_TextStylePalettes = new List<TextStylePalette>();

        public TextStylePalette ActiveTextStylePalette
        {
            get
            {
                return m_ActiveTextStylePalette;
            }
            set
            {
                SetActiveTextStylePalette(value);
            }
        }

        public TextStylePalette[] TextStylePalettes
        {
            get
            {
                return m_TextStylePalettes.ToArray();
            }
            set
            {
                m_TextStylePalettes = value.ToList();
            }
        }

        public void SetActiveTextStylePalette(TextStylePalette textStylePalette)
        {
            m_ActiveTextStylePalette = textStylePalette;
            if (!m_TextStylePalettes.Contains(textStylePalette))
            {
                m_TextStylePalettes.Add(textStylePalette);
            }
        }

        public void SetActiveTextStylePalette(string name)
        {
            m_ActiveTextStylePalette = GetTextStylePalette(name);
        }

        public TextStylePalette GetTextStylePalette(string name)
        {
            if (m_ActiveTextStylePalette != null)
            {
                if (m_ActiveTextStylePalette.name == name)
                {
                    return m_ActiveTextStylePalette;
                }
            }
            return m_TextStylePalettes.FirstOrDefault(x => x != null && x.name == name);
        }

        public TextStyle[] GetActiveTextStyles(string[] textStyleNames)
        {
            return textStyleNames.Select(x => GetActiveTextStyle(x)).ToArray();
        }

        public TextStyle GetActiveTextStyle(string textStyleName)
        {
            if (textStyleName != null)
            {
                TextStylePalette activeTextStylePalette = m_ActiveTextStylePalette;
                if (activeTextStylePalette != null)
                {
                    foreach (TextStyle activeTextStyle in activeTextStylePalette.TextStyles)
                    {
                        if (activeTextStyle.name == textStyleName)
                        {
                            return activeTextStyle;
                        }
                    }
                    Debug.LogError(string.Format("TextStyle with name ({0}) was not found in TextStylePalette ({1}) in TextStylePaletteManager ({2})", textStyleName, activeTextStylePalette, name));
                }
            }
            return null;
        }
        
        public TextStyle GetActiveTextStyle(FontData fontData)
        {
            TextStylePalette activeTextStylePalette = m_ActiveTextStylePalette;
            if (activeTextStylePalette != null)
            {
                foreach (TextStyle activeTextStyle in activeTextStylePalette.TextStyles)
                {
                    if (fontData.Compare(activeTextStyle.FontData))
                    {
                        return activeTextStyle;
                    }
                }
            }
            return null;
        }

        public TextStyle GetActiveTextStyle(TextStyle textStyle)
        {
            if (textStyle != null)
            {
                TextStylePalette activeTextStylePalette = m_ActiveTextStylePalette;
                if (activeTextStylePalette != null)
                {
                    foreach (TextStyle activeTextStyleSwatch in activeTextStylePalette.TextStyles)
                    {
                        if (activeTextStyleSwatch.name == textStyle.name)
                        {
                            return activeTextStyleSwatch;
                        }
                    }
                }
                return textStyle;
            }
            return null;
        }

        public FontData GetActiveFontData(TextStyle textStyleSwatch)
        {
            if (textStyleSwatch != null)
            {
                TextStyle activeTextStyleSwatch = GetActiveTextStyle(textStyleSwatch);
                if (activeTextStyleSwatch != null)
                {
                    return activeTextStyleSwatch.FontData;
                }
                return textStyleSwatch.FontData;
            }
            return FontData.defaultFontData;
        }

        public FontData GetActiveFontData(string textStyleName)
        {
            if (textStyleName != null)
            {
                TextStyle activeTextStyleSwatch = GetActiveTextStyle(textStyleName);
                if (activeTextStyleSwatch != null)
                {
                    return activeTextStyleSwatch.FontData;
                }
            }
            return FontData.defaultFontData;
        }
    }

}