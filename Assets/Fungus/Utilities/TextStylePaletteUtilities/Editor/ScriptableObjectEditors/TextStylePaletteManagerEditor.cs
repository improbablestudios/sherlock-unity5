#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(TextStylePaletteManager), true)]
    public class TextStylePaletteManagerEditor : SettingsScriptableObjectEditor
    {
        private static readonly Dictionary<Type, Type> k_textStyleControllerDictionary = TextStyleController.GetTextStyleControllerTypeDictionary();

        protected static string TextStylePaletteFolderEditorPreferenceKey
        {
            get
            {
                return typeof(TextStylePaletteManager).AssemblyQualifiedName + "+TextStylePaletteFolder";
            }
        }
        
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            TextStylePaletteManager t = target as TextStylePaletteManager;

            GUILayout.Space(20f);

            using (new EditorGUILayout.VerticalScope(ProcessorBoxStyle))
            {
                string errorTitle = nameof(TextStylePaletteManager) + " Error";
                string errorOk = "OK";

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(TextStyleController).Name + "s",
                    "Attaching " + typeof(TextStyleController).Name + "s",
                    errorTitle,
                    "No Text components found",
                    errorOk,
                    AttachTextStyleControllersToGameObject);

                EditorGUILayout.Separator();

                DrawProcessGameObjectsGUI(
                    "Sync " + typeof(TextStyleController).Name + "s with Active Text Style Palette",
                    "Syncing " + typeof(TextStyleController).Name + "s",
                    errorTitle,
                    "No " + typeof(TextStyleController).Name + " components found",
                    errorOk,
                    SyncTextStyleControllerTextStylesInGameObject);

                EditorGUILayout.Separator();

                DrawProcessGameObjectsGUI(
                    "Apply Active TextStyle Palette To " + typeof(TextStyleController).Name + "Texts",
                    "Applying Active TextStyle Palette To " + typeof(TextStyleController).Name + "Texts",
                    errorTitle,
                    "No Text components found with " + typeof(TextStyleController).Name + " component attached",
                    errorOk,
                    ApplyTextStylePaletteToTextStyleControllersInGameObject);
            }
        }
        
        public bool AttachTextStyleControllersToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;
            foreach (Type controlledType in k_textStyleControllerDictionary.Keys)
            {
                Component[] components =
                   (processChildren) ?
                   gameObject.GetComponentsInChildren(controlledType, true) :
                   gameObject.GetComponents(controlledType);

                foreach (Component component in components)
                {
                    Type controllerType = k_textStyleControllerDictionary[controlledType];
                    if (component.gameObject.GetComponent(controllerType) == null)
                    {
                        Undo.AddComponent(component.gameObject, controllerType);
                    }
                    found = true;
                }
            }
            return found;
        }
        
        public bool SyncTextStyleControllerTextStylesInGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type type = typeof(TextStyleController);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (TextStyleController textStyleController in components)
            {
                Undo.RecordObject(textStyleController, "Sync Controllers");
                textStyleController.InitializeTextStyles();
                found = true;
            }

            return found;
        }
        
        public bool ApplyTextStylePaletteToTextStyleControllersInGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type type = typeof(TextStyleController);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (TextStyleController textStyleController in components)
            {
                Undo.RecordObject(textStyleController, "Apply Palette To Controllers");
                textStyleController.ApplyTextStylePalette();
                found = true;
            }

            return found;
        }
    }

}
#endif