﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    [CustomPropertyDrawer(typeof(TextStyle), true)]
    public class TextStyleFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SearchPopupGUI.ObjectSearchPopupField<TextStyle>(position, property, label, null, true, SearchPopupWindowFlags.HideCreateNewOption);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SearchPopupGUI.ObjectSearchPopupField<TextStyle>(position, property, label, null, true, SearchPopupWindowFlags.HideCreateNewOption);
        }
    }

}
#endif