﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    public class TextStylePaletteManagerSettingsProvider : BaseAssetSettingsProvider<TextStylePaletteManager>
    {
        public TextStylePaletteManagerSettingsProvider() : base("Project/Managers/Text Style Palette Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new TextStylePaletteManagerSettingsProvider();
        }
    }

    public class TextStylePaletteManagerSettingsBuilder : BaseAssetSettingsBuilder<TextStylePaletteManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif