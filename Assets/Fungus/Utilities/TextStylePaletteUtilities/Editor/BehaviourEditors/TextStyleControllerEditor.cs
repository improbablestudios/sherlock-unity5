#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.TextStylePaletteUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(TextStyleController), true)]
    public class TextStyleControllerEditor : PersistentBehaviourEditor
    {
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            if (GUILayout.Button("Apply Text Style"))
            {
                foreach (TextStyleController t in targets)
                {
                    t.ApplyTextStylePalette();
                }
            }
        }
    }

}
#endif