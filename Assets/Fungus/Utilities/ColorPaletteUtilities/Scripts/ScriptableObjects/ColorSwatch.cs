﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.ColorPaletteUtilities
{
    
    public class ColorSwatch : IdentifiableScriptableObject, INestedAsset
    {
        public const string DEFAULT_COLOR_PRESET_NAME = "Color";

        public static readonly Color DEFAULT_COLOR_PRESET_COLOR = Color.white;

        [SerializeField]
        protected Color m_Color = DEFAULT_COLOR_PRESET_COLOR;

        public Color Color
        {
            get
            {
                return m_Color;
            }
            set
            {
                m_Color = value;
            }
        }

        public override Texture2D GetIcon()
        {
#if UNITY_EDITOR
            Color color = m_Color;

            int size = 1;

            Texture2D texture = new Texture2D(size, size, TextureFormat.ARGB32, false);
            Color[] colorArray = texture.GetPixels();
            for (int i = 0; i < colorArray.Length; i++)
            {
                colorArray[i] = color;
            }
            texture.SetPixels(colorArray);
            texture.Apply();

            return texture;
#else
            return null;
#endif
        }
    }

}