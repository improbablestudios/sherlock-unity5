﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.ColorPaletteUtilities
{

    [CreateAssetMenu]
    public class ColorPalette : IdentifiableScriptableObject
    {
        [SerializeField]
        [Tooltip("The different colors in this color palette")]
        protected List<ColorSwatch> m_Swatches = new List<ColorSwatch>();
        
        public ColorSwatch[] Swatches
        {
            get
            {
                return m_Swatches.ToArray();
            }
            set
            {
                m_Swatches = value.ToList();
            }
        }
        
        public virtual string GetUniqueColorSwatchName(ColorSwatch colorSwatch, string newName)
        {
            string[] names = m_Swatches.Where(x => x != null).Select(x => x.name).ToArray();
            string defaultName = ColorSwatch.DEFAULT_COLOR_PRESET_NAME;
            int ignoreIndex = m_Swatches.IndexOf(colorSwatch);

            return names.GetUniqueName(newName, defaultName, ignoreIndex);
        }
    }

}