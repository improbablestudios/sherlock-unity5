﻿using System;
using System.Linq;
using UnityEngine;
using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;

namespace ImprobableStudios.ColorPaletteUtilities
{
    
    public class ColorPaletteManager : SettingsScriptableObject<ColorPaletteManager>
    {
        [Tooltip("The name of the current color palette to use. (The color names in the active color palette must match the color names in the default color palette)")]
        [SerializeField]
        protected ColorPalette m_ActiveColorPalette;

        [Tooltip("The possible color palettes")]
        [SerializeField]
        protected List<ColorPalette> m_ColorPalettes = new List<ColorPalette>();

        [Tooltip("The threshold to use when determining if a color is close enough to match a color in a color palette")]
        [SerializeField]
        [Range(0, 1)]
        protected float m_ColorSimilarityThreshold = 0.99f;
        
        public ColorPalette ActiveColorPalette
        {
            get
            {
                return m_ActiveColorPalette;
            }
            set
            {
                SetActiveColorPalette(value);
            }
        }

        public ColorPalette[] ColorPalettes
        {
            get
            {
                return m_ColorPalettes.ToArray();
            }
            set
            {
                m_ColorPalettes = value.ToList();
            }
        }

        public void SetActiveColorPalette(ColorPalette colorPalette)
        {
            m_ActiveColorPalette = colorPalette;
            if (!m_ColorPalettes.Contains(colorPalette))
            {
                m_ColorPalettes.Add(colorPalette);
            }
        }

        public void SetActiveColorPalette(string name)
        {
            m_ActiveColorPalette = GetColorPalette(name);
        }

        public ColorPalette GetColorPalette(string name)
        {
            if (m_ActiveColorPalette != null)
            {
                if (m_ActiveColorPalette.name == name)
                {
                    return m_ActiveColorPalette;
                }
            }
            return m_ColorPalettes.FirstOrDefault(x => x != null && x.name == name);
        }

        public ColorSwatch[] GetActiveColorSwatches(string[] colorNames)
        {
            return colorNames.Select(x => GetActiveColorSwatch(x)).ToArray();
        }

        public ColorSwatch GetActiveColorSwatch(string colorName)
        {
            if (colorName != null)
            {
                ColorPalette activeColorPalette = m_ActiveColorPalette;
                if (activeColorPalette != null)
                {
                    foreach (ColorSwatch activeColorSwatch in activeColorPalette.Swatches)
                    {
                        if (activeColorSwatch.name == colorName)
                        {
                            return activeColorSwatch;
                        }
                    }
                    Debug.LogError(string.Format("Color with name ({0}) was not found in ColorPalette ({1}) in ColorPaletteManager ({2})", colorName, activeColorPalette, name));
                }
            }
            return null;
        }
        
        public ColorSwatch GetActiveColorSwatch(Color color)
        {
            ColorPalette activeColorPalette = m_ActiveColorPalette;
            if (activeColorPalette != null)
            {
                foreach (ColorSwatch activeColorSwatch in activeColorPalette.Swatches)
                {
                    if (AreColorsSimilar(color, activeColorSwatch.Color, m_ColorSimilarityThreshold))
                    {
                        return activeColorSwatch;
                    }
                }
            }
            return null;
        }

        public ColorSwatch GetActiveColorSwatch(ColorSwatch colorSwatch)
        {
            if (colorSwatch != null)
            {
                ColorPalette activeColorPalette = m_ActiveColorPalette;
                if (activeColorPalette != null)
                {
                    foreach (ColorSwatch activeColorSwatch in activeColorPalette.Swatches)
                    {
                        if (activeColorSwatch.name == colorSwatch.name)
                        {
                            return activeColorSwatch;
                        }
                    }
                }
                return colorSwatch;
            }
            return null;
        }

        public Color GetActiveColor(ColorSwatch colorSwatch)
        {
            if (colorSwatch != null)
            {
                ColorSwatch activeColorSwatch = GetActiveColorSwatch(colorSwatch);
                if (activeColorSwatch != null)
                {
                    return activeColorSwatch.Color;
                }
                return colorSwatch.Color;
            }
            return Color.white;
        }

        public Color GetActiveColor(string colorName)
        {
            if (colorName != null)
            {
                ColorSwatch activeColorSwatch = GetActiveColorSwatch(colorName);
                if (activeColorSwatch != null)
                {
                    return activeColorSwatch.Color;
                }
            }
            return Color.white;
        }

        public bool AreColorsSimilar(Color color1, Color color2, float threshold)
        {

            var rDistance = Math.Abs(color1.r - color2.r);
            var gDistance = Math.Abs(color1.g - color2.g);
            var bDistance = Math.Abs(color1.b - color2.b);

            if (rDistance < (1 - threshold) &&
                gDistance < (1 - threshold) &&
                bDistance < (1 - threshold))
            {
                return true;
            }

            return false;
        }
    }

}