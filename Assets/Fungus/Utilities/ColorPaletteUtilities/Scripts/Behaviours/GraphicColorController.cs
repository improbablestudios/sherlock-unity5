﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace ImprobableStudios.ColorPaletteUtilities
{

    [RequireComponent(typeof(Graphic))]
    [AddComponentMenu("UI/Helper/Graphic Color Controller")]
    public class GraphicColorController : ColorController<Graphic>
    {
        [Tooltip("Determines whether the graphic color will be multiplied by the color palette color instead of replaced")]
        [SerializeField]
        protected bool m_Multiply;

        [Tooltip("The color swatch to use for this graphic")]
        [SerializeField]
        protected ColorSwatch m_ColorSwatch;
        
        public bool Multiply
        {
            get
            {
                return m_Multiply;
            }
            set
            {
                m_Multiply = value;
            }
        }

        public ColorSwatch ColorSwatch
        {
            get
            {
                return m_ColorSwatch;
            }
            set
            {
                m_ColorSwatch = value;
            }
        }
        
        public override ColorSwatch[] GetColorSwatches()
        {
            return new ColorSwatch[] { m_ColorSwatch };
        }

        public override void SetColorSwatches(ColorSwatch[] colorSwatches)
        {
            if (colorSwatches.Length > 0)
            {
                m_ColorSwatch = colorSwatches[0];
            }
        }

        public override Color[] GetColors()
        {
            return new Color[] { Target.color };
        }

        public override void SetColors(Color[] colors)
        {
            if (colors.Length > 0)
            {
                Color newColor = colors[0];

                if (m_Multiply)
                {
                    if (!m_UseAlpha)
                    {
                        newColor.a = Target.canvasRenderer.GetColor().a;
                    }

                    Target.canvasRenderer.SetColor(newColor);
                }
                else
                {
                    if (!m_UseAlpha)
                    {
                        newColor.a = Target.color.a;
                    }

                    Target.color = newColor;

#if UNITY_EDITOR
                    if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
                    {
                        Target.canvasRenderer.SetColor(UnityEngine.Color.white);
                    }
#endif
                }
            }
        }
    }

}