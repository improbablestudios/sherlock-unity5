﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.ColorPaletteUtilities
{

    public abstract class ColorController<T> : ColorController where T : Component
    {
        [SerializeField]
        [Tooltip("The component who's color will be controlled")]
        protected T m_Target;
        
        public new T Target
        {
            get
            {
                if (m_Target == null)
                {
                    m_Target = GetComponent<T>();
                }
                return m_Target as T;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_Target == null)
            {
                m_Target = GetComponent<T>();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            if (m_Target == null)
            {
                m_Target = GetComponent<T>();
            }
        }

        protected sealed override Type GetControlledComponentType()
        {
            return typeof(T);
        }

        protected sealed override Component GetTarget()
        {
            return m_Target;
        }
    }

    public abstract class ColorController : PersistentBehaviour
    {
        [Tooltip("Determines whether the color will be automatically set on awake")]
        [SerializeField]
        protected bool m_ApplyColorOnAwake = true;

        [Tooltip("Determines whether the component will use the alpha value of the color swatch")]
        [SerializeField]
        protected bool m_UseAlpha = true;
        
        public bool ApplyColorOnAwake
        {
            get
            {
                return m_ApplyColorOnAwake;
            }
            set
            {
                m_ApplyColorOnAwake = value;
            }
        }

        public bool UseAlpha
        {
            get
            {
                return m_UseAlpha;
            }
            set
            {
                m_UseAlpha = value;
            }
        }

        public Component Target
        {
            get
            {
                return GetTarget();
            }
        }

        protected override void Reset()
        {
            base.Reset();

            InitializeColorSwatches();
        }

        protected override void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
            {
                if (m_ApplyColorOnAwake)
                {
                    ApplyColorPalette();
                }
            }
        }

        protected abstract Type GetControlledComponentType();

        protected abstract Component GetTarget();

        public abstract ColorSwatch[] GetColorSwatches();

        public abstract void SetColorSwatches(ColorSwatch[] colorSwatches);

        public abstract Color[] GetColors();

        public abstract void SetColors(Color[] colors);

        public string[] GetColorNames()
        {
            return GetColorSwatches().Select(x => (x != null) ? x.name : null).ToArray();
        }

        public void InitializeColorSwatches()
        {
            Color[] colors = GetColors();
            ColorSwatch[] colorSwatch = new ColorSwatch[colors.Length];
            for (int i = 0; i < colors.Length; i++)
            {
                Color color = colors[i];
                ColorSwatch matchingColorSwatch = ColorPaletteManager.Instance.GetActiveColorSwatch(color);
                if (matchingColorSwatch != null)
                {
                    colorSwatch[i] = matchingColorSwatch;
                }
                else
                {
                    colorSwatch[i] = null;
                }
            }
            SetColorSwatches(colorSwatch);
        }
        
        public void ApplyColorPalette()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(Target, "Apply Color");
            }
#endif
            Color[] currentColors = GetColors();
            ColorSwatch[] matchingColorSwatches = ColorPaletteManager.Instance.GetActiveColorSwatches(GetColorNames());
            Color[] matchingColors = new Color[currentColors.Length];
            for (int i = 0; i < matchingColors.Length; i++)
            {
                Color currentColor = currentColors[i];
                ColorSwatch matchingColorSwatch = matchingColorSwatches[i];
                if (matchingColorSwatch != null)
                {
                    Color newColor = matchingColorSwatch.Color;

                    if (!m_UseAlpha)
                    {
                        newColor.a = currentColor.a;
                    }

                    matchingColors[i] = newColor;
                }
                else
                {
                    matchingColors[i] = currentColor;
                }
            }

            SetColors(matchingColors);

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(Target, "Apply Color");
                UnityEditor.EditorUtility.SetDirty(Target);
            }
#endif
        }

        public static Type[] GetControllerTypes()
        {
            return typeof(ColorController).FindAllInstantiableDerivedTypes().ToArray();
        }

        public static Type[] GetControlledTypes()
        {
            return GetColorControllerTypeDictionary().Keys.ToArray();
        }

        public static Dictionary<Type, Type> GetColorControllerTypeDictionary()
        {
            Dictionary<Type, Type> types = new Dictionary<Type, Type>();
            Type[] colorControllerComponentTypes = GetControllerTypes();
            foreach (Type controllerType in colorControllerComponentTypes)
            {
                Type controlledType = GetControlledType(controllerType);
                types[controlledType] = controllerType;
            }
            return types;
        }

        public static Type GetControllerType(Type controlledType)
        {
            foreach (Type controllerType in GetControllerTypes())
            {
                if (controlledType == GetControlledType(controllerType))
                {
                    return controllerType;
                }
            }

            return null;
        }
        public static Type GetControlledType(Type controllerType)
        {
            Type baseGenericType = controllerType.GetBaseGenericType();
            if (baseGenericType != null)
            {
                Type[] genericArguments = baseGenericType.GetGenericArguments();
                if (genericArguments != null && genericArguments.Length > 0)
                {
                    return genericArguments[0];
                }
            }

            return null;
        }
    }

}