﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace ImprobableStudios.ColorPaletteUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("UI/Helper/Selectable Transition Color Controller")]
    public class SelectableTransitionColorController : ColorController<Selectable>
    {
        [Tooltip("The color swatch to use for the selectable's normal transition")]
        [SerializeField]
        protected ColorSwatch m_NormalColorSwatch;

        [Tooltip("The color swatch to use for the selectable's highlighted transition")]
        [SerializeField]
        protected ColorSwatch m_HighlightedColorSwatch;

        [Tooltip("The color swatch to use for the selectable's pressed transition")]
        [SerializeField]
        protected ColorSwatch m_PressedColorSwatch;

        [Tooltip("The color swatch to use for the selectable's disabled transition")]
        [SerializeField]
        protected ColorSwatch m_DisabledColorSwatch;
        
        public ColorSwatch NormalColorSwatch
        {
            get
            {
                return m_NormalColorSwatch;
            }
            set
            {
                m_NormalColorSwatch = value;
            }
        }

        public ColorSwatch HighlightedColorSwatch
        {
            get
            {
                return m_HighlightedColorSwatch;
            }
            set
            {
                m_HighlightedColorSwatch = value;
            }
        }

        public ColorSwatch PressedColorSwatch
        {
            get
            {
                return m_PressedColorSwatch;
            }
            set
            {
                m_PressedColorSwatch = value;
            }
        }

        public ColorSwatch DisabledColorSwatch
        {
            get
            {
                return m_DisabledColorSwatch;
            }
            set
            {
                m_DisabledColorSwatch = value;
            }
        }
        
        public override ColorSwatch[] GetColorSwatches()
        {
            return new ColorSwatch[] 
            {
                m_NormalColorSwatch,
                m_HighlightedColorSwatch,
                m_PressedColorSwatch,
                m_DisabledColorSwatch
            };
        }

        public override void SetColorSwatches(ColorSwatch[] colorSwatches)
        {
            if (colorSwatches.Length > 0)
            {
                m_NormalColorSwatch = colorSwatches[0];
            }
            if (colorSwatches.Length > 1)
            {
                m_HighlightedColorSwatch = colorSwatches[1];
            }
            if (colorSwatches.Length > 2)
            {
                m_PressedColorSwatch = colorSwatches[2];
            }
            if (colorSwatches.Length > 3)
            {
                m_DisabledColorSwatch = colorSwatches[3];
            }
        }

        public override Color[] GetColors()
        {
            return new Color[] 
            {
                Target.colors.normalColor,
                Target.colors.highlightedColor,
                Target.colors.pressedColor,
                Target.colors.disabledColor,
            };
        }

        public override void SetColors(Color[] colors)
        {
            ColorBlock colorBlock = Target.colors;
            if (colors.Length > 0)
            {
                colorBlock.normalColor = colors[0];
            }
            if (colors.Length > 1)
            {
                colorBlock.highlightedColor = colors[1];
            }
            if (colors.Length > 2)
            {
                colorBlock.pressedColor = colors[2];
            }
            if (colors.Length > 3)
            {
                colorBlock.disabledColor = colors[3];
            }
            Target.colors = colorBlock;
        }
    }

}