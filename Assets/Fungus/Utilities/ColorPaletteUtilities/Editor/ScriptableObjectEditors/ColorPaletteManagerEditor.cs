#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.FileUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ColorPaletteUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(ColorPaletteManager), true)]
    public class ColorPaletteManagerEditor : SettingsScriptableObjectEditor
    {
        private static readonly Dictionary<Type, Type> k_colorControllerDictionary = ColorController.GetColorControllerTypeDictionary();

        public static Type ColorPresetLibraryType
        {
            get
            {
                return Type.GetType("UnityEditor.ColorPresetLibrary,UnityEditor");
            }
        }

        public static Type ColorPresetType
        {
            get
            {
                return Type.GetType("UnityEditor.ColorPresetLibrary+ColorPreset,UnityEditor");
            }
        }
        
        public static string ColorPaletteFolderEditorPreference
        {
            get
            {
                return EditorPrefs.GetString(ColorPaletteFolderEditorPreferenceKey, "Assets");
            }
            set
            {
                EditorPrefs.SetString(ColorPaletteFolderEditorPreferenceKey, value);
            }
        }

        public static string ColorPresetLibraryFolderEditorPreference
        {
            get
            {
                return EditorPrefs.GetString(ColorPresetLibraryFolderEditorPreferenceKey, "Assets");
            }
            set
            {
                EditorPrefs.SetString(ColorPresetLibraryFolderEditorPreferenceKey, value);
            }
        }

        protected static string ColorPaletteFolderEditorPreferenceKey
        {
            get
            {
                return typeof(ColorPaletteManager).AssemblyQualifiedName + "+ColorPaletteFolder";
            }
        }

        protected static string ColorPresetLibraryFolderEditorPreferenceKey
        {
            get
            {
                return typeof(ColorPaletteManager).AssemblyQualifiedName + "+ColorPresetLibraryFolder";
            }
        }
        
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            ColorPaletteManager t = target as ColorPaletteManager;

            GUILayout.Space(20f);

            using (new EditorGUILayout.VerticalScope(ProcessorBoxStyle))
            {
                string errorTitle = nameof(ColorPaletteManager) + " Error";
                string errorOk = "OK";

                DrawProcessGameObjectsGUI(
                    "Attach " + typeof(ColorController).Name + "s",
                    "Attaching " + typeof(ColorController).Name + "s",
                    errorTitle,
                    "No Graphic components found",
                    errorOk,
                    AttachColorControllersToGameObject);

                EditorGUILayout.Separator();

                DrawProcessGameObjectsGUI(
                    "Sync " + typeof(ColorController).Name + "s with Active Color Palette",
                    "Syncing " + typeof(ColorController).Name + "s",
                    errorTitle,
                    "No " + typeof(ColorController).Name + " components found",
                    errorOk,
                    SyncColorControllerColorSwatchesInGameObject);

                EditorGUILayout.Separator();

                DrawProcessGameObjectsGUI(
                    "Apply Active Color Palette To " + typeof(ColorController).Name + "Graphics",
                    "Applying Active Color Palette To " + typeof(ColorController).Name + "Graphics",
                    errorTitle,
                    "No Graphic components found with " + typeof(ColorController).Name + " component attached",
                    errorOk,
                    ApplyColorPaletteToColorControllersInGameObject);

                EditorGUILayout.Separator();

                using (new EditorGUILayout.VerticalScope())
                {
                    EditorGUILayout.LabelField("Color Preset Libraries", ProcessorLabelStyle);

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("Import"))
                        {
                            if (!ImportColorPresetLibraries())
                            {
                                EditorUtility.DisplayDialog(
                                    errorTitle, 
                                    "No color preset libraries found in Color Preset Libraries Folder",
                                    errorOk);
                            }
                        }

                        if (GUILayout.Button("Export"))
                        {
                            if (!ExportColorPresetLibraries())
                            {
                                EditorUtility.DisplayDialog(
                                    errorTitle, 
                                    "No color palettes found in Color Palettes Folder",
                                    errorOk);
                            }
                        }
                    }
                    ColorPresetLibraryFolderEditorPreference = PathEditorGUI.OpenAssetFolderPathField(ColorPresetLibraryFolderEditorPreference, new GUIContent("Color Preset Library Folder"), "Select Preset Library Folder", "");
                    ColorPaletteFolderEditorPreference = PathEditorGUI.OpenAssetFolderPathField(ColorPaletteFolderEditorPreference, new GUIContent("Color Palette Folder"), "Select Color Palette Folder", "");
                }
            }
        }
        
        public bool AttachColorControllersToGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;
            foreach (Type controlledType in k_colorControllerDictionary.Keys)
            {
                Component[] components =
                   (processChildren) ?
                   gameObject.GetComponentsInChildren(controlledType, true) :
                   gameObject.GetComponents(controlledType);

                foreach (Component component in components)
                {
                    Type controllerType = k_colorControllerDictionary[controlledType];
                    if (component.gameObject.GetComponent(controllerType) == null)
                    {
                        Undo.AddComponent(component.gameObject, controllerType);
                    }
                    found = true;
                }
            }
            return found;
        }
        
        public bool SyncColorControllerColorSwatchesInGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type type = typeof(ColorController);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (ColorController colorController in components)
            {
                Undo.RecordObject(colorController, "Sync Color Controller Swatches");
                colorController.InitializeColorSwatches();
                found = true;
            }

            return found;
        }
        
        public bool ApplyColorPaletteToColorControllersInGameObject(GameObject gameObject, bool processChildren)
        {
            bool found = false;

            Type type = typeof(ColorController);

            Component[] components =
               (processChildren) ?
               gameObject.GetComponentsInChildren(type, true) :
               gameObject.GetComponents(type);

            foreach (ColorController colorController in components)
            {
                Undo.RecordObject(colorController, "Apply Color Palette To Color Controllers");
                colorController.ApplyColorPalette();
                found = true;
            }

            return found;
        }

        public bool ImportColorPresetLibraries()
        {
            bool found = false;

            if (!string.IsNullOrEmpty(ColorPaletteFolderEditorPreference))
            {
                ColorPaletteManager t = target as ColorPaletteManager;
                
                string[] colorPresetPaths = AssetDatabase.FindAssets("", new string[] { ColorPresetLibraryFolderEditorPreference }).Select(x => AssetDatabase.GUIDToAssetPath(x)).ToArray();

                List<ColorPalette> colorPalettes = t.ColorPalettes.ToList();
                for (int i = 0; i < colorPresetPaths.Length; i++)
                {
                    string colorPresetPath = colorPresetPaths[i];
                    
                    try
                    {
                        if (colorPresetPath.EndsWith(".colors"))
                        {
                            if (EditorUtility.DisplayCancelableProgressBar("Creating Color Palettes From Color Preset Libraries", string.Format("Processing '{0}'", colorPresetPath), (float)i / colorPresetPaths.Length))
                            {
                                break;
                            }

                            found = true;

                            UnityEngine.Object colorPreset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(colorPresetPath);

                            if (colorPreset != null)
                            {
                                string colorPaletteName = Path.GetFileNameWithoutExtension(colorPresetPath);

                                string path = ColorPaletteFolderEditorPreference + "/" + colorPaletteName + ".asset";

                                bool allowUndo = true;
                                ColorPalette colorPalette = colorPalettes.FirstOrDefault(x => x != null && x.name == colorPaletteName);

                                if (colorPalette == null)
                                {
                                    colorPalette = AssetDatabase.LoadAssetAtPath<ColorPalette>(path);
                                }

                                if (colorPalette == null)
                                {
                                    allowUndo = false;
                                    colorPalette = ScriptableObject.CreateInstance<ColorPalette>();
                                    colorPalette.name = colorPaletteName;
                                    AssetDatabase.CreateAsset(colorPalette, path);
                                    if (!colorPalettes.Contains(colorPalette))
                                    {
                                        colorPalettes.Add(colorPalette);
                                    }
                                }

                                SetupColorPalette(colorPalette, colorPreset, allowUndo);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }

                EditorUtility.ClearProgressBar();

                if (t.ColorPalettes.Length == 0 && colorPalettes.Count > 0 && t.ActiveColorPalette == null)
                {
                    t.ActiveColorPalette = colorPalettes[0];
                }
                t.ColorPalettes = colorPalettes.ToArray();
            }

            return found;
        }
        
        public bool ExportColorPresetLibraries()
        {
            bool found = false;

            if (!string.IsNullOrEmpty(ColorPresetLibraryFolderEditorPreference))
            {
                ColorPaletteManager t = target as ColorPaletteManager;

                string[] colorPalettePaths = AssetDatabase.FindAssets("t:" + typeof(ColorPalette).Name, new string[] { ColorPaletteFolderEditorPreference }).Select(x => AssetDatabase.GUIDToAssetPath(x)).ToArray();

                for (int i = 0; i < colorPalettePaths.Length; i++)
                {
                    string colorPalettePath = colorPalettePaths[i];

                    if (EditorUtility.DisplayCancelableProgressBar("Creating Color Preset Libraries From Color Palettes", string.Format("Processing '{0}'", colorPalettePath), (float)i / colorPalettePaths.Length))
                    {
                        break;
                    }

                    try
                    {
                        found = true;

                        ColorPalette colorPalette = AssetDatabase.LoadAssetAtPath<ColorPalette>(colorPalettePath);

                        if (colorPalette != null)
                        {
                            string colorPaletteName = colorPalette.name;

                            string path = ColorPresetLibraryFolderEditorPreference + "/" + colorPaletteName + ".colors";

                            bool allowUndo = true;
                            UnityEngine.Object colorPresetLibrary = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);

                            if (colorPresetLibrary == null)
                            {
                                allowUndo = false;
                                colorPresetLibrary = ScriptableObject.CreateInstance(ColorPresetLibraryType.Name);
                                colorPresetLibrary.name = colorPaletteName;
                                AssetDatabase.CreateAsset(colorPresetLibrary, path);
                            }

                            SetupColorPresetLibrary(colorPresetLibrary, colorPalette, allowUndo);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }

                EditorUtility.ClearProgressBar();
            }

            return found;
        }

        private void SetupColorPresetLibrary(UnityEngine.Object colorPresetLibrary, ColorPalette colorPalette, bool allowUndo)
        {
            if (allowUndo)
            {
                Undo.RecordObject(colorPresetLibrary, "Update Color Library");
            }

            IList presetList = Activator.CreateInstance(typeof(List<>).MakeGenericType(ColorPresetType)) as IList;
            for (int i = 0; i < colorPalette.Swatches.Length; i++)
            {
                ColorSwatch colorSwatch = colorPalette.Swatches[i];
                if (colorSwatch != null)
                {
                    string colorSwatchName = colorSwatch.name;
                    Color colorSwatchColor = colorSwatch.Color;
                    presetList.Add(Activator.CreateInstance(ColorPresetType, colorSwatchColor, colorSwatchName));
                }
            }
            ColorPresetLibraryType.GetField("m_Presets", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(colorPresetLibrary, presetList);

            if (!allowUndo)
            {
                EditorUtility.SetDirty(colorPresetLibrary);
                AssetDatabase.SaveAssets();
            }
            EditorGUIUtility.PingObject(colorPresetLibrary);
        }

        private void SetupColorPalette(ColorPalette colorPalette, UnityEngine.Object colorPresetLibrary, bool allowUndo)
        {
            if (allowUndo)
            {
                Undo.RecordObject(colorPalette, "Update Color Palette");
            }

            List<ColorSwatch> colorSwatches = colorPalette.Swatches.ToList();
            object colorPalettePresets = ColorPresetLibraryType.GetField("m_Presets", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(colorPresetLibrary);
            IList presetList = colorPalettePresets as IList;
            for (int i = 0; i < presetList.Count; i++)
            {
                object colorPreset = presetList[i];
                string colorSwatchName = ColorPresetType.GetField("m_Name", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(colorPreset) as string;
                Color colorSwatchColor = (Color)ColorPresetType.GetField("m_Color", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(colorPreset);
                ColorSwatch colorSwatch = colorSwatches.FirstOrDefault(x => x.name == colorSwatchName);
                if (colorSwatch == null)
                {
                    colorSwatch = ScriptableObject.CreateInstance<ColorSwatch>();
                    colorSwatches.Add(colorSwatch);
                    AssetDatabase.AddObjectToAsset(colorSwatch, AssetDatabase.GetAssetPath(colorPalette));
                }
                if (colorSwatch.name != colorSwatchName)
                {
                    if (allowUndo)
                    {
                        Undo.RecordObject(colorSwatch, "Update Color Swatch");
                    }
                    colorSwatch.name = colorSwatchName;
                }
                if (colorSwatch.Color != colorSwatchColor)
                {
                    if (allowUndo)
                    {
                        Undo.RecordObject(colorSwatch, "Update Color Swatch");
                    }
                    colorSwatch.Color = colorSwatchColor;
                }
            }
            colorPalette.Swatches = colorSwatches.ToArray();

            if (!allowUndo)
            {
                EditorUtility.SetDirty(colorPalette);
                AssetDatabase.SaveAssets();
            }
            EditorGUIUtility.PingObject(colorPalette);
        }
    }

}
#endif