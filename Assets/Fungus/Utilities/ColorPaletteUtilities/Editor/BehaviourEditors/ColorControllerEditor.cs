#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ColorPaletteUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(ColorController), true)]
    public class ColorControllerEditor : PersistentBehaviourEditor
    {
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            if (GUILayout.Button("Apply Color"))
            {
                foreach (ColorController t in targets)
                {
                    t.ApplyColorPalette();
                }
            }
        }
    }

}
#endif