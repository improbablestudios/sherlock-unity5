#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.ColorPaletteUtilities
{
    
    [CustomPropertyDrawer(typeof(ColorSwatch), true)]
    public class ColorSwatchFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SearchPopupGUI.ObjectSearchPopupField<ColorSwatch>(position, property, label, null, true, SearchPopupWindowFlags.HideCreateNewOption);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SearchPopupGUI.ObjectSearchPopupField<ColorSwatch>(position, property, label, null, true, SearchPopupWindowFlags.HideCreateNewOption);
        }
    }

}
#endif