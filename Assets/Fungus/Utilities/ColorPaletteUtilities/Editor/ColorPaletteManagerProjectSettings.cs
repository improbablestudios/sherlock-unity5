﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.ColorPaletteUtilities
{

    public class ColorPaletteManagerSettingsProvider : BaseAssetSettingsProvider<ColorPaletteManager>
    {
        public ColorPaletteManagerSettingsProvider() : base("Project/Managers/Color Palette Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new ColorPaletteManagerSettingsProvider();
        }
    }

    public class ColorPaletteManagerSettingsBuilder : BaseAssetSettingsBuilder<ColorPaletteManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif