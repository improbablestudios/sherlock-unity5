﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace ImprobableStudios.ProjectSettingsUtilities
{

    public abstract class BaseAssetSettingsProvider<T> : AssetSettingsProvider
        where T : ScriptableObject
    {
        protected static readonly GUIContent k_activeSettings = new GUIContent(string.Format("Active {0}", ObjectNames.NicifyVariableName(typeof(T).Name)), string.Format("The {0} that will be used by this project and included into any builds.", ObjectNames.NicifyVariableName(typeof(T).Name)));
        protected static readonly GUIContent k_noSettingsMsg = new GUIContent(string.Format("You have no active {0} set. Would you like to create one?", ObjectNames.NicifyVariableName(typeof(T).Name)));
        
        protected string m_searchContext;
        protected VisualElement m_rootElement;

        public BaseAssetSettingsProvider(string settingsWindowPath) : base(settingsWindowPath, GetActiveSettings)
        {
        }

        public static UnityEngine.Object GetActiveSettings()
        {
            return AssetSettingsConfig.GetActiveSettings<T>();
        }
        
        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            m_searchContext = searchContext;
            m_rootElement = rootElement;
            base.OnActivate(searchContext, rootElement);
        }
        
        public override void OnGUI(string searchContext)
        {
            ScriptableObject activeSettings = AssetSettingsConfig.GetActiveSettings<T>();
            using (GetSettingsWindowGUIScope())
            {
                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorGUI.BeginChangeCheck();
                    activeSettings = EditorGUILayout.ObjectField(k_activeSettings, activeSettings, typeof(T), false) as ScriptableObject;
                    if (EditorGUI.EndChangeCheck())
                    {
                        AssetSettingsConfig.SetActiveSettings(activeSettings);
                        base.OnActivate(m_searchContext, m_rootElement);
                    }

                    if (activeSettings == null)
                    {
                        EditorGUILayout.HelpBox(k_noSettingsMsg.text, MessageType.Info, true);
                        if (GUILayout.Button("Create", GUILayout.Width(100)))
                        {
                            T created = CreateSettings();
                            if (created != null)
                            {
                                AssetSettingsConfig.SetActiveSettings(created);
                                base.OnActivate(m_searchContext, m_rootElement);
                            }
                        }
                    }
                }
            }

            if (activeSettings != null)
            {
                base.OnGUI(searchContext);
            }
        }

        public static T CreateSettings()
        {
            string path = EditorUtility.SaveFilePanelInProject(
                string.Format("Save {0}", ObjectNames.NicifyVariableName(typeof(T).Name)), 
                typeof(T).Name, 
                "asset",
                string.Format("Please enter a filename to save the project's {0} to.", ObjectNames.NicifyVariableName(typeof(T).Name)));

            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            T settings = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(settings, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = settings;
            return settings;
        }

        public static Type GetSettingsWindowType()
        {
            return typeof(Editor).Assembly.GetType("UnityEditor.SettingsWindow");
        }

        public static EditorWindow GetSettingsWindow()
        {
            return EditorWindow.GetWindow(GetSettingsWindowType());
        }

        public static GUI.Scope GetSettingsWindowGUIScope()
        {
            Type settingsWindowGUIScopeType = typeof(Editor).Assembly.GetType("UnityEditor.SettingsWindow+GUIScope");
            return Activator.CreateInstance(settingsWindowGUIScopeType) as GUI.Scope;
        }
    }
}
#endif