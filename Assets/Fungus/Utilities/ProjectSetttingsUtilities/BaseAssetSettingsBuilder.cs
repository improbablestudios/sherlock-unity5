﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ImprobableStudios.ProjectSettingsUtilities
{

    public class BaseAssetSettingsBuilder<T>
        where T : ScriptableObject
    {
        // Required property for IPreprocessBuildWithReport and IPostprocessBuildWithReport
        public int callbackOrder
        {
            get
            {
                return 0;
            }
        }

        public BaseAssetSettingsBuilder()
        {
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            T settings = AssetSettingsConfig.GetActiveSettings<T>() as T;

            List<UnityEngine.Object> preloadedAssets = PlayerSettings.GetPreloadedAssets().ToList();
            List<T> otherSettings = preloadedAssets.OfType<T>().ToList();
            if (settings != null && otherSettings.Contains(settings))
            {
                otherSettings.Remove(settings);
            }
            if (otherSettings.Count > 0)
            {
                preloadedAssets = preloadedAssets.Except(otherSettings).ToList();
            }
            if (settings != null && !preloadedAssets.Contains(settings))
            {
                preloadedAssets.Add(settings);
            }

            PlayerSettings.SetPreloadedAssets(preloadedAssets.ToArray());
        }

        public void OnPostprocessBuild(BuildReport report)
        {
        }
    }

}
#endif