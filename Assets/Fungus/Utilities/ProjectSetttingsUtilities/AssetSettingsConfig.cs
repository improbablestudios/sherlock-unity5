﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ProjectSettingsUtilities
{

    public static class AssetSettingsConfig
    {
        public static event Action<Type, ScriptableObject> OnActiveSettingsChanged;

        public static T GetActiveSettings<T>() where T : ScriptableObject
        {
            return GetActiveSettings(typeof(T)) as T;
        }

        public static ScriptableObject GetActiveSettings(Type type)
        {
            ScriptableObject settings;
            EditorBuildSettings.TryGetConfigObject(GetConfigName(type), out settings);
            return settings;
        }

        public static void SetActiveSettings<T>(T settings) where T : ScriptableObject
        {
            SetActiveSettings(typeof(T), settings);
        }

        public static void SetActiveSettings(Type type, ScriptableObject settings)
        {
            if (settings == null)
            {
                EditorBuildSettings.RemoveConfigObject(GetConfigName(type));
            }
            else
            {
                if (!type.IsAssignableFrom(settings.GetType()))
                {
                    throw new ArgumentException(nameof(settings) + " must be of type " + type);
                }
                EditorBuildSettings.AddConfigObject(GetConfigName(type), settings, true);
                OnActiveSettingsChanged?.Invoke(type, settings);
            }
        }

        public static string GetConfigName<T>() where T : ScriptableObject
        {
            return GetConfigName(typeof(T));
        }

        public static string GetConfigName(Type type)
        {
            return type.Name + "." + "Settings";
        }
    }

}
#endif