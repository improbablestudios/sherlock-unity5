#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;

namespace ImprobableStudios.SerializedPropertyUtilities
{
    
    public class SerializedPropertyCache
    {
        protected SerializedObject m_cachedSerializedObject;
        protected SerializedProperty[] m_cachedSerializedProperties;
        protected Func<SerializedProperty, int> m_getOrder;

        public SerializedPropertyCache(SerializedObject serializedObject, Func<SerializedProperty, int> getOrder = null)
        {
            m_cachedSerializedObject = serializedObject;
            m_cachedSerializedProperties = serializedObject.GetVisibleProperties(false);
            m_getOrder = getOrder;
            if (m_getOrder != null)
            {
                m_cachedSerializedProperties = m_cachedSerializedProperties.OrderBy(x => m_getOrder(x)).ToArray();
            }
        }

        public SerializedProperty[] this[SerializedObject serializedObject]
        {
            get
            {
                if (!IsValid(serializedObject))
                {
                    m_cachedSerializedObject = serializedObject;
                    m_cachedSerializedProperties = serializedObject.GetVisibleProperties(false);
                    if (m_getOrder != null)
                    {
                        m_cachedSerializedProperties = m_cachedSerializedProperties.OrderBy(x => m_getOrder(x)).ToArray();
                    }
                }
                return m_cachedSerializedProperties;
            }
        }

        protected bool IsValid(SerializedObject serializedObject)
        {
            return (m_cachedSerializedObject == serializedObject && serializedObject.targetObject != null);
        }
    }

}
#endif