﻿#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SerializedPropertyUtilities
{

    public abstract class PropertyAttributeDrawer : PropertyDrawer
    {
        public override sealed void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (GuiSupportsSerializedPropertyType(position, property, label, attribute.GetType(), GetSupportedPropertyTypes()))
            {
                OnPropertyGUI(position, property, label);
            }
        }

        public abstract void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label);

        public abstract Type[] GetSupportedPropertyTypes();

        public static bool GuiSupportsSerializedPropertyType(Rect position, SerializedProperty property, GUIContent label, Type guiType, params Type[] supportedTypes)
        {
            string[] supportedTypeNames = supportedTypes.Select(x => x.GetNiceTypeName()).ToArray();
            return GuiSupportsSerializedPropertyType(position, property, label, guiType, supportedTypeNames, supportedTypes);
        }

        public static bool GuiSupportsSerializedPropertyType(Rect position, SerializedProperty property, GUIContent label, Type guiType, string supportedTypeName, Type supportedType)
        {
            return GuiSupportsSerializedPropertyType(position, property, label, guiType, new string[] { supportedTypeName }, new Type[] { supportedType });
        }

        public static bool GuiSupportsSerializedPropertyType(Rect position, SerializedProperty property, GUIContent label, Type guiType, string[] supportedTypeNames, Type[] supportedTypes)
        {
            Type propertyType = property.GetPropertyType();
            if (supportedTypes != null && !supportedTypes.Any(x => x.IsAssignableFrom(propertyType)))
            {
                EditorGUI.LabelField(position, label.text, string.Format("Use {0} with {1}.", guiType.Name, string.Join(" or ", supportedTypeNames)));
                return false;
            }
            return true;
        }
    }

}
#endif