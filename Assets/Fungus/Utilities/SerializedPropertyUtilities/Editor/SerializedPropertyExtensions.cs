#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SerializedPropertyUtilities
{

    public static class SerializedPropertyExtensions
    {
        public static Type ScriptAttrubuteUtilityType
        {
            get
            {
                return Type.GetType("UnityEditor.ScriptAttributeUtility,UnityEditor");
            }
        }

        public static Type PropertyHandlerType
        {
            get
            {
                return Type.GetType("UnityEditor.PropertyHandler,UnityEditor");
            }
        }

        public static object GetPropertyHandler(this SerializedProperty property)
        {
            return ScriptAttrubuteUtilityType.GetMethod("GetHandler", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { property });
        }

        public static float GetPropertyHeight(this SerializedProperty property, GUIContent label, bool includeChildren)
        {
            object propertyHandler = GetPropertyHandler(property);
            float height = (float)PropertyHandlerType.GetMethod("GetHeight", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(propertyHandler, new object[] { property, label, includeChildren });
            return height;
        }

        public static bool OnPropertyHandlerGUI(this SerializedProperty property, Rect position, GUIContent label, bool includeChildren)
        {
            Rect visibleArea = new Rect(0.0f, 0.0f, position.width, float.MaxValue);
            return OnPropertyHandlerGUI(property, position, label, includeChildren, visibleArea);
        }

        public static bool OnPropertyHandlerGUI(this SerializedProperty property, Rect position, GUIContent label, bool includeChildren, Rect visibleArea)
        {
            object propertyHandler = GetPropertyHandler(property);
            bool foldout = (bool)PropertyHandlerType.GetMethod("OnGUI", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(propertyHandler, new object[] { position, property, label, includeChildren, visibleArea });
            return foldout;
        }

        public static bool HasPropertyDrawer(this SerializedProperty property)
        {
            object propertyHandler = GetPropertyHandler(property);
            bool hasPropertyDrawer = (bool)PropertyHandlerType.GetProperty("hasPropertyDrawer", BindingFlags.Public | BindingFlags.Instance).GetValue(propertyHandler);
            return hasPropertyDrawer;
        }
        
        public static PropertyDrawer GetPropertyDrawer(this SerializedProperty property)
        {
            object propertyHandler = GetPropertyHandler(property);
            PropertyDrawer propertyDrawer = PropertyHandlerType.GetProperty("propertyDrawer", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(propertyHandler) as PropertyDrawer;
            return propertyDrawer;
        }

        public static void SetPropertyDrawerFieldInfo(this PropertyDrawer drawer, FieldInfo fieldInfo)
        {
            drawer.GetType().GetField("m_FieldInfo", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(drawer, fieldInfo);
        }

        public static Type GetDrawerTypeForType(Type type)
        {
            return ScriptAttrubuteUtilityType.GetMethod("GetDrawerTypeForType", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { type }) as Type;
        }

        public static PropertyDrawer CreatePropertyDrawer(this FieldInfo propertyField)
        {
            Type propertyType = propertyField.FieldType;

            PropertyDrawer propertyDrawer = null;

            Type drawerTypeForType = GetDrawerTypeForType(propertyType);
            if (drawerTypeForType != null && typeof(PropertyDrawer).IsAssignableFrom(drawerTypeForType))
            {
                if (propertyType == null || !propertyType.IsArray)
                {
                    propertyDrawer = (PropertyDrawer)Activator.CreateInstance(drawerTypeForType);
                    propertyDrawer.SetPropertyDrawerFieldInfo(propertyField);
                }
            }

            return propertyDrawer;
        }
        
        public static T GetAttribute<T>(this SerializedProperty property, bool inherit) where T : Attribute
        {
            FieldInfo field = GetPropertyField(property);
            if (field != null)
            {
                foreach (object obj in field.GetCustomAttributes(inherit))
                {
                    T attribute = obj as T;
                    if (attribute != null)
                    {
                        return attribute;
                    }
                }
            }
            return null;
        }

        public static string GetPropertyTooltip(this SerializedProperty property)
        {
            FieldInfo fieldInfo = GetPropertyField(property);
            if (fieldInfo != null)
            {
                foreach (object obj in GetPropertyField(property).GetCustomAttributes(false))
                {
                    TooltipAttribute tooltipAttribute = obj as TooltipAttribute;
                    if (tooltipAttribute != null)
                    {
                        return tooltipAttribute.tooltip;
                    }
                }
            }
            return "";
        }
        
        public static Type GetPropertyFieldType(this SerializedProperty property, FieldInfo fieldInfo)
        {
            Type type = fieldInfo.FieldType;
            if (typeof(IList).IsAssignableFrom(type) && property.propertyType != SerializedPropertyType.Generic)
            {
                if (type.IsArray)
                {
                    type = type.GetElementType();
                }
                else
                {
                    type = type.GetGenericArguments()[0];
                }
            }
            return type;
        }

        public static SerializedProperty GetParentProperty(this SerializedProperty property)
        {
            int index = property.propertyPath.LastIndexOf(".");
            if (index > -1)
            {
                string parentPropertyPath = property.propertyPath.Remove(index);
                return property.serializedObject.FindProperty(parentPropertyPath);
            }
            return null;
        }

        public static FieldInfo GetPropertyField(this SerializedProperty property)
        {
            string path = property.propertyPath;
            object rootObj = property.serializedObject.targetObject;

            MemberInfo member = null;
            object[] parameters = null;
            int memberDepth = -1;
            if (rootObj != null)
            {
                Type type = rootObj.GetType();
                foreach (string name in path.Split('.'))
                {
                    memberDepth++;

                    if (name == "Array")
                    {
                        continue;
                    }
                    
                    if (name.StartsWith("data[", StringComparison.Ordinal))
                    {
                        member = type.GetItemProperty();
                        int index = GetArrayIndexFromName(name);
                        parameters = new object[] { index };
                        type = type.GetItemType();
                    }
                    else
                    {
                        member = type.GetUnitySerializedField(name);
                        parameters = null;
                        if (member != null)
                        {
                            type = member.GetMemberType();
                        }
                    }

                    if (member == null || type == null)
                    {
                        break;
                    }
                }

                if (member != null)
                {
                    return member as FieldInfo;
                }
            }

            return null;
        }

        public static Type GetPropertyType(this SerializedProperty property)
        {
            Type type = ConvertSerializedPropertyTypeToType(property.propertyType);
            if (type == null)
            {
                type = GetPropertyType(property.propertyPath, property.serializedObject.targetObject);
            }
            return type;
        }

        public static Type ConvertSerializedPropertyTypeToType(this SerializedPropertyType serializedPropertyType)
        {
            if (serializedPropertyType == SerializedPropertyType.Integer)
            {
                return typeof(int);
            }
            else if (serializedPropertyType == SerializedPropertyType.Boolean)
            {
                return typeof(bool);
            }
            else if (serializedPropertyType == SerializedPropertyType.Float)
            {
                return typeof(float);
            }
            else if (serializedPropertyType == SerializedPropertyType.String)
            {
                return typeof(string);
            }
            else if (serializedPropertyType == SerializedPropertyType.Color)
            {
                return typeof(Color);
            }
            else if (serializedPropertyType == SerializedPropertyType.ObjectReference)
            {
                return typeof(UnityEngine.Object);
            }
            else if (serializedPropertyType == SerializedPropertyType.LayerMask)
            {
                return typeof(LayerMask);
            }
            else if (serializedPropertyType == SerializedPropertyType.Vector2)
            {
                return typeof(Vector2);
            }
            else if (serializedPropertyType == SerializedPropertyType.Vector3)
            {
                return typeof(Vector3);
            }
            else if (serializedPropertyType == SerializedPropertyType.Vector4)
            {
                return typeof(Vector4);
            }
            else if (serializedPropertyType == SerializedPropertyType.Rect)
            {
                return typeof(Rect);
            }
            else if (serializedPropertyType == SerializedPropertyType.Character)
            {
                return typeof(char);
            }
            else if (serializedPropertyType == SerializedPropertyType.AnimationCurve)
            {
                return typeof(AnimationCurve);
            }
            else if (serializedPropertyType == SerializedPropertyType.Bounds)
            {
                return typeof(Bounds);
            }
            else if (serializedPropertyType == SerializedPropertyType.Gradient)
            {
                return typeof(Gradient);
            }
            else if (serializedPropertyType == SerializedPropertyType.Quaternion)
            {
                return typeof(Quaternion);
            }
            else if (serializedPropertyType == SerializedPropertyType.Vector2Int)
            {
                return typeof(Vector2Int);
            }
            else if (serializedPropertyType == SerializedPropertyType.Vector3Int)
            {
                return typeof(Vector3Int);
            }
            else if (serializedPropertyType == SerializedPropertyType.RectInt)
            {
                return typeof(RectInt);
            }
            else if (serializedPropertyType == SerializedPropertyType.BoundsInt)
            {
                return typeof(BoundsInt);
            }
            else
            {
                return null;
            }
        }

        public static SerializedPropertyType ConvertTypeToSerializedPropertyType(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (typeof(int).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Integer;
            }
            else if (typeof(bool).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Boolean;
            }
            else if (typeof(float).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Float;
            }
            else if (typeof(string).IsAssignableFrom(type))
            {
                return SerializedPropertyType.String;
            }
            else if (typeof(Color).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Color;
            }
            else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                return SerializedPropertyType.ObjectReference;
            }
            else if (typeof(LayerMask).IsAssignableFrom(type))
            {
                return SerializedPropertyType.LayerMask;
            }
            else if (type.IsEnum)
            {
                return SerializedPropertyType.Enum;
            }
            else if (typeof(Vector2).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Vector2;
            }
            else if (typeof(Vector3).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Vector3;
            }
            else if (typeof(Vector4).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Vector4;
            }
            else if (typeof(Rect).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Rect;
            }
            else if (typeof(char).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Character;
            }
            else if (typeof(AnimationCurve).IsAssignableFrom(type))
            {
                return SerializedPropertyType.AnimationCurve;
            }
            else if (typeof(Bounds).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Bounds;
            }
            else if (typeof(Gradient).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Gradient;
            }
            else if (typeof(Quaternion).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Quaternion;
            }
            else if (typeof(Vector2Int).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Vector2Int;
            }
            else if (typeof(Vector3Int).IsAssignableFrom(type))
            {
                return SerializedPropertyType.Vector3Int;
            }
            else if (typeof(RectInt).IsAssignableFrom(type))
            {
                return SerializedPropertyType.RectInt;
            }
            else if (typeof(BoundsInt).IsAssignableFrom(type))
            {
                return SerializedPropertyType.BoundsInt;
            }
            else
            {
                return SerializedPropertyType.Generic;
            }
        }

        public static object GetPropertyValue(this SerializedProperty property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    return property.intValue;
                case SerializedPropertyType.Boolean:
                    return property.boolValue;
                case SerializedPropertyType.Float:
                    return property.floatValue;
                case SerializedPropertyType.String:
                    return property.stringValue;
                case SerializedPropertyType.Color:
                    return property.colorValue;
                case SerializedPropertyType.ObjectReference:
                    return property.objectReferenceValue;
                case SerializedPropertyType.LayerMask:
                    return property.intValue;
                case SerializedPropertyType.Enum:
                    return Enum.ToObject(property.GetPropertyType(), property.intValue);
                case SerializedPropertyType.Vector2:
                    return property.vector2Value;
                case SerializedPropertyType.Vector3:
                    return property.vector3Value;
                case SerializedPropertyType.Vector4:
                    return property.vector4Value;
                case SerializedPropertyType.Rect:
                    return property.rectValue;
                case SerializedPropertyType.ArraySize:
                    return property.arraySize;
                case SerializedPropertyType.Character:
                    return Convert.ChangeType(property.intValue, typeof(char));
                case SerializedPropertyType.AnimationCurve:
                    return property.animationCurveValue;
                case SerializedPropertyType.Bounds:
                    return property.boundsValue;
                case SerializedPropertyType.Gradient:
                    return property.GetGradientValue();
                case SerializedPropertyType.Quaternion:
                    return property.quaternionValue;
                case SerializedPropertyType.Vector2Int:
                    return property.vector2IntValue;
                case SerializedPropertyType.Vector3Int:
                    return property.vector3IntValue;
                case SerializedPropertyType.RectInt:
                    return property.rectIntValue;
                case SerializedPropertyType.BoundsInt:
                    return property.boundsIntValue;
                default:
                    SerializedMember SerializedMember = GetPropertySerializedMember(property);
                    if (SerializedMember != null)
                    {
                        return SerializedMember.Value;
                    }
                    break;
            }

            return null;
        }

        public static void SetPropertyValue(this SerializedProperty property, object value)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    property.intValue = (int)value;
                    break;
                case SerializedPropertyType.Boolean:
                    property.boolValue = (bool)value;
                    break;
                case SerializedPropertyType.Float:
                    property.floatValue = (float)value;
                    break;
                case SerializedPropertyType.String:
                    property.stringValue = (string)value;
                    break;
                case SerializedPropertyType.Color:
                    property.colorValue = (Color)value;
                    break;
                case SerializedPropertyType.ObjectReference:
                    property.objectReferenceValue = (UnityEngine.Object)value;
                    break;
                case SerializedPropertyType.LayerMask:
                    property.intValue = (LayerMask)value;
                    break;
                case SerializedPropertyType.Enum:
                    property.intValue = (int)value;
                    break;
                case SerializedPropertyType.Vector2:
                    property.vector2Value = (Vector2)value;
                    break;
                case SerializedPropertyType.Vector3:
                    property.vector3Value = (Vector3)value;
                    break;
                case SerializedPropertyType.Vector4:
                    property.vector4Value = (Vector4)value;
                    break;
                case SerializedPropertyType.Rect:
                    property.rectValue = (Rect)value;
                    break;
                case SerializedPropertyType.ArraySize:
                    property.arraySize = (int)value;
                    break;
                case SerializedPropertyType.Character:
                    property.intValue = (int)value;
                    break;
                case SerializedPropertyType.AnimationCurve:
                    property.animationCurveValue = (AnimationCurve)value;
                    break;
                case SerializedPropertyType.Bounds:
                    property.boundsValue = (Bounds)value;
                    break;
                case SerializedPropertyType.Gradient:
                    property.SetGradientValue((Gradient)value);
                    break;
                case SerializedPropertyType.Quaternion:
                    property.quaternionValue = (Quaternion)value;
                    break;
                case SerializedPropertyType.Vector2Int:
                    property.vector2IntValue = (Vector2Int)value;
                    break;
                case SerializedPropertyType.Vector3Int:
                    property.vector3IntValue = (Vector3Int)value;
                    break;
                case SerializedPropertyType.RectInt:
                    property.rectIntValue = (RectInt)value;
                    break;
                case SerializedPropertyType.BoundsInt:
                    property.boundsIntValue = (BoundsInt)value;
                    break;
                default:
                    SerializedMember SerializedMember = GetPropertySerializedMember(property);
                    if (SerializedMember != null)
                    {
                        SerializedMember.Value = value;
                    }
                    break;
            }
        }

        public static void ResetPropertyValue(this SerializedProperty property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    property.intValue = 0;
                    break;
                case SerializedPropertyType.Boolean:
                    property.boolValue = false;
                    break;
                case SerializedPropertyType.Float:
                    property.floatValue = 0f;
                    break;
                case SerializedPropertyType.String:
                    property.stringValue = "";
                    break;
                case SerializedPropertyType.Color:
                    property.colorValue = Color.black;
                    break;
                case SerializedPropertyType.ObjectReference:
                    property.objectReferenceValue = null;
                    break;
                case SerializedPropertyType.LayerMask:
                    property.intValue = 0;
                    break;
                case SerializedPropertyType.Enum:
                    property.enumValueIndex = 0;
                    break;
                case SerializedPropertyType.Vector2:
                    property.vector2Value = default(Vector2);
                    break;
                case SerializedPropertyType.Vector3:
                    property.vector3Value = default(Vector3);
                    break;
                case SerializedPropertyType.Vector4:
                    property.vector4Value = default(Vector4);
                    break;
                case SerializedPropertyType.Rect:
                    property.rectValue = default(Rect);
                    break;
                case SerializedPropertyType.ArraySize:
                    property.arraySize = 0;
                    break;
                case SerializedPropertyType.Character:
                    property.intValue = 0;
                    break;
                case SerializedPropertyType.AnimationCurve:
                    property.animationCurveValue = AnimationCurve.Linear(0f, 0f, 1f, 1f);
                    break;
                case SerializedPropertyType.Bounds:
                    property.boundsValue = default(Bounds);
                    break;
                case SerializedPropertyType.Gradient:
                    property.SetGradientValue(default(Gradient));
                    break;
                case SerializedPropertyType.Quaternion:
                    property.quaternionValue = default(Quaternion);
                    break;
                case SerializedPropertyType.Vector2Int:
                    property.vector2IntValue = default(Vector2Int);
                    break;
                case SerializedPropertyType.Vector3Int:
                    property.vector3IntValue = default(Vector3Int);
                    break;
                case SerializedPropertyType.RectInt:
                    property.rectIntValue = default(RectInt);
                    break;
                case SerializedPropertyType.BoundsInt:
                    property.boundsIntValue = default(BoundsInt);
                    break;
                default:
                    SerializedMember SerializedMember = GetPropertySerializedMember(property);
                    if (SerializedMember != null)
                    {
                        SerializedMember.Value = SerializedMember.ValueType.GetDefaultValue();
                    }
                    break;
            }

            if (property.isArray)
            {
                property.arraySize = 0;
            }
        }

        public static Type GetPropertyType(string path, object rootObj)
        {
            Type type = null;
            if (rootObj != null)
            {
                type = rootObj.GetType();
                foreach (string name in path.Split('.'))
                {
                    if (name == "Array")
                    {
                        continue;
                    }
                    if (name.StartsWith("data[", StringComparison.Ordinal))
                    {
                        type = type.GetItemType();
                    }
                    else
                    {
                        type = type.GetMemberType(name);
                    }
                }
            }
            return type;
        }

        public static SerializedMember GetPropertySerializedMember(this SerializedProperty property)
        {
            string path = property.propertyPath;
            object rootObj = property.serializedObject.targetObject;

            MemberInfo member = null;
            object parentObj = rootObj;
            object[] parameters = null;
            int memberDepth = -1;
            string memberPath = "";
            if (rootObj != null)
            {
                Type type = rootObj.GetType();
                foreach (string name in path.Split('.'))
                {
                    memberDepth++;

                    if (name == "Array")
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(memberPath))
                    {
                        memberPath += ".";
                    }

                    if (member != null)
                    {
                        parentObj = member.GetMemberValue(parentObj, parameters);
                    }

                    if (name.StartsWith("data[", StringComparison.Ordinal))
                    {
                        member = type.GetItemProperty();
                        int index = GetArrayIndexFromName(name);
                        parameters = new object[] { index };
                        type = type.GetItemType();
                        if (member != null)
                        {
                            memberPath += member.Name + "[" + index + "]";
                        }
                    }
                    else
                    {
                        member = type.GetUnitySerializedField(name);
                        parameters = null;
                        if (member != null)
                        {
                            type = member.GetMemberType();
                            memberPath += member.Name;
                        }
                    }

                    if (member == null || parentObj == null || type == null)
                    {
                        break;
                    }
                }

                if (member != null && parentObj != null)
                {
                    return new SerializedMember(member, parentObj, parameters, memberDepth, memberPath, member.Name, property.hasChildren, property.isExpanded);
                }
            }

            return null;
        }
        
        private static int GetArrayIndexFromName(string name)
        {
            int prefixLength = ("data[").Length;
            int startIndex = prefixLength;
            string indexString = name.Substring(startIndex, name.Length - prefixLength - 1);
            int index = -1;
            int.TryParse(indexString, out index);
            return index;
        }

        public static int[] GetArrayIndicesFromPath(string path)
        {
            List<int> indices = new List<int>();
            foreach (string name in path.Split('.'))
            {
                if (name == "Array")
                {
                    continue;
                }
                
                if (name.StartsWith("data[", StringComparison.Ordinal))
                {
                    indices.Add(GetArrayIndexFromName(name));
                }
            }

            return indices.ToArray();
        }

        public static string GetArrayElementString(int index)
        {
            return ".Array.data[" + index + "]";
        }

        public static Gradient GetGradientValue(this SerializedProperty property)
        {
            PropertyInfo gradientPropertyInfo = GetGradientPropertyInfo();

            if (gradientPropertyInfo == null)
            {
                return default(Gradient);
            }

            return gradientPropertyInfo.GetValue(property, null) as Gradient;
        }

        public static void SetGradientValue(this SerializedProperty property, Gradient value)
        {
            PropertyInfo gradientPropertyInfo = GetGradientPropertyInfo();

            if (gradientPropertyInfo == null)
            {
                return;
            }

            gradientPropertyInfo.SetValue(property, value, null);
        }

        private static PropertyInfo GetGradientPropertyInfo()
        {
            return typeof(SerializedProperty).GetProperty("gradientValue", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, typeof(Gradient), new Type[0], null);
        }

        private static PropertyInfo GetInspectorModePropertyInfo()
        {
            return typeof(UnityEditor.SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
        }

        public static InspectorMode GetInspectorMode(this SerializedObject serializedObject)
        {
            PropertyInfo inspectorModeInfo = GetInspectorModePropertyInfo();
            return (InspectorMode)inspectorModeInfo.GetValue(serializedObject);
        }

        public static void SetInspectorMode(this SerializedObject serializedObject, InspectorMode inspectorMode)
        {
            PropertyInfo inspectorModeInfo = GetInspectorModePropertyInfo();
            inspectorModeInfo.SetValue(serializedObject, inspectorMode, null);
        }

        public static string GetMemberPath(this SerializedProperty property)
        {
            return UnityReflectionExtensions.GetMemberPath(property.propertyPath);
        }

        public static SerializedProperty[] GetVisibleProperties(this SerializedObject serializedObject, bool includeChildren)
        {
            List<SerializedProperty> properties = new List<SerializedProperty>();
            SerializedProperty iterator = serializedObject.GetIterator();
            iterator = iterator.Copy();
            bool enterChildren = true;
            while (iterator.NextVisible(enterChildren))
            {
                SerializedProperty childProperty = iterator.Copy();

                enterChildren = (includeChildren && childProperty.hasVisibleChildren);

                properties.Add(childProperty);
            }
            return properties.ToArray();
        }

        public static SerializedProperty[] GetVisibleChildren(this SerializedProperty property, bool includeChildrenOfChildren)
        {
            List<SerializedProperty> properties = new List<SerializedProperty>();
            SerializedProperty iterator = property.Copy();
            SerializedProperty endProperty = iterator.GetEndProperty();
            bool enterChildren = true;
            while (iterator.NextVisible(enterChildren) && !SerializedProperty.EqualContents(iterator, endProperty))
            {
                SerializedProperty childProperty = iterator.Copy();

                enterChildren = (includeChildrenOfChildren && childProperty.hasVisibleChildren);

                properties.Add(childProperty);
            }
            return properties.ToArray();
        }
        
        public static int GetSmallestArraySize(this SerializedProperty arrayProperty)
        {
            int smallestArraySize = int.MaxValue;
            try
            {
                foreach (UnityEngine.Object obj in arrayProperty.serializedObject.targetObjects)
                {
                    SerializedObject serializedObj = new SerializedObject(obj);
                    SerializedProperty individualArrayProperty = serializedObj.FindProperty(arrayProperty.propertyPath);
                    smallestArraySize = Math.Min(smallestArraySize, individualArrayProperty.arraySize);
                }
            }
            catch
            {
                return 0;
            }
            return smallestArraySize;
        }
    }

}
#endif