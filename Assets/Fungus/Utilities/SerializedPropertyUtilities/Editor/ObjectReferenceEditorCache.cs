﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace ImprobableStudios.SerializedPropertyUtilities
{

    public class ObjectReferenceEditorCache
    {
        protected Dictionary<UnityEngine.Object, Editor> m_editorCache = new Dictionary<UnityEngine.Object, Editor>();

        public ObjectReferenceEditorCache()
        {
        }

        public Editor this[UnityEngine.Object key]
        {
            get
            {
                if (!m_editorCache.ContainsKey(key))
                {
                    m_editorCache[key] = CreateEditor(key);
                }
                return m_editorCache[key];
            }
            set
            {
                if (!m_editorCache.ContainsKey(key) || m_editorCache[key] != value)
                {
                    m_editorCache[key] = value;
                }
            }
        }

        public Editor[] Editors
        {
            get
            {
                return m_editorCache.Values.ToArray();
            }
        }

        public bool ContainsKey(UnityEngine.Object obj)
        {
            return (m_editorCache.ContainsKey(obj));
        }

        protected virtual Editor CreateEditor(UnityEngine.Object obj)
        {
            return Editor.CreateEditor(obj);
        }
    }

    public class ObjectReferenceEditorCache<T> : ObjectReferenceEditorCache where T : Editor
    {
        public ObjectReferenceEditorCache() : base()
        {
        }

        public new T this[UnityEngine.Object key]
        {
            get
            {
                if (!m_editorCache.ContainsKey(key))
                {
                    m_editorCache[key] = CreateEditor(key);
                }
                return m_editorCache[key] as T;
            }
            set
            {
                if (!m_editorCache.ContainsKey(key) || m_editorCache[key] != value)
                {
                    m_editorCache[key] = value;
                }
            }
        }

        public new T[] Editors
        {
            get
            {
                return m_editorCache.Values.Cast<T>().ToArray();
            }
        }
    }

}
#endif