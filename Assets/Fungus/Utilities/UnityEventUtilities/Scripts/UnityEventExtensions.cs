﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Events;

namespace ImprobableStudios.UnityEventUtilities
{
    
    public static class UnityEventExtensions
    {
        public static void AddPersistentListener(this UnityEvent unityEvent, UnityAction call)
        {
            UnityEngine.Object target = call.Target as UnityEngine.Object;
            MethodInfo method = call.Method;

            if (method.IsStatic || (target == null) || target.GetInstanceID() == 0)
            {
                unityEvent.AddListener(call);
                return;
            }

            MethodInfo reflectedMethod = typeof(UnityEvent).GetMethod("AddPersistentListener", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(UnityAction) }, null);
            if (reflectedMethod != null)
            {
                reflectedMethod.Invoke(unityEvent, new object[] { call });
            }
            else
            {
                int index = unityEvent.GetPersistentEventCount();

                IList persistentCalls_CallsList = GetPersistentCalls_CallsList(unityEvent);

                Type persistentCallType = persistentCalls_CallsList.GetType().GetGenericArguments()[0];
                object listener = Activator.CreateInstance(persistentCallType);
                persistentCalls_CallsList.Add(listener);

                FieldInfo listener_targetField = persistentCallType.GetField("m_Target", BindingFlags.Instance | BindingFlags.NonPublic);
                FieldInfo listener_methodNameField = persistentCallType.GetField("m_MethodName", BindingFlags.Instance | BindingFlags.NonPublic);
                FieldInfo listener_modeField = persistentCallType.GetField("m_Mode", BindingFlags.Instance | BindingFlags.NonPublic);
                listener_targetField.SetValue(listener, call.Target);
                listener_methodNameField.SetValue(listener, call.Method.Name);
                listener_modeField.SetValue(listener, PersistentListenerMode.EventDefined);

                unityEvent.SetPersistentListenerState(index, UnityEventCallState.RuntimeOnly);
            }
        }

        public static void RemovePersistentListener(this UnityEvent unityEvent, UnityAction call)
        {
            UnityEngine.Object target = call.Target as UnityEngine.Object;
            MethodInfo method = call.Method;

            if (method.IsStatic || (target == null) || target.GetInstanceID() == 0)
            {
                unityEvent.RemoveListener(call);
                return;
            }

            MethodInfo reflectedMethod = typeof(UnityEventBase).GetMethod("RemovePersistentListener", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(UnityEngine.Object), typeof(MethodInfo) }, null);
            if (reflectedMethod != null)
            {
                reflectedMethod.Invoke(unityEvent, new object[] { target, call.Method });
            }
            else
            {
                if (method != null && !method.IsStatic && !(target == null) && target.GetInstanceID() != 0)
                {
                    IList persistentCalls_CallsList = GetPersistentCalls_CallsList(unityEvent);
                    Type persistentCalls_CallsType = persistentCalls_CallsList.GetType().GetGenericArguments()[0];

                    List<object> removeList = new List<object>();
                    for (int i = 0; i < persistentCalls_CallsList.Count; i++)
                    {
                        object listener = persistentCalls_CallsList[i];
                        FieldInfo targetField = persistentCalls_CallsType.GetField("m_Target", BindingFlags.Instance | BindingFlags.NonPublic);
                        FieldInfo methodNameField = persistentCalls_CallsType.GetField("m_MethodName", BindingFlags.Instance | BindingFlags.NonPublic);
                        UnityEngine.Object listenerTarget = targetField.GetValue(listener) as UnityEngine.Object;
                        string listenerMethodName = methodNameField.GetValue(listener) as string;
                        if (listenerTarget == target && listenerMethodName == method.Name)
                        {
                            removeList.Add(listener);
                        }
                    }
                    foreach (object removeObj in removeList)
                    {
                        persistentCalls_CallsList.Remove(removeObj);
                    }

                    DirtyPersistentCalls(unityEvent);
                }
            }
        }

        public static void RemovePersistentListener(this UnityEvent unityEvent, int index)
        {
            MethodInfo reflectedMethod = typeof(UnityEventBase).GetMethod("RemovePersistentListener", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(int) }, null);
            if (reflectedMethod != null)
            {
                reflectedMethod.Invoke(unityEvent, new object[] { index });
            }
            else
            {
                IList persistentCalls_CallsList = GetPersistentCalls_CallsList(unityEvent);

                persistentCalls_CallsList.RemoveAt(index);

                DirtyPersistentCalls(unityEvent);
            }
        }

        public static void RemoveAllPersistentListeners(this UnityEvent unityEvent)
        {
            for (int i = 0; i < unityEvent.GetPersistentEventCount(); i++)
            {
                unityEvent.RemovePersistentListener(i);
            }
        }

        private static IList GetPersistentCalls_CallsList(UnityEvent unityEvent)
        {
            FieldInfo persistentCallsField = typeof(UnityEventBase).GetField("m_PersistentCalls", BindingFlags.Instance | BindingFlags.NonPublic);
            object persistentCalls = persistentCallsField.GetValue(unityEvent);
            FieldInfo persistentCalls_CallsField = persistentCalls.GetType().GetField("m_Calls", BindingFlags.Instance | BindingFlags.NonPublic);
            object persistentCalls_Calls = persistentCalls_CallsField.GetValue(persistentCalls);
            IList persistentCalls_CallsList = persistentCalls_Calls as IList;
            return persistentCalls_CallsList;
        }

        private static void DirtyPersistentCalls(UnityEvent unityEvent)
        {
            FieldInfo callsField = typeof(UnityEventBase).GetField("m_Calls", BindingFlags.Instance | BindingFlags.NonPublic);
            object calls = callsField.GetValue(unityEvent);
            FieldInfo calls_PersistentCallsField = calls.GetType().GetField("m_PersistentCalls", BindingFlags.Instance | BindingFlags.NonPublic);
            object calls_PersistentCalls = calls_PersistentCallsField.GetValue(calls);
            IList calls_PersistentCallsList = calls_PersistentCalls as IList;
            calls_PersistentCallsList.Clear();
            FieldInfo calls_NeedsUpdateField = calls.GetType().GetField("m_NeedsUpdate", BindingFlags.Instance | BindingFlags.NonPublic);
            calls_NeedsUpdateField.SetValue(calls, true);

            FieldInfo callsDirtyField = typeof(UnityEventBase).GetField("m_CallsDirty", BindingFlags.Instance | BindingFlags.NonPublic);
            callsDirtyField.SetValue(unityEvent, true);
        }

        public static void RemoveAllPersistentAndNonPersistentListeners(this UnityEvent unityEvent)
        {
            unityEvent.RemoveAllListeners();
            unityEvent.RemoveAllPersistentListeners();
        }
    }

}