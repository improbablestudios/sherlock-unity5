﻿using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.ScriptableBehaviourUtilities
{

    public class ScriptableBehaviourManager : MonoBehaviour
    {
        private static ScriptableBehaviourManager s_instance;

        private static event System.Action s_onUpdate;

        private static event System.Action s_onFixedUpdate;

        private static event System.Action s_onLateUpdate;

        private static event System.Action s_onNextUpdate;

        private static event System.Action s_onNextFixedUpdate;

        private static event System.Action s_onNextLateUpdate;

        public static System.Action OnUpdate
        {
            get
            {
                return s_onUpdate;
            }
            set
            {
                s_onUpdate = value;
            }
        }

        public static System.Action OnFixedUpdate
        {
            get
            {
                return s_onFixedUpdate;
            }
            set
            {
                s_onFixedUpdate = value;
            }
        }

        public static System.Action OnLateUpdate
        {
            get
            {
                return s_onLateUpdate;
            }
            set
            {
                s_onLateUpdate = value;
            }
        }

        public static System.Action OnNextUpdate
        {
            get
            {
                return s_onNextUpdate;
            }
            set
            {
                s_onNextUpdate = value;
            }
        }

        public static System.Action OnNextFixedUpdate
        {
            get
            {
                return s_onNextFixedUpdate;
            }
            set
            {
                s_onNextFixedUpdate = value;
            }
        }

        public static System.Action OnNextLateUpdate
        {
            get
            {
                return s_onNextLateUpdate;
            }
            set
            {
                s_onNextLateUpdate = value;
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void OnLoad()
        {
            if (Application.isPlaying)
            {
                if (s_instance == null)
                {
                    s_instance = FindObjectOfType<ScriptableBehaviourManager>();
                }

                if (s_instance == null)
                {
                    ScriptableBehaviourManager instance = new GameObject(nameof(ScriptableBehaviourManager)).AddComponent<ScriptableBehaviourManager>();
                    DontDestroyOnLoad(instance.gameObject);
                    s_instance = instance;
                }
            }
        }

        private void Update()
        {
            s_onUpdate?.Invoke();
            s_onNextUpdate?.Invoke();
            s_onNextUpdate = null;
        }

        private void FixedUpdate()
        {
            s_onFixedUpdate?.Invoke();
            s_onNextFixedUpdate?.Invoke();
            s_onNextFixedUpdate = null;
        }

        private void LateUpdate()
        {
            s_onLateUpdate?.Invoke();
            s_onNextLateUpdate?.Invoke();
            s_onNextLateUpdate = null;
        }
    }

}