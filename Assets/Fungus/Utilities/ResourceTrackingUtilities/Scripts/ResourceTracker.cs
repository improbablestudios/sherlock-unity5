﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.ResourceTrackingUtilities
{
#if UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
#endif
    public static class ResourceTracker
    {
        private static Dictionary<string, UnityEngine.Object> s_loadedInstanceObjects = new Dictionary<string, UnityEngine.Object>();

        private static Dictionary<string, UnityEngine.Object> s_loadedPrefabObjects = new Dictionary<string, UnityEngine.Object>();

        private static Dictionary<string, UnityEngine.Object> s_loadedAssetObjects = new Dictionary<string, UnityEngine.Object>();
        
        public delegate void ObjectEventHandler(UnityEngine.Object obj);
        
        public static event ObjectEventHandler AddedObjectToCache = delegate { };

        public static event ObjectEventHandler RemovedObjectFromCache = delegate { };

        static ResourceTracker()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged += OnPlayModeChanged;
            UnityEditor.EditorApplication.delayCall += 
                () =>
                {
                    if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
                    {
                        RefreshObjectCache(true);
                    }
                };
#else
            RefreshObjectCache(true);
#endif
        }

#if UNITY_EDITOR
        private static void OnPlayModeChanged(UnityEditor.PlayModeStateChange state)
        {
            if (state == UnityEditor.PlayModeStateChange.EnteredEditMode)
            {
                RefreshObjectCache(true);
            }
        }
#endif

        public static void RefreshObjectCache(bool loadAllAssets)
        {
            ClearObjectCache();

#if UNITY_EDITOR
            if (loadAllAssets)
            {
                UnityEditor.EditorUtility.UnloadUnusedAssetsImmediate();
                GC.Collect();

                LoadAllPrefabs();
            }
#endif

            CacheLoadedObjects();
        }

        public static void ClearObjectCache()
        {
            s_loadedInstanceObjects.Clear();
            s_loadedPrefabObjects.Clear();
            s_loadedAssetObjects.Clear();
        }

#if UNITY_EDITOR
        public static void LoadAllPrefabs()
        {
            LoadAllPrefabs("Assets");
        }

        public static void LoadAllScriptableObjects()
        {
            LoadAllScriptableObjects("Assets");
        }

        public static void LoadAllPrefabs(string folder)
        {
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode && !UnityEditor.EditorApplication.isPlaying)
            {
                foreach (string guid in UnityEditor.AssetDatabase.FindAssets("t:" + typeof(GameObject).Name, new string[] { folder }))
                {
                    string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                    if (path.EndsWith(".prefab", StringComparison.Ordinal))
                    {
                        UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    }
                }
            }
        }

        public static void LoadAllScriptableObjects(string folder)
        {
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode && !UnityEditor.EditorApplication.isPlaying)
            {
                foreach (string guid in UnityEditor.AssetDatabase.FindAssets("t:" + typeof(ScriptableObject).Name, new string[] { folder }))
                {
                    string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                    UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path);
                }
            }
        }
#endif

        public static void CacheLoadedObjects()
        {
            foreach (UnityEngine.Object obj in Resources.FindObjectsOfTypeAll<UnityEngine.Object>())
            {
                if (obj != null)
                {
                    AddToCache(obj);
                }
            }
        }

        public static void AddAllToCache(UnityEngine.Object obj)
        {
            GameObject go = obj.GetGameObject();
            if (go != null)
            {
                AddToCache(go);
                foreach (Component component in go.GetComponentsInChildren<Component>(true))
                {
                    AddToCache(component);
                }
            }
            else
            {
                AddToCache(obj);
            }
        }

        public static void AddToCache(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                if (!ObjectIsInternalUnityObject(obj))
                {
                    bool added = false;
                    if (ObjectIsInstance(obj))
                    {
                        added = AddToLoadedInstanceObjectCache(obj);
                    }
                    else if (ObjectIsPrefab(obj))
                    {
                        added = AddToLoadedPrefabObjectCache(obj);
                    }
                    else
                    {
                        added = AddToLoadedAssetObjectCache(obj);
                    }
                    if (added)
                    {
                        AddedObjectToCache(obj);
#if UNITY_EDITOR
                        new UnityEditor.SerializedObject(obj);
#endif
                    }
                }
            }
        }

        public static void RemoveAllFromCache(UnityEngine.Object obj)
        {
            GameObject go = obj.GetGameObject();
            if (go != null)
            {
                RemoveFromCache(go);
                foreach (Component component in go.GetComponentsInChildren<Component>(true))
                {
                    RemoveFromCache(component);
                }
            }
            else
            {
                RemoveFromCache(obj);
            }
        }

        public static void RemoveFromCache(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                if (!ObjectIsInternalUnityObject(obj))
                {
                    bool removed = false;
                    if (ObjectIsInstance(obj))
                    {
                        removed = RemoveFromLoadedInstanceObjectCache(obj);
                    }
                    else if (ObjectIsPrefab(obj))
                    {
                        removed = RemoveFromLoadedPrefabObjectCache(obj);
                    }
                    else
                    {
                        removed = RemoveFromLoadedAssetObjectCache(obj);
                    }
                    if (removed)
                    {
                        RemovedObjectFromCache(obj);
                    }
                }
            }
        }

        public static bool ObjectIsInternalUnityObject(UnityEngine.Object obj)
        {
            return obj.hideFlags.HasFlag(HideFlags.HideAndDontSave) || (int)obj.hideFlags == 125;
        }

        public static bool ObjectIsInstance(UnityEngine.Object obj)
        {
            return obj.GetGameObject() != null && obj.GetGameObject().scene.IsValid();
        }

        public static bool ObjectIsPrefab(UnityEngine.Object obj)
        {
            return obj.GetGameObject() != null && !obj.GetGameObject().scene.IsValid();
        }

        public static string GetObjectKey(UnityEngine.Object obj)
        {
            string key = "";

            if (obj != null)
            {
                IIdentifiable identifable = obj as IIdentifiable;

                if (identifable != null)
                {
                    key = identifable.GetKey();
                }
                else
                {
                    Component component = obj as Component;
                    GameObject gameObject = obj as GameObject;
                    
                    if (component != null)
                    {
                        IIdentifiable identifiableComponentAttached = component.GetComponent<IIdentifiable>();
                        
                        if (identifiableComponentAttached != null)
                        {
                            key = obj.GetUniqueName(obj.GetType(), identifiableComponentAttached.GetKey(), obj.GetType().Name, true, true);
                        }
                        else
                        {
                            key = component.transform.GetPathFromRoot(true, false);
                            key += "/" + component.GetUniqueName();
                        }
                    }
                    else if (gameObject != null)
                    {
                        key = gameObject.GetPathFromRoot(true, false);
                        key += "/" + gameObject.GetUniqueName();
                    }
                    else
                    {
                        key = obj.GetUniqueName();
                    }
                }
            }

            return key;
        }

        private static bool AddToLoadedInstanceObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (!s_loadedInstanceObjects.ContainsKey(key) || s_loadedInstanceObjects[key] != obj)
                {
                    s_loadedInstanceObjects[key] = obj;
                    return true;
                }
            }

            return false;
        }

        private static bool RemoveFromLoadedInstanceObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (s_loadedInstanceObjects.ContainsKey(key))
                {
                    s_loadedInstanceObjects.Remove(key);
                    return true;
                }
            }

            return false;
        }

        private static bool AddToLoadedPrefabObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (!s_loadedPrefabObjects.ContainsKey(key) || s_loadedPrefabObjects[key] != obj)
                {
                    s_loadedPrefabObjects[key] = obj;
                    return true;
                }
            }

            return false;
        }

        private static bool RemoveFromLoadedPrefabObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (s_loadedPrefabObjects.ContainsKey(key))
                {
                    s_loadedPrefabObjects.Remove(key);
                    return true;
                }
            }

            return false;
        }

        private static bool AddToLoadedAssetObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (!s_loadedAssetObjects.ContainsKey(key) || s_loadedAssetObjects[key] != obj)
                {
                    s_loadedAssetObjects[key] = obj;
                    return true;
                }
            }
            return false;
        }

        private static bool RemoveFromLoadedAssetObjectCache(UnityEngine.Object obj)
        {
            string key = GetObjectKey(obj);
            if (key != "")
            {
                if (s_loadedAssetObjects.ContainsKey(key))
                {
                    s_loadedAssetObjects.Remove(key);
                    return true;
                }
            }

            return false;
        }

        public static T[] GetCachedInstanceObjectsOfType<T>()
        {
            return GetCachedInstanceObjectsOfType(typeof(T))
                .Cast<T>()
                .ToArray();
        }

        public static UnityEngine.Object[] GetCachedInstanceObjectsOfType(Type type)
        {
            return s_loadedInstanceObjects.Values
                .Where(x => ((x as UnityEngine.Object) != null) && (type.IsAssignableFrom(x.GetType())))
                .ToArray();
        }


        public static T[] GetCachedAssetObjectsOfType<T>()
        {
            return GetCachedAssetObjectsOfType(typeof(T))
                .Cast<T>()
                .ToArray();
        }

        public static UnityEngine.Object[] GetCachedAssetObjectsOfType(Type type)
        {
            return s_loadedAssetObjects.Values
                .Where(x => ((x as UnityEngine.Object) != null) && (type.IsAssignableFrom(x.GetType())))
                .ToArray();
        }
        
        public static UnityEngine.Object GetCachedObject(Type type, string key)
        {
            if (key == null || key == "")
            {
                return null;
            }

            if (s_loadedInstanceObjects.ContainsKey(key))
            {
                return s_loadedInstanceObjects[key];
            }

            if (s_loadedAssetObjects.ContainsKey(key))
            {
                return s_loadedAssetObjects[key];
            }

            if (s_loadedPrefabObjects.ContainsKey(key))
            {
                return s_loadedPrefabObjects[key];
            }

            return null;
        }

        public static UnityEngine.Object GetCachedInstanceObject(Type type, UnityEngine.Object obj)
        {
            if (obj == null)
            {
                return null;
            }
            return GetCachedInstanceObject(type, GetObjectKey(obj));
        }

        public static UnityEngine.Object GetCachedInstanceObject(Type type, string key)
        {
            if (s_loadedInstanceObjects.ContainsKey(key))
            {
                return s_loadedInstanceObjects[key];
            }
            return null;
        }

        public static T[] FindLoadedInstanceObjectsOfType<T>() where T : UnityEngine.Object
        {
            return FindLoadedInstanceObjectsOfType(typeof(T)).Cast<T>().ToArray();
        }

        public static UnityEngine.Object[] FindLoadedInstanceObjectsOfType(Type type)
        {
            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            UnityEngine.Object[] allLoadedObjects = new UnityEngine.Object[0];
            if (type.IsInterface)
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
            }
            else if (typeof(Component).IsAssignableFrom(type) || typeof(GameObject).IsAssignableFrom(type))
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(type);
            }
            foreach (UnityEngine.Object obj in allLoadedObjects)
            {
                GameObject go = obj.GetGameObject();
                if (go != null && ResourceTracker.ObjectIsInstance(obj) && go.hideFlags != HideFlags.HideAndDontSave)
                {
                    if (!type.IsInterface || go.GetComponent(type) != null)
                    {
                        objectList.Add(obj as UnityEngine.Object);
                    }
                }
            }
            return objectList.Distinct().ToArray();
        }

        public static T FindLoadedInstanceObjectOfType<T>() where T : UnityEngine.Object
        {
            return FindLoadedInstanceObjectOfType(typeof(T)) as T;
        }

        public static UnityEngine.Object FindLoadedInstanceObjectOfType(Type type)
        {
            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            UnityEngine.Object[] allLoadedObjects = new UnityEngine.Object[0];
            if (type.IsInterface)
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
            }
            else if (typeof(Component).IsAssignableFrom(type) || typeof(GameObject).IsAssignableFrom(type))
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(type);
            }
            foreach (UnityEngine.Object obj in allLoadedObjects)
            {
                GameObject go = obj.GetGameObject();
                if (go != null && ResourceTracker.ObjectIsInstance(obj) && go.hideFlags != HideFlags.HideAndDontSave)
                {
                    if (!type.IsInterface || go.GetComponent(type) != null)
                    {
                        return obj as UnityEngine.Object;
                    }
                }
            }
            return null;
        }

        public static GameObject[] FindLoadedInstanceGameObjectsWithComponentType<T>() where T : Component
        {
            return FindLoadedInstanceGameObjectsWithComponentType(typeof(T));
        }

        public static GameObject[] FindLoadedInstanceGameObjectsWithComponentType(Type type)
        {
            return FindLoadedInstanceGameObjectsWithComponentTypes(new Type[] { type });
        }

        public static GameObject[] FindLoadedInstanceGameObjectsWithComponentTypes(Type[] types)
        {
            List<GameObject> gameObjectList = new List<GameObject>();
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
            {
                if (ObjectIsInstance(gameObject) && !ObjectIsInternalUnityObject(gameObject))
                {
                    foreach (Type type in types)
                    {
                        if (gameObject.GetComponent(type) != null)
                        {
                            gameObjectList.Add(gameObject);
                            break;
                        }
                    }
                }
            }
            return gameObjectList.Distinct().ToArray();
        }

        public static GameObject[] FindLoadedInstanceGameObjects()
        {
            return Resources.FindObjectsOfTypeAll(typeof(GameObject)).Cast<GameObject>().ToArray();
        }

        public static GameObject[] FindLoadedInstanceRoots()
        {
            return Resources.FindObjectsOfTypeAll(typeof(GameObject)).Cast<GameObject>().Where(x => x.transform.root == x.transform).ToArray();
        }

        public static GameObject[] FindLoadedInstanceRootsWithComponentType<T>() where T : Component
        {
            return FindLoadedInstanceRootsWithComponentType(typeof(T));
        }

        public static GameObject[] FindLoadedInstanceRootsWithComponentType(Type type)
        {
            return FindLoadedInstanceRootsWithComponentTypes(new Type[] { type });
        }

        public static GameObject[] FindLoadedInstanceRootsWithComponentTypes(Type[] types)
        {
            List<GameObject> gameObjectList = new List<GameObject>();
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
            {
                if (gameObject.transform.root == gameObject.transform)
                {
                    if (ObjectIsInstance(gameObject) && !ObjectIsInternalUnityObject(gameObject))
                    {
                        foreach (Type type in types)
                        {
                            if (gameObject.GetComponentsInChildren(type, true) != null)
                            {
                                gameObjectList.Add(gameObject);
                                break;
                            }
                        }
                    }
                }
            }
            return gameObjectList.Distinct().ToArray();
        }

        public static UnityEngine.Object[] GetCachedInstanceGameObjectsWithComponentType<T>() where T : Component
        {
            return GetCachedInstanceGameObjectsWithComponentType(typeof(T));
        }

        public static UnityEngine.Object[] GetCachedInstanceGameObjectsWithComponentType(Type type)
        {
            return GetCachedInstanceGameObjectsWithComponentTypes(new Type[] { type });
        }

        public static UnityEngine.Object[] GetCachedInstanceGameObjectsWithComponentTypes(Type[] types)
        {
            return types.SelectMany(x => GetCachedInstanceObjectsOfType(x)).Select(x => x.GetGameObject()).Distinct().ToArray();
        }
        
        public static UnityEngine.Object[] FindObjectsInGameObjectOfType(GameObject go, Type type)
        {
            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            if (typeof(Component).IsAssignableFrom(type))
            {
                foreach (UnityEngine.Object obj in go.GetComponentsInChildren(type, true))
                {
                    objectList.Add(obj);
                }
            }
            else if (typeof(GameObject).IsAssignableFrom(type))
            {
                foreach (Transform t in go.GetComponentsInChildren<Transform>(true))
                {
                    UnityEngine.Object obj = t.gameObject as UnityEngine.Object;
                    objectList.Add(obj);
                }
            }
            return objectList.Distinct().ToArray();
        }

        public static UnityEngine.Object[] FindGameObjectsInGameObjectWithComponentType(GameObject go, Type type)
        {
            List<UnityEngine.Object> gameObjectList = new List<UnityEngine.Object>();
            foreach (Transform t in go.GetComponentsInChildren<Transform>(true))
            {
                if ((typeof(Component).IsAssignableFrom(type) || type.IsInterface) && t.GetComponent(type) != null)
                {
                    UnityEngine.Object obj = t.gameObject as UnityEngine.Object;
                    gameObjectList.Add(obj);
                }
            }
            return gameObjectList.Distinct().ToArray();
        }

#if UNITY_EDITOR
        public static T[] FindLoadedPrefabObjectsOfType<T>() where T : UnityEngine.Object
        {
            return FindLoadedPrefabObjectsOfType(typeof(T)).Cast<T>().ToArray();
        }

        public static UnityEngine.Object[] FindLoadedPrefabObjectsOfType(Type type)
        {
            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            UnityEngine.Object[] allLoadedObjects = null;
            if (type.IsInterface)
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(typeof(Component));
            }
            else
            {
                allLoadedObjects = Resources.FindObjectsOfTypeAll(type);
            }
            foreach (UnityEngine.Object obj in allLoadedObjects)
            {
                if (ObjectIsPrefab(obj) && !ObjectIsInternalUnityObject(obj))
                {
                    if (!type.IsInterface || obj.GetGameObject().GetComponent(type) != null)
                    {
                        objectList.Add(obj);
                    }
                }
            }
            return objectList.Distinct().ToArray();
        }

        public static GameObject[] FindLoadedPrefabGameObjectsWithComponentType<T>() where T : Component
        {
            return FindLoadedPrefabGameObjectsWithComponentType(typeof(T));
        }

        public static GameObject[] FindLoadedPrefabGameObjectsWithComponentType(Type type)
        {
            return FindLoadedPrefabGameObjectsWithComponentTypes(new Type[] { type });
        }

        public static GameObject[] FindLoadedPrefabGameObjectsWithComponentTypes(Type[] types)
        {
            List<GameObject> gameObjectList = new List<GameObject>();
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
            {
                if (ObjectIsPrefab(gameObject) && !ObjectIsInternalUnityObject(gameObject))
                {
                    foreach (Type type in types)
                    {
                        if (gameObject.GetComponent(type) != null)
                        {
                            gameObjectList.Add(gameObject);
                            break;
                        }
                    }
                }
            }
            return gameObjectList.Distinct().ToArray();
        }

        public static GameObject[] FindLoadedPrefabRootsWithComponentType<T>() where T : Component
        {
            return FindLoadedPrefabRootsWithComponentType(typeof(T));
        }

        public static GameObject[] FindLoadedPrefabRootsWithComponentType(Type type)
        {
            return FindLoadedPrefabRootsWithComponentTypes(new Type[] { type });
        }

        public static GameObject[] FindLoadedPrefabRootsWithComponentTypes(Type[] types)
        {
            List<GameObject> gameObjectList = new List<GameObject>();
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
            {
                if (gameObject.transform.root == gameObject.transform)
                {
                    if (ObjectIsPrefab(gameObject) && !ObjectIsInternalUnityObject(gameObject))
                    {
                        foreach (Type type in types)
                        {
                            if (gameObject.GetComponentsInChildren(type, true) != null)
                            {
                                gameObjectList.Add(gameObject);
                                break;
                            }
                        }
                    }
                }
            }
            return gameObjectList.Distinct().ToArray();
        }

        public static UnityEngine.Object[] GetCachedPrefabGameObjectsWithComponentType<T>() where T : Component
        {
            return GetCachedPrefabGameObjectsWithComponentType(typeof(T));
        }

        public static UnityEngine.Object[] GetCachedPrefabGameObjectsWithComponentType(Type type)
        {
            return GetCachedPrefabGameObjectsWithComponentTypes(new Type[] { type });
        }

        public static UnityEngine.Object[] GetCachedPrefabGameObjectsWithComponentTypes(Type[] types)
        {
            return types.SelectMany(x => GetCachedPrefabObjectsOfType(x))
                .Select(x => x.GetGameObject())
                .Distinct()
                .ToArray();
        }

        public static T[] GetCachedPrefabObjectsOfType<T>() where T : UnityEngine.Object
        {
            return GetCachedPrefabObjectsOfType(typeof(T)).Cast<T>().ToArray();
        }

        public static UnityEngine.Object[] GetCachedPrefabObjectsOfType(Type type)
        {
            return s_loadedPrefabObjects.Values
                .Where(x => ((x as UnityEngine.Object) != null) && type.IsAssignableFrom(x.GetType()))
                .Distinct()
                .ToArray();
        }

        public static UnityEngine.Object[] GetCachedPrefabObjects()
        {
            return s_loadedPrefabObjects.Values.ToArray();
        }
#endif

        public static bool IsObjectSavedInBuild(UnityEngine.Object obj)
        {
            return 
                (obj != null) && 
                !(
                    (obj.hideFlags & HideFlags.DontSaveInEditor) == HideFlags.DontSaveInEditor
                 || (obj.hideFlags & HideFlags.DontSaveInBuild) == HideFlags.DontSaveInBuild
                 || (obj.GetGameObject() != null && obj.GetGameObject().tag == "EditorOnly")
#if UNITY_EDITOR
                 || (UnityEditor.AssetDatabase.GetLabels(obj).Contains("Editor"))
#endif
                );
        }

        public static bool IsObjectEditable(UnityEngine.Object obj)
        {
            return !((obj.hideFlags & HideFlags.NotEditable) == HideFlags.NotEditable);
        }

        public static bool IsValidInstance(UnityEngine.Object obj)
        {
            return 
                ObjectIsInstance(obj) && 
                IsObjectSavedInBuild(obj) && 
                IsObjectEditable(obj) &&
                !ObjectIsInternalUnityObject(obj);
        }
    }

}