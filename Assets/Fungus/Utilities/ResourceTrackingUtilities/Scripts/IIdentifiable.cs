﻿using UnityEngine;

namespace ImprobableStudios.ResourceTrackingUtilities
{

    public interface IIdentifiable
    {
        void RegenerateKey();
        string GetKey();
        string GetName();
        Texture2D GetIcon();
        GUIStyle GetStyle();
    }

}