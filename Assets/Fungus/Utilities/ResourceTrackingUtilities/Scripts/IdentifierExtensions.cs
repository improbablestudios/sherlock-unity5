﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace ImprobableStudios.ResourceTrackingUtilities
{

    public static class IdentifierExtensions
    {
        public static bool IsUniqueKeyValid(string key)
        {
            return (!string.IsNullOrEmpty(key) && !key.StartsWith("0_", StringComparison.Ordinal) && !key.EndsWith("_", StringComparison.Ordinal));
        }

        public static bool TryGetUniqueKey(this UnityEngine.Object obj, out string key)
        {
            key = GetUniqueKey(obj);
            if (IsUniqueKeyValid(key))
            {
                return true;
            }
            return false;
        }

        public static string GetUniqueKey(this UnityEngine.Object obj)
        {
            if (obj != null)
            {
#if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
                {
                    UnityEngine.Object prefabObject = UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(obj);
                    if (prefabObject != null && 
                        (UnityEditor.PrefabUtility.GetPrefabAssetType(obj) == UnityEditor.PrefabAssetType.NotAPrefab || 
                        (UnityEditor.PrefabUtility.GetPrefabAssetType(obj) == UnityEditor.PrefabAssetType.Regular && !UnityEditor.SceneManagement.EditorSceneManager.IsPreviewScene(obj.GetGameObject().scene))))
                    {
                        return GetKey(prefabObject);
                    }
                    return GetKey(obj);
                }
#endif
                return obj.GetInstanceID().ToString();
            }
            return "";
        }

#if UNITY_EDITOR
        public static long GetLocalFileIdentifier(this UnityEngine.Object obj)
        {
            PropertyInfo inspectorModeInfo = typeof(UnityEditor.SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
            UnityEditor.SerializedObject serializedObject = new UnityEditor.SerializedObject(obj);
            inspectorModeInfo.SetValue(serializedObject, UnityEditor.InspectorMode.Debug, null);
            UnityEditor.SerializedProperty fileIDProp = serializedObject.FindProperty("m_LocalIdentfierInFile"); // NOTE: This property is mispelled in unity's source code! Do not attempt to correct spelling!
            long fileID = fileIDProp.longValue;
            if (fileID == 0)
            {
                string guid = "";
                UnityEditor.AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out guid, out fileID);
            }
            if (fileID == 0)
            {
                ulong unsignedFileId = 0;
                try
                {
                    unsignedFileId = GetOrGenerateFileID(obj);
                    fileID = checked((long)unsignedFileId);
                }
                catch (OverflowException)
                {
                    return 0;
                }
            }
            return fileID;
        }

        private static ulong GetOrGenerateFileID(UnityEngine.Object obj)
        {
            ulong fileID = GetFileIDHint(obj);

            if (fileID == 0)
            {
                // GenerateFileIDHint only work on saved nested prefabs instances.
                UnityEngine.Object instanceHandle = UnityEditor.PrefabUtility.GetPrefabInstanceHandle(obj);
                if (instanceHandle != null)
                {
                    bool isPrefabInstanceSaved = GetFileIDHint(instanceHandle) != 0;
                    if (isPrefabInstanceSaved && UnityEditor.PrefabUtility.IsPartOfNonAssetPrefabInstance(obj) && UnityEditor.PrefabUtility.GetPrefabAssetType(obj) != UnityEditor.PrefabAssetType.MissingAsset)
                        fileID = GenerateFileIDHint(obj);
                }
            }

            return fileID;
        }

        private static ulong GenerateFileIDHint(UnityEngine.Object obj)
        {
            return (ulong)typeof(UnityEditor.Unsupported).GetMethod("GenerateFileIDHint", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { obj });
        }

        private static ulong GetFileIDHint(UnityEngine.Object obj)
        {
            return (ulong)typeof(UnityEditor.Unsupported).GetMethod("GetFileIDHint", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { obj });
        }

        public static string GetGUID(this UnityEngine.Object obj)
        {
            string assetPath = UnityEditor.AssetDatabase.GetAssetPath(obj);

            if (string.IsNullOrEmpty(assetPath))
            {
                GameObject objGameObject = obj.GetGameObject();

                if (objGameObject != null)
                {
                    if (string.IsNullOrEmpty(assetPath))
                    {
                        if (objGameObject.scene != null)
                        {
                            assetPath = objGameObject.scene.path;
                        }
                    }

                    if (string.IsNullOrEmpty(assetPath))
                    {
                        UnityEditor.Experimental.SceneManagement.PrefabStage prefabStage = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(objGameObject);
                        if (prefabStage != null)
                        {
                            assetPath = prefabStage.prefabAssetPath;
                        }
                    }
                }
            }

            return GetGUID(assetPath);
        }

        private static string GetGUID(string assetPath)
        {
            return UnityEditor.AssetDatabase.AssetPathToGUID(assetPath);
        }

        private static string GetKey(UnityEngine.Object obj)
        {
            return GetKey(GetLocalFileIdentifier(obj), GetGUID(obj));
        }

        private static string GetKey(long localID, string guid)
        {
            return localID.ToString() + "_" + guid;
        }
#endif

        /// <summary>
        /// Returns a new name that is guaranteed not to clash with any existing name in a list.
        /// </summary>
        /// <param name="names">The list of names</param>
        /// <param name="newName">The name you want to use</param>
        /// <param name="defaultName">The name that should be used if <paramref name="newName"/> is null or empty</param>
        /// <param name="ignoreIndex">Ignore this index when checking for collisions</param>
        /// <returns>A unique name that does not match any names that currently exist in the specified list</returns>
        public static string GetUniqueName(this string[] names, string newName, string defaultName, int ignoreIndex = -1)
        {
            int suffix = 0;

            // No empty keys allowed
            if (string.IsNullOrEmpty(newName))
            {
                newName = defaultName;
            }

            string baseName = newName.TrimEnd('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

            string key = newName;
            while (true)
            {
                bool collision = false;
                for (int i = 0; i < names.Length; i++)
                {
                    string name = names[i];

                    if (i == ignoreIndex || name == null)
                    {
                        continue;
                    }

                    if (name.Equals(key, StringComparison.CurrentCultureIgnoreCase))
                    {
                        collision = true;
                        suffix++;
                        key = baseName + suffix;
                    }
                }

                if (!collision)
                {
                    return key;
                }
            }
        }

        public static string GetUniqueName(this UnityEngine.Object obj)
        {
            return GetUniqueName(obj, obj.GetType(), true);
        }

        public static string GetUniqueName(this UnityEngine.Object obj, bool includeTypeName)
        {
            return GetUniqueName(obj, obj.GetType(), includeTypeName);
        }

        public static string GetUniqueName(this UnityEngine.Object obj, Type type, bool includeTypeName)
        {
            return GetUniqueName(obj, type, obj.name, type.Name, true, includeTypeName);
        }

        public static string GetUniqueName(this UnityEngine.Object obj, Type type, string name, string typeName, bool includeName, bool includeTypeName)
        {
            if (obj is Component)
            {
                Component component = obj as Component;
                Component[] components = component.gameObject.GetComponents(type);
                if (components.Length > 1)
                {
                    int index = Array.IndexOf(components, component);
                    string componentIdentifier = FormatUniqueName(name, typeName, index, includeName, includeTypeName, true);
                    if (!string.IsNullOrEmpty(componentIdentifier))
                    {
                        return componentIdentifier;
                    }
                }
            }

            return FormatUniqueName(name, typeName, -1, includeName, includeTypeName, false);
        }

        public static string FormatUniqueName(string name, string typeName, int index, bool includeName, bool includeTypeName, bool includeIndex)
        {
            if (includeName && !string.IsNullOrEmpty(name))
            {
                if (includeTypeName && !string.IsNullOrEmpty(typeName))
                {
                    if (includeIndex && index >= 0)
                    {
                        return string.Format("{0} ({1}:{2})", name, typeName, index);
                    }
                    else
                    {
                        return string.Format("{0} ({1})", name, typeName);
                    }
                }
                else
                {
                    if (includeIndex && index >= 0)
                    {
                        return string.Format("{0} ({1})", name, index);
                    }
                    else
                    {
                        return name;
                    }
                }
            }
            else
            {
                if (includeTypeName && !string.IsNullOrEmpty(typeName))
                {
                    if (includeIndex && index >= 0)
                    {
                        return string.Format("({0}:{1})", typeName, index);
                    }
                    else
                    {
                        return string.Format("({0})", typeName);
                    }
                }
                else
                {
                    if (includeIndex && index >= 0)
                    {
                        return string.Format("({0})", index);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }

        public static string GetPathFromRoot(this GameObject gameObject, bool includeRoot, bool includeSelf, char delimiter = '/')
        {
            return GetPathFromRoot(gameObject.transform, includeRoot, includeSelf, delimiter);
        }

        public static string GetPathFromRoot(this Transform transform, bool includeRoot, bool includeSelf, char delimiter = '/')
        {
            string path = "";
            List<string> result = new List<string>();
            if (!includeSelf)
            {
                transform = transform.parent;
            }
            GetParentPathList(transform, ref result, includeRoot);
            result.Reverse();
            foreach (string t in result)
            {
                if (!string.IsNullOrEmpty(t))
                {
                    path += delimiter + t;
                }
            }
            return path.TrimStart(new char[] { delimiter });
        }

        private static void GetParentPathList(Transform child, ref List<string> parentPathList, bool includeRoot)
        {
            if (child == null)
            {
                return;
            }

            if (child.parent == null)
            {
                if (includeRoot)
                {
                    parentPathList.Add(child.name);
                    return;
                }
                else
                {
                    return;
                }
            }

            parentPathList.Add(child.name);

            GetParentPathList(child.parent, ref parentPathList, includeRoot);
        }
        
        public static GameObject GetGameObject<T>(this T target) where T : UnityEngine.Object
        {
            GameObject gameObject = target as GameObject;
            if (gameObject != null)
            {
                return gameObject;
            }
            Component component = target as Component;
            if (component != null)
            {
                return component.gameObject;
            }
            return null;
        }
    }

}