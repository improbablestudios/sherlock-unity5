﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ResourceTrackingUtilities
{

    public class PostProcessAssetChanges : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            EditorApplication.delayCall += 
                () =>
                {
                    foreach (string path in deletedAssets)
                    {
                        UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                        ResourceTracker.RemoveAllFromCache(obj);
                    }

                    foreach (string path in importedAssets)
                    {
                        UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                        ResourceTracker.RemoveAllFromCache(obj);
                        ResourceTracker.AddAllToCache(obj);
                    }

                    foreach (string path in movedFromAssetPaths)
                    {
                        UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                        ResourceTracker.RemoveAllFromCache(obj);
                    }

                    foreach (string path in movedAssets)
                    {
                        UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                        ResourceTracker.RemoveAllFromCache(obj);
                        ResourceTracker.AddAllToCache(obj);
                    }
                };
        }
    }

}
#endif