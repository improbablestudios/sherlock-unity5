﻿#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.GraphWindowUtilities
{

    public class GraphWindowSkin : ScriptableObject
    {
        private static GraphWindowSkin s_instance;
        private static SkinTextures s_textures;

        /// <summary>
        /// Gets the name of the one-and-only <see cref="GraphWindowSkin"/> instance.
        /// </summary>
        public static string SkinName
        {
            get
            {
                return typeof(GraphWindowSkin).Name;
            }
        }

        /// <summary>
        /// Gets the one-and-only <see cref="GraphWindowSkin"/> instance.
        /// </summary>
        public static GraphWindowSkin Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = Resources.Load<GraphWindowSkin>("Editor/~" + SkinName);
                }
                return s_instance;
            }
        }

        /// <summary>
        /// Gets the current skin.
        /// </summary>
        public static SkinTextures Textures
        {
            get
            {
                if (s_textures == null)
                {
                    s_textures = EditorGUIUtility.isProSkin ? Instance.m_DarkSkin : Instance.m_LightSkin;
                }
                return s_textures;
            }
        }
        
        [SerializeField]
        protected SkinTextures m_DarkSkin = new SkinTextures();

        [SerializeField]
        protected SkinTextures m_LightSkin = new SkinTextures();
        
        protected virtual void Reset()
        {
            m_DarkSkin.Reset();
            m_LightSkin.Reset();
        }

        [System.Serializable]
        public class SkinTextures
        {
            [SerializeField]
            protected Texture2D m_PlayBig;

            [SerializeField]
            protected Texture2D m_PlaySmall;

            [SerializeField]
            protected Texture2D m_NodeHexBlueOff;

            [SerializeField]
            protected Texture2D m_NodeHexBlueOn;

            [SerializeField]
            protected Texture2D m_NodeHexGreyOff;

            [SerializeField]
            protected Texture2D m_NodeHexGreyOn;

            [SerializeField]
            protected Texture2D m_NodeHexOrangeOff;

            [SerializeField]
            protected Texture2D m_NodeHexOrangeOn;

            [SerializeField]
            protected Texture2D m_NodeHexPurpleOff;

            [SerializeField]
            protected Texture2D m_NodeHexPurpleOn;

            [SerializeField]
            protected Texture2D m_NodeHexYellowOff;

            [SerializeField]
            protected Texture2D m_NodeHexYellowOn;

            [SerializeField]
            protected Texture2D m_NodeHexRedOff;

            [SerializeField]
            protected Texture2D m_NodeHexRedOn;

            [SerializeField]
            protected Texture2D m_NodePillBlueOff;

            [SerializeField]
            protected Texture2D m_NodePillBlueOn;

            [SerializeField]
            protected Texture2D m_NodePillGreyOff;

            [SerializeField]
            protected Texture2D m_NodePillGreyOn;

            [SerializeField]
            protected Texture2D m_NodePillOrangeOff;

            [SerializeField]
            protected Texture2D m_NodePillOrangeOn;

            [SerializeField]
            protected Texture2D m_NodePillPurpleOff;

            [SerializeField]
            protected Texture2D m_NodePillPurpleOn;

            [SerializeField]
            protected Texture2D m_NodePillYellowOff;

            [SerializeField]
            protected Texture2D m_NodePillYellowOn;

            [SerializeField]
            protected Texture2D m_NodePillRedOff;

            [SerializeField]
            protected Texture2D m_NodePillRedOn;

            [SerializeField]
            protected Texture2D m_NodeSquareBlueOff;

            [SerializeField]
            protected Texture2D m_NodeSquareBlueOn;

            [SerializeField]
            protected Texture2D m_NodeSquareGreyOff;

            [SerializeField]
            protected Texture2D m_NodeSquareGreyOn;

            [SerializeField]
            protected Texture2D m_NodeSquareOrangeOff;

            [SerializeField]
            protected Texture2D m_NodeSquareOrangeOn;

            [SerializeField]
            protected Texture2D m_NodeSquarePurpleOff;

            [SerializeField]
            protected Texture2D m_NodeSquarePurpleOn;

            [SerializeField]
            protected Texture2D m_NodeSquareYellowOff;

            [SerializeField]
            protected Texture2D m_NodeSquareYellowOn;

            [SerializeField]
            protected Texture2D m_NodeSquareRedOff;

            [SerializeField]
            protected Texture2D m_NodeSquareRedOn;

            public Texture2D PlayBig
            {
                get
                {
                    return m_PlayBig;
                }
            }

            public Texture2D PlaySmall
            {
                get
                {
                    return m_PlaySmall;
                }
            }

            public Texture2D NodeHexBlueOff
            {
                get
                {
                    return m_NodeHexBlueOff;
                }
            }

            public Texture2D NodeHexBlueOn
            {
                get
                {
                    return m_NodeHexBlueOn;
                }
            }

            public Texture2D NodeHexGreyOff
            {
                get
                {
                    return m_NodeHexGreyOff;
                }
            }

            public Texture2D NodeHexGreyOn
            {
                get
                {
                    return m_NodeHexGreyOn;
                }
            }

            public Texture2D NodeHexOrangeOff
            {
                get
                {
                    return m_NodeHexOrangeOff;
                }
            }

            public Texture2D NodeHexOrangeOn
            {
                get
                {
                    return m_NodeHexOrangeOn;
                }
            }

            public Texture2D NodeHexPurpleOff
            {
                get
                {
                    return m_NodeHexPurpleOff;
                }
            }

            public Texture2D NodeHexPurpleOn
            {
                get
                {
                    return m_NodeHexPurpleOn;
                }
            }

            public Texture2D NodeHexYellowOff
            {
                get
                {
                    return m_NodeHexYellowOff;
                }
            }

            public Texture2D NodeHexYellowOn
            {
                get
                {
                    return m_NodeHexYellowOn;
                }
            }

            public Texture2D NodeHexRedOff
            {
                get
                {
                    return m_NodeHexRedOff;
                }
            }

            public Texture2D NodeHexRedOn
            {
                get
                {
                    return m_NodeHexRedOn;
                }
            }

            public Texture2D NodePillBlueOff
            {
                get
                {
                    return m_NodePillBlueOff;
                }
            }

            public Texture2D NodePillBlueOn
            {
                get
                {
                    return m_NodePillBlueOn;
                }
            }

            public Texture2D NodePillGreyOff
            {
                get
                {
                    return m_NodePillGreyOff;
                }
            }

            public Texture2D NodePillGreyOn
            {
                get
                {
                    return m_NodePillGreyOn;
                }
            }

            public Texture2D NodePillOrangeOff
            {
                get
                {
                    return m_NodePillOrangeOff;
                }
            }

            public Texture2D NodePillOrangeOn
            {
                get
                {
                    return m_NodePillOrangeOn;
                }
            }

            public Texture2D NodePillPurpleOff
            {
                get
                {
                    return m_NodePillPurpleOff;
                }
            }

            public Texture2D NodePillPurpleOn
            {
                get
                {
                    return m_NodePillPurpleOn;
                }
            }

            public Texture2D NodePillYellowOff
            {
                get
                {
                    return m_NodePillYellowOff;
                }
            }

            public Texture2D NodePillYellowOn
            {
                get
                {
                    return m_NodePillYellowOn;
                }
            }

            public Texture2D NodePillRedOff
            {
                get
                {
                    return m_NodePillRedOff;
                }
            }

            public Texture2D NodePillRedOn
            {
                get
                {
                    return m_NodePillRedOn;
                }
            }

            public Texture2D NodeSquareBlueOff
            {
                get
                {
                    return m_NodeSquareBlueOff;
                }
            }

            public Texture2D NodeSquareBlueOn
            {
                get
                {
                    return m_NodeSquareBlueOn;
                }
            }

            public Texture2D NodeSquareGreyOff
            {
                get
                {
                    return m_NodeSquareGreyOff;
                }
            }

            public Texture2D NodeSquareGreyOn
            {
                get
                {
                    return m_NodeSquareGreyOn;
                }
            }

            public Texture2D NodeSquareOrangeOff
            {
                get
                {
                    return m_NodeSquareOrangeOff;
                }
            }

            public Texture2D NodeSquareOrangeOn
            {
                get
                {
                    return m_NodeSquareOrangeOn;
                }
            }

            public Texture2D NodeSquarePurpleOff
            {
                get
                {
                    return m_NodeSquarePurpleOff;
                }
            }

            public Texture2D NodeSquarePurpleOn
            {
                get
                {
                    return m_NodeSquarePurpleOn;
                }
            }

            public Texture2D NodeSquareYellowOff
            {
                get
                {
                    return m_NodeSquareYellowOff;
                }
            }

            public Texture2D NodeSquareYellowOn
            {
                get
                {
                    return m_NodeSquareYellowOn;
                }
            }

            public Texture2D NodeSquareRedOff
            {
                get
                {
                    return m_NodeSquareRedOff;
                }
            }

            public Texture2D NodeSquareRedOn
            {
                get
                {
                    return m_NodeSquareRedOn;
                }
            }

            public virtual void Reset()
            {
                foreach (FieldInfo field in typeof(SkinTextures).GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    field.SetValue(this, Resources.Load<Texture2D>("Editor/Textures/~" + field.Name.Substring(2)));
                }
            }
        }
    }
}

#endif