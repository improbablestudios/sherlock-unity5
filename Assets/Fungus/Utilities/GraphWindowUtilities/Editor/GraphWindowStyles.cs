#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace ImprobableStudios.GraphWindowUtilities
{

    public static class GraphWindowStyles
    {
        private static GUIStyle s_nodeHexBlueOff;
        public static GUIStyle NodeHexBlueOff
        {
            get
            {
                if (s_nodeHexBlueOff == null)
                {
                    s_nodeHexBlueOff = GetNodeStyle(GraphWindowSkin.Textures.NodeHexBlueOff);
                }
                return s_nodeHexBlueOff;
            }
        }

        private static GUIStyle s_nodeHexBlueOn;
        public static GUIStyle NodeHexBlueOn
        {
            get
            {
                if (s_nodeHexBlueOn == null)
                {
                    s_nodeHexBlueOn = GetNodeStyle(GraphWindowSkin.Textures.NodeHexBlueOn);
                }
                return s_nodeHexBlueOn;
            }
        }

        private static GUIStyle s_nodeHexGreyOff;
        public static GUIStyle NodeHexGreyOff
        {
            get
            {
                if (s_nodeHexGreyOff == null)
                {
                    s_nodeHexGreyOff = GetNodeStyle(GraphWindowSkin.Textures.NodeHexGreyOff);
                }
                return s_nodeHexGreyOff;
            }
        }

        private static GUIStyle s_nodeHexGreyOn;
        public static GUIStyle NodeHexGreyOn
        {
            get
            {
                if (s_nodeHexGreyOn == null)
                {
                    s_nodeHexGreyOn = GetNodeStyle(GraphWindowSkin.Textures.NodeHexGreyOn);
                }
                return s_nodeHexGreyOn;
            }
        }

        private static GUIStyle s_nodeHexOrangeOff;
        public static GUIStyle NodeHexOrangeOff
        {
            get
            {
                if (s_nodeHexOrangeOff == null)
                {
                    s_nodeHexOrangeOff = GetNodeStyle(GraphWindowSkin.Textures.NodeHexOrangeOff);
                }
                return s_nodeHexOrangeOff;
            }
        }

        private static GUIStyle s_nodeHexOrangeOn;
        public static GUIStyle NodeHexOrangeOn
        {
            get
            {
                if (s_nodeHexOrangeOn == null)
                {
                    s_nodeHexOrangeOn = GetNodeStyle(GraphWindowSkin.Textures.NodeHexOrangeOn);
                }
                return s_nodeHexOrangeOn;
            }
        }

        private static GUIStyle s_nodeHexPurpleOff;
        public static GUIStyle NodeHexPurpleOff
        {
            get
            {
                if (s_nodeHexPurpleOff == null)
                {
                    s_nodeHexPurpleOff = GetNodeStyle(GraphWindowSkin.Textures.NodeHexPurpleOff);
                }
                return s_nodeHexPurpleOff;
            }
        }

        private static GUIStyle s_nodeHexPurpleOn;
        public static GUIStyle NodeHexPurpleOn
        {
            get
            {
                if (s_nodeHexPurpleOn == null)
                {
                    s_nodeHexPurpleOn = GetNodeStyle(GraphWindowSkin.Textures.NodeHexPurpleOn);
                }
                return s_nodeHexPurpleOn;
            }
        }

        private static GUIStyle s_nodeHexYellowOff;
        public static GUIStyle NodeHexYellowOff
        {
            get
            {
                if (s_nodeHexYellowOff == null)
                {
                    s_nodeHexYellowOff = GetNodeStyle(GraphWindowSkin.Textures.NodeHexYellowOff);
                }
                return s_nodeHexYellowOff;
            }
        }

        private static GUIStyle s_nodeHexYellowOn;
        public static GUIStyle NodeHexYellowOn
        {
            get
            {
                if (s_nodeHexYellowOn == null)
                {
                    s_nodeHexYellowOn = GetNodeStyle(GraphWindowSkin.Textures.NodeHexYellowOn);
                }
                return s_nodeHexYellowOn;
            }
        }

        private static GUIStyle s_nodeHexRedOff;
        public static GUIStyle NodeHexRedOff
        {
            get
            {
                if (s_nodeHexRedOff == null)
                {
                    s_nodeHexRedOff = GetErrorNodeStyle(GraphWindowSkin.Textures.NodeHexRedOff);
                }
                return s_nodeHexRedOff;
            }
        }

        private static GUIStyle s_nodeHexRedOn;
        public static GUIStyle NodeHexRedOn
        {
            get
            {
                if (s_nodeHexRedOn == null)
                {
                    s_nodeHexRedOn = GetErrorNodeStyle(GraphWindowSkin.Textures.NodeHexRedOn);
                }
                return s_nodeHexRedOn;
            }
        }

        private static GUIStyle s_nodePillBlueOff;
        public static GUIStyle NodePillBlueOff
        {
            get
            {
                if (s_nodePillBlueOff == null)
                {
                    s_nodePillBlueOff = GetNodeStyle(GraphWindowSkin.Textures.NodePillBlueOff);
                }
                return s_nodePillBlueOff;
            }
        }

        private static GUIStyle s_nodePillBlueOn;
        public static GUIStyle NodePillBlueOn
        {
            get
            {
                if (s_nodePillBlueOn == null)
                {
                    s_nodePillBlueOn = GetNodeStyle(GraphWindowSkin.Textures.NodePillBlueOn);
                }
                return s_nodePillBlueOn;
            }
        }

        private static GUIStyle s_nodePillGreyOff;
        public static GUIStyle NodePillGreyOff
        {
            get
            {
                if (s_nodePillGreyOff == null)
                {
                    s_nodePillGreyOff = GetNodeStyle(GraphWindowSkin.Textures.NodePillGreyOff);
                }
                return s_nodePillGreyOff;
            }
        }

        private static GUIStyle s_nodePillGreyOn;
        public static GUIStyle NodePillGreyOn
        {
            get
            {
                if (s_nodePillGreyOn == null)
                {
                    s_nodePillGreyOn = GetNodeStyle(GraphWindowSkin.Textures.NodePillGreyOn);
                }
                return s_nodePillGreyOn;
            }
        }

        private static GUIStyle s_nodePillOrangeOff;
        public static GUIStyle NodePillOrangeOff
        {
            get
            {
                if (s_nodePillOrangeOff == null)
                {
                    s_nodePillOrangeOff = GetNodeStyle(GraphWindowSkin.Textures.NodePillOrangeOff);
                }
                return s_nodePillOrangeOff;
            }
        }

        private static GUIStyle s_nodePillOrangeOn;
        public static GUIStyle NodePillOrangeOn
        {
            get
            {
                if (s_nodePillOrangeOn == null)
                {
                    s_nodePillOrangeOn = GetNodeStyle(GraphWindowSkin.Textures.NodePillOrangeOn);
                }
                return s_nodePillOrangeOn;
            }
        }

        private static GUIStyle s_nodePillPurpleOff;
        public static GUIStyle NodePillPurpleOff
        {
            get
            {
                if (s_nodePillPurpleOff == null)
                {
                    s_nodePillPurpleOff = GetNodeStyle(GraphWindowSkin.Textures.NodePillPurpleOff);
                }
                return s_nodePillPurpleOff;
            }
        }

        private static GUIStyle s_nodePillPurpleOn;
        public static GUIStyle NodePillPurpleOn
        {
            get
            {
                if (s_nodePillPurpleOn == null)
                {
                    s_nodePillPurpleOn = GetNodeStyle(GraphWindowSkin.Textures.NodePillPurpleOn);
                }
                return s_nodePillPurpleOn;
            }
        }

        private static GUIStyle s_nodePillYellowOff;
        public static GUIStyle NodePillYellowOff
        {
            get
            {
                if (s_nodePillYellowOff == null)
                {
                    s_nodePillYellowOff = GetNodeStyle(GraphWindowSkin.Textures.NodePillYellowOff);
                }
                return s_nodePillYellowOff;
            }
        }

        private static GUIStyle s_nodePillYellowOn;
        public static GUIStyle NodePillYellowOn
        {
            get
            {
                if (s_nodePillYellowOn == null)
                {
                    s_nodePillYellowOn = GetNodeStyle(GraphWindowSkin.Textures.NodePillYellowOn);
                }
                return s_nodePillYellowOn;
            }
        }

        private static GUIStyle s_nodePillRedOff;
        public static GUIStyle NodePillRedOff
        {
            get
            {
                if (s_nodePillRedOff == null)
                {
                    s_nodePillRedOff = GetErrorNodeStyle(GraphWindowSkin.Textures.NodePillRedOff);
                }
                return s_nodePillRedOff;
            }
        }

        private static GUIStyle s_nodePillRedOn;
        public static GUIStyle NodePillRedOn
        {
            get
            {
                if (s_nodePillRedOn == null)
                {
                    s_nodePillRedOn = GetErrorNodeStyle(GraphWindowSkin.Textures.NodePillRedOn);
                }
                return s_nodePillRedOn;
            }
        }

        private static GUIStyle s_nodeSquareBlueOff;
        public static GUIStyle NodeSquareBlueOff
        {
            get
            {
                if (s_nodeSquareBlueOff == null)
                {
                    s_nodeSquareBlueOff = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareBlueOff);
                }
                return s_nodeSquareBlueOff;
            }
        }

        private static GUIStyle s_nodeSquareBlueOn;
        public static GUIStyle NodeSquareBlueOn
        {
            get
            {
                if (s_nodeSquareBlueOn == null)
                {
                    s_nodeSquareBlueOn = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareBlueOn);
                }
                return s_nodeSquareBlueOn;
            }
        }

        private static GUIStyle s_nodeSquareGreyOff;
        public static GUIStyle NodeSquareGreyOff
        {
            get
            {
                if (s_nodeSquareGreyOff == null)
                {
                    s_nodeSquareGreyOff = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareGreyOff);
                }
                return s_nodeSquareGreyOff;
            }
        }

        private static GUIStyle s_nodeSquareGreyOn;
        public static GUIStyle NodeSquareGreyOn
        {
            get
            {
                if (s_nodeSquareGreyOn == null)
                {
                    s_nodeSquareGreyOn = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareGreyOn);
                }
                return s_nodeSquareGreyOn;
            }
        }

        private static GUIStyle s_nodeSquareOrangeOff;
        public static GUIStyle NodeSquareOrangeOff
        {
            get
            {
                if (s_nodeSquareOrangeOff == null)
                {
                    s_nodeSquareOrangeOff = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareOrangeOff);
                }
                return s_nodeSquareOrangeOff;
            }
        }

        private static GUIStyle s_nodeSquareOrangeOn;
        public static GUIStyle NodeSquareOrangeOn
        {
            get
            {
                if (s_nodeSquareOrangeOn == null)
                {
                    s_nodeSquareOrangeOn = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareOrangeOn);
                }
                return s_nodeSquareOrangeOn;
            }
        }

        private static GUIStyle s_nodeSquarePurpleOff;
        public static GUIStyle NodeSquarePurpleOff
        {
            get
            {
                if (s_nodeSquarePurpleOff == null)
                {
                    s_nodeSquarePurpleOff = GetNodeStyle(GraphWindowSkin.Textures.NodeSquarePurpleOff);
                }
                return s_nodeSquarePurpleOff;
            }
        }

        private static GUIStyle s_nodeSquarePurpleOn;
        public static GUIStyle NodeSquarePurpleOn
        {
            get
            {
                if (s_nodeSquarePurpleOn == null)
                {
                    s_nodeSquarePurpleOn = GetNodeStyle(GraphWindowSkin.Textures.NodeSquarePurpleOn);
                }
                return s_nodeSquarePurpleOn;
            }
        }
        
        private static GUIStyle s_nodeSquareYellowOff;
        public static GUIStyle NodeSquareYellowOff
        {
            get
            {
                if (s_nodeSquareYellowOff == null)
                {
                    s_nodeSquareYellowOff = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareYellowOff);
                }
                return s_nodeSquareYellowOff;
            }
        }

        private static GUIStyle s_nodeSquareYellowOn;
        public static GUIStyle NodeSquareYellowOn
        {
            get
            {
                if (s_nodeSquareYellowOn == null)
                {
                    s_nodeSquareYellowOn = GetNodeStyle(GraphWindowSkin.Textures.NodeSquareYellowOn);
                }
                return s_nodeSquareYellowOn;
            }
        }

        private static GUIStyle s_nodeSquareRedOff;
        public static GUIStyle NodeSquareRedOff
        {
            get
            {
                if (s_nodeSquareRedOff == null)
                {
                    s_nodeSquareRedOff = GetErrorNodeStyle(GraphWindowSkin.Textures.NodeSquareRedOff);
                }
                return s_nodeSquareRedOff;
            }
        }

        private static GUIStyle s_nodeSquareRedOn;
        public static GUIStyle NodeSquareRedOn
        {
            get
            {
                if (s_nodeSquareRedOn == null)
                {
                    s_nodeSquareRedOn = GetErrorNodeStyle(GraphWindowSkin.Textures.NodeSquareRedOn);
                }
                return s_nodeSquareRedOn;
            }
        }
        
        private static GUIStyle s_nodeSmallGrey;
        public static GUIStyle NodeSmallGrey
        {
            get
            {
                if (s_nodeSmallGrey == null)
                {
                    s_nodeSmallGrey = new GUIStyle(GUI.skin.FindStyle("sv_label_0"));
                    s_nodeSmallGrey.fontSize = 11;
                    s_nodeSmallGrey.fixedHeight = 0;
                    s_nodeSmallGrey.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallGrey;
            }
        }

        private static GUIStyle s_nodeSmallBlue;
        public static GUIStyle NodeSmallBlue
        {
            get
            {
                if (s_nodeSmallBlue == null)
                {
                    s_nodeSmallBlue = new GUIStyle(GUI.skin.FindStyle("sv_label_1"));
                    s_nodeSmallBlue.fontSize = 11;
                    s_nodeSmallBlue.fixedHeight = 0;
                    s_nodeSmallBlue.alignment = TextAnchor.MiddleCenter;

                }
                return s_nodeSmallBlue;
            }
        }

        private static GUIStyle s_nodeSmallTeal;
        public static GUIStyle NodeSmallTeal
        {
            get
            {
                if (s_nodeSmallTeal == null)
                {
                    s_nodeSmallTeal = new GUIStyle(GUI.skin.FindStyle("sv_label_2"));
                    s_nodeSmallTeal.fontSize = 11;
                    s_nodeSmallTeal.fixedHeight = 0;
                    s_nodeSmallTeal.alignment = TextAnchor.MiddleCenter;

                }
                return s_nodeSmallTeal;
            }
        }

        private static GUIStyle s_nodeSmallGreen;
        public static GUIStyle NodeSmallGreen
        {
            get
            {
                if (s_nodeSmallGreen == null)
                {
                    s_nodeSmallGreen = new GUIStyle(GUI.skin.FindStyle("sv_label_3"));
                    s_nodeSmallGreen.fontSize = 11;
                    s_nodeSmallGreen.fixedHeight = 0;
                    s_nodeSmallGreen.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallGreen;
            }
        }

        private static GUIStyle s_nodeSmallYellow;
        public static GUIStyle NodeSmallYellow
        {
            get
            {
                if (s_nodeSmallYellow == null)
                {
                    s_nodeSmallYellow = new GUIStyle(GUI.skin.FindStyle("sv_label_4"));
                    s_nodeSmallYellow.fontSize = 11;
                    s_nodeSmallYellow.fixedHeight = 0;
                    s_nodeSmallYellow.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallYellow;
            }
        }

        private static GUIStyle s_nodeSmallOrange;
        public static GUIStyle NodeSmallOrange
        {
            get
            {
                if (s_nodeSmallOrange == null)
                {
                    s_nodeSmallOrange = new GUIStyle(GUI.skin.FindStyle("sv_label_5"));
                    s_nodeSmallOrange.fontSize = 11;
                    s_nodeSmallOrange.fixedHeight = 0;
                    s_nodeSmallOrange.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallOrange;
            }
        }

        private static GUIStyle s_nodeSmallRed;
        public static GUIStyle NodeSmallRed
        {
            get
            {
                if (s_nodeSmallRed == null)
                {
                    s_nodeSmallRed = new GUIStyle(GUI.skin.FindStyle("sv_label_6"));
                    s_nodeSmallRed.fontSize = 11;
                    s_nodeSmallRed.fixedHeight = 0;
                    s_nodeSmallRed.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallRed;
            }
        }

        private static GUIStyle s_nodeSmallPurple;
        public static GUIStyle NodeSmallPurple
        {
            get
            {
                if (s_nodeSmallPurple == null)
                {
                    s_nodeSmallPurple = new GUIStyle(GUI.skin.FindStyle("sv_label_7"));
                    s_nodeSmallPurple.fontSize = 11;
                    s_nodeSmallPurple.fixedHeight = 0;
                    s_nodeSmallPurple.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeSmallPurple;
            }
        }

        private static GUIStyle s_nodeTopLabel;
        public static GUIStyle NodeTopLabel
        {
            get
            {
                if (s_nodeTopLabel == null)
                {
                    s_nodeTopLabel = new GUIStyle();
                    s_nodeTopLabel.normal.textColor = Color.white;
                    s_nodeTopLabel.alignment = TextAnchor.MiddleCenter;
                }
                return s_nodeTopLabel;
            }
        }

        private static GUIStyle s_connectionDescriptionLabel;
        public static GUIStyle ConnectionDescriptionLabel
        {
            get
            {
                if (s_connectionDescriptionLabel == null)
                {
                    s_connectionDescriptionLabel = new GUIStyle();
                    s_connectionDescriptionLabel.normal.textColor = new Color(0.78f, 0.78f, 0.78f, 1.0f);
                    s_connectionDescriptionLabel.alignment = TextAnchor.MiddleCenter;
                }
                return s_connectionDescriptionLabel;
            }
        }
        
        private static Texture2D s_playBigIcon;
        public static Texture2D PlayBigIcon
        {
            get
            {
                if (s_playBigIcon == null)
                {
                    s_playBigIcon = GraphWindowSkin.Textures.PlayBig;
                }
                return s_playBigIcon;
            }
        }

        private static Texture2D s_playSmallIcon;
        public static Texture2D PlaySmallIcon
        {
            get
            {
                if (s_playSmallIcon == null)
                {
                    s_playSmallIcon = GraphWindowSkin.Textures.PlaySmall;
                }
                return s_playSmallIcon;
            }
        }
        
        private static GUIStyle GetNodeStyle(Texture2D background)
        {
            GUIStyle style = new GUIStyle();
            style.border = new RectOffset(20, 20, 0, 0);
            style.padding = new RectOffset(20, 20, 10, 10);
            style.margin = new RectOffset(0, 0, 0, 0);
            style.fontSize = 11;
            style.alignment = TextAnchor.MiddleCenter;
            style.fontStyle = FontStyle.Normal;
            style.normal.textColor = Color.black;
            style.hover.textColor = Color.black;
            style.active.textColor = Color.black;
            style.focused.textColor = Color.black;
            style.normal.background = background;
            style.hover.background = background;
            style.active.background = background;
            style.focused.background = background;
            return style;
        }

        private static GUIStyle GetErrorNodeStyle(Texture2D background)
        {
            GUIStyle style = new GUIStyle();
            style.border = new RectOffset(20, 20, 0, 0);
            style.padding = new RectOffset(20, 20, 10, 10);
            style.margin = new RectOffset(0, 0, 0, 0);
            style.fontSize = 11;
            style.alignment = TextAnchor.MiddleCenter;
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.white;
            style.hover.textColor = Color.white;
            style.active.textColor = Color.white;
            style.focused.textColor = Color.white;
            style.normal.background = background;
            style.hover.background = background;
            style.active.background = background;
            style.focused.background = background;
            return style;
        }
    }

}
#endif