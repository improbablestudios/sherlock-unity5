#if UNITY_EDITOR
using ImprobableStudios.BaseWindowUtilities;
using ImprobableStudios.SearchPopupUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ImprobableStudios.GraphWindowUtilities
{

    public abstract class GraphWindow : BaseWindow
    {
        /// <summary>
        /// Duration of fade for executing icon displayed beside blocks & commands.
        /// </summary>
        public const float EXECUTING_ICON_FADE_TIME = 0.5f;

        /// <summary>
        /// Scroll position of graph editor window.
        /// </summary>
        private Vector2 m_scrollPos;

        /// <summary>
        /// Zoom level of graph editor window.
        /// </summary>
        private float m_zoomLevel = 1f;

        /// <summary>
        /// The amount that new nodes are offset.
        /// </summary>
        private Vector2 m_newNodeOffset;
        
        protected List<Action> m_ScheduledActions = new List<Action>();

        protected List<INode> m_windowNodeMap = new List<INode>();

        protected INode[] m_executingNodes;

        protected Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> m_switchConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();
        protected Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> m_activateConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();
        protected Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> m_deactivateConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();

        protected Dictionary<INode, GUIStyle> m_nodeStyles = new Dictionary<INode, GUIStyle>();
        protected Dictionary<INode, bool> m_nodeErrors = new Dictionary<INode, bool>();
        protected Dictionary<INode, IConnectNodes[]> m_nodeConnections = new Dictionary<INode, IConnectNodes[]>();
        protected Dictionary<INode, GUIStyle> m_nodeWidestLabelStyle = new Dictionary<INode, GUIStyle>();
        protected Dictionary<INode, GUIContent> m_nodeWidestLabelContent = new Dictionary<INode, GUIContent>();

        private int m_dragWindowId = -1;
        private Vector2 m_startDragPosition;
        private Vector2 m_startMousePosition;
        private List<Vector2> m_offsetsFromDraggedNode = new List<Vector2>();
        private bool m_wasPanningGridWithRightClick;

        private bool m_clickedAlreadySelectedElementOnMouseDown;
        private bool m_mouseDownWasOnNode;
        private bool m_mouseDownWasOnGrid;
        private bool m_isDraggingNode;
        private bool m_isPanning;
        private EventType m_lastEventType;

        private string m_wasFocusedOn;
        private INode m_shouldFocusOn;

        private int m_forceRepaintCount;

        protected RectSelection m_rectSelection;

        private string m_searchString = "";
        protected INode[] m_focusedNodes;

        protected float m_animDuration = 0.25f;

        protected bool m_scrollPosIsAnimating;
        protected float m_scrollPosAnim;
        protected double m_scrollPosAnimStartTime;
        protected Vector2 m_scrollPosStart;
        protected Vector2 m_scrollPosEnd;

        protected bool m_zoomLevelIsAnimating;
        protected float m_zoomLevelAnim;
        protected double m_zoomLevelAnimStartTime;
        protected float m_zoomLevelStart;
        protected float m_zoomLevelEnd = 1;

        public Vector2 ScrollPos
        {
            get
            {
                return m_scrollPos;
            }
            set
            {
                m_scrollPos = value;
            }
        }

        public float ZoomLevel
        {
            get
            {
                return m_zoomLevel;
            }
            set
            {
                m_zoomLevel = value;
            }
        }

        public Vector2 NewNodeOffset
        {
            get
            {
                return m_newNodeOffset;
            }
            set
            {
                m_newNodeOffset = value;
            }
        }
        

        protected virtual int NodeMinWidth
        {
            get
            {
                return 100;
            }
        }

        protected virtual int NodeMinHeight
        {
            get
            {
                return 40;
            }
        }

        protected virtual int GridSize
        {
            get
            {
                return 100;
            }
        }

        protected virtual int SnapSize
        {
            get
            {
                return 10;
            }
        }

        protected virtual float MinZoomValue
        {
            get
            {
                return 0.1f;
            }
        }

        protected virtual float MaxZoomValue
        {
            get
            {
                return 1f;
            }
        }

        protected virtual bool CanCreateNodes
        {
            get
            {
                return false;
            }
        }
        
        protected virtual float ConnectionButtonHeight
        {
            get
            {
                return 20f;
            }
        }

        protected virtual float ConnectionInOutOffset
        {
            get
            {
                return 10f;
            }
        }

        protected virtual float ArrowSize
        {
            get
            {
                return 10f;
            }
        }

        protected virtual float DescriptionOffset
        {
            get
            {
                return 12f;
            }
        }

        protected virtual float LineWidth
        {
            get
            {
                return 3f;
            }
        }

        public virtual GUIStyle ToolbarStyle
        {
            get
            {
                GUIStyle style = new GUIStyle(EditorStyles.toolbar);
                style.fixedHeight = 0;
                style.stretchHeight = true;
                return style;
            }
        }

        public virtual GUIStyle FooterStyle
        {
            get
            {
                return GUIStyle.none;
            }
        }

        public virtual GUIStyle TitlebarStyle
        {
            get
            {
                GUIStyle style = new GUIStyle((GUIStyle)"ProjectBrowserTopBarBg");
                style.padding = new RectOffset(8, 4, 2, 2);
                style.border = new RectOffset(3, 3, 3, 3);
                style.fixedHeight = 25f;
                return style;
            }
        }

        public virtual GUIStyle GraphNameStyle
        {
            get
            {
                GUIStyle style = new GUIStyle(EditorStyles.whiteBoldLabel);
                style.normal.textColor = Color.white;
                style.clipping = TextClipping.Clip;
                style.padding = new RectOffset(0, 0, 0, 0);
                return style;
            }
        }

        public virtual GUIStyle GraphDescriptionStyle
        {
            get
            {
                return EditorStyles.helpBox;
            }
        }

        public virtual GUIStyle SaveToggleStyle
        {
            get
            {
                GUIStyle style = new GUIStyle(EditorStyles.toggle);
                style.font = EditorStyles.miniLabel.font;
                style.fontSize = EditorStyles.miniLabel.fontSize;
                style.fontStyle = EditorStyles.miniLabel.fontStyle;
                style.alignment = TextAnchor.MiddleLeft;
                style.padding.top = 3;
                style.padding.bottom = 0;
                style.margin.top = 0;
                style.margin.bottom = 2;
                return style;
            }
        }

        public virtual GUIStyle SaveButtonStyle
        {
            get
            {
                GUIStyle style = new GUIStyle(EditorStyles.miniButton);
                style.margin.top = 4;
                return style;
            }
        }
        
        public virtual GUIStyle AddButtonStyle
        {
            get
            {
                return EditorStyles.toolbarButton;
            }
        }

        public virtual Texture AddButtonIcon
        {
            get
            {
                return EditorGUIUtility.FindTexture(EditorGUIUtility.isProSkin ? "d_Toolbar Plus" : "Toolbar Plus");
            }
        }

        protected virtual Color GridBackgroundColor
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return new Color(0.18f, 0.18f, 0.18f, 1.0f);
                }
                else
                {
                    return new Color(0.34f, 0.34f, 0.34f, 1.0f);
                }
            }
        }

        protected virtual Color GridMajorLineColor
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return new Color(0.08f, 0.08f, 0.08f, 1.0f);
                }
                else
                {
                    return new Color(0.28f, 0.28f, 0.28f, 1.0f);
                }
            }
        }

        protected virtual Color GridMinorLineColor
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return new Color(0.14f, 0.14f, 0.14f, 1.0f);
                }
                else
                {
                    return new Color(0.31f, 0.31f, 0.31f, 1.0f);
                }
            }
        }

        protected virtual Color ConnectionNormalColor
        {
            get
            {
                return new Color(0.55f, 0.55f, 0.55f, 1.0f);
            }
        }
        
        protected virtual Color ConnectionHighlightColor
        {
            get
            {
                return Color.green;
            }
        }

        public Graph TargetGraph
        {
            get;
            set;
        }

        public abstract Graph GetGraph();

        public virtual string GetGraphTitle()
        {
            return "";
        }

        public virtual string GetGraphDescription()
        {
            return "";
        }

        public virtual bool ShouldForceCenterViewOnGraph()
        {
            return TargetGraph != GetGraph();
        }
        
        public virtual Vector2 GetNewNodePositionInGraph()
        {
            Vector2 newNodePosition = new Vector2(50 - m_scrollPos.x, 50 - m_scrollPos.y);
            newNodePosition += m_newNodeOffset;
            m_newNodeOffset += new Vector2(20, 20);
            return newNodePosition;
        }

        protected virtual void RefreshWindow()
        {
            m_activateConnectionMap.Clear();
            m_deactivateConnectionMap.Clear();
            m_switchConnectionMap.Clear();
            m_windowNodeMap.Clear();
            
            RefreshNodeWidths();
            RefreshNodeStyles();
            RefreshNodeErrors();
            RefreshNodeConnections();

            Repaint();
        }


        protected virtual void RefreshNodeWidths()
        {
            m_nodeWidestLabelStyle.Clear();
            m_nodeWidestLabelContent.Clear();
        }

        protected virtual void RefreshNodeStyles()
        {
            m_nodeStyles.Clear();
        }

        protected virtual void RefreshNodeErrors()
        {
            m_nodeErrors.Clear();
        }

        protected virtual void RefreshNodeConnections()
        {
            m_nodeConnections.Clear();
        }

        protected virtual void PopulateGraph()
        {
            PopulateGraphNodes();
            PopulateConnections();
        }

        protected virtual void PopulateGraphNodes()
        {
        }

        protected void PopulateConnections()
        {
            if (TargetGraph != null)
            {
                m_switchConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();
                m_activateConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();
                m_deactivateConnectionMap = new Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>>();

                foreach (INode node in TargetGraph.m_Nodes)
                {
                    PopulateNodeConnectionMaps(node, m_switchConnectionMap, m_activateConnectionMap, m_deactivateConnectionMap);
                }
            }
        }
        
        protected virtual void OnFocus()
        {
            PopulateGraph();
            RefreshWindow();
        }

        protected virtual void OnEnable()
        {
            TargetGraph = GetGraph();
            if (TargetGraph != null)
            {
                TargetGraph.SelectedNodesChanged += OnSelectedNodesChanged;
            }
            PopulateGraph();
            m_rectSelection = new RectSelection(this);

            Undo.undoRedoPerformed += RefreshWindow;
        }

        protected virtual void OnDisable()
        {
            Undo.undoRedoPerformed -= RefreshWindow;
        }

        protected virtual void OnInspectorUpdate()
        {
            m_forceRepaintCount--;
            m_forceRepaintCount = Math.Max(0, m_forceRepaintCount);
        }

        protected override void OnGUI()
        {
            base.OnGUI();

            bool shouldCenterViewOnGraph = ShouldForceCenterViewOnGraph();

            if (TargetGraph != GetGraph())
            {
                Graph oldGraph = TargetGraph;

                TargetGraph = GetGraph();

                PopulateGraph();

                if (oldGraph != null)
                {
                    oldGraph.SelectedNodesChanged -= OnSelectedNodesChanged;
                }

                if (TargetGraph != null)
                {
                    TargetGraph.SelectedNodesChanged += OnSelectedNodesChanged;
                }

                OnGraphChanged(oldGraph, TargetGraph);

                PopulateGraph();
                RefreshWindow();
            }

            if (shouldCenterViewOnGraph)
            {
                CenterViewOnGraph(true, true);
            }

            if (TargetGraph == null)
            {
                return;
            }

            if (TargetGraph.m_Nodes.Any(i => i.GetUnityObject() == null))
            {
                PopulateGraph();
            }
            
            foreach (Action scheduledAction in m_ScheduledActions.ToArray())
            {
                if (scheduledAction != null)
                {
                    scheduledAction();
                }
            }
            m_ScheduledActions.Clear();

            DrawGraphHeader(GetGraphTitle());
            GUILayout.FlexibleSpace();
            DrawGraphFooter();

            Rect graphViewRect = GetGraphViewRect();
            DrawGraphView(graphViewRect);
            DrawGraphOverlay(graphViewRect, GetGraphDescription());

            if (Event.current.type == EventType.ContextClick)
            {
                m_wasPanningGridWithRightClick = false;
            }

            ProcessKeyboardShortcuts();

            if (m_forceRepaintCount > 0)
            {
                // Redraw on next frame to get crisp refresh rate
                Repaint();
            }

            if (Event.current.type != EventType.Layout && Event.current.type != EventType.Repaint)
            {
                m_lastEventType = Event.current.type;
            }
        }

        protected virtual void Update()
        {
            if (IsAnimatingPan())
            {
                m_scrollPos = GetAnimatedScrollPosition();
                Repaint();
                if (m_scrollPosAnim == 1)
                {
                    StopAnimatedPan();
                }
            }
            
            if (IsAnimatingZoom())
            {
                float zoomLevel = GetAnimatedZoomLevel();
                zoomLevel = Mathf.Clamp(zoomLevel, MinZoomValue, MaxZoomValue);
                m_zoomLevel = zoomLevel;
                Repaint();
                if (m_zoomLevelAnim == 1)
                {
                    StopAnimatedZoom();
                }
            }
        }

        protected virtual void OnSelectedNodesChanged()
        {
            RefreshWindow();
        }
        
        protected virtual void DrawGraphHeader(string title)
        {
            if (TargetGraph == null)
            {
                return;
            }

            using (new EditorGUILayout.HorizontalScope(ToolbarStyle))
            {
                DrawToolbarContents();
            }

            using (new EditorGUILayout.HorizontalScope(TitlebarStyle))
            {
                DrawTitlebarContents(title);
            }

            // Eat all mouse events in header
            if (Event.current.type == EventType.MouseDown || 
                Event.current.type == EventType.MouseUp)
            {
                if (Event.current.mousePosition.y < GetHeaderHeight())
                {
                    Event.current.Use();
                }
            }
        }

        protected virtual void DrawGraphFooter()
        {
            if (TargetGraph == null)
            {
                return;
            }

            using (new EditorGUILayout.HorizontalScope(FooterStyle))
            {
                DrawFooterContents();
            }

            // Eat all click events in footer
            if (Event.current.type == EventType.MouseDown || 
                Event.current.type == EventType.MouseUp)
            {
                if (Event.current.mousePosition.y < GetHeaderHeight())
                {
                    Event.current.Use();
                }
            }
        }

        protected virtual void DrawGraphOverlay(Rect position, string description)
        {
            if (TargetGraph == null)
            {
                return;
            }

            DrawOverlayContents(position, description);
        }

        protected virtual void DrawToolbarContents()
        {
            if (CanCreateNodes)
            {
                DrawAddNodeControl();
            }

            DrawScaleGraphControl();

            DrawCenterGraphControl();

            DrawSearchControl();
        }

        protected virtual void DrawTitlebarContents(string title)
        {
            if (title.Length > 0)
            {
                GUIContent titleContent = new GUIContent(title);
                GUILayout.Label(titleContent, GraphNameStyle);
            }
        }

        protected virtual void DrawFooterContents()
        {
        }

        protected virtual void DrawOverlayContents(Rect position, string description)
        {
            position.x -= 2f;
            using (new GUI.ClipScope(position))
            {
                Rect pos = position;
                pos.position = Vector2.zero;
                
                float margin = 4f;

                pos.xMin += margin;
                pos.xMax -= margin;
                pos.yMin += margin;
                pos.yMax -= margin;
                
                if (description.Length > 0)
                {
                    GUIContent descriptionContent = new GUIContent(description);
                    Vector2 descriptionSize = GraphDescriptionStyle.CalcSize(descriptionContent);
                    float maxWidth = pos.width / 2;
                    float width = Math.Min(maxWidth, descriptionSize.x + 1);
                    float height = GraphDescriptionStyle.CalcHeight(descriptionContent, width);
                    Rect descriptionPosition = pos;
                    descriptionPosition.width = width;
                    descriptionPosition.height = height;

                    GUI.Label(descriptionPosition, descriptionContent, GraphDescriptionStyle);
                }
            }
        }

        protected virtual void DrawAddNodeControl()
        {
            GUILayout.Space(2);

            if (GUILayout.Button(new GUIContent(AddButtonIcon, "Create New " + NodeTypeName()), AddButtonStyle))
            {
                OnClickAddButton();
            }

            GUILayout.Label("", EditorStyles.toolbarButton, GUILayout.Width(8)); // Separator
        }

        protected virtual void OnClickAddButton()
        {
            INode newNode = CreateNode(GetNewNodePositionInGraph());
            OnNodeAddedToGraph(newNode);
        }

        protected virtual void DrawScaleGraphControl()
        {
            GUILayout.Label("Scale", EditorStyles.miniLabel);
            float zoomLevel = m_zoomLevel;
            EditorGUI.BeginChangeCheck();
            zoomLevel = GUILayout.HorizontalSlider(zoomLevel, MinZoomValue, MaxZoomValue, GUILayout.Width(100));
            if (EditorGUI.EndChangeCheck())
            {
                StopAnimatedZoom();
                zoomLevel = Mathf.Clamp(zoomLevel, MinZoomValue, MaxZoomValue);
                if (m_zoomLevel != zoomLevel)
                {
                    m_zoomLevel = zoomLevel;
                }
            }
            GUILayout.Label(m_zoomLevel.ToString("0.0#x"), EditorStyles.miniLabel, GUILayout.Width(30));
        }

        protected virtual void DrawCenterGraphControl()
        {
            if (GUILayout.Button("Center", EditorStyles.toolbarButton))
            {
                CenterViewOnGraph(true);
            }
        }

        protected virtual void DrawSearchControl()
        {
            GUILayout.FlexibleSpace();

            Rect searchRect = EditorGUILayout.GetControlRect(GUILayout.Width(150));

            object[] values = TargetGraph.m_Nodes.ToArray();
            GUIContent[] displayOptions = TargetGraph.m_Nodes.Select(x => new GUIContent(x.GetNodeName(), GetNodeStyle(x, TargetGraph.IsNodeSelected(x)).normal.background)).ToArray();
            SearchPopupWindowFlags flags = SearchPopupWindowFlags.HideSearch | SearchPopupWindowFlags.HideCreateNewOption | SearchPopupWindowFlags.HideDefaultOption;
            
            int controlId = SearchPopupGUI.GenericToolbarSearchPopupField(searchRect, GUIContent.none, null, values, displayOptions, ref m_searchString, flags);

            SearchPopupWindow.ProcessSearchedItems(controlId, OnNodesFocused);
            SearchPopupWindow.ProcessSelectedItem(controlId, OnNodeFocused);
        }

        public virtual void FocusNodes(INode[] nodes)
        {
            m_focusedNodes = nodes;
            if (nodes != null)
            {
                TargetGraph.SelectNodes(nodes, Graph.SelectionType.Normal);
                CenterViewOnNodes(nodes, true);
            }
            else
            {
                TargetGraph.ClearSelectedNodes();
            }
        }

        protected virtual void OnNodesFocused(object[] objs)
        {
            if (objs != null)
            {
                INode[] nodes = objs.Cast<INode>().ToArray();
                FocusNodes(nodes);
            }
            else
            {
                FocusNodes(null);
            }
        }

        protected virtual void OnNodeFocused(object obj)
        {
            INode node = obj as INode;
            if (node != null)
            {
                TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
                CenterViewOnNode(node, m_zoomLevel);
            }
        }

        protected virtual void OnGraphChanged(Graph oldGraph, Graph newGraph)
        {
        }

        protected virtual string NodeTypeName()
        {
            return "Node";
        }

        protected virtual bool CanDeleteNode(INode node)
        {
            return true;
        }

        protected virtual bool CanMoveNode(INode node)
        {
            return true;
        }

        protected virtual bool CanRenameNode(INode node)
        {
            return true;
        }

        protected virtual void OnLeftClickNode(INode node)
        {
            if (Event.current.shift)
            {
                if (!TargetGraph.GetSelectedNodes().Contains(node))
                {
                    TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Additive);
                }
            }
            else if (EditorGUI.actionKey)
            {
                if (TargetGraph.GetSelectedNodes().Contains(node))
                {
                    TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Subtractive);
                }
                else
                {
                    TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Additive);
                }
            }
            else
            {
                if (!TargetGraph.GetSelectedNodes().Contains(node))
                {
                    TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
                }
                else
                {
                    m_clickedAlreadySelectedElementOnMouseDown = true;
                }
            }

            GUIUtility.keyboardControl = 0; // Fix for textarea not refeshing (change focus)

        }

        protected virtual void OnLeftClickGrid()
        {
            TargetGraph.ClearSelectedNodes();
        }

        protected virtual void OnRightClickNode(INode node)
        {
            if (!TargetGraph.IsNodeSelected(node))
            {
                TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
            }
        }

        protected virtual void OnContextClickNodes()
        {
            bool showDelete = (TargetGraph.GetSelectedNodes().Length > 0 &&
                TargetGraph.GetSelectedNodes().ToList().Find(i => CanDeleteNode(i)) != null);

            GenericMenu menu = new GenericMenu();

            if (showDelete)
            {
                menu.AddItem(new GUIContent("Delete"), false, DeleteMenuFunction);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent("Delete"));
            }

            menu.ShowAsContext();
        }

        protected virtual void OnContextClickGrid()
        {
            if (CanCreateNodes)
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Create"), false, CreateMenuFunction, GetNewNodePositionAtMouse());

                menu.ShowAsContext();
            }
        }

        protected virtual void ProcessKeyboardShortcuts()
        {
            if (GUIUtility.keyboardControl == 0) //Only call keyboard shortcuts when not typing in a text field
            {
                Event e = Event.current;

                // Delete keyboard shortcut
                if (e.type == EventType.ValidateCommand && (e.commandName == "SoftDelete" || e.commandName == "Delete"))
                {
                    if (TargetGraph.GetSelectedNodes().Length > 0)
                    {
                        e.Use();
                    }
                }

                if (e.type == EventType.ExecuteCommand && (e.commandName == "SoftDelete" || e.commandName == "Delete"))
                {
                    DeleteMenuFunction();
                    if (e.type != EventType.Layout)
                    {
                        e.Use();
                    }
                }

                // SelectAll keyboard shortcut
                if (e.type == EventType.ValidateCommand && e.commandName == "SelectAll")
                {
                    e.Use();
                }

                if (e.type == EventType.ExecuteCommand && e.commandName == "SelectAll")
                {
                    TargetGraph.SelectNodes(TargetGraph.m_Nodes.ToArray(), Graph.SelectionType.Normal);
                    e.Use();
                }
            }
        }

        protected virtual void OnNodeNameChanged(INode node)
        {
        }

        protected virtual void OnNodeNameEndEdit(INode node)
        {
        }

        protected virtual void OnNodeMoveEnd(INode node)
        {
        }

        protected virtual void OnNodeAddedToGraph(INode node)
        {
            FocusOnNodeNameTextField(node);
        }

        protected virtual float GetHeaderHeight()
        {
            return 40f;
        }

        protected virtual float GetFooterHeight()
        {
            return 0f;
        }

        protected Rect GetGraphViewRect()
        {
            return new Rect(2, GetHeaderHeight() + 2, this.position.width, (this.position.height - GetHeaderHeight() - GetFooterHeight() - 1));
        }
        
        protected Rect GetGraphViewZoomRect(float zoomLevel)
        {
            Rect zoomRect = GetGraphViewRect();
            zoomRect.width /= zoomLevel;
            zoomRect.height /= zoomLevel;
            return zoomRect;
        }

        private void BeginZoomArea()
        {
            EditorZoomArea.Begin(m_zoomLevel, GetGraphViewZoomRect(m_zoomLevel));
        }

        private void EndZoomArea()
        {
            EditorZoomArea.End();
        }

        protected virtual void DrawGraphView(Rect position)
        {
            if (m_rectSelection == null)
            {
                m_rectSelection = new RectSelection(this);
            }

            if (m_isPanning || Event.current.alt)
            {
                EditorGUIUtility.AddCursorRect(position, MouseCursor.Pan);
            }

            BeginZoomArea();

            Rect rect = GetGraphViewZoomRect(m_zoomLevel);
            rect.x = 0;
            rect.y = 0;

            using (new GUI.ClipScope(rect))
            {
                DrawGrid();

                DrawNodes();

                PanAndZoom();

                if (!Event.current.alt && !m_isDraggingNode)
                {
                    m_rectSelection.OnGUI();
                }
            }

            EndZoomArea();
        }

        private void DrawNodes()
        {
            DrawConnectionArrowsAndDescriptions();

            GUIStyle windowStyle = new GUIStyle();
            windowStyle.stretchHeight = true;

            BeginWindows();
            
            m_windowNodeMap.Clear();
            for (int i = 0; i < TargetGraph.m_Nodes.Count; ++i)
            {
                INode node = TargetGraph.m_Nodes[i];

                if (node.GetUnityObject() == null)
                {
                    break;
                }

                if (m_dragWindowId == i)
                {
                    HandleMoveNode(node);
                }

                Rect windowRect = new Rect(GetNodeRect(node));
                windowRect.x += m_scrollPos.x;
                windowRect.y += m_scrollPos.y;
                
                GUILayout.Window(i, windowRect, DrawNodeWindow, "", windowStyle);

                GUI.backgroundColor = Color.white;

                m_windowNodeMap.Add(node);
            }

            EndWindows();

            foreach (INode node in TargetGraph.m_Nodes)
            {
                if (node.GetUnityObject() == null)
                {
                    break;
                }

                DrawNodeTopLabel(node, NodeTopLabel(node));

                if (Application.isPlaying)
                {
                    DrawExecutionIcon(node);
                }
            }

            // Keep view centered on the node that is currently executing
            if (Application.isPlaying)
            {
                INode[] currentlyExecutingNodes = TargetGraph.GetExecutingNodes();
                // If the list of blocks that are currently executing has changed
                if (m_executingNodes != null && !currentlyExecutingNodes.SequenceEqual(m_executingNodes))
                {
                    if (TargetGraph.GetFirstSelectedNode() != null)
                    {
                        // If first selected block is not executing
                        if (!TargetGraph.GetFirstSelectedNode().IsExecuting())
                        {
                            // Select and center on the first currently executing node
                            foreach (INode n in currentlyExecutingNodes)
                            {
                                if (n.GetUnityObject() == null)
                                {
                                    break;
                                }

                                if (n.IsExecuting())
                                {
                                    TargetGraph.SelectNodes(new INode[] { n }, Graph.SelectionType.Normal);
                                    if (!m_mouseDownWasOnGrid || !m_rectSelection.IsSelecting)
                                    {
                                        CenterViewOnNode(n, m_zoomLevel);
                                    }
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (!m_mouseDownWasOnGrid || !m_rectSelection.IsSelecting)
                            {
                                CenterViewOnNode(TargetGraph.GetFirstSelectedNode(), m_zoomLevel);
                            }
                        }
                    }
                }

                m_executingNodes = currentlyExecutingNodes;
            }

            if (Event.current.button == 0 &&
                    Event.current.type == EventType.MouseDown)
            {
                if (!m_mouseDownWasOnNode)
                {
                    m_mouseDownWasOnGrid = true;
                    m_mouseDownWasOnNode = false;
                    if (!EditorGUI.actionKey && !Event.current.shift)
                    {
                        OnLeftClickGrid();
                        GUI.FocusControl("");
                    }
                }
            }
            else if (Event.current.type == EventType.ContextClick)
            {
                if (!m_wasPanningGridWithRightClick)
                {
                    if (TargetGraph.GetSelectedNodes().Length == 0)
                    {
                        OnContextClickGrid();
                    }
                    else
                    {
                        OnContextClickNodes();
                    }
                }
            }

            if (Event.current.button == 0 &&
               Event.current.type == EventType.MouseUp)
            {
                m_mouseDownWasOnNode = false;
                m_mouseDownWasOnGrid = false;
            }
        }

        private void HandleMoveNode(INode node)
        {
            if (Event.current.button == 0)
            {
                if (Event.current.type == EventType.MouseDrag)
                {
                    m_isDraggingNode = true;

                    for (int ni = 0; ni < TargetGraph.GetSelectedNodes().Length; ni++)
                    {
                        INode n = TargetGraph.GetSelectedNodes()[ni];
                        if (CanMoveNode(n))
                        {
                            Vector2 np = n.GetNodePosition();
                            Vector2 offset = (ni >= 0 && ni < m_offsetsFromDraggedNode.Count) ? m_offsetsFromDraggedNode[ni] : Vector2.zero;
                            np.x = ((int)(Event.current.mousePosition.x - m_scrollPos.x - m_startMousePosition.x) / SnapSize) * SnapSize - offset.x;
                            np.y = ((int)(Event.current.mousePosition.y - m_scrollPos.y - m_startMousePosition.y) / SnapSize) * SnapSize - offset.y;
                            float width = GetNodeWidth(node);
                            float height = GetNodeHeight(node);
                            np.x += width / 2;
                            np.y -= height / 2;
                            n.SetNodePosition(np);
                        }
                    }

                    m_forceRepaintCount = 6;
                }
                else if (Event.current.type == EventType.MouseUp)
                {
                    for (int ni = 0; ni < TargetGraph.GetSelectedNodes().Length; ni++)
                    {
                        INode n = TargetGraph.GetSelectedNodes()[ni];

                        UnityEngine.Object obj = n.GetUnityObject();

                        if (m_isDraggingNode && CanMoveNode(n) && obj != null)
                        {
                            Vector2 oldPos = m_startDragPosition - m_offsetsFromDraggedNode[ni];
                            Vector2 newPos = n.GetNodePosition();

                            if (newPos != oldPos)
                            {
                                n.SetNodePosition(oldPos);

                                Undo.RecordObject(obj, "Node Position");

                                n.SetNodePosition(newPos);

                                Undo.RecordObject(obj, "Node Position");

                                GameObject nodeGameObject = obj as GameObject;
                                if (nodeGameObject == null)
                                {
                                    Component nodeComponent = obj as Component;
                                    if (nodeComponent != null)
                                    {
                                        nodeGameObject = nodeComponent.gameObject;
                                    }
                                }

                                if (nodeGameObject == null || !nodeGameObject.scene.IsValid())
                                {
                                    EditorUtility.SetDirty(obj);
                                    OnNodeMoveEnd(node);
                                }
                            }
                        }
                    }

                    m_dragWindowId = -1;
                    m_forceRepaintCount = 6;
                    m_mouseDownWasOnNode = false;
                    m_mouseDownWasOnGrid = false;
                    m_isDraggingNode = false;
                }
            }
        }

        protected void DrawConnectionArrowsAndDescriptions()
        {
            Dictionary<KeyValuePair<INode, Connection>, int> switchConnectionNotHighlightedMap = new Dictionary<KeyValuePair<INode, Connection>, int>();
            Dictionary<KeyValuePair<INode, Connection>, int> switchConnectionHighlightedMap = new Dictionary<KeyValuePair<INode, Connection>, int>();
            Dictionary<KeyValuePair<INode, Connection>, int> activateConnectionMap = new Dictionary<KeyValuePair<INode, Connection>, int>();
            Dictionary<KeyValuePair<INode, Connection>, int> deactivateConnectionMap = new Dictionary<KeyValuePair<INode, Connection>, int>();

            foreach (INode node in TargetGraph.m_Nodes)
            {
                int connectionButtonIndex = 0;

                foreach (Connection connection in GetNodeCachedSwitchConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedSwitchConnectionMap(node)[connection].ToArray();
                        bool highlight = ShouldHighlightConnection(connectNodesWithSameConnection, connection);
                        bool shouldDrawSwitchConnectionButton = ShouldDrawSwitchConnectionButton(connection);

                        KeyValuePair<INode, Connection> mapKey = new KeyValuePair<INode, Connection>(node, connection);
                        int mapValue = shouldDrawSwitchConnectionButton ? connectionButtonIndex : -1;

                        if (highlight)
                        {
                            switchConnectionHighlightedMap[mapKey] = mapValue;
                        }
                        else
                        {
                            switchConnectionNotHighlightedMap[mapKey] = mapValue;
                        }

                        if (shouldDrawSwitchConnectionButton)
                        {
                            connectionButtonIndex++;
                        }
                    }
                }

                foreach (Connection connection in GetNodeCachedActivateConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        KeyValuePair<INode, Connection> mapKey = new KeyValuePair<INode, Connection>(node, connection);
                        int mapValue = connectionButtonIndex;
                        
                        activateConnectionMap.Add(mapKey, mapValue);
                        connectionButtonIndex++;
                    }
                }

                foreach (Connection connection in GetNodeCachedDeactivateConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        KeyValuePair<INode, Connection> mapKey = new KeyValuePair<INode, Connection>(node, connection);
                        int mapValue = connectionButtonIndex;

                        deactivateConnectionMap.Add(mapKey, mapValue);
                        connectionButtonIndex++;
                    }
                }
            }

            DrawConnectionArrowsAndDescriptions(switchConnectionNotHighlightedMap, true, false);

            DrawConnectionArrowsAndDescriptions(switchConnectionHighlightedMap, true, true);

            DrawConnectionArrowsAndDescriptions(activateConnectionMap, false, false);

            DrawConnectionArrowsAndDescriptions(deactivateConnectionMap, false, false);
        }
        
        protected void DrawNodeTopLabel(INode node, GUIContent nodeTopLabel)
        {
            if (!string.IsNullOrEmpty(nodeTopLabel.text))
            {
                Rect nodeRect = GetNodeRect(node);
                Rect rect = new Rect(nodeRect);
                rect.height = GraphWindowStyles.NodeTopLabel.CalcHeight(nodeTopLabel, nodeRect.width);
                rect.x += m_scrollPos.x;
                rect.y += m_scrollPos.y - rect.height;

                GUI.Label(rect, nodeTopLabel, GraphWindowStyles.NodeTopLabel);
            }
        }

        public bool MouseIsOverNode(INode node)
        {
            Rect nodeRect = GetNodeRect(node);

            Rect windowRect = new Rect(nodeRect);
            windowRect.x += m_scrollPos.x;
            windowRect.y += m_scrollPos.y;

            return (windowRect.Contains(Event.current.mousePosition));
        }

        public bool MouseIsOverAnyNode()
        {
            foreach (INode node in TargetGraph.m_Nodes)
            {
                if (MouseIsOverNode(node))
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual void DrawExecutionIcon(INode node)
        {
            if (node.IsExecuting())
            {
                node.SetNodeExecutingIconTimer(Time.realtimeSinceStartup + EXECUTING_ICON_FADE_TIME);
                m_forceRepaintCount = 6;
            }

            if (node.GetNodeExecutingIconTimer() > Time.realtimeSinceStartup)
            {
                Rect rect = new Rect(GetNodeRect(node));

                rect.x += m_scrollPos.x - 37;
                rect.y += m_scrollPos.y + 3;
                rect.width = 34;
                rect.height = 34;

                if (!node.IsExecuting())
                {
                    float alpha = (node.GetNodeExecutingIconTimer() - Time.realtimeSinceStartup) / EXECUTING_ICON_FADE_TIME;
                    alpha = Mathf.Clamp01(alpha);
                    GUI.color = new Color(1f, 1f, 1f, alpha);
                }

                if (GUI.Button(rect, GraphWindowStyles.PlayBigIcon, new GUIStyle()))
                {
                    TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
                }

                GUI.color = Color.white;
            }
        }

        protected Rect GetNodeRect(INode node)
        {
            Vector2 position = node.GetNodePosition();
            float width = GetNodeWidth(node);
            float height = GetNodeHeight(node);
            position.x -= width / 2;
            position.y += height / 2;
            return new Rect(position, new Vector2(width, height));
        }
        
        protected virtual float GetNodeWidth(INode node)
        {
            if (!m_nodeWidestLabelStyle.ContainsKey(node) || !m_nodeWidestLabelContent.ContainsKey(node))
            {
                // Make sure node is wide enough to fit the node name text and connection node text
                GUIStyle widestLabelStyle = GetNodeStyle(node, ShouldHighlightNode(node));
                GUIContent widestLabelContent = new GUIContent(node.GetNodeName());
                float maxWidth = GetSnappableWidth(widestLabelStyle, widestLabelContent, NodeMinWidth);
                
                foreach (Connection connection in GetNodeCachedSwitchConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        if (ShouldDrawSwitchConnectionButton(connection))
                        {
                            IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedSwitchConnectionMap(node)[connection].ToArray();
                            bool highlightConnection = ShouldHighlightConnection(connectNodesWithSameConnection, connection);
                            string connectedNodeName = GetConnectionButtonName(connectNodesWithSameConnection, connection);
                            GUIStyle currWidestLabelStyle = GetConnectionNodeStyle(connection, highlightConnection, true);
                            GUIContent currWidestLabelContent = new GUIContent(connectedNodeName);
                            float currMaxWidth = GetSnappableWidth(widestLabelStyle, widestLabelContent, NodeMinWidth);
                            if (currMaxWidth > maxWidth)
                            {
                                widestLabelStyle = currWidestLabelStyle;
                                widestLabelContent = currWidestLabelContent;
                                maxWidth = currMaxWidth;
                            }
                        }
                    }
                }

                foreach (Connection connection in GetNodeCachedActivateConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedActivateConnectionMap(node)[connection].ToArray();
                        bool highlightConnection = ShouldHighlightConnection(connectNodesWithSameConnection, connection);
                        string connectedNodeName = GetConnectionButtonName(connectNodesWithSameConnection, connection);
                        GUIStyle currWidestLabelStyle = GetConnectionNodeStyle(connection, highlightConnection, true);
                        GUIContent currWidestLabelContent = new GUIContent(connectedNodeName);
                        float currMaxWidth = GetSnappableWidth(widestLabelStyle, widestLabelContent, NodeMinWidth);
                        if (currMaxWidth > maxWidth)
                        {
                            widestLabelStyle = currWidestLabelStyle;
                            widestLabelContent = currWidestLabelContent;
                            maxWidth = currMaxWidth;
                        }
                    }
                }

                foreach (Connection connection in GetNodeCachedDeactivateConnectionMap(node).Keys)
                {
                    if (ShouldDrawConnection(connection))
                    {
                        IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedDeactivateConnectionMap(node)[connection].ToArray();
                        bool highlightConnection = ShouldHighlightConnection(connectNodesWithSameConnection, connection);
                        string connectedNodeName = GetConnectionButtonName(connectNodesWithSameConnection, connection);
                        GUIStyle currWidestLabelStyle = GetConnectionNodeStyle(connection, highlightConnection, true);
                        GUIContent currWidestLabelContent = new GUIContent(connectedNodeName);
                        float currMaxWidth = GetSnappableWidth(widestLabelStyle, widestLabelContent, NodeMinWidth);
                        if (currMaxWidth > maxWidth)
                        {
                            widestLabelStyle = currWidestLabelStyle;
                            widestLabelContent = currWidestLabelContent;
                            maxWidth = currMaxWidth;
                        }
                    }
                }

                m_nodeWidestLabelStyle[node] = widestLabelStyle;
                m_nodeWidestLabelContent[node] = widestLabelContent;
            }
            
            return GetSnappableWidth(m_nodeWidestLabelStyle[node], m_nodeWidestLabelContent[node], NodeMinWidth);
        }


        protected virtual float GetNodeHeight(INode node)
        {
            return NodeMinHeight;
        }

        protected virtual GUIContent NodeTopLabel(INode node)
        {
            return GUIContent.none;
        }

        protected virtual GUIStyle GetConnectionNodeStyle(Connection connection, bool highlight, bool activate)
        {
            GUIStyle style = null;

            if (highlight)
            {
                style = GraphWindowStyles.NodeSmallGreen;
            }
            else if (activate)
            {
                style = GraphWindowStyles.NodeSmallBlue;
            }
            else
            {
                style = GraphWindowStyles.NodeSmallGrey;
            }

            return style;
        }

        protected virtual void DrawNodeWindow(int windowId)
        {
            INode node = null;

            if (windowId >= 0 && windowId < m_windowNodeMap.Count)
            {
                node = m_windowNodeMap[windowId];
            }

            if (node == null || node.GetUnityObject() == null)
            {
                return;
            }

            if (Event.current.button == 0 &&
                Event.current.type == EventType.MouseDown)
            {
                // Check if might be start of a node window drag
                if (!Event.current.alt)
                {
                    if (windowId < m_windowNodeMap.Count)
                    {
                        OnLeftClickNode(node);
                        m_mouseDownWasOnNode = true;
                        m_mouseDownWasOnGrid = false;
                    }

                    m_dragWindowId = windowId;
                    m_startDragPosition = node.GetNodePosition();
                    m_startMousePosition = Event.current.mousePosition;
                    m_offsetsFromDraggedNode.Clear();
                    foreach (INode n in TargetGraph.GetSelectedNodes())
                    {
                        m_offsetsFromDraggedNode.Add(node.GetNodePosition() - n.GetNodePosition());
                    }
                }
            }
            else if (Event.current.button == 1 &&
                     Event.current.type == EventType.MouseDown)
            {
                OnRightClickNode(node);
            }
            else if (Event.current.button == 0 &&
                     Event.current.type == EventType.MouseUp)
            {
                if (windowId < m_windowNodeMap.Count)
                {
                    if (!m_isDraggingNode && (!m_mouseDownWasOnGrid || !m_rectSelection.IsSelecting) && !EditorGUI.actionKey && !Event.current.shift && !Event.current.alt)
                    {
                        if (m_clickedAlreadySelectedElementOnMouseDown)
                        {
                            TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
                        }
                    }
                }
                m_clickedAlreadySelectedElementOnMouseDown = false;
            }

            GUIStyle nodeStyle = GetNodeStyle(node, ShouldHighlightNode(node));

            Color originalColor = GUI.backgroundColor;

            GUI.backgroundColor = GetNodeColor(node);

            string textFieldControlName = GetNodeTextFieldControlName(node);
            if (CanRenameNode(node) &&
               (GUI.GetNameOfFocusedControl() == textFieldControlName && Event.current.keyCode == KeyCode.Return))
            {
                GUI.FocusControl("");
            }
            else if (CanRenameNode(node) && node != null &&
                    ((GUI.GetNameOfFocusedControl() == textFieldControlName) ||
                    (Event.current.type == EventType.MouseDown && Event.current.clickCount == 2) ||
                    (TargetGraph.GetSelectedNodes().Length == 1 && TargetGraph.IsNodeSelected(node) && Event.current.keyCode == KeyCode.F2) ||
                    m_shouldFocusOn == node))
            {
                if (m_shouldFocusOn != null)
                {
                    m_shouldFocusOn = null;
                }

                GUI.SetNextControlName(textFieldControlName);

                NodeNameTextField(node);

                if (GUI.changed)
                {
                    EditorUtility.SetDirty(node.GetUnityObject());
                    OnNodeNameChanged(node);
                }

                EditorGUI.FocusTextInControl(textFieldControlName);
                m_wasFocusedOn = textFieldControlName;
            }
            else
            {
                if (m_wasFocusedOn == textFieldControlName)
                {
                    OnNodeNameEndEdit(node);
                    m_wasFocusedOn = null;
                }

                Rect nodeRect = GetNodeRect(node);
                GUILayout.Box(node.GetNodeName(), nodeStyle, GUILayout.Width(nodeRect.width), GUILayout.Height(nodeRect.height));
            }

            GUI.backgroundColor = originalColor;

            DrawNodeConnectionButtons(node);

            OnDrawNode(node);

            if (!string.IsNullOrEmpty(node.GetNodeDescription()))
            {
                GUIStyle descriptionStyle = new GUIStyle(EditorStyles.helpBox);
                descriptionStyle.wordWrap = true;
                GUILayout.Label(node.GetNodeDescription(), descriptionStyle);
            }
        }

        protected virtual void NodeNameTextField(INode node)
        {
            Rect nodeRect = GetNodeRect(node);
            GUIStyle nodeStyle = GetNodeStyle(node, ShouldHighlightNode(node));
            string newNodeName = node.GetNodeName();
            EditorGUI.BeginChangeCheck();
            newNodeName = GUILayout.TextField(newNodeName, nodeStyle, GUILayout.Width(nodeRect.width), GUILayout.Height(nodeRect.height));
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(node.GetUnityObject(), "Node Name");

                node.SetNodeName(newNodeName);
            }
        }

        protected string GetNodeTextFieldControlName(INode node)
        {
            int windowId = m_windowNodeMap.IndexOf(node);
            return "NodeTextField" + GetInstanceID() + windowId;
        }

        protected void FocusOnNodeNameTextField(INode node)
        {
            m_shouldFocusOn = node;
        }

        protected virtual void OnDrawNode(INode node)
        {
        }

        protected virtual GUIStyle GetNodeStyle(INode node, bool highlight)
        {
            if (!m_nodeStyles.ContainsKey(node))
            {
                GUIStyle nodeStyle = highlight ? GraphWindowStyles.NodeSquareRedOn : GraphWindowStyles.NodeSquareRedOff;
                
                m_nodeStyles[node] = nodeStyle;
            }

            return m_nodeStyles[node];
        }

        public virtual Texture2D GetNodeTexture(INode node, bool highlight)
        {
            return GetNodeStyle(node, highlight).normal.background;
        }

        protected virtual bool GetNodeError(INode node)
        {
            if (!m_nodeErrors.ContainsKey(node))
            {
                m_nodeErrors[node] = false;
            }

            return m_nodeErrors[node];
        }

        protected virtual Color GetNodeColor(INode node)
        {
            Color color = Color.white;
            if (m_focusedNodes != null && !m_focusedNodes.Contains(node))
            {
                color.r = 0.5f;
                color.g = 0.5f;
                color.b = 0.5f;
            }
            return color;
        }

        protected virtual IConnectNodes[] GetNodeConnections(INode node)
        {
            return node.GetNodeConnections();
        }

        protected virtual bool ShouldDrawConnection(Connection connection)
        {
            return connection.Node != connection.ConnectedNode;
        }

        protected virtual bool ShouldHighlightConnection(IConnectNodes[] connectNodesWithSameConnection, Connection connection)
        {
            return TargetGraph.IsNodeSelected(connection.Node);
        }

        protected virtual bool ShouldHighlightNode(INode node)
        {
            return TargetGraph.IsNodeSelected(node);
        }

        protected virtual bool ShouldDrawSwitchConnectionButton(Connection connection)
        {
            INode connectedNode = connection.ConnectedNode;
            return (connectedNode == null || connectedNode.Equals(null) || !IsNodeInThisGraph(connectedNode));
        }

        protected virtual void DrawNodeConnectionButtons(INode node)
        {
            foreach (Connection connection in GetNodeCachedSwitchConnectionMap(node).Keys)
            {
                if (ShouldDrawConnection(connection))
                {
                    IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedSwitchConnectionMap(node)[connection].ToArray();
                    bool highlight = ShouldHighlightConnection(connectNodesWithSameConnection, connection);

                    if (ShouldDrawSwitchConnectionButton(connection))
                    {
                        DrawNodeConnectionButton(node, connectNodesWithSameConnection, connection, highlight, true);
                    }
                }
            }
            
            foreach (Connection connection in GetNodeCachedActivateConnectionMap(node).Keys)
            {
                if (ShouldDrawConnection(connection))
                {
                    IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedActivateConnectionMap(node)[connection].ToArray();
                    bool highlight = ShouldHighlightConnection(connectNodesWithSameConnection, connection);

                    DrawNodeConnectionButton(node, connectNodesWithSameConnection, connection, highlight, true);
                }
            }
            
            foreach (Connection connection in GetNodeCachedDeactivateConnectionMap(node).Keys)
            {
                if (ShouldDrawConnection(connection))
                {
                    IConnectNodes[] connectNodesWithSameConnection = GetNodeCachedDeactivateConnectionMap(node)[connection].ToArray();
                    bool highlight = ShouldHighlightConnection(connectNodesWithSameConnection, connection);

                    DrawNodeConnectionButton(node, connectNodesWithSameConnection, connection, highlight, false);
                }
            }
        }

        protected void PopulateNodeConnectionMaps(INode node, Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> switchConnectionMap, Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> activateConnectionMap, Dictionary<INode, Dictionary<Connection, List<IConnectNodes>>> deactivateConnectionMap)
        {
            switchConnectionMap[node] = GetNodeSwitchConnectionMap(node);
            activateConnectionMap[node] = GetNodeActivateConnectionMap(node);
            deactivateConnectionMap[node] = GetNodeDeactivateConnectionMap(node);
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeCachedSwitchConnectionMap(INode node)
        {
            if (!m_switchConnectionMap.ContainsKey(node))
            {
                m_switchConnectionMap[node] = GetNodeSwitchConnectionMap(node);
            }

            return m_switchConnectionMap[node];
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeCachedActivateConnectionMap(INode node)
        {
            if (!m_activateConnectionMap.ContainsKey(node))
            {
                m_activateConnectionMap[node] = GetNodeActivateConnectionMap(node);
            }

            return m_activateConnectionMap[node];
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeCachedDeactivateConnectionMap(INode node)
        {
            if (!m_deactivateConnectionMap.ContainsKey(node))
            {
                m_deactivateConnectionMap[node] = GetNodeDeactivateConnectionMap(node);
            }

            return m_deactivateConnectionMap[node];
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeSwitchConnectionMap(INode node)
        {
            Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>> initialConnectionMap = new Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>>();
            foreach (IConnectNodes connectNodes in GetNodeConnections(node))
            {
                if (connectNodes != null && (connectNodes as UnityEngine.Object) != null)
                {
                    SerializedObject serializedObject = new SerializedObject(connectNodes.GetUnityObject());
                    string[] propertyPaths = connectNodes.GetSwitchNodePropertyPaths();
                    if (propertyPaths != null)
                    {
                        Connection[] connections = new Connection[propertyPaths.Length];
                        for (int i = 0; i < propertyPaths.Length; i++)
                        {
                            string propertyPath = propertyPaths[i];
                            INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                            Connection connection = new Connection(node, connectedNode, connectNodes.GetDescription());
                            connections[i] = connection;
                        }
                        AddConnectionsToConnectionMap(initialConnectionMap, connectNodes, connections);
                    }
                }
            }
            Dictionary<Connection, List<IConnectNodes>> connectionMapWithCombindedDescriptions = new Dictionary<Connection, List<IConnectNodes>>();
            CombineDescriptionsInConnectionMap(connectionMapWithCombindedDescriptions, initialConnectionMap);
            return connectionMapWithCombindedDescriptions;
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeActivateConnectionMap(INode node)
        {
            Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>> initialConnectionMap = new Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>>();
            foreach (IConnectNodes connectNodes in GetNodeConnections(node))
            {
                if (connectNodes != null && (connectNodes as UnityEngine.Object) != null)
                {
                    SerializedObject serializedObject = new SerializedObject(connectNodes.GetUnityObject());
                    string[] propertyPaths = connectNodes.GetActivateNodePropertyPaths();
                    if (propertyPaths != null)
                    {
                        Connection[] connections = new Connection[propertyPaths.Length];
                        for (int i = 0; i < propertyPaths.Length; i++)
                        {
                            string propertyPath = propertyPaths[i];
                            INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                            Connection connection = new Connection(node, connectedNode, connectNodes.GetDescription());
                            connections[i] = connection;
                        }
                        AddConnectionsToConnectionMap(initialConnectionMap, connectNodes, connections);
                    }
                }
            }
            Dictionary<Connection, List<IConnectNodes>> connectionMapWithCombindedDescriptions = new Dictionary<Connection, List<IConnectNodes>>();
            CombineDescriptionsInConnectionMap(connectionMapWithCombindedDescriptions, initialConnectionMap);
            return connectionMapWithCombindedDescriptions;
        }

        protected Dictionary<Connection, List<IConnectNodes>> GetNodeDeactivateConnectionMap(INode node)
        {
            Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>> initialConnectionMap = new Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>>();
            foreach (IConnectNodes connectNodes in GetNodeConnections(node))
            {
                if (connectNodes != null && (connectNodes as UnityEngine.Object) != null)
                {
                    SerializedObject serializedObject = new SerializedObject(connectNodes.GetUnityObject());
                    string[] propertyPaths = connectNodes.GetDeactivateNodePropertyPaths();
                    if (propertyPaths != null)
                    {
                        Connection[] connections = new Connection[propertyPaths.Length];
                        for (int i = 0; i < propertyPaths.Length; i++)
                        {
                            string propertyPath = propertyPaths[i];
                            INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                            Connection connection = new Connection(node, connectedNode, connectNodes.GetDescription());
                            connections[i] = connection;
                        }
                        AddConnectionsToConnectionMap(initialConnectionMap, connectNodes, connections);
                    }
                }
            }
            Dictionary<Connection, List<IConnectNodes>> connectionMapWithCombindedDescriptions = new Dictionary<Connection, List<IConnectNodes>>();
            CombineDescriptionsInConnectionMap(connectionMapWithCombindedDescriptions, initialConnectionMap);
            return connectionMapWithCombindedDescriptions;
        }

        protected void CombineDescriptionsInConnectionMap(Dictionary<Connection, List<IConnectNodes>> connectionMapWithCombinedDescriptions, Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>> connectionMap)
        {
            foreach (Connection connection in connectionMap.Keys)
            {
                KeyValuePair<List<IConnectNodes>, List<string>> pair = connectionMap[connection];
                List<IConnectNodes> connectNodes = pair.Key;
                List<string> descriptions = pair.Value;
                connection.Description = string.Join(" OR ", descriptions.Where(i => !string.IsNullOrEmpty(i)).Distinct().Select(i => string.Format("({0})", i)).ToArray());
                connectionMapWithCombinedDescriptions[connection] = connectNodes;
            }
        }

        protected void AddConnectionsToConnectionMap(Dictionary<Connection, KeyValuePair<List<IConnectNodes>, List<string>>> connectionMap, IConnectNodes connectNodes, Connection[] connections)
        {
            foreach (Connection connection in connections)
            {
                if (ShouldDrawConnection(connection))
                {
                    if (connectionMap.ContainsKey(connection))
                    {
                        connectionMap[connection].Key.Add(connectNodes);
                        connectionMap[connection].Value.Add(connection.Description);
                    }
                    else
                    {
                        connectionMap[connection] = new KeyValuePair<List<IConnectNodes>, List<string>>(new List<IConnectNodes>() { connectNodes }, new List<string> { connection.Description });
                    }
                }
            }
        }
        
        protected Rect GetConnectionNodeRect(INode node, int connectionNodeIndex)
        {
            Rect connectionNodeRect = new Rect(GetNodeRect(node));
            connectionNodeRect.height = ConnectionButtonHeight;
            connectionNodeRect.y += GetNodeRect(node).height + connectionNodeRect.height / 2f + connectionNodeRect.height * connectionNodeIndex;
            return connectionNodeRect;
        }

        protected virtual string GetConnectionButtonName(IConnectNodes[] connectNodesWithSameConnection, Connection connection)
        {
            INode connectedNode = connection.ConnectedNode;
            if (connectedNode != null && (connectedNode as UnityEngine.Object) != null)
            {
                string name = connectedNode.GetNodeName();
                if (!string.IsNullOrEmpty(name))
                {
                    return name;
                }
            }

            return string.Join(", ", connectNodesWithSameConnection.Select(i => GetConnectNodesButtonName(i)).Where(i => !string.IsNullOrEmpty(i)).Distinct().ToArray());
        }

        protected virtual string GetConnectNodesButtonName(IConnectNodes connectNodes)
        {
            return "";
        }

        protected virtual void DrawNodeConnectionButton(INode node, IConnectNodes[] connectNodesWithSameConnection, Connection connection, bool highlight, bool activate)
        {
            Rect connectionNodeRect = GetConnectionNodeRect(node, 0);
            INode connectedNode = connection.ConnectedNode;
            string connectionNodeName = GetConnectionButtonName(connectNodesWithSameConnection, connection);
            GUIStyle style = GetConnectionNodeStyle(connection, highlight, activate);
            style.active = GUI.skin.button.active;

            Rect buttonRect = GUILayoutUtility.GetRect(connectionNodeRect.width, connectionNodeRect.height);

            if (connectedNode != null && !connectedNode.Equals(null))
            {
                if (GUI.Button(buttonRect, connectionNodeName, style))
                {
                    OnClickConnectionButton(connectedNode);
                }
            }
            else
            {
                GUI.Box(buttonRect, connectionNodeName, style);
            }
        }

        protected virtual void OnClickConnectionButton(INode node)
        {
            GameObject nodeGameObject = node.GetUnityObject() as GameObject;
            if (nodeGameObject == null)
            {
                Component nodeComponent = node.GetUnityObject() as Component;
                if (nodeComponent != null)
                {
                    nodeGameObject = nodeComponent.gameObject;
                }
            }
            if (nodeGameObject != null)
            {
                Selection.activeGameObject = nodeGameObject;
            }
            TargetGraph = GetGraph();
            PopulateGraph();
            TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Normal);
            PerformCenterViewOnNode(node, m_zoomLevel);
        }
        
        protected virtual bool IsNodeInThisGraph(INode node)
        {
            return TargetGraph.m_Nodes.Contains(node);
        }

        protected void DrawConnectionArrowsAndDescriptions(Dictionary<KeyValuePair<INode, Connection>, int> connectionMap, bool drawArrowToConnectionButton, bool highlight)
        {
            foreach (KeyValuePair<INode, Connection> nodeAndConnection in connectionMap.Keys)
            {
                int connectionButtonIndex = connectionMap[nodeAndConnection];
                INode node = nodeAndConnection.Key;
                Connection connection = nodeAndConnection.Value;
                if (connectionButtonIndex >= 0)
                {
                    if (drawArrowToConnectionButton)
                    {
                        DrawConnectionButtonArrowAndDescription(node, connection, connectionButtonIndex, highlight);
                    }
                    else
                    {
                        DrawConnectionButtonDescription(node, connection, connectionButtonIndex);
                    }
                }
                else
                {
                    DrawConnectionLineArrowAndDescription(connection, highlight);
                }
            }
        }

        protected void DrawConnectionButtonDescription(INode node, Connection connection, int connectionButtonIndex)
        {
            Rect connectionNodeRect = GetConnectionNodeRect(node, connectionButtonIndex);
            
            Rect connectionDescriptionRect = new Rect(connectionNodeRect.position, GraphWindowStyles.ConnectionDescriptionLabel.CalcSize(new GUIContent(connection.Description)));
            connectionDescriptionRect.position += m_scrollPos;
            connectionDescriptionRect.x -= connectionDescriptionRect.width;
            connectionDescriptionRect.y -= connectionDescriptionRect.height / 2;
            GUI.Label(connectionDescriptionRect, connection.Description, GraphWindowStyles.ConnectionDescriptionLabel);
        }

        protected void DrawConnectionButtonArrowAndDescription(INode node, Connection connection, int connectionButtonIndex, bool highlight)
        {
            int pointIndex = 0;
            Vector2 pointA = GetNodeArrowConnectionPoints(GetNodeRect(node), GetNodeStyle(node, ShouldHighlightNode(node)))[pointIndex];
            Rect connectionNodeRect = GetConnectionNodeRect(node, connectionButtonIndex);
            Vector2 pointB = GetNodeArrowConnectionPoints(connectionNodeRect, GetConnectionNodeStyle(connection, highlight, true))[pointIndex];
            Vector2 pointAlignmentA = Vector2.left;
            Vector2 pointAlignmentB = Vector2.left;
            DrawLineArrowAndDescription(pointA, pointB, pointAlignmentA, pointAlignmentB, highlight, connection.Description);
        }

        protected virtual void DrawConnectionLineArrowAndDescription(Connection connection, bool highlight)
        {
            INode node = connection.Node;
            INode connectedNode = connection.ConnectedNode;

            Rect rectA = GetNodeRect(node);
            Rect rectB = GetNodeRect(connectedNode);

            Vector2[] pointsA = GetNodeArrowConnectionPoints(rectA, GetNodeStyle(node, ShouldHighlightNode(node)));
            Vector2[] pointsB = GetNodeArrowConnectionPoints(rectB, GetNodeStyle(connectedNode, ShouldHighlightNode(connectedNode)));

            Vector2 pointA = Vector2.zero;
            Vector2 pointB = Vector2.zero;
            float minDist = float.MaxValue;

            foreach (Vector2 a in pointsA)
            {
                foreach (Vector2 b in pointsB)
                {
                    float d = Vector2.Distance(a, b);
                    if (d < minDist)
                    {
                        pointA = a;
                        pointB = b;
                        minDist = d;
                    }
                }
            }

            Vector2 pointAlignmentA = GetConnectionPointAlignment(pointsA, pointA);
            Vector2 pointAlignmentB = GetConnectionPointAlignment(pointsB, pointB);

            DrawLineArrowAndDescription(pointA, pointB, pointAlignmentA, pointAlignmentB, highlight, connection.Description);
        }

        private Vector2 GetOffsetPointA(Vector2 pointA, Vector2 pointAlignmentA, float offset)
        {
            if (Math.Abs(pointAlignmentA.x) > 0)
            {
                pointA.y += offset;
            }
            if (Math.Abs(pointAlignmentA.y) > 0)
            {
                pointA.x += offset;
            }
            return pointA;
        }

        private Vector2 GetOffsetPointB(Vector2 pointB, Vector2 pointAlignmentB, float offset)
        {
            if (Math.Abs(pointAlignmentB.x) > 0)
            {
                pointB.y -= offset;
            }
            if (Math.Abs(pointAlignmentB.y) > 0)
            {
                pointB.x -= offset;
            }
            return pointB;
        }

        private Vector2 GetLineConnectionPoint(Vector2 point, Vector2 pointAlignment, float offset)
        {
            if (pointAlignment == Vector2.right)
            {
                point.x += offset;
            }
            if (pointAlignment == Vector2.left)
            {
                point.x -= offset;
            }
            if (pointAlignment == Vector2.up)
            {
                point.y += offset;
            }
            if (pointAlignment == Vector2.down)
            {
                point.y -= offset;
            }
            return point;
        }

        private Vector2 GetConnectionPointAlignment(Vector2[] points, Vector2 point)
        {
            if (points[0] == point)
            {
                return Vector2.left;
            }
            else if (points[1] == point)
            {
                return Vector2.down;
            }
            else if (points[2] == point)
            {
                return Vector2.up;
            }
            else if (points[3] == point)
            {
                return Vector2.right;
            }
            return Vector2.zero;
        }

        private Vector2 GetConnectionTangent(Vector2 point, Vector2 pointAlignment)
        {
            float curvyness = 30f;
            return point + pointAlignment * curvyness;
        }

        private static Vector2 GetPointOnCurve(Vector2 pointA, Vector2 pointB, Vector2 tangentA, Vector2 tangentB, float t)
        {
            float rt = 1 - t;
            float rtt = rt * t;
            return rt * rt * rt * pointA + 3 * rt * rtt * tangentA + 3 * rtt * t * tangentB + t * t * t * pointB;
        }

        protected void DrawLineArrowAndDescription(Vector2 pointA, Vector2 pointB, Vector2 pointAlignmentA, Vector2 pointAlignmentB, bool highlight, string description)
        {
            float connectionInOutOffset = ConnectionInOutOffset;
            float arrowWidth = ArrowSize;
            float lineWidth = LineWidth;

            pointA = GetOffsetPointA(pointA, pointAlignmentA, connectionInOutOffset);
            pointB = GetOffsetPointB(pointB, pointAlignmentB, connectionInOutOffset);

            Color color = ConnectionNormalColor;
            if (highlight)
            {
                color = ConnectionHighlightColor;
            }

            Handles.color = color;

            Vector2 outgoingLineStartPoint = GetLineConnectionPoint(pointA, pointAlignmentA, arrowWidth);
            Vector2 outgoingLineEndPoint = GetLineConnectionPoint(pointB, pointAlignmentB, arrowWidth);

            DrawLine(pointA, outgoingLineStartPoint, Vector2.zero, Vector2.zero, color, lineWidth);
            DrawLine(outgoingLineStartPoint, outgoingLineEndPoint, pointAlignmentA, pointAlignmentB, color, lineWidth);

            if (!string.IsNullOrEmpty(description))
            {
                DrawDescription(outgoingLineStartPoint, outgoingLineEndPoint, pointAlignmentA, pointAlignmentB, description);
            }

            DrawArrow(pointA, pointB, pointAlignmentA, pointAlignmentB, arrowWidth);

            Handles.color = Color.white;
        }

        protected virtual void DrawLine(Vector2 pointA, Vector2 pointB, Vector2 pointAlignmentA, Vector2 pointAlignmentB, Color color, float lineWidth)
        {
            Vector2 tangentA = GetConnectionTangent(pointA, pointAlignmentA);
            Vector2 tangentB = GetConnectionTangent(pointB, pointAlignmentB);

            Handles.DrawBezier(pointA, pointB, tangentA, tangentB, color, null, lineWidth);
        }
        
        protected virtual void DrawArrow(Vector2 pointA, Vector2 pointB, Vector2 pointAlignmentA, Vector2 pointAlignmentB, float size)
        {
            Vector2 tangentA = GetConnectionTangent(pointA, pointAlignmentA);
            Vector2 tangentB = GetConnectionTangent(pointB, pointAlignmentB);

            Vector2 position = GetPointOnCurve(pointA, pointB, tangentA, tangentB, 1f);
            Vector2 direction = pointAlignmentB;
            Vector2 perp = new Vector2(direction.y, -direction.x);
            Handles.DrawAAConvexPolygon(position, position + direction * size + perp * (size/2f), position + direction * size - perp * (size / 2f));
        }

        protected virtual void DrawDescription(Vector2 pointA, Vector2 pointB, Vector2 pointAlignmentA, Vector2 pointAlignmentB, string description)
        {
            Vector2 tangentA = GetConnectionTangent(pointA, pointAlignmentA);
            Vector2 tangentB = GetConnectionTangent(pointB, pointAlignmentB);

            Vector2 position = GetPointOnCurve(pointA, pointB, tangentA, tangentB, 0.25f);
            
            Rect connectionDescriptionRect = new Rect(position, GraphWindowStyles.ConnectionDescriptionLabel.CalcSize(new GUIContent(description)));

            float halfWidth = connectionDescriptionRect.width / 2f;
            float halfHeight = connectionDescriptionRect.height / 2f;

            connectionDescriptionRect.x -= halfWidth;
            connectionDescriptionRect.y -= halfHeight;
            
            float offset = DescriptionOffset;

            if (pointAlignmentA == Vector2.down && pointAlignmentB == Vector2.down)
            {
                connectionDescriptionRect.y -= halfHeight + offset;
            }
            if ((pointAlignmentA == Vector2.right && pointAlignmentB == Vector2.left))
            {
                connectionDescriptionRect.y += halfHeight + offset;
                connectionDescriptionRect.x += halfWidth + offset;
            }
            if (pointAlignmentA == Vector2.left && pointAlignmentB == Vector2.right)
            {
                connectionDescriptionRect.y += halfHeight + offset;
                connectionDescriptionRect.x -= halfWidth + offset;
            }
            if (pointAlignmentA == Vector2.up && pointAlignmentB == Vector2.up)
            {
                connectionDescriptionRect.y += halfHeight + offset;
            }

            if ((pointAlignmentA == Vector2.right && pointAlignmentB == Vector2.right) ||
                (pointAlignmentA == Vector2.up && pointAlignmentB == Vector2.down) ||
                (pointAlignmentA == Vector2.down && pointAlignmentB == Vector2.up) ||
                (pointAlignmentA == Vector2.left && pointAlignmentB == Vector2.up) ||
                (pointAlignmentA == Vector2.left && pointAlignmentB == Vector2.down) ||
                (pointAlignmentA == Vector2.right && pointAlignmentB == Vector2.up) ||
                (pointAlignmentA == Vector2.right && pointAlignmentB == Vector2.down) ||
                (pointAlignmentA == Vector2.down && pointAlignmentB == Vector2.left))
            {
                connectionDescriptionRect.x += halfWidth + offset;
            }

            if ((pointAlignmentA == Vector2.left && pointAlignmentB == Vector2.left) ||
                (pointAlignmentA == Vector2.down && pointAlignmentB == Vector2.right) ||
                (pointAlignmentA == Vector2.up && pointAlignmentB == Vector2.right))
            {
                connectionDescriptionRect.x -= halfWidth + offset;
            }

            if (pointAlignmentA == Vector2.up && pointAlignmentB == Vector2.left)
            {
                connectionDescriptionRect.x -= halfWidth + offset;
                connectionDescriptionRect.y += halfHeight + offset;
            }

            GUI.Label(connectionDescriptionRect, description, GraphWindowStyles.ConnectionDescriptionLabel);
        }
        
        public RectOffset GetArrowConnectionPointOffset(GUIStyle style)
        {
            if (style.normal.background == GraphWindowStyles.NodeHexGreyOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexGreyOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeHexBlueOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexBlueOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeHexPurpleOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexPurpleOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeHexYellowOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexYellowOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeHexOrangeOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexOrangeOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeHexRedOff.normal.background || style.normal.background == GraphWindowStyles.NodeHexRedOn.normal.background)
            {
                return new RectOffset(13, 13, 4, 4);
            }
            else if ((style.normal.background == GraphWindowStyles.NodeSquareGreyOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquareGreyOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeSquareBlueOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquareBlueOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeSquarePurpleOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquarePurpleOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeSquareYellowOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquareYellowOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeSquareOrangeOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquareOrangeOn.normal.background ||
                style.normal.background == GraphWindowStyles.NodeSquareRedOff.normal.background || style.normal.background == GraphWindowStyles.NodeSquareRedOn.normal.background))
            {
                return new RectOffset(4, 4, 4, 4);
            }
            else
            {
                return new RectOffset(0, 0, 4, 4);
            }
        }

        protected Vector2[] GetNodeArrowConnectionPoints(Rect rect, GUIStyle style)
        {
            rect.position += m_scrollPos;

            RectOffset offset = GetArrowConnectionPointOffset(style);
            
            return new Vector2[] {
                new Vector2(rect.xMin + offset.left, rect.center.y),         // left
                new Vector2(rect.xMin + rect.width / 2, rect.yMin + offset.top), // up
                new Vector2(rect.xMin + rect.width / 2, rect.yMax - offset.bottom), // down
                new Vector2(rect.xMax - offset.right, rect.center.y)          // right
            };
        }

        public virtual INode CreateNode(Vector2 position)
        {
            return null;
        }

        public virtual void DeleteNode(INode node)
        {
        }

        public void OnCreateNode(INode node)
        {
            if (!TargetGraph.m_Nodes.Contains(node))
            {
                TargetGraph.m_Nodes.Add(node);
            }
        }

        public void OnDeleteNode(INode node)
        {
            if (TargetGraph.m_Nodes.Contains(node))
            {
                TargetGraph.m_Nodes.Remove(node);
            }
        }

        protected virtual void CreateMenuFunction(object obj)
        {
            Vector2 newPosition = (Vector2)obj;

            INode newNode = CreateNode(newPosition);
            OnNodeAddedToGraph(newNode);
        }

        protected virtual void DeleteMenuFunction()
        {
            List<INode> deleteList = new List<INode>();
            foreach (INode node in TargetGraph.GetSelectedNodes())
            {
                deleteList.Add(node);
            }
            Action deleteNodesAction = () => DeleteNodes(deleteList);
            m_ScheduledActions.Add(deleteNodesAction);
        }

        protected virtual void DeleteNodes(List<INode> deleteList)
        {
            foreach (INode node in deleteList)
            {
                if (CanDeleteNode(node))
                {
                    if (TargetGraph.IsNodeSelected(node))
                    {
                        TargetGraph.SelectNodes(new INode[] { node }, Graph.SelectionType.Subtractive);
                    }
                }
            }
            foreach (INode node in deleteList)
            {
                if (CanDeleteNode(node))
                {
                    DeleteNode(node);
                }
            }
            deleteList.Clear();
        }

        public void CenterViewOnGraph(bool zoomToFit, bool instantly = false)
        {
            Action centerViewOnGraphAction = () => PerformCenterViewOnGraph(zoomToFit, instantly);
            m_ScheduledActions.Add(centerViewOnGraphAction);
        }

        private void PerformCenterViewOnGraph(bool zoomToFit, bool instantly = false)
        {
            PopulateGraph();
            PerformCenterViewOnNodes(TargetGraph.m_Nodes.ToArray(), zoomToFit, instantly);
        }

        public void CenterViewOnNodes(INode[] nodes, bool zoomToFit, bool instantly = false)
        {
            Action centerViewOnNodesAction = () => PerformCenterViewOnNodes(nodes, zoomToFit, instantly);
            m_ScheduledActions.Add(centerViewOnNodesAction);
        }

        private void PerformCenterViewOnNodes(INode[] nodes, bool zoomToFit, bool instantly = false)
        {
            if (nodes != null && nodes.Length > 0)
            {
                Rect firstNodeRect = GetNodeRect(nodes[0]);

                Vector2 min = firstNodeRect.min;
                Vector2 max = firstNodeRect.max;

                foreach (INode node in nodes)
                {
                    Rect nodeRect = GetNodeRect(node);

                    min.x = Mathf.Min(min.x, nodeRect.min.x);
                    min.y = Mathf.Min(min.y, nodeRect.min.y);
                    max.x = Mathf.Max(max.x, nodeRect.max.x);
                    max.y = Mathf.Max(max.y, nodeRect.max.y);
                }

                Rect rectEncompassingAllNodes = new Rect(min, new Vector2(Math.Abs(max.x - min.x), Math.Abs(max.y - min.y)));
                Rect windowRect = new Rect(min, GetGraphViewRect().size);

                float zoomTarget = m_zoomLevel;

                if (zoomToFit)
                {
                    float preferredZoomLevel = Math.Min((windowRect.size.x / rectEncompassingAllNodes.size.x), (windowRect.size.y / rectEncompassingAllNodes.size.y));
                    zoomTarget = Mathf.Clamp(preferredZoomLevel, MinZoomValue, MaxZoomValue);
                    if (instantly)
                    {
                        StopAnimatedZoom();
                        m_zoomLevel = zoomTarget;
                    }
                    else
                    {
                        StartAnimatedZoom(zoomTarget);
                    }
                }

                CenterViewOnPosition(rectEncompassingAllNodes.center, zoomTarget, instantly);
            }
        }

        public void CenterViewOnTopmostNode(float zoomLevel, bool instantly = false)
        {
            m_ScheduledActions.Add(() => PerformCenterViewOnTopmostNode(zoomLevel, instantly));
        }

        public void CenterViewOnBottommostNode(float zoomLevel, bool instantly = false)
        {
            m_ScheduledActions.Add(() => PerformCenterViewOnBottommostNode(zoomLevel, instantly));
        }

        public void CenterViewOnLeftmostNode(float zoomLevel, bool instantly = false)
        {
            m_ScheduledActions.Add(() => PerformCenterViewOnLeftmostNode(zoomLevel, instantly));
        }

        public void CenterViewOnRightmostNode(float zoomLevel, bool instantly = false)
        {
            m_ScheduledActions.Add(() => { PerformCenterViewOnRightmostNode(zoomLevel, instantly); });
        }

        public void CenterViewOnNode(INode node, float zoomLevel, bool instantly = false)
        {
            m_ScheduledActions.Add(() => PerformCenterViewOnNode(node, zoomLevel, instantly));
        }

        public void PerformCenterViewOnTopmostNode(float zoomLevel, bool instantly = false)
        {
            if (TargetGraph != null && TargetGraph.m_Nodes != null && TargetGraph.m_Nodes.Count > 0)
            {
                INode node = TargetGraph.m_Nodes.OrderBy(i => GetNodeRect(i).center.y).ThenBy(i => GetNodeRect(i).center.x).First();
                PerformCenterViewOnNode(node, zoomLevel, instantly);
            }
        }

        public void PerformCenterViewOnBottommostNode(float zoomLevel, bool instantly = false)
        {
            if (TargetGraph != null && TargetGraph.m_Nodes != null && TargetGraph.m_Nodes.Count > 0)
            {
                INode node = TargetGraph.m_Nodes.OrderByDescending(i => GetNodeRect(i).center.y).ThenBy(i => GetNodeRect(i).center.x).First();
                PerformCenterViewOnNode(node, zoomLevel, instantly);
            }
        }

        public void PerformCenterViewOnLeftmostNode(float zoomLevel, bool instantly = false)
        {
            if (TargetGraph != null && TargetGraph.m_Nodes != null && TargetGraph.m_Nodes.Count > 0)
            {
                INode node = TargetGraph.m_Nodes.OrderBy(i => GetNodeRect(i).center.x).ThenBy(i => GetNodeRect(i).center.y).First();
                PerformCenterViewOnNode(node, zoomLevel, instantly);
            }
        }

        public void PerformCenterViewOnRightmostNode(float zoomLevel, bool instantly = false)
        {
            if (TargetGraph != null && TargetGraph.m_Nodes != null && TargetGraph.m_Nodes.Count > 0)
            {
                INode node = TargetGraph.m_Nodes.OrderByDescending(i => GetNodeRect(i).center.x).ThenBy(i => GetNodeRect(i).center.y).First();
                PerformCenterViewOnNode(node, zoomLevel, instantly);
            }
        }

        public void PerformCenterViewOnNode(INode node, float zoomLevel, bool instantly = false)
        {
            StartAnimatedZoom(MaxZoomValue);
            CenterViewOnPosition(GetNodeRect(node).center, zoomLevel, instantly);
            m_forceRepaintCount = 6;
        }

        public void CenterViewOnPosition(Vector2 centerPos, float zoomLevel, bool instantly = false)
        {
            if (TargetGraph != null)
            {
                if (zoomLevel > 0)
                {
                    Rect zoomedRect = GetGraphViewZoomRect(zoomLevel);
                    Vector2 panTarget = new Vector2(-centerPos.x + (zoomedRect.width / 2), -centerPos.y + (zoomedRect.height / 2));
                    if (instantly)
                    {
                        StopAnimatedPan();
                        m_scrollPos = panTarget;
                    }
                    else
                    {
                        StartAnimatedPan(panTarget);
                    }
                }
            }
        }

        private void StartAnimatedPan(Vector2 target)
        {
            m_scrollPosAnim = 0;
            m_scrollPosAnimStartTime = EditorApplication.timeSinceStartup;
            m_scrollPosIsAnimating = true;
            m_scrollPosStart = m_scrollPos;
            m_scrollPosEnd = target;
        }

        private void StopAnimatedPan()
        {
            m_scrollPosIsAnimating = false;
        }

        private bool IsAnimatingPan()
        {
            return m_scrollPosIsAnimating;
        }

        private void StartAnimatedZoom(float target)
        {
            m_zoomLevelAnim = 0;
            m_zoomLevelAnimStartTime = EditorApplication.timeSinceStartup;
            m_zoomLevelIsAnimating = true;
            m_zoomLevelStart = m_zoomLevel;
            m_zoomLevelEnd = target;
        }

        private void StopAnimatedZoom()
        {
            m_zoomLevelIsAnimating = false;
        }

        private bool IsAnimatingZoom()
        {
            return m_zoomLevelIsAnimating;
        }

        private Vector2 GetAnimatedScrollPosition()
        {
            Vector2 start = m_scrollPosStart;
            Vector2 end = m_scrollPosEnd;
            Vector2 delta = end - start;

            float t = (float)(EditorApplication.timeSinceStartup - m_scrollPosAnimStartTime) / m_animDuration;
            m_scrollPosAnim = Mathf.SmoothStep(0f, 1f, t);

            return start + new Vector2(delta.x * m_scrollPosAnim, delta.y * m_scrollPosAnim);
        }

        private float GetAnimatedZoomLevel()
        {
            float start = m_zoomLevelStart;
            float end = m_zoomLevelEnd;
            float delta = end - start;

            float t = (float)(EditorApplication.timeSinceStartup - m_zoomLevelAnimStartTime) / m_animDuration;
            m_zoomLevelAnim = Mathf.SmoothStep(0f, 1f, t);

            return start + delta * m_zoomLevelAnim;
        }

        protected virtual void PanAndZoom()
        {
            bool zoom = false;
            
            // Right click to drag view
            bool drag = false;

            if (m_lastEventType != EventType.ContextClick)
            {
                // Right button drag
                if (Event.current.button == 1 && Event.current.type == EventType.MouseDrag)
                {
                    drag = true;
                    m_isPanning = true;
                    m_wasPanningGridWithRightClick = true;
                }

                // Alt + left mouse drag
                if (Event.current.alt &&
                    Event.current.button == 0 && Event.current.type == EventType.MouseDrag)
                {
                    drag = true;
                    m_isPanning = true;
                }
            }
            
            if (Event.current.type == EventType.MouseUp || !GetGraphViewZoomRect(m_zoomLevel).Contains(Event.current.mousePosition))
            {
                m_isPanning = false;
            }
            
            if (drag)
            {
                StopAnimatedPan();
                m_scrollPos += Event.current.delta;
                m_newNodeOffset = Vector2.zero;
                m_forceRepaintCount = 6;
            }

            // Zoom tool
            if (UnityEditor.Tools.current == Tool.View && UnityEditor.Tools.viewTool == ViewTool.Zoom &&
                Event.current.button == 0 && Event.current.type == EventType.MouseDrag)
            {
                zoom = true;
            }

            // Scroll wheel
            if (Event.current.type == EventType.ScrollWheel)
            {
                zoom = true;
            }

            if (zoom)
            {
                StopAnimatedZoom();
                float zoomLevel = m_zoomLevel - Event.current.delta.y * 0.01f;
                zoomLevel = Mathf.Clamp(zoomLevel, MinZoomValue, MaxZoomValue);
                m_zoomLevel = zoomLevel;
                m_forceRepaintCount = 6;
            }
        }

        protected virtual void DrawGrid()
        {
            int gridSize = GridSize;
            int snapSize = SnapSize;

            Rect graphZoomRect = GetGraphViewZoomRect(m_zoomLevel);
            float width = graphZoomRect.width;
            float height = graphZoomRect.height;
            
            GUI.color = GridBackgroundColor;
            GUI.DrawTexture(new Rect(0, 0, width, height), EditorGUIUtility.whiteTexture);

            GUI.color = Color.white;

            Handles.color = GridMinorLineColor;
            float x = m_scrollPos.x % snapSize;
            while (x < width)
            {
                Handles.DrawLine(new Vector2(x, 0), new Vector2(x, height));
                x += snapSize;
            }
            float y = (m_scrollPos.y % snapSize);
            while (y < height)
            {
                if (y >= 0)
                {
                    Handles.DrawLine(new Vector2(0, y), new Vector2(width, y));
                }
                y += snapSize;
            }

            Handles.color = GridMajorLineColor;
            x = m_scrollPos.x % gridSize;
            while (x < width)
            {
                Handles.DrawLine(new Vector2(x, 0), new Vector2(x, height));
                x += gridSize;
            }
            y = m_scrollPos.y % gridSize;
            while (y < height)
            {
                if (y >= 0)
                {
                    Handles.DrawLine(new Vector2(0, y), new Vector2(width, y));
                }
                y += gridSize;
            }

            Handles.color = Color.white;
        }
        
        public INode[] GetNodesInRect(Rect rect)
        {
            rect = new Rect(rect.position.x - m_scrollPos.x, rect.position.y - m_scrollPos.y, rect.width, rect.height);
            
            List<INode> nodesInRect = new List<INode>();
            foreach (INode node in TargetGraph.m_Nodes)
            {
                Rect nodeRect = GetNodeRect(node);
                if (rect.Overlaps(nodeRect))
                {
                    nodesInRect.Add(node);
                }
            }
            return nodesInRect.ToArray();
        }

        protected Vector2 GetNewNodePositionAtMouse()
        {
            int snapSize = SnapSize;

            Vector2 newNodePosition = Vector2.zero;
            newNodePosition.x = ((int)(Event.current.mousePosition.x - m_scrollPos.x) / snapSize) * snapSize;
            newNodePosition.y = ((int)(Event.current.mousePosition.y - m_scrollPos.y) / snapSize) * snapSize;
            return newNodePosition;
        }

        protected float GetSnappableWidth(GUIStyle nodeStyle, GUIContent label, float minWidth)
        {
            int snapSize = SnapSize;

            float nodeWidth = nodeStyle.CalcSize(label).x;
            nodeWidth = (int)(nodeWidth / snapSize) * snapSize + snapSize;
            return Mathf.Max(nodeWidth, minWidth);
        }
    }
}
#endif