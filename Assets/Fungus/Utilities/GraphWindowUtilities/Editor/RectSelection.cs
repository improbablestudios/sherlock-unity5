#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.GraphWindowUtilities
{
    public class RectSelection
    {
        private Vector2 m_selectStartPoint;

        private Vector2 m_selectMousePoint;

        private bool m_rectSelecting;
        
        private INode[] m_nodeSelectionStart;

        private Dictionary<INode, bool> m_lastNodeSelection;

        private INode[] m_currentNodeSelection;

        private GraphWindow m_graphWindow;

        private static int s_rectSelectionId = GUIUtility.GetControlID(FocusType.Passive);

        public bool IsSelecting
        {
            get
            {
                return m_rectSelecting;
            }
        }

        public RectSelection(GraphWindow window)
        {
            this.m_graphWindow = window;
        }

        public void OnGUI()
        {
            Event current = Event.current;
            Vector2 mousePosition = current.mousePosition;
            int num = RectSelection.s_rectSelectionId;
            EventType typeForControl = current.GetTypeForControl(num);
            switch (typeForControl)
            {
                case EventType.MouseDown:
                    if (GUIUtility.hotControl == 0)
                    {
                        if (current.button == 0)
                        {
                            GUIUtility.hotControl = num;
                            this.m_selectStartPoint = mousePosition;
                            this.m_nodeSelectionStart = m_graphWindow.TargetGraph.GetSelectedNodes();
                            this.m_rectSelecting = false;

                            current.Use();
                        }
                    }
                    break;
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == num)
                    {
                        GUIUtility.hotControl = 0;
                        if (current.button == 0)
                        {
                            if (this.m_rectSelecting)
                            {
                                EditorApplication.modifierKeysChanged = (EditorApplication.CallbackFunction)Delegate.Remove(EditorApplication.modifierKeysChanged, new EditorApplication.CallbackFunction(this.SendCommandsOnModifierKeys));
                                this.m_rectSelecting = false;
                                this.m_nodeSelectionStart = new INode[0];
                                current.Use();
                            }
                        }
                    }
                    break;
                case EventType.MouseMove:
                case EventType.KeyDown:
                case EventType.KeyUp:
                case EventType.ScrollWheel:
                    if (typeForControl != EventType.ExecuteCommand)
                    {
                        break;
                    }
                    if (num == GUIUtility.hotControl && current.commandName == "ModifierKeysChanged")
                    {
                        if (EditorGUI.actionKey)
                        {
                            INode[] existingSelection = this.m_nodeSelectionStart.Except(this.m_currentNodeSelection).ToArray();
                            INode[] newNodesToSelect = this.m_currentNodeSelection.Except(this.m_nodeSelectionStart).ToArray();
                            RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, existingSelection, newNodesToSelect, Graph.SelectionType.Additive, this.m_rectSelecting);
                        }
                        else if (current.shift)
                        {
                            RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, this.m_nodeSelectionStart, this.m_currentNodeSelection, Graph.SelectionType.Additive, this.m_rectSelecting);
                        }
                        else
                        {
                            RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, this.m_nodeSelectionStart, this.m_currentNodeSelection, Graph.SelectionType.Normal, this.m_rectSelecting);
                        }
                        current.Use();
                    }
                    break;
                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == num)
                    {
                        if (!this.m_rectSelecting && (mousePosition - this.m_selectStartPoint).magnitude > 6f)
                        {
                            EditorApplication.modifierKeysChanged = (EditorApplication.CallbackFunction)Delegate.Combine(EditorApplication.modifierKeysChanged, new EditorApplication.CallbackFunction(this.SendCommandsOnModifierKeys));
                            this.m_rectSelecting = true;
                            this.m_lastNodeSelection = null;
                            this.m_currentNodeSelection = null;
                        }
                        if (this.m_rectSelecting)
                        {
                            this.m_selectMousePoint = new Vector2(Mathf.Max(mousePosition.x, 0f), Mathf.Max(mousePosition.y, 0f));
                            INode[] array = m_graphWindow.GetNodesInRect(FromToRect(this.m_selectStartPoint, this.m_selectMousePoint));
                            this.m_currentNodeSelection = array;
                            bool flag = false;
                            if (this.m_lastNodeSelection == null)
                            {
                                this.m_lastNodeSelection = new Dictionary<INode, bool>();
                                flag = true;
                            }
                            flag |= (this.m_lastNodeSelection.Count != array.Length);
                            if (!flag)
                            {
                                Dictionary<INode, bool> dictionary = new Dictionary<INode, bool>(array.Length);
                                INode[] array2 = array;
                                for (int i = 0; i < array2.Length; i++)
                                {
                                    INode key = array2[i];
                                    dictionary.Add(key, false);
                                }
                                foreach (INode current2 in this.m_lastNodeSelection.Keys)
                                {
                                    if (!dictionary.ContainsKey(current2))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            if (flag)
                            {
                                this.m_lastNodeSelection = new Dictionary<INode, bool>(array.Length);
                                INode[] array3 = array;
                                for (int j = 0; j < array3.Length; j++)
                                {
                                    INode key2 = array3[j];
                                    this.m_lastNodeSelection.Add(key2, false);
                                }
                                if (array != null)
                                {
                                    if (EditorGUI.actionKey)
                                    {
                                        INode[] existingSelection = this.m_nodeSelectionStart.Except(array).ToArray();
                                        INode[] newNodesToSelect = array.Except(this.m_nodeSelectionStart).ToArray();
                                        RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, existingSelection, newNodesToSelect, Graph.SelectionType.Additive, this.m_rectSelecting);
                                    }
                                    else if (current.shift)
                                    {
                                        RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, this.m_nodeSelectionStart, array, Graph.SelectionType.Additive, this.m_rectSelecting);
                                    }
                                    else
                                    {
                                        RectSelection.UpdateSelection(this.m_graphWindow.TargetGraph, this.m_nodeSelectionStart, array, Graph.SelectionType.Normal, this.m_rectSelecting);
                                    }
                                }
                            }
                        }
                        current.Use();
                    }
                    break;
                case EventType.Repaint:
                    if (GUIUtility.hotControl == num && this.m_rectSelecting)
                    {
                        GUI.skin.GetStyle("selectionRect").Draw(FromToRect(this.m_selectStartPoint, this.m_selectMousePoint), GUIContent.none, false, false, false, false);
                    }
                    break;
            }
        }

        private static void UpdateSelection(Graph graph, INode[] existingSelection, INode newNodeToSelect, Graph.SelectionType type, bool isRectSelection)
        {
            INode[] newNodesToSelect;
            if (newNodeToSelect == null)
            {
                newNodesToSelect = new INode[0];
            }
            else
            {
                newNodesToSelect = new INode[]
                {
                    newNodeToSelect
                };
            }
            RectSelection.UpdateSelection(graph, existingSelection, newNodesToSelect, type, isRectSelection);
        }

        private static void UpdateSelection(Graph graph, INode[] existingSelection, INode[] newNodesToSelect, Graph.SelectionType type, bool isRectSelection)
        {
            switch (type)
            {
                case Graph.SelectionType.Additive:
                    if (newNodesToSelect.Length > 0)
                    {
                        INode[] array = existingSelection.Union(newNodesToSelect).ToArray();
                        graph.SelectNodes(array, Graph.SelectionType.Normal);
                    }
                    else
                    {
                        graph.SelectNodes(existingSelection, Graph.SelectionType.Normal);
                    }
                    break;
                case Graph.SelectionType.Subtractive:
                    {
                        INode[] array = existingSelection.Except(newNodesToSelect).ToArray();
                        graph.SelectNodes(array, Graph.SelectionType.Normal);
                        break;
                    }
                case Graph.SelectionType.Normal:
                    graph.SelectNodes(newNodesToSelect, Graph.SelectionType.Normal);
                    break;
            }
        }

        internal void SendCommandsOnModifierKeys()
        {
            this.m_graphWindow.SendEvent(EditorGUIUtility.CommandEvent("ModifierKeysChanged"));
        }

        internal static Rect FromToRect(Vector2 start, Vector2 end)
        {
            Rect result = new Rect(start.x, start.y, end.x - start.x, end.y - start.y);
            if (result.width < 0f)
            {
                result.x += result.width;
                result.width = -result.width;
            }
            if (result.height < 0f)
            {
                result.y += result.height;
                result.height = -result.height;
            }
            return result;
        }
    }
}
#endif