﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ImprobableStudios.GraphWindowUtilities
{

    [Serializable]
    public class Graph
    {
        public enum SelectionType
        {
            Normal,
            Additive,
            Subtractive
        }

        /// <summary>
        /// Currently selected node in the graph editor.
        /// </summary>
        [NonSerialized]
        public List<INode> m_SelectedNodes = new List<INode>();

        /// <summary>
        /// All nodes in the graph.
        /// </summary>
        [HideInInspector]
        [NonSerialized]
        public List<INode> m_Nodes = new List<INode>();

        public delegate void SelectedNodesChangedEventHandler();
        public event SelectedNodesChangedEventHandler SelectedNodesChanged = delegate { };

        ~Graph()
        {
            m_Nodes.Clear();
            foreach (SelectedNodesChangedEventHandler d in SelectedNodesChanged.GetInvocationList())
            {
                SelectedNodesChanged -= d;
            }
            SelectedNodesChanged = null;
        }

        public bool IsNodeSelected(INode node)
        {
            return m_SelectedNodes.Contains(node);
        }

        public INode[] GetSelectedNodes()
        {
            return m_SelectedNodes.ToArray();
        }

        public INode GetFirstSelectedNode()
        {
            if (m_SelectedNodes.Count > 0)
            {
                return m_SelectedNodes[0];
            }
            return null;
        }

        public void SelectNodes(INode[] nodes, SelectionType selectionType)
        {
            switch (selectionType)
            {
                case SelectionType.Additive:
                    foreach (INode node in nodes)
                    {
                        SelectNode(node);
                    }
                    break;
                case SelectionType.Subtractive:
                    foreach (INode node in nodes)
                    {
                        DeselectNode(node);
                    }
                    break;
                case SelectionType.Normal:
                    SetSelectedNodes(nodes);
                    break;
            }
        }

        public void ClearSelectedNodes()
        {
            if (m_SelectedNodes.Count > 0)
            {
                m_SelectedNodes.Clear();
                SelectedNodesChanged();
            }
        }

        private void DeselectNode(INode node)
        {
            if (m_SelectedNodes.Contains(node))
            {
                m_SelectedNodes.Remove(node);
                SelectedNodesChanged();
            }
        }

        private void SelectNode(INode node)
        {
            if (!m_SelectedNodes.Contains(node))
            {
                m_SelectedNodes.Add(node);
                SelectedNodesChanged();
            }
        }

        private void SetSelectedNodes(INode[] nodes)
        {
            if (!m_SelectedNodes.SequenceEqual(nodes))
            {
                m_SelectedNodes = nodes.ToList();
                SelectedNodesChanged();
            }
        }
        
        public virtual INode[] GetExecutingNodes()
        {
            return m_Nodes.Where(i => i.IsExecuting()).ToArray();
        }

#if UNITY_EDITOR

        public INode[] GetNodesInGameObjectSelection()
        {
            List<INode> nodesInGameObjectSelection = new List<INode>();
            foreach (UnityEngine.Object selectedObj in UnityEditor.Selection.objects)
            {
                GameObject selectedGameObject = selectedObj as GameObject;
                if (selectedGameObject != null)
                {
                    foreach (INode childNode in selectedGameObject.GetComponents<INode>())
                    {
                        foreach (INode node in m_Nodes)
                        {
                            if (node.GetKey() == childNode.GetKey())
                            {
                                nodesInGameObjectSelection.Add(node);
                            }
                        }
                    }
                }
            }
            return nodesInGameObjectSelection.ToArray();
        }

        public GameObject[] GetGameObjectsInNodeSelection()
        {
            List<GameObject> gameObjectsInNodeSelection = new List<GameObject>();
            foreach (INode selectedNode in GetSelectedNodes())
            {
                UnityEngine.Object selectedNodeObj = selectedNode.GetUnityObject();
                GameObject selectedNodeGameObject = selectedNodeObj as GameObject;
                if (selectedNodeGameObject != null)
                {
                    gameObjectsInNodeSelection.Add(selectedNodeGameObject);
                }
                Component selectedNodeComponent = selectedNodeObj as Component;
                if (selectedNodeComponent != null)
                {
                    gameObjectsInNodeSelection.Add(selectedNodeComponent.gameObject);
                }
            }
            return gameObjectsInNodeSelection.ToArray();
        }
        
        public bool ObjectIsNodeInThisGraph(UnityEngine.Object obj)
        {
            GameObject selectedGameObject = obj as GameObject;
            if (selectedGameObject != null)
            {
                foreach (INode childNode in selectedGameObject.GetComponents<INode>())
                {
                    if (m_Nodes.Contains(childNode))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
#endif
    }

}
