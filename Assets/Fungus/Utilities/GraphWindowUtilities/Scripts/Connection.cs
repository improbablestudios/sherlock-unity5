﻿namespace ImprobableStudios.GraphWindowUtilities
{

    public class Connection
    {
        private INode m_node;
        private INode m_connectedNode;
        private string m_description;

        public INode Node
        {
            get
            {
                return m_node;
            }
            set
            {
                m_node = value;
            }
        }

        public INode ConnectedNode
        {
            get
            {
                return m_connectedNode;
            }
            set
            {
                m_connectedNode = value;
            }
        }

        public string Description
        {
            get
            {
                return m_description;
            }
            set
            {
                m_description = value;
            }
        }

        public Connection(INode node, INode connectedNode, string description)
        {
            m_node = node;
            m_connectedNode = connectedNode;
            m_description = description;
        }

        public override int GetHashCode()
        {
            if (m_node != null && m_connectedNode != null)
            {
                return m_node.GetHashCode() ^ m_connectedNode.GetHashCode();
            }
            else if (m_node != null)
            {
                return m_node.GetHashCode();
            }
            else if (m_connectedNode != null)
            {
                return m_connectedNode.GetHashCode();
            }
            return 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return Equals((Connection)obj);
        }

        public bool Equals(Connection other)
        {
            return other.m_node == m_node && (other.m_node == null || other.m_node.Equals(m_node)) && other.m_connectedNode == m_connectedNode && (other.m_connectedNode == null || other.m_connectedNode.Equals(m_connectedNode));
        }
    }

}
