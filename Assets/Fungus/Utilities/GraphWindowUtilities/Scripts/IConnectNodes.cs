﻿using System;

namespace ImprobableStudios.GraphWindowUtilities
{

    public interface IConnectNodes
    {
        UnityEngine.Object GetUnityObject();

        Type GetNodeType();

        string[] GetSwitchNodePropertyPaths();

        string[] GetActivateNodePropertyPaths();

        string[] GetDeactivateNodePropertyPaths();

        string GetDescription();
    }

}
