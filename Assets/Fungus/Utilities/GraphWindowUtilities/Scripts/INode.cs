﻿using UnityEngine;

namespace ImprobableStudios.GraphWindowUtilities
{
    public interface INode
    {
        Vector2 GetNodePosition();

        void SetNodePosition(Vector2 rect);

        float GetNodeExecutingIconTimer();

        void SetNodeExecutingIconTimer(float time);

        string GetNodeName();

        void SetNodeName(string newNodeName);

        string GetNodeDescription();

        bool IsExecuting();

        IConnectNodes[] GetNodeConnections();

        UnityEngine.Object GetUnityObject();

        string GetKey();
    }
}
