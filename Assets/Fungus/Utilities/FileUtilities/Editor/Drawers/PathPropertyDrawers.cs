﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.FileUtilities
{

    [CustomPropertyDrawer(typeof(OpenAssetFolderPathFieldAttribute))]
    public class OpenAssetFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenAssetFolderPathFieldAttribute pathAttribute = attribute as OpenAssetFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            PathEditorGUI.OpenAssetFolderPathField(position, property, label, title, directory);
        }
    }

    [CustomPropertyDrawer(typeof(OpenStreamingAssetFolderPathFieldAttribute))]
    public class OpenStreamingAssetFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenStreamingAssetFolderPathFieldAttribute pathAttribute = attribute as OpenStreamingAssetFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            PathEditorGUI.OpenStreamingAssetFolderPathField(position, property, label, title, directory);
        }
    }

    [CustomPropertyDrawer(typeof(OpenPersistentDataFolderPathFieldAttribute))]
    public class OpenPersistentDataFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenPersistentDataFolderPathFieldAttribute pathAttribute = attribute as OpenPersistentDataFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            PathEditorGUI.OpenPersistentDataFolderPathField(position, property, label, title, directory);
        }
    }

    [CustomPropertyDrawer(typeof(OpenResourceFolderPathFieldAttribute))]
    public class OpenResourceFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenResourceFolderPathFieldAttribute pathAttribute = attribute as OpenResourceFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            PathEditorGUI.OpenResourceFolderPathField(position, property, label, title, directory);
        }
    }

    [CustomPropertyDrawer(typeof(SaveAssetFolderPathFieldAttribute))]
    public class SaveAssetFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveAssetFolderPathFieldAttribute pathAttribute = attribute as SaveAssetFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            PathEditorGUI.SaveAssetFolderPathField(position, property, label, title, directory, defaultName);
        }
    }

    [CustomPropertyDrawer(typeof(SaveStreamingAssetFolderPathFieldAttribute))]
    public class SaveStreamingAssetFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveStreamingAssetFolderPathFieldAttribute pathAttribute = attribute as SaveStreamingAssetFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            PathEditorGUI.SaveStreamingAssetFolderPathField(position, property, label, title, directory, defaultName);
        }
    }

    [CustomPropertyDrawer(typeof(SavePersistentDataFolderPathFieldAttribute))]
    public class SavePersistentDataFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SavePersistentDataFolderPathFieldAttribute pathAttribute = attribute as SavePersistentDataFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            PathEditorGUI.SavePersistentDataFolderPathField(position, property, label, title, directory, defaultName);
        }
    }

    [CustomPropertyDrawer(typeof(SaveResourceFolderPathFieldAttribute))]
    public class SaveResourceFolderPathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveResourceFolderPathFieldAttribute pathAttribute = attribute as SaveResourceFolderPathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            PathEditorGUI.SaveResourceFolderPathField(position, property, label, title, directory, defaultName);
        }
    }

    [CustomPropertyDrawer(typeof(OpenAssetFilePathFieldAttribute))]
    public class OpenAssetFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenAssetFilePathFieldAttribute pathAttribute = attribute as OpenAssetFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            PathEditorGUI.OpenAssetFilePathField(position, property, label, title, directory, extension);
        }
    }

    [CustomPropertyDrawer(typeof(OpenStreamingAssetFilePathFieldAttribute))]
    public class OpenStreamingAssetFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenStreamingAssetFilePathFieldAttribute pathAttribute = attribute as OpenStreamingAssetFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            PathEditorGUI.OpenStreamingAssetFilePathField(position, property, label, title, directory, extension);
        }
    }

    [CustomPropertyDrawer(typeof(OpenPersistentDataFilePathFieldAttribute))]
    public class OpenPersistentDataFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenPersistentDataFilePathFieldAttribute pathAttribute = attribute as OpenPersistentDataFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            PathEditorGUI.OpenPersistentDataFilePathField(position, property, label, title, directory, extension);
        }
    }

    [CustomPropertyDrawer(typeof(OpenResourceFilePathFieldAttribute))]
    public class OpenResourceFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OpenResourceFilePathFieldAttribute pathAttribute = attribute as OpenResourceFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.Extension);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            PathEditorGUI.OpenResourceFilePathField(position, property, label, title, directory, extension);
        }
    }

    [CustomPropertyDrawer(typeof(SaveAssetFilePathFieldAttribute))]
    public class SaveAssetFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveAssetFilePathFieldAttribute pathAttribute = attribute as SaveAssetFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            PathEditorGUI.SaveAssetFilePathField(position, property, label, title, directory, defaultName, extension);
        }
    }

    [CustomPropertyDrawer(typeof(SaveStreamingAssetFilePathFieldAttribute))]
    public class SaveStreamingAssetFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveStreamingAssetFilePathFieldAttribute pathAttribute = attribute as SaveStreamingAssetFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            PathEditorGUI.SaveStreamingAssetFilePathField(position, property, label, title, directory, defaultName, extension);
        }
    }

    [CustomPropertyDrawer(typeof(SavePersistentDataFilePathFieldAttribute))]
    public class SavePersistentDataFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SavePersistentDataFilePathFieldAttribute pathAttribute = attribute as SavePersistentDataFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            PathEditorGUI.SavePersistentDataFilePathField(position, property, label, title, directory, defaultName, extension);
        }
    }

    [CustomPropertyDrawer(typeof(SaveResourceFilePathFieldAttribute))]
    public class SaveResourceFilePathFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SaveResourceFilePathFieldAttribute pathAttribute = attribute as SaveResourceFilePathFieldAttribute;
            DrawProperty(position, property, label, pathAttribute.Title, pathAttribute.Directory, pathAttribute.DefaultName, pathAttribute.Extension);
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            PathEditorGUI.SaveResourceFilePathField(position, property, label, title, directory, defaultName, extension);
        }
    }

}
#endif