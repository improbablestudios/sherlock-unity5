﻿#if UNITY_EDITOR
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.FileUtilities
{

    public static class PathEditorGUI
    {
        private const string k_resourcesFolderName = "/Resources/";

        private static FieldInfo s_mixedValueContentField = typeof(EditorGUI).GetField("s_MixedValueContent", BindingFlags.Static | BindingFlags.NonPublic);
        private static FieldInfo s_mixedValueContentColorTempField = typeof(EditorGUI).GetField("s_MixedValueContentColorTemp", BindingFlags.Static | BindingFlags.NonPublic);

        public static GUIContent GetMixedValueContent()
        {
            return (GUIContent)s_mixedValueContentField.GetValue(null);
        }

        public static Color GetMixedValueColor()
        {
            return (Color)s_mixedValueContentColorTempField.GetValue(null);
        }

        public static void SetMixedValueColor(Color color)
        {
            s_mixedValueContentColorTempField.SetValue(null, color);
        }

        public static void OpenAssetFolderPathField(SerializedProperty property, string title, string directory, params GUILayoutOption[] options)
        {
            OpenAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory);
        }

        public static void OpenAssetFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            OpenAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory);
        }

        public static void OpenAssetFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenAssetFolderPathField(position, path, label, title, directory);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenAssetFolderPathField(string folderPath, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            return OpenAssetFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory);
        }

        public static string OpenAssetFolderPathField(Rect position, string path, GUIContent label, string title, string directory)
        {
            return PathField(position, path, label, title, directory, "", "", GetAssetPathPrefix(), true, false);
        }

        public static void OpenStreamingAssetFolderPathField(SerializedProperty property, string title, string directory, params GUILayoutOption[] options)
        {
            OpenStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory);
        }

        public static void OpenStreamingAssetFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            OpenStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory);
        }

        public static void OpenStreamingAssetFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenStreamingAssetFolderPathField(position, path, label, title, directory);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenStreamingAssetFolderPathField(string folderPath, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            return OpenStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory);
        }

        public static string OpenStreamingAssetFolderPathField(Rect position, string path, GUIContent label, string title, string directory)
        {
            return PathField(position, path, label, title, directory, "", "", Application.streamingAssetsPath, true, false);
        }

        public static void OpenPersistentDataFolderPathField(SerializedProperty property, string title, string directory, params GUILayoutOption[] options)
        {
            OpenPersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory);
        }

        public static void OpenPersistentDataFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            OpenPersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory);
        }

        public static void OpenPersistentDataFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenPersistentDataFolderPathField(position, path, label, title, directory);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenPersistentDataFolderPathField(string folderPath, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            return OpenPersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory);
        }

        public static string OpenPersistentDataFolderPathField(Rect position, string path, GUIContent label, string title, string directory)
        {
            return PathField(position, path, label, title, directory, "", "", Application.persistentDataPath, true, false);
        }

        public static void OpenResourceFolderPathField(SerializedProperty property, string title, string directory, params GUILayoutOption[] options)
        {
            OpenResourceFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory);
        }

        public static void OpenResourceFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            OpenResourceFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory);
        }

        public static void OpenResourceFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenResourceFolderPathField(position, path, label, title, directory);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenResourceFolderPathField(string folderPath, GUIContent label, string title, string directory, params GUILayoutOption[] options)
        {
            return OpenResourceFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory);
        }

        public static string OpenResourceFolderPathField(Rect position, string path, GUIContent label, string title, string directory)
        {
            return PathField(position, path, label, title, directory, "", "", k_resourcesFolderName, true, false);
        }

        public static void SaveAssetFolderPathField(SerializedProperty property, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName);
        }

        public static void SaveAssetFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName);
        }

        public static void SaveAssetFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveAssetFolderPathField(position, path, label, title, directory, defaultName);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveAssetFolderPathField(string folderPath, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            return SaveAssetFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName);
        }

        public static string SaveAssetFolderPathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName)
        {
            return PathField(position, path, label, title, directory, defaultName, "", GetAssetPathPrefix(), true, true);
        }

        public static void SaveStreamingAssetFolderPathField(SerializedProperty property, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName);
        }

        public static void SaveStreamingAssetFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName);
        }

        public static void SaveStreamingAssetFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveStreamingAssetFolderPathField(position, path, label, title, directory, defaultName);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveStreamingAssetFolderPathField(string folderPath, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            return SaveStreamingAssetFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName);
        }

        public static string SaveStreamingAssetFolderPathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName)
        {
            return PathField(position, path, label, title, directory, defaultName, "", Application.streamingAssetsPath, true, true);
        }

        public static void SavePersistentDataFolderPathField(SerializedProperty property, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SavePersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName);
        }

        public static void SavePersistentDataFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SavePersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName);
        }

        public static void SavePersistentDataFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SavePersistentDataFolderPathField(position, path, label, title, directory, defaultName);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SavePersistentDataFolderPathField(string folderPath, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            return SavePersistentDataFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName);
        }

        public static string SavePersistentDataFolderPathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName)
        {
            return PathField(position, path, label, title, directory, defaultName, "", Application.persistentDataPath, true, true);
        }

        public static void SaveResourceFolderPathField(SerializedProperty property, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveResourceFolderPathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName);
        }

        public static void SaveResourceFolderPathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            SaveResourceFolderPathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName);
        }

        public static void SaveResourceFolderPathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveResourceFolderPathField(position, path, label, title, directory, defaultName);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveResourceFolderPathField(string folderPath, GUIContent label, string title, string directory, string defaultName, params GUILayoutOption[] options)
        {
            return SaveResourceFolderPathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName);
        }

        public static string SaveResourceFolderPathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName)
        {
            return PathField(position, path, label, title, directory, defaultName, "", k_resourcesFolderName, true, true);
        }

        public static void OpenAssetFilePathField(SerializedProperty property, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenAssetFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, extension);
        }

        public static void OpenAssetFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenAssetFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, extension);
        }

        public static void OpenAssetFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenAssetFilePathField(position, path, label, title, directory, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenAssetFilePathField(string folderPath, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            return OpenAssetFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, extension);
        }

        public static string OpenAssetFilePathField(Rect position, string path, GUIContent label, string title, string directory, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, GetAssetPathPrefix(), false, false);
        }

        public static void OpenStreamingAssetFilePathField(SerializedProperty property, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, extension);
        }

        public static void OpenStreamingAssetFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, extension);
        }

        public static void OpenStreamingAssetFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenStreamingAssetFilePathField(position, path, label, title, directory, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenStreamingAssetFilePathField(string folderPath, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            return OpenStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, extension);
        }

        public static string OpenStreamingAssetFilePathField(Rect position, string path, GUIContent label, string title, string directory, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, Application.streamingAssetsPath, false, false);
        }

        public static void OpenPersistentDataFilePathField(SerializedProperty property, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenPersistentDataFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, extension);
        }

        public static void OpenPersistentDataFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenPersistentDataFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, extension);
        }

        public static void OpenPersistentDataFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenPersistentDataFilePathField(position, path, label, title, directory, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenPersistentDataFilePathField(string folderPath, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            return OpenPersistentDataFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, extension);
        }

        public static string OpenPersistentDataFilePathField(Rect position, string path, GUIContent label, string title, string directory, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, Application.persistentDataPath, false, false);
        }

        public static void OpenResourceFilePathField(SerializedProperty property, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenResourceFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, extension);
        }

        public static void OpenResourceFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            OpenResourceFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, extension);
        }

        public static void OpenResourceFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = OpenResourceFilePathField(position, path, label, title, directory, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string OpenResourceFilePathField(string folderPath, GUIContent label, string title, string directory, string extension, params GUILayoutOption[] options)
        {
            return OpenResourceFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, extension);
        }

        public static string OpenResourceFilePathField(Rect position, string path, GUIContent label, string title, string directory, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, k_resourcesFolderName, false, false);
        }

        public static void SaveAssetFilePathField(SerializedProperty property, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveAssetFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName, extension);
        }

        public static void SaveAssetFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveAssetFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName, extension);
        }

        public static void SaveAssetFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveAssetFilePathField(position, path, label, title, directory, defaultName, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveAssetFilePathField(string folderPath, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            return SaveAssetFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName, extension);
        }

        public static string SaveAssetFilePathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, GetAssetPathPrefix(), false, true);
        }

        public static void SaveStreamingAssetFilePathField(SerializedProperty property, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName, extension);
        }

        public static void SaveStreamingAssetFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName, extension);
        }

        public static void SaveStreamingAssetFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveStreamingAssetFilePathField(position, path, label, title, directory, defaultName, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveStreamingAssetFilePathField(string folderPath, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            return SaveStreamingAssetFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName, extension);
        }

        public static string SaveStreamingAssetFilePathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, Application.streamingAssetsPath, false, true);
        }

        public static void SavePersistentDataFilePathField(SerializedProperty property, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SavePersistentDataFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName, extension);
        }

        public static void SavePersistentDataFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SavePersistentDataFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName, extension);
        }

        public static void SavePersistentDataFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SavePersistentDataFilePathField(position, path, label, title, directory, defaultName, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SavePersistentDataFilePathField(string folderPath, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            return SavePersistentDataFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName, extension);
        }

        public static string SavePersistentDataFilePathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            return PathField(position, path, label, title, directory, "", extension, Application.persistentDataPath, false, true);
        }

        public static void SaveResourceFilePathField(SerializedProperty property, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveResourceFilePathField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), title, directory, defaultName, extension);
        }

        public static void SaveResourceFilePathField(SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            SaveResourceFilePathField(EditorGUILayout.GetControlRect(options), property, label, title, directory, defaultName, extension);
        }

        public static void SaveResourceFilePathField(Rect position, SerializedProperty property, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string path = property.stringValue;

            path = SaveResourceFilePathField(position, path, label, title, directory, defaultName, extension);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = path;
            }

            EditorGUI.EndProperty();
        }

        public static string SaveResourceFilePathField(string folderPath, GUIContent label, string title, string directory, string defaultName, string extension, params GUILayoutOption[] options)
        {
            return SaveResourceFilePathField(EditorGUILayout.GetControlRect(options), folderPath, label, title, directory, defaultName, extension);
        }

        public static string SaveResourceFilePathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName, string extension)
        {
            return PathField(position, path, label, title, directory, defaultName, extension, k_resourcesFolderName, false, true);
        }

        public static string PathField(Rect position, string path, GUIContent label, string title, string directory, string defaultName, string extension, string pathPrefix, bool folder, bool save)
        {
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);

            int controlID = GUIUtility.GetControlID(("PathField").GetHashCode(), FocusType.Keyboard, contentPosition);

            Event current = Event.current;
            EventType eventType = current.type;

            if (!GUI.enabled && Event.current.rawType == EventType.MouseDown)
            {
                eventType = Event.current.rawType;
            }

            GUIContent content;
            if (EditorGUI.showMixedValue)
            {
                content = GetMixedValueContent();
            }
            else
            {
                content = new GUIContent(path);
            }

            switch (eventType)
            {
                case EventType.KeyDown:
                    if (GUIUtility.keyboardControl == controlID)
                    {
                        if (current.keyCode == KeyCode.Backspace || current.keyCode == KeyCode.Delete)
                        {
                            path = null;
                            GUI.changed = true;
                            current.Use();
                        }
                        if (MainActionKeyForControl(current, controlID))
                        {
                            if (GUI.enabled)
                            {
                                path = OpenFileOrFolderPicker(path, title, directory, defaultName, extension, pathPrefix, folder, save);
                                GUI.changed = true;
                                current.Use();
                            }
                        }
                        break;
                    }
                    break;
                case EventType.Repaint:
                    BeginHandleMixedValueContentColor();
                    GUIStyle style = new GUIStyle(EditorStyles.objectField);
                    style.Draw(contentPosition, content, controlID, DragAndDrop.activeControlID == controlID);
                    EndHandleMixedValueContentColor();
                    break;
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (contentPosition.Contains(Event.current.mousePosition) && GUI.enabled)
                    {
                        string[] draggedPaths = DragAndDrop.paths;
                        if (draggedPaths != null && draggedPaths.Length > 0)
                        {
                            DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
                            if (current.type == EventType.DragPerform)
                            {
                                string filePath = Path.Combine(GetAssetPathPrefix(), draggedPaths[0]).Replace("\\", "/");
                                if (pathPrefix == k_resourcesFolderName)
                                {
                                    path = GetResourcePathWithoutPrefix(filePath);
                                }
                                else
                                {
                                    path = GetFilePathWithoutPrefix(filePath, pathPrefix);
                                }
                                GUI.changed = true;
                                current.Use();
                                DragAndDrop.AcceptDrag();
                                DragAndDrop.activeControlID = 0;
                            }
                            else
                            {
                                DragAndDrop.activeControlID = controlID;
                            }
                            current.Use();
                        }
                    }
                    break;
                case EventType.MouseDown:
                    if (current.button == 0)
                    {
                        float buttonWidth = 15f;
                        Rect buttonRect = new Rect(position.xMax - buttonWidth, position.y, buttonWidth, position.height);

                        if (contentPosition.Contains(Event.current.mousePosition) && !buttonRect.Contains(Event.current.mousePosition))
                        {
                            string filePath = GetFilePath(path, pathPrefix);
                            string assetFilePathPrefix = GetAssetPathPrefix();

                            if (filePath.StartsWith(assetFilePathPrefix))
                            {
                                string assetPath = filePath.Substring(assetFilePathPrefix.Length + 1);

                                UnityEngine.Object clickedObject = AssetDatabase.LoadAssetAtPath(assetPath, typeof(UnityEngine.Object));
                                if (EditorGUI.showMixedValue)
                                {
                                    clickedObject = (UnityEngine.Object)null;
                                }
                                if (clickedObject != null)
                                {
                                    if (Event.current.clickCount == 1)
                                    {
                                        GUIUtility.keyboardControl = controlID;
                                        EditorGUIUtility.PingObject(clickedObject);
                                        current.Use();
                                    }
                                    else if (Event.current.clickCount == 2)
                                    {
                                        if ((bool)clickedObject)
                                        {
                                            AssetDatabase.OpenAsset(clickedObject);
                                            GUIUtility.ExitGUI();
                                        }
                                        current.Use();
                                    }
                                }
                            }
                        }
                        if (buttonRect.Contains(Event.current.mousePosition))
                        {
                            if (GUI.enabled)
                            {
                                path = OpenFileOrFolderPicker(path, title, directory, defaultName, extension, pathPrefix, folder, save);
                                GUI.changed = true;
                                current.Use();
                            }
                        }
                    }
                    break;
                default:
                    if (current.type != EventType.Layout && current.type != EventType.ExecuteCommand && current.type != EventType.DragExited)
                    {
                        if (GUI.enabled)
                        {
                            HandleUtility.Repaint();
                        }
                    }
                    break;
            }
            return path;
        }

        public static void BeginHandleMixedValueContentColor()
        {
            SetMixedValueColor(GUI.contentColor);
            GUI.contentColor = !EditorGUI.showMixedValue ? GUI.contentColor : GUI.contentColor * GetMixedValueColor();
        }

        public static void EndHandleMixedValueContentColor()
        {
            GUI.contentColor = GetMixedValueColor();
        }

        public static string OpenFileOrFolderPicker(string path, string title, string directory, string defaultName, string extension, string pathPrefix, bool folder, bool save)
        {
            path = path.Replace('\\', '/');
            pathPrefix = pathPrefix.Replace('\\', '/');
            string filePath = GetFilePath(path, pathPrefix);
            if (string.IsNullOrEmpty(directory))
            {
                directory = (!string.IsNullOrEmpty(filePath)) ? Path.GetDirectoryName(filePath) : Application.dataPath;
            }
            if (folder)
            {
                if (save)
                {
                    filePath = EditorUtility.SaveFolderPanel(title, directory, defaultName);
                }
                else
                {
                    filePath = EditorUtility.OpenFolderPanel(title, directory, defaultName);
                }
            }
            else
            {
                if (save)
                {
                    filePath = EditorUtility.SaveFilePanel(title, directory, defaultName, extension);
                }
                else
                {
                    filePath = EditorUtility.OpenFilePanel(title, directory, extension);
                }
            }
            if (!string.IsNullOrEmpty(filePath))
            {
                if (pathPrefix == k_resourcesFolderName)
                {
                    path = GetResourcePathWithoutPrefix(filePath);
                }
                else
                {
                    path = GetFilePathWithoutPrefix(filePath, pathPrefix);
                }
            }
            return path;
        }

        private static string GetFilePath(string path, string pathPrefix)
        {
            path = path.Replace('\\', '/');
            pathPrefix = pathPrefix.Replace('\\', '/');
            string filePath = path;
            if (pathPrefix == k_resourcesFolderName)
            {
                filePath = GetResourcePathWithPrefix(filePath);
            }
            else
            {
                filePath = GetFilePathWithPrefix(filePath, pathPrefix);
            }
            if (filePath != null)
            {
                return filePath.Replace('\\', '/');
            }

            return filePath;
        }

        private static bool MainActionKeyForControl(Event evt, int controlId)
        {
            if (GUIUtility.keyboardControl != controlId)
            {
                return false;
            }
            bool flag = evt.alt || evt.shift || evt.command || evt.control;
            if (evt.type != EventType.KeyDown || (int)evt.character != 32 || flag)
            {
                return evt.type == EventType.KeyDown && (evt.keyCode == KeyCode.Space || evt.keyCode == KeyCode.Return || evt.keyCode == KeyCode.KeypadEnter) && !flag;
            }
            evt.Use();
            return false;
        }

        private static string GetAssetPathPrefix()
        {
            string assetsString = "Assets";
            return Application.dataPath.Remove(Application.dataPath.LastIndexOf(assetsString)).TrimEnd('/', '\\');
        }

        private static string GetFilePathWithPrefix(string path, string filePathPrefix)
        {
            path = path.Replace('\\', '/');
            filePathPrefix = filePathPrefix.Replace('\\', '/');
            if (!string.IsNullOrEmpty(filePathPrefix) && !path.StartsWith(filePathPrefix))
            {
                path = Path.Combine(filePathPrefix, path);
            }
            return path.Replace("\\", "/");
        }

        private static string GetFilePathWithoutPrefix(string filePath, string filePathPrefix)
        {
            filePath = filePath.Replace('\\', '/');
            filePathPrefix = filePathPrefix.Replace('\\', '/');
            if (!string.IsNullOrEmpty(filePath) && !string.IsNullOrEmpty(filePathPrefix))
            {
                filePath = filePath.Replace(filePathPrefix, "");
                filePath = filePath.TrimStart('/', '\\');
            }
            if (filePath != null)
            {
                return filePath.Replace("\\", "/");
            }
            return null;
        }

        private static string GetResourceAssetPath(string path)
        {
            path = path.Replace('\\', '/');
            UnityEngine.Object obj = Resources.Load(path);
            if (obj != null)
            {
                return AssetDatabase.GetAssetPath(obj);
            }

            return null;
        }

        private static string GetResourcePathWithPrefix(string path)
        {
            path = path.Replace('\\', '/');
            UnityEngine.Object obj = Resources.Load(path);
            if (obj != null)
            {
                string resourceAssetPath = AssetDatabase.GetAssetPath(obj);
                string filePathPrefix = Path.Combine(GetAssetPathPrefix(), resourceAssetPath);
                string filePath = GetFilePathWithPrefix(path, filePathPrefix);
                if (filePath != null)
                {
                    return filePath.Replace("\\", "/");
                }
            }

            return null;
        }

        private static string GetResourcePathWithoutPrefix(string filePath)
        {
            string resourcePath = Path.GetDirectoryName(filePath) + "/" + Path.GetFileNameWithoutExtension(filePath);
            resourcePath = resourcePath.Replace('\\', '/');
            int resourcesPathIndex = resourcePath.LastIndexOf(k_resourcesFolderName);
            if (resourcesPathIndex >= 0)
            {
                resourcePath = resourcePath.Substring(resourcesPathIndex + k_resourcesFolderName.Length);
            }
            UnityEngine.Object obj = Resources.Load(resourcePath);
            if (obj != null)
            {
                if (resourcePath != null)
                {
                    return resourcePath.Replace("\\", "/");
                }
            }
            return null;
        }
    }

}
#endif