﻿using System.IO;
using System.Text;
using UnityEngine;

namespace ImprobableStudios.FileUtilities
{

    public static class FileWriter
    {
        public static void WriteDataToPersistentData(string path, byte[] data)
        {
            WriteData(Application.persistentDataPath, path, data);
        }

        public static void WriteTextToPersistentData(string path, string text, Encoding encoding)
        {
            WriteDataToPersistentData(path, encoding.GetBytes(text));
        }

        public static void WriteTextToPersistentData(string path, string text)
        {
            WriteTextToPersistentData(path, text, Encoding.UTF8);
        }

        public static void WriteDataToStreamingAssets(string path, byte[] data)
        {
            WriteData(Application.streamingAssetsPath, path, data);
        }
        
        public static void WriteTextToStreamingAssets(string path, string text, Encoding encoding)
        {
            WriteDataToStreamingAssets(path, encoding.GetBytes(text));
        }

        public static void WriteTextToStreamingAssets(string path, string text)
        {
            WriteTextToStreamingAssets(path, text, Encoding.UTF8);
        }

        public static void WriteData(string location, string path, byte[] data)
        {
            string filePath = Path.Combine(location, path);

            if (File.Exists(filePath))
            {
                File.WriteAllBytes(filePath, data);
            }
            else
            {
                throw new FileNotFoundException(filePath);
            }
        }
    }

}
