﻿using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace ImprobableStudios.FileUtilities
{

    public static class FileReader
    {
        public static string ReadTextFromResources(string path)
        {
            return ReadTextFromResources(path, Encoding.UTF8);
        }

        public static string ReadTextFromStreamingAssets(string path)
        {
            return ReadTextFromStreamingAssets(path, Encoding.UTF8);
        }

        public static string ReadTextFromPersistentData(string path)
        {
            return ReadTextFromPersistentData(path, Encoding.UTF8);
        }
        
        public static string ReadTextFromResources(string path, Encoding encoding)
        {
            return encoding.GetString(ReadDataFromResources(path));
        }

        public static string ReadTextFromStreamingAssets(string path, Encoding encoding)
        {
            return encoding.GetString(ReadDataFromStreamingAssets(path));
        }

        public static string ReadTextFromPersistentData(string path, Encoding encoding)
        {
            return encoding.GetString(ReadDataFromPersistentData(path));
        }

        public static byte[] ReadDataFromResources(string path)
        {
            TextAsset asset = Resources.Load<TextAsset>(path);
            if (asset != null)
            {
                return asset.bytes;
            }
            else
            {
                throw new FileNotFoundException(path);
            }
        }

        public static byte[] ReadDataFromStreamingAssets(string path)
        {
            string filePath = Path.Combine(Application.streamingAssetsPath, path);
            byte[] data = null;

            if (filePath.Contains("://") || filePath.Contains(":///"))
            {
                UnityWebRequest downloadRequest = UnityWebRequest.Get(filePath);
                while (!downloadRequest.isDone)
                {
                }
                if (string.IsNullOrEmpty(downloadRequest.error))
                {
                    data = downloadRequest.downloadHandler.data;
                }
                else
                {
                    throw new FileNotFoundException(filePath);
                }
            }
            else
            {
                return ReadData(Application.streamingAssetsPath, path);
            }
            return data;
        }

        public static byte[] ReadDataFromPersistentData(string path)
        {
            return ReadData(Application.persistentDataPath, path);
        }

        public static byte[] ReadData(string location, string path)
        {
            string filePath = Path.Combine(location, path);
            byte[] data = null;

            if (File.Exists(filePath))
            {
                data = File.ReadAllBytes(filePath);
            }
            else
            {
                throw new FileNotFoundException(filePath);
            }

            return data;
        }
    }

}