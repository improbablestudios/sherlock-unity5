﻿using UnityEngine;

namespace ImprobableStudios.FileUtilities
{
    
    public class OpenFolderPathFieldAttribute : PropertyAttribute
    {
        public OpenFolderPathFieldAttribute() : base()
        {
            Title = "Select a folder";
            Directory = "";
        }

        public OpenFolderPathFieldAttribute(string title, string directory) : base()
        {
            Title = title;
            Directory = directory;
        }

        public string Title
        {
            get;
            set;
        } = "Select a folder";

        public string Directory
        {
            get;
            set;
        } = "";
    }

    public class SaveFolderPathFieldAttribute : PropertyAttribute
    {
        public SaveFolderPathFieldAttribute() : base()
        {
            Title = "Save folder";
            Directory = "";
            DefaultName = "";
        }

        public SaveFolderPathFieldAttribute(string title, string directory, string defaultName) : base()
        {
            Title = title;
            Directory = directory;
            DefaultName = defaultName;
        }

        public string Title
        {
            get;
            set;
        } = "Save folder";

        public string Directory
        {
            get;
            set;
        } = "";

        public string DefaultName
        {
            get;
            set;
        } = "";
    }

    public class OpenFilePathFieldAttribute : PropertyAttribute
    {
        public OpenFilePathFieldAttribute()
        {
            Title = "Select a file";
            Directory = "";
            Extension = "";
        }

        public OpenFilePathFieldAttribute(string title, string directory, string extension)
        {
            Title = title;
            Directory = directory;
            Extension = extension;
        }

        public string Title
        {
            get;
            set;
        } = "Select a file";

        public string Directory
        {
            get;
            set;
        } = "";

        public string Extension
        {
            get;
            set;
        } = "";
    }

    public class SaveFilePathFieldAttribute : PropertyAttribute
    {
        public SaveFilePathFieldAttribute() : base()
        {
            Title = "Save file";
            Directory = "";
            DefaultName = "";
            Extension = "";
        }

        public SaveFilePathFieldAttribute(string title, string directory, string defaultName, string extension) : base()
        {
            Title = title;
            Directory = directory;
            DefaultName = defaultName;
            Extension = extension;
        }

        public string Title
        {
            get;
            set;
        } = "Save file";

        public string Directory
        {
            get;
            set;
        } = "";

        public string DefaultName
        {
            get;
            set;
        } = "";

        public string Extension
        {
            get;
            set;
        } = "";
    }

    public class OpenAssetFolderPathFieldAttribute : OpenFolderPathFieldAttribute
    {
        public OpenAssetFolderPathFieldAttribute() : base()
        {
        }

        public OpenAssetFolderPathFieldAttribute(string title, string directory) : base(title, directory)
        {
        }
    }

    public class OpenStreamingAssetFolderPathFieldAttribute : OpenFolderPathFieldAttribute
    {
        public OpenStreamingAssetFolderPathFieldAttribute() : base()
        {
        }

        public OpenStreamingAssetFolderPathFieldAttribute(string title, string directory) : base(title, directory)
        {
        }
    }

    public class OpenPersistentDataFolderPathFieldAttribute : OpenFolderPathFieldAttribute
    {
        public OpenPersistentDataFolderPathFieldAttribute() : base()
        {
        }

        public OpenPersistentDataFolderPathFieldAttribute(string title, string directory) : base(title, directory)
        {
        }
    }

    public class OpenResourceFolderPathFieldAttribute : OpenFolderPathFieldAttribute
    {
        public OpenResourceFolderPathFieldAttribute() : base()
        {
        }

        public OpenResourceFolderPathFieldAttribute(string title, string directory) : base(title, directory)
        {
        }
    }

    public class SaveAssetFolderPathFieldAttribute : SaveFolderPathFieldAttribute
    {
        public SaveAssetFolderPathFieldAttribute() : base()
        {
        }

        public SaveAssetFolderPathFieldAttribute(string title, string directory, string defaultName) : base(title, directory, defaultName)
        {
        }
    }

    public class SaveStreamingAssetFolderPathFieldAttribute : SaveFolderPathFieldAttribute
    {
        public SaveStreamingAssetFolderPathFieldAttribute() : base()
        {
        }

        public SaveStreamingAssetFolderPathFieldAttribute(string title, string directory, string defaultName) : base(title, directory, defaultName)
        {
        }
    }

    public class SavePersistentDataFolderPathFieldAttribute : SaveFolderPathFieldAttribute
    {
        public SavePersistentDataFolderPathFieldAttribute() : base()
        {
        }

        public SavePersistentDataFolderPathFieldAttribute(string title, string directory, string defaultName) : base(title, directory, defaultName)
        {
        }
    }

    public class SaveResourceFolderPathFieldAttribute : SaveFolderPathFieldAttribute
    {
        public SaveResourceFolderPathFieldAttribute() : base()
        {
        }

        public SaveResourceFolderPathFieldAttribute(string title, string directory, string defaultName) : base(title, directory, defaultName)
        {
        }
    }

    public class OpenAssetFilePathFieldAttribute : OpenFilePathFieldAttribute
    {
        public OpenAssetFilePathFieldAttribute() : base()
        {
        }

        public OpenAssetFilePathFieldAttribute(string title, string directory, string extension) : base(title, directory, extension)
        {
        }
    }

    public class OpenStreamingAssetFilePathFieldAttribute : OpenFilePathFieldAttribute
    {
        public OpenStreamingAssetFilePathFieldAttribute() : base()
        {
        }

        public OpenStreamingAssetFilePathFieldAttribute(string title, string directory, string extension) : base(title, directory, extension)
        {
        }
    }

    public class OpenPersistentDataFilePathFieldAttribute : OpenFilePathFieldAttribute
    {
        public OpenPersistentDataFilePathFieldAttribute() : base()
        {
        }

        public OpenPersistentDataFilePathFieldAttribute(string title, string directory, string extension) : base(title, directory, extension)
        {
        }
    }

    public class OpenResourceFilePathFieldAttribute : OpenFilePathFieldAttribute
    {
        public OpenResourceFilePathFieldAttribute() : base()
        {
        }

        public OpenResourceFilePathFieldAttribute(string title, string directory, string extension) : base(title, directory, extension)
        {
        }
    }

    public class SaveAssetFilePathFieldAttribute : SaveFilePathFieldAttribute
    {
        public SaveAssetFilePathFieldAttribute() : base()
        {
        }

        public SaveAssetFilePathFieldAttribute(string title, string directory, string defaultName, string extension) : base(title, directory, defaultName, extension)
        {
        }
    }

    public class SaveStreamingAssetFilePathFieldAttribute : SaveFilePathFieldAttribute
    {
        public SaveStreamingAssetFilePathFieldAttribute() : base()
        {
        }

        public SaveStreamingAssetFilePathFieldAttribute(string title, string directory, string defaultName, string extension) : base(title, directory, defaultName, extension)
        {
        }
    }

    public class SavePersistentDataFilePathFieldAttribute : SaveFilePathFieldAttribute
    {
        public SavePersistentDataFilePathFieldAttribute() : base()
        {
        }

        public SavePersistentDataFilePathFieldAttribute(string title, string directory, string defaultName, string extension) : base(title, directory, defaultName, extension)
        {
        }
    }

    public class SaveResourceFilePathFieldAttribute : SaveFilePathFieldAttribute
    {
        public SaveResourceFilePathFieldAttribute() : base()
        {
        }

        public SaveResourceFilePathFieldAttribute(string title, string directory, string defaultName, string extension) : base(title, directory, defaultName, extension)
        {
        }
    }

}