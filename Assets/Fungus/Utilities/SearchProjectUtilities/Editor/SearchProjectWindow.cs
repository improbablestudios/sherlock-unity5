﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace ImprobableStudios.SearchProjectUtilities
{

    public abstract class SearchProjectWindow : EditorWindow
    {
        public enum SearchMode
        {
            DontSearch,
            SearchAll,
            SearchSelected
        }

        protected SearchMode m_sceneSearchMode = SearchMode.SearchAll;

        protected SearchMode m_assetSearchMode = SearchMode.SearchSelected;

        protected bool m_loadAndSearchSceneAssets = true;

        protected SelectionMode m_sceneSearchSelectionMode = SelectionMode.Deep;

        protected SelectionMode m_assetSearchSelectionMode = SelectionMode.DeepAssets;

        protected bool m_searched;

        protected List<UnityEngine.Object> m_foundObjects = new List<UnityEngine.Object>();

        protected List<GameObject> m_foundPrefabs = new List<GameObject>();

        protected List<SceneAsset> m_foundScenes = new List<SceneAsset>();
        
        protected bool m_foundObjectsFoldout = true;
        
        protected bool m_foundPrefabsFoldout = true;

        protected bool m_foundScenesFoldout = true;
        
        protected Vector2 m_foundObjectsScrollPosition;

        protected Vector2 m_foundPrefabsScrollPosition;

        protected Vector2 m_foundScenesScrollPosition;

        protected abstract void OnSearchGUI();

        protected abstract bool IsSearchValid();
        
        protected abstract string GetHierarchySearchString();

        protected abstract SearchableEditorWindow.SearchMode GetHierarchySearchMode();
        
        protected abstract bool IsSearchMatch(UnityEngine.Object obj);

        protected abstract GUIContent GetSearchButtonLabel();

        protected abstract string GetNoneFoundMessage();

        protected virtual void OnEnable()
        {
            ClearSearch();
        }

        protected virtual void OnSelectionChange()
        {
            Repaint();
        }

        protected virtual void OnGUI()
        {
            OnSearchGUI();

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_sceneSearchMode = (SearchMode)EditorGUILayout.EnumPopup(new GUIContent("Current Scenes"), m_sceneSearchMode);

                switch (m_sceneSearchMode)
                {
                    case SearchMode.DontSearch:
                        EditorGUILayout.HelpBox("The gameobjects in the Hierarchy window will not be searched", MessageType.Info);
                        break;
                    case SearchMode.SearchAll:
                        EditorGUILayout.HelpBox("All gameobjects in the Hierarchy window will be searched", MessageType.Info);
                        break;
                    case SearchMode.SearchSelected:
                        EditorGUILayout.HelpBox("Selected gameobjects in the Hierarchy window (and their children) will be searched", MessageType.Info);
                        break;
                }
            }

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_assetSearchMode = (SearchMode)EditorGUILayout.EnumPopup(new GUIContent("Assets"), m_assetSearchMode);
                
                if (m_assetSearchMode != SearchMode.DontSearch)
                {
                    m_loadAndSearchSceneAssets = EditorGUILayout.Toggle(new GUIContent("Load Scene Assets"), m_loadAndSearchSceneAssets);
                }

                switch (m_assetSearchMode)
                {
                    case SearchMode.DontSearch:
                        EditorGUILayout.HelpBox("The assets and folders in the Project window will not be searched", MessageType.Info);
                        break;
                    case SearchMode.SearchAll:
                        EditorGUILayout.HelpBox("All assets and folders in the Project window will be searched", MessageType.Info);
                        break;
                    case SearchMode.SearchSelected:
                        EditorGUILayout.HelpBox("Selected assets and folders in the Project window will be searched", MessageType.Info);
                        break;
                }
            }

            bool enabled = GUI.enabled;

            if (!IsSearchValid())
            {
                GUI.enabled = false;
            }

            if (GUILayout.Button(GetSearchButtonLabel()))
            {
                ClearSearch();

                Search(GetSceneSearchObjects(), IsSearchMatch, m_loadAndSearchSceneAssets, m_foundObjects, m_foundPrefabs, m_foundScenes);
                Search(GetAssetSearchObjects(), IsSearchMatch, m_loadAndSearchSceneAssets, m_foundObjects, m_foundPrefabs, m_foundScenes);
                m_foundObjects = m_foundObjects.Distinct().ToList();
                m_foundPrefabs = m_foundPrefabs.Distinct().ToList();
                m_foundScenes = m_foundScenes.Distinct().ToList();
                m_searched = true;
            }

            GUI.enabled = enabled;

            if (m_foundObjects.Count > 0 || m_foundPrefabs.Count > 0 || m_foundScenes.Count > 0)
            {
                int spacing = 20;
                GUIStyle foundStyle = new GUIStyle(EditorStyles.helpBox);
                foundStyle.padding = new RectOffset(spacing, spacing, spacing, spacing);
                using (new EditorGUILayout.VerticalScope(foundStyle))
                {
                    OnFoundObjectsGUI();

                    GUILayout.Space(spacing);

                    OnFoundPrefabsGUI();

                    GUILayout.Space(spacing);

                    OnFoundScenesGUI();
                }
            }
            else
            {
                if (m_searched)
                {
                    string noneFoundMessage = GetNoneFoundMessage();
                    if (!string.IsNullOrEmpty(noneFoundMessage))
                    {
                        GUIStyle style = new GUIStyle(EditorStyles.helpBox);
                        style.alignment = TextAnchor.MiddleCenter;
                        EditorGUILayout.LabelField(noneFoundMessage, style);
                    }
                }
            }
        }

        protected virtual UnityEngine.Object[] GetAssetSearchObjects()
        {
            switch (m_assetSearchMode)
            {
                case SearchMode.SearchAll:
                    {
                        List<UnityEngine.Object> objs = new List<UnityEngine.Object>();
                        foreach (string guid in AssetDatabase.FindAssets("t:" + typeof(GameObject).Name, new string[] { "Assets" }))
                        {
                            string path = AssetDatabase.GUIDToAssetPath(guid);
                            if (path.EndsWith(".prefab", StringComparison.Ordinal))
                            {
                                objs.Add(AssetDatabase.LoadAssetAtPath<GameObject>(path));
                            }
                        }
                        foreach (string guid in UnityEditor.AssetDatabase.FindAssets("t:" + typeof(ScriptableObject).Name, new string[] { "Assets" }))
                        {
                            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                            UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path);
                        }
                        return objs.ToArray();
                    }
                case SearchMode.SearchSelected:
                    {
                        List<UnityEngine.Object> objs = new List<UnityEngine.Object>();
                        foreach (UnityEngine.Object obj in Selection.GetFiltered<UnityEngine.Object>(m_assetSearchSelectionMode))
                        {
                            GameObject gameObject = obj as GameObject;
                            Component component = obj as Component;
                            if (gameObject == null && component != null)
                            {
                                gameObject = component.gameObject;
                            }
                            if (gameObject == null)
                            {
                                objs.Add(obj);
                            }
                        }
                        return objs.ToArray();
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        protected virtual UnityEngine.Object[] GetSceneSearchObjects()
        {
            switch (m_assetSearchMode)
            {
                case SearchMode.SearchAll:
                    {
                        return FindObjectsInScene(typeof(UnityEngine.Object));
                    }
                case SearchMode.SearchSelected:
                    {
                        List<UnityEngine.Object> objs = new List<UnityEngine.Object>();
                        foreach (UnityEngine.Object obj in Selection.GetFiltered<UnityEngine.Object>(m_assetSearchSelectionMode))
                        {
                            GameObject gameObject = obj as GameObject;
                            Component component = obj as Component;
                            if (gameObject == null && component != null)
                            {
                                gameObject = component.gameObject;
                            }
                            if (gameObject != null)
                            {
                                objs.Add(obj);
                            }
                        }
                        return objs.ToArray();
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        protected virtual void OnFoundObjectsGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_foundObjectsFoldout = EditorGUILayout.Foldout(m_foundObjectsFoldout, new GUIContent(string.Format("Objects ({0})", m_foundObjects.Count)), true);

                if (m_foundObjects.Count > 0)
                {
                    if (m_foundObjectsFoldout)
                    {
                        using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_foundObjectsScrollPosition))
                        {
                            m_foundObjectsScrollPosition = scrollView.scrollPosition;

                            foreach (UnityEngine.Object foundObj in m_foundObjects)
                            {
                                if (foundObj != null)
                                {
                                    Rect objectFieldPosition = EditorGUILayout.GetControlRect();
                                    EditorGUI.ObjectField(objectFieldPosition, foundObj, foundObj.GetType(), true);
                                    SelectObjectWhenObjectFieldClicked(objectFieldPosition, foundObj);
                                }
                            }
                        }
                    }

                    if (GUILayout.Button("Select Objects"))
                    {
                        SelectFound();
                    }
                    if (GUILayout.Button("Filter Scene Hierarchy"))
                    {
                        FilterFound();
                    }
                }
            }
        }

        protected virtual void OnFoundPrefabsGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_foundPrefabsFoldout = EditorGUILayout.Foldout(m_foundPrefabsFoldout, new GUIContent(string.Format("Prefabs Containing Object ({0})", m_foundPrefabs.Count)), true);

                if (m_foundPrefabs.Count > 0)
                {
                    if (m_foundPrefabsFoldout)
                    {
                        using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_foundPrefabsScrollPosition))
                        {
                            m_foundPrefabsScrollPosition = scrollView.scrollPosition;

                            foreach (UnityEngine.Object foundObj in m_foundPrefabs)
                            {
                                if (foundObj != null)
                                {
                                    Rect objectFieldPosition = EditorGUILayout.GetControlRect();
                                    EditorGUI.ObjectField(objectFieldPosition, foundObj, foundObj.GetType(), true);
                                    SelectObjectWhenObjectFieldClicked(objectFieldPosition, foundObj);
                                }
                            }
                        }
                    }

                    if (GUILayout.Button("Instantiate All Prefabs"))
                    {
                        foreach (GameObject foundGameObject in m_foundPrefabs)
                        {
                            GameObject instance = PrefabUtility.InstantiatePrefab(foundGameObject) as GameObject;
                            Undo.RegisterCreatedObjectUndo(instance, "Instantiate All Prefabs");
                        }
                    }
                }
            }
        }

        protected virtual void OnFoundScenesGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_foundScenesFoldout = EditorGUILayout.Foldout(m_foundScenesFoldout, new GUIContent(string.Format("Scenes Containing Object ({0})", m_foundScenes.Count)), true);

                if (m_foundScenes.Count > 0)
                {
                    if (m_foundScenesFoldout)
                    {
                        using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_foundScenesScrollPosition))
                        {
                            m_foundScenesScrollPosition = scrollView.scrollPosition;

                            foreach (SceneAsset foundScene in m_foundScenes)
                            {
                                if (foundScene != null)
                                {
                                    using (new EditorGUILayout.HorizontalScope())
                                    {
                                        EditorGUILayout.ObjectField(foundScene, foundScene.GetType(), true);

                                        GUIContent openSceneButtonLabel = new GUIContent("Open Scene");
                                        if (GUILayout.Button(openSceneButtonLabel, GUILayout.Width(GUI.skin.button.CalcSize(openSceneButtonLabel).x)))
                                        {
                                            string scenePath = AssetDatabase.GetAssetPath(foundScene);

                                            if (!string.IsNullOrEmpty(scenePath))
                                            {
                                                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                                                {
                                                    EditorSceneManager.OpenScene(scenePath);
                                                }
                                            }
                                            else
                                            {
                                                Debug.LogWarning(string.Format("Scene path could not be found for '{0}'", foundScene.name));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (GUILayout.Button("Open All Scenes"))
                    {
                        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                        {
                            foreach (SceneAsset foundScene in m_foundScenes)
                            {
                                if (foundScene != null)
                                {
                                    string scenePath = AssetDatabase.GetAssetPath(foundScene);

                                    if (!string.IsNullOrEmpty(scenePath))
                                    {
                                        EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void SelectObjectWhenObjectFieldClicked(Rect contentRect, UnityEngine.Object obj)
        {
            Event current = Event.current;
            EventType eventType = current.type;
            if (eventType == EventType.Used && Event.current.button == 0 && contentRect.Contains(Event.current.mousePosition))
            {
                Component component = obj as Component;
                if (component != null)
                {
                    obj = component.gameObject;
                }
                Selection.objects = new UnityEngine.Object[] { obj };
            }
        }
        
        public static void Search(UnityEngine.Object[] searchObjects, Func<UnityEngine.Object, bool> searchCondition, bool loadAndSearchSceneAssets, List<UnityEngine.Object> foundObjects, List<GameObject> foundPrefabs, List<SceneAsset> foundScenes)
        {
            if (searchObjects == null)
            {
                return;
            }
            try
            {
                for (int i = 0; i < searchObjects.Length; i++)
                {
                    UnityEngine.Object obj = searchObjects[i];

                    if (EditorUtility.DisplayCancelableProgressBar("Find", string.Format("Searching asset '{0}'", obj.name), ((float)i / (float)searchObjects.Length)))
                    {
                        break;
                    }

                    if (searchCondition(obj))
                    {
                        foundObjects.Add(obj);
                    }

                    GameObject gameobject = obj as GameObject;
                    SceneAsset sceneAsset = obj as SceneAsset;
                    if (gameobject != null)
                    {
                        List<UnityEngine.Object> foundObjs = new List<UnityEngine.Object>();

                        foundObjs.AddRange(gameobject.GetComponentsInChildren(typeof(Component), true));
                        foundObjs.AddRange(gameobject.GetComponentsInChildren(typeof(Transform), true).Select(x => x.gameObject).ToArray());
                        foundObjs = foundObjs.Distinct().ToList();

                        bool found = false;
                        foreach (UnityEngine.Object foundObj in foundObjs)
                        {
                            if (foundObj != null)
                            {
                                if (searchCondition(foundObj))
                                {
                                    found = true;
                                    foundObjects.Add(foundObj);
                                }
                            }
                        }

                        if (found)
                        {
                            if (!gameobject.scene.IsValid())
                            {
                                foundPrefabs.Add(gameobject);
                            }
                        }
                    }
                    else if (loadAndSearchSceneAssets && sceneAsset != null)
                    {
                        string scenePath = AssetDatabase.GetAssetPath(sceneAsset);

                        if (!string.IsNullOrEmpty(scenePath))
                        {
                            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                            {
                                EditorSceneManager.OpenScene(scenePath);
                            }

                            List<UnityEngine.Object> foundObjs = new List<UnityEngine.Object>();

                            bool found = false;
                            foreach (UnityEngine.Object foundObj in FindObjectsInScene(typeof(UnityEngine.Object)))
                            {
                                if (foundObj != null)
                                {
                                    if (searchCondition(foundObj))
                                    {
                                        found = true;
                                        foundObjects.Add(foundObj);
                                    }
                                }
                            }

                            if (found)
                            {
                                foundScenes.Add(sceneAsset);
                            }
                        }
                        else
                        {
                            Debug.LogWarning(string.Format("Scene path could not be found for '{0}'", sceneAsset.name));
                        }
                    }
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        protected void FilterFound()
        {
            string searchString = GetHierarchySearchString();
            if (searchString != null)
            {
                FilterSceneHierarchy(searchString, GetHierarchySearchMode());
            }
        }

        protected void SelectFound()
        {
            if (m_foundObjects.Count > 0)
            {
                Selection.objects = m_foundObjects.Select(x => (x is Component) ? ((Component)x).gameObject : x).ToArray();
            }
        }
        
        public void ClearSearch()
        {
            m_foundObjects.Clear();
            m_foundPrefabs.Clear();
            m_foundScenes.Clear();
        }

        public static void FilterSceneHierarchy(string searchString, SearchableEditorWindow.SearchMode searchMode)
        {
            foreach (SearchableEditorWindow sw in GetAllSceneHierarchyWindows())
            {
                typeof(SearchableEditorWindow).GetMethod("SetSearchFilter", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                    .Invoke(sw, new object[] { searchString, searchMode, true, false });
                typeof(SearchableEditorWindow).GetField("m_HasSearchFilterFocus", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                    .SetValue(sw, true);
                sw.Repaint();
            }
        }

        public static SearchableEditorWindow[] GetAllSceneHierarchyWindows()
        {
            Type windowType = typeof(Editor).Assembly.GetType("UnityEditor.SceneHierarchyWindow");
            UnityEngine.Object[] windows = Resources.FindObjectsOfTypeAll(windowType);
            return windows.Cast<SearchableEditorWindow>().ToArray();
        }

        public static T[] FindObjectsInScene<T>() where T : UnityEngine.Object
        {
            return FindObjectsInScene(typeof(T)).Cast<T>().ToArray();
        }
        
        public static UnityEngine.Object[] FindObjectsInScene(Type type)
        {
            List<UnityEngine.Object> objs = new List<UnityEngine.Object>();
            foreach (UnityEngine.Object obj in Resources.FindObjectsOfTypeAll(type))
            {
                GameObject gameObject = obj as GameObject;
                Component component = obj as Component;
                if (gameObject == null && component != null)
                {
                    gameObject = component.gameObject;
                }
                if (gameObject != null && gameObject.scene.IsValid() && gameObject.hideFlags != HideFlags.HideAndDontSave)
                {
                    objs.Add(obj);
                }
            }
            return objs.Distinct().ToArray();
        }

        public static T[] FindObjectsInAssets<T>() where T : UnityEngine.Object
        {
            return FindObjectsInAssets(typeof(T)).Cast<T>().ToArray();
        }

        public static UnityEngine.Object[] FindObjectsInAssets(Type type)
        {
            string[] paths = AssetDatabase.FindAssets("t: " + type.Name, new string[] { "Assets" }).Select(x => AssetDatabase.GUIDToAssetPath(x)).ToArray();
            List<UnityEngine.Object> objects = new List<UnityEngine.Object>();
            for (int i = 0; i < paths.Length; i++)
            {
                objects.Add(AssetDatabase.LoadAssetAtPath(paths[i], type));
            }
            return objects.ToArray();
        }
    }

}
#endif