#if UNITY_EDITOR
using ImprobableStudios.SearchPopupUtilities;
using System;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchProjectUtilities
{

    public class FindComponentWindow : SearchProjectWindow
    {
        protected static Type m_find;
        
        [MenuItem("Search/Find Component", false, 10)]
        public static void Init()
        {
            FindComponentWindow window = EditorWindow.GetWindow<FindComponentWindow>("Find");
            window.Show();
        }

        protected override void OnSearchGUI()
        {
            EditorGUI.BeginChangeCheck();
            m_find = SearchPopupGUI.ComponentTypeSearchPopupField(m_find, new GUIContent("Find what:", "The component type to search for"), null);
            if (EditorGUI.EndChangeCheck())
            {
                ClearSearch();
            }
        }

        protected override bool IsSearchValid()
        {
            return m_find != null;
        }

        protected override string GetHierarchySearchString()
        {
            if (m_find != null)
            {
                return m_find.FullName;
            }
            return null;
        }

        protected override SearchableEditorWindow.SearchMode GetHierarchySearchMode()
        {
            return SearchableEditorWindow.SearchMode.Type;
        }
        
        protected override bool IsSearchMatch(UnityEngine.Object obj)
        {
            if (m_find != null)
            {
                return m_find.IsAssignableFrom(obj.GetType());
            }
            return true;
        }

        protected override GUIContent GetSearchButtonLabel()
        {
            return new GUIContent("Find");
        }

        protected override string GetNoneFoundMessage()
        {
            if (m_find != null)
            {
                return string.Format("No components of type ({0}) found", m_find.FullName);
            }
            return null;
        }
    }

}
#endif