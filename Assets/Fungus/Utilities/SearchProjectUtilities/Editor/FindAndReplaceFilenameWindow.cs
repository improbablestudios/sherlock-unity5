#if UNITY_EDITOR
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchProjectUtilities
{

    public class FindAndReplaceFilenameWindow : EditorWindow
    {
        protected string m_find = "";

        protected string m_replace = "";

        protected bool m_renameFolders = true;

        protected bool m_renameFiles = true;

        protected bool m_renameChildren = true;

        protected SelectionMode m_selectionMode = SelectionMode.DeepAssets;

        [MenuItem("Search/Rename Using Find And Replace", false, 10)]
        private static void RenameUsingFindAndReplace()
        {
            FindAndReplaceFilenameWindow window = EditorWindow.GetWindow<FindAndReplaceFilenameWindow>("Find And Replace");
            window.Show();
        }

        private void OnSelectionChange()
        {
            Repaint();
        }

        void OnGUI()
        {
            m_find = EditorGUILayout.TextField(new GUIContent("Find what:", "The string to find in the name"), m_find);

            m_replace = EditorGUILayout.TextField(new GUIContent("Replace with:", "The string that will replace the found string in the name"), m_replace);

            m_renameFolders = EditorGUILayout.Toggle(new GUIContent("Rename folders", "Find and replace folder names"), m_renameFolders);

            m_renameFiles = EditorGUILayout.Toggle(new GUIContent("Rename files", "Find and replace file names"), m_renameFiles);

            m_renameChildren = EditorGUILayout.Toggle(new GUIContent("Rename children", "Find and replace child game object names"), m_renameChildren);

            bool deep = (m_selectionMode == SelectionMode.DeepAssets);
            deep = EditorGUILayout.Toggle(new GUIContent("Deep Search", "If the current selection contains folders, also search through all assets and folders that are within those folders"), deep);
            m_selectionMode = deep ? SelectionMode.DeepAssets : SelectionMode.Assets;

            UnityEngine.Object[] selectedObjects = Selection.GetFiltered<UnityEngine.Object>(m_selectionMode);

            bool enabled = GUI.enabled;

            if (selectedObjects.Length == 0)
            {
                GUI.enabled = false;
            }

            if (GUILayout.Button("Find and Replace"))
            {
                FindAndReplace(selectedObjects, m_find, m_replace, m_renameFolders, m_renameFiles, m_renameChildren);
            }

            GUI.enabled = enabled;

            if (selectedObjects.Length == 0)
            {
                EditorGUILayout.HelpBox("Select the files and/or folders you want to search in the project window", MessageType.Info);
            }
        }

        public static void FindAndReplace(UnityEngine.Object[] objects, string find, string replace, bool renameFolders, bool renameFiles, bool renameChildren)
        {
            try
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    UnityEngine.Object selectedObject = objects[i];

                    if (EditorUtility.DisplayCancelableProgressBar("Find And Replace", string.Format("Replacing '{0}' with '{1}' in '{2}'", find, replace, selectedObject.name), ((float)i / (float)objects.Length)))
                    {
                        break;
                    }

                    string newName = selectedObject.name.Replace(find, replace);

                    string path = AssetDatabase.GetAssetPath(selectedObject);
                    if (renameFolders)
                    {
                        if (Directory.Exists(path))
                        {
                            if (selectedObject.name != newName)
                            {
                                AssetDatabase.RenameAsset(path, newName);
                            }
                        }
                    }
                    else
                    {
                        if (renameFiles)
                        {
                            if (selectedObject.name != newName)
                            {
                                AssetDatabase.RenameAsset(path, newName);
                            }
                        }
                        if (renameChildren)
                        {
                            GameObject gameObject = selectedObject as GameObject;
                            if (gameObject != null)
                            {
                                FindAndReplaceGameObjectChildNames(gameObject, find, replace);
                            }
                            Texture2D texture2D = selectedObject as Texture2D;
                            if (texture2D != null)
                            {
                                FindAndReplaceSpriteChildNames(path, find, replace);
                            }
                        }
                    }
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        public static void FindAndReplaceGameObjectChildNames(GameObject gameObject, string find, string replace)
        {
            foreach (Transform child in gameObject.GetComponentsInChildren<Transform>(true))
            {
                string newChildName = child.name.Replace(find, replace);

                if (child.name != newChildName)
                {
                    child.name = newChildName;
                    EditorUtility.SetDirty(child);
                }
            }
        }

        public static void FindAndReplaceSpriteChildNames(string path, string find, string replace)
        {
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

            bool changed = false;
            SpriteMetaData[] spriteArray = textureImporter.spritesheet.ToArray();
            for (int i = 0; i < spriteArray.Length; i++)
            {
                string newName = spriteArray[i].name.Replace(find, replace);
                if (spriteArray[i].name != newName)
                {
                    spriteArray[i].name = newName;
                    changed = true;
                }
            }

            if (changed)
            {
                textureImporter.spritesheet = spriteArray;
                EditorUtility.SetDirty(textureImporter);
                textureImporter.SaveAndReimport();
            }
        }
    }

}
#endif