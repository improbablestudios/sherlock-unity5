﻿#if UNITY_EDITOR
using System;

namespace ImprobableStudios.SearchPopupUtilities
{

    [Flags]
    public enum SearchPopupWindowFlags
    {
        /// <summary>
        /// Hide the window header.
        /// </summary>
        HideHeader = 0x00001,
        /// <summary>
        /// Hide the window search bar.
        /// </summary>
        HideSearch = 0x00002,
        /// <summary>
        /// Hide the window path footer.
        /// </summary>
        HidePath = 0x00004,
        /// <summary>
        /// Hide the "Create New" option.
        /// </summary>
        HideCreateNewOption = 0x00008,
        /// <summary>
        /// Hide the "Default" option.
        /// </summary>
        HideDefaultOption = 0x00010,
        /// <summary>
        /// Show options in flat list with no folders
        /// </summary>
        DisableFolders = 0x00020,
    }

}

#endif