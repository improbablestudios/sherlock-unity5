﻿#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class UnityObjectReferenceSearchPopupWindow : UnityObjectSearchPopupWindow
    {
        protected enum ProjectLocation
        {
            Scene,
            Assets
        }

        protected class UnityObjectReferenceElement : ItemElement
        {
            public UnityObjectReferenceElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj) : base(level, content, style, menuPath, obj)
            {
            }
        }

        protected class NewUnityObjectReferenceElement : NewUnityObjectElement
        {
            public NewUnityObjectReferenceElement() : base()
            {
                m_name = "Create New " + ObjectNames.NicifyVariableName(s_baseType.Name);
                m_content.text = m_name;
            }
            
            protected override string GetExtension()
            {
                if (typeof(GameObject).IsAssignableFrom(s_baseType) || typeof(Component).IsAssignableFrom(s_baseType))
                {
                    return "prefab";
                }
                return "asset";
            }
            
            protected override void DrawControls()
            {
                DrawNameControl();

                DrawTypeControl();

                if (typeof(GameObject).IsAssignableFrom(s_baseType) || typeof(Component).IsAssignableFrom(s_baseType))
                {
                    DrawProjectLocationControl();
                }
            }

            protected virtual void DrawProjectLocationControl()
            {
                GUILayout.Label("Project Location");
                GUI.SetNextControlName("NewProjectLocation");
                NewItemProjectLocation = (ProjectLocation)EditorGUILayout.EnumPopup(NewItemProjectLocation);
            }

            protected virtual void DrawTypeControl()
            {
                if (s_componentTypes != null && s_componentTypes.Length > 0)
                {
                    GUILayout.Label("Type");
                    GUI.SetNextControlName("NewType");
                    NewItemType = UnityObjectTypeField(EditorGUILayout.GetControlRect(), NewItemType, GUIContent.none, s_componentTypes);
                }
                else if (!s_baseType.IsInstantiable())
                {
                    GUILayout.Label("Type");
                    GUI.SetNextControlName("NewType");
                    NewItemType = UnityObjectTypeField(EditorGUILayout.GetControlRect(), NewItemType, GUIContent.none, s_baseType);
                }
                else
                {
                    NewItemType = s_baseType;
                }
            }

            protected override string[] GetControlNames()
            {
                if (typeof(GameObject).IsAssignableFrom(s_baseType) || typeof(Component).IsAssignableFrom(s_baseType))
                {
                    return new string[] { "NewName", "NewType", "NewProjectLocation", "NewSaveLocation" };
                }
                else
                {
                    return new string[] { "NewName", "NewType", "NewSaveLocation" };
                }
            }
            
            protected override object CreateNewItem()
            {
                if (typeof(ScriptableObject).IsAssignableFrom(s_baseType))
                {
                    if (SetTargetDirectory())
                    {
                        return CreateScriptableObject(Name, NewItemType, GetTargetPath());
                    }
                }
                else if (typeof(GameObject).IsAssignableFrom(s_baseType) || typeof(Component).IsAssignableFrom(s_baseType))
                {
                    if (NewItemProjectLocation == ProjectLocation.Assets)
                    {
                        if (SetTargetDirectory())
                        {
                            return CreatePrefabObject(Name, NewItemType, GetTargetPath());
                        }
                    }
                    else if (NewItemProjectLocation == ProjectLocation.Scene)
                    {
                        return CreateSceneObject(NewItemName, NewItemType);
                    }
                }

                return null;
            }

            protected static UnityEngine.Object CreateScriptableObject(string name, Type type, string path)
            {
                if (!string.IsNullOrEmpty(path))
                {
                    ScriptableObject scriptableObject = ScriptableObject.CreateInstance(type);
                    AssetDatabase.CreateAsset(scriptableObject, path);
                    return scriptableObject;
                }
                return null;
            }

            protected static UnityEngine.Object CreatePrefabObject(string name, Type type, string path)
            {
                if (!string.IsNullOrEmpty(path))
                {
                    GameObject go = null;

                    if (typeof(Component).IsAssignableFrom(type))
                    {
                        go = new GameObject(name, type);
                    }
                    else if (typeof(GameObject).IsAssignableFrom(type))
                    {
                        go = new GameObject(name);
                    }

                    if (go == null)
                    {
                        return null;
                    }

                    go.transform.position = Vector3.zero;

                    GameObject savedPrefab = PrefabUtility.SaveAsPrefabAsset(go, path);

                    Undo.DestroyObjectImmediate(go);

                    Selection.activeGameObject = savedPrefab;

                    Undo.RegisterCreatedObjectUndo(savedPrefab, "Create Object");

                    if (typeof(Component).IsAssignableFrom(type))
                    {
                        return savedPrefab.GetComponent(type);
                    }
                    return savedPrefab;
                }
                return null;
            }

            protected static UnityEngine.Object CreateSceneObject(string name, Type type)
            {
                GameObject go = null;

                if (typeof(Component).IsAssignableFrom(type))
                {
                    go = new GameObject(name, type);
                }
                else if (typeof(GameObject).IsAssignableFrom(type))
                {
                    go = new GameObject(name);
                }

                if (go == null)
                {
                    return null;
                }

                SceneView view = SceneView.lastActiveSceneView;
                if (view != null)
                {
                    Camera sceneCam = view.camera;
                    Vector3 pos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
                    pos.z = 0f;
                    go.transform.position = pos;
                }

                Selection.activeGameObject = go;

                go.transform.position = Vector3.zero;

                Undo.RegisterCreatedObjectUndo(go, "Create Object");

                if (typeof(Component).IsAssignableFrom(type))
                {
                    return go.GetComponent(type);
                }
                return go;
            }

            protected override string GetError()
            {
                if (NewItemType == null)
                {
                    return "No type selected";
                }
                return "";
            }
            
            protected static Type UnityObjectTypeField(Rect position, Type type, GUIContent label, Type baseComponentType)
            {
                Type[] componentTypes = baseComponentType.FindAllInstantiableDerivedTypes();
                return UnityObjectTypeField(position, type, label, componentTypes);
            }

            protected static Type UnityObjectTypeField(Rect position, Type type, GUIContent label, Type[] componentTypes)
            {
                EditorGUI.BeginChangeCheck();
                Type[] orderedTypes = componentTypes.OrderBy(i => i.Name).ToArray();
                GUIContent[] orderedLabels = orderedTypes.Select(i => new GUIContent(i.Name)).ToArray();
                int selectedIndex = Array.IndexOf(orderedTypes, type);
                selectedIndex = EditorGUI.Popup(position, label, selectedIndex, orderedLabels);
                if (EditorGUI.EndChangeCheck())
                {
                    type = orderedTypes[selectedIndex];
                }
                return type;
            }
        }

        protected static Type s_baseType;

        protected static Type[] s_componentTypes;

        protected static Dictionary<string, List<UnityEngine.Object>> s_objectDictionary = new Dictionary<string, List<UnityEngine.Object>>();

        protected static UnityEngine.Object[] s_sortedItems = new UnityEngine.Object[0];

        protected static GUIContent[] s_sortedLabels = new GUIContent[0];

        protected static GUIStyle[] s_sortedStyles = new GUIStyle[0];

        protected static Type s_newItemType;

        private static ProjectLocation s_newItemProjectLocation;

        protected static UnityEngine.Object[] s_objects;

        protected static UnityEngine.Object s_targetObject;

        protected static Type NewItemType
        {
            get
            {
                return s_newItemType;
            }
            set
            {
                s_newItemType = value;
            }
        }

        protected static ProjectLocation NewItemProjectLocation
        {
            get
            {
                return s_newItemProjectLocation;
            }
            set
            {
                s_newItemProjectLocation = value;
            }
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, SerializedProperty property, Type baseType, Type[] componentTypes, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, property, null, baseType, componentTypes, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, SerializedProperty property, GUIContent defaultOptionLabel, Type baseType, Type[] componentTypes, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, property, defaultOptionLabel, baseType, componentTypes, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, SerializedProperty property, GUIContent defaultOptionLabel, Type baseType, Type[] componentTypes, SearchPopupWindowFlags flags = 0) where T : UnityObjectReferenceSearchPopupWindow
        {
            if (componentTypes != null)
            {
                componentTypes = componentTypes.SelectMany(x => x.FindAllInstantiableDerivedTypes()).Distinct().ToArray();
            }

            s_targetObject = property.serializedObject.targetObject;
            s_objects = null;
            s_baseType = baseType;
            s_componentTypes = componentTypes;
            
            return SearchPopupWindow.Show<T>(controlID, rect, property.objectReferenceValue, defaultOptionLabel, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, Type baseType, Type[] componentTypes, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, null, null, baseType, componentTypes, targetObject, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, Type baseType, Type[] componentTypes, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, baseType, componentTypes, targetObject, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, UnityEngine.Object selectedObject, Type baseType, Type[] componentTypes, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, selectedObject, null, baseType, componentTypes, targetObject, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, UnityEngine.Object selectedObject, GUIContent defaultOptionLabel, Type baseType, Type[] componentTypes, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, selectedObject, defaultOptionLabel, baseType, componentTypes, targetObject, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, UnityEngine.Object selectedObject, GUIContent defaultOptionLabel, Type baseType, Type[] componentTypes, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0) where T : UnityObjectReferenceSearchPopupWindow
        {
            if (componentTypes != null)
            {
                componentTypes = componentTypes.SelectMany(x => x.FindAllInstantiableDerivedTypes()).Distinct().ToArray();
            }

            s_targetObject = targetObject;
            s_objects = null;
            s_baseType = baseType;
            s_componentTypes = componentTypes;
            
            return SearchPopupWindow.Show<T>(controlID, rect, selectedObject, defaultOptionLabel, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, Type baseType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, null, null, baseType, objects, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, Type baseType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, baseType, objects, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, UnityEngine.Object selectedObject, Type baseType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, selectedObject, null, baseType, objects, flags);
        }

        public static UnityObjectReferenceSearchPopupWindow Show(int controlID, Rect rect, UnityEngine.Object selectedObject, GUIContent defaultOptionLabel, Type baseType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            return Show<UnityObjectReferenceSearchPopupWindow>(controlID, rect, selectedObject, defaultOptionLabel, baseType, objects, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, UnityEngine.Object selectedObject, GUIContent defaultOptionLabel, Type baseType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0) where T : UnityObjectReferenceSearchPopupWindow
        {
            s_targetObject = null;
            s_objects = objects;
            s_baseType = baseType;
            s_componentTypes = null;

            return SearchPopupWindow.Show<T>(controlID, rect, selectedObject, defaultOptionLabel, flags);
        }

        public static UnityEngine.Object ProcessSelectedObject(int controlID, UnityEngine.Object selectedObject)
        {
            return SearchPopupWindow.ProcessSelectedItem(controlID, selectedObject) as UnityEngine.Object;
        }

        public static void ProcessSetObjectProperty(int controlID, SerializedProperty property)
        {
            SearchPopupWindow.ProcessSelectedItem(
                controlID,
                (object obj) =>
                {
                    UnityEngine.Object selectedObject = obj as UnityEngine.Object;

                    property.objectReferenceValue = selectedObject;
                });
        }

        protected override string GetWindowName()
        {
            return ObjectNames.NicifyVariableName(s_baseType.Name);
        }

        protected override ItemElement GetItemElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj)
        {
            return new UnityObjectReferenceElement(level, content, style, menuPath, obj);
        }

        protected override NewItemElement GetNewItemElement()
        {
            if (typeof(GameObject).IsAssignableFrom(s_baseType) || typeof(Component).IsAssignableFrom(s_baseType) || typeof(ScriptableObject).IsAssignableFrom(s_baseType))
            {
                return new NewUnityObjectReferenceElement();
            }
            return null;
        }

        protected override object[] GetSortedItems()
        {
            return s_sortedItems;
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            return s_sortedLabels;
        }

        protected override GUIStyle[] GetSortedStyles(object[] objs)
        {
            return s_sortedStyles;
        }

        protected override string DefaultCreateNewItemName()
        {
            if (s_baseType != null)
            {
                return s_baseType.Name + "Name";
            }

            return base.DefaultCreateNewItemName();
        }

        protected override string SearchSaveKey()
        {
            if (s_baseType != null)
            {
                return s_baseType.AssemblyQualifiedName + base.SearchSaveKey();
            }

            return base.SearchSaveKey();
        }

        public static List<UnityEngine.Object> GetSelfObjectList(UnityEngine.Object targetObject, Type baseType, Type[] componentTypes)
        {
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                GameObject targetGameObject = targetObject.GetGameObject();
                if (targetGameObject != null)
                {
                    if (baseType == typeof(GameObject) && componentTypes != null)
                    {
                        foreach (Type t in componentTypes)
                        {
                            selfObjectList.AddRange(ResourceTracker.FindGameObjectsInGameObjectWithComponentType(targetGameObject, t));
                        }
                    }
                    else
                    {
                        if (componentTypes == null)
                        {
                            selfObjectList.AddRange(ResourceTracker.FindObjectsInGameObjectOfType(targetGameObject, baseType));
                        }
                        else
                        {
                            foreach (Type t in componentTypes)
                            {
                                selfObjectList.AddRange(ResourceTracker.FindObjectsInGameObjectOfType(targetGameObject, t));
                            }
                        }
                    }
                    selfObjectList = selfObjectList.ToList();
                }
            }
            return selfObjectList;
        }

        public static List<UnityEngine.Object> GetSceneObjectList(Type baseType, Type[] componentTypes)
        {
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (baseType == typeof(GameObject) && componentTypes != null)
            {
                foreach (Type t in componentTypes)
                {
                    sceneObjectList.AddRange(ResourceTracker.FindLoadedInstanceGameObjectsWithComponentType(t));
                }
            }
            else
            {
                if (componentTypes == null)
                {
                    sceneObjectList.AddRange(ResourceTracker.FindLoadedInstanceObjectsOfType(baseType));
                }
                else
                {
                    foreach (Type t in componentTypes)
                    {
                        sceneObjectList.AddRange(ResourceTracker.FindLoadedInstanceObjectsOfType(t));
                    }
                }
            }
            sceneObjectList = sceneObjectList.Where(x => ResourceTracker.IsObjectSavedInBuild(x)).ToList();
            return sceneObjectList;
        }

        public static List<UnityEngine.Object> GetAssetObjectList(Type baseType, Type[] componentTypes)
        {
            List<UnityEngine.Object> assetObjectList = new List<UnityEngine.Object>();
            if (typeof(GameObject).IsAssignableFrom(baseType) && componentTypes != null)
            {
                foreach (Type t in componentTypes)
                {
                    assetObjectList.AddRange(ResourceTracker.GetCachedPrefabGameObjectsWithComponentType(t));
                }
            }
            else if (typeof(GameObject).IsAssignableFrom(baseType) || typeof(Component).IsAssignableFrom(baseType))
            {
                if (componentTypes == null)
                {
                    assetObjectList.AddRange(ResourceTracker.GetCachedPrefabObjectsOfType(baseType));
                }
                else
                {
                    foreach (Type t in componentTypes)
                    {
                        assetObjectList.AddRange(ResourceTracker.GetCachedPrefabObjectsOfType(t));
                    }
                }
            }
            else
            {
                if (componentTypes == null)
                {
                    assetObjectList.AddRange(ResourceTracker.GetCachedAssetObjectsOfType(baseType));
                }
                else
                {
                    foreach (Type t in componentTypes)
                    {
                        assetObjectList.AddRange(ResourceTracker.GetCachedAssetObjectsOfType(t));
                    }
                }
            }
            assetObjectList = assetObjectList.Where(x => ResourceTracker.IsObjectSavedInBuild(x)).ToList();
            return assetObjectList;
        }

        public static List<UnityEngine.Object> GetObjectList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfObjectList(targetObject, baseType, componentTypes).Distinct().Where(i => i != null).ToList();
                if (targetObject != null && targetObject.GetGameObject() != null && !UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();
            }
            List<UnityEngine.Object> assetObjectList = GetAssetObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();

            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            objectList.AddRange(selfObjectList);
            objectList.AddRange(sceneObjectList);
            objectList.AddRange(assetObjectList);

            return objectList;
        }

        protected virtual Dictionary<string, List<UnityEngine.Object>> GetObjectDictionary(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfObjectList(targetObject, baseType, componentTypes).Distinct().Where(i => i != null).ToList();
                if (targetObject != null && targetObject.GetGameObject() != null && !UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();
            }
            List<UnityEngine.Object> assetObjectList = GetAssetObjectList(baseType, componentTypes).Distinct().Where(i => i != null).ToList();

            return new Dictionary<string, List<UnityEngine.Object>>
            {
                {
                    "Self", selfObjectList
                },
                {
                    "Scene", sceneObjectList
                },
                {
                    "Assets", assetObjectList
                },
            };
        }

        protected override void PrepareItems()
        {
            if (s_baseType != null)
            {
                if (s_objects != null)
                {
                    PrepareItems(s_baseType, s_objects);
                }
                else
                {
                    PrepareItems(s_baseType, s_componentTypes, s_targetObject);
                }
            }
        }

        protected virtual void PrepareItems(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            Dictionary<string, List<UnityEngine.Object>> objectDictionary = GetObjectDictionary(baseType, componentTypes, targetObject);

            PrepareItems(objectDictionary, baseType);
        }

        protected virtual void PrepareItems(Type baseType, UnityEngine.Object[] objects)
        {
            Dictionary<string, List<UnityEngine.Object>> objectDictionary = new Dictionary<string, List<UnityEngine.Object>>();
            objectDictionary[""] = objects.ToList();

            PrepareItems(objectDictionary, baseType);
        }

        protected virtual void PrepareItems(Dictionary<string, List<UnityEngine.Object>> objectDictionary, Type baseType)
        {
            List<KeyValuePair<string, UnityEngine.Object>> sortedPairs = new List<KeyValuePair<string, UnityEngine.Object>>();

            List<List<string>> pathLists = new List<List<string>>();
            List<string> longestCommonPaths = new List<string>();
            int numNonEmptyCategories = 0;
            foreach (KeyValuePair<string, List<UnityEngine.Object>> kvp in objectDictionary)
            {
                string key = kvp.Key;
                List<UnityEngine.Object> list = kvp.Value;
                List<string> paths = list.Select(x => GetObjectPopupPath(baseType, x)).ToList();
                pathLists.Add(paths);
                longestCommonPaths.Add(GetLongestCommonPath(paths));
                if (list.Count > 0)
                {
                    numNonEmptyCategories++;
                }
            }

            bool prefixNecessary = (numNonEmptyCategories > 1);

            int index = 0;
            foreach (KeyValuePair<string, List<UnityEngine.Object>> kvp in objectDictionary)
            {
                string key = kvp.Key;
                List<UnityEngine.Object> list = kvp.Value;
                List<string> pathsList = pathLists[index];
                string longestCommonPath = longestCommonPaths[index];

                for (int j = 0; j < list.Count; ++j)
                {
                    UnityEngine.Object obj = list[j];

                    if (obj == null)
                    {
                        continue;
                    }

                    string prefix = "";
                    if (prefixNecessary)
                    {
                        prefix += key + "/";
                    }

                    sortedPairs.Add(new KeyValuePair<string, UnityEngine.Object>(GetObjectPopupString(prefix, pathsList[j], longestCommonPath), obj));
                }
                index++;
            }

            sortedPairs.Sort(ComparePairs);

            s_objectDictionary = objectDictionary;
            s_sortedItems = sortedPairs.Select(x => x.Value).ToArray();
            s_sortedLabels = new GUIContent[sortedPairs.Count];
            for (int i = 0; i < sortedPairs.Count; i++)
            {
                s_sortedLabels[i] = new GUIContent(sortedPairs[i].Key, GetSelectedDisplayOptionContent(sortedPairs[i].Value, s_baseType, m_defaultOptionLabel).image);
            }
            s_sortedStyles = new GUIStyle[sortedPairs.Count];
            for (int i = 0; i < sortedPairs.Count; i++)
            {
                s_sortedStyles[i] = GetSelectedDisplayOptionStyle(sortedPairs[i].Value, s_baseType);
            }
        }

        protected int GetCategory(string label)
        {
            int i = 0;
            foreach (string category in s_objectDictionary.Keys)
            {
                if (label.StartsWith(category, StringComparison.Ordinal))
                {
                    return i + 1;
                }
                i++;
            }
            return 0;
        }
        
        protected int ComparePairs(KeyValuePair<string, UnityEngine.Object> x, KeyValuePair<string, UnityEngine.Object> y)
        {
            int compare = 0;

            if (compare == 0)
            {
                int xCategory = GetCategory(x.Key);
                int yCategory = GetCategory(y.Key);

                compare = (xCategory.CompareTo(yCategory));
            }

            if (compare == 0)
            {
                compare = x.Key.CompareTo(y.Key);
            }

            return compare;
        }

        public static string GetObjectPopupString(string prefix, string path, string longestCommonPath)
        {
            string optionString = path;
            optionString += " ";
            if (longestCommonPath.Length > 0)
            {
                if (longestCommonPath.Length + 1 < optionString.Length)
                {
                    optionString = optionString.Substring(longestCommonPath.Length + 1);
                }
            }
            return prefix + optionString;
        }
        
        public static string GetObjectPopupPath(Type type, UnityEngine.Object obj)
        {
            string key = "";

            GameObject gameObject = obj.GetGameObject();
            Component component = obj as Component;
            ScriptableObject scriptableObject = obj as ScriptableObject;

            if (gameObject != null && gameObject.transform != gameObject.transform.root)
            {
                if (!gameObject.scene.IsValid())
                {
                    key = AssetDatabase.GetAssetPath(obj);
                    key = Path.GetFileNameWithoutExtension(key);
                    string pathFromRoot = gameObject.transform.GetPathFromRoot(true, false);
                    if (!string.IsNullOrEmpty(pathFromRoot))
                    {
                        key += "/" + pathFromRoot;
                    }
                }
                else
                {
                    string pathFromRoot = gameObject.transform.GetPathFromRoot(true, false);
                    if (!string.IsNullOrEmpty(pathFromRoot))
                    {
                        key = pathFromRoot;
                    }
                }
            }
            if (scriptableObject != null)
            {
                string path = AssetDatabase.GetAssetPath(scriptableObject);
                if (!string.IsNullOrEmpty(path))
                {
                    key = path;
                    key = Path.GetFileNameWithoutExtension(key);
                    if (AssetDatabase.IsMainAsset(scriptableObject))
                    {
                        key = Path.GetDirectoryName(key);
                    }
                }
            }

            IIdentifiable identifiable = obj as IIdentifiable;

            string name = obj.GetUniqueName(type, false);
            if (identifiable != null && identifiable.GetName() != obj.name && identifiable.GetName() != null)
            {
                name = identifiable.GetName();
            }
            else
            {
                name = obj.GetUniqueName();
            }

            if (!string.IsNullOrEmpty(key))
            {
                return key + "/" + name;
            }
            else
            {
                return name;
            }
        }
        
        public static GUIContent GetSelectedDisplayOptionContent(UnityEngine.Object obj, Type type, GUIContent defaultOptionLabel)
        {
            GUIContent fieldContent = EditorGUIUtility.ObjectContent(obj, type);

            if (obj != null)
            {
                IIdentifiable identifiable = obj as IIdentifiable;

                string name = obj.GetUniqueName(type, false);
                if (identifiable != null && identifiable.GetName() != obj.name && identifiable.GetName() != null)
                {
                    fieldContent.text = identifiable.GetName();
                }
                else
                {
                    fieldContent.text = obj.GetUniqueName();
                }
                if (identifiable != null && identifiable.GetIcon() != null)
                {
                    fieldContent.image = identifiable.GetIcon();
                }
            }
            else
            {
                string typeSuffix = " (" + type.Name + ")";

                if (defaultOptionLabel != null)
                {
                    fieldContent.text = defaultOptionLabel.text + typeSuffix;
                }
                else
                {
                    fieldContent.text = SearchPopupWindow.DEFAULT_OPTION_LABEL.text + typeSuffix;
                }
            }
            
            return fieldContent;
        }


        public static GUIStyle GetSelectedDisplayOptionStyle(UnityEngine.Object obj, Type type)
        {
            GUIStyle fieldStyle = null;

            if (obj != null)
            {
                IIdentifiable identifiable = obj as IIdentifiable;

                if (identifiable != null && identifiable.GetStyle() != null)
                {
                    fieldStyle = identifiable.GetStyle();
                }
            }

            return fieldStyle;
        }
    }

}
#endif