#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class ComponentTypeSearchPopupWindow : UnityObjectTypeSearchPopupWindow
    {
        protected class NewComponentScriptElement : NewScriptElement
        {
            public NewComponentScriptElement() : base()
            {
                m_name = "Create New " + typeof(Component).Name + " Type";
                m_content.text = m_name;
            }

            protected override string GetNewItemTemplateFolder()
            {
                return System.IO.Path.Combine(EditorApplication.applicationContentsPath, "Resources/Editor/ScriptTemplates");
            }

            protected override string GetNewItemTemplateFilename()
            {
                Language s_Lang = ComponentTypeSearchPopupWindow.s_language;
                if (s_Lang != Language.JavaScript)
                {
                    if (s_Lang != Language.CSharp)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    return "81-C# Script-NewBehaviourScript.cs.txt";
                }
                else
                {
                    return "82-Javascript-NewBehaviourScript.js.txt";
                }
            }
        }

        protected Dictionary<Type, Texture> s_monoScriptIcons;

        protected override NewItemElement GetNewItemElement()
        {
            return new NewComponentScriptElement();
        }
        
        protected override string GetWindowName()
        {
            return "Component";
        }

        protected override void PrepareItems()
        {
            base.PrepareItems();

            if (s_monoScriptIcons == null)
            {
                s_monoScriptIcons = new Dictionary<Type, Texture>();

                string[] guids = AssetDatabase.FindAssets("t:" + typeof(MonoScript).Name);

                foreach (string guid in guids)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guid);
                    MonoScript monoScript = AssetDatabase.LoadAssetAtPath<MonoScript>(path);

                    if (monoScript != null)
                    {
                        Type scriptType = monoScript.GetClass();
                        if (scriptType != null)
                        {
                            s_monoScriptIcons[scriptType] = AssetDatabase.GetCachedIcon(path);
                        }
                    }
                }
            }
        }

        protected override object[] GetSortedItems()
        {
            string[] unityNamespaceNames = GetUnityNamespaceNames();
            List<object> types = new List<object>();
            foreach (string command in Unsupported.GetSubmenusCommands("Component"))
            {
                Type componentType = null;
                if (command == "ADD")
                {
                }
                else if (command.StartsWith("SCRIPT", StringComparison.Ordinal))
                {
                    int scriptId = int.Parse(command.Substring(6));
                    UnityEngine.Object obj = EditorUtility.InstanceIDToObject(scriptId);
                    MonoScript monoScript = obj as MonoScript;
                    componentType = monoScript.GetClass();
                }
                else
                {
                    int classId = int.Parse(command);
                    Type unityTypeType = typeof(Editor).Assembly.GetType("UnityEditor.UnityType");
                    object unityType = unityTypeType.GetMethod("FindTypeByPersistentTypeID", BindingFlags.Static | BindingFlags.Public)
                        .Invoke(null, new object[] { classId });
                    string name = unityTypeType.GetProperty("name", BindingFlags.Instance | BindingFlags.Public).GetValue(unityType, null) as string;
                    componentType = ConvertUnityTypeNameToType(name);
                    if (componentType == null)
                    {
                        foreach (string namespaceName in unityNamespaceNames)
                        {
                            componentType = ConvertUnityTypeNameToType(namespaceName, name);
                            if (componentType != null)
                            {
                                break;
                            }
                        }
                    }
                }
                types.Add(componentType);
            }
            types.RemoveAt(0);
            return types.ToArray();
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            List<GUIContent> subMenuList = Unsupported.GetSubmenus("Component").Select(x => new GUIContent(x)).ToList();
            subMenuList.RemoveAt(0);
            for (int i = 0; i < subMenuList.Count; i++)
            {
                Type type = objs[i] as Type;
                subMenuList[i].text = subMenuList[i].text.Substring(("Component/").Length);
                Texture icon = AssetPreview.GetMiniTypeThumbnail(type);
                if (type != null && icon == null)
                {
                    if (s_monoScriptIcons.ContainsKey(type))
                    {
                        icon = s_monoScriptIcons[type];
                    }
                }
                subMenuList[i].image = icon;
            }
            return subMenuList.ToArray();
        }

        protected string[] GetUnityNamespaceNames()
        {
            return new string[] { "AI", "Video", "Rendering", "Tilemaps", "Playables", "XR.WSA", "Animations", "Animations", "Experimental.U2D" };
        }

        protected Type ConvertUnityTypeNameToType(string typeName)
        {
            return Type.GetType("UnityEngine." + typeName + ", UnityEngine");
        }

        protected Type ConvertUnityTypeNameToType(string namespaceName, string typeName)
        {
            return Type.GetType("UnityEngine." + namespaceName + "." + typeName + ", UnityEngine");
        }

        protected override string FormatNewName(string str)
        {
            if (str == null)
            {
                return str;
            }

            if (str.Length < 2)
            {
                return str.ToUpper();
            }

            string[] words = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string result = "";

            foreach (string word in words)
            {
                result += word.Substring(0, 1).ToUpper() + word.Substring(1);
            }

            return result;
        }

        public static ComponentTypeSearchPopupWindow Show(int controlID, Rect rect, SearchPopupWindowFlags flags = 0)
        {
            return Show<ComponentTypeSearchPopupWindow>(controlID, rect, null, null, flags);
        }

        public static ComponentTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, SearchPopupWindowFlags flags = 0)
        {
            return Show<ComponentTypeSearchPopupWindow>(controlID, rect, selectedType, null, flags);
        }

        public static ComponentTypeSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<ComponentTypeSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, flags);
        }

        public static ComponentTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<ComponentTypeSearchPopupWindow>(controlID, rect, selectedType, defaultOptionLabel, flags);
        }

        public static T Show<T>(int controlID, Rect rect, Type selectedType, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0) where T : ComponentTypeSearchPopupWindow
        {
            return SearchPopupWindow.Show<T>(controlID, rect, selectedType, defaultOptionLabel, flags);
        }

        public static Type ProcessSelectedType(int controlID, Type selectedObject)
        {
            return SearchPopupWindow.ProcessSelectedItem(controlID, selectedObject) as Type;
        }

        public static void ProcessSetTypeNameProperty(int controlID, SerializedProperty property)
        {
            SearchPopupWindow.ProcessSelectedItem(
                controlID,
                (object obj) =>
                {
                    Type selectedType = obj as Type;

                    if (selectedType == null)
                    {
                        property.stringValue = null;
                    }
                    else
                    {
                        property.stringValue = selectedType.AssemblyQualifiedName;
                    }
                });
        }

        public static void ProcessAddComponent(int controlID, GameObject[] gameObjects)
        {
            SearchPopupWindow.ProcessSelectedItem(
                controlID,
                (object obj) =>
                {
                    Type selectedType = obj as Type;

                    foreach (GameObject gameObject in gameObjects)
                    {
                        Undo.RecordObject(gameObject, "Add Component");

                        Undo.AddComponent(gameObject, selectedType);
                    }
                });
        }

        public static GUIContent GetSelectedDisplayOptionContent(Type type, GUIContent defaultOptionLabel)
        {
            if (type == null)
            {
                if (defaultOptionLabel != null)
                {
                    return defaultOptionLabel;
                }
                else
                {
                    return DEFAULT_OPTION_LABEL;
                }
            }

            return new GUIContent(ObjectNames.NicifyVariableName(type.Name));
        }
    }

}
#endif