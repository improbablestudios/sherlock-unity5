﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class GenericSearchPopupWindow : SearchPopupWindow
    {
        protected static object[] s_values;

        protected static GUIContent[] s_displayOptions;

        public static GenericSearchPopupWindow Show(int controlID, Rect rect, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<GenericSearchPopupWindow>(controlID, rect, null, null, values, displayOptions, flags);
        }

        public static GenericSearchPopupWindow Show(int controlID, Rect rect, object selectedObject, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<GenericSearchPopupWindow>(controlID, rect, selectedObject, null, values, displayOptions, flags);
        }

        public static GenericSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<GenericSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, values, displayOptions, flags);
        }

        public static GenericSearchPopupWindow Show(int controlID, Rect rect, object selectedObject, GUIContent defaultOptionLabel, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<GenericSearchPopupWindow>(controlID, rect, selectedObject, defaultOptionLabel, values, displayOptions, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, object selectedObject, GUIContent defaultOptionLabel, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0) where T : GenericSearchPopupWindow
        {
            s_values = values;
            s_displayOptions = displayOptions;

            return SearchPopupWindow.Show<T>(controlID, rect, selectedObject, defaultOptionLabel, flags);
        }
        
        public static GUIContent GetSelectedDisplayOptionContent(object value, object[] values, GUIContent[] displayOptions, GUIContent defaultOptionLabel)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (value == values[i])
                {
                    return displayOptions[i];
                }
            }

            if (value == null)
            {
                if (defaultOptionLabel != null)
                {
                    return defaultOptionLabel;
                }
                else
                {
                    return DEFAULT_OPTION_LABEL;
                }
            }

            return new GUIContent(value.ToString());
        }

        protected override object[] GetSortedItems()
        {
            return s_values;
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            return s_displayOptions;
        }
    }

}
#endif