﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class StringSearchPopupWindow : SearchPopupWindow
    {
        protected static string[] s_values;

        protected static GUIContent[] s_displayOptions;

        public static StringSearchPopupWindow Show(int controlID, Rect rect, string[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<StringSearchPopupWindow>(controlID, rect, null, null, values, displayOptions, flags);
        }

        public static StringSearchPopupWindow Show(int controlID, Rect rect, string selectedString, string[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<StringSearchPopupWindow>(controlID, rect, selectedString, null, values, displayOptions, flags);
        }

        public static StringSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<StringSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, values, displayOptions, flags);
        }

        public static StringSearchPopupWindow Show(int controlID, Rect rect, string selectedString, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return Show<StringSearchPopupWindow>(controlID, rect, selectedString, defaultOptionLabel, values, displayOptions, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, string selectedString, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0) where T : StringSearchPopupWindow
        {
            s_values = values;
            s_displayOptions = displayOptions;

            return SearchPopupWindow.Show<T>(controlID, rect, selectedString, defaultOptionLabel, flags);
        }
        
        public static string ProcessSelectedString(int controlID, string selectedObject)
        {
            return SearchPopupWindow.ProcessSelectedItem(controlID, selectedObject) as string;
        }
        
        public static GUIContent GetSelectedDisplayOptionContent(string value, string[] values, GUIContent[] displayOptions, GUIContent defaultOptionLabel)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (value == values[i])
                {
                    return displayOptions[i];
                }
            }

            if (string.IsNullOrEmpty(value))
            {
                if (defaultOptionLabel != null)
                {
                    return defaultOptionLabel;
                }
                else
                {
                    return DEFAULT_OPTION_LABEL;
                }
            }

            return new GUIContent(value);
        }

        protected override object[] GetSortedItems()
        {
            return s_values;
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            return s_displayOptions;
        }
    }

}
#endif