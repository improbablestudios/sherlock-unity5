#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    public abstract class SearchPopupWindow : EditorWindow
    {
        public static readonly GUIContent DEFAULT_OPTION_LABEL = new GUIContent("<None>");

        public delegate void OnSearchDelegate(object[] objs);

        public delegate void OnSelectDelegate(object obj);

        protected abstract class Element : IComparable
        {
            protected int m_level;

            protected string m_name;

            protected string m_menuPath;

            protected GUIContent m_content;

            protected GUIStyle m_style;

            public int Level
            {
                get
                {
                    return m_level;
                }
            }

            public string Name
            {
                get
                {
                    return m_name;
                }
            }

            public string MenuPath
            {
                get
                {
                    return m_menuPath;
                }
            }

            public GUIContent Content
            {
                get
                {
                    return m_content;
                }
            }

            public GUIStyle Style
            {
                get
                {
                    return m_style;
                }
            }

            public virtual int CompareTo(object o)
            {
                return Name.CompareTo((o as Element).Name);
            }

            public Element(string name, GUIStyle style)
            {
                m_name = name;
                m_content = new GUIContent(name);
                m_style = style;
            }

            public Element(GUIContent content, GUIStyle style)
            {
                m_name = content.text;
                m_content = content;
                m_style = style;
            }
        }

        protected class ItemElement : Element
        {
            protected object m_item;

            public object Item
            {
                get
                {
                    return m_item;
                }
            }

            public ItemElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj) : base(content, style)
            {
                m_level = level;
                m_item = obj;
                m_menuPath = menuPath;
            }
        }
        
        [Serializable]
        protected class GroupElement : Element
        {
            protected Vector2 m_scroll;

            protected int m_selectedIndex = 0;

            public Vector2 Scroll
            {
                get
                {
                    return m_scroll;
                }
                set
                {
                    m_scroll = value;
                }
            }

            public int SelectedIndex
            {
                get
                {
                    return m_selectedIndex;
                }
                set
                {
                    m_selectedIndex = value;
                }
            }

            public GroupElement(int level, string name, string menuPath) : base(name, null)
            {
                m_level = level;
                m_menuPath = menuPath;
                m_content = new GUIContent(m_name, AssetDatabase.GetCachedIcon("Assets"));
            }
        }

        protected abstract class NewItemElement : GroupElement
        {
            public NewItemElement() : base(1, "Create New...", "")
            {
                m_content = new GUIContent(m_name, EditorGUIUtility.FindTexture(EditorGUIUtility.isProSkin ? "d_editicon.sml" : "editicon.sml"));
            }
            
            protected abstract object CreateNewItem();

            protected virtual string GetError()
            {
                return null;
            }

            public virtual void OnGUI()
            {
                DrawControls();
                
                ForceControlFocus();

                GUILayout.FlexibleSpace();

                string error = GetError();
                bool hasError = !string.IsNullOrEmpty(error);
                bool canCreate = !string.IsNullOrEmpty(NewItemName) && !hasError;
                if (hasError)
                {
                    EditorGUILayout.HelpBox(error, MessageType.Error);
                }

                using (new EditorGUI.DisabledScope(!canCreate))
                {
                    if (GUILayout.Button("Create", new GUILayoutOption[0]))
                    {
                        CreateNew();
                    }
                }
                EditorGUILayout.Space();
            }

            public virtual float GetGUIHeight()
            {
                return 100f;
            }

            protected virtual void DrawControls()
            {
                DrawNameControl();
            }

            protected virtual void DrawNameControl()
            {
                GUILayout.Label("Name");
                GUI.SetNextControlName("NewName");
                NewItemName = EditorGUILayout.TextField(NewItemName);
            }

            protected virtual string[] GetControlNames()
            {
                return new string[] { "NewName" };
            }

            private void ForceControlFocus()
            {
                if (!GetControlNames().Contains(GUI.GetNameOfFocusedControl()))
                {
                    EditorGUI.FocusTextInControl("NewName");
                }
            }
            
            public void CreateNew()
            {
                object obj = CreateNewItem();
                SearchPopupWindow.InvokeOnSelect(obj);
            }
        }

        protected class Styles
        {
            public GUIStyle m_Header = new GUIStyle(GUI.skin.GetStyle("In BigTitle"));

            public GUIStyle m_PathBar = new GUIStyle("ProjectBrowserBottomBarBg");

            public GUIStyle m_ItemButton = new GUIStyle("PR Label");

            public GUIStyle m_GroupButton;

            public GUIStyle m_Background = "grey_border";

            public GUIStyle m_PreviewBackground = "PopupCurveSwatchBackground";

            public GUIStyle m_PreviewHeader = new GUIStyle(EditorStyles.label);

            public GUIStyle m_PreviewText = new GUIStyle(EditorStyles.wordWrappedLabel);

            public GUIStyle m_RightArrow = "AC RightArrow";

            public GUIStyle m_LeftArrow = "AC LeftArrow";

            public float m_SearchBarHeight = 20;

            public float m_HeaderHeight = 25;

            public float m_PathBarHeight = 20;

            public Styles()
            {
                m_Header.font = EditorStyles.boldLabel.font;
                m_ItemButton.alignment = TextAnchor.MiddleLeft;
                m_ItemButton.padding.left = 10;
                m_ItemButton.fixedHeight = 20f;
                m_GroupButton = new GUIStyle(m_ItemButton);
                m_PreviewText.padding.left += 3;
                m_PreviewText.padding.right += 3;
                m_PreviewHeader.padding.left++;
                m_PreviewHeader.padding.right += 3;
                m_PreviewHeader.padding.top += 3;
                m_PreviewHeader.padding.bottom += 2;
            }
        }

        protected static SearchPopupWindow s_searchPopupWindow;

        protected static Styles s_styles;
        
        private static int s_controlID;

        private static object s_selectedObject;

        private static object[] s_searchedObjects;

        private static string s_searchString = "";

        private static string s_delayedSearchString = null;

        private static string s_newItemName = "";
        
        private static readonly string kSearchPopupWindowSelectedItemCommand = "SearchPopupWindowSelectedItem";

        private static readonly string kSearchPopupWindowSearchedItemsCommand = "SearchPopupWindowSearchedItems";

        private static readonly string kSearchPopupWindowKeyboardCommand = "SearchPopupWindowKeyboard";

        private static readonly int kControlIDHash = "s_SearchPopupWindowHash".GetHashCode();
        
        private Rect m_buttonRect;

        private Element[] m_tree;

        private Element[] m_searchResultTree;

        private NewItemElement m_newItemElement;

        private List<GroupElement> m_stack = new List<GroupElement>();

        private float m_anim = 1f;

        private float m_animSpeed = 4f;

        private int m_animTarget = 1;

        private long m_lastTime = 0L;
        
        private bool m_scrollToSelected = false;
        
        protected GUIContent m_defaultOptionLabel = DEFAULT_OPTION_LABEL;

        private SearchPopupWindowFlags m_flags;

        private bool m_willDisplayHeader;

        private bool m_willDisplaySearch;

        private bool m_willDisplayPath;

        private bool m_willDisplayCreateNewOption;

        private bool m_willDisplayDefaultOption;

        private bool m_willSortIntoFolders;

        private bool m_foldersExist;

        private static EditorWindow s_lastFocusedWindow;

        public static SearchPopupWindow Instance
        {
            get
            {
                return s_searchPopupWindow;
            }
        }

        public SearchPopupWindowFlags Flags
        {
            get
            {
                return m_flags;
            }
            set
            {
                SetFlags(value);
            }
        }

        public static int ControlIDHash
        {
            get
            {
                return kControlIDHash;
            }
        }

        public static int ControlID
        {
            get
            {
                return s_controlID;
            }
        }

        public static object SelectedObject
        {
            get
            {
                return s_selectedObject;
            }
        }

        public static object[] SearchedObjects
        {
            get
            {
                return s_searchedObjects;
            }
        }

        protected static string NewItemName
        {
            get
            {
                return s_newItemName;
            }
            set
            {
                s_newItemName = value;
            }
        }

        public static bool HasSearchString
        {
            get
            {
                return !string.IsNullOrEmpty(s_searchString);
            }
        }

        public static string SearchString
        {
            get
            {
                return s_searchString;
            }
        }

        private GroupElement ActiveParent
        {
            get
            {
                int index = m_stack.Count - 2 + m_animTarget;
                if (index >= 0 && index < m_stack.Count)
                {
                    return m_stack[index];
                }
                return null;
            }
        }

        private Element[] ActiveTree
        {
            get
            {
                return (HasSearchString) ? m_searchResultTree : m_tree;
            }
        }

        private Element ActiveElement
        {
            get
            {
                Element result = null;
                if (ActiveTree != null)
                {
                    List<Element> children = GetChildren(ActiveTree, ActiveParent);
                    if (children.Count > 0)
                    {
                        if (ActiveParent.SelectedIndex >= 0 && ActiveParent.SelectedIndex < children.Count)
                        {
                            result = children[ActiveParent.SelectedIndex];
                        }
                        else
                        {
                            result = children[0];
                        }
                    }
                }
                return result;
            }
        }

        private bool IsAnimating
        {
            get
            {
                return m_anim != (float)m_animTarget;
            }
        }

        static SearchPopupWindow()
        {
            s_searchPopupWindow = null;
        }

        protected virtual void OnEnable()
        {
            s_searchPopupWindow = this;
            s_searchString = EditorPrefs.GetString(SearchSaveKey(), "");

        }

        protected virtual void OnDisable()
        {
            s_searchPopupWindow = null;
        }
        
        internal void OnGUI()
        {
            if (s_styles == null)
            {
                s_styles = new Styles();
            }
            GUI.Label(new Rect(0f, 0f, base.position.width, base.position.height), GUIContent.none, s_styles.m_Background);

            if (ActiveTree == null)
            {
                return;
            }

            HandleKeyboardCommand(Event.current);

            float topPadding = 0;
            float bottomPadding = 0;

            if (IsSearchVisible())
            {
                topPadding += GetSearchBarHeight();

                DrawSearch();
            }

            if (IsPathVisible())
            {
                bottomPadding += GetPathBarHeight();
            }

            GUILayoutUtility.GetRect(10f, base.position.height - topPadding - bottomPadding);
            ListGUI(GetListPosition(m_anim, topPadding, bottomPadding), ActiveTree, GetElementRelative(0), GetElementRelative(-1));
            if (m_anim < 1f)
            {
                ListGUI(GetListPosition(m_anim + 1f, topPadding, bottomPadding), ActiveTree, GetElementRelative(-1), GetElementRelative(-2));
            }

            if (IsPathVisible())
            {
                DrawPath();
            }
            
            if (IsAnimating && Event.current.type == EventType.Repaint)
            {
                long ticks = DateTime.Now.Ticks;
                float num = (float)(ticks - m_lastTime) / 1E+07f;
                m_lastTime = ticks;
                m_anim = Mathf.MoveTowards(m_anim, (float)m_animTarget, num * m_animSpeed);
                if (m_animTarget == 0 && m_anim == 0f)
                {
                    m_anim = 1f;
                    m_animTarget = 1;
                    m_stack.RemoveAt(m_stack.Count - 1);
                }
                base.Repaint();
            }
        }
        
        protected static T Show<T>(int controlID, Rect rect, object selectedObject, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags) where T : SearchPopupWindow
        {
            EditorWindow focusedWindow = EditorWindow.focusedWindow;
            if (focusedWindow != null && !(focusedWindow is SearchPopupWindow))
            {
                s_lastFocusedWindow = focusedWindow;
            }

            try
            {
                foreach (SearchPopupWindow searchPopupWindow in Resources.FindObjectsOfTypeAll(typeof(SearchPopupWindow)))
                {
                    try
                    {
                        searchPopupWindow.Close();
                    }
                    catch
                    {
                        DestroyImmediate(searchPopupWindow);
                        throw;
                    }
                }
                
                s_searchPopupWindow = ScriptableObject.CreateInstance(typeof(T)) as T;
                s_searchPopupWindow.Init(controlID, rect, selectedObject, defaultOptionLabel, flags);
                Event.current.Use();
            }
            catch
            {
                if (s_searchPopupWindow != null)
                {
                    DestroyImmediate(s_searchPopupWindow);
                }
                s_searchPopupWindow = null;

                throw;
            }
            
            return s_searchPopupWindow as T;
        }

        protected virtual float GetSearchBarHeight()
        {
            if (s_styles == null)
            {
                s_styles = new Styles();
            }
            return s_styles.m_SearchBarHeight + 7f;
        }

        protected virtual void DrawSearch()
        {
            GUILayout.Space(7f);
            if (!(ActiveParent is NewItemElement))
            {
                GUI.SetNextControlName(SearchControlName());
            }
            Rect searchBarRect = GUILayoutUtility.GetRect(10f, s_styles.m_SearchBarHeight);
            searchBarRect.x += 8f;
            searchBarRect.width -= EditorGUIUtility.singleLineHeight;
            using (new EditorGUI.DisabledScope(ActiveParent is NewItemElement))
            {
                EditorGUI.BeginChangeCheck();
                string text = SearchField(searchBarRect, s_searchString);
                if (EditorGUI.EndChangeCheck())
                {
                    SearchInternal(text);
                }
            }
        }

        protected virtual float GetPathBarHeight()
        {
            if (s_styles == null)
            {
                s_styles = new Styles();
            }
            return s_styles.m_PathBarHeight;
        }

        protected virtual void DrawPath()
        {
            Rect pathRect = GUILayoutUtility.GetRect(10f, s_styles.m_PathBarHeight);
            pathRect.xMin += 1;
            pathRect.xMax -= 1;
            PathBar(pathRect, ActiveElement);
        }

        protected void SetFlags(SearchPopupWindowFlags flags)
        {
            m_flags = flags;
            m_willDisplayHeader = !flags.HasFlag(SearchPopupWindowFlags.HideHeader);
            m_willDisplaySearch = !flags.HasFlag(SearchPopupWindowFlags.HideSearch);
            m_willDisplayPath = !flags.HasFlag(SearchPopupWindowFlags.HidePath);
            m_willDisplayCreateNewOption = !flags.HasFlag(SearchPopupWindowFlags.HideCreateNewOption);
            m_willDisplayDefaultOption = !flags.HasFlag(SearchPopupWindowFlags.HideDefaultOption);
            m_willSortIntoFolders = !flags.HasFlag(SearchPopupWindowFlags.DisableFolders);
        }

        protected abstract object[] GetSortedItems();

        protected abstract GUIContent[] GetSortedMenus(object[] objs);

        protected virtual GUIStyle[] GetSortedStyles(object[] objs)
        {
            GUIStyle[] styles = new GUIStyle[objs.Length];
            for (int i = 0; i < objs.Length; i++)
            {
                styles[i] = null;
            }
            return styles;
        }

        protected virtual ItemElement GetItemElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj)
        {
            return new ItemElement(level, content, style, menuPath, obj);
        }

        protected virtual NewItemElement GetNewItemElement()
        {
            return null;
        }

        public virtual object GetDefaultOption()
        {
            return null;
        }

        public virtual GUIContent GetDefaultOptionLabel()
        {
            return DEFAULT_OPTION_LABEL;
        }

        public virtual GUIStyle GetDefaultOptionStyle()
        {
            return null;
        }

        protected virtual string GetWindowName()
        {
            return "";
        }

        protected virtual string DefaultCreateNewItemName()
        {
            return "NewName";
        }

        public static string SearchControlName()
        {
            return typeof(SearchPopupWindow).AssemblyQualifiedName + "ItemSearchString";
        }

        protected virtual string SearchSaveKey()
        {
            return GetType().AssemblyQualifiedName + "ItemSearchString";
        }

        protected virtual string GetNewItemNameSearch(NewItemElement e, string name)
        {
            return name;
        }
        
        public static object ProcessSelectedItem(int controlID, object selectedObject)
        {
            Event current = Event.current;
            if (current.type == EventType.ExecuteCommand && current.commandName == kSearchPopupWindowSelectedItemCommand && s_controlID == controlID)
            {
                current.Use();
                EditorGUIUtility.editingTextField = false;
                GUIUtility.keyboardControl = 0;
                GUI.changed = true;
                selectedObject = s_selectedObject;
            }
            return selectedObject;
        }

        public static void ProcessSelectedItem(int controlID, OnSelectDelegate onSelect)
        {
            Event current = Event.current;
            if (current.type == EventType.ExecuteCommand && current.commandName == kSearchPopupWindowSelectedItemCommand && s_controlID == controlID)
            {
                current.Use();
                EditorGUIUtility.editingTextField = false;
                GUIUtility.keyboardControl = 0;
                GUI.changed = true;
                onSelect(s_selectedObject);
            }
        }

        public static void InvokeOnSelect(object obj)
        {
            s_selectedObject = obj;
            if (Instance != null)
            {
                Instance.Close();
            }
            SendEvent(kSearchPopupWindowSelectedItemCommand);
        }

        public static object[] ProcessSearchedItems(int controlID, object[] objects)
        {
            Event current = Event.current;
            if (current.type == EventType.ExecuteCommand && current.commandName == kSearchPopupWindowSearchedItemsCommand && s_controlID == controlID)
            {
                current.Use();
                GUI.changed = true;
                objects = s_searchedObjects;
            }
            return objects;
        }

        public static void ProcessSearchedItems(int controlID, OnSearchDelegate onSearch)
        {
            Event current = Event.current;
            if (current.type == EventType.ExecuteCommand && current.commandName == kSearchPopupWindowSearchedItemsCommand && s_controlID == controlID)
            {
                current.Use();
                GUI.changed = true;
                onSearch(s_searchedObjects);
            }
        }

        public static void InvokeOnSearch(object[] objs)
        {
            s_searchedObjects = objs;
            SendEvent(kSearchPopupWindowSearchedItemsCommand);
        }

        private static void SendEvent(string eventName)
        {
            EditorApplication.delayCall += 
                () =>
                {
                    if (s_lastFocusedWindow != null)
                    {
                        Event e = new Event();
                        e.type = EventType.ExecuteCommand;
                        e.commandName = eventName;
                        s_lastFocusedWindow.SendEvent(e);
                    }
                };
            GUIUtility.ExitGUI();
        }

        private void Init(int controlID, Rect buttonRect, object selectedObject, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags)
        {
            s_controlID = controlID;
            s_selectedObject = selectedObject;
            m_defaultOptionLabel = defaultOptionLabel;
            s_newItemName = DefaultCreateNewItemName();
            
            SetFlags(flags);

            ClearItemTree();
            CreateItemTree();
            if (HasSearchString)
            {
                RebuildSearch();
            }

            if (IsSearchVisible())
            {
                EditorGUIUtility.editingTextField = false;
            }
            GUIUtility.keyboardControl = controlID;

            m_buttonRect = new Rect(GUIUtility.GUIToScreenPoint(buttonRect.position), buttonRect.size);

            Vector2 windowSize = new Vector2(GetWindowWidth(m_buttonRect), GetWindowHeight());
            ShowAsDropDown(m_buttonRect, windowSize, IsSearchVisible());
            Repaint();
            base.wantsMouseMove = true;
        }
        
        protected void ShowAsDropDown(Rect buttonRect, Vector2 windowSize, bool giveFocus)
        {
            typeof(EditorWindow)
                .GetMethod(
                "ShowAsDropDown", 
                BindingFlags.NonPublic | BindingFlags.Instance, 
                null, 
                new Type[] { typeof(Rect), typeof(Vector2), Type.GetType("UnityEditor.PopupLocation,UnityEditor").MakeArrayType(), Type.GetType("UnityEditor.ShowMode,UnityEditor"), typeof(bool) }, 
                new ParameterModifier[] { })
                .Invoke(this, new object[] { buttonRect, windowSize, null, 1, giveFocus });
        }

        protected Rect ShowAsDropDownFitToScreen(Rect buttonRect, Vector2 windowSize)
        {
            return 
                (Rect)
                typeof(EditorWindow)
                .GetMethod(
                "ShowAsDropDownFitToScreen",
                BindingFlags.NonPublic | BindingFlags.Instance,
                null,
                new Type[] { typeof(Rect), typeof(Vector2), Type.GetType("UnityEditor.PopupLocation,UnityEditor").MakeArrayType() },
                new ParameterModifier[] { })
                .Invoke(this, new object[] { buttonRect, windowSize, null });
        }

        protected void ShowPopupWithMode(bool giveFocus)
        {
            typeof(EditorWindow)
            .GetMethod(
            "ShowPopupWithMode",
            BindingFlags.NonPublic | BindingFlags.Instance,
            null,
            new Type[] { Type.GetType("UnityEditor.ShowMode,UnityEditor"), typeof(bool) },
            new ParameterModifier[] { })
            .Invoke(this, new object[] { 1, giveFocus });
        }

        protected void SetDropDownPosition(Rect buttonRect, Vector2 windowSize, bool giveFocus)
        {
            position = ShowAsDropDownFitToScreen(buttonRect, windowSize);
            ShowPopupWithMode(giveFocus);
            position = ShowAsDropDownFitToScreen(buttonRect, windowSize);
            minSize = new Vector2(position.width, position.height);
            maxSize = new Vector2(position.width, position.height);
            base.wantsMouseMove = true;
        }

        protected float GetWindowWidth(Rect buttonRect)
        {
            return Math.Max(150f, buttonRect.width);
        }

        protected float GetListHeight()
        {
            int largestSelectableElementCount = 0;
            for (int i = 0; i < m_stack.Count; i++)
            {
                GroupElement parent = m_stack[i];
                int count = GetChildren(ActiveTree, parent).Count;
                if (count > largestSelectableElementCount)
                {
                    largestSelectableElementCount = count;
                }
            }
            float selectElementsGUIHeight = largestSelectableElementCount * (EditorGUIUtility.singleLineHeight + 4f);
            float createElementGUIHeight = (m_newItemElement != null) ? m_newItemElement.GetGUIHeight() : 0f;
            
            return Math.Max(selectElementsGUIHeight, createElementGUIHeight);
        }

        protected float GetWindowHeight()
        {
            float height = GetListHeight();

            if (IsSearchVisible())
            {
                height += GetSearchBarHeight();
            }
            if (IsHeaderVisible())
            {
                height += GetHeaderHeight();
            }
            if (IsPathVisible())
            {
                height += GetPathBarHeight();
            }

            return Math.Min(320f, height);
        }

        protected bool IsHeaderVisible()
        {
            return m_willDisplayHeader && (!string.IsNullOrEmpty(GetWindowName()) || m_foldersExist);
        }

        protected bool IsPathVisible()
        {
            return m_willDisplayPath && m_foldersExist;
        }

        protected bool IsSearchVisible()
        {
            return m_willDisplaySearch;
        }

        protected virtual void PrepareItems()
        {
        }

        private void ClearItemTree()
        {
            m_tree = null;
        }

        private void CreateItemTree()
        {
            PrepareItems();

            object[] sortedObjects = GetSortedItems();
            GUIContent[] sortedSubmenus = GetSortedMenus(sortedObjects);
            GUIStyle[] sortedStyles = GetSortedStyles(sortedObjects);

            List<object> objects = sortedObjects.ToList();
            List<GUIContent> submenus = sortedSubmenus.ToList();
            List<GUIStyle> styles = sortedStyles.ToList();

            if (m_willDisplayDefaultOption)
            {
                objects.Insert(0, GetDefaultOption());
                submenus.Insert(0, (m_defaultOptionLabel != null) ? m_defaultOptionLabel : GetDefaultOptionLabel());
                styles.Insert(0, GetDefaultOptionStyle());
            }

            if (!m_willSortIntoFolders)
            {
                submenus = submenus.Select(x => new GUIContent(x.text.Substring(x.text.LastIndexOf("/") + 1), x.image, x.tooltip)).ToList();
                m_foldersExist = false;
            }
            else
            {
                m_foldersExist = submenus.Any(x => x.text.Contains("/"));
            }

            List<string> displayOptionList = new List<string>();
            List<Element> elementList = new List<Element>();
            Element selectedElement = null;
            for (int i = 0; i < submenus.Count; i++)
            {
                string menuPath = submenus[i].text;

                string windowName = GetWindowName();
                windowName = (windowName != null) ? windowName : "";
                menuPath = windowName + "/" + menuPath;

                string[] textArray = menuPath.Split(new char[]
                {
                        '/'
                });
                while (textArray.Length - 1 < displayOptionList.Count)
                {
                    displayOptionList.RemoveAt(displayOptionList.Count - 1);
                }
                while (displayOptionList.Count > 0 && textArray[displayOptionList.Count - 1] != displayOptionList[displayOptionList.Count - 1])
                {
                    displayOptionList.RemoveAt(displayOptionList.Count - 1);
                }
                while (textArray.Length - 1 > displayOptionList.Count)
                {
                    string[] pathArray = textArray.ToList().GetRange(0, displayOptionList.Count + 1).ToArray();
                    GroupElement groupElement = new GroupElement(displayOptionList.Count, textArray[displayOptionList.Count], string.Join("/", pathArray));
                    elementList.Add(groupElement);
                    displayOptionList.Add(textArray[displayOptionList.Count]);
                }
                GUIContent submenu = submenus[i];
                GUIContent content = new GUIContent(textArray[displayOptionList.Count], submenu.image, submenu.tooltip);
                GUIStyle style = styles[i];
                ItemElement itemElement = GetItemElement(displayOptionList.Count, content, style, menuPath, objects[i]);
                elementList.Add(itemElement);
                if ((s_selectedObject == null && objects[i] == null) ||
                    (s_selectedObject != null && s_selectedObject.Equals(objects[i])))
                {
                    selectedElement = itemElement;
                }
            }
            if (m_willDisplayCreateNewOption)
            {
                m_newItemElement = GetNewItemElement();
                if (m_newItemElement != null)
                {
                    elementList.Add(m_newItemElement);
                }
            }
            m_tree = elementList.ToArray();
            if (m_tree.Length > 0)
            {
                if (m_stack.Count == 0)
                {
                    m_stack.Add(m_tree[0] as GroupElement);
                }
                else
                {
                    GroupElement groupElement = m_tree[0] as GroupElement;
                    int level = 0;
                    while (true)
                    {
                        if (level >= m_stack.Count)
                        {
                            break;
                        }
                        GroupElement groupElement2 = m_stack[level];
                        m_stack[level] = groupElement;
                        m_stack[level].SelectedIndex = groupElement2.SelectedIndex;
                        m_stack[level].Scroll = groupElement2.Scroll;
                        level++;
                        if (level == m_stack.Count)
                        {
                            break;
                        }
                        List<Element> children = GetChildren(ActiveTree, groupElement);
                        Element element = children.FirstOrDefault((Element x) => x.Name == m_stack[level].Name);
                        if (element != null && element is GroupElement)
                        {
                            groupElement = (element as GroupElement);
                        }
                        else
                        {
                            while (m_stack.Count > level)
                            {
                                m_stack.RemoveAt(level);
                            }
                        }
                    }
                }
            }
            if (selectedElement != null)
            {
                GoToParentInstantly(selectedElement.MenuPath);
                HighlightElement(selectedElement);
                m_scrollToSelected = true;
            }
        }

        public static void ProcessSearch(int controlID, string text)
        {
            if (Instance != null)
            {
                if (s_controlID == controlID)
                {
                    Instance.SearchInternal((text != null) ? text : "");
                    Instance.Repaint();
                }
            }
        }

        protected void SearchInternal(string text)
        {
            if (text != s_searchString || s_delayedSearchString != null)
            {
                if (!IsAnimating)
                {
                    s_searchString = (s_delayedSearchString ?? text);
                    EditorPrefs.SetString(SearchSaveKey(), s_searchString);
                    RebuildSearch();
                    ResizeWindow();
                    s_delayedSearchString = null;
                }
                else
                {
                    s_delayedSearchString = text;
                }
                if (string.IsNullOrEmpty(s_searchString))
                {
                    InvokeOnSearch(null);
                }
                else
                {
                    if (m_searchResultTree != null)
                    {
                        InvokeOnSearch(m_searchResultTree.OfType<ItemElement>().Select(x => x.Item).ToArray());
                    }
                }
            }
        }
        
        public string SearchField(Rect position, string text)
        {
            GUIStyle searchTextFieldStyle = GUI.skin.FindStyle("SearchTextField");
            GUIStyle searchCancelButtonStyle = GUI.skin.FindStyle("SearchCancelButton");
            GUIStyle searchCancelButtonEmptyStyle = GUI.skin.FindStyle("SearchCancelButtonEmpty");
            Rect position2 = position;
            position2.width -= 15f;
            text = EditorGUI.TextField(position2, text, searchTextFieldStyle);
            Rect position3 = position;
            position3.x += position.width - 15f;
            position3.width = 15f;
            bool cleared = false;
            if (GUI.Button(position3, GUIContent.none, (!(text != "")) ? searchCancelButtonEmptyStyle : searchCancelButtonStyle) && text != "")
            {
                text = "";
                EditorGUIUtility.editingTextField = false;
                GUIUtility.keyboardControl = 0;
                cleared = true;
            }
            if (!cleared && !(ActiveParent is NewItemElement))
            {
                EditorGUI.FocusTextInControl(SearchControlName());
            }
            return text;
        }

        public virtual void InvokeKeyboardCommand(KeyCode keyCode)
        {
            Event e = EditorGUIUtility.CommandEvent(kSearchPopupWindowKeyboardCommand);
            e.keyCode = keyCode;
            HandleKeyboardCommand(e);
            Repaint();
        }

        protected virtual void HandleKeyboardCommand(Event current)
        {
            if (current.type == EventType.KeyDown || current.commandName == kSearchPopupWindowKeyboardCommand)
            {
                KeyCode keyCode = current.keyCode;
                if (ActiveParent is NewItemElement)
                {
                    if (keyCode == KeyCode.Return || keyCode == KeyCode.KeypadEnter)
                    {
                        (ActiveParent as NewItemElement).CreateNew();
                        current.Use();
                        GUIUtility.ExitGUI();
                        GUI.changed = true;
                    }
                    if (keyCode == KeyCode.Escape)
                    {
                        GoToParent();
                        current.Use();
                    }
                }
                else
                {
                    if (keyCode == KeyCode.DownArrow)
                    {
                        ActiveParent.SelectedIndex++;
                        ActiveParent.SelectedIndex = Mathf.Min(ActiveParent.SelectedIndex, GetChildren(ActiveTree, ActiveParent).Count - 1);
                        m_scrollToSelected = true;
                        current.Use();
                    }
                    if (keyCode == KeyCode.UpArrow)
                    {
                        ActiveParent.SelectedIndex--;
                        ActiveParent.SelectedIndex = Mathf.Max(ActiveParent.SelectedIndex, 0);
                        m_scrollToSelected = true;
                        current.Use();
                    }
                    if (keyCode == KeyCode.Return || keyCode == KeyCode.KeypadEnter)
                    {
                        GoToChild(ActiveElement, true);
                        current.Use();
                    }
                    if (!HasSearchString)
                    {
                        if (keyCode == KeyCode.LeftArrow || keyCode == KeyCode.Backspace)
                        {
                            GoToParent();
                            current.Use();
                        }
                        if (keyCode == KeyCode.RightArrow)
                        {
                            GoToChild(ActiveElement, false);
                            current.Use();
                        }
                        if (keyCode == KeyCode.Escape)
                        {
                            base.Close();
                            current.Use();
                        }
                    }
                }
            }
        }

        private void RebuildSearch()
        {
            if (!HasSearchString)
            {
                m_searchResultTree = null;
                if (m_stack != null && m_stack.Count > 0 && m_stack[m_stack.Count - 1].Name == "Search")
                {
                    m_stack.Clear();
                    m_stack.Add(m_tree[0] as GroupElement);
                }
                m_animTarget = 1;
                m_lastTime = DateTime.Now.Ticks;
                s_newItemName = DefaultCreateNewItemName();
            }
            else
            {
                s_newItemName = s_searchString;
                string[] searchCharacters = s_searchString.ToLower().Split(new char[]
                {
                    ' '
                });
                List<Element> exactNameMatchElements = new List<Element>();
                List<Element> looseNameMatchElements = new List<Element>();
                List<Element> exactPathMatchElements = new List<Element>();
                List<Element> loosePathMatchElements = new List<Element>();
                Element[] tree = (m_tree != null) ? m_tree : new Element[0];
                bool isFirstElement = true;
                for (int i = 0; i < tree.Length; i++)
                {
                    Element element = tree[i];
                    if (element is ItemElement)
                    {
                        if (isFirstElement)
                        {
                            isFirstElement = false;
                            if (m_willDisplayDefaultOption)
                            {
                                continue;
                            }
                        }
                        int nameMatchPriority = IsMatch(element.Name, searchCharacters);
                        if (nameMatchPriority == 0)
                        {
                            exactNameMatchElements.Add(element);
                        }
                        if (nameMatchPriority == 1)
                        {
                            looseNameMatchElements.Add(element);
                        }

                        int pathMatchPriority = IsMatch(element.MenuPath, searchCharacters);
                        if (pathMatchPriority == 0)
                        {
                            exactPathMatchElements.Add(element);
                        }
                        if (pathMatchPriority == 1)
                        {
                            loosePathMatchElements.Add(element);
                        }
                    }
                }
                exactNameMatchElements.Sort();
                looseNameMatchElements.Sort();
                exactPathMatchElements.Sort();
                loosePathMatchElements.Sort();
                List<Element> searchResultElements = new List<Element>();
                searchResultElements.Add(new GroupElement(0, "Search", ""));
                searchResultElements.AddRange(exactNameMatchElements);
                searchResultElements.AddRange(looseNameMatchElements);
                searchResultElements.AddRange(exactPathMatchElements);
                searchResultElements.AddRange(loosePathMatchElements);
                NewItemElement[] newItemElements = m_tree.OfType<NewItemElement>().ToArray();
                searchResultElements.AddRange(newItemElements);
                m_searchResultTree = searchResultElements.Distinct().ToArray();
                m_stack.Clear();
                m_stack.Add(m_searchResultTree[0] as GroupElement);
                if (GetChildren(ActiveTree, ActiveParent).Count >= 1)
                {
                    ActiveParent.SelectedIndex = 0;
                }
                else
                {
                    ActiveParent.SelectedIndex = -1;
                }
            }
            OnRebuildSearch();
        }

        protected void ResizeWindow()
        {
            SetDropDownPosition(m_buttonRect, new Vector2(GetWindowWidth(m_buttonRect), GetWindowHeight()), IsSearchVisible());
        }

        protected virtual void OnRebuildSearch()
        {
            string name = "";
            if (HasSearchString)
            {
                name = SearchString;
            }
            else
            {
                name = DefaultCreateNewItemName();
            }

            NewItemName = FormatNewName(name);
        }

        protected virtual string FormatNewName(string str)
        {
            return str;
        }

        private int IsMatch(string text, string[] searchCharacters)
        {
            text = text.ToLower().Replace(" ", "");

            bool containsSearchCharacter = true;
            bool startsWithSearchCharacter = false;
            for (int j = 0; j < searchCharacters.Length; j++)
            {
                string searchCharacter = searchCharacters[j];
                if (!text.Contains(searchCharacter))
                {
                    containsSearchCharacter = false;
                    break;
                }
                if (j == 0 && text.StartsWith(searchCharacter, StringComparison.Ordinal))
                {
                    startsWithSearchCharacter = true;
                }
            }
            if (containsSearchCharacter)
            {
                if (startsWithSearchCharacter)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            return -1;
        }

        private GroupElement GetElementRelative(int rel)
        {
            int index = m_stack.Count - 1 + rel;

            return GetElementAbsolute(index);
        }

        private GroupElement GetElementAbsolute(int index)
        {
            if (index >= 0 && index < m_stack.Count)
            {
                return m_stack[index];
            }

            return null;
        }

        private void GoToParent()
        {
            if (m_stack.Count > 1)
            {
                m_animTarget = 0;
                m_lastTime = DateTime.Now.Ticks;
            }
        }

        private void GoToParentInstantly(string path)
        {
            Element[] tree = m_tree;

            m_stack.Clear();

            foreach (Element e in tree)
            {
                if (!(e is NewItemElement) && (path == e.MenuPath || path.StartsWith(e.MenuPath + "/", StringComparison.Ordinal)))
                {
                    GroupElement groupElement = e as GroupElement;
                    if (groupElement != null)
                    {
                        m_stack.Add(groupElement);
                    }
                }
            }

            m_stack.OrderBy(x => x.MenuPath.Length);

            m_animTarget = 1;
        }

        private void HighlightElement(Element element)
        {
            if (ActiveParent != null && ActiveTree != null)
            {
                int index = -1;

                List<Element> children = GetChildren(ActiveTree, ActiveParent);
                if (children.Count >= 1)
                {
                    index = children.IndexOf(element);
                }

                HighlightIndex(index);
            }
        }

        private void HighlightIndex(int index)
        {
            if (ActiveParent != null)
            {
                ActiveParent.SelectedIndex = index;
            }
        }

        private void GoToChild(Element e, bool selectIfItem)
        {
            if (e is NewItemElement)
            {
                if (!HasSearchString)
                {
                    s_newItemName = GetNewItemNameSearch(e as NewItemElement, s_newItemName);
                }
            }
            if (e is ItemElement)
            {
                if (selectIfItem)
                {
                    ItemElement ce = e as ItemElement;
                    object obj = null;
                    if (ce != null)
                    {
                        obj = ce.Item;
                    }
                    InvokeOnSelect(obj);
                }
            }
            else if (!HasSearchString || e is NewItemElement)
            {
                m_lastTime = DateTime.Now.Ticks;
                if (m_animTarget == 0)
                {
                    m_animTarget = 1;
                }
                else if (m_anim == 1f)
                {
                    m_anim = 0f;
                    m_stack.Add(e as GroupElement);
                }
            }
        }

        private Rect GetListPosition(float anim, float topPadding, float bottomPadding)
        {
            anim = Mathf.Floor(anim) + Mathf.SmoothStep(0f, 1f, Mathf.Repeat(anim, 1f));
            Rect listPosition = base.position;
            listPosition.x = base.position.width * (1f - anim) + 1f;
            listPosition.y = topPadding;
            listPosition.height -= (topPadding + bottomPadding);
            listPosition.width -= 2f;

            return listPosition;
        }

        protected virtual void ListGUI(Rect position, Element[] tree, GroupElement parent, GroupElement grandParent)
        {
            using (new GUILayout.AreaScope(position))
            {
                if (IsHeaderVisible())
                {
                    DrawHeader(parent, grandParent);
                }
                if (parent is NewItemElement)
                {
                    (parent as NewItemElement).OnGUI();
                }
                else
                {
                    ListGUI(tree, parent);
                }
            }
        }

        protected virtual float GetHeaderHeight()
        {
            if (s_styles == null)
            {
                s_styles = new Styles();
            }
            return s_styles.m_HeaderHeight;
        }

        protected virtual void DrawHeader(GroupElement parent, GroupElement grandParent)
        {
            Rect rect = GUILayoutUtility.GetRect(10f, s_styles.m_HeaderHeight);
            string name = parent.Name;
            GUI.Label(rect, name, s_styles.m_Header);
            if (grandParent != null)
            {
                Rect position2 = new Rect(rect.x + 4f, rect.y + 7f, 13f, 13f);
                if (Event.current.type == EventType.Repaint)
                {
                    s_styles.m_LeftArrow.Draw(position2, false, false, false, false);
                }
                if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
                {
                    GoToParent();
                    Event.current.Use();
                }
            }
        }

        private void PathBar(Rect position, Element element)
        {
            GUI.Label(position, GUIContent.none, s_styles.m_PathBar);

            if (element == null)
            {
                return;
            }

            List<string> breadCrumbs = new List<string>();
            string folder = element.MenuPath;
            string[] strArray = folder.Split('/');
            foreach (string text in strArray)
            {
                breadCrumbs.Add(text);
            }
            if (breadCrumbs.Count > 0)
            {
                breadCrumbs.RemoveAt(breadCrumbs.Count - 1);
            }
            Rect rect = position;
            ++rect.y;
            rect.x += 4f;
            string path = "";
            for (int i = 0; i < breadCrumbs.Count; ++i)
            {
                bool isLastBreadCrumb = i == breadCrumbs.Count - 1;
                GUIStyle style = !isLastBreadCrumb ? EditorStyles.label : EditorStyles.boldLabel;
                string name = breadCrumbs[i];
                GUIContent nameContent = new GUIContent(name);
                float nameWidth = style.CalcSize(nameContent).x;
                rect.width = nameWidth;
                if (!string.IsNullOrEmpty(path))
                {
                    path += "/";
                }
                path += name;
                if (GUI.Button(rect, nameContent, style))
                {
                    GoToParentInstantly(path);
                    HighlightIndex(0);
                }
                rect.x += nameWidth;

                if (!isLastBreadCrumb)
                {
                    Rect arrowRect = new Rect(rect.x, rect.y, 13f, rect.height - 2f);
                    if (Event.current.type == EventType.Repaint)
                    {
                        s_styles.m_RightArrow.Draw(arrowRect, false, false, false, false);
                    }
                    rect.x += arrowRect.width;
                }
            }
        }

        private void ListGUI(Element[] tree, GroupElement parent)
        {
            Rect selectedRect = default(Rect);

            using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(parent.Scroll, new GUILayoutOption[0]))
            {
                parent.Scroll = scrollView.scrollPosition;
                EditorGUIUtility.SetIconSize(new Vector2(EditorGUIUtility.singleLineHeight, EditorGUIUtility.singleLineHeight));
                List<Element> children = GetChildren(tree, parent);
                for (int i = 0; i < children.Count; i++)
                {
                    Element element = children[i];
                    Rect rect2 = GUILayoutUtility.GetRect(EditorGUIUtility.singleLineHeight, 20f, new GUILayoutOption[]
                    {
                    GUILayout.ExpandWidth(true)
                    });
                    if (Event.current.type == EventType.MouseMove || Event.current.type == EventType.MouseDown)
                    {
                        if (parent.SelectedIndex != i && rect2.Contains(Event.current.mousePosition))
                        {
                            parent.SelectedIndex = i;
                            base.Repaint();
                        }
                    }
                    bool isSelected = false;
                    if (i == parent.SelectedIndex)
                    {
                        isSelected = true;
                        selectedRect = rect2;
                    }
                    if (Event.current.type == EventType.Repaint)
                    {
                        GUIStyle gUIStyle = s_styles.m_GroupButton;
                        GUIContent content = element.Content;
                        bool isItem = element is ItemElement;
                        if (isItem)
                        {
                            gUIStyle = s_styles.m_ItemButton;
                            if (element.Style != null)
                            {
                                gUIStyle = element.Style;
                            }
                        }
                        gUIStyle.Draw(rect2, content, false, false, isSelected, isSelected);
                        if (!isItem)
                        {
                            Rect arrowRect = new Rect(rect2.x + rect2.width - 13f, rect2.y + 4f, 13f, 13f);
                            s_styles.m_RightArrow.Draw(arrowRect, false, false, false, false);
                        }
                    }
                    if (Event.current.type == EventType.MouseUp && isSelected && rect2.Contains(Event.current.mousePosition))
                    {
                        Event.current.Use();
                        parent.SelectedIndex = i;
                        GoToChild(element, true);
                    }
                }
                EditorGUIUtility.SetIconSize(Vector2.zero);
                if (m_scrollToSelected && Event.current.type == EventType.Repaint)
                {
                    m_scrollToSelected = false;
                    GUI.ScrollTo(selectedRect);
                    base.Repaint();
                }
            }
        }

        private List<Element> GetChildren(Element[] tree, Element parent)
        {
            List<Element> elements = new List<Element>();

            if (tree == null || parent == null)
            {
                return elements;
            }

            int level = -1;
            int i;
            for (i = 0; i < tree.Length; i++)
            {
                if (tree[i] == parent)
                {
                    level = parent.Level + 1;
                    i++;
                    break;
                }
            }
            List<Element> childElements;
            if (level == -1)
            {
                childElements = elements;
            }
            else
            {
                while (i < tree.Length)
                {
                    Element element = tree[i];
                    if (element.Level < level)
                    {
                        break;
                    }
                    if (element.Level <= level || HasSearchString)
                    {
                        elements.Add(element);
                    }
                    i++;
                }
                childElements = elements;
            }
            return childElements;
        }

        private Element GetChild(Element[] tree, Element parent, string name)
        {
            int level = -1;
            int i;
            for (i = 0; i < tree.Length; i++)
            {
                if (tree[i] == parent)
                {
                    level = parent.Level + 1;
                    i++;
                    break;
                }
            }

            if (level > -1)
            {
                while (i < tree.Length)
                {
                    Element element = tree[i];
                    if (element.Level < level)
                    {
                        break;
                    }
                    if (element.Level <= level || HasSearchString)
                    {
                        if (element.Name == name)
                        {
                            return element;
                        }
                    }
                    i++;
                }
            }

            return null;
        }

        public static string GetLongestCommonPath(List<string> paths)
        {
            if (paths.Count == 0)
            {
                return "";
            }

            IEnumerable<string> MatchingChars =
                from len in Enumerable.Range(0, paths.Min(s => s.Length)).Reverse()
                let possibleMatch = paths.First().Substring(0, len)
                where paths.All(f => f.StartsWith(possibleMatch, StringComparison.Ordinal))
                select possibleMatch;

            string longestDir = "";

            try
            {
                longestDir = System.IO.Path.GetDirectoryName(MatchingChars.First());
            }
            catch (Exception)
            {
            }

            return longestDir;
        }
    }
}
#endif