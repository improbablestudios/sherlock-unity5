﻿#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    public class TypeSearchPopupWindow : UnityObjectSearchPopupWindow
    {
        protected static Type[] s_types;

        protected override string GetWindowName()
        {
            return "Type";
        }

        protected override object[] GetSortedItems()
        {
            return s_types.FindAllInstantiableDerivedTypes();
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            GUIContent[] menus = new GUIContent[objs.Length];
            for (int i = 0; i < objs.Length; i++)
            {
                Type type = objs[i] as Type;
                GUIContent menu = new GUIContent(GetTypePath(type), AssetPreview.GetMiniTypeThumbnail(type));
                menus[i] = menu;
            }
            return menus;
        }

        protected string GetTypePath(Type type)
        {
            if (type != null)
            {
                string typeFullName = null;
                if (typeof(IList).IsAssignableFrom(type))
                {
                    Type itemType = type.GetItemType();
                    typeFullName += "List" + "." + itemType.GetNiceTypeFullName();
                }
                else
                {
                    typeFullName = type.GetNiceTypeFullName();
                }

                if (typeFullName != null)
                {
                    return typeFullName.Replace(".", "/").Replace("+", "/").Replace("\\", "/");
                }
            }

            return DEFAULT_OPTION_LABEL.text;
        }
        
        public static TypeSearchPopupWindow Show(int controlID, Rect rect, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<TypeSearchPopupWindow>(controlID, rect, null, null, types, flags);
        }

        public static TypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<TypeSearchPopupWindow>(controlID, rect, selectedType, null, types, flags);
        }

        public static TypeSearchPopupWindow Show(int controlID, Rect rect, GUIContent defaultOptionLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<TypeSearchPopupWindow>(controlID, rect, null, defaultOptionLabel, types, flags);
        }

        public static TypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent defaultOptionLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<TypeSearchPopupWindow>(controlID, rect, selectedType, defaultOptionLabel, types, flags);
        }

        public static T Show<T>(int controlID, Rect rect, Type selectedType, GUIContent defaultOptionLabel, Type[] types, SearchPopupWindowFlags flags = 0) where T : TypeSearchPopupWindow
        {
            s_types = types;
            return SearchPopupWindow.Show<T>(controlID, rect, selectedType, defaultOptionLabel, flags);
        }

        public static Type ProcessSelectedType(int controlID, Type selectedObject)
        {
            return SearchPopupWindow.ProcessSelectedItem(controlID, selectedObject) as Type;
        }

        public static void ProcessSetTypeNameProperty(int controlID, SerializedProperty property)
        {
            SearchPopupWindow.ProcessSelectedItem(
                controlID,
                (object obj) =>
                {
                    Type selectedType = obj as Type;

                    if (selectedType == null)
                    {
                        property.stringValue = null;
                    }
                    else
                    {
                        property.stringValue = selectedType.AssemblyQualifiedName;
                    }
                });
        }

        public static GUIContent GetSelectedDisplayOptionContent(Type type, GUIContent defaultOptionLabel)
        {
            if (type == null)
            {
                if (defaultOptionLabel != null)
                {
                    return defaultOptionLabel;
                }
                else
                {
                    return DEFAULT_OPTION_LABEL;
                }
            }

            return new GUIContent(type.GetNiceTypeName());
        }
    }
}
#endif