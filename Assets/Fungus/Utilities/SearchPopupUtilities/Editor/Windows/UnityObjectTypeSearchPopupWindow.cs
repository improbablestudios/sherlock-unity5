﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    public abstract class UnityObjectTypeSearchPopupWindow : UnityObjectSearchPopupWindow
    {
        protected enum Language
        {
            CSharp,
            JavaScript
        }

        protected class ScriptElement : UnityObjectElement
        {
            public ScriptElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj) : base(level, content, style, menuPath, obj)
            {
            }
        }

        protected abstract class NewScriptElement : NewUnityObjectElement
        {
            private const string kStartCharacterExp = @"[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Lm}\p{Nl}]";

            private const string kOtherCharacterExp = @"[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Lm}\p{Nl}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\p{Cf}]";

            private const string kNameExp = kStartCharacterExp + kOtherCharacterExp + "{0,}";

            protected static readonly Regex kClassNameValidator = new Regex(@"^" + kNameExp + @"$", RegexOptions.Singleline | RegexOptions.Compiled);
            
            protected override string GetExtension()
            {
                Language s_Lang = ComponentTypeSearchPopupWindow.s_language;
                string result;
                if (s_Lang != Language.CSharp)
                {
                    if (s_Lang != Language.JavaScript)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    result = "js";
                }
                else
                {
                    result = "cs";
                }
                return result;
            }
            
            protected override void DrawControls()
            {
                DrawNameControl();

                EditorGUILayout.Space();

                DrawLanguageControl();
            }

            protected virtual void DrawLanguageControl()
            {
                Language language = (Language)EditorGUILayout.EnumPopup("Language", s_language);
                if (language != s_language)
                {
                    s_language = language;
                    EditorPrefs.SetInt((s_searchPopupWindow as ComponentTypeSearchPopupWindow).NewScriptLanguageSaveKey(), (int)language);
                }
            }

            protected override string[] GetControlNames()
            {
                return new string[] { "NewName", "Language" };
            }
            
            protected override string GetError()
            {
                string result = string.Empty;
                if (NewItemName != string.Empty)
                {
                    if (DoesClassAlreadyExist(NewItemName))
                    {
                        result = "A class called \"" + NewItemName + "\" already exists.";
                    }
                    else if (!IsClassNameValid(NewItemName))
                    {
                        result = "The script name may only consist of a-z, A-Z, 0-9, _ and cannot start with a number.";
                    }
                }
                return result;
            }

            protected override object CreateNewItem()
            {
                Type componentType = null;
                if (SetTargetDirectory())
                {
                    MonoScript monoScript = CreateScriptAssetFromTemplate(GetTargetPath(), TemplatePath);
                    if (monoScript != null)
                    {
                        componentType = monoScript.GetClass();
                        AssetDatabase.OpenAsset(monoScript);
                    }
                }
                return componentType;
            }

            protected virtual MonoScript CreateScriptAssetFromTemplate(string targetPath, string templatePath)
            {
                if (!string.IsNullOrEmpty(targetPath))
                {
                    string fullPath = System.IO.Path.GetFullPath(targetPath);
                    string text = File.ReadAllText(templatePath);
                    text = ReplacePlaceholdersInTemplate(text, targetPath);
                    UTF8Encoding encoding = new UTF8Encoding(true);
                    File.WriteAllText(fullPath, text, encoding);
                    AssetDatabase.ImportAsset(targetPath);
                    return AssetDatabase.LoadAssetAtPath(targetPath, typeof(MonoScript)) as MonoScript;
                }
                return null;
            }

            protected virtual string ReplacePlaceholdersInTemplate(string text, string targetPath)
            {
                text = text.Replace("#NOTRIM#", "");
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(targetPath);
                text = text.Replace("#NAME#", fileNameWithoutExtension);
                string text2 = fileNameWithoutExtension.Replace(" ", "");
                text = text.Replace("#SCRIPTNAME#", text2);
                if (char.IsUpper(text2, 0))
                {
                    text2 = char.ToLower(text2[0]) + text2.Substring(1);
                    text = text.Replace("#SCRIPTNAME_LOWER#", text2);
                }
                else
                {
                    text2 = "my" + char.ToUpper(text2[0]) + text2.Substring(1);
                    text = text.Replace("#SCRIPTNAME_LOWER#", text2);
                }
                return text;
            }

            protected static bool IsClassNameValid(string className)
            {
                return kClassNameValidator.IsMatch(className);
            }

            protected static bool DoesClassAlreadyExist(string className)
            {
                return !(className == string.Empty) && AppDomain.CurrentDomain.GetAssemblies().Any((Assembly a) => a.GetType(className, false) != null);
            }
        }
        protected static Language s_language;

        protected override void OnEnable()
        {
            base.OnEnable();

            s_language = (Language)EditorPrefs.GetInt(NewScriptLanguageSaveKey(), 0);
            if (!Enum.IsDefined(typeof(Language), s_language))
            {
                EditorPrefs.SetInt(NewScriptLanguageSaveKey(), 0);
                s_language = Language.CSharp;
            }

            NewItemAssetLocation = EditorPrefs.GetString(NewItemSaveLocationSaveKey(), "");
        }

        protected override ItemElement GetItemElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj)
        {
            return new ScriptElement(level, content, style, menuPath, obj);
        }

        protected override string DefaultCreateNewItemName()
        {
            return "ScriptName";
        }

        protected virtual string NewScriptLanguageSaveKey()
        {
            return GetType().AssemblyQualifiedName + "NewScriptLanguage";
        }
    }
}
#endif