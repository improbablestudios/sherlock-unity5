﻿#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class AssetSearchPopupWindow : SearchPopupWindow
    {
        protected static Type s_type;

        protected static string[] s_searchFolders;

        protected static string[] s_paths;

        public static AssetSearchPopupWindow Show(int controlID, Rect rect, string path, Type type, string[] searchFolders, SearchPopupWindowFlags flags = 0)
        {
            return Show<AssetSearchPopupWindow>(controlID, rect, path, null, type, searchFolders, flags);
        }

        public static AssetSearchPopupWindow Show(int controlID, Rect rect, string path, GUIContent defaultOptionLabel, Type type, string[] searchFolders, SearchPopupWindowFlags flags = 0)
        {
            return Show<AssetSearchPopupWindow>(controlID, rect, path, defaultOptionLabel, type, searchFolders, flags);
        }

        protected static T Show<T>(int controlID, Rect rect, string path, GUIContent defaultOptionLabel, Type type, string[] searchFolders, SearchPopupWindowFlags flags = 0) where T : AssetSearchPopupWindow
        {
            s_type = type;
            s_searchFolders = searchFolders;
            s_paths = AssetDatabase.FindAssets("t:" + type, searchFolders).Select(x => AssetDatabase.GUIDToAssetPath(x)).ToArray();

            return SearchPopupWindow.Show<T>(controlID, rect, path, defaultOptionLabel, flags);
        }
        
        public static string ProcessSelectedPath(int controlID, string selectedPath)
        {
            return SearchPopupWindow.ProcessSelectedItem(controlID, selectedPath) as string;
        }
        
        protected override string GetWindowName()
        {
            return ObjectNames.NicifyVariableName(s_type.GetNiceTypeName());
        }
        
        protected override object[] GetSortedItems()
        {
            return s_paths;
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            return s_paths.Select(x => new GUIContent(x)).ToArray();
        }
    }

}
#endif