﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    public abstract class UnityObjectSearchPopupWindow : SearchPopupWindow
    {
        protected abstract class UnityObjectElement : ItemElement
        {
            public UnityObjectElement(int level, GUIContent content, GUIStyle style, string menuPath, object obj) : base(level, content, style, menuPath, obj)
            {
            }
        }

        protected abstract class NewUnityObjectElement : NewItemElement
        {
            protected static readonly char[] kInvalidPathChars = new char[]
            {
                    '<',
                    '>',
                    ':',
                    '"',
                    '|',
                    '?',
                    '*',
                    '\0'
            };

            protected static readonly char[] kPathSepChars = new char[]
            {
                '/',
                '\\'
            };

            protected string TemplatePath
            {
                get
                {
                    string scriptTemplateFolder = GetNewItemTemplateFolder();
                    string scriptTemplateName = GetNewItemTemplateFilename();
                    if (scriptTemplateFolder != null && scriptTemplateName != null)
                    {
                        return System.IO.Path.Combine(scriptTemplateFolder, scriptTemplateName);
                    }
                    return null;
                }
            }

            public NewUnityObjectElement() : base()
            {
            }

            protected abstract string GetExtension();

            protected virtual string GetNewItemTemplateFolder()
            {
                return null;
            }

            protected virtual string GetNewItemTemplateFilename()
            {
                return null;
            }

            protected override void DrawControls()
            {
                DrawNameControl();
            }
            
            protected override string[] GetControlNames()
            {
                return new string[] { "NewName" };
            }
            
            public string GetTargetPath()
            {
                return NewItemAssetLocation + "/" + NewItemName + "." + GetExtension();
            }

            protected bool SetTargetDirectory()
            {
                string folderPath = GetFullPathToNewItemAssetLocation();
                folderPath = EditorUtility.SaveFolderPanel("Save To Folder", folderPath, "");
                if (!Directory.Exists(folderPath))
                {
                    return false;
                }
                string assetsFolderName = "Assets";
                folderPath = assetsFolderName + folderPath.Replace(Application.dataPath, "");
                NewItemAssetLocation = folderPath;

                EditorPrefs.SetString((s_searchPopupWindow as UnityObjectSearchPopupWindow).NewItemSaveLocationSaveKey(), NewItemAssetLocation);

                if (File.Exists(GetTargetPath()))
                {
                    EditorUtility.DisplayDialog("File not saved", "A file called \"" + NewItemName + "\" already exists at that path.", "OK");
                    return false;
                }
                return true;
            }

            protected string GetFullPathToNewItemAssetLocation()
            {
                if (!string.IsNullOrEmpty(NewItemAssetLocation))
                {
                    string location = NewItemAssetLocation;
                    int startIndex = ("Assets/").Length;
                    if (startIndex < location.Length)
                    {
                        location = location.Substring(startIndex);
                    }
                    return System.IO.Path.Combine(Application.dataPath, location);
                }
                return "";
            }
            
            protected static bool IsPathValid(string targetPath)
            {
                if (targetPath.IndexOfAny(kInvalidPathChars) >= 0)
                {
                    return false;
                }
                if (targetPath.Split(kPathSepChars, StringSplitOptions.None).Contains(string.Empty))
                {
                    return false;
                }
                return true;
            }
        }

        private static string s_newItemAssetLocation = "";

        protected static string NewItemAssetLocation
        {
            get
            {
                return s_newItemAssetLocation;
            }
            set
            {
                s_newItemAssetLocation = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            NewItemAssetLocation = EditorPrefs.GetString(NewItemSaveLocationSaveKey(), NewItemAssetLocation);
        }

        protected virtual string NewItemSaveLocationSaveKey()
        {
            return GetType().AssemblyQualifiedName + "NewItemSaveLocation";
        }
    }
}
#endif