﻿#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    
    public class UnityObjectDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = GetPropertyFieldType(property);

            DrawProperty(position, property, label, type);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, Type objType)
        {
            if (typeof(GameObject).IsAssignableFrom(objType) || typeof(Component).IsAssignableFrom(objType) || typeof(ScriptableObject).IsAssignableFrom(objType))
            {
                SearchPopupGUI.ObjectSearchPopupField(position, property, label, null, objType, true);
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        public Type GetPropertyFieldType(SerializedProperty property)
        {
            Type type = fieldInfo.FieldType;
            if (typeof(IList).IsAssignableFrom(type) && property.propertyType != SerializedPropertyType.Generic)
            {
                if (type.IsArray)
                {
                    type = type.GetElementType();
                }
                else
                {
                    type = type.GetGenericArguments()[0];
                }
            }
            return type;
        }
    }

    [CustomPropertyDrawer(typeof(Component), true)]
    public class ComponentDrawer : UnityObjectDrawer
    {
    }

    [CustomPropertyDrawer(typeof(GameObject), true)]
    public class GameObjectDrawer : UnityObjectDrawer
    {
    }

    [CustomPropertyDrawer(typeof(ScriptableObject), true)]
    public class ScriptableObjectDrawer : UnityObjectDrawer
    {
    }

    [CustomPropertyDrawer(typeof(GameObjectWithComponentFieldAttribute), true)]
    public class GameObjectWithComponentDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = fieldInfo.FieldType;
            GameObjectWithComponentFieldAttribute componentDataFieldAttribute = attribute as GameObjectWithComponentFieldAttribute;
            GUIContent defaultOptionLabel = componentDataFieldAttribute.NullLabel;
            Type[] componentTypes = componentDataFieldAttribute.ComponentTypes;

            DrawProperty(position, property, label, defaultOptionLabel, type, componentTypes);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes)
        {
            SearchPopupGUI.ObjectSearchPopupField(position, property, label, defaultOptionLabel, objType, componentTypes, true);
        }
    }

    [CustomPropertyDrawer(typeof(ComponentFieldAttribute), true)]
    public class ComponentFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type objType = fieldInfo.FieldType;
            ComponentFieldAttribute componentDataFieldAttribute = attribute as ComponentFieldAttribute;
            Type[] componentTypes = componentDataFieldAttribute.ComponentTypes;
            GUIContent defaultOptionLabel = componentDataFieldAttribute.NullLabel;
            bool allowNone = !componentDataFieldAttribute.DefaultToNonNull;

            DrawProperty(position, property, label, defaultOptionLabel, objType, componentTypes, allowNone);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes, bool allowNone)
        {
            SearchPopupGUI.ObjectSearchPopupField(position, property, label, defaultOptionLabel, objType, componentTypes, allowNone);
        }
    }

    [CustomPropertyDrawer(typeof(ObjectPopupAttribute), true)]
    public class ObjectPopupDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = fieldInfo.FieldType;
            ObjectPopupAttribute popupDefaultAttribute = attribute as ObjectPopupAttribute;
            GUIContent defaultOptionLabel = popupDefaultAttribute.NullLabel;
            bool allowNone = !popupDefaultAttribute.DefaultToNonNull;
            
            DrawProperty(position, property, label, defaultOptionLabel, type, allowNone);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, bool allowNone)
        {
            SearchPopupGUI.ObjectSearchPopupField(position, property, label, defaultOptionLabel, objType, null, allowNone);
        }
    }

    [CustomPropertyDrawer(typeof(ChildComponentPopupAttribute), true)]
    public class ChildComponentPopupDrawer : PropertyDrawer
    {
        protected Component[] m_cachedComponents;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type componentType = fieldInfo.FieldType;
            ChildComponentPopupAttribute childComponentPopupAttribute = attribute as ChildComponentPopupAttribute;
            GUIContent defaultOptionLabel = childComponentPopupAttribute.NullLabel;

            DrawProperty(position, property, label, defaultOptionLabel, componentType, ref m_cachedComponents);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type componentType, ref Component[] cachedComponents)
        {
            UnityEngine.Object target = property.serializedObject.targetObject;
            SearchPopupGUI.ChildComponentPopup(position, property, label, defaultOptionLabel, componentType, ref cachedComponents);
        }
    }
    
}
#endif