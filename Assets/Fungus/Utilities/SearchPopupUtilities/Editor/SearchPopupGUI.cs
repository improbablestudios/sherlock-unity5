#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{
    public delegate object DrawField(Rect position, GUIContent content, int controlID, bool on, object value);
    public delegate GUIContent GetSelectedDisplayOptionContent(object value, GUIContent defaultOptionLabel);
    public delegate object ProcessSelectedItem(int controlID, object value);
    public delegate SearchPopupWindow ShowSearchPopupWindow(int controlID, Rect rect, object value, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0);
    
    public class SearchPopupGUI
    {
        public static float SearchFieldPopupWidth
        {
            get
            {
                return 15f;
            }
        }

        private enum ObjectFieldVisualType
        {
            IconAndText,
            LargePreview,
            MiniPreview,
        }

        private static FieldInfo s_mixedValueContentField = typeof(EditorGUI).GetField("s_MixedValueContent", BindingFlags.Static | BindingFlags.NonPublic);
        private static FieldInfo s_mixedValueContentColorTempField = typeof(EditorGUI).GetField("s_MixedValueContentColorTemp", BindingFlags.Static | BindingFlags.NonPublic);
        private static MethodInfo s_validateObjectFieldAssignmentMethod = typeof(EditorGUI).GetMethod("ValidateObjectFieldAssignment", BindingFlags.Static | BindingFlags.NonPublic);
        private static MethodInfo s_drawObjectFieldLargeThumbMethod = typeof(EditorGUI).GetMethod("DrawObjectFieldLargeThumb", BindingFlags.Static | BindingFlags.NonPublic);
        private static MethodInfo s_drawObjectFieldMiniThumbMethod = typeof(EditorGUI).GetMethod("DrawObjectFieldMiniThumb", BindingFlags.Static | BindingFlags.NonPublic);
        private static MethodInfo s_pingObjectOrShowPreviewOnClick = typeof(EditorGUI).GetMethod("PingObjectOrShowPreviewOnClick", BindingFlags.Static | BindingFlags.NonPublic);

        public static GUIContent GetMixedValueContent()
        {
            return (GUIContent)s_mixedValueContentField.GetValue(null);
        }

        public static Color GetMixedValueColor()
        {
            return (Color)s_mixedValueContentColorTempField.GetValue(null);
        }

        public static void SetMixedValueColor(Color color)
        {
            s_mixedValueContentColorTempField.SetValue(null, color);
        }

        private static UnityEngine.Object ValidateObjectFieldAssignment(UnityEngine.Object[] references, System.Type objType, bool exactItemTypeMatch)
        {
            return (UnityEngine.Object)s_validateObjectFieldAssignmentMethod.Invoke(null, new object[] { references, objType, null, exactItemTypeMatch ? 1 : 0 });
        }

        private static void DrawObjectFieldLargeThumb(Rect position, int id, UnityEngine.Object obj, GUIContent content)
        {
            s_drawObjectFieldLargeThumbMethod.Invoke(null, new object[] { position, id, obj, content });
        }

        private static void DrawObjectFieldMiniThumb(Rect position, int id, UnityEngine.Object obj, GUIContent content)
        {
            s_drawObjectFieldMiniThumbMethod.Invoke(null, new object[] { position, id, obj, content });
        }

        private static void PingObjectOrShowPreviewOnClick(UnityEngine.Object targetObject, Rect position)
        {
            s_pingObjectOrShowPreviewOnClick.Invoke(null, new object[] { targetObject, position });
        }

        private static void BeginHandleMixedValueContentColor()
        {
            SetMixedValueColor(GUI.contentColor);
            GUI.contentColor = !EditorGUI.showMixedValue ? GUI.contentColor : GUI.contentColor * GetMixedValueColor();
        }

        private static void EndHandleMixedValueContentColor()
        {
            GUI.contentColor = GetMixedValueColor();
        }

        private static bool MainActionKeyForControl(Event evt, int controlId)
        {
            if (GUIUtility.keyboardControl != controlId)
            {
                return false;
            }
            bool flag = evt.alt || evt.shift || evt.command || evt.control;
            if (evt.type != EventType.KeyDown || (int)evt.character != 32 || flag)
            {
                return evt.type == EventType.KeyDown && (evt.keyCode == KeyCode.Space || evt.keyCode == KeyCode.Return || evt.keyCode == KeyCode.KeypadEnter) && !flag;
            }
            evt.Use();
            return false;
        }
        
        public static void ObjectSearchPopupField<T>(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, typeof(T), allowNone, flags);
        }

        public static void ObjectSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, objType, allowNone, flags);
        }

        public static void ObjectSearchPopupField<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, bool allowNone, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(position, property, label, defaultOptionLabel, typeof(T), allowNone, flags);
        }

        public static void ObjectSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, bool allowNone, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            UnityEngine.Object selectedObject = property.objectReferenceValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, property.serializedObject.targetObject, allowNone, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static T ObjectSearchPopupField<T>(T selectedObject, GUIContent label, GUIContent defaultOptionLabel,  UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, typeof(T), targetObject, allowNone, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, objType, targetObject, allowNone, flags);
        }

        public static T ObjectSearchPopupField<T>(Rect position, T selectedObject, GUIContent label, GUIContent defaultOptionLabel, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, typeof(T), targetObject, allowNone, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(Rect position, UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0)
        {
            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }
            
            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => UnityObjectReferenceSearchPopupWindow.Show(i, r, v as UnityEngine.Object, d, objType, null, targetObject, f);

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, showSearchPopupWindow, true, flags);

            if (!allowNone && selectedObject == null)
            {
                selectedObject = UnityObjectReferenceSearchPopupWindow.GetObjectList(objType, null, targetObject).FirstOrDefault();
                if (selectedObject != null)
                {
                    GUI.changed = true;
                }
            }

            return selectedObject;
        }

        public static void ObjectSearchPopupField<T>(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type[] componentTypes, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, typeof(T), componentTypes, allowNone, flags);
        }

        public static void ObjectSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, objType, componentTypes, allowNone, flags);
        }

        public static void ObjectSearchPopupField<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type[] componentTypes, bool allowNone, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(position, property, label, defaultOptionLabel, typeof(T), componentTypes, allowNone, flags);
        }

        public static void ObjectSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes, bool allowNone, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            UnityEngine.Object selectedObject = property.objectReferenceValue;
            
            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, componentTypes, property.serializedObject.targetObject, allowNone, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static T ObjectSearchPopupField<T>(T selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type[] componentTypes, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, typeof(T), componentTypes, targetObject, allowNone, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, objType, componentTypes, targetObject, allowNone, flags);
        }

        public static T ObjectSearchPopupField<T>(Rect position, T selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type[] componentTypes, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, typeof(T), componentTypes, targetObject, allowNone, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(Rect position, UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, Type[] componentTypes, UnityEngine.Object targetObject, bool allowNone, SearchPopupWindowFlags flags = 0)
        {
            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => UnityObjectReferenceSearchPopupWindow.Show(i, r, v as UnityEngine.Object, d, objType, componentTypes, targetObject, f);

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, showSearchPopupWindow, true, flags);
            
            if (!allowNone && selectedObject == null)
            {
                selectedObject = UnityObjectReferenceSearchPopupWindow.GetObjectList(objType, componentTypes, targetObject).FirstOrDefault();
                if (selectedObject != null)
                {
                    GUI.changed = true;
                }
            }

            return selectedObject;
        }

        public static void ObjectSearchPopupField<T>(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, T[] objects, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, typeof(T), objects, flags);
        }

        public static void ObjectSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, objType, objects, flags);
        }
        
        public static void ObjectSearchPopupField<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, T[] objects, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(position, property, label, defaultOptionLabel, typeof(T), objects, flags);
        }

        public static void ObjectSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            UnityEngine.Object selectedObject = property.objectReferenceValue;
            
            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, objects, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static T ObjectSearchPopupField<T>(T selectedObject, GUIContent label, GUIContent defaultOptionLabel, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, typeof(T), objects, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, objType, objects, flags);
        }

        public static T ObjectSearchPopupField<T>(Rect position, T selectedObject, GUIContent label, GUIContent defaultOptionLabel, T[] objects, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, typeof(T), objects, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(Rect position, UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, UnityEngine.Object[] objects, SearchPopupWindowFlags flags = 0)
        {
            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => UnityObjectReferenceSearchPopupWindow.Show(i, r, v as UnityEngine.Object, d, objType, objects, f);

            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, showSearchPopupWindow, false, flags);
        }

        public static void ObjectSearchPopupField<T>(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] searchFolders, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, typeof(T), searchFolders, flags);
        }

        public static void ObjectSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, string[] searchFolders, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, objType, searchFolders, flags);
        }

        public static void ObjectSearchPopupField<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] searchFolders, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            ObjectSearchPopupField(position, property, label, defaultOptionLabel, typeof(T), searchFolders, flags);
        }

        public static void ObjectSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type objType, string[] searchFolders, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            UnityEngine.Object selectedObject = property.objectReferenceValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            selectedObject = ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, objType, searchFolders, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static T ObjectSearchPopupField<T>(T selectedObject, GUIContent label, GUIContent defaultOptionLabel, string[] searchFolders, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, typeof(T), searchFolders, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type objType, string[] searchFolders, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ObjectSearchPopupField(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, objType, searchFolders, flags);
        }

        public static T ObjectSearchPopupField<T>(Rect position, T selectedObject, GUIContent label, GUIContent defaultOptionLabel, string[] searchFolders, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, typeof(T), searchFolders, flags) as T;
        }

        public static UnityEngine.Object ObjectSearchPopupField(Rect position, UnityEngine.Object value, GUIContent label, GUIContent defaultOptionLabel, Type objType, string[] searchFolders, SearchPopupWindowFlags flags = 0)
        {
            string path = AssetDatabase.GetAssetPath(value);
            EditorGUI.BeginChangeCheck();
            path = SearchPopupField(
                position, 
                path, 
                label, 
                defaultOptionLabel,
                null,
                (object v, GUIContent d) => UnityObjectReferenceSearchPopupWindow.GetSelectedDisplayOptionContent(value as UnityEngine.Object, objType, d),
                (int i, object v) => AssetSearchPopupWindow.ProcessSelectedPath(i, v as string),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => AssetSearchPopupWindow.Show(i, r, v as string, d, objType, searchFolders, f),
                null,
                flags) as string;
            if (EditorGUI.EndChangeCheck())
            {
                value = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
            }
            return value;
        }

        public static void ChildComponentPopup<T>(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, ref Component[] cachedComponents, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            ChildComponentPopup(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, typeof(T), ref cachedComponents, flags);
        }

        public static void ChildComponentPopup(Type componentType, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, ref Component[] cachedComponents, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ChildComponentPopup(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, componentType, ref cachedComponents, flags);
        }

        public static void ChildComponentPopup<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, ref Component[] cachedComponents, SearchPopupWindowFlags flags = 0) where T : UnityEngine.Object
        {
            ChildComponentPopup(position, property, label, defaultOptionLabel, typeof(T), ref cachedComponents, flags);
        }

        public static void ChildComponentPopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type componentType, ref Component[] cachedComponents, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            UnityEngine.Object selectedObject = property.objectReferenceValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            selectedObject = ChildComponentPopup(position, selectedObject, label, defaultOptionLabel, componentType, ref cachedComponents, property.serializedObject.targetObject, flags);

            EditorGUI.showMixedValue = mixed;
            
            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static T ChildComponentPopup<T>(T selectedObject, GUIContent label, GUIContent defaultOptionLabel, ref Component[] cachedComponents, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options) where T : UnityEngine.Object
        {
            return ChildComponentPopup(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, typeof(T), ref cachedComponents, targetObject, flags) as T;
        }

        public static UnityEngine.Object ChildComponentPopup(UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type componentType, UnityEngine.Object targetObject, ref Component[] cachedComponents, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ChildComponentPopup(EditorGUILayout.GetControlRect(options), selectedObject, label, defaultOptionLabel, componentType, ref cachedComponents, targetObject, flags);
        }

        public static UnityEngine.Object ChildComponentPopup(Rect position, UnityEngine.Object selectedObject, GUIContent label, GUIContent defaultOptionLabel, Type componentType, ref Component[] cachedComponents, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            if (cachedComponents == null || (Event.current.type == EventType.MouseDown && position.Contains(Event.current.mousePosition)))
            {
                cachedComponents = GetChildComponents(targetObject, componentType);
            }

            UnityEngine.Object[] objects = cachedComponents.Cast<UnityEngine.Object>().ToArray();

            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => UnityObjectReferenceSearchPopupWindow.Show(i, r, v as UnityEngine.Object, d, componentType, objects, f | SearchPopupWindowFlags.DisableFolders);

            return ObjectSearchPopupField(position, selectedObject, label, defaultOptionLabel, componentType, showSearchPopupWindow, false, flags);
        }
        
        public static Component[] GetChildComponents(UnityEngine.Object targetObject, Type componentType)
        {
            GameObject gameObject = targetObject as GameObject;
            Component component = targetObject as Component;
            if (gameObject != null)
            {
                return gameObject.GetComponentsInChildren(componentType, true);
            }
            else if (component != null)
            {
                return component.GetComponentsInChildren(componentType, true);
            }
            return new Component[0];
        }

        public static string ObjectToolbarSearchPopupField(Rect position, UnityEngine.Object value, GUIContent label, GUIContent defaultOptionLabel, Type objType, ShowSearchPopupWindow showSearchPopupWindow, bool allowDragAndDrop, SearchPopupWindowFlags flags = 0)
        {
            return SearchPopupField(
                position, 
                value, 
                label, 
                defaultOptionLabel,
                (Rect r, GUIContent c, int i, bool o, object v) =>
                {
                    string searchControlName = SearchPopupWindow.SearchControlName() + i;
                    EditorGUI.BeginChangeCheck();
                    GUI.SetNextControlName(searchControlName);
                    v = ToolbarSearchField(r, v as string);
                    if (EditorGUI.EndChangeCheck())
                    {
                        SearchPopupWindow.ProcessSearch(i, v as string);
                        if (GUIUtility.keyboardControl == 0)
                        {
                            SearchPopupWindow.InvokeOnSearch(null);
                        }
                    }
                    if (SearchPopupWindow.Instance != null && SearchPopupWindow.ControlID == i)
                    {
                        Event current = Event.current;
                        EventType eventType = current.type;
                        KeyCode keyCode = current.keyCode;
                        if ((eventType == EventType.Used || eventType == EventType.KeyDown) &&
                        (keyCode == KeyCode.UpArrow || keyCode == KeyCode.DownArrow || keyCode == KeyCode.Return || keyCode == KeyCode.KeypadEnter))
                        {
                            SearchPopupWindow.Instance.InvokeKeyboardCommand(keyCode);
                        }
                        if (GUIUtility.keyboardControl != 0)
                        {
                            EditorGUI.FocusTextInControl(searchControlName);
                        }
                    }
                    return v;
                },
                (object v, GUIContent d) => UnityObjectReferenceSearchPopupWindow.GetSelectedDisplayOptionContent(v as UnityEngine.Object, objType, d),
                (int i, object v) => UnityObjectReferenceSearchPopupWindow.ProcessSelectedObject(i, v as UnityEngine.Object),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) =>
                {
                    SearchPopupWindow searchPopupWindow = showSearchPopupWindow(i, r, v as UnityEngine.Object, d, f);
                    if (SearchPopupWindow.Instance != null)
                    {
                        SearchPopupWindow.ProcessSearch(i, (string)null);
                    }
                    return searchPopupWindow;
                },
                null,
                flags | SearchPopupWindowFlags.HideSearch
                ) as string;
        }

        public static UnityEngine.Object ObjectSearchPopupField(Rect position, UnityEngine.Object value, GUIContent label, GUIContent defaultOptionLabel, Type objType, ShowSearchPopupWindow showSearchPopupWindow, bool allowDragAndDrop, SearchPopupWindowFlags flags = 0)
        {
            Rect buttonRect = new Rect(position.xMax - SearchFieldPopupWidth, position.y, SearchFieldPopupWidth, position.height);

            return SearchPopupField(
                position, 
                value, 
                label, 
                defaultOptionLabel,
                null,
                (object v, GUIContent d) => UnityObjectReferenceSearchPopupWindow.GetSelectedDisplayOptionContent(v as UnityEngine.Object, objType, d),
                (int i, object v) => UnityObjectReferenceSearchPopupWindow.ProcessSelectedObject(i, v as UnityEngine.Object),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) =>
                {
                    if (buttonRect.Contains(Event.current.mousePosition))
                    {
                        return showSearchPopupWindow(i, r, v, d, f);
                    }
                    return null;
                },
                (Event e, int i) =>
                {
                    if (e.type == EventType.DragUpdated || e.type == EventType.DragPerform)
                    {
                        if (allowDragAndDrop)
                        {
                            if (position.Contains(Event.current.mousePosition) && GUI.enabled)
                            {
                                UnityEngine.Object[] objectReferences = DragAndDrop.objectReferences;
                                UnityEngine.Object target = ValidateObjectFieldAssignment(objectReferences, objType, false);
                                if (target != (UnityEngine.Object)null)
                                {
                                    DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
                                    if (e.type == EventType.DragPerform)
                                    {
                                        value = target;
                                        GUI.changed = true;
                                        DragAndDrop.AcceptDrag();
                                        DragAndDrop.activeControlID = 0;
                                    }
                                    else
                                        DragAndDrop.activeControlID = i;
                                    Event.current.Use();
                                }
                            }
                        }
                    }
                    if (e.type != EventType.Layout && e.type != EventType.ExecuteCommand && e.type != EventType.DragExited)
                    {
                        if (GUI.enabled)
                        {
                            HandleUtility.Repaint();
                        }
                    }
                    if (e.type == EventType.MouseDown && e.type == EventType.MouseDown && Event.current.button == 0)
                    {
                        if (position.Contains(Event.current.mousePosition) && !buttonRect.Contains(Event.current.mousePosition))
                        {
                            UnityEngine.Object clickedObject = value;
                            Component component = clickedObject as Component;
                            if ((bool)((UnityEngine.Object)component))
                                clickedObject = (UnityEngine.Object)component.gameObject;
                            if (EditorGUI.showMixedValue)
                                clickedObject = (UnityEngine.Object)null;
                            if (Event.current.clickCount == 1)
                            {
                                GUIUtility.keyboardControl = i;
                                PingObjectOrShowPreviewOnClick(clickedObject, position);
                                e.Use();
                            }
                            else if (Event.current.clickCount == 2)
                            {
                                if ((bool)clickedObject)
                                {
                                    AssetDatabase.OpenAsset(clickedObject);
                                    GUIUtility.ExitGUI();
                                }
                                e.Use();
                            }
                        }
                    }
                    return value;
                },
                flags
                ) as UnityEngine.Object;
        }
        
        public static void ComponentTypeSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            ComponentTypeSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, flags);
        }
        
        public static void ComponentTypeSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string value = property.stringValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            value = ComponentTypeSearchPopupField(position, Type.GetType(value), label, defaultOptionLabel, flags).AssemblyQualifiedName;

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = value;
            }

            EditorGUI.EndProperty();
        }
        
        public static Type ComponentTypeSearchPopupField(Type value, GUIContent label, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return ComponentTypeSearchPopupField(EditorGUILayout.GetControlRect(options), value, label, defaultOptionLabel, flags);
        }

        public static Type ComponentTypeSearchPopupField(Rect position, Type value, GUIContent label, GUIContent defaultOptionLabel, SearchPopupWindowFlags flags = 0)
        {
            return SearchPopupField(
                position, 
                value as Type, label, 
                defaultOptionLabel,
                null,
                (object v, GUIContent d) => ComponentTypeSearchPopupWindow.GetSelectedDisplayOptionContent(v as Type, d),
                (int i, object v) => ComponentTypeSearchPopupWindow.ProcessSelectedType(i, v as Type),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => ComponentTypeSearchPopupWindow.Show(i, r, v as Type, d, f),
                null, 
                flags) as Type;
        }

        public static void TypeSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type[] baseTypes, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            TypeSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, baseTypes, flags);
        }

        public static void TypeSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type[] baseTypes, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string value = property.stringValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            Type type = Type.GetType(value);

            type = TypeSearchPopupField(position, type, label, defaultOptionLabel, baseTypes, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                if (type != null)
                {
                    property.stringValue = type.AssemblyQualifiedName;
                }
                else
                {
                    property.stringValue = "";
                }
            }

            EditorGUI.EndProperty();
        }

        public static Type TypeSearchPopupField(Type value, GUIContent label, GUIContent defaultOptionLabel, Type[] baseTypes, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return TypeSearchPopupField(EditorGUILayout.GetControlRect(options), value, label, defaultOptionLabel, baseTypes, flags);
        }

        public static Type TypeSearchPopupField(Rect position, Type value, GUIContent label, GUIContent defaultOptionLabel, Type[] baseTypes, SearchPopupWindowFlags flags = 0)
        {
            return SearchPopupField(
                position, 
                value as Type, 
                label, 
                defaultOptionLabel,
                null,
                (object v, GUIContent d) => TypeSearchPopupWindow.GetSelectedDisplayOptionContent(v as Type, d),
                (int i, object v) => TypeSearchPopupWindow.ProcessSelectedType(i, v as Type),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => TypeSearchPopupWindow.Show(i, r, v as Type, d, baseTypes, f),
                null,
                flags) as Type;
        }

        public static void TextSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] values, bool allowOther, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            TextSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, values, values.Select(x => new GUIContent(x)).ToArray(), allowOther, flags);
        }
        
        public static void TextSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] values, bool allowOther, SearchPopupWindowFlags flags = 0)
        {
            TextSearchPopupField(position, property, label, defaultOptionLabel, values, values.Select(x => new GUIContent(x)).ToArray(), allowOther, flags);
        }
        
        public static void TextSearchPopupField(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, bool allowOther, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            TextSearchPopupField(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, values, displayOptions, allowOther, flags);
        }
        
        public static void TextSearchPopupField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, bool allowOther, SearchPopupWindowFlags flags = 0)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string text = property.stringValue;

            bool mixed = EditorGUI.showMixedValue;

            EditorGUI.showMixedValue = property.hasMultipleDifferentValues;

            text = TextSearchPopupField(position, text, label, defaultOptionLabel, values, displayOptions, allowOther, flags);

            EditorGUI.showMixedValue = mixed;

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = text;
            }

            EditorGUI.EndProperty();
        }

        public static string TextSearchPopupField(string value, GUIContent label, GUIContent defaultOptionLabel, string[] values, bool allowOther, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return TextSearchPopupField(EditorGUILayout.GetControlRect(options), value, label, defaultOptionLabel, values, values.Select(x => new GUIContent(x)).ToArray(), allowOther, flags);
        }

        public static string TextSearchPopupField(Rect position, string value, GUIContent label, GUIContent defaultOptionLabel, string[] values, bool allowOther, SearchPopupWindowFlags flags = 0)
        {
            return TextSearchPopupField(position, value, label, defaultOptionLabel, values, values.Select(x => new GUIContent(x)).ToArray(), allowOther, flags);
        }

        public static string TextSearchPopupField(string value, GUIContent label, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, bool allowOther, SearchPopupWindowFlags flags = 0, params GUILayoutOption[] options)
        {
            return TextSearchPopupField(EditorGUILayout.GetControlRect(options), value, label, defaultOptionLabel, values, displayOptions, allowOther, flags);
        }

        public static string TextSearchPopupField(Rect position, string value, GUIContent label, GUIContent defaultOptionLabel, string[] values, GUIContent[] displayOptions, bool allowOther, SearchPopupWindowFlags flags = 0)
        {
            return SearchPopupField(
                position, 
                value, 
                label, 
                defaultOptionLabel,
                (Rect r, GUIContent c, int i, bool o, object v) =>
                {
                    if (allowOther && SearchPopupWindow.Instance != null && SearchPopupWindow.ControlID == i)
                    {
                        string searchControlName = SearchPopupWindow.SearchControlName() + i;
                        EditorGUI.BeginChangeCheck();
                        GUI.SetNextControlName(searchControlName);
                        v = EditorGUI.TextField(r, v as string);
                        if (EditorGUI.EndChangeCheck())
                        {
                            SearchPopupWindow.ProcessSearch(i, v as string);
                        }
                        if (SearchPopupWindow.Instance != null && SearchPopupWindow.ControlID == i)
                        {
                            Event current = Event.current;
                            EventType eventType = current.type;
                            KeyCode keyCode = current.keyCode;
                            if ((eventType == EventType.Used || eventType == EventType.KeyDown) && 
                            (keyCode == KeyCode.UpArrow || keyCode == KeyCode.DownArrow || keyCode == KeyCode.Return || keyCode == KeyCode.KeypadEnter))
                            {
                                SearchPopupWindow.Instance.InvokeKeyboardCommand(keyCode);
                            }
                            if (GUIUtility.keyboardControl != 0)
                            {
                                EditorGUI.FocusTextInControl(searchControlName);
                            }
                        }
                    }
                    else
                    {
                        if (Event.current.type == EventType.Repaint)
                        {
                            DrawSearchPopupBackground(r, c, i);
                            DrawSearchPopupIcon(r, i);
                        }
                    }
                    return v;
                },
                (object v, GUIContent d) => StringSearchPopupWindow.GetSelectedDisplayOptionContent(v as string, values, displayOptions, d),
                (int i, object v) => StringSearchPopupWindow.ProcessSelectedString(i, v as string),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) =>
                {
                    if (allowOther)
                    {
                        f = f | SearchPopupWindowFlags.HideSearch;
                    }
                    SearchPopupWindow searchPopupWindow = StringSearchPopupWindow.Show(i, r, v as string, d, values, displayOptions, f);
                    if (SearchPopupWindow.Instance != null)
                    {
                        SearchPopupWindow.ProcessSearch(i, (string)null);
                    }
                    return searchPopupWindow;
                },
                null,
                flags) as string;
        }

        public static int GenericToolbarSearchPopupField(Rect position, GUIContent label, GUIContent defaultOptionLabel, object[] values, GUIContent[] displayOptions, ref string searchString, SearchPopupWindowFlags flags = 0)
        {
            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash + position.GetHashCode(), FocusType.Keyboard, position);

            if (label != null && (!string.IsNullOrEmpty(label.text) || label.image != null))
            {
                position = EditorGUI.PrefixLabel(position, controlId, label);
            }

            Rect fieldPosition = position;

            Event current = Event.current;
            EventType eventType = current.type;
            
            string searchControlName = SearchPopupWindow.SearchControlName() + controlId;
            EditorGUI.BeginChangeCheck();
            GUI.SetNextControlName(searchControlName);
            searchString = ToolbarSearchField(position, searchString);
            if (EditorGUI.EndChangeCheck())
            {
                SearchPopupWindow.ProcessSearch(controlId, searchString);
                if (GUIUtility.keyboardControl == 0)
                {
                    SearchPopupWindow.InvokeOnSearch(null);
                }
            }
            if (SearchPopupWindow.Instance != null && SearchPopupWindow.ControlID == controlId)
            {
                KeyCode keyCode = current.keyCode;
                if ((eventType == EventType.Used || eventType == EventType.KeyDown) &&
                (keyCode == KeyCode.UpArrow || keyCode == KeyCode.DownArrow || keyCode == KeyCode.Return || keyCode == KeyCode.KeypadEnter))
                {
                    SearchPopupWindow.Instance.InvokeKeyboardCommand(keyCode);
                }
                if (GUIUtility.keyboardControl != 0)
                {
                    EditorGUI.FocusTextInControl(searchControlName);
                }
            }
            
            Vector2 iconSize = EditorGUIUtility.GetIconSize();
            EditorGUIUtility.SetIconSize(new Vector2(12f, 12f));

            if (eventType == EventType.MouseDown && Event.current.button == 0 && position.Contains(Event.current.mousePosition))
            {
                if (GUI.enabled)
                {
                    Rect searchButtonPosition = GetSearchButtonPosition(position);
                    if (!searchButtonPosition.Contains(Event.current.mousePosition))
                    {
                        GenericSearchPopupWindow.Show(controlId, position, null, defaultOptionLabel, values, displayOptions, flags);
                        if (SearchPopupWindow.Instance != null && SearchPopupWindow.ControlID == controlId)
                        {
                            SearchPopupWindow.ProcessSearch(controlId, null);
                        }
                    }
                }
            }

            EditorGUIUtility.SetIconSize(iconSize);

            return controlId;
        }

        public static object GenericSearchPopupField(Rect position, object value, GUIContent label, GUIContent defaultOptionLabel, object[] values, GUIContent[] displayOptions, SearchPopupWindowFlags flags = 0)
        {
            return SearchPopupField(
                position,
                value, 
                label,
                defaultOptionLabel,
                null,
                (object v, GUIContent d) => GenericSearchPopupWindow.GetSelectedDisplayOptionContent(v, values, displayOptions, d),
                (int i, object v) => GenericSearchPopupWindow.ProcessSelectedItem(i, v),
                (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) =>
                {
                    SearchPopupWindow searchPopupWindow = GenericSearchPopupWindow.Show(i, r, v, d, values, displayOptions, f);
                    return searchPopupWindow;
                },
                null,
                flags);
        }

        public static object SearchPopupField(Rect position, object value, GUIContent label, GUIContent defaultOptionLabel, DrawField drawField, GetSelectedDisplayOptionContent getSelectedDisplayOptionContent, ProcessSelectedItem processSelectedItem, ShowSearchPopupWindow showSearchPopupWindow, Func<Event, int, object> processOtherEvents, SearchPopupWindowFlags flags = 0)
        {
            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash + position.GetHashCode(), FocusType.Keyboard, position);

            if (label != null && (!string.IsNullOrEmpty(label.text) || label.image != null))
            {
                position = EditorGUI.PrefixLabel(position, controlId, label);
            }

            Rect fieldPosition = position;

            Event current = Event.current;
            EventType eventType = current.type;

            Vector2 iconSize = EditorGUIUtility.GetIconSize();
            EditorGUIUtility.SetIconSize(new Vector2(12f, 12f));

            GUIContent content;
            if (EditorGUI.showMixedValue)
                content = GetMixedValueContent();
            else
                content = getSelectedDisplayOptionContent(value, defaultOptionLabel);

            if (drawField != null)
            {
                BeginHandleMixedValueContentColor();
                value = drawField(fieldPosition, content, controlId, DragAndDrop.activeControlID == controlId, value);
                EndHandleMixedValueContentColor();
            }

            if (!GUI.enabled && Event.current.rawType == EventType.MouseDown)
                eventType = Event.current.rawType;
            switch (eventType)
            {
                case EventType.KeyDown:
                    if (GUIUtility.keyboardControl == controlId)
                    {
                        if (current.keyCode == KeyCode.Backspace || current.keyCode == KeyCode.Delete)
                        {
                            value = null;
                            GUI.changed = true;
                            current.Use();
                        }
                        if (MainActionKeyForControl(current, controlId))
                        {
                            if (GUI.enabled)
                            {
                                if (SearchPopupWindow.Instance == null || SearchPopupWindow.ControlID != controlId)
                                {
                                    showSearchPopupWindow(controlId, position, value, defaultOptionLabel, flags);
                                }
                            }
                        }
                        break;
                    }
                    break;
                case EventType.Repaint:
                    if (drawField == null)
                    {
                        BeginHandleMixedValueContentColor();
                        DrawSearchPopupBackground(fieldPosition, content, controlId);
                        DrawSearchPopupIcon(fieldPosition, controlId);
                        EndHandleMixedValueContentColor();
                    }
                    break;
                default:
                    if (processOtherEvents != null)
                    {
                        value = processOtherEvents(current, controlId);
                    }
                    if (eventType == EventType.MouseDown && Event.current.button == 0 && position.Contains(Event.current.mousePosition))
                    {
                        if (GUI.enabled)
                        {
                            if (SearchPopupWindow.Instance == null || SearchPopupWindow.ControlID != controlId)
                            {
                                showSearchPopupWindow(controlId, position, value, defaultOptionLabel, flags);
                            }
                        }
                    }
                    value = processSelectedItem(controlId, value);
                    break;
            }
            EditorGUIUtility.SetIconSize(iconSize);
            return value;
        }

        public static void DrawSearchPopupBackground(Rect rect, GUIContent content, int controlID)
        {
            float buttonWidth = SearchFieldPopupWidth;

            GUIStyle style = new GUIStyle(EditorStyles.objectField);
            GUIStyle boxStyle = new GUIStyle(EditorStyles.textField);
            style.normal = boxStyle.normal;
            style.hover = boxStyle.hover;
            style.active = boxStyle.active;
            style.focused = boxStyle.focused;
            style.onNormal = boxStyle.onNormal;
            style.onHover = boxStyle.onHover;
            style.onActive = boxStyle.onActive;
            style.onFocused = boxStyle.onFocused;
            style.border = boxStyle.border;
            style.margin = boxStyle.margin;
            style.padding = boxStyle.padding;
            style.padding.right += (int)buttonWidth;

            style.Draw(rect, content, controlID, DragAndDrop.activeControlID == controlID);
        }

        public static void DrawSearchPopupIcon(Rect rect, int controlID)
        {
            float buttonWidth = SearchFieldPopupWidth;

            GUIStyle popupStyle = new GUIStyle("ToolbarSeachTextField");
            popupStyle.border = new RectOffset();
            popupStyle.imagePosition = ImagePosition.ImageOnly;

            Rect searchButtonPosition = rect;
            searchButtonPosition.xMin = searchButtonPosition.xMax - buttonWidth;
            searchButtonPosition.xMin -= 6;
            searchButtonPosition.xMax -= 5f;
            searchButtonPosition.yMin += 3;
            searchButtonPosition.yMax -= 3;
            searchButtonPosition.y += 1f;
            searchButtonPosition.x += 2f;

            Texture2D dropdownIcon = EditorGUIUtility.FindTexture(EditorGUIUtility.isProSkin ? "d_icon dropdown" : "icon dropdown");
            Rect dropdownIconPosition = searchButtonPosition;
            dropdownIconPosition.size = new Vector2(4f, 3f);
            dropdownIconPosition.x += 5f;
            dropdownIconPosition.y += 2f;

            using (new GUI.ClipScope(searchButtonPosition))
            {
                Rect searchIconRect = new Rect(0, 0, popupStyle.normal.background.width, popupStyle.normal.background.height);
                searchIconRect.width = -searchIconRect.width;
                searchIconRect.x -= searchIconRect.width;
                searchIconRect.x += 6;
                searchIconRect.y -= 3;
                popupStyle.Draw(searchIconRect, new GUIContent(null, null, "Change selection"), controlID, false);
            }
            Color oldColor = GUI.color;
            if (DragAndDrop.activeControlID != controlID)
            {
                GUI.color = new Color(1f, 1f, 1f, 0.45f);
            }
            GUI.DrawTexture(dropdownIconPosition, dropdownIcon);
            GUI.color = oldColor;
        }

        public static string SearchField(GUIContent label, string text, params GUILayoutOption[] options)
        {
            return SearchField(EditorGUILayout.GetControlRect(options), label, text);
        }

        public static string SearchField(Rect position, GUIContent label, string text)
        {
            if (label != null && (!string.IsNullOrEmpty(label.text) || label.image != null))
            {
                position = EditorGUI.PrefixLabel(position, label);
            }
            return SearchField(position, text);
        }

        public static string SearchField(string text, params GUILayoutOption[] options)
        {
            return SearchField(EditorGUILayout.GetControlRect(options), text);
        }

        public static string SearchField(Rect position, string text)
        {
            GUIStyle searchTextFieldStyle = GUI.skin.FindStyle("SearchTextField");
            GUIStyle searchCancelButtonStyle = GUI.skin.FindStyle("SearchCancelButton");
            GUIStyle searchCancelButtonEmptyStyle = GUI.skin.FindStyle("SearchCancelButtonEmpty");
            return SearchField(position, text, searchTextFieldStyle, searchCancelButtonStyle, searchCancelButtonEmptyStyle);
        }

        public static string ToolbarSearchField(GUIContent label, string text, params GUILayoutOption[] options)
        {
            return ToolbarSearchField(EditorGUILayout.GetControlRect(options), label, text);
        }

        public static string ToolbarSearchField(Rect position, GUIContent label, string text)
        {
            if (label != null && (!string.IsNullOrEmpty(label.text) || label.image != null))
            {
                position = EditorGUI.PrefixLabel(position, label);
            }
            return ToolbarSearchField(position, text);
        }

        public static string ToolbarSearchField(string text, params GUILayoutOption[] options)
        {
            return ToolbarSearchField(EditorGUILayout.GetControlRect(options), text);
        }

        public static string ToolbarSearchField(Rect position, string text)
        {
            GUIStyle searchTextFieldStyle = GUI.skin.FindStyle("ToolbarSeachTextField");
            GUIStyle searchCancelButtonStyle = GUI.skin.FindStyle("ToolbarSeachCancelButton");
            GUIStyle searchCancelButtonEmptyStyle = GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty");
            return SearchField(position, text, searchTextFieldStyle, searchCancelButtonStyle, searchCancelButtonEmptyStyle);
        }

        public static string SearchField(Rect position, string text, GUIStyle searchTextFieldStyle, GUIStyle searchCancelButtonStyle, GUIStyle searchCancelButtonEmptyStyle)
        {
            text = EditorGUI.TextField(GetSearchTextFieldPosition(position), text, searchTextFieldStyle);
            if (GUI.Button(GetSearchButtonPosition(position), GUIContent.none, string.IsNullOrEmpty(text) ? searchCancelButtonEmptyStyle : searchCancelButtonStyle) && !string.IsNullOrEmpty(text))
            {
                text = "";
                GUIUtility.keyboardControl = 0;
                GUI.changed = true;
            }
            return text;
        }

        public static Rect GetSearchTextFieldPosition(Rect position)
        {
            position.width -= 15f;
            return position;
        }

        public static Rect GetSearchButtonPosition(Rect position)
        {
            position.x += position.width - 15f;
            position.width = 15f;
            return position;
        }
    }

}
#endif