﻿using System;
using UnityEngine;

namespace ImprobableStudios.SearchPopupUtilities
{

    public class NullLabelAttribute : PropertyAttribute
    {
        public NullLabelAttribute()
        {
        }

        public NullLabelAttribute(string nullLabel)
        {
            if (nullLabel != null)
            {
                this.NullLabel = new GUIContent(nullLabel);
            }
        }

        public GUIContent NullLabel
        {
            get;
            set;
        }
    }

    public class NullLabelWithDefaultAttribute : NullLabelAttribute
    {
        public NullLabelWithDefaultAttribute() : base()
        {
        }

        public NullLabelWithDefaultAttribute(string nullLabel) : base(nullLabel)
        {
        }

        public NullLabelWithDefaultAttribute(bool defaultToNonNull) : base()
        {
            this.DefaultToNonNull = defaultToNonNull;
        }

        public NullLabelWithDefaultAttribute(string nullLabel, bool defaultToNonNull) : base(nullLabel)
        {
            this.DefaultToNonNull = defaultToNonNull;
        }

        public bool DefaultToNonNull
        {
            get;
            set;
        }
    }

    public class ObjectPopupAttribute : NullLabelWithDefaultAttribute
    {
        public ObjectPopupAttribute() : base()
        {
        }

        public ObjectPopupAttribute(string nullLabel) : base(nullLabel)
        {
        }

        public ObjectPopupAttribute(bool defaultToNonNull) : base(defaultToNonNull)
        {
        }

        public ObjectPopupAttribute(string nullLabel, bool defaultToNonNull) : base(nullLabel, defaultToNonNull)
        {
        }
    }

    public class ChildComponentPopupAttribute : NullLabelAttribute
    {
        public ChildComponentPopupAttribute() : base()
        {
        }

        public ChildComponentPopupAttribute(string nullLabel) : base(nullLabel)
        {
        }
    }

    public class GameObjectWithComponentFieldAttribute : NullLabelWithDefaultAttribute
    {
        public GameObjectWithComponentFieldAttribute(params Type[] componentTypes) : base()
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentFieldAttribute(string nullLabel, params Type[] componentTypes) : base(nullLabel)
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentFieldAttribute(bool defaultToNonNull, params Type[] componentTypes) : base(defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentFieldAttribute(string nullLabel, bool defaultToNonNull, params Type[] componentTypes) : base(nullLabel, defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public Type[] ComponentTypes
        {
            get;
            set;
        }
    }

    public class ComponentFieldAttribute : NullLabelWithDefaultAttribute
    {
        public ComponentFieldAttribute(params Type[] componentTypes) : base()
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentFieldAttribute(string nullLabel, params Type[] componentTypes) : base(nullLabel)
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentFieldAttribute(bool defaultToNonNull, params Type[] componentTypes) : base(defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentFieldAttribute(string nullLabel, bool defaultToNonNull, params Type[] componentTypes) : base(nullLabel, defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public Type[] ComponentTypes
        {
            get;
            set;
        }
    }

    public class TypeNameFieldAttribute : NullLabelAttribute
    {
        public TypeNameFieldAttribute(params Type[] baseTypes) : base()
        {
            BaseTypes = baseTypes;
        }

        public TypeNameFieldAttribute(string nullLabel, params Type[] baseTypes) : base(nullLabel)
        {
            BaseTypes = baseTypes;
        }

        public Type[] BaseTypes
        {
            get;
            set;
        }
    }

}
