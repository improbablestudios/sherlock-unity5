#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{
    
    [CanEditMultipleObjects, CustomEditor(typeof(Draggable), true)]
    public class DraggableEditor : PersistentBehaviourEditor
    {
        private Color m_rotatableColliderColor = Color.yellow;
        private int m_handleControlId;
        
        public void OnSceneGUI()
        {
            Draggable t = target as Draggable;

            if (t.Rotatable)
            {
                bool flag = GUIUtility.hotControl == this.m_handleControlId;
                int hotControl = GUIUtility.hotControl;
                Color color = Handles.color;
                bool enabled = GUI.enabled;

                Handles.color = m_rotatableColliderColor;
                if (!Selection.Contains(t.gameObject) && !flag)
                {
                    GUI.enabled = false;
                    Handles.color = new Color(0f, 0f, 0f, 0.001f);
                }

                Vector3 lossyScale = t.transform.lossyScale;
                float maxAxisScale = Mathf.Max(Mathf.Max(Mathf.Abs(lossyScale.x), Mathf.Abs(lossyScale.y)), Mathf.Abs(lossyScale.z));
                float worldRadius = maxAxisScale * t.RotationBoundsRadius;
                worldRadius = Mathf.Abs(worldRadius);
                worldRadius = Mathf.Max(worldRadius, 1E-05f);
                Vector3 worldPosition = t.transform.TransformPoint(t.RotationBoundsOffset);
                
                float newWorldRadius = Handles.RadiusHandle(Quaternion.identity, worldPosition, worldRadius, true);
                Vector3 normal = t.transform.rotation * new Vector3(0f, 0f, 1f);
                Handles.DrawWireDisc(worldPosition, normal, worldRadius);
                if (GUI.changed)
                {
                    Undo.RecordObject(t, "Adjust Radius");
                    t.RotationBoundsRadius = newWorldRadius / maxAxisScale;
                }

                if (hotControl != GUIUtility.hotControl && GUIUtility.hotControl != 0)
                {
                    this.m_handleControlId = GUIUtility.hotControl;
                }
                Handles.color = color;
                GUI.enabled = enabled;
            }
        }
    }

}

#endif