﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.InputUtilities
{

    public class KeyboardShortcutManagerSettingsProvider : BaseAssetSettingsProvider<KeyboardShortcutManager>
    {
        public KeyboardShortcutManagerSettingsProvider() : base("Project/Managers/Keyboard Shortcut Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new KeyboardShortcutManagerSettingsProvider();
        }
    }

    public class KeyboardShortcutManagerSettingsBuilder : BaseAssetSettingsBuilder<KeyboardShortcutManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif