#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{
    
    public class InputMenuItems
    {
        [MenuItem("Tools/Input/Controllable Input Module", false, 10)]
        [MenuItem("GameObject/Input/Controllable Input Module", false, 10)]
        static void CreateControllableInputModule()
        {
            PrefabSpawner.SpawnPrefab<ControllableInputModule>("~ControllableInputModule", true);
        }

        [MenuItem("Tools/Input/Draggable", false, 100)]
        [MenuItem("GameObject/Input/Draggable", false, 100)]
        static void CreateDraggable()
        {
            PrefabSpawner.SpawnPrefab<Draggable>("~Draggable", true, typeof(SpriteRenderer));
        }

        [MenuItem("Tools/Input/Target Zone", false, 100)]
        [MenuItem("GameObject/Input/Target Zone", false, 100)]
        static void CreateTargetZone()
        {
            PrefabSpawner.SpawnPrefab<TargetZone>("~TargetZone", true);
        }

        [MenuItem("Tools/Input/Target Grid", false, 100)]
        [MenuItem("GameObject/Input/Target Grid", false, 100)]
        static void CreateTargetGrid()
        {
            PrefabSpawner.SpawnPrefab<TargetGrid>("~TargetGrid", true);
        }
    }

}
#endif