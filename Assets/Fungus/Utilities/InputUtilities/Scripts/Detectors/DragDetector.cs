﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Drag Detector")]
    public class DragDetector : InputDetector, IDragHandler
    {
        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}
