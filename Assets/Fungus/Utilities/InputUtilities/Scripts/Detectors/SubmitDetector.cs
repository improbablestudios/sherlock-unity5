﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Submit Detector")]
    public class SubmitDetector : InputDetector, ISubmitHandler
    {
        void ISubmitHandler.OnSubmit(BaseEventData eventData)
        {
            if (m_DetectInput)
            {
                m_onInputDetected.Invoke();
            }
        }
    }

}
