﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Cancel Detector")]
    public class CancelDetector : InputDetector, ICancelHandler
    {
        void ICancelHandler.OnCancel(BaseEventData eventData)
        {
            if (m_DetectInput)
            {
                m_onInputDetected.Invoke();
            }
        }
    }

}
