﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/End Drag Detector")]
    public class EndDragDetector : InputDetector, IEndDragHandler
    {
        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}