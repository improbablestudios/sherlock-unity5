﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Drop Detector")]
    public class DropDetector : InputDetector, IDropHandler
    {
        void IDropHandler.OnDrop(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}