﻿using UnityEngine.Serialization;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityObjectUtilities;
using UnityEngine.UI;

namespace ImprobableStudios.InputUtilities
{
    
    public abstract class InputDetector : PersistentBehaviour
    {
        [Tooltip("If true, this detector will detect input, otherwise, it won't")]
        [SerializeField]
        [FormerlySerializedAs("interactable")]
        [FormerlySerializedAs("m_Interactable")]
        protected bool m_DetectInput = true;

        protected UnityEvent m_onInputDetected = new UnityEvent();

        public bool DetectInput
        {
            get
            {
                return m_DetectInput;
            }
            set
            {
                m_DetectInput = value;
            }
        }

        public UnityEvent OnInputDetected
        {
            get
            {
                return m_onInputDetected;
            }
        }

        protected virtual void Start()
        {
            if (gameObject.GetComponent<UIBehaviour>() == null && GetComponent<Collider>() == null && GetComponent<Collider2D>() == null)
            {
                gameObject.AddComponent<PolygonCollider2D>();
            }
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_DetectInput))
            {
                return GetInputDetectionError(gameObject);
            }

            return base.GetPropertyError(propertyPath);
        }

        public static string GetInputDetectionError(GameObject gameObject)
        {
            UIBehaviour uiBehaviour = gameObject.GetComponent<UIBehaviour>();
            Collider collider = gameObject.GetComponent<Collider>();
            Collider2D collider2D = gameObject.GetComponent<Collider2D>();

            if (uiBehaviour == null &&
                collider == null &&
                collider2D == null)
            {
                return "Object does not have a Collider or UIBehaviour component attached";
            }

            if (uiBehaviour != null)
            {
                Canvas canvas = uiBehaviour.GetComponentInParent<Canvas>(true);
                if (canvas == null)
                {
                    return "Object is not in a canvas";
                }
                if (canvas.GetComponent<GraphicRaycaster>() == null)
                {
                    return "Canvas does not have a GraphicRaycaster component attached";
                }
            }
            if (collider2D != null)
            {
                if (Camera.main != null)
                {
                    if (Camera.main.GetComponent<Physics2DRaycaster>() == null)
                    {
                        return "Main camera does not have a Physics2DRaycaster component attached";
                    }
                }
            }
            else if (collider != null)
            {
                if (Camera.main != null)
                {
                    if (Camera.main.GetComponent<PhysicsRaycaster>() == null)
                    {
                        return "Main camera does not have a PhysicsRaycaster component attached";
                    }
                }
            }
            return null;
        }
    }

}
