﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Pointer Up Detector")]
    public class PointerUpDetector : InputDetector, IPointerUpHandler
    {
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}
