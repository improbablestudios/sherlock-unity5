﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Select Detector")]
    public class SelectDetector : InputDetector, ISelectHandler
    {
        void ISelectHandler.OnSelect(BaseEventData eventData)
        {
            if (m_DetectInput)
            {
                m_onInputDetected.Invoke();
            }
        }
    }

}
