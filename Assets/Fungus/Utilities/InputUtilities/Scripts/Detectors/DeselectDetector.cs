﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Deselect Detector")]
    public class DeselectDetector : InputDetector, IDeselectHandler
    {
        void IDeselectHandler.OnDeselect(BaseEventData eventData)
        {
            if (m_DetectInput)
            {
                m_onInputDetected.Invoke();
            }
        }
    }

}
