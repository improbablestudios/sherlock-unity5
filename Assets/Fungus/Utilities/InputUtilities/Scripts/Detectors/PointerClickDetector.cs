﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Pointer Click Detector")]
    public class PointerClickDetector : InputDetector, IPointerClickHandler
    {
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}
