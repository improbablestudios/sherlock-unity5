﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Begin Drag Detector")]
    public class BeginDragDetector : InputDetector, IBeginDragHandler
    {
        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}