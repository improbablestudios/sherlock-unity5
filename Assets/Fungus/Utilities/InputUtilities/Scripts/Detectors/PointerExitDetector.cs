﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Pointer Exit Detector")]
    public class PointerExitDetector : InputDetector, IPointerExitHandler
    {
        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            if (m_DetectInput)
            {
                m_onInputDetected.Invoke();
            }
        }
    }

}
