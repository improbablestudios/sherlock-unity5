﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Detector/Pointer Down Detector")]
    public class PointerDownDetector : InputDetector, IPointerDownHandler
    {
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            m_onInputDetected.Invoke();
        }
    }

}
