﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Circle Edge Collider 2D")]
    public class CircleEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        [Tooltip("Radius of the circle")]
        [Min(0)]
        [SerializeField]
        protected float m_Radius = 1f;
        
        [Tooltip("The number of points to use when creating the circle's edges")]
        [Min(1)]
        [SerializeField]
        protected int m_Smoothness = 30;

        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public float Radius
        {
            get
            {
                return m_Radius;
            }
            set
            {
                m_Radius = value;
                SetPoints();
            }
        }

        public int Smoothness
        {
            get
            {
                return m_Smoothness;
            }
            set
            {
                m_Smoothness = value;
                SetPoints();
            }
        }

        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        void SetPoints()
        {
            Vector2[] points = new Vector2[m_Smoothness + 1];

            for (int i = 0; i <= m_Smoothness; i++)
            {
                float angle = (Mathf.PI * 2.0f / m_Smoothness) * i;
                points[i] = m_Offset + (new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * m_Radius);
            }

            EdgeCollider2D.points = points;
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}