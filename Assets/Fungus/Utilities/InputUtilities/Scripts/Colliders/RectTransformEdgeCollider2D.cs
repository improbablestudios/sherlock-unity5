﻿using UnityEngine;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Rect Transform Edge Collider 2D")]
    public class RectTransformEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The rect transform to match the shape of")]
        [SerializeField]
        protected RectTransform m_RectTransform;

        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }
        
        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        public RectTransform RectTransform
        {
            get
            {
                return m_RectTransform;
            }
            set
            {
                m_RectTransform = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        private void Update()
        {
            SetPoints();
        }

        void SetPoints()
        {
            if (m_RectTransform == null)
            {
                return;
            }

            if (Camera.main == null)
            {
                return;
            }

            Vector3[] rectTransformWorldCorners = new Vector3[4];

            m_RectTransform.GetWorldCorners(rectTransformWorldCorners);
            if (m_RectTransform.GetComponentInParent<Canvas>() != null && m_RectTransform.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceOverlay)
            {
                for (int i = 0; i < rectTransformWorldCorners.Length; i++)
                {
                    rectTransformWorldCorners[i] = Camera.main.ScreenToWorldPoint(rectTransformWorldCorners[i]);
                }
            }

            Vector2[] points = new Vector2[5];

            points[0] = rectTransformWorldCorners[0];
            points[1] = rectTransformWorldCorners[1];
            points[2] = rectTransformWorldCorners[2];
            points[3] = rectTransformWorldCorners[3];
            points[4] = rectTransformWorldCorners[0];

            for (int i = 0; i < points.Length; i++)
            {
                points[i] += m_Offset;
            }

            EdgeCollider2D.points = points;
        }
        
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}