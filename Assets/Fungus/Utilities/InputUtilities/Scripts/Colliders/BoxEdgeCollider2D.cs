﻿using UnityEngine;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Box Edge Collider 2D")]
    public class BoxEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.one;

        [Tooltip("Size of the box")]
        [SerializeField]
        protected Vector2 m_Size = Vector2.one;

        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public Vector2 Size
        {
            get
            {
                return m_Size;
            }
            set
            {
                m_Size = value;
                SetPoints();
            }
        }

        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        void SetPoints()
        {
            Vector2[] points = new Vector2[5];

            points[0] = m_Offset + (new Vector2(-m_Size.x, -m_Size.y) * 0.5f);
            points[1] = m_Offset + (new Vector2(m_Size.x, -m_Size.y) * 0.5f);
            points[2] = m_Offset + (new Vector2(m_Size.x, m_Size.y) * 0.5f);
            points[3] = m_Offset + (new Vector2(-m_Size.x, m_Size.y) * 0.5f);
            points[4] = m_Offset + (new Vector2(-m_Size.x, -m_Size.y) * 0.5f);

            EdgeCollider2D.points = points;
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}