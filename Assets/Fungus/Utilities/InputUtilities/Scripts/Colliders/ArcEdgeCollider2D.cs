﻿using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Arc Edge Collider 2D")]
    public class ArcEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        [Tooltip("The rotation of the arc")]
        [Range(0, 360)]
        [SerializeField]
        protected int m_Rotation = 0;

        [Tooltip("Radius of the arc")]
        [Min(0)]
        [SerializeField]
        protected float m_Radius = 1;

        [Tooltip("The total angle of the arc")]
        [Range(0, 360)]
        [SerializeField]
        protected int m_TotalAngle = 45;

        [Tooltip("The number of points to use when creating the arc's edges")]
        [Min(1)]
        [SerializeField]
        protected int m_Smoothness = 30;

        [Tooltip("If true, the arc is displayed as a slice with two lines extending from the edges of the arc to the center")]
        [SerializeField]
        protected bool m_Slice = true;

        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public int Rotation
        {
            get
            {
                return m_Rotation;
            }
            set
            {
                m_Rotation = value;
                SetPoints();
            }
        }

        public float Radius
        {
            get
            {
                return m_Radius;
            }
            set
            {
                m_Radius = value;
                SetPoints();
            }
        }

        public int TotalAngle
        {
            get
            {
                return m_TotalAngle;
            }
            set
            {
                m_TotalAngle = value;
                SetPoints();
            }
        }

        public int Smoothness
        {
            get
            {
                return m_Smoothness;
            }
            set
            {
                m_Smoothness = value;
                SetPoints();
            }
        }

        public bool Slice
        {
            get
            {
                return m_Slice;
            }
            set
            {
                m_Slice = value;
                SetPoints();
            }
        }

        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        public void SetPoints()
        {
            List<Vector2> points = new List<Vector2>();

            float ang = m_Rotation - (m_TotalAngle / 2);

            if (m_Slice && m_TotalAngle % 360 != 0)
            {
                points.Add(m_Offset + Vector2.zero);
            }

            for (int i = 0; i <= m_Smoothness; i++)
            {
                float x = m_Radius * Mathf.Cos(ang * Mathf.Deg2Rad);
                float y = m_Radius * Mathf.Sin(ang * Mathf.Deg2Rad);

                points.Add(m_Offset + new Vector2(x, y));

                ang += (float)m_TotalAngle / m_Smoothness;
            }

            if (m_Slice && m_TotalAngle % 360 != 0)
            {
                points.Add(m_Offset + Vector2.zero);
            }

            EdgeCollider2D.points = points.ToArray();
        }
        
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}