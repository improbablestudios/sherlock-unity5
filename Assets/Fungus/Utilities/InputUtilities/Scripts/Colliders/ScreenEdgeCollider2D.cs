﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.InputUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Screen Edge Collider 2D")]
    public class ScreenEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        [Tooltip("Size of the screen box")]
        [Min(0)]
        [SerializeField]
        protected float m_Size = 1;
        
        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public float Size
        {
            get
            {
                return m_Size;
            }
            set
            {
                m_Size = value;
                SetPoints();
            }
        }

        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        private void Update()
        {
            SetPoints();
        }

        void SetPoints()
        {
            if (Camera.main == null)
            {
                return;
            }

            Vector2[] points = new Vector2[5];

            float screenAspect = (float)Screen.width / (float)Screen.height;
            float cameraHeight = Camera.main.orthographicSize * 2 * m_Size;
            float cameraWidth = cameraHeight * screenAspect;
            Vector2 size = new Vector2(cameraWidth, cameraHeight);

            points[0] = ((Vector2)Camera.main.transform.position + new Vector2(-size.x, -size.y) * 0.5f) - ((Vector2)transform.position);
            points[1] = ((Vector2)Camera.main.transform.position + new Vector2(size.x, -size.y) * 0.5f) - ((Vector2)transform.position);
            points[2] = ((Vector2)Camera.main.transform.position + new Vector2(size.x, size.y) * 0.5f) - ((Vector2)transform.position);
            points[3] = ((Vector2)Camera.main.transform.position + new Vector2(-size.x, size.y) * 0.5f) - ((Vector2)transform.position);
            points[4] = ((Vector2)Camera.main.transform.position + new Vector2(-size.x, -size.y) * 0.5f) - ((Vector2)transform.position);

            points[0] += (Camera.main.orthographicSize * 2 * m_Offset);
            points[1] += (Camera.main.orthographicSize * 2 * m_Offset);
            points[2] += (Camera.main.orthographicSize * 2 * m_Offset);
            points[3] += (Camera.main.orthographicSize * 2 * m_Offset);
            points[4] += (Camera.main.orthographicSize * 2 * m_Offset);

            EdgeCollider2D.points = points;
        }
        
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}