﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.InputUtilities
{

    [ExecuteInEditMode]
    [RequireComponent(typeof(PolygonCollider2D))]
    [AddComponentMenu("Input/Collider/Rect Transform Collider 2D")]
    public class RectTransformCollider2D : PersistentBehaviour
    {
        [Tooltip("The rect transform to match the shape of")]
        [SerializeField]
        protected RectTransform m_RectTransform;

        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        private PolygonCollider2D m_polygonCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public PolygonCollider2D PolygonCollider2D
        {
            get
            {
                if (m_polygonCollider2D == null)
                {
                    m_polygonCollider2D = GetComponent<PolygonCollider2D>();
                }
                return m_polygonCollider2D;
            }
            set
            {
                m_polygonCollider2D = value;
            }
        }

        public RectTransform RectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
            set
            {
                m_RectTransform = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        private void Update()
        {
            SetPoints();
        }

        void SetPoints()
        {
            if (Camera.main == null)
            {
                return;
            }

            Vector3[] rectTransformWorldCorners = new Vector3[4];

            RectTransform.GetWorldCorners(rectTransformWorldCorners);
            if (RectTransform.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceOverlay)
            {
                for (int i = 0; i < rectTransformWorldCorners.Length; i++)
                {
                    rectTransformWorldCorners[i] = Camera.main.ScreenToWorldPoint(rectTransformWorldCorners[i]);
                }
            }

            Vector2[] points = new Vector2[5];

            points[0] = rectTransformWorldCorners[0];
            points[1] = rectTransformWorldCorners[1];
            points[2] = rectTransformWorldCorners[2];
            points[3] = rectTransformWorldCorners[3];
            points[4] = rectTransformWorldCorners[0];

            for (int i = 0; i < points.Length; i++)
            {
                points[i] += m_Offset;
            }

            PolygonCollider2D.points = points;
        }
        
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}