﻿using UnityEngine;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(EdgeCollider2D))]
    [AddComponentMenu("Input/Collider/Ellipse Edge Collider 2D")]
    public class EllipseEdgeCollider2D : PersistentBehaviour
    {
        [Tooltip("The local offset of the collider geometry")]
        [SerializeField]
        protected Vector2 m_Offset = Vector2.zero;

        [Tooltip("Radius of the ellipse in the x direction")]
        [Min(0)]
        [SerializeField]
        protected float m_RadiusX = 1;

        [Tooltip("Radius of the ellipse in the y direction")]
        [Min(0)]
        [SerializeField]
        protected float m_RadiusY = 2;

        [Tooltip("The number of points to use when creating the circle's edges")]
        [Min(1)]
        [SerializeField]
        protected int m_Smoothness = 30;

        [Tooltip("The rotation of the ellipse")]
        [SerializeField]
        protected int m_Rotation = 0;

        private EdgeCollider2D m_edgeCollider2D;

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SetPoints();
            }
        }

        public float RadiusX
        {
            get
            {
                return m_RadiusX;
            }
            set
            {
                m_RadiusX = value;
                SetPoints();
            }
        }

        public float RadiusY
        {
            get
            {
                return m_RadiusY;
            }
            set
            {
                m_RadiusY = value;
                SetPoints();
            }
        }

        public int Smoothness
        {
            get
            {
                return m_Smoothness;
            }
            set
            {
                m_Smoothness = value;
                SetPoints();
            }
        }

        public int Rotation
        {
            get
            {
                return m_Rotation;
            }
            set
            {
                m_Rotation = value;
                SetPoints();
            }
        }

        public EdgeCollider2D EdgeCollider2D
        {
            get
            {
                if (m_edgeCollider2D == null)
                {
                    m_edgeCollider2D = GetComponent<EdgeCollider2D>();
                }
                return m_edgeCollider2D;
            }
            set
            {
                m_edgeCollider2D = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            SetPoints();
        }
        
        protected virtual void Start()
        {
            SetPoints();
        }

        public void SetPoints()
        {
            Vector2[] points = new Vector2[m_Smoothness + 1];

            float ang = 0;
            float o = m_Rotation * Mathf.Deg2Rad;

            for (int i = 0; i <= m_Smoothness; i++)
            {
                float a = ang * Mathf.Deg2Rad;

                float x = m_RadiusX * Mathf.Cos(a) * Mathf.Cos(o) - m_RadiusY * Mathf.Sin(a) * Mathf.Sin(o);
                float y = -m_RadiusX * Mathf.Cos(a) * Mathf.Sin(o) - m_RadiusY * Mathf.Sin(a) * Mathf.Cos(o);

                points[i] = m_Offset + new Vector2(x, y);

                ang += 360f / m_Smoothness;
            }
            
            EdgeCollider2D.points = points;
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            SetPoints();
        }
    }

}