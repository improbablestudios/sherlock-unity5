﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ScriptableBehaviourUtilities;

namespace ImprobableStudios.InputUtilities
{

    public class KeyboardShortcut : IdentifiableScriptableObject, INestedAsset
    {
        ///<summary>
        ///The default name of a new keyboard shortcut.
        ///</summary>
        public const string DEFAULT_KEYBOARD_SHORTCUT_NAME = "NEW SHORTCUT";

        [Tooltip("Is keyboard shortcut active?")]
        [SerializeField]
        protected bool m_Active = true;

        [Serializable]
        public class Binding
        {
            [Tooltip("The keys that when pressed together, execute this keyboard shortcut")]
            [FormerlySerializedAs("keys")]
            public KeyCode[] m_Keys = new KeyCode[0];
        }
        
        [Tooltip("The key bindings for this keyboard shortcut")]
        [SerializeField]
        [FormerlySerializedAs("bindings")]
        protected Binding[] m_Bindings = new Binding[0];

        [Tooltip("Event to execute when keyboard shortcut is detected")]
        [SerializeField]
        protected UnityEvent m_OnPressed = new UnityEvent();
        
        protected UnityEvent m_onNextPressed = new UnityEvent();

        private GameObject m_previousSelectedGameObject;

        private bool m_wasPressed;
        
        public Binding[] Bindings
        {
            get
            {
                return m_Bindings;
            }
            set
            {
                m_Bindings = value;
            }
        }

        public UnityEvent OnPressed
        {
            get
            {
                return m_OnPressed;
            }
        }

        public UnityEvent OnNextPressed
        {
            get
            {
                return m_onNextPressed;
            }
        }

        public bool Active
        {
            get
            {
                return m_Active;
            }
            set
            {
                m_Active = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            ScriptableBehaviourManager.OnUpdate += Update;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            ScriptableBehaviourManager.OnUpdate -= Update;
        }
        
        protected void Update()
        {
            if (m_Active)
            {
                if (EventSystem.current != null)
                {
                    bool isPressed = KeyboardShortcutIsPressed();
                    if (ShouldCheckKeyboardPresses())
                    {
                        if (!m_wasPressed && isPressed)
                        {
                            m_wasPressed = true;
                            InvokeOnPressed();
                        }
                    }
                    if (m_wasPressed && !isPressed)
                    {
                        m_wasPressed = false;
                    }
                }
            }
        }

        /// <summary>
        /// Force the shortcut to invoke the a button press.
        /// </summary>
        public void ForceShortcutPress()
        {
            InvokeOnPressed();
        }
        
        /// <summary>
        /// Simulate a keyboard press by invoking active keyboard shortcuts.
        /// </summary>
        /// <param name="shortcutName">The name of the keyboard shortcut</param>
        /// <returns>True if the shortcut was active in the screen and was able to be invoked.  False otherwise.</returns>
        public static bool SimulatePress(string shortcutName)
        {
            foreach (KeyboardShortcut keyboardShortcut in GetAllActive<KeyboardShortcut>())
            {
                if (keyboardShortcut.name == shortcutName)
                {
                    keyboardShortcut.InvokeOnPressed();
                    return true;
                }
            }
            return false;
        }

        public bool KeyboardShortcutIsPressed()
        {
            if (EventSystem.current != null && (ControllableInputModule.Instance == null || ControllableInputModule.Instance.ProcessKeyboardEvents))
            {
                for (int i = 0; i < m_Bindings.Length; i++)
                {
                    Binding binding = m_Bindings[i];
                    bool allDown = true;
                    for (int j = 0; j < binding.m_Keys.Length; j++)
                    {
                        allDown &= Input.GetKey(binding.m_Keys[j]);
                    }

                    if (allDown)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected virtual bool ShouldCheckKeyboardPresses()
        {
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
            if (selectedGameObject == null)
            {
                m_previousSelectedGameObject = selectedGameObject;
                return true;
            }

            if (selectedGameObject != m_previousSelectedGameObject)
            {
                if (selectedGameObject.GetComponent<InputField>() != null)
                {
                    m_previousSelectedGameObject = selectedGameObject;
                    return false;
                }
            }
            
            m_previousSelectedGameObject = selectedGameObject;
            return true;
        }

        private void InvokeOnPressed()
        {
            if (m_Active)
            {
                OnPressed.Invoke();
                OnNextPressed.Invoke();
                OnNextPressed.RemoveAllPersistentAndNonPersistentListeners();
            }
        }
    }

}
