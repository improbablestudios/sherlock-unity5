﻿using System;
using UnityEngine;
using ImprobableStudios.BaseUtilities;
using System.Linq;
using System.Collections.Generic;

namespace ImprobableStudios.InputUtilities
{

    public class KeyboardShortcutManager : SettingsScriptableObject<KeyboardShortcutManager>
    {
        [Tooltip("The possible keyboard shortcuts")]
        [SerializeField]
        protected List<KeyboardShortcut> m_KeyboardShortcuts = new List<KeyboardShortcut>();

        public KeyboardShortcut[] KeyboardShortcuts
        {
            get
            {
                return m_KeyboardShortcuts.ToArray();
            }
            set
            {
                m_KeyboardShortcuts = value.ToList();
            }
        }
    }

}
