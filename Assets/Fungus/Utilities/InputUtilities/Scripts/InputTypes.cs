﻿namespace ImprobableStudios.InputUtilities
{
    
    public enum InputType
    {
        PointAndClick,
        DragAndDrop,
        Draw,
        Arrow,
        Text,
        Number,
    }

    public enum MoveTargetType
    {
        Zone,
        Grid,
        Position
    }

    public enum RotateTargetType
    {
        Zone,
        Rotation
    }

}