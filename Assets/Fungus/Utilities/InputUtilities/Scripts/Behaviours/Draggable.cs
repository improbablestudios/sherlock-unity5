﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.VectorUtilities;

namespace ImprobableStudios.InputUtilities
{

    public enum DragBounds
    {
        Screen,
        AnyDragArea,
        DragArea,
        Nothing,
    }

    public enum SnapTo
    {
        AnyTarget,
        Target,
        Nothing,
    }

    [RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
    [AddComponentMenu("Input/Draggable")]
    public class Draggable : PersistentBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [Tooltip("Allow object to be moved or rotated by clicking and dragging it")]
        [SerializeField]
        protected bool m_Interactable = true;

        [Tooltip("Allow object to be moved by dragging")]
        [SerializeField]
        [FormerlySerializedAs("isMovable")]
        [FormerlySerializedAs("m_IsMovable")]
        protected bool m_Movable = true;

        [Tooltip("What this draggable snaps to when moving")]
        [SerializeField]
        [FormerlySerializedAs("snapTo")]
        protected SnapTo m_SnapTo;

        [Tooltip("The target to snap to")]
        [SerializeField]
        [FormerlySerializedAs("snapTarget")]
        protected Target m_SnapTarget;

        [Tooltip("Don't snap if the target is occupied by another draggable")]
        [SerializeField]
        [FormerlySerializedAs("dontSnapIfTargetIsOccupied")]
        protected bool m_DontSnapIfTargetIsOccupied = true;

        [Tooltip("The move speed of the draggable")]
        [Min(0)]
        [SerializeField]
        protected float m_MoveSpeed = 32f;

        [Tooltip("Allow object to be rotated by dragging")]
        [SerializeField]
        [FormerlySerializedAs("isRotatable")]
        [FormerlySerializedAs("m_IsRotatable")]
        protected bool m_Rotatable;

        [Tooltip("The radius of the circle outside of which the sprite can be rotated rather than dragged")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("colliderRadius")]
        [FormerlySerializedAs("m_ColliderRadius")]
        protected float m_RotationBoundsRadius = 0.9f;

        [Tooltip("The center of the circle outside of which the sprite can be rotated rather than dragged")]
        [SerializeField]
        [FormerlySerializedAs("colliderOffset")]
        [FormerlySerializedAs("m_ColliderOffset")]
        protected Vector2 m_RotationBoundsOffset;

        [Tooltip("The amount to increment the rotation by when dragging")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("incrementAngle")]
        [FormerlySerializedAs("m_IncrementAngle")]
        protected int m_AngleSnapIncrement = 15;

        [Tooltip("The rotation speed of the draggable")]
        [Min(0)]
        [SerializeField]
        protected float m_RotationSpeed = 15f;

        [Tooltip("Draggable will collide with any colliders on these layers")]
        [SerializeField]
        protected LayerMask m_CollisionLayers = 1;

        [Tooltip("Draggable will collide with other draggables")]
        [SerializeField]
        protected bool m_CollideWithOtherDraggables;

        [Tooltip("Move the object to the front of the other draggables when clicked")]
        [SerializeField]
        [FormerlySerializedAs("moveToFrontWhenClicked")]
        protected bool m_MoveToFrontWhenClicked = true;
        
        protected UnityEvent m_onBeginMove = new UnityEvent();
        
        protected UnityEvent m_onMove = new UnityEvent();
        
        protected UnityEvent m_onEndMove = new UnityEvent();
        
        protected UnityEvent m_onBeginRotate = new UnityEvent();
        
        protected UnityEvent m_onRotate = new UnityEvent();
        
        protected UnityEvent m_onEndRotate = new UnityEvent();
        
        protected bool m_isMoving;
        
        protected bool m_isRotating;
        
        protected Vector2 m_startingPosition;
        
        protected Vector2 m_mouseWorldPositionOffset;
        
        protected float m_mouseWorldEulerAngleZOffset;
        
        protected Vector3 m_startingRotation;
        
        protected Vector2 m_moveVelocity;
        
        protected float m_rotateVelocity;

        private Collider2D m_collider2D;

        private Rigidbody2D m_rigidbody2D;
        
        public bool Interactable
        {
            get
            {
                return m_Interactable;
            }
            set
            {
                m_Interactable = value;
            }
        }

        public bool Movable
        {
            get
            {
                return m_Movable;
            }
            set
            {
                m_Movable = value;
            }
        }
        
        public SnapTo SnapTo
        {
            get
            {
                return m_SnapTo;
            }
            set
            {
                m_SnapTo = value;
            }
        }
        
        public Target SnapTarget
        {
            get
            {
                return m_SnapTarget;
            }
            set
            {
                m_SnapTarget = value;
            }
        }
        
        public bool DontSnapIfTargetIsOccupied
        {
            get
            {
                return m_DontSnapIfTargetIsOccupied;
            }
            set
            {
                m_DontSnapIfTargetIsOccupied = value;
            }
        }
        
        public float MoveSpeed
        {
            get
            {
                return m_MoveSpeed;
            }
            set
            {
                m_MoveSpeed = value;
            }
        }
        
        public bool Rotatable
        {
            get
            {
                return m_Rotatable;
            }
            set
            {
                m_Rotatable = value;
            }
        }
        
        public float RotationBoundsRadius
        {
            get
            {
                return m_RotationBoundsRadius;
            }
            set
            {
                m_RotationBoundsRadius = value;
            }
        }
        
        public Vector2 RotationBoundsOffset
        {
            get
            {
                return m_RotationBoundsOffset;
            }
            set
            {
                m_RotationBoundsOffset = value;
                Rigidbody2D.centerOfMass = m_RotationBoundsOffset;
            }
        }
        
        public int AngleSnapIncrement
        {
            get
            {
                return m_AngleSnapIncrement;
            }
            set
            {
                m_AngleSnapIncrement = value;
            }
        }

        public LayerMask CollisionLayers
        {
            get
            {
                return m_CollisionLayers;
            }
            set
            {
                m_CollisionLayers = value;
            }
        }

        public bool CollideWithOtherDraggables
        {
            get
            {
                return m_CollideWithOtherDraggables;
            }
            set
            {
                m_CollideWithOtherDraggables = value;
            }
        }

        public bool MoveToFrontWhenClicked
        {
            get
            {
                return m_MoveToFrontWhenClicked;
            }
            set
            {
                m_MoveToFrontWhenClicked = value;
            }
        }
        
        public UnityEvent OnBeginMove
        {
            get
            {
                return m_onBeginMove;
            }
        }

        public UnityEvent OnMove
        {
            get
            {
                return m_onMove;
            }
        }

        public UnityEvent OnEndMove
        {
            get
            {
                return m_onEndMove;
            }
        }

        public UnityEvent OnBeginRotate
        {
            get
            {
                return m_onBeginRotate;
            }
        }

        public UnityEvent OnRotate
        {
            get
            {
                return m_onRotate;
            }
        }

        public UnityEvent OnEndRotate
        {
            get
            {
                return m_onEndRotate;
            }
        }

        public Rigidbody2D Rigidbody2D
        {
            get
            {
                if (m_rigidbody2D == null)
                {
                    m_rigidbody2D = gameObject.GetRequiredComponent<Rigidbody2D>();
                }
                return m_rigidbody2D;
            }
        }
        
        protected override void Reset()
        {
            base.Reset();

            UpdateRigidbody2DSettings();
        }

        protected virtual void Start()
        {
            m_startingPosition = transform.position;
            m_startingRotation = transform.eulerAngles;
            if (m_collider2D == null) // Find collider originally on object and use as dragCollider
            {
                m_collider2D = gameObject.GetComponent<Collider2D>();
            }
            if (m_rigidbody2D == null)
            {
                m_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
            }
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            MoveToFront();
            ScanForCollidersToIgnore();
        }

        protected virtual void Update()
        {
            UpdateRigidbody2DSettings();
        }

        protected virtual void FixedUpdate()
        {
            if (m_Interactable)
            {
                if (m_Movable)
                {
                    if (m_isMoving)
                    {
                        Move();
                        OnMove.Invoke();
                    }
                }

                if (m_Rotatable)
                {
                    if (m_isRotating)
                    {
                        Rotate();
                        OnRotate.Invoke();
                    }
                }
            }
        }

        public void UpdateRigidbody2DSettings()
        {
            Rigidbody2D rigidbody2D = Rigidbody2D;

            if (!m_Interactable || (!m_Movable && !m_Rotatable))
            {
                rigidbody2D.bodyType = RigidbodyType2D.Static;
                return;
            }
            else
            {
                rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            }
            
            if (m_isMoving && m_isRotating)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.None;
            }
            else if (m_isMoving && !m_isRotating)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
            else if (!m_isMoving && m_isRotating)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezePosition;
            }
            else if (!m_isMoving && !m_isRotating)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
            }

            rigidbody2D.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            rigidbody2D.drag = 0f;
            rigidbody2D.angularDrag = 0f;
            rigidbody2D.gravityScale = 0f;
            rigidbody2D.centerOfMass = m_RotationBoundsOffset;
        }

        public void ScanForCollidersToIgnore()
        {
            foreach (Collider2D otherCollider in FindObjectsOfType<Collider2D>())
            {
                foreach (Collider2D thisCollider in GetAttachedColliders())
                {
                    bool willColliderWithLayer = (((1 << otherCollider.gameObject.layer) & m_CollisionLayers) != 0);
                    Physics2D.IgnoreCollision(thisCollider, otherCollider, !willColliderWithLayer);
                }
            }
            
            foreach (Draggable otherDraggable in GetAllActive<Draggable>())
            {
                if (otherDraggable != this)
                {
                    foreach (Collider2D otherCollider in otherDraggable.GetAttachedColliders())
                    {
                        foreach (Collider2D thisCollider in GetAttachedColliders())
                        {
                            if (!m_CollideWithOtherDraggables)
                            {
                                Physics2D.IgnoreCollision(thisCollider, otherCollider);
                            }
                        }
                    }
                }
            }
        }

        public Collider2D[] GetAttachedColliders()
        {
            Collider2D[] attachedColliders = new Collider2D[Rigidbody2D.attachedColliderCount];
            Rigidbody2D.GetAttachedColliders(attachedColliders);
            return attachedColliders;
        }

        public void ResetPosition(float duration, Ease easeType, UnityAction onComplete = null)
        {
            this.transform.DOMove(m_startingPosition, duration)
                .SetEase(easeType)
                .OnComplete(
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    });
        }

        public void ResetRotation(float duration, Ease easeType, UnityAction onComplete = null)
        {
            this.transform.DORotate(m_startingRotation, duration)
                .SetEase(easeType);
        }

        public Vector2 GetMousePositionInWorld()
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        public Vector2 GetPositionInWorld(Vector2 position)
        {
            RenderMode renderMode = RenderMode.WorldSpace;
            Canvas parentCanvas = this.GetComponentInParent<Canvas>();
            if (parentCanvas != null)
            {
                renderMode = parentCanvas.renderMode;
            }

            Vector2 worldSpacePosition = position;
            if (renderMode == RenderMode.ScreenSpaceOverlay || renderMode == RenderMode.ScreenSpaceCamera)
            {
                worldSpacePosition = Camera.main.ScreenToWorldPoint(position);
            }

            return worldSpacePosition;
        }

        public Vector2 GetPosition(Vector2 worldSpacePosition)
        {
            RenderMode renderMode = RenderMode.WorldSpace;
            Canvas parentCanvas = this.GetComponentInParent<Canvas>();
            if (parentCanvas != null)
            {
                renderMode = parentCanvas.renderMode;
            }

            Vector2 position = worldSpacePosition;
            if (renderMode == RenderMode.ScreenSpaceOverlay || renderMode == RenderMode.ScreenSpaceCamera)
            {
                position = Camera.main.WorldToScreenPoint(worldSpacePosition);
            }

            return position;
        }
        
        private void SetPosition(Vector2 worldSpacePosition)
        {
            Vector2 position = GetPosition(worldSpacePosition);
            transform.position = new Vector3(position.x, position.y, transform.position.z);
        }
        
        public bool WillSnap(Vector2 targetPosition)
    {
            Target containingTarget = Target.GetTargetContainingPoint(targetPosition);
            if (containingTarget != null)
            {
                if (m_SnapTo == SnapTo.AnyTarget || m_SnapTo == SnapTo.Target && m_SnapTarget == containingTarget)
                {
                    if (!m_DontSnapIfTargetIsOccupied || !TargetIsOccupiedAtPosition(containingTarget, targetPosition))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public Vector2 GetSnapPosition(Vector2 targetPosition)
        {
            Target containingTarget = Target.GetTargetContainingPoint(targetPosition);
            if (containingTarget != null)
            {
                if (m_SnapTo == SnapTo.AnyTarget || m_SnapTo == SnapTo.Target && m_SnapTarget == containingTarget)
                {
                    if (!m_DontSnapIfTargetIsOccupied || !TargetIsOccupiedAtPosition(containingTarget, targetPosition))
                    {
                        return containingTarget.GetSnappedPosition(targetPosition);
                    }
                }
            }
            return Vector2.zero;
        }

        protected bool TargetIsOccupiedAtPosition(Target target, Vector2 targetPosition)
        {
            TargetGrid grid = target as TargetGrid;
            TargetZone zone = target as TargetZone;
            bool targetIsOccupied = false;
            if (grid != null)
            {
                Vector2 targetTileCoordinate = grid.GetTileCoordinate(targetPosition);
                
                foreach (Draggable draggable in GetAllActive<Draggable>())
                {
                    if (grid.TileContains(targetTileCoordinate, draggable.transform.position))
                    {
                        if (draggable != this)
                        {
                            targetIsOccupied = true;
                        }
                    }
                }
            }
            else if (zone != null)
            {
                foreach (Draggable draggable in GetAllActive<Draggable>())
                {
                    if (zone.Contains(draggable.transform.position))
                    {
                        if (draggable != this)
                        {
                            targetIsOccupied = true;
                        }
                    }
                }
            }
            return targetIsOccupied;
        }

        public void MoveToFront()
        {
            SetPrimaryActive(this);
            Draggable[] activeDraggables = GetAllActive<Draggable>();
            for (int i = 0; i < activeDraggables.Length; i++)
            {
                Draggable activeDraggable = activeDraggables[i];
                Vector3 newPosition = activeDraggable.transform.position;
                newPosition.z = i * 0.001f;
                activeDraggable.transform.position = newPosition;
            }
        }

        protected virtual void OnMoveStarted()
        {
            ScanForCollidersToIgnore();

            Vector2 mousePosition = GetMousePositionInWorld();

            Vector2 draggablePosition = GetPositionInWorld(transform.position);

            m_mouseWorldPositionOffset = mousePosition - draggablePosition;

            m_isMoving = true;
            m_isRotating = false;
        }

        protected virtual void OnRotateStarted()
        {
            Vector2 mouseWorldPosition = GetMousePositionInWorld();

            m_mouseWorldEulerAngleZOffset = GetAngleToPosition(mouseWorldPosition) - Rigidbody2D.rotation;

            Vector3 worldPosition = GetRotationAreaWorldPosition();
            float worldRadius = GetRotationAreaWorldRadius();

            if (Math3d.IsPointInCircle(mouseWorldPosition, worldPosition, worldRadius))
            {
                m_isRotating = false;
                m_isMoving = true;
            }
            else
            {
                m_isRotating = true;
                m_isMoving = false;
            }
        }

        protected virtual void Move()
        {
            Vector2 mouseWorldPosition = GetMousePositionInWorld();
            Vector2 targetWorldPosition = mouseWorldPosition - m_mouseWorldPositionOffset;
            Vector2 draggableWorldPosition = GetPositionInWorld(transform.position);

            Vector2 positionHeading = targetWorldPosition - draggableWorldPosition;

            if (WillSnap(mouseWorldPosition))
            {
                SetPosition(GetSnapPosition(mouseWorldPosition));
                m_moveVelocity = Vector2.zero;
            }
            else
            {
                m_moveVelocity = positionHeading * m_MoveSpeed;
            }
            
            Rigidbody2D.MovePosition(Rigidbody2D.position + m_moveVelocity * Time.fixedDeltaTime);
        }

        protected virtual void Rotate()
        {
            Vector2 mouseWorldPosition = GetMousePositionInWorld();
            float targetWorldEulerAngle = GetAngleToPosition(mouseWorldPosition) - m_mouseWorldEulerAngleZOffset;
            float draggableWorldEulerAngle = Rigidbody2D.rotation;

            float snappedTargetWorldAngle = GetSnappedAngle(targetWorldEulerAngle);

            float target = Math3d.NormalizeAngle((int)snappedTargetWorldAngle);

            float current = Math3d.NormalizeAngle((int)draggableWorldEulerAngle);

            float difference = target - current;

            float rotationHeading = difference;

            if (Math.Abs(difference) > 180)
            {
                if (difference < 0)
                {
                    rotationHeading = (difference + 360);
                }
                else if (difference > 0)
                {
                    rotationHeading = (difference - 360);
                }
            }

            m_rotateVelocity = rotationHeading * m_RotationSpeed;

            if (m_rotateVelocity == 0)
            {
                Rigidbody2D.rotation = GetSnappedAngle(Rigidbody2D.rotation);
            }
            else
            {
                Rigidbody2D.MoveRotation(Rigidbody2D.rotation + m_rotateVelocity * Time.fixedDeltaTime);
            }
        }

        public Vector3 GetRotationAreaWorldPosition()
        {
            return transform.TransformPoint(m_RotationBoundsOffset);
        }

        public float GetRotationAreaWorldRadius()
        {
            float radiusMultiplier = Mathf.Max(Mathf.Max(Mathf.Abs(transform.lossyScale.x), Mathf.Abs(transform.lossyScale.y)), Mathf.Abs(transform.lossyScale.z));
            float worldRadius = Mathf.Abs(radiusMultiplier * m_RotationBoundsRadius);
            worldRadius = Mathf.Max(worldRadius, 1E-05f);
            return worldRadius;
        }

        public float GetAngleToPosition(Vector2 mouseWorldPosition)
        {
            Vector2 rotatationCenter = transform.TransformPoint(m_RotationBoundsOffset);
            Vector2 rotationWorldCenter = GetPositionInWorld(rotatationCenter);
            Vector2 heading = mouseWorldPosition - rotationWorldCenter;

            return Mathf.Atan2(heading.y, heading.x) * Mathf.Rad2Deg;
        }

        public int GetSnappedAngle(float targetAngle)
        {
            return (int)(Math.Round(targetAngle / m_AngleSnapIncrement) * m_AngleSnapIncrement);
        }
        
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (m_Interactable)
            {
                if (m_Movable)
                {
                    OnMoveStarted();
                    OnBeginMove.Invoke();
                }

                if (m_Rotatable)
                {
                    OnRotateStarted();
                    OnBeginRotate.Invoke();
                }
            }

            if (m_MoveToFrontWhenClicked)
            {
                MoveToFront();
            }
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (m_Interactable)
            {
                if (m_isMoving)
                {
                    OnEndMove.Invoke();
                }
                if (m_isRotating)
                {
                    Rigidbody2D.rotation = GetSnappedAngle(Rigidbody2D.rotation);
                    OnEndRotate.Invoke();
                }
                m_isMoving = false;
                m_isRotating = false;
                m_moveVelocity = Vector2.zero;
                m_rotateVelocity = 0;
            }
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Movable))
            {
                if (!m_Interactable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveSpeed))
            {
                if (!m_Interactable || !m_Movable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_SnapTo))
            {
                if (!m_Interactable || !m_Movable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_SnapTarget))
            {
                if (!m_Interactable || !m_Movable || m_SnapTo != SnapTo.Target)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_DontSnapIfTargetIsOccupied))
            {
                if (!m_Interactable || !m_Movable || m_SnapTo == SnapTo.Nothing)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Rotatable))
            {
                if (!m_Interactable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_RotationBoundsRadius))
            {
                if (!m_Interactable || !m_Rotatable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_RotationBoundsOffset))
            {
                if (!m_Interactable || !m_Rotatable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AngleSnapIncrement))
            {
                if (!m_Interactable || !m_Rotatable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_RotationSpeed))
            {
                if (!m_Interactable || !m_Rotatable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_CollideWithOtherDraggables))
            {
                if (m_CollisionLayers == 0)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveToFrontWhenClicked))
            {
                if (!m_Interactable)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyIndentLevelOffset(string propertyPath)
        {
            if (propertyPath == nameof(m_SnapTo))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_SnapTarget))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_DontSnapIfTargetIsOccupied))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_MoveSpeed))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_RotationBoundsRadius))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_RotationBoundsOffset))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_AngleSnapIncrement))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_RotationSpeed))
            {
                return 1;
            }
            else
            {
                return base.GetPropertyIndentLevelOffset(propertyPath);
            }
        }
    }

}
