﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.InputUtilities
{
    
    public abstract class Target : PersistentBehaviour
    {
        [Tooltip("Determines where objects will snap to")]
        [SerializeField]
        [FormerlySerializedAs("tileAlignment")]
        [FormerlySerializedAs("m_TileAlignment")]
        protected SpriteAlignment m_SnapAlignment = SpriteAlignment.Center;

        [Tooltip("Custom pivot that objects will snap to")]
        [SerializeField]
        [FormerlySerializedAs("customTileAlignment")]
        [FormerlySerializedAs("m_CustomTileAlignment")]
        protected Vector2 m_CustomSnapAlignment;
        
        public SpriteAlignment SnapAlignment
        {
            get
            {
                return m_SnapAlignment;
            }
            set
            {
                m_SnapAlignment = value;
            }
        }

        public Vector2 CustomSnapAlignment
        {
            get
            {
                return m_CustomSnapAlignment;
            }
            set
            {
                m_CustomSnapAlignment = value;
            }
        }
        
        public Vector2 GetPivotValue()
        {
            switch (m_SnapAlignment)
            {
                case SpriteAlignment.Center:
                    return new Vector2(0.5f, 0.5f);
                case SpriteAlignment.TopLeft:
                    return new Vector2(0f, 1f);
                case SpriteAlignment.TopCenter:
                    return new Vector2(0.5f, 1f);
                case SpriteAlignment.TopRight:
                    return new Vector2(1f, 1f);
                case SpriteAlignment.LeftCenter:
                    return new Vector2(0f, 0.5f);
                case SpriteAlignment.RightCenter:
                    return new Vector2(1f, 0.5f);
                case SpriteAlignment.BottomLeft:
                    return new Vector2(0f, 0f);
                case SpriteAlignment.BottomCenter:
                    return new Vector2(0.5f, 0f);
                case SpriteAlignment.BottomRight:
                    return new Vector2(1f, 0f);
                case SpriteAlignment.Custom:
                    return m_CustomSnapAlignment;
                default:
                    return Vector2.zero;
            }
        }

        public static Target GetTargetContainingPoint(Vector2 point)
        {
            foreach (Target target in GetAllActive<Target>())
            {
                if (target.Contains(point))
                {
                    return target;
                }
            }
            return null;
        }

        public abstract bool Contains(Vector2 point);

        public abstract Vector2 GetSnappedPosition(Vector2 position);
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_CustomSnapAlignment))
            {
                if (m_SnapAlignment != SpriteAlignment.Custom)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
