﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using System;

namespace ImprobableStudios.InputUtilities
{
    
    public abstract class InputEventGenerator : PersistentBehaviour, IInputEventGenerator
    {
        [Tooltip("Only execute event if this gameobject can be clicked (i.e. only if a raycast to the center of this gameobject will not be blocked by another gameobject)")]
        [SerializeField]
        [CheckNotNull]
        protected bool m_DontExecuteWhenObscured = true;
        
        protected void ExecuteEventIfAllowed<T>(ExecuteEvents.EventFunction<T> functor) where T : IEventSystemHandler
        {
            if (AllowExecution())
            {
                ExecuteEvent(functor);
            }
        }

        protected void ExecuteEvent<T>(ExecuteEvents.EventFunction<T> functor) where T : IEventSystemHandler
        {
            BaseEventData baseEvent = new BaseEventData(EventSystem.current);
            ExecuteEvents.Execute(gameObject, baseEvent, functor);
        }

        protected virtual bool AllowExecution()
        {
            return (!m_DontExecuteWhenObscured || !IsObscured());
        }

        protected virtual bool IsObscured()
        {
            if (Camera.main == null)
            {
                return true;
            }

            if (EventSystem.current == null)
            {
                return true;
            }

            List<RaycastResult> results = new List<RaycastResult>();
            PointerEventData pointerEventData = new PointerEventData(null);
            pointerEventData.position = GetRaycastPosition();
            EventSystem.current.RaycastAll(pointerEventData, results);
            if (results.Count == 0)
            {
                return true;
            }

            RaycastResult result = results[0];

            if (!result.isValid)
            {
                return true;
            }

            if (!result.gameObject.transform.IsChildOf(gameObject.transform))
            {
                return true;
            }

            return false;
        }

        protected virtual Vector2 GetRaycastPosition()
        {
            return transform.position;
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_DontExecuteWhenObscured))
            {
                return int.MaxValue;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}