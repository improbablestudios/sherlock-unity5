﻿using UnityEngine;
using ImprobableStudios.BaseUtilities;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Helper/Disable Input While Active")]
    public class DisableInputWhileActive : PersistentBehaviour
    {
        [Tooltip("If true, disable mouse input")]
        [SerializeField]
        protected bool m_DisableMouseInput = true;

        [Tooltip("If true, disable keyboard input")]
        [SerializeField]
        protected bool m_DisableKeyboardInput = true;

        protected bool m_disabledMouseInput;

        protected bool m_disabledKeyboardInput;

        public bool DisableMouseInput
        {
            get
            {
                return m_DisableMouseInput;
            }
            set
            {
                m_DisableMouseInput = value;
            }
        }

        public bool DisableKeyboardInput
        {
            get
            {
                return m_DisableKeyboardInput;
            }
            set
            {
                m_DisableKeyboardInput = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            DisableInputIfNecessary();
        }

        protected virtual void Update()
        {
            DisableInputIfNecessary();
        }

        protected virtual void DisableInputIfNecessary()
        {
            if (m_DisableMouseInput && ControllableInputModule.Instance != null && ControllableInputModule.Instance.ProcessMouseEvents == true)
            {
                ControllableInputModule.Instance.ProcessMouseEvents = false;
                m_disabledMouseInput = true;
            }
            if (m_DisableKeyboardInput && ControllableInputModule.Instance != null && ControllableInputModule.Instance.ProcessKeyboardEvents == true)
            {
                ControllableInputModule.Instance.ProcessKeyboardEvents = false;
                m_disabledKeyboardInput = true;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_disabledMouseInput)
            {
                ControllableInputModule.Instance.ProcessMouseEvents = true;
            }
            if (m_disabledKeyboardInput)
            {
                ControllableInputModule.Instance.ProcessKeyboardEvents = true;
            }
        }
    }

}