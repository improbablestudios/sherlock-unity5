﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Controllable Input Module")]
    public class ControllableInputModule : StandaloneInputModule
    {
        public static ControllableInputModule Instance
        {
            get
            {
                ControllableInputModule inputModule = null;
                if (EventSystem.current != null)
                {
                    inputModule = EventSystem.current.currentInputModule as ControllableInputModule;
                }

                return inputModule;
            }
        }

        [SerializeField]
        protected bool m_ProcessKeyboardEvents = true;

        [SerializeField]
        protected bool m_ProcessMouseEvents = true;

        [SerializeField]
        protected float m_MoveDeadZone = 0.6f;

        protected GameObject m_previousSelectedGameObject;

        protected GameObject m_currentSelectedGameObject;

        protected float m_prevActionTime;
        protected Vector2 m_lastMoveVector;
        protected int m_consecutiveMoveCount = 0;

        public class MoveEvent : UnityEvent<AxisEventData> {}

        [SerializeField]
        protected MoveEvent m_OnBeforeMove = new MoveEvent();

        [SerializeField]
        protected MoveEvent m_OnAfterMove = new MoveEvent();

        public bool ProcessKeyboardEvents
        {
            get
            {
                return m_ProcessKeyboardEvents;
            }
            set
            {
                m_ProcessKeyboardEvents = value;
            }
        }

        public bool ProcessMouseEvents
        {
            get
            {
                return m_ProcessMouseEvents;
            }
            set
            {
                m_ProcessMouseEvents = value;
            }
        }

        public MoveEvent OnBeforeMove
        {
            get
            {
                return m_OnBeforeMove;
            }
        }

        public MoveEvent OnAfterMove
        {
            get
            {
                return m_OnAfterMove;
            }
        }

        public GameObject PreviousSelectedGameObject
        {
            get
            {
                return m_previousSelectedGameObject;
            }
        }

        public override void Process()
        {
            if (!eventSystem.isFocused && ShouldIgnoreEventsOnNoFocus())
                return;
            
            bool usedEvent = SendUpdateEventToSelectedObject();

            if (m_ProcessKeyboardEvents)
            {
                if (eventSystem.sendNavigationEvents)
                {

                    if (!usedEvent)
                    {
                        usedEvent |= InvokeOnMoveTriggerAndSendMoveEventToSelectedObject();
                    }
                    if (!usedEvent)
                    {
                        SendSubmitEventToSelectedObject();
                    }
                }
            }

            if (m_ProcessMouseEvents)
            {
                if (!ProcessTouchEvents() && input.mousePresent)
                {
                    ProcessMouseEvent();
                }
            }

            if (m_currentSelectedGameObject != eventSystem.currentSelectedGameObject)
            {
                m_previousSelectedGameObject = m_currentSelectedGameObject;
                m_currentSelectedGameObject = eventSystem.currentSelectedGameObject;
            }
        }
        
        protected virtual bool ProcessTouchEvents()
        {
            for (int i = 0; i < input.touchCount; i++)
            {
                Touch touch = input.GetTouch(i);
                if (touch.type != TouchType.Indirect)
                {
                    bool pressed;
                    bool flag;
                    PointerEventData touchPointerEventData = GetTouchPointerEventData(touch, out pressed, out flag);
                    ProcessTouchPress(touchPointerEventData, pressed, flag);
                    if (!flag)
                    {
                        ProcessMove(touchPointerEventData);
                        ProcessDrag(touchPointerEventData);
                    }
                    else
                    {
                        RemovePointerData(touchPointerEventData);
                    }
                }
            }
            return input.touchCount > 0;
        }

        protected bool InvokeOnMoveTriggerAndSendMoveEventToSelectedObject()
        {
            float time = Time.unscaledTime;

            Vector2 movement = GetRawMoveVector();
            if (Mathf.Approximately(movement.x, 0f) && Mathf.Approximately(movement.y, 0f))
            {
                m_consecutiveMoveCount = 0;
                return false;
            }

            // If user pressed key again, always allow event
            bool allow = input.GetButtonDown(horizontalAxis) || input.GetButtonDown(verticalAxis);
            bool similarDir = (Vector2.Dot(movement, m_lastMoveVector) > 0);
            if (!allow)
            {
                // Otherwise, user held down key or axis.
                // If direction didn't change at least 90 degrees, wait for delay before allowing consequtive event.
                if (similarDir && m_consecutiveMoveCount == 1)
                    allow = (time > m_prevActionTime + repeatDelay);
                // If direction changed at least 90 degree, or we already had the delay, repeat at repeat rate.
                else
                    allow = (time > m_prevActionTime + 1f / inputActionsPerSecond);
            }
            if (!allow)
                return false;

            // Debug.Log(m_ProcessingEvent.rawType + " axis:" + m_AllowAxisEvents + " value:" + "(" + x + "," + y + ")");
            AxisEventData axisEventData = GetAxisEventData(movement.x, movement.y, m_MoveDeadZone);

            if (axisEventData.moveDir != MoveDirection.None)
            {
                m_OnBeforeMove.Invoke(axisEventData);
                ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);
                if (!similarDir)
                    m_consecutiveMoveCount = 0;
                m_consecutiveMoveCount++;
                m_prevActionTime = time;
                m_lastMoveVector = movement;
                m_OnAfterMove.Invoke(axisEventData);
            }
            else
            {
                m_consecutiveMoveCount = 0;
            }

            return axisEventData.used;
        }

        public Vector2 GetRawMoveVector()
        {
            Vector2 move = Vector2.zero;
            move.x = input.GetAxisRaw(horizontalAxis);
            move.y = input.GetAxisRaw(verticalAxis);

            if (input.GetButtonDown(horizontalAxis))
            {
                if (move.x < 0)
                    move.x = -1f;
                if (move.x > 0)
                    move.x = 1f;
            }
            if (input.GetButtonDown(verticalAxis))
            {
                if (move.y < 0)
                    move.y = -1f;
                if (move.y > 0)
                    move.y = 1f;
            }
            return move;
        }

        protected bool ShouldIgnoreEventsOnNoFocus()
        {
            switch (SystemInfo.operatingSystemFamily)
            {
                case OperatingSystemFamily.Windows:
                case OperatingSystemFamily.Linux:
                case OperatingSystemFamily.MacOSX:
#if UNITY_EDITOR
                    if (UnityEditor.EditorApplication.isRemoteConnected)
                        return false;
#endif
                    return true;
                default:
                    return false;
            }
        }
    }

}