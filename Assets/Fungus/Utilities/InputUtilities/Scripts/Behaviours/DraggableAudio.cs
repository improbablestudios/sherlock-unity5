﻿using UnityEngine;
using UnityEngine.Audio;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(Draggable))]
    [AddComponentMenu("Input/Helper/Draggable Audio")]
    public class DraggableAudio : PersistentBehaviour
    {
        [Tooltip("The sound that plays when this game object starts moving")]
        [SerializeField]
        protected AudioClip m_MoveBeginSound;

        [Tooltip("The sound that is played looping while this game object is being moved")]
        [SerializeField]
        protected AudioClip m_MoveLoopSound;

        [Tooltip("The sound that plays when this game object stops moving")]
        [SerializeField]
        protected AudioClip m_MoveEndSound;

        [Tooltip("The sound that plays when this game object starts rotating")]
        [SerializeField]
        protected AudioClip m_RotateBeginSound;

        [Tooltip("The sound that is played looping while this game object is being rotated")]
        [SerializeField]
        protected AudioClip m_RotateLoopSound;

        [Tooltip("The sound that plays when this game object stops rotating")]
        [SerializeField]
        protected AudioClip m_RotateEndSound;
        
        [Tooltip("The audio mixer group that the sounds output to")]
        [SerializeField]
        protected AudioMixerGroup m_SoundOutput;

        private Draggable m_draggable;

        private AudioSource m_moveBeginAudioSource;

        private AudioSource m_moveLoopAudioSource;

        private AudioSource m_moveEndAudioSource;

        private AudioSource m_rotateBeginAudioSource;

        private AudioSource m_rotateLoopAudioSource;

        private AudioSource m_rotateEndAudioSource;

        public AudioClip MoveBeginSound
        {
            get
            {
                return m_MoveBeginSound;
            }
            set
            {
                m_MoveBeginSound = value;
            }
        }

        public AudioClip MoveLoopSound
        {
            get
            {
                return m_MoveLoopSound;
            }
            set
            {
                m_MoveLoopSound = value;
            }
        }

        public AudioClip MoveEndSound
        {
            get
            {
                return m_MoveEndSound;
            }
            set
            {
                m_MoveEndSound = value;
            }
        }

        public AudioClip RotateBeginSound
        {
            get
            {
                return m_RotateBeginSound;
            }
            set
            {
                m_RotateBeginSound = value;
            }
        }

        public AudioClip RotateLoopSound
        {
            get
            {
                return m_RotateLoopSound;
            }
            set
            {
                m_RotateLoopSound = value;
            }
        }

        public AudioClip RotateEndSound
        {
            get
            {
                return m_RotateEndSound;
            }
            set
            {
                m_RotateEndSound = value;
            }
        }

        public AudioMixerGroup SoundOutput
        {
            get
            {
                return m_SoundOutput;
            }
            set
            {
                m_SoundOutput = value;
            }
        }

        public Draggable Draggable
        {
            get
            {
                if (m_draggable == null)
                {
                    m_draggable = gameObject.GetRequiredComponent<Draggable>();
                }
                return m_draggable;
            }
        }

        protected AudioSource MoveBeginAudioSource
        {
            get
            {
                if (m_moveBeginAudioSource == null)
                {
                    m_moveBeginAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("MoveBeginAudioSource");
                    m_moveBeginAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_moveBeginAudioSource.playOnAwake = false;
                }
                return m_moveBeginAudioSource;
            }
        }

        protected AudioSource MoveLoopAudioSource
        {
            get
            {
                if (m_moveLoopAudioSource == null)
                {
                    m_moveLoopAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("MoveLoopAudioSource");
                    m_moveLoopAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_moveLoopAudioSource.playOnAwake = false;
                    m_moveLoopAudioSource.loop = true;
                }
                return m_moveLoopAudioSource;
            }
        }

        protected AudioSource MoveEndAudioSource
        {
            get
            {
                if (m_moveEndAudioSource == null)
                {
                    m_moveEndAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("MoveEndAudioSource");
                    m_moveEndAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_moveEndAudioSource.playOnAwake = false;
                }
                return m_moveEndAudioSource;
            }
        }

        protected AudioSource RotateBeginAudioSource
        {
            get
            {
                if (m_rotateBeginAudioSource == null)
                {
                    m_rotateBeginAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("RotateBeginAudioSource");
                    m_rotateBeginAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_rotateBeginAudioSource.playOnAwake = false;
                }
                return m_rotateBeginAudioSource;
            }
        }

        protected AudioSource RotateLoopAudioSource
        {
            get
            {
                if (m_rotateLoopAudioSource == null)
                {
                    m_rotateLoopAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("RotateLoopAudioSource");
                    m_rotateLoopAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_rotateLoopAudioSource.playOnAwake = false;
                    m_rotateLoopAudioSource.loop = true;
                }
                return m_rotateLoopAudioSource;
            }
        }

        protected AudioSource RotateEndAudioSource
        {
            get
            {
                if (m_rotateEndAudioSource == null)
                {
                    m_rotateEndAudioSource = gameObject.GetRequiredChildComponent<AudioSource>("RotateEndAudioSource");
                    m_rotateEndAudioSource.outputAudioMixerGroup = m_SoundOutput;
                    m_rotateEndAudioSource.playOnAwake = false;
                }
                return m_rotateEndAudioSource;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Draggable.OnBeginMove.AddListener(PlayMoveBeginSound);
            Draggable.OnMove.AddListener(PlayMoveLoopSound);
            Draggable.OnEndMove.AddListener(PlayMoveEndSound);
            Draggable.OnBeginRotate.AddListener(PlayRotateBeginSound);
            Draggable.OnRotate.AddListener(PlayRotateLoopSound);
            Draggable.OnEndRotate.AddListener(PlayRotateEndSound);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Draggable.OnBeginMove.RemoveListener(PlayMoveBeginSound);
            Draggable.OnMove.RemoveListener(PlayMoveLoopSound);
            Draggable.OnEndMove.RemoveListener(PlayMoveEndSound);
            Draggable.OnBeginRotate.RemoveListener(PlayRotateBeginSound);
            Draggable.OnRotate.RemoveListener(PlayRotateLoopSound);
            Draggable.OnEndRotate.RemoveListener(PlayRotateEndSound);
        }

        private void PlayMoveBeginSound()
        {
            PlaySound(MoveBeginAudioSource, MoveBeginSound);
        }

        private void PlayMoveLoopSound()
        {
            PlaySound(MoveLoopAudioSource, MoveLoopSound);
        }

        private void PlayMoveEndSound()
        {
            MoveLoopAudioSource.Stop();
            PlaySound(MoveEndAudioSource, MoveEndSound);
        }

        private void PlayRotateBeginSound()
        {
            PlaySound(RotateBeginAudioSource, RotateBeginSound);
        }

        private void PlayRotateLoopSound()
        {
            PlaySound(RotateLoopAudioSource, RotateLoopSound);
        }

        private void PlayRotateEndSound()
        {
            RotateLoopAudioSource.Stop();
            PlaySound(RotateEndAudioSource, RotateEndSound);
        }

        private void PlaySound(AudioSource audioSource, AudioClip audioClip)
        {
            if (audioSource.isActiveAndEnabled)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
            }
        }
    }

}
