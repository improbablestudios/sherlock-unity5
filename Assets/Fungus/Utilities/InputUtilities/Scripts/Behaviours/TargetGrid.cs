﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.UnityObjectUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.InputUtilities
{

    [AddComponentMenu("Input/Target/Target Grid")]
    public class TargetGrid : Target
    {
        [Tooltip("Number of rows in this grid")]
        [Min(1)]
        [SerializeField]
        [FormerlySerializedAs("numberOfRows")]
        protected int m_NumberOfRows = 5;

        [Tooltip("Number of columns in this grid")]
        [Min(1)]
        [SerializeField]
        [FormerlySerializedAs("numberOfColumns")]
        protected int m_NumberOfColumns = 5;

        [Tooltip("The local offset of the grid")]
        [SerializeField]
        protected Vector2 m_Offset;

        [Tooltip("The local size of the grid")]
        [SerializeField]
        protected Vector2 m_Size = Vector2.one;
        
        public int NumberOfRows
        {
            get
            {
                return m_NumberOfRows;
            }
            set
            {
                m_NumberOfRows = value;
            }
        }
        
        public int NumberOfColumns
        {
            get
            {
                return m_NumberOfColumns;
            }
            set
            {
                m_NumberOfColumns = value;
            }
        }

        public Vector2 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
            }
        }
        
        public Vector2 Size
        {
            get
            {
                return m_Size;
            }
            set
            {
                m_Size = value;
            }
        }
        
        public override bool Contains(Vector2 point)
        {
            Vector3 gridOrigin = GetGridWorldOrigin();
            float tileWidth = GetTileWorldScale().x;
            float tileHeight = GetTileWorldScale().y;

            if (point.x >= gridOrigin.x &&
                point.x <= gridOrigin.x + (m_NumberOfColumns * tileWidth) &&
                point.y >= gridOrigin.y &&
                point.y <= gridOrigin.y + (m_NumberOfRows * tileHeight))
            {
                return true;
            }
            return false;
        }

        public override Vector2 GetSnappedPosition(Vector2 position)
        {
            return GetTilePosition(GetTileCoordinate(position));
        }

        public bool TileContains(Vector2 tileCoordinate, Vector2 point)
        {
            Vector3 gridOrigin = GetGridWorldOrigin();
            float tileWidth = GetTileWorldScale().x;
            float tileHeight = GetTileWorldScale().y;

            if (point.x >= gridOrigin.x + (tileCoordinate.x * tileWidth) &&
                point.x <= gridOrigin.x + ((tileCoordinate.x + 1) * tileWidth) &&
                point.y >= gridOrigin.y + (tileCoordinate.y * tileHeight) &&
                point.y <= gridOrigin.y + ((tileCoordinate.y + 1) * tileHeight))
            {
                return true;
            }
            return false;
        }

        public Vector2 GetTileCoordinate(Vector2 point)
        {
            Vector2 tileCoordinate;

            Vector3 gridOrigin = GetGridWorldOrigin();
            float tileWidth = GetTileWorldScale().x;
            float tileHeight = GetTileWorldScale().y;

            float distanceFromYAxis = point.x - gridOrigin.x;
            float distanceFromXAxis = point.y - gridOrigin.y;

            tileCoordinate.x = (int)(distanceFromYAxis / tileWidth);
            tileCoordinate.y = (int)(distanceFromXAxis / tileHeight);

            return tileCoordinate;
        }

        public Vector2 GetTilePosition(Vector2 tileCoordinate)
        {
            Vector3 gridOrigin = GetGridWorldOrigin();
            float tileWidth = GetTileWorldScale().x;
            float tileHeight = GetTileWorldScale().y;

            Vector2 alignmentVector = GetPivotValue();

            Vector2 snapPosition;

            snapPosition.x = gridOrigin.x + (tileCoordinate.x * tileWidth) + (alignmentVector.x * tileWidth);
            snapPosition.y = gridOrigin.y + (tileCoordinate.y * tileHeight) + (alignmentVector.y * tileHeight);

            return snapPosition;
        }

        public Vector3 GetGridWorldOrigin()
        {
            return transform.TransformPoint(m_Offset);
        }

        public Vector2 GetTileWorldScale()
        {
            return new Vector2(transform.lossyScale.x * m_Size.x, transform.lossyScale.y * m_Size.y);
        }

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            if (!this.IsParentOrSelfSelected())
            {
                return;
            }

            int hotControl = GUIUtility.hotControl;
            Color color = Handles.color;

            Handles.color = Physics2D.colliderAwakeColor;

            // Grid position and scale
            Vector3 position = transform.TransformPoint(m_Offset);
            Vector2 scale = new Vector2(transform.lossyScale.x * m_Size.x, transform.lossyScale.y * m_Size.y);

            float gridX = position.x;
            float gridY = position.y;
            float gridZ = position.z;
            float tileWidth = scale.x;
            float tileHeight = scale.y;
            float gridWidth = m_NumberOfColumns * tileWidth;
            float gridHeight = m_NumberOfRows * tileHeight;

            // Grid Hover Label Style
            GUIStyle tileLabelStyle = new GUIStyle(EditorStyles.label);
            tileLabelStyle.fontStyle = FontStyle.Bold;
            tileLabelStyle.fontSize = 12;
            tileLabelStyle.alignment = TextAnchor.MiddleCenter;
            GUIStyle tooltipStyle = new GUIStyle(tileLabelStyle);
            tooltipStyle.normal.background = Texture2D.whiteTexture;
            tooltipStyle.fontSize = 10;
            float guiTileWidth = Math.Abs(HandleUtility.WorldToGUIPoint(new Vector3(1, 0, 0)).x - HandleUtility.WorldToGUIPoint(new Vector3(0, 0, 0)).x);
            float guiTileHeight = Math.Abs(HandleUtility.WorldToGUIPoint(new Vector3(0, 1, 0)).y - HandleUtility.WorldToGUIPoint(new Vector3(0, 0, 0)).y);
            tileLabelStyle.fixedWidth = tileWidth * guiTileWidth;
            tileLabelStyle.fixedHeight = tileHeight * guiTileHeight;
            tooltipStyle.fixedWidth = tileWidth * guiTileWidth;
            tooltipStyle.fixedHeight = tileHeight * guiTileHeight;

            // Draw Grid
            int xCoordinate = 0;
            for (float x = 0; x < gridWidth; x += tileWidth)
            {
                int yCoordinate = 0;
                for (float y = 0; y < gridHeight; y += tileHeight)
                {
                    string xLabel = xCoordinate.ToString();
                    string yLabel = yCoordinate.ToString();
                    string tooltip = xLabel + "," + yLabel;

                    float tileXCenter = gridX + x + tileWidth / 2;
                    float tileYCenter = gridY + y + tileHeight / 2;

                    if (xCoordinate == 0)
                    {
                        // Draw vertical line
                        Handles.DrawLine(new Vector3(gridX + gridWidth, gridY + y, gridZ),
                                         new Vector3(gridX, gridY + y, gridZ));

                        // Draw left label
                        Vector3 leftTileCenter = new Vector3(tileXCenter - tileWidth, tileYCenter, gridZ);
                        Handles.Label(leftTileCenter, yLabel, tileLabelStyle);

                        // Draw right label
                        Vector3 rightTileCenter = new Vector3(tileXCenter + gridWidth, tileYCenter, gridZ);
                        Handles.Label(rightTileCenter, new GUIContent(yLabel), tileLabelStyle);
                    }
                    if (yCoordinate == 0)
                    {
                        // Draw horizontal line
                        Handles.DrawLine(new Vector3(gridX + x, gridY, gridZ),
                                         new Vector3(gridX + x, gridY + gridHeight, gridZ));

                        // Draw top label
                        Vector3 topTileCenter = new Vector3(tileXCenter, tileYCenter + gridHeight, gridZ);
                        Handles.Label(topTileCenter, new GUIContent(xLabel), tileLabelStyle);

                        // Draw bottom label
                        Vector3 bottomTileCenter = new Vector3(tileXCenter, tileYCenter - tileHeight, gridZ);
                        Handles.Label(bottomTileCenter, new GUIContent(xLabel), tileLabelStyle);
                    }

                    // Draw hover label
                    Rect tile = new Rect(gridX + x, gridY + y, tileWidth, tileHeight);
                    if (tile.Contains(HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin))
                    {
                        Vector3 leftTileCenter = new Vector3(tileXCenter, tileYCenter, gridZ);
                        Handles.Label(leftTileCenter, new GUIContent(tooltip), tooltipStyle);
                    }
                    yCoordinate++;
                }
                xCoordinate++;
            }

            Handles.color = color;

            // Repaint the grid every frame so that it responds instantly to changes
            HandleUtility.Repaint();
        }
#endif
    }
}