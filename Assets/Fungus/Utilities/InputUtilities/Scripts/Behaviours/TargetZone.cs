﻿using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(Collider2D))]
    [AddComponentMenu("Input/Target/Target Zone")]
    public class TargetZone : Target
    {
        private Collider2D m_collider2D;
        
        public Collider2D Collider2D
        {
            get
            {
                if (m_collider2D == null)
                {
                    m_collider2D = this.GetRequiredComponent<Collider2D>();
                }
                return m_collider2D;
            }
        }

        protected override void Reset()
        {
            base.Reset();

            Setup();
        }
        
        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            Setup();
        }

        protected virtual void Setup()
        {
            Collider2D.isTrigger = true;
        }

        public override bool Contains(Vector2 point)
        {
            return Collider2D.OverlapPoint(point);
        }

        public override Vector2 GetSnappedPosition(Vector2 position)
        {
            Vector2 alignmentVector = GetPivotValue();

            Vector2 boundsMin = Collider2D.bounds.min;
            Vector2 boundsSize = Collider2D.bounds.size;

            Vector2 snapPosition;

            snapPosition.x = boundsMin.x + (alignmentVector.x * boundsSize.x);
            snapPosition.y = boundsMin.y + (alignmentVector.y * boundsSize.y);
            
            return snapPosition;
        }
    }

}
