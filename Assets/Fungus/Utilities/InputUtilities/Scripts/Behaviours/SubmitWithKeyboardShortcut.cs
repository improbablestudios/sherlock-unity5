﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.InputUtilities
{

    [RequireComponent(typeof(Selectable))]
    [AddComponentMenu("Input/Helper/Submit With Keyboard Shortcut")]
    public class SubmitWithKeyboardShortcut : SubmitEventGenerator
    {
        [Tooltip("The keyboard shortcut that will fire a submit event for the attached button")]
        [SerializeField]
        [FormerlySerializedAs("keyboardShortcut")]
        [CheckNotNull]
        protected KeyboardShortcut m_KeyboardShortcut;
        
        public KeyboardShortcut KeyboardShortcut
        {
            get
            {
                return m_KeyboardShortcut;
            }
            set
            {
                m_KeyboardShortcut = value;
            }
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            m_KeyboardShortcut.OnPressed.AddListener(SubmitIfAllowed);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_KeyboardShortcut.OnPressed.RemoveListener(SubmitIfAllowed);
        }
    }

}