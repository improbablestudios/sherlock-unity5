﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ImprobableStudios.InputUtilities
{

    public abstract class SubmitEventGenerator : InputEventGenerator, ISubmitGenerator
    {
        [SerializeField]
        protected bool m_Select = true;

        protected void SubmitIfAllowed()
        {
            if (AllowExecution())
            {
                if (m_Select)
                {
                    EventSystem.current.SetSelectedGameObject(gameObject);
                }

                ExecuteEvent(ExecuteEvents.submitHandler);
            }
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_Select))
            {
                return int.MaxValue;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}