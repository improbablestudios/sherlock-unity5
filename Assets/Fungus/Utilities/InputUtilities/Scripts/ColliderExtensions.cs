﻿using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.InputUtilities
{

    public static class ColliderExtensions
    {
        /// <summary>
        /// Computes the center of a collider2D by averaging all its points.
        /// (Warning: Computation intensive for polygonCollider2D and edgeCollider2D. Cache the result when possible.)
        /// </summary>
        /// <param name="collider2D"></param>
        /// <returns></returns>
        public static Vector2 GetCenter(this Collider2D collider2D)
        {
            PolygonCollider2D polygonCollider2D = collider2D as PolygonCollider2D;
            if (polygonCollider2D != null && polygonCollider2D.points.Length > 0)
            {
                Vector2 points = Vector3.zero;
                foreach (Vector2 point in polygonCollider2D.points)
                {
                    points += (Vector2)polygonCollider2D.transform.TransformPoint(point);
                }
                return (points / polygonCollider2D.points.Length);
            }

            EdgeCollider2D edgeCollider2D = collider2D as EdgeCollider2D;
            if (edgeCollider2D != null && edgeCollider2D.points.Length > 0)
            {
                Vector2 points = Vector3.zero;
                foreach (Vector2 point in edgeCollider2D.points)
                {
                    points += (Vector2)edgeCollider2D.transform.TransformPoint(point);
                }
                return (points / edgeCollider2D.points.Length);
            }

            if (collider2D != null)
            {
                return collider2D.bounds.center;
            }

            return collider2D.transform.position;
        }
    }

}