﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace ImprobableStudios.BaseWindowUtilities
{

    public class BaseWindow : EditorWindow, IHasCustomMenu
    {
        private static List<IEnumerator> s_editorCoroutines = new List<IEnumerator>();

        private GUIStyle m_lockButtonStyle;

        protected bool m_locked;

        protected InspectorMode m_inspectorMode;

        protected ActiveEditorTracker m_Tracker;

        private bool m_resetKeyboardControl;

        public bool Locked
        {
            get
            {
                return m_locked;
            }
            set
            {
                m_locked = value;
            }
        }

        public InspectorMode InspectorMode
        {
            get
            {
                return m_inspectorMode;
            }
            set
            {
                m_inspectorMode = value;
            }
        }

        protected virtual void OnGUI()
        {
            HandleResetKeyboardControl();
        }

        private static void Update()
        {
            foreach (IEnumerator editorCoroutine in s_editorCoroutines.ToArray())
            {
                if (!editorCoroutine.MoveNext())
                {
                    StopCoroutine(editorCoroutine);
                }
            }

            if (s_editorCoroutines.Count == 0)
            {
                EditorApplication.update -= Update;
            }
        }

        private static void Start()
        {
            EditorApplication.update -= Update;
            EditorApplication.update += Update;
        }

        public static void StartCoroutine(IEnumerator coroutine)
        {
            s_editorCoroutines.Add(coroutine);
            Start();
        }

        public static void StopCoroutine(IEnumerator coroutine)
        {
            if (s_editorCoroutines.Contains(coroutine))
            {
                s_editorCoroutines.Remove(coroutine);
            }
        }

        public static bool CoroutineIsRunning(IEnumerator coroutine)
        {
            return (s_editorCoroutines.Contains(coroutine));
        }

        private static IEnumerator Invoke(Action action, float delay)
        {
            for (int i = 0; i < delay; i++)
            {
                yield return null; //wait one frame
            }

            if (action != null)
            {
                action();
            }
        }

        public static void DelayedCall(Action action, float delay)
        {
            StartCoroutine(Invoke(action, delay));
        }

        void ShowButton(Rect position)
        {
            if (m_lockButtonStyle == null)
                m_lockButtonStyle = "IN LockButton";
            m_locked = GUI.Toggle(position, m_locked, GUIContent.none, m_lockButtonStyle);
        }

        public virtual void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Normal"), m_inspectorMode == InspectorMode.Normal, SetNormal);
            menu.AddItem(new GUIContent("Debug"), m_inspectorMode == InspectorMode.Debug, SetDebug);
            if (Unsupported.IsDeveloperMode())
            {
                menu.AddItem(EditorGUIUtility.TrTextContent("Debug-Internal"), m_inspectorMode == InspectorMode.DebugInternal, SetDebugInternal);
            }
            menu.AddSeparator("");
            menu.AddItem(
                new GUIContent("Lock"), 
                m_locked,
                () => 
                {
                    m_locked = !m_locked;
                });
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Print"), false, Print);
        }

        void RefreshTitle()
        {
            if (m_inspectorMode == InspectorMode.Normal)
            {
                titleContent.text = GetWindowName();
            }
            else
            {
                titleContent.text = "Debug";
            }
        }

        void SetMode(InspectorMode mode)
        {
            m_inspectorMode = mode;
            RefreshTitle();
            ResetKeyboardControl();
        }

        protected void ResetKeyboardControl()
        {
            m_resetKeyboardControl = true;
        }

        private void HandleResetKeyboardControl()
        {
            if (m_resetKeyboardControl)
            {
                GUIUtility.keyboardControl = 0;
                m_resetKeyboardControl = false;
            }
        }

        void SetNormal()
        {
            SetMode(InspectorMode.Normal);
        }

        void SetDebug()
        {
            SetMode(InspectorMode.Debug);
        }

        void SetDebugInternal()
        {
            SetMode(InspectorMode.DebugInternal);
        }

        protected virtual void Print()
        {
            string printSavePath = EditorUtility.SaveFilePanel("Print Window", "", GetWindowName(), "png");

            if (string.IsNullOrEmpty(printSavePath))
            {
                return;
            }

            //Need to delay the actual print call so the SaveFilePanel has time to go away
            DelayedCall(() => PrintWindow(this, printSavePath), 20);
        }

        public static void PrintWindow(EditorWindow window, string savePath)
        {
            Texture2D tex = GrabScreenSwatch(window.position);
            byte[] bytes = tex.EncodeToPNG();
            File.WriteAllBytes(savePath, bytes);
        }

        public static Texture2D GrabScreenSwatch(Rect rect)
        {
            int width = (int)rect.width;
            int height = (int)rect.height;
            int x = (int)rect.x;
            int y = (int)rect.y;
            Vector2 position = new Vector2(x, y);

            Color[] pixels = UnityEditorInternal.InternalEditorUtility.ReadScreenPixel(position, width, height);

            Texture2D texture = new Texture2D(width, height);
            texture.SetPixels(pixels);
            texture.Apply();

            return texture;
        }

        public static string GetWindowName(Type windowType)
        {
            return ObjectNames.NicifyVariableName(windowType.Name.Replace("Window", ""));
        }

        public virtual string GetWindowName()
        {
            return GetWindowName(GetType());
        }

        public InspectorMode GetInspectorMode()
        {
            return m_inspectorMode;
        }

        public static T ShowWindow<T>(params Type[] desiredDockNextTo) where T : BaseWindow
        {
            return ShowWindow<T>(GetWindowName(typeof(T)), desiredDockNextTo);
        }

        public static T ShowWindow<T>(string name, params Type[] desiredDockNextTo) where T : BaseWindow
        {
            T window = GetWindow<T>(name, desiredDockNextTo);
            window.minSize = window.GetMinWindowSize();

            return window;
        }

        public static BaseWindow ShowWindow(Type type)
        {
            return ShowWindow(type, GetWindowName(type));
        }

        public static BaseWindow ShowWindow(Type type, string name)
        {
            BaseWindow window = GetWindow(type, false, name, true) as BaseWindow;
            window.minSize = window.GetMinWindowSize();

            return window;
        }

        public virtual Vector2 GetMinWindowSize()
        {
            return new Vector2(300, 200);
        }

        public virtual void SetTarget(UnityEngine.Object target)
        {
        }
        
        public static void DrawMessage(string message, MessageType messageType)
        {
            BeginDrawMessage();

            DrawMessageLabel(message, messageType);

            EndDrawMessage();
        }

        public static void DrawMessageLabel(string message, MessageType messageType)
        {
            Texture2D messageIcon = GetMessageIcon(messageType);

            GUIStyle messageStyle = GetMessageStyle();
            GUILayout.Label(new GUIContent(messageIcon), messageStyle);
            GUILayout.Label(new GUIContent(message), messageStyle);
        }

        public static bool DrawMessageButton(string label)
        {
            return DrawMessageButton(new GUIContent(label));
        }

        public static bool DrawMessageButton(GUIContent label)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(label, GUILayout.ExpandWidth(false)))
            {
                return true;
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            return false;
        }

        public static GUIStyle GetMessageStyle()
        {
            GUIStyle messageStyle = new GUIStyle(EditorStyles.label);
            messageStyle.alignment = TextAnchor.MiddleCenter;
            messageStyle.wordWrap = true;
            return messageStyle;
        }

        public static Texture2D GetMessageIcon(MessageType messageType)
        {
            Texture2D messageIcon = null;
            switch (messageType)
            {
                case MessageType.Info:
                    messageIcon = GUI.skin.GetStyle("CN EntryInfoIcon").normal.background;
                    break;
                case MessageType.Warning:
                    messageIcon = GUI.skin.GetStyle("CN EntryWarnIcon").normal.background;
                    break;
                case MessageType.Error:
                    messageIcon = GUI.skin.GetStyle("CN EntryErrorIcon").normal.background;
                    break;

            }
            return messageIcon;
        }

        public static void BeginDrawMessage()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
        }

        public static void EndDrawMessage()
        {
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
    }

}
#endif