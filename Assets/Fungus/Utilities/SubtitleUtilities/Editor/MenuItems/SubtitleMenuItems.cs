#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SubtitleUtilities
{

    public class SubtitleMenuItems
    {
        [MenuItem("Tools/Subtitles/Subtitle Displayer", false, 10)]
        [MenuItem("GameObject/Subtitles/Subtitle Displayer", false, 10)]
        static void CreateSubtitleDisplayer()
        {
            PrefabSpawner.SpawnPrefab<SubtitleDisplayer>("~SubtitleDisplayer", true);
        }
    }

}
#endif