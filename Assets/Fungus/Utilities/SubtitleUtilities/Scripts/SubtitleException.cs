﻿using System;

namespace ImprobableStudios.SubtitleUtilities
{

    /// <summary>
    /// The base exception class used for subtitles.
    /// </summary>
    public class SubtitleException : Exception
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SubtitleException() : base()
        {
        }

        /// <summary>
        /// Message constructor.
        /// </summary>
        public SubtitleException(string message) : base(message)
        {
        }

        /// <summary>
        /// Message and inner constructor.
        /// </summary>
        public SubtitleException(string message, Exception inner) : base(message, inner)
        {
        }
    }

}
