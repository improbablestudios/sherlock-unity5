﻿using System;
namespace ImprobableStudios.SubtitleUtilities
{

    /// <summary>
    /// The base exception class used for subtitle parsing.
    /// </summary>
    public class SubtitleParseException : SubtitleException
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SubtitleParseException() : base()
        {
        }

        /// <summary>
        /// Message constructor.
        /// </summary>
        public SubtitleParseException(string message) : base(message)
        {
        }

        /// <summary>
        /// Message and inner constructor.
        /// </summary>
        public SubtitleParseException(string message, Exception inner) : base(message, inner)
        {
        }
    }

}
