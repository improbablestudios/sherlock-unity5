﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.SubtitleUtilities
{

    [Serializable]
    public class SubtitleItem
    {
        [SerializeField]
        protected int m_Id = -1;

        [SerializeField]
        protected double m_StartTime;

        [SerializeField]
        protected double m_EndTime;

        [SerializeField]
        protected string m_Subtitle;

        public int Id
        {
            get
            {
                return m_Id;
            }

            set
            {
                m_Id = value;
            }
        }

        public double StartTime
        {
            get
            {
                return m_StartTime;
            }
            set
            {
                m_StartTime = value;
            }
        }

        public double EndTime
        {
            get
            {
                return m_EndTime;
            }

            set
            {
                m_EndTime = value;
            }
        }

        public string Subtitle
        {
            get
            {
                return m_Subtitle;
            }

            set
            {
                m_Subtitle = value;
            }
        }

        public SubtitleItem()
        {
        }

        public SubtitleItem(int id, double startTime, double endTime, string subtitle)
        {
            m_Id = id;
            m_StartTime = startTime;
            m_EndTime = endTime;
            m_Subtitle = subtitle;
        }

        public static SubtitleItem[] GetSubtitlesAtTime(SubtitleItem[] subtitles, double time)
        {
            if (subtitles != null)
            {
                List<SubtitleItem> currentSubtitles = new List<SubtitleItem>();
                foreach (SubtitleItem subtitleItem in subtitles)
                {
                    if (time >= subtitleItem.StartTime && time < subtitleItem.EndTime)
                    {
                        currentSubtitles.Add(subtitleItem);
                    }
                }
                return currentSubtitles.ToArray();
            }
            return null;
        }

        public override string ToString()
        {
            return SubtitleParser.ToSRT(this);
        }
    }
}
