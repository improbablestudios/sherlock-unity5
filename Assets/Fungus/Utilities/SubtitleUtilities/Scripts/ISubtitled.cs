﻿namespace ImprobableStudios.SubtitleUtilities
{

    public interface ISubtitled
    {
        double GetSubtitleTime();
    }

}
