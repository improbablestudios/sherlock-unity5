﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Playables;

namespace ImprobableStudios.SubtitleUtilities
{

    [RequireComponent(typeof(PlayableDirector))]
    [AddComponentMenu("UI/Subtitles/Playable Subtitle Timer")]
    public class PlayableSubtitleTimer : PersistentBehaviour, ISubtitled
    {
        private PlayableDirector m_playableDirector;

        public PlayableDirector PlayableDirector
        {
            get
            {
                if (m_playableDirector == null)
                {
                    m_playableDirector = GetComponent<PlayableDirector>();
                }
                return m_playableDirector;
            }
        }

        public double GetSubtitleTime()
        {
            return PlayableDirector.time;
        }
    }

}

