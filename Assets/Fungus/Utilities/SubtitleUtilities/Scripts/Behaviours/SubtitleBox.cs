﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.SubtitleUtilities
{

    [AddComponentMenu("UI/Subtitles/Subtitle Box")]
    public class SubtitleBox : PersistentBehaviour
    {
        [SerializeField]
        protected Text m_Text;

        [SerializeField]
        protected Image m_Background;

        [SerializeField]
        protected CanvasGroup m_CanvasGroup;

        public Text Text
        {
            get
            {
                return m_Text;
            }
            set
            {
                m_Text = value;
            }
        }

        public Image Background
        {
            get
            {
                return m_Background;
            }
            set
            {
                m_Background = value;
            }
        }

        public CanvasGroup CanvasGroup
        {
            get
            {
                return m_CanvasGroup;
            }
            set
            {
                m_CanvasGroup = value;
            }
        }
    }

}
