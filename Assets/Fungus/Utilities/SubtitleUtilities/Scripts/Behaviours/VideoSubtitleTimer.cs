﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Video;

namespace ImprobableStudios.SubtitleUtilities
{

    [RequireComponent(typeof(VideoPlayer))]
    [AddComponentMenu("UI/Subtitles/Video Subtitle Timer")]
    public class VideoSubtitleTimer : PersistentBehaviour, ISubtitled
    {
        private VideoPlayer m_videoPlayer;

        public VideoPlayer VideoPlayer
        {
            get
            {
                if (m_videoPlayer == null)
                {
                    m_videoPlayer = GetComponent<VideoPlayer>();
                }
                return m_videoPlayer;
            }
        }

        public double GetSubtitleTime()
        {
            return VideoPlayer.time;
        }
    }

}
  
