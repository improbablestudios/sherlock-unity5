﻿using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.SubtitleUtilities
{

    [RequireComponent(typeof(ISubtitled))]
    [AddComponentMenu("UI/Subtitles/Subtitle Displayer")]
    public class SubtitleDisplayer : PersistentBehaviour
    {
        [Tooltip("The file which contains the subtitles. (Must be in srt format and have extension .txt)")]
        [SerializeField]
        protected TextAsset m_SubtitlesAsset;
        
        [Tooltip("If greater than -1, lines longer than the max line width will be shown on a new line.")]
        [SerializeField]
        protected float m_MaxLineWidth = -1;

        [Tooltip("The boxes which will display subtitle lines. (The length of this list determines how many subtitle lines can be shown simultaneously)")]
        [SerializeField]
        protected SubtitleBox[] m_SubtitleBoxes = new SubtitleBox[0];

        private ISubtitled m_timedObject;

        private SubtitleItem[] m_subtitles;

        public TextAsset SubtitlesAsset
        {
            get
            {
                return m_SubtitlesAsset;
            }

            set
            {
                m_SubtitlesAsset = value;
                LoadSubtitles(m_SubtitlesAsset);
            }
        }

        public float MaxLineWidth
        {
            get
            {
                return m_MaxLineWidth;
            }
            set
            {
                m_MaxLineWidth = value;
            }
        }

        public SubtitleBox[] SubtitleBoxes
        {
            get
            {
                return m_SubtitleBoxes;
            }
            set
            {
                m_SubtitleBoxes = value;
            }
        }

        public ISubtitled TimedObject
        {
            get
            {
                if (m_timedObject == null)
                {
                    m_timedObject = GetComponent<ISubtitled>();
                }
                return m_timedObject;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            LoadSubtitles(m_SubtitlesAsset);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            LoadSubtitles(m_SubtitlesAsset);
            ClearDisplayedSubtitles();
        }

        protected virtual void Update()
        {
            if (TimedObject != null)
            {
                double time = TimedObject.GetSubtitleTime();
                if (m_SubtitlesAsset != null)
                {
                    SubtitleItem[] currentSubtitles = SubtitleItem.GetSubtitlesAtTime(m_subtitles, time);
                    DisplaySubtitles(currentSubtitles);
                }
            }
        }

        protected void LoadSubtitles(TextAsset subtitlesAsset)
        {
            if (subtitlesAsset != null)
            {
                m_subtitles = SubtitleParser.FromSRT(subtitlesAsset.text);
            }
        }

        public void SetSubtitleBackgroundColor(Color color)
        {
            if (m_SubtitleBoxes != null)
            {
                foreach (SubtitleBox subtitleBox in m_SubtitleBoxes)
                {
                    if (subtitleBox != null)
                    {
                        if (subtitleBox.Background != null)
                        {
                            subtitleBox.Background.color = color;
                        }
                    }
                }
            }
        }

        public void ClearDisplayedSubtitles()
        {
            DisplaySubtitles(null);
        }

        public void DisplaySubtitle(SubtitleItem subtitle, float visiblity = 1)
        {
            DisplaySubtitles(new SubtitleItem[] { subtitle }, new float[] { visiblity });
        }

        public void DisplaySubtitles(SubtitleItem[] subtitles, float[] visibilities = null)
        {
            if (m_SubtitleBoxes != null)
            {
                int boxIndex = 0;
                if (subtitles != null)
                {
                    for (int i = 0; i < subtitles.Length; i++)
                    {
                        SubtitleItem subtitleItem = subtitles[i];
                        if (subtitleItem != null && !string.IsNullOrEmpty(subtitleItem.Subtitle))
                        {
                            float visibility = 1f;
                            if (visibilities != null && i < visibilities.Length)
                            {
                                visibility = visibilities[i];
                            }

                            if (boxIndex < m_SubtitleBoxes.Length)
                            {
                                SubtitleBox subtitleBox = m_SubtitleBoxes[boxIndex];
                                if (subtitleBox != null)
                                {
                                    Text text = subtitleBox.Text;
                                    if (text != null)
                                    {
                                        string[] lines = (m_MaxLineWidth >= 0) ? GetLines(subtitleItem.Subtitle, text, m_MaxLineWidth) : new string[] { subtitleItem.Subtitle };
                                        foreach (string line in lines)
                                        {
                                            if (boxIndex >= m_SubtitleBoxes.Length)
                                            {
                                                break;
                                            }
                                            SubtitleBox currentSubtitleBox = m_SubtitleBoxes[boxIndex];
                                            Text currentText = currentSubtitleBox.Text;
                                            if (currentText != null)
                                            {
                                                currentText.text = line;
                                            }
                                            CanvasGroup currentCanvasGroup = currentSubtitleBox.CanvasGroup;
                                            if (currentCanvasGroup != null)
                                            {
                                                currentCanvasGroup.alpha = visibility;
                                            }
                                            currentSubtitleBox.gameObject.SetActive(true);
                                            boxIndex++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (int i = boxIndex; i < m_SubtitleBoxes.Length; i++)
                {
                    SubtitleBox subtitleBox = m_SubtitleBoxes[i];
                    if (subtitleBox != null)
                    {
                        subtitleBox.gameObject.SetActive(false);
                    }
                }
            }
        }

        public static string[] GetLines(string str, Text text, float maxTextWidth)
        {
            List<string> lines = new List<string>();
            TextGenerator generator = new TextGenerator();
            TextGenerationSettings settings = text.GetGenerationSettings(new Vector2(maxTextWidth, float.MaxValue));
            generator.Populate(str, settings);

            for (int i = 0; i < generator.lineCount; i++)
            {
                int currentLineStartIndex = generator.lines[i].startCharIdx;
                int nextLineStartIndex = (i + 1 < generator.lineCount) ? generator.lines[i + 1].startCharIdx : str.Length;
                int length = nextLineStartIndex - currentLineStartIndex;
                lines.Add(str.Substring(currentLineStartIndex, length));
            }

            return lines.ToArray();
        }
    }

}
