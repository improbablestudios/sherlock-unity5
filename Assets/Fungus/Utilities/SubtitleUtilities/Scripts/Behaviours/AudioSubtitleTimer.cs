﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;

namespace ImprobableStudios.SubtitleUtilities
{

    [RequireComponent(typeof(AudioSource))]
    [AddComponentMenu("UI/Subtitles/Audio Subtitle Timer")]
    public class AudioSubtitleTimer : PersistentBehaviour, ISubtitled
    {
        private AudioSource m_audioSource;

        public AudioSource AudioSource
        {
            get
            {
                if (m_audioSource == null)
                {
                    m_audioSource = GetComponent<AudioSource>();
                }
                return m_audioSource;
            }
        }

        public double GetSubtitleTime()
        {
            return AudioSource.time;
        }
    }

}

