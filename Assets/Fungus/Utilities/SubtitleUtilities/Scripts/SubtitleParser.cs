﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImprobableStudios.SubtitleUtilities
{

    [Serializable]
    public static class SubtitleParser
    {
        public static SubtitleItem[] FromSRT(string srtData)
        {
            List<SubtitleItem> subtitles = new List<SubtitleItem>();

            string[] srtItems = srtData.Split(new string[] { Environment.NewLine + Environment.NewLine }, StringSplitOptions.None);

            foreach (string srtItem in srtItems)
            {
                string[] srtLines = srtItem.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int minLineCount = 3;
                if (srtLines.Length >= minLineCount)
                {
                    string sequenceNumberLine = srtLines[0].Trim();
                    int sequenceNumber = ParseSequenceNumber(sequenceNumberLine);

                    string timeCodeLine = srtLines[1].Trim();
                    string[] timeStrings = timeCodeLine.Split(new string[] { " --> " }, StringSplitOptions.None);
                    double startTime = 0;
                    double endTime = 0;
                    if (timeStrings.Length > 1)
                    {
                        startTime = ParseTime(timeStrings[0]);
                        endTime = ParseTime(timeStrings[1]);

                    }
                    else
                    {
                        throw new SubtitleParseException(string.Format("Start time and end time could not be parsed from line: {0}", timeCodeLine));
                    }

                    string subtitle = "";
                    for (int i = 2; i < srtLines.Length; i++)
                    {
                        subtitle += srtLines[i];
                    }
                    subtitle = subtitle.Trim();

                    subtitles.Add(new SubtitleItem(sequenceNumber, startTime, endTime, subtitle));
                }
                else
                {
                    throw new SubtitleParseException(string.Format("SRT Item must have at least {0} lines: {1}", minLineCount, srtItem));
                }
            }

            return subtitles.ToArray();
        }

        private static int ParseSequenceNumber(string sequenceNumber)
        {
            if (int.TryParse(sequenceNumber, out int result))
            {
                return result;
            }
            else
            {
                throw new SubtitleParseException(string.Format("Could not parse sequence number from line: {0}", sequenceNumber));
            }
        }

        private static double ParseTime(string time)
        {
            if (TimeSpan.TryParse(time, out TimeSpan result))
            {
                return result.TotalSeconds;
            }
            else
            {
                throw new SubtitleParseException(string.Format("Could not parse start time from line: {0}", time));
            }
        }

        public static string ToSRT(SubtitleItem subtitle)
        {
            return ToSRT(new SubtitleItem[] { subtitle });
        }

        public static string ToSRT(SubtitleItem[] subtitles)
        {
            StringBuilder s = new StringBuilder();

            if (subtitles != null && subtitles.Length > 0)
            {
                for (int i = 0; i < subtitles.Length; i++)
                {
                    SubtitleItem subtitle = subtitles[i];
                    s.AppendLine(subtitle.Id.ToString());
                    s.Append(FormatTime(subtitle.StartTime));
                    s.Append(" --> ");
                    s.Append(FormatTime(subtitle.EndTime));
                    s.AppendLine();
                    s.AppendLine(subtitle.Subtitle);
                    if (i < subtitles.Length - 1)
                    {
                        s.AppendLine();
                    }
                }
            }

            return s.ToString();
        }

        public static string FormatTime(double time)
        {
            return TimeSpan.FromSeconds(time).ToString("G");
        }
    }
}
