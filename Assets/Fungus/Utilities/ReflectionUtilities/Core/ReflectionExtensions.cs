﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ImprobableStudios.ReflectionUtilities
{

    public delegate void MemberAction(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember);
    public delegate bool MemberCondition(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember);
    public delegate string GetMemberKey(MemberInfo member);
    public delegate MemberInfo[] GetMembers(Type objType);
    public delegate bool TypeCondition(Type type);

    public static class ReflectionExtensions
    {
        private static Type[] s_typesInLoadedAssemblies;

        static ReflectionExtensions()
        {
            s_typesInLoadedAssemblies = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => 
                {
                    try
                    {
                        return x.GetTypes();
                    }
                    catch
                    {
                    }
                    return new Type[0];
                })
                .ToArray();
        }

        public static Type[] TypesInLoadedAssemblies
        {
            get
            {
                return s_typesInLoadedAssemblies;
            }
        }

        public static string[] GetDeclaredMemberPaths<T>(this T obj)
        {
            return GetDeclaredMemberPaths(obj, typeof(T));
        }

        public static string[] GetDeclaredMemberPaths(this object obj, Type t)
        {
            return GetMemberPaths(obj, t, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
        }

        public static string[] GetMemberPaths<T>(this T obj, BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
        {
            return GetMemberPaths(obj, typeof(T));
        }

        public static string[] GetMemberPaths(this object obj, Type t, BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
        {
            return GetMemberPaths(obj, t, null);
        }

        public static string[] GetMemberPaths<T>(this T obj, Type memberType, BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
        {
            return GetMemberPaths(obj, typeof(T), memberType);
        }

        public static string[] GetMemberPaths(this object obj, Type t, Type memberType, BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
        {
            List<string> memberPaths = new List<string>();
            foreach (FieldInfo f in t.GetFields(flags))
            {
                if (!f.IsInitOnly && !Attribute.IsDefined(f, typeof(ObsoleteAttribute)))
                {
                    if (memberType == null || memberType.IsAssignableFrom(f.FieldType))
                    {
                        memberPaths.Add(f.Name);
                    }
                    else if (obj != null && typeof(IList).IsAssignableFrom(f.FieldType) && memberType.IsAssignableFrom(f.FieldType.GetItemType()))
                    {
                        IList list = f.GetValue(obj) as IList;
                        if (list != null)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                memberPaths.Add(f.Name + "[" + i + "]");
                            }
                        }
                    }
                }
            }
            foreach (PropertyInfo p in t.GetProperties(flags))
            {
                if (!Attribute.IsDefined(p, typeof(ObsoleteAttribute)))
                {
                    if (memberType == null || memberType.IsAssignableFrom(p.PropertyType))
                    {
                        memberPaths.Add(p.Name);
                    }
                    else if (obj != null && typeof(IList).IsAssignableFrom(p.PropertyType) && memberType.IsAssignableFrom(p.PropertyType.GetItemType()))
                    {
                        IList list = p.GetValue(obj) as IList;
                        if (list != null)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                memberPaths.Add(p.Name + "[" + i + "]");
                            }
                        }
                    }
                }
            }
            return memberPaths.ToArray();
        }

        public static string GetMemberPath<T>(this T obj, Expression<Func<T, object>> memberAccess)
        {
            return GetMemberPath(memberAccess);
        }

        public static string GetMemberPath<T>(this Expression<Func<T, object>> memberAccess)
        {
            return GetMemberPath<T, object>(memberAccess);
        }

        public static string GetMemberPath<T, TValue>(this Expression<Func<T, TValue>> memberAccess)
        {
            if (memberAccess == null)
            {
                return null;
            }

            List<string> namePath = new List<string>();
            WalkMemberName(namePath, memberAccess.Body);
            namePath.Reverse();
            namePath.RemoveAt(0);
            string name = string.Join(".", namePath.ToArray());
            return name;
        }

        private static string GetExpressionString(Expression expr)
        {
            switch (expr.NodeType)
            {
                case ExpressionType.Parameter:
                    return ((ParameterExpression)expr).Name;
                case ExpressionType.MemberAccess:
                    return ((MemberExpression)expr).Member.Name;
                case ExpressionType.Call:
                    return ((MethodCallExpression)expr).Method.Name;
                case ExpressionType.ArrayLength:
                    return "Length";
                default:
                    return "";
            }
        }

        private static object WalkMemberValue(Expression expr)
        {
            switch (expr.NodeType)
            {
                case ExpressionType.ArrayIndex:
                    BinaryExpression be = (BinaryExpression)expr;
                    Array arr = (Array)WalkMemberValue(be.Left);
                    int arrayIndex = (int)WalkMemberValue(be.Right);
                    return arr.GetValue(arrayIndex);
                case ExpressionType.MemberAccess:
                    MemberExpression me = (MemberExpression)expr;
                    switch (me.Member.MemberType)
                    {
                        case MemberTypes.Property:
                            return ((PropertyInfo)me.Member).GetValue(WalkMemberValue(me.Expression), null);
                        case MemberTypes.Field:
                            return ((FieldInfo)me.Member).GetValue(WalkMemberValue(me.Expression));
                        default:
                            return null;
                    }
                case ExpressionType.Constant:
                    return ((ConstantExpression)expr).Value;
                case ExpressionType.Call:
                    MethodCallExpression callExpr = (MethodCallExpression)expr;
                    ReadOnlyCollection<Expression> args = (callExpr).Arguments;
                    if (args.Count > 0)
                    {
                        int listIndex = (int)WalkMemberValue(args[0]);
                        return listIndex;
                    }
                    return null;
                case ExpressionType.Parameter:
                    return null;
                default:
                    throw new Exception("Couldn't evaluate expression: " + expr.NodeType);
            }
        }

        private static void WalkMemberName(List<string> path, Expression expr, string suffix = "")
        {
            switch (expr.NodeType)
            {
                case ExpressionType.ArrayIndex:
                    BinaryExpression aie = (BinaryExpression)expr;
                    int arrayIndex = (int)WalkMemberValue(aie.Right);
                    WalkMemberName(path, aie.Left, "[" + arrayIndex + "]");
                    return;
                case ExpressionType.MemberAccess:
                    MemberExpression mae = (MemberExpression)expr;
                    path.Add(GetExpressionString(mae) + suffix);
                    WalkMemberName(path, mae.Expression);
                    return;
                case ExpressionType.Call:
                    MethodCallExpression ce = (MethodCallExpression)expr;
                    ReadOnlyCollection<Expression> args = (ce).Arguments;
                    if (args.Count > 0)
                    {
                        int listIndex = (int)WalkMemberValue(args[0]);
                        WalkMemberName(path, ce.Object, "[" + listIndex + "]");
                    }
                    return;
                case ExpressionType.Parameter:
                    path.Add(GetExpressionString(expr) + suffix);
                    return;
                case ExpressionType.Convert:
                    if (expr is UnaryExpression)
                    {
                        MemberExpression cmae = ((UnaryExpression)expr).Operand as MemberExpression;
                        if (cmae != null)
                        {
                            WalkMemberName(path, cmae);
                        }
                    }
                    return;
                default:
                    throw new Exception("Couldn't evaluate expression: " + expr.NodeType);
            }
        }

        public static MemberInfo GetMember<T, TValue>(this Expression<Func<T, TValue>> memberAccess)
        {
            Expression expr = memberAccess.Body;
            return GetMember(expr);
        }

        private static MemberInfo GetMember(Expression expr)
        {
            switch (expr.NodeType)
            {
                case ExpressionType.MemberAccess:
                    MemberExpression mae = (MemberExpression)expr;
                    return mae.Member;
                case ExpressionType.Convert:
                    if (expr is UnaryExpression)
                    {
                        MemberExpression cmae = ((UnaryExpression)expr).Operand as MemberExpression;
                        if (cmae != null)
                        {
                            return GetMember(cmae);
                        }
                    }
                    break;
            }

            throw new Exception("Couldn't evaluate expression: " + expr);
        }

        public static MemberInfo GetMember(Type type, string fieldName, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            FieldInfo field = type.GetField(fieldName, bindings);
            if (field != null)
            {
                return field;
            }

            PropertyInfo property = type.GetProperty(fieldName, bindings);
            if (property != null)
            {
                return property;
            }

            return null;
        }

        public static Type GetMemberType(this Type type, string memberName, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            FieldInfo field = type.GetField(memberName, bindings);
            if (field != null)
            {
                return field.FieldType;
            }

            PropertyInfo property = type.GetProperty(memberName, bindings);
            if (property != null)
            {
                return property.PropertyType;
            }

            if (type.BaseType != null)
            {
                return GetMemberType(type.BaseType, memberName, bindings);
            }

            return null;
        }

        public static Type GetMemberType(this MemberInfo member)
        {
            FieldInfo field = member as FieldInfo;
            if (field != null)
            {
                return field.FieldType;
            }

            PropertyInfo property = member as PropertyInfo;
            if (property != null)
            {
                return property.PropertyType;
            }

            return null;
        }

        public static object GetMemberValue(this Type type, object obj, string memberName, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            FieldInfo field = type.GetField(memberName, bindings);
            if (field != null)
            {
                return field.GetValue(obj);
            }

            PropertyInfo property = type.GetProperty(memberName, bindings);
            if (property != null)
            {
                return property.GetValue(obj, null);
            }

            if (type.BaseType != null)
            {
                return GetMemberValue(type.BaseType, obj, memberName, bindings);
            }

            return null;
        }

        public static object GetMemberValue(this MemberInfo member, object declaringObj, object[] propertyParameters = null)
        {
            if (declaringObj != null)
            {
                FieldInfo field = member as FieldInfo;
                if (field != null)
                {
                    return field.GetValue(declaringObj);
                }

                PropertyInfo property = member as PropertyInfo;
                if (property != null)
                {
                    return property.GetValue(declaringObj, propertyParameters);
                }
            }

            return null;
        }

        public static bool IsValueNull(this object value)
        {
            return value == null || value.Equals(null);
        }

        public static bool IsValueEmpty(this object value)
        {
            if (value is string)
            {
                return ((string)value).Length == 0;
            }
            else if (value is IList)
            {
                return ((IList)value).Count == 0;
            }

            return false;
        }

        public static bool IsNumeric(this Type type)
        {
            return
                type == typeof(sbyte) ||
                type == typeof(byte) ||
                type == typeof(short) ||
                type == typeof(int) ||
                type == typeof(long) ||
                type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(decimal) ||
                type == typeof(ushort) ||
                type == typeof(uint) ||
                type == typeof(ulong);
        }

        public static bool IsMemberValueNull(this MemberInfo member, object declaringObj, object[] propertyParameters = null)
        {
            object value = GetMemberValue(member, declaringObj, propertyParameters);
            return IsValueNull(value);
        }

        public static bool IsMemberValueEmpty(this MemberInfo member, object declaringObj, object[] propertyParameters = null)
        {
            object value = GetMemberValue(member, declaringObj, propertyParameters);

            return IsValueEmpty(value);
        }

        public static void SetMemberValue(this Type type, object declaringObj, string memberName, object value, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            FieldInfo field = type.GetField(memberName, bindings);
            if (field != null)
            {
                field.SetValue(declaringObj, value);
            }

            PropertyInfo property = type.GetProperty(memberName, bindings);
            if (property != null)
            {
                property.SetValue(declaringObj, value, null);
            }

            if (type.BaseType != null)
            {
                SetMemberValue(type.BaseType, declaringObj, memberName, value, bindings);
            }
        }

        public static void SetMemberValue(this MemberInfo member, object obj, object value, object[] propertyParameters = null)
        {
            FieldInfo field = member as FieldInfo;
            PropertyInfo property = member as PropertyInfo;
            if (field != null)
            {
                field.SetValue(obj, value);
            }
            else if (property != null)
            {
                property.SetValue(obj, value, propertyParameters);
            }
        }

        public static object GetItemValue(this Type listType, object listObj, int index)
        {
            return listType.GetItemProperty().GetValue(listObj, new object[] { index });
        }

        public static void SetItemValue(this Type listType, object listObj, object value, int index)
        {
            listType.GetItemProperty().SetValue(listObj, value, new object[] { index });
        }

        public static PropertyInfo GetItemProperty(this Type listType)
        {
            PropertyInfo itemProperty = listType.GetProperty("Item");
            if (itemProperty == null)
            {
                itemProperty = typeof(IList).GetProperty("Item");
            }
            return itemProperty;
        }

        public static Type GetItemType(this Type listType)
        {
            if (typeof(IList).IsAssignableFrom(listType))
            {
                if (listType.IsArray)
                {
                    return listType.GetElementType();
                }
                Type baseGenericType = listType.GetBaseGenericType();
                if (baseGenericType != null)
                {
                    Type[] genericArguments = baseGenericType.GetGenericArguments();
                    if (genericArguments.Length > 0)
                    {
                        return genericArguments[0];
                    }
                }
            }
            return null;
        }

        public static IList Cast(this IList value, Type itemType)
        {
            Type listType = value.GetType();

            if (typeof(IList).IsAssignableFrom(listType))
            {
                if (listType.IsArray)
                {
                    IList newArray = Activator.CreateInstance(listType, new object[] { value.Count }) as IList;
                    for (int i = 0; i < value.Count; i++)
                    {
                        newArray[i] = Convert.ChangeType(value[i], itemType);
                    }
                    return newArray;
                }
                else
                {
                    Type newListType = typeof(List<>).MakeGenericType(itemType);
                    IList newList = Activator.CreateInstance(newListType) as IList;
                    for (int i = 0; i < value.Count; i++)
                    {
                        newList.Add(Convert.ChangeType(value[i], itemType));
                    }
                    return newList;
                }
            }

            return value;
        }

        public static T GetAttribute<T>(this PropertyInfo property, bool inherit) where T : Attribute
        {
            object[] attributes = property.GetCustomAttributes(inherit);
            foreach (object obj in attributes)
            {
                T attr = obj as T;
                if (attr != null)
                {
                    return attr;
                }
            }
            return null;
        }

        public static T GetAttribute<T>(this FieldInfo field, bool inherit) where T : Attribute
        {
            object[] attributes = field.GetCustomAttributes(inherit);
            foreach (object obj in attributes)
            {
                T attr = obj as T;
                if (attr != null)
                {
                    return attr;
                }
            }
            return null;
        }

        public static T GetAttribute<T>(this Type type, bool inherit) where T : Attribute
        {
            object[] attributes = type.GetCustomAttributes(inherit);
            foreach (object obj in attributes)
            {
                T attr = obj as T;
                if (attr != null)
                {
                    return attr;
                }
            }
            return null;
        }

        public static Type GetBaseGenericType(this Type type)
        {
            while (type != null && type != typeof(object))
            {
                if (type.IsGenericType)
                {
                    return type;
                }
                type = type.BaseType;
            }
            return null;
        }

        public static IEnumerable<Type> GetInheritanceHierarchy(this Type type)
        {
            for (Type current = type; current != null; current = current.BaseType)
            {
                yield return current;
            }
        }

        public static IEnumerable<Type> GetInheritanceHierarchy(this Type type, Type baseType)
        {
            for (Type current = type; current != baseType; current = current.BaseType)
            {
                yield return current;
            }
        }

        public static bool IsStruct(this Type type)
        {
            return (type.IsValueType && !type.IsEnum && !type.IsPrimitive);
        }

        public static object GetDefaultValue(this Type t)
        {
            if (t.IsValueType)
            {
                return Activator.CreateInstance(t);
            }

            return null;
        }

        public static Type[] GetSubclasses(this Type parent)
        {
            return s_typesInLoadedAssemblies
                   .Where(t => t.IsSubclassOf(parent))
                   .ToArray();
        }

        public static Type[] FindAllDerivedTypes(this Type type, TypeCondition typeCondition = null)
        {
            return s_typesInLoadedAssemblies
                .Where(t => (t == type || type.IsAssignableFrom(t)) && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllDerivedTypes(this Type type, Assembly assembly, TypeCondition typeCondition = null)
        {
            return assembly
                .GetTypes()
                .Where(t => (t == type || type.IsAssignableFrom(t)) && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllDerivedTypes(this Type[] types, TypeCondition typeCondition = null)
        {
            return s_typesInLoadedAssemblies
                .Where(t => types.Any(u => u == t || u.IsAssignableFrom(t)) && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllInstantiableDerivedTypes(this Type type, TypeCondition typeCondition = null)
        {
            return s_typesInLoadedAssemblies
                .Where(t => (t == type || type.IsAssignableFrom(t)) && t.IsInstantiable() && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllInstantiableDerivedTypes(this Type type, Assembly assembly, TypeCondition typeCondition = null)
        {
            return assembly
                .GetTypes()
                .Where(t => (t == type || type.IsAssignableFrom(t)) && t.IsInstantiable() && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllInstantiableDerivedTypes(this Type[] types, TypeCondition typeCondition = null)
        {
            return s_typesInLoadedAssemblies
                .Where(t => types.Any(u => u == t || u.IsAssignableFrom(t)) && t.IsInstantiable() && (typeCondition == null || typeCondition(t)))
                .ToArray();
        }

        public static Type[] FindAllDerivedTypesOfGenericType(this Type type)
        {
            return s_typesInLoadedAssemblies
                .Where(t => t.BaseType != null && t.BaseType.IsGenericType && t.BaseType.GetGenericTypeDefinition() == type)
                .ToArray();
        }

        public static string GetTypeFullNameWithAssembly(Type type)
        {
            if (type == null)
            {
                return null;
            }

            string typeName = type.Name;
            string namespaceName = type.Namespace;
            string assemblyName = type.Assembly.GetName().Name;
            return GetNamespaceTypeAssemblyFormat(namespaceName, typeName, assemblyName);
        }

        public static string GetNamespaceTypeAssemblyFormat(string namespaceName, string typeName, string assemblyName)
        {
            string typeFullName = typeName;
            if (!string.IsNullOrEmpty(namespaceName))
            {
                typeFullName = namespaceName + "." + typeFullName;
            }
            return typeFullName + ", " + assemblyName;
        }

        public static Type DeserializeType(this string typeString)
        {
            Type type = Type.GetType(typeString);

            if (type != null)
            {
                return type;
            }

            string typeFullName = typeString;
            int assemblyIndex = typeFullName.IndexOf(",", StringComparison.Ordinal);
            if (assemblyIndex >= 0)
            {
                typeFullName = typeFullName.Remove(assemblyIndex);
            }

            type = Type.GetType(typeFullName);

            if (type != null)
            {
                return type;
            }

            string typeName = typeFullName;
            int typeNameIndex = typeName.IndexOf(".", StringComparison.Ordinal) + 1;
            if (typeNameIndex >= 0)
            {
                typeName = typeName.Substring(typeNameIndex);
            }

            type = Type.GetType(typeName);

            if (type != null)
            {
                return type;
            }

            foreach (Type typeInLoadedAssemblies in s_typesInLoadedAssemblies)
            {
                if (typeInLoadedAssemblies.FullName == typeFullName)
                {
                    return typeInLoadedAssemblies;
                }
            }

            foreach (Type typeInLoadedAssemblies in s_typesInLoadedAssemblies)
            {
                if (typeInLoadedAssemblies.Name == typeName)
                {
                    return typeInLoadedAssemblies;
                }
            }

            return null;
        }

        public static string SerializeType(this Type type)
        {
            if (type == null)
            {
                return null;
            }

            string typeName = GetTypeFullNameWithAssembly(type);

            if (Type.GetType(typeName) != null)
            {
                return typeName;
            }

            return type.AssemblyQualifiedName;
        }

        public static string GetDisplayName(string name)
        {
            name = GetAutoPropertyName(name);
            name = GetNameWithoutMemberPrefixes(name);
            name = name.Replace("_", " ");
            name = SplitCamelCaseString(name);
            name = GetTitleCaseString(name);
            return name;
        }

        public static string GetNameWithoutMemberPrefixes(string fieldName)
        {
            if (fieldName.Length > 0 && fieldName[0] == 'k')
            {
                fieldName = fieldName.Substring(1);
            }
            else if (fieldName.Length > 0 && fieldName[0] == '_')
            {
                fieldName = fieldName.Substring(1);
            }
            else if (fieldName.Length > 1 && fieldName[1] == '_')
            {
                fieldName = fieldName.Substring(2);
            }
            return fieldName;
        }

        public static string GetAutoPropertyName(string fieldName)
        {
            Match match = Regex.Match(fieldName, $"<(.+?)>k__BackingField");
            return match.Success ? match.Groups[1].Value : fieldName;
        }

        public static string GetTitleCaseString(string name)
        {
            TextInfo textInfo = CultureInfo.CurrentUICulture.TextInfo;
            return textInfo.ToTitleCase(name);
        }

        public static string SplitCamelCaseString(string name)
        {
            string[] words = Regex.Matches(name, "(^[a-z]+|[A-Z]+(?![a-z])|[A-Z][a-z]+)").OfType<Match>().Select(m => m.Value).ToArray();
            return string.Join(" ", words);
        }

        public static string GetNiceTypeName(this Type type)
        {
            if (typeof(IList).IsAssignableFrom(type))
            {
                Type itemType = type.GetItemType();
                return "List" + "<" + GetNiceTypeName(itemType) + ">";
            }
            else if (typeof(IDictionary).IsAssignableFrom(type))
            {
                Type genericType = type.GetBaseGenericType();
                Type itemKeyType = genericType.GetGenericArguments()[0];
                Type itemValueType = genericType.GetGenericArguments()[1];

                return "Dictionary" + "<" + GetNiceTypeName(itemKeyType) + "," + GetNiceTypeName(itemValueType) + ">";
            }
            else if (typeof(sbyte).IsAssignableFrom(type))
            {
                return "SignedByte";
            }
            else if (typeof(short).IsAssignableFrom(type))
            {
                return "Short";
            }
            else if (typeof(ushort).IsAssignableFrom(type))
            {
                return "UnsignedShort";
            }
            else if (typeof(int).IsAssignableFrom(type))
            {
                return "Integer";
            }
            else if (typeof(uint).IsAssignableFrom(type))
            {
                return "UnsignedInteger";
            }
            else if (typeof(long).IsAssignableFrom(type))
            {
                return "Long";
            }
            else if (typeof(ulong).IsAssignableFrom(type))
            {
                return "UnsignedLong";
            }
            else if (typeof(float).IsAssignableFrom(type))
            {
                return "Float";
            }
            return type.Name;
        }

        public static string GetNiceTypeFullName(this Type type)
        {
            if (typeof(IList).IsAssignableFrom(type))
            {
                Type itemType = type.GetItemType();
                return "List" + "<" + GetNiceTypeFullName(itemType) + ">";
            }
            else if (typeof(IDictionary).IsAssignableFrom(type))
            {
                Type genericType = type.GetBaseGenericType();
                Type itemKeyType = genericType.GetGenericArguments()[0];
                Type itemValueType = genericType.GetGenericArguments()[1];

                return "Dictionary" + "<" + GetNiceTypeFullName(itemKeyType) + "," + GetNiceTypeFullName(itemValueType) + ">";
            }
            else if (typeof(sbyte).IsAssignableFrom(type))
            {
                return "System.SignedByte";
            }
            else if (typeof(short).IsAssignableFrom(type))
            {
                return "System.Short";
            }
            else if (typeof(ushort).IsAssignableFrom(type))
            {
                return "System.UnsignedShort";
            }
            else if (typeof(int).IsAssignableFrom(type))
            {
                return "System.Integer";
            }
            else if (typeof(uint).IsAssignableFrom(type))
            {
                return "System.UnsignedInteger";
            }
            else if (typeof(long).IsAssignableFrom(type))
            {
                return "System.Long";
            }
            else if (typeof(ulong).IsAssignableFrom(type))
            {
                return "System.UnsignedLong";
            }
            else if (typeof(float).IsAssignableFrom(type))
            {
                return "System.Float";
            }
            return type.FullName;
        }

        public static object InvokeGenericMethod(this Type type, string methodName, BindingFlags flags, Type genericType, object obj, params object[] parameters)
        {
            MethodInfo[] methods = type.GetMethods(flags);
            MethodInfo method = methods.Where(m => m.IsGenericMethod && m.Name.Equals(methodName, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();

            if (method != null)
            {
                MethodInfo generic = method.MakeGenericMethod(genericType);
                return generic.Invoke(obj, parameters);
            }

            return null;
        }

        public static bool IsInstantiable(this Type type)
        {
            return (!type.ContainsGenericParameters && !type.IsAbstract);
        }

        private static bool IsValidFieldOrProperty(this MemberInfo member)
        {
            return !Attribute.IsDefined(member, typeof(ObsoleteAttribute)) &&
                (member.MemberType == MemberTypes.Field ||
                (member.MemberType == MemberTypes.Property && ((PropertyInfo)member).CanWrite && ((PropertyInfo)member).CanRead));
        }

        public static IEnumerable<MemberInfo> GetOrderedMembers(this Type type, BindingFlags bindingAttr)
        {
            List<Type> typeList = new List<Type>();

            while (type != null)
            {
                typeList.Add(type);
                type = type.BaseType;
            }

            typeList.Reverse();

            return typeList.SelectMany(x => type.GetMembers(bindingAttr));
        }

        public static bool IsReflectableType(this Type type)
        {
            return (!type.IsPrimitive &&
                !type.IsEnum &&
                type != typeof(string) &&
                type != typeof(object) &&
                type != typeof(Type));
        }

        public static string GetMemberKey(MemberInfo member)
        {
            return member.Name;
        }

        public static MemberInfo[] GetMembers(Type type)
        {
            return type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static MemberInfo[] GetPublicMembers(Type type)
        {
            return type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
        }

        public static MemberInfo[] GetInstanceMembers(Type type)
        {
            return type.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static MemberInfo[] GetPublicInstanceMembers(Type type)
        {
            return type.GetMembers(BindingFlags.Instance | BindingFlags.Public);
        }

        public static void PerformActionOnAllMembersThatMeetCondition(this object obj, MemberCondition memberCondition, MemberAction memberAction, GetMemberKey getMemberKey, GetMembers getMembers, TypeCondition typeIsReflectable, bool performMemberActionRecursively)
        {
            PerformActionOnAllMembersThatMeetConditionInternal(obj, memberCondition, memberAction, getMemberKey, getMembers, typeIsReflectable, performMemberActionRecursively, -1, "", null, null, null, null, null);
        }

        private static void PerformActionOnAllMembersThatMeetConditionInternal(this object memberValue, MemberCondition memberCondition, MemberAction memberAction, GetMemberKey getMemberKey, GetMembers getMembers, TypeCondition typeIsReflectable, bool performMemberActionRecursively, int memberDepth, string memberPath, Type memberType, MemberInfo member, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            if (memberValue != null)
            {
                memberType = memberValue.GetType();
            }

            if (memberType == null)
            {
                return;
            }

            string memberPathPrefix = "";
            if (!string.IsNullOrEmpty(memberPath))
            {
                memberPathPrefix = memberPath + ".";
            }

            bool hasChildren =
                (member != null && typeof(IList).IsAssignableFrom(memberType)) ||
                (member != null && typeof(IDictionary).IsAssignableFrom(memberType)) ||
                (member == null || typeIsReflectable(memberType));

            if (performMemberActionRecursively && member != null && (memberCondition == null || memberCondition(memberDepth, memberPath, hasChildren, member, memberType, declaringObj, propertyParameters, declaringMember)))
            {
                if (memberAction != null)
                {
                    memberAction(memberDepth, memberPath, hasChildren, member, memberType, declaringObj, propertyParameters, declaringMember);
                }
            }

            if (!performMemberActionRecursively && member != null && (memberCondition == null || memberCondition(memberDepth, memberPath, hasChildren, member, memberType, declaringObj, propertyParameters, declaringMember)))
            {
                if (memberAction != null)
                {
                    memberAction(memberDepth, memberPath, hasChildren, member, memberType, declaringObj, propertyParameters, declaringMember);
                }
            }
            else if (typeof(IList).IsAssignableFrom(memberType))
            {
                IList list = (IList)memberValue;
                if (list != null)
                {
                    PropertyInfo itemProperty = memberType.GetItemProperty();

                    Type itemType = GetItemType(memberType);

                    for (int i = 0; i < list.Count; i++)
                    {
                        Type itemExactType = itemType;
                        if (list[i] != null)
                        {
                            itemExactType = list[i].GetType();
                        }

                        PerformActionOnAllMembersThatMeetConditionInternal(list[i], memberCondition, memberAction, getMemberKey, getMembers, typeIsReflectable, performMemberActionRecursively, memberDepth + 1, memberPath + "[" + i + "]", itemExactType, itemProperty, memberValue, new object[] { i }, member);
                    }
                }
            }
            else if (typeof(IDictionary).IsAssignableFrom(memberType))
            {
                IDictionary dict = (IDictionary)memberValue;
                if (dict != null)
                {
                    PropertyInfo itemProperty = memberType.GetItemProperty();

                    Type genericType = memberType.GetBaseGenericType();
                    Type itemKeyType = genericType.GetGenericArguments()[0];
                    Type itemValueType = genericType.GetGenericArguments()[1];

                    int i = 0;
                    foreach (object k in dict.Keys)
                    {
                        Type itemKeyExactType = itemKeyType;
                        if (k != null)
                        {
                            itemKeyExactType = k.GetType();
                        }

                        Type itemValueExactType = itemValueType;
                        if (dict[k] != null)
                        {
                            itemValueExactType = dict[k].GetType();
                        }

                        PerformActionOnAllMembersThatMeetConditionInternal(k, memberCondition, memberAction, getMemberKey, getMembers, typeIsReflectable, performMemberActionRecursively, memberDepth + 1, memberPath + "[" + i + "].Key", itemKeyExactType, null, memberValue, null, member);
                        PerformActionOnAllMembersThatMeetConditionInternal(dict[k], memberCondition, memberAction, getMemberKey, getMembers, typeIsReflectable, performMemberActionRecursively, memberDepth + 1, memberPath + "[" + i + "].Value", itemValueExactType, itemProperty, memberValue, new object[] { k }, member);

                        i++;
                    }
                }
            }
            else if (member == null || typeIsReflectable(memberType))
            {
                if (memberValue != null && getMembers != null)
                {
                    foreach (MemberInfo childMember in getMembers(memberType))
                    {
                        object childFieldValue = GetMemberValue(childMember, memberValue, null);
                        Type childFieldType = GetMemberType(childMember);

                        PerformActionOnAllMembersThatMeetConditionInternal(childFieldValue, memberCondition, memberAction, getMemberKey, getMembers, typeIsReflectable, performMemberActionRecursively, memberDepth + 1, memberPathPrefix + getMemberKey(childMember), childFieldType, childMember, memberValue, null, member);
                    }
                }
            }
            if (member != null)
            {
                if (memberType.IsStruct()) // Unbox if Struct
                {
                    if (declaringObj.GetType().GetField(member.Name) != null)
                    {
                        declaringObj.GetType().GetField(member.Name).SetValue(declaringObj, memberValue);
                    }
                }
            }
        }
    }

}
