﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ImprobableStudios.ReflectionUtilities
{
    public delegate int GetSerializedMemberOrder(SerializedMember serializedMember);

    public class SerializedMember
    {
        protected object m_targetObject;

        protected SerializedMember m_first;

        protected SerializedMember m_nextChild;

        protected SerializedMember m_next;

        protected MemberInfo m_member;

        protected object m_declaringObject;

        protected object[] m_memberParameters;

        protected int m_memberDepth;

        protected string m_memberPath;

        protected string m_memberName;

        protected object m_memberValue;

        protected string m_displayName;

        protected string m_description;

        protected bool m_hasChildren;

        protected bool m_isExpanded;

        protected GetMembers m_getMembers;

        protected TypeCondition m_isReflectableType;

        protected MemberCondition m_memberCondition;

        protected GetSerializedMemberOrder m_getOrder;

        public MemberInfo Member
        {
            get
            {
                return m_member;
            }
        }

        public object DeclaringObject
        {
            get
            {
                return m_declaringObject;
            }
        }

        public object[] MemberParameters
        {
            get
            {
                return m_memberParameters;
            }
        }

        public int MemberDepth
        {
            get
            {
                return m_memberDepth;
            }
        }

        public string MemberPath
        {
            get
            {
                return m_memberPath;
            }
        }

        public string MemberName
        {
            get
            {
                return m_memberName;
            }
        }

        public string DisplayName
        {
            get
            {
                return m_displayName;
            }
        }

        public string Description
        {
            get
            {
                return m_description;
            }
        }

        public bool HasChildren
        {
            get
            {
                return m_hasChildren;
            }
        }

        public bool IsExpanded
        {
            get
            {
                return m_isExpanded;
            }
            set
            {
                m_isExpanded = value;
            }
        }

        public Type MemberType
        {
            get
            {
                return m_member.GetMemberType();
            }
        }

        public object Value
        {
            get
            {
                if (m_member != null)
                {
                    IList list = m_declaringObject as IList;
                    if (list == null || (int)m_memberParameters[0] < list.Count)
                    {
                        return m_member.GetMemberValue(m_declaringObject, m_memberParameters);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return m_memberValue;
                }
            }
            set
            {
                if (m_member != null)
                {
                    m_member.SetMemberValue(m_declaringObject, value, m_memberParameters);
                }
                else
                {
                    m_memberValue = value;
                }
                Refresh();
            }
        }

        public object TargetObject
        {
            get
            {
                return m_targetObject;
            }
        }

        public Type ValueType
        {
            get
            {
                if (Value != null)
                {
                    return Value.GetType();
                }
                return m_member.GetMemberType();
            }
        }

        public SerializedMember(MemberInfo member, object declaringObject, object[] memberParameters, int memberDepth, string memberPath, string memberName, bool hasChildren, bool isExpanded)
        {
            m_member = member;
            m_declaringObject = declaringObject;
            m_memberParameters = memberParameters;
            m_memberDepth = memberDepth;
            m_memberPath = memberPath;
            m_memberName = memberName;
            DisplayNameAttribute displayNameAttribute = member.GetCustomAttribute<DisplayNameAttribute>();
            if (displayNameAttribute != null)
            {
                m_displayName = displayNameAttribute.DisplayName;
            }
            else
            {
                m_displayName = ReflectionExtensions.GetDisplayName(member.Name);
            }
            DescriptionAttribute descriptionAttribute = member.GetCustomAttribute<DescriptionAttribute>();
            if (descriptionAttribute != null)
            {
                m_description = descriptionAttribute.Description;
            }
            m_hasChildren = hasChildren;
            m_isExpanded = isExpanded;
        }

        private SerializedMember(object value, string memberName, string displayName, string description, bool isExpanded)
        {
            m_member = null;
            m_declaringObject = null;
            m_memberParameters = null;
            m_memberDepth = -1;
            m_memberPath = "";
            m_memberName = memberName;
            m_memberValue = value;
            m_displayName = displayName;
            m_description = description;
            m_hasChildren = true;
            m_isExpanded = isExpanded;
        }
        
    public SerializedMember Next(bool enterChildren)
        {
            if (enterChildren)
            {
                if (m_nextChild != null)
                {
                    return m_nextChild;
                }
                else
                {
                    return m_next;
                }
            }
            else
            {
                return m_next;
            }
        }

        private void SetupIterators()
        {
            SerializedMember[] serializedMembers = GetSerializedMembers(m_first, m_targetObject, m_getMembers, m_isReflectableType, m_memberCondition, m_getOrder);
            if (serializedMembers.Length > 0)
            {
                m_nextChild = serializedMembers[0];
            }
        }

        private void Refresh()
        {
            if (m_first != null)
            {
                Dictionary<string, SerializedMember> oldSerializedMembers = GetCurrentSerializedMemberDictionary(m_first);
                SerializedMember newIterator = GetIterator(m_first.m_memberName, m_first.m_displayName, m_first.m_description, m_targetObject, m_getMembers, m_isReflectableType, m_memberCondition, m_getOrder);
                Dictionary<string, SerializedMember> newSerializedMembers = GetCurrentSerializedMemberDictionary(newIterator);
                m_first.m_next = newIterator.m_next;
                m_first.m_nextChild = newIterator.m_nextChild;
                foreach (KeyValuePair<string, SerializedMember> kvp in newSerializedMembers)
                {
                    bool isExpanded = oldSerializedMembers.ContainsKey(kvp.Key) ? oldSerializedMembers[kvp.Key].IsExpanded : false;
                    SerializedMember newSerializedMember = kvp.Value;
                    newSerializedMember.IsExpanded = isExpanded;
                    newSerializedMember.m_first = m_first;
                }
            }
        }

        public override string ToString()
        {
            return m_memberPath;
        }

        public static SerializedMember GetIterator(string name, string displayName, string description, object targetObject, GetMembers getMembers, TypeCondition isReflectableType, MemberCondition memberCondition, GetSerializedMemberOrder getSerializedMemberOrder)
        {
            SerializedMember serializedMember = new SerializedMember(targetObject, name, displayName, description, false);
            serializedMember.m_first = serializedMember;
            serializedMember.m_targetObject = targetObject;
            serializedMember.m_getMembers = getMembers;
            serializedMember.m_isReflectableType = isReflectableType;
            serializedMember.m_memberCondition = memberCondition;
            serializedMember.m_getOrder = getSerializedMemberOrder;
            serializedMember.SetupIterators();
            return serializedMember;
        }

        public static SerializedMember GetIterator(MemberInfo memberInfo, string displayName, string description, object declaringObject, GetMembers getMembers, TypeCondition isReflectableType, MemberCondition memberCondition, GetSerializedMemberOrder getSerializedMemberOrder)
        {
            object targetObject = memberInfo.GetMemberValue(declaringObject);
            SerializedMember serializedMember = new SerializedMember(targetObject, memberInfo.Name, displayName, description, false);
            serializedMember.m_first = serializedMember;
            serializedMember.m_targetObject = targetObject;
            serializedMember.m_getMembers = getMembers;
            serializedMember.m_isReflectableType = isReflectableType;
            serializedMember.m_memberCondition = memberCondition;
            serializedMember.m_getOrder = getSerializedMemberOrder;
            serializedMember.SetupIterators();
            return serializedMember;
        }

        public static SerializedMember[] GetSerializedMembers(SerializedMember iterator, object targetObject, GetMembers getMembers, TypeCondition isReflectableType, MemberCondition memberCondition, GetSerializedMemberOrder getSerializedMemberOrder)
        {
            return GetSerializedMembers(iterator, targetObject, ReflectionExtensions.GetMemberKey, getMembers, isReflectableType, memberCondition, getSerializedMemberOrder);
        }

        private static SerializedMember[] GetSerializedMembers(SerializedMember iterator, object targetObject, GetMemberKey getMemberKey, GetMembers getMembers, TypeCondition isReflectableType, MemberCondition memberCondition, GetSerializedMemberOrder getSerializedMemberOrder)
        {
            List<SerializedMember> serializedMembers = new List<SerializedMember>();

            ReflectionExtensions.PerformActionOnAllMembersThatMeetCondition(
                targetObject,
                memberCondition,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetSerializedMembersRecursiveInternal(md, mp, hc, m, mt, d, p, dm, serializedMembers),
                getMemberKey,
                getMembers,
                isReflectableType,
                true);

            if (getSerializedMemberOrder != null)
            {
                serializedMembers = serializedMembers.OrderBy(x => getSerializedMemberOrder(x)).ToList();
            }

            for (int i = 0; i < serializedMembers.Count; i++)
            {
                SerializedMember serializedMember = serializedMembers[i];

                serializedMember.m_first = iterator;
                serializedMember.m_targetObject = targetObject;
                serializedMember.m_getMembers = getMembers;
                serializedMember.m_isReflectableType = isReflectableType;
                serializedMember.m_memberCondition = memberCondition;
                serializedMember.m_getOrder = getSerializedMemberOrder;

                SerializedMember nextChild = null;
                for (int j = i + 1; j < serializedMembers.Count;)
                {
                    if (serializedMembers[j].MemberDepth > serializedMember.MemberDepth)
                    {
                        nextChild = serializedMembers[j];
                    }
                    break;
                }
                serializedMember.m_nextChild = nextChild;

                SerializedMember next = null;
                for (int j = i + 1; j < serializedMembers.Count; j++)
                {
                    if (serializedMembers[j].MemberDepth <= serializedMember.MemberDepth)
                    {
                        next = serializedMembers[j];
                        break;
                    }
                }
                serializedMember.m_next = next;
            }

            return serializedMembers.ToArray();
        }

        private static void GetSerializedMembersRecursiveInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] memberParameters, MemberInfo declaringMember, List<SerializedMember> serializedMembers)
        {
            if (member != null)
            {
                serializedMembers.Add(new SerializedMember(member, declaringObj, memberParameters, memberDepth, memberPath, member.Name, hasChildren, false));
            }
        }
        
        private static Dictionary<string, SerializedMember> GetCurrentSerializedMemberDictionary(SerializedMember iterator)
        {
            Dictionary<string, SerializedMember> dictionary = new Dictionary<string, SerializedMember>();

            foreach (SerializedMember serializedMember in GetCurrentSerializedMembers(iterator))
            {
                dictionary[serializedMember.MemberPath] = serializedMember;
            }

            return dictionary;
        }

        private static SerializedMember[] GetCurrentSerializedMembers(SerializedMember iterator)
        {
            List<SerializedMember> serializedMembers = new List<SerializedMember>();

            bool enterChildren = true;
            while (iterator != null && (iterator = iterator.Next(enterChildren)) != null)
            {
                serializedMembers.Add(iterator);
            }
            return serializedMembers.ToArray();
        }

    }

}