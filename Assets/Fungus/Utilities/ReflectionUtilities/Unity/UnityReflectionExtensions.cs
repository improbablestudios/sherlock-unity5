﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using System.Collections;
using System.Linq.Expressions;

namespace ImprobableStudios.ReflectionUtilities
{

    public static class UnityReflectionExtensions
    {
        public static bool IsUnityAssetType(this Type type)
        {
            return type.IsSubclassOf(typeof(UnityEngine.Object)) && !typeof(MonoBehaviour).IsAssignableFrom(type) && !typeof(ScriptableObject).IsAssignableFrom(type) && !typeof(GameObject).IsAssignableFrom(type) && !typeof(Component).IsAssignableFrom(type);
        }

        public static T Clone<T>(this T scriptableObject) where T : ScriptableObject
        {
            if (scriptableObject != null)
            {
                T clone = ScriptableObject.CreateInstance(scriptableObject.GetType()) as T;
                scriptableObject.CopyAndPasteValues(clone, false, null);
                return clone;
            }
            return null;
        }

        public static FieldInfo[] GetUnitySerializedFields(this Type type)
        {
            List<FieldInfo> fields = new List<FieldInfo>();
            GetUnitySerializedFieldsInternal(type, fields);
            return fields.ToArray();
        }

        public static void GetUnitySerializedFieldsInternal(this Type type, List<FieldInfo> fields)
        {
            if (type.BaseType != null)
            {
                GetUnitySerializedFieldsInternal(type.BaseType, fields);
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            fields.AddRange(type.GetFields(flags).Where(m => m.IsUnitySerializableField()));
        }
        
        public static FieldInfo GetUnitySerializedField(this Type type, string name)
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            FieldInfo field = type.GetField(name, flags);
            if (field != null)
            {
                if (field.IsUnitySerializableField())
                {
                    return field;
                }
                else
                {
                    return null;
                }
            }

            if (type.BaseType != null)
            {
                return GetUnitySerializedField(type.BaseType, name);
            }

            return null;
        }

        public static bool IsUnitySerializableField(this FieldInfo field)
        {
            if (!Attribute.IsDefined(field, typeof(ObsoleteAttribute)))
            {
                if (field.IsPublic)
                {
                    if (!Attribute.IsDefined(field, typeof(NonSerializedAttribute)))
                    {
                        return true;
                    }
                }
                else
                {
                    if (Attribute.IsDefined(field, typeof(SerializeField)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool IsUnitySerializable(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            FieldInfo field = member as FieldInfo;
            if (field != null)
            {
                return field.IsUnitySerializableField();
            }

            PropertyInfo property = member as PropertyInfo;
            if (property != null)
            {
                FieldInfo declaringField = declaringMember as FieldInfo;
                if (declaringField != null)
                {
                    if (typeof(IList).IsAssignableFrom(declaringField.FieldType))
                    {
                        return declaringField.IsUnitySerializableField();
                    }
                }
            }
            return false;
        }


        public static Dictionary<string, object> GetUnitySerializedValueDictionary(this object obj)
        {
            Dictionary<string, object> serializedValueDictionary = new Dictionary<string, object>();

            obj.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IsUnitySerializable,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetUnitySerializedValueDictionaryInternal(md, mp, hc, m, mt, d, p, dm, serializedValueDictionary),
                false);

            return serializedValueDictionary;
        }

        private static void GetUnitySerializedValueDictionaryInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Dictionary<string, object> serializedValueDictionary)
        {
            object value = member.GetMemberValue(declaringObj);
            serializedValueDictionary[memberPath] = value;
        }

        public static void SetUnitySerializedValues(this object obj, Dictionary<string, object> serializedValueDictionary)
        {
            obj.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IsUnitySerializable,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => SetUnitySerializedValuesInternal(md, mp, hc, m, mt, d, p, dm, serializedValueDictionary),
                false);
        }

        private static void SetUnitySerializedValuesInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Dictionary<string, object> serializedValueDictionary)
        {
            object value = serializedValueDictionary[memberPath];
            member.SetMemberValue(declaringObj, value);
        }

        public static Dictionary<string, object> GetRemappedUnitySerializedValueDictionary(this UnityEngine.Object src, UnityEngine.Object dst, Func<bool, Component> componentFilter)
        {
            Dictionary<string, object> serializedValueDictionary = new Dictionary<string, object>();

            GameObject srcGameObject = src as GameObject;
            Component srcComponent = src as Component;
            if (srcGameObject == null && srcComponent != null)
            {
                srcGameObject = srcComponent.gameObject;
            }

            GameObject dstGameObject = dst as GameObject;
            Component dstComponent = dst as Component;
            if (dstGameObject == null && dstComponent != null)
            {
                dstGameObject = dstComponent.gameObject;
            }

            if (srcGameObject != null && dstGameObject != null)
            {
                Component[] srcComponents = srcGameObject.GetComponentsInChildren<Component>(true).Where(i => componentFilter(i)).ToArray();
                Component[] dstComponents = dstGameObject.GetComponentsInChildren<Component>(true).Where(i => componentFilter(i)).ToArray();

                src.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                    IsUnitySerializable,
                    (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetRemappedUnitySerializedValueDictionaryInternal(md, mp, hc, m, mt, d, p, dm, srcComponents, dstComponents, serializedValueDictionary),
                    false);
            }

            return serializedValueDictionary;
        }

        private static void GetRemappedUnitySerializedValueDictionaryInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Component[] srcComponents, Component[] dstComponents, Dictionary<string, object> serializedValueDictionary)
        {
            object value = member.GetMemberValue(declaringObj);

            UnityEngine.Object objValue = value as UnityEngine.Object;
            if (objValue != null)
            {
                value = GetRemappedReference(srcComponents, dstComponents, objValue);
            }

            serializedValueDictionary[memberPath] = value;
        }

        public static UnityEngine.Object GetRemappedReference(this Component[] srcComponents, Component[] dstComponents, UnityEngine.Object value)
        {
            Component componentValue = value as Component;
            if (componentValue != null)
            {
                Component[] srcComponentValues = srcComponents.Where(i => i.GetType() == value.GetType()).ToArray();
                int srcComponentValueIndex = Array.IndexOf(srcComponentValues, value);
                if (srcComponentValueIndex >= 0)
                {
                    Component[] dstComponentValues = dstComponents.Where(i => i.GetType() == value.GetType()).ToArray();
                    if (srcComponentValueIndex < dstComponentValues.Length)
                    {
                        value = dstComponentValues[srcComponentValueIndex];
                    }
                }
            }
            GameObject gameObjectValue = value as GameObject;
            if (gameObjectValue != null)
            {
                GameObject[] srcGameObjectValues = srcComponents.OfType<Transform>().Select(i => i.gameObject).ToArray();
                int srcGameObjectValueIndex = Array.IndexOf(srcGameObjectValues, value);
                if (srcGameObjectValueIndex >= 0)
                {
                    GameObject[] dstGameObjectValues = dstComponents.OfType<Transform>().Select(i => i.gameObject).ToArray();
                    if (srcGameObjectValueIndex < dstGameObjectValues.Length)
                    {
                        value = dstGameObjectValues[srcGameObjectValueIndex];
                    }
                }
            }

            return value;
        }

        public static void CopyAndPasteValues(this UnityEngine.Object src, UnityEngine.Object dst, bool remapSelfReferences, Func<bool, Component> componentFilter)
        {
            Dictionary<string, object> serializedValueDictionary = null;
            if (remapSelfReferences)
            {
                serializedValueDictionary = GetRemappedUnitySerializedValueDictionary(src, dst, componentFilter);
            }
            else
            {
                serializedValueDictionary = GetUnitySerializedValueDictionary(src);
            }

            SetUnitySerializedValues(dst, serializedValueDictionary);
        }

        public static bool IsReflectableUnityType(this Type type)
        {
            return (type.IsReflectableType() && !typeof(UnityEngine.Object).IsAssignableFrom(type));
        }

        public static void PerformActionOnAllUnitySerializedFieldsThatMeetCondition(this object obj, MemberCondition memberCondition, MemberAction memberAction, bool performMemberActionRecursively)
        {
            obj.PerformActionOnAllMembersThatMeetCondition(memberCondition, memberAction, ReflectionExtensions.GetMemberKey, GetUnitySerializedFields, IsReflectableUnityType, performMemberActionRecursively);
        }
        
        public static SerializedMember GetUnitySerializedMember(this object obj, string memberPath, Type memberType = null)
        {
            Dictionary<string, SerializedMember> dictionary = GetUnitySerializedMemberRecursive(obj, memberType);
            if (dictionary.ContainsKey(memberPath))
            {
                return dictionary[memberPath];
            }

            return null;
        }

        private static Dictionary<string, SerializedMember> GetUnitySerializedMemberRecursive(this object obj, Type memberType)
        {
            Dictionary<string, SerializedMember> dictionary = new Dictionary<string, SerializedMember>();

            MemberCondition memberCondition = null;
            if (memberType != null)
            {
                memberCondition = (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => memberType == null || memberType.IsAssignableFrom(mt);
            }

            ReflectionExtensions.PerformActionOnAllMembersThatMeetCondition(
                obj,
                memberCondition,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetUnitySerializedMemberRecursiveInternal(md, mp, hc, m, mt, d, p, dm, dictionary),
                ReflectionExtensions.GetMemberKey,
                GetUnitySerializedFields,
                IsReflectableUnityType,
                true);

            return dictionary;
        }

        private static void GetUnitySerializedMemberRecursiveInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] memberParameters, MemberInfo declaringMember, Dictionary<string, SerializedMember> dictionary)
        {
            if (member != null)
            {
                dictionary[memberPath] = new SerializedMember(member, declaringObj, memberParameters, memberDepth, memberPath, member.Name, hasChildren, false);
            }
        }

        public static string GetSerializedPropertyPath<T>(this T obj, Expression<Func<T, object>> memberAccess)
        {
            return GetSerializedPropertyPath(obj.GetMemberPath(memberAccess));
        }

        public static string GetSerializedPropertyPath(string memberPath)
        {
            return memberPath.Replace("[", ".Array.data[");
        }
        
        public static string GetMemberPath(string serializedPropertyPath)
        {
            return serializedPropertyPath.Replace(".Array.data[", "[");
        }

        public static void LoadFields(this UnityEngine.Object obj, Type typeToLoad, MemberCondition memberShouldBeLoaded, MemberAction onLoadField)
        {
            obj.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                memberShouldBeLoaded,
                onLoadField,
                false);
        }

        public static bool MemberShouldBeLoaded(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Type typeToLoad)
        {
            if (memberType.IsUnityAssetType())
            {
                return false;
            }
            if (typeToLoad.IsAssignableFrom(memberType) && declaringObj != null)
            {
                return true;
            }
            return false;
        }

        public static void AutoLoadFields(this UnityEngine.Object obj)
        {
            obj.LoadFields(
                typeof(Component),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] pp, MemberInfo dm) => MemberShouldBeLoaded(md, mp, hc, m, mt, d, pp, dm, typeof(Component)),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] pp, MemberInfo dm) => AutoLoadField(md, mp, hc, m, mt, d, pp, dm, obj));

            obj.LoadFields(
                typeof(IList),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] pp, MemberInfo dm) => MemberShouldBeLoaded(md, mp, hc, m, mt, d, pp, dm, typeof(IList)),
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] pp, MemberInfo dm) => AutoLoadListField(md, mp, hc, m, mt, d, pp, dm, obj));
        }

        private static void AutoLoadField(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, UnityEngine.Object obj)
        {
            string childName = ReflectionExtensions.GetNameWithoutMemberPrefixes(member.Name);
            if (typeof(Component).IsAssignableFrom(memberType))
            {
                GameObject gameObject = obj as GameObject;
                Component component = obj as Component;
                if (component != null)
                {
                    gameObject = component.gameObject;
                }
                if (gameObject != null)
                {
                    foreach (Component c in gameObject.GetComponentsInChildren(memberType, true))
                    {
                        if (c.name == childName)
                        {
                            member.SetMemberValue(declaringObj, c, propertyParameters);
                        }
                    }
                }
            }
        }

        private static void AutoLoadListField(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, UnityEngine.Object obj)
        {
            IList list = member.GetMemberValue(declaringObj, propertyParameters) as IList;
            if (list != null)
            {
                Type itemType = null;
                if (memberType.IsArray)
                {
                    itemType = memberType.GetElementType();
                }
                else
                {
                    itemType = memberType.GetGenericArguments()[0];
                }

                string childName = ReflectionExtensions.GetNameWithoutMemberPrefixes(member.Name);
                if (typeof(Component).IsAssignableFrom(itemType))
                {
                    list.Clear();
                    GameObject gameObject = obj as GameObject;
                    Component component = obj as Component;
                    if (component != null)
                    {
                        gameObject = component.gameObject;
                    }
                    if (gameObject != null)
                    {
                        foreach (Component c in gameObject.GetComponentsInChildren(itemType, true))
                        {
                            if (c.name.StartsWith(childName))
                            {
                                list.Add(c);
                            }
                        }
                    }
                    member.SetMemberValue(declaringObj, list, propertyParameters);
                }
            }
        }

    }

}
