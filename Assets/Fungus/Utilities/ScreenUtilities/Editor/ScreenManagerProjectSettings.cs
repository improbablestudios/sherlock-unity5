﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.ScreenUtilities
{

    public class ScreenManagerSettingsProvider : BaseAssetSettingsProvider<ScreenManager>
    {
        public ScreenManagerSettingsProvider() : base("Project/Managers/Screen Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new ScreenManagerSettingsProvider();
        }
    }

    public class ScreenManagerSettingsBuilder : BaseAssetSettingsBuilder<ScreenManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif