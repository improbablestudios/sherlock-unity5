﻿using ImprobableStudios.BaseUtilities;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.ScreenUtilities
{
    
    public class ScreenManager : SettingsScriptableObject<ScreenManager>
    {
        [Tooltip("The current resolution width")]
        [SerializeField]
        protected bool m_UseNativeResolution = true;

        [Tooltip("The current resolution width")]
        [Min(0)]
        [SerializeField]
        protected int m_ResolutionWidth;

        [Tooltip("The current resolution height")]
        [Min(0)]
        [SerializeField]
        protected int m_ResolutionHeight;

        [Tooltip("If true, display the game fullscreened. Otherwise, display the game windowed")]
        [SerializeField]
        [FormerlySerializedAs("fullscreen")]
        protected bool m_Fullscreen = false;

        [Tooltip("Platform agnostic fullscreen mode")]
        [SerializeField]
        protected FullScreenMode m_FullscreenMode;

        [Tooltip("A power saving setting, allowing the screen to dim some time after the last active user interaction (Set to -1 for NeverSleep or -2 for SystemSetting)")]
        [Min(-2)]
        [SerializeField]
        protected int m_SleepTimeout = -1;

        [Tooltip("Specifies the logical orientation of the screen")]
        [SerializeField]
        [FormerlySerializedAs("orientation")]
        protected ScreenOrientation m_Orientation = ScreenOrientation.AutoRotation;
        
        [Tooltip("Allow autorotation to landscape left?")]
        [SerializeField]
        protected bool m_AutorotateToLandscapeLeft = true;

        [Tooltip("Allow autorotation to landscape right?")]
        [SerializeField]
        protected bool m_AutorotateToLandscapeRight = true;

        [Tooltip("Allow autorotation to portrait?")]
        [SerializeField]
        protected bool m_AutorotateToPortrait = true;

        [Tooltip("Allow autorotation to portrait, upside down?")]
        [SerializeField]
        protected bool m_AutorotateToPortraitUpsideDown = true;

        public bool UseNativeResolution
        {
            get
            {
                return m_UseNativeResolution;
            }
            set
            {
                m_UseNativeResolution = value;
                PlayerPrefs.SetInt("Screenmanager Use Native", (m_UseNativeResolution) ? 1 : 0);
            }
        }

        public int ResolutionWidth
        {
            get
            {
                return m_ResolutionWidth;
            }
        }

        public int ResolutionHeight
        {
            get
            {
                return m_ResolutionHeight;
            }
        }

        public Resolution Resolution
        {
            get
            {
                Resolution resolution = new Resolution();
                resolution.width = m_ResolutionWidth;
                resolution.height = m_ResolutionHeight;
                return resolution;
            }
            set
            {
                m_ResolutionWidth = value.width;
                m_ResolutionHeight = value.height;
                Screen.SetResolution(m_ResolutionWidth, m_ResolutionHeight, m_Fullscreen);
                PlayerPrefs.SetInt("Screenmanager Resolution Width", m_ResolutionWidth);
                PlayerPrefs.SetInt("Screenmanager Resolution Height", m_ResolutionHeight);
            }
        }

        public bool Fullscreen
        {
            get
            {
                return m_Fullscreen;
            }
            set
            {
                m_Fullscreen = value;
                Screen.fullScreen = value;
            }
        }

        public FullScreenMode FullscreenMode
        {
            get
            {
                return m_FullscreenMode;
            }
            set
            {
                m_FullscreenMode = value;
                Screen.fullScreenMode = value;
                PlayerPrefs.SetInt("Screenmanager Fullscreen mode", (int)m_FullscreenMode);
            }
        }

        public int SleepTimeout
        {
            get
            {
                return m_SleepTimeout;
            }
            set
            {
                m_SleepTimeout = value;
                Screen.sleepTimeout = value;
            }
        }

        public ScreenOrientation Orientation
        {
            get
            {
                return m_Orientation;
            }
            set
            {
                m_Orientation = value;
                Screen.orientation = value;
            }
        }

        public bool AutorotateToLandscapeLeft
        {
            get
            {
                return m_AutorotateToLandscapeLeft;
            }
            set
            {
                m_AutorotateToLandscapeLeft = value;
                Screen.autorotateToLandscapeLeft = value;
            }
        }

        public bool AutorotateToLandscapeRight
        {
            get
            {
                return m_AutorotateToLandscapeRight;
            }
            set
            {
                m_AutorotateToLandscapeRight = value;
                Screen.autorotateToLandscapeRight = value;
            }
        }

        public bool AutorotateToPortrait
        {
            get
            {
                return m_AutorotateToPortrait;
            }
            set
            {
                m_AutorotateToPortrait = value;
                Screen.autorotateToPortrait = value;
            }
        }

        public bool AutorotateToPortraitUpsideDown
        {
            get
            {
                return m_AutorotateToPortraitUpsideDown;
            }
            set
            {
                m_AutorotateToPortraitUpsideDown = value;
                Screen.autorotateToPortraitUpsideDown = value;
            }
        }
        protected override void OnEnable()
        {
            base.OnEnable();

            Setup();
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            Setup();
        }

        protected virtual void Setup()
        {
            UseNativeResolution = m_UseNativeResolution;
            if (m_UseNativeResolution)
            {
                Resolution = Screen.currentResolution;
            }
            else
            {
                Resolution resolution = new Resolution();
                resolution.width = m_ResolutionWidth;
                resolution.height = m_ResolutionHeight;
                Resolution = resolution;
            }
            Fullscreen = m_Fullscreen;
            FullscreenMode = m_FullscreenMode;
            Orientation = m_Orientation;
            SleepTimeout = m_SleepTimeout;
            AutorotateToLandscapeLeft = m_AutorotateToLandscapeLeft;
            AutorotateToLandscapeRight = m_AutorotateToLandscapeRight;
            AutorotateToPortrait = m_AutorotateToPortrait;
            AutorotateToPortraitUpsideDown = m_AutorotateToPortraitUpsideDown;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ResolutionWidth))
            {
                if (m_UseNativeResolution)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ResolutionHeight))
            {
                if (m_UseNativeResolution)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}