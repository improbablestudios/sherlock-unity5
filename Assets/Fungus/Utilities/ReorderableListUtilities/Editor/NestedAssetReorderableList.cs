﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class NestedAssetReorderableList : AssetReorderableList
    {
        public NestedAssetReorderableList() : base()
        {
        }

        public NestedAssetReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public NestedAssetReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public NestedAssetReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public NestedAssetReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override object Create()
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            UnityEngine.Object childAsset = ScriptableObject.CreateInstance(GetElementType());

            if (childAsset != null)
            {
                string path = UnityEditor.AssetDatabase.GetAssetPath(target);
                if (!string.IsNullOrEmpty(path))
                {
                    UnityEditor.AssetDatabase.AddObjectToAsset(childAsset, target);
                    AssetDatabase.ImportAsset(path);
                }
            }

            return childAsset;
        }

        protected override void Insert(int index)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            UnityEngine.Object childAsset = ScriptableObject.CreateInstance(GetElementType());

            if (childAsset != null)
            {
                string path = UnityEditor.AssetDatabase.GetAssetPath(target);
                if (!string.IsNullOrEmpty(path))
                {
                    if (!AssetDatabase.LoadAllAssetRepresentationsAtPath(path).Contains(childAsset))
                    {
                        UnityEditor.AssetDatabase.AddObjectToAsset(childAsset, target);
                        AssetDatabase.ImportAsset(path);
                    }
                    base.Insert(index, childAsset);
                }
            }
        }

        protected override void Insert(int index, object element)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            UnityEngine.Object childAsset = element as UnityEngine.Object;

            if (childAsset != null)
            {
                string path = UnityEditor.AssetDatabase.GetAssetPath(target);
                if (!string.IsNullOrEmpty(path))
                {
                    if (!AssetDatabase.LoadAllAssetRepresentationsAtPath(path).Contains(childAsset))
                    {
                        UnityEditor.AssetDatabase.AddObjectToAsset(childAsset, target);
                        AssetDatabase.ImportAsset(path);
                    }
                    base.Insert(index, childAsset);
                }
            }
        }

        protected override void Delete(int[] indices)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                UnityEngine.Object childAsset = this[index].objectReferenceValue as UnityEngine.Object;
                if (childAsset != null)
                {
                    string path = AssetDatabase.GetAssetPath(target);
                    if (!string.IsNullOrEmpty(path))
                    {
                        if (AssetDatabase.LoadAllAssetRepresentationsAtPath(path).Contains(childAsset))
                        {
                            Undo.DestroyObjectImmediate(childAsset);
                            AssetDatabase.ImportAsset(path);
                        }
                    }
                }
            }

            base.Delete(indices);
        }

        protected override void RenameAsset(UnityEngine.Object asset, string newName)
        {
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(asset));
        }
    }

}
#endif