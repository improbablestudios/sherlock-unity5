#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using static ImprobableStudios.ReorderableListUtilities.GenericReorderableList;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class GenericReorderableListCache
    {
        protected Dictionary<string, GenericReorderableList> m_reorderableListCache = new Dictionary<string, GenericReorderableList>();

        protected ReorderableListFlags m_flags = 0;

        protected ElementDrawerDelegate m_expandedElementDrawerCallback;

        protected ElementHeightDelegate m_expandedElementHeightCallback;

        protected ElementDrawerDelegate m_collapsedElementDrawerCallback;

        protected ElementHeightDelegate m_collapsedElementHeightCallback;

        protected ObjectFieldDelegate m_objectFieldCallback;

        public ElementDrawerDelegate ExpandedElementDrawerCallback
        {
            get
            {
                return m_expandedElementDrawerCallback;
            }
            set
            {
                m_expandedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate ExpandedElementHeightCallback
        {
            get
            {
                return m_expandedElementHeightCallback;
            }
            set
            {
                m_expandedElementHeightCallback = value;
            }
        }

        public ElementDrawerDelegate CollapsedElementDrawerCallback
        {
            get
            {
                return m_collapsedElementDrawerCallback;
            }
            set
            {
                m_collapsedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate CollapsedElementHeightCallback
        {
            get
            {
                return m_collapsedElementHeightCallback;
            }
            set
            {
                m_collapsedElementHeightCallback = value;
            }
        }

        public ObjectFieldDelegate ObjectFieldCallback
        {
            get
            {
                return m_objectFieldCallback;
            }
            set
            {
                m_objectFieldCallback = value;
            }
        }

        public GenericReorderableListCache()
        {
        }

        public GenericReorderableListCache(ReorderableListFlags flags)
        {
            SetFlags(flags);
        }

        public GenericReorderableListCache(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(0);
        }

        public GenericReorderableListCache(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(0);
        }

        public GenericReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(flags);
        }

        public GenericReorderableListCache(ReorderableListFlags flags, ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(flags);
        }

        public GenericReorderableListCache(ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(0);
        }

        public GenericReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback,
            ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(flags);
        }

        public void SetFlags(ReorderableListFlags flags)
        {
            m_flags = flags;
        }

        public GenericReorderableList this[IList list]
        {
            get
            {
                string key = GetKey(list);
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                    m_reorderableListCache[key].Init(list);
                }
                return m_reorderableListCache[key];
            }
            set
            {
                string key = GetKey(list);
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                    m_reorderableListCache[key].Init(list);
                }
            }
        }

        public GenericReorderableList this[string key]
        {
            get
            {
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
                return m_reorderableListCache[key];
            }
            set
            {
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
            }
        }

        public GenericReorderableList[] ReorderableLists
        {
            get
            {
                return m_reorderableListCache.Values.ToArray();
            }
        }

        public bool ContainsKey(IList list)
        {
            return (m_reorderableListCache.ContainsKey(GetKey(list)));
        }

        protected string GetKey(IList list)
        {
            return list.GetHashCode().ToString();
        }

        protected virtual GenericReorderableList CreateReorderableList()
        {
            GenericReorderableList reorderableList =
                new GenericReorderableList(
                    m_flags,
                    m_expandedElementDrawerCallback, m_expandedElementHeightCallback,
                    m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback);
            return reorderableList;
        }
    }

    public class GenericReorderableListCache<T> : GenericReorderableListCache where T : GenericReorderableList
    {
        public GenericReorderableListCache() : base()
        {
        }

        public GenericReorderableListCache(ReorderableListFlags flags) : base(flags)
        {
        }

        public GenericReorderableListCache(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) 
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public GenericReorderableListCache(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) 
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                  collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public GenericReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) 
            : base(flags,
                  expandedElementDrawerCallback, expandedElementHeightCallback,
                  collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }
        
        public GenericReorderableListCache(ReorderableListFlags flags, ObjectFieldDelegate objectFieldCallback) : base(flags, objectFieldCallback)
        {
        }

        public GenericReorderableListCache(ObjectFieldDelegate objectFieldCallback) : base(objectFieldCallback)
        {
        }

        public GenericReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback,
            ObjectFieldDelegate objectFieldCallback)
            : base(flags,
                  expandedElementDrawerCallback, expandedElementHeightCallback,
                  collapsedElementDrawerCallback, collapsedElementHeightCallback,
                  objectFieldCallback)
        {
        }

        public new T this[IList list]
        {
            get
            {
                string key = GetKey(list);
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                    m_reorderableListCache[key].Init(list);
                }
                return m_reorderableListCache[key] as T;
            }
            set
            {
                string key = GetKey(list);
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = value;
                }
            }
        }

        public new T this[string key]
        {
            get
            {
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
                return m_reorderableListCache[key] as T;
            }
            set
            {
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
            }
        }

        public new T[] ReorderableLists
        {
            get
            {
                return m_reorderableListCache.Values.Cast<T>().ToArray();
            }
        }

        protected override GenericReorderableList CreateReorderableList()
        {
            GenericReorderableList reorderableList =
                Activator.CreateInstance(typeof(T),
                    new object[] {
                        m_flags,
                        m_expandedElementDrawerCallback, m_expandedElementHeightCallback,
                        m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback,
                        m_objectFieldCallback })
                        as T;
            return reorderableList;
        }
    }

}
#endif