﻿#if UNITY_EDITOR
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.UnityEditorUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class AssetReorderableList : SerializedPropertyReorderableList
    {
        public string AssetFolderEditorPreference
        {
            get
            {
                return EditorPrefs.GetString(AssetFolderEditorPreferenceKey, "Assets");
            }
            set
            {
                EditorPrefs.SetString(AssetFolderEditorPreferenceKey, value);
            }
        }

        protected string AssetFolderEditorPreferenceKey
        {
            get
            {
                return GetType().AssemblyQualifiedName + "+SaveFolder";
            }
        }

        public delegate void EditorElementDrawerDelegate(Rect position, UnityEngine.Object obj);

        public delegate float EditorElementHeightDelegate(UnityEngine.Object obj);

        public delegate bool EditorElementBooleanCondition(UnityEngine.Object obj, SerializedProperty property);

        public delegate int EditorElementIntegerCondition(UnityEngine.Object obj, SerializedProperty property);

        protected EditorElementDrawerDelegate m_editorExpandedElementDrawerCallback;

        protected EditorElementHeightDelegate m_editorExpandedElementHeightCallback;

        protected EditorElementDrawerDelegate m_editorCollapsedElementDrawerCallback;

        protected EditorElementHeightDelegate m_editorCollapsedElementHeightCallback;

        protected EditorElementBooleanCondition m_editorIsPropertyVisibleCallback;

        protected EditorElementBooleanCondition m_editorIsPropertyEditableCallback;

        protected EditorElementIntegerCondition m_editorGetPropertyIndentLevelOffsetCallback;

        protected SerializedPropertyReorderableListCache[] m_reorderableListCaches = new SerializedPropertyReorderableListCache[0];

        public EditorElementDrawerDelegate EditorExpandedElementDrawerCallback
        {
            get
            {
                return m_editorExpandedElementDrawerCallback;
            }
            set
            {
                m_editorExpandedElementDrawerCallback = value;
            }
        }

        public EditorElementHeightDelegate EditorExpandedElementHeightCallback
        {
            get
            {
                return m_editorExpandedElementHeightCallback;
            }
            set
            {
                m_editorExpandedElementHeightCallback = value;
            }
        }

        public EditorElementDrawerDelegate EditorCollapsedElementDrawerCallback
        {
            get
            {
                return m_editorCollapsedElementDrawerCallback;
            }
            set
            {
                m_editorCollapsedElementDrawerCallback = value;
            }
        }

        public EditorElementHeightDelegate EditorCollapsedElementHeightCallback
        {
            get
            {
                return m_editorCollapsedElementHeightCallback;
            }
            set
            {
                m_editorCollapsedElementHeightCallback = value;
            }
        }

        public EditorElementBooleanCondition EditorIsPropertyVisibleCallback
        {
            get
            {
                return m_editorIsPropertyVisibleCallback;
            }
            set
            {
                m_editorIsPropertyVisibleCallback = value;
            }
        }

        public EditorElementBooleanCondition EditorIsPropertyEditableCallback
        {
            get
            {
                return m_editorIsPropertyEditableCallback;
            }
            set
            {
                m_editorIsPropertyEditableCallback = value;
            }
        }

        public EditorElementIntegerCondition EditorGetPropertyIndentLevelOffsetCallback
        {
            get
            {
                return m_editorGetPropertyIndentLevelOffsetCallback;
            }
            set
            {
                m_editorGetPropertyIndentLevelOffsetCallback = value;
            }
        }

        public SerializedPropertyReorderableListCache[] ReorderableListCaches
        {
            get
            {
                int length = GetElementCount();
                if (m_reorderableListCaches.Length != length)
                {
                    SerializedPropertyReorderableListCache[] reorderableListCaches = new SerializedPropertyReorderableListCache[length];
                    for (int i = 0; i < reorderableListCaches.Length; i++)
                    {
                        if (i < m_reorderableListCaches.Length)
                        {
                            reorderableListCaches[i] = m_reorderableListCaches[i];
                        }
                    }
                    m_reorderableListCaches = reorderableListCaches;
                }
                return m_reorderableListCaches;
            }
            set
            {
                m_reorderableListCaches = value;
            }
        }

        public AssetReorderableList() : base()
        {
        }

        public AssetReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public AssetReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public AssetReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public AssetReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override object[] Copy(int[] indices)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            object[] copies = new object[indices.Length];

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                UnityEngine.Object mainAsset = this[index].objectReferenceValue;
                object copy = null;
                if (mainAsset != null)
                {
                    copy = UnityEngine.Object.Instantiate(mainAsset);
                }
                copies[i] = copy;
            }

            return copies;
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            UnityEngine.Object mainAsset = this[index].objectReferenceValue as UnityEngine.Object;
            if (mainAsset != null)
            {
                SerializedObject childSerializedObject = new SerializedObject(mainAsset);

                List<string> values = new List<string>();
                values.Add(mainAsset.name);
                foreach (SerializedProperty childProperty in childSerializedObject.GetVisibleProperties(true))
                {
                    if (IsPropertyVisible(mainAsset, childProperty))
                    {
                        string valueString = GetPropertyIdentifier(childProperty);
                        if (valueString != null)
                        {
                            values.Add(valueString);
                        }
                    }
                }
                return values.ToArray();
            }
            else
            {
                return base.GetElementIdentifiers(index);
            }
        }

        public override bool CanCollapse(int index)
        {
            SerializedProperty elementProperty = this[index];
            UnityEngine.Object mainAsset = this[index].objectReferenceValue as UnityEngine.Object;
            if (mainAsset != null)
            {
                return true;
            }
            else
            {
                return base.CanCollapse(index);
            }
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            SerializedProperty elementProperty = this[index];
            UnityEngine.Object target = this[index].objectReferenceValue as UnityEngine.Object;

            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = GetChildLabelWidth(position);
            
            if (m_expandedElementDrawerCallback != null)
            {
                position = DrawName(position, index);
                m_expandedElementDrawerCallback(position, elementProperty, GUIContent.none);
            }
            else if (target != null)
            {
                if (EditorExpandedElementDrawerCallback != null)
                {
                    position = DrawName(position, index);
                    EditorExpandedElementDrawerCallback(position, target);
                }
                else
                {
                    position = DrawName(position, index);

                    SerializedObject childSerializedObject = new SerializedObject(target);

                    childSerializedObject.Update();

                    ExtendedEditorGUI.DrawVisibleProperties(
                        position,
                        childSerializedObject,
                        (SerializedProperty p) => IsPropertyVisible(target, p),
                        (SerializedProperty p) => IsPropertyEditable(target, p),
                        (SerializedProperty p) => GetPropertyIndentLevelOffset(target, p),
                        (Rect r, SerializedProperty p, GUIContent l, bool c) => ChildPropertyField(index, r, p, l, c),
                        (SerializedProperty p, GUIContent l, bool c) => GetChildPropertyHeight(index, p, l, c));

                    childSerializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                base.DrawElementExpandedContent(position, index);
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected override void DrawElementCollapsedContent(Rect position, int index)
        {
            SerializedProperty elementProperty = this[index];
            UnityEngine.Object target = this[index].objectReferenceValue as UnityEngine.Object;

            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = GetChildLabelWidth(position);

            if (m_collapsedElementDrawerCallback != null)
            {
                m_collapsedElementDrawerCallback(position, elementProperty, GUIContent.none);
            }
            else if (target != null)
            {
                if (EditorCollapsedElementDrawerCallback != null)
                {
                    position = DrawName(position, index);
                    EditorCollapsedElementDrawerCallback(position, target);
                }
                else
                {
                    DrawName(position, index);
                }
            }
            else
            {
                base.DrawElementCollapsedContent(position, index);
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            SerializedProperty elementProperty = this[index];
            UnityEngine.Object target = this[index].objectReferenceValue as UnityEngine.Object;

            if (m_expandedElementHeightCallback != null)
            {
                return m_expandedElementHeightCallback(elementProperty, GUIContent.none);
            }
            else if (target != null)
            {
                if (EditorExpandedElementHeightCallback != null)
                {
                    return EditorGUIUtility.singleLineHeight + 2f +
                    EditorExpandedElementHeightCallback(target);
                }
                else
                {
                    SerializedObject childSerializedObject = new SerializedObject(target);
                    return EditorGUIUtility.singleLineHeight + 2f +
                        ExtendedEditorGUI.GetVisiblePropertiesHeight(
                            childSerializedObject,
                            (SerializedProperty p) => IsPropertyVisible(target, p),
                            (SerializedProperty p, GUIContent l, bool c) => GetChildPropertyHeight(index, p, l, c));
                }
            }
            else
            {
                return base.GetElementExpandedContentHeight(index);
            }
        }

        protected override float GetElementCollapsedContentHeight(int index)
        {
            SerializedProperty elementProperty = this[index];
            UnityEngine.Object target = this[index].objectReferenceValue as UnityEngine.Object;

            if (m_collapsedElementHeightCallback != null)
            {
                return m_collapsedElementHeightCallback(elementProperty, GUIContent.none);
            }
            else if (target != null)
            {
                if (EditorCollapsedElementHeightCallback != null)
                {
                    return EditorGUIUtility.singleLineHeight + 2f +
                    EditorCollapsedElementHeightCallback(target);
                }
                else
                {
                    return EditorGUIUtility.singleLineHeight;
                }
            }
            else
            {
                return base.GetElementCollapsedContentHeight(index);
            }
        }

        public virtual bool IsPropertyVisible(UnityEngine.Object obj, SerializedProperty property)
        {
            if (EditorIsPropertyVisibleCallback != null)
            {
                return EditorIsPropertyVisibleCallback(obj, property);
            }
            return base.IsPropertyVisible(property);
        }

        public virtual bool IsPropertyEditable(UnityEngine.Object obj, SerializedProperty property)
        {
            if (EditorIsPropertyEditableCallback != null)
            {
                return EditorIsPropertyEditableCallback(obj, property);
            }
            return base.IsPropertyEditable(property);
        }

        public int GetPropertyIndentLevelOffset(UnityEngine.Object obj, SerializedProperty property)
        {
            if (EditorGetPropertyIndentLevelOffsetCallback != null)
            {
                return EditorGetPropertyIndentLevelOffsetCallback(obj, property);
            }
            return base.GetPropertyIndentLevelOffset(property);
        }

        public bool ChildPropertyField(int index, Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (property.isArray && property.propertyType != SerializedPropertyType.String)
            {
                SerializedPropertyReorderableListCache reorderableListCache = ReorderableListCaches[index];
                if (reorderableListCache == null)
                {
                    reorderableListCache = new SerializedPropertyReorderableListCache(m_flags, m_expandedElementDrawerCallback, m_expandedElementHeightCallback, m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback);
                    ReorderableListCaches[index] = reorderableListCache;
                }
                SerializedPropertyReorderableList reorderableList = reorderableListCache[property];
                position.height = reorderableList.GetHeight(property, label);
                reorderableList.Draw(position, property, label);
                return false;
            }
            else
            {
                return base.ChildPropertyField(position, property, label, includeChildren);
            }
        }

        public float GetChildPropertyHeight(int index, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal))
            {
                return 0f;
            }
            else if (property.isArray && property.propertyType != SerializedPropertyType.String)
            {
                SerializedPropertyReorderableListCache reorderableListCache = ReorderableListCaches[index];
                if (reorderableListCache == null)
                {
                    reorderableListCache = new SerializedPropertyReorderableListCache(m_flags, m_expandedElementDrawerCallback, m_expandedElementHeightCallback, m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback);
                    ReorderableListCaches[index] = reorderableListCache;
                }
                SerializedPropertyReorderableList reorderableList = reorderableListCache[property];
                return reorderableList.GetHeight(property, label);
            }
            else
            {
                return base.GetChildPropertyHeight(property, label, includeChildren);
            }
        }


        protected virtual Rect DrawName(Rect position, int index)
        {
            UnityEngine.Object target = this[index].objectReferenceValue as UnityEngine.Object;

            float margin = 2f;

            SerializedObject childSerializedObject = new SerializedObject(target);
            childSerializedObject.Update();

            SerializedProperty nameProp = childSerializedObject.FindProperty("m_Name");

            Rect fieldPosition = position;
            fieldPosition.height = EditorGUI.GetPropertyHeight(nameProp);

            Texture2D buttonIcon = EditorGUIUtility.FindTexture(EditorGUIUtility.isProSkin ? "d_Project" : "Project");
            GUIContent buttonLabel = new GUIContent(buttonIcon);
            float buttonWidth = GUIStyle.none.CalcSize(buttonLabel).x;

            Rect namePosition = fieldPosition;
            namePosition.xMax -= buttonWidth + margin;

            Rect buttonPosition = fieldPosition;
            buttonPosition.xMin = namePosition.xMax + margin;

            EditorGUI.BeginChangeCheck();
            nameProp.stringValue = EditorGUI.DelayedTextField(namePosition, new GUIContent("Name"), nameProp.stringValue);
            if (EditorGUI.EndChangeCheck())
            {
                childSerializedObject.ApplyModifiedProperties();
                RenameAsset(target, nameProp.stringValue);
                childSerializedObject.Update();
            }

            if (GUI.Button(buttonPosition, buttonLabel, GUIStyle.none))
            {
                EditorGUIUtility.PingObject(target);
            }

            position.yMin += fieldPosition.height + margin;

            childSerializedObject.ApplyModifiedProperties();

            return position;
        }

        protected virtual void RenameAsset(UnityEngine.Object asset, string newName)
        {
            AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(asset), newName);
        }

        protected string GetAssetSavePath()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save " + ObjectNames.NicifyVariableName(GetElementType().Name),
                                                               GetElementType().Name + ".asset",
                                                               "asset",
                                                               "Please enter a filename to save the " + ObjectNames.NicifyVariableName(GetElementType().Name) + " as",
                                                               AssetFolderEditorPreference);
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }

            AssetFolderEditorPreference = Path.GetDirectoryName(path);

            return path;
        }
    }

}
#endif