#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{
    public static class ReorderableListStyles
    {
        static ReorderableListStyles()
        {
        }

        private static GUIStyle s_scrollViewStyle;
        public static GUIStyle ScrollViewStyle
        {
            get
            {
                if (s_scrollViewStyle == null)
                {
                    s_scrollViewStyle = new GUIStyle(ContainerStyle);
                    s_scrollViewStyle.padding.left = 0;
                    s_scrollViewStyle.padding.top = 0;
                }
                return s_scrollViewStyle;
            }
        }

        private static GUIStyle s_scrollViewNoMarginStyle;
        public static GUIStyle ScrollViewNoMarginStyle
        {
            get
            {
                if (s_scrollViewNoMarginStyle == null)
                {
                    s_scrollViewNoMarginStyle = new GUIStyle(ContainerStyle);
                    s_scrollViewNoMarginStyle.padding.left = 0;
                    s_scrollViewNoMarginStyle.padding.top = 0;
                    s_scrollViewNoMarginStyle.margin = new RectOffset();
                }
                return s_scrollViewNoMarginStyle;
            }
        }

        private static GUIContent s_createIcon;
        public static GUIContent CreateIcon
        {
            get
            {
                if (s_createIcon == null)
                {
                    s_createIcon = new GUIContent(ReorderableListSkin.Textures.Icon_Create, "Create in list");
                }
                return s_createIcon;
            }
        }

        private static GUIContent s_addMenuIcon;
        public static GUIContent CreateMenuIcon
        {
            get
            {
                if (s_addMenuIcon == null)
                {
                    s_addMenuIcon = new GUIContent(ReorderableListSkin.Textures.Icon_CreateMenu, "Choose type to create");
                }
                return s_addMenuIcon;
            }
        }

        private static GUIContent s_deleteIcon;
        public static GUIContent DeleteIcon
        {
            get
            {
                if (s_deleteIcon == null)
                {
                    s_deleteIcon = new GUIContent(ReorderableListSkin.Textures.Icon_Delete, "Delete from list");
                }
                return s_deleteIcon;
            }
        }

        private static GUIStyle s_sizeFieldStyle;
        public static GUIStyle SizeFieldStyle
        {
            get
            {
                if (s_sizeFieldStyle == null)
                {
                    s_sizeFieldStyle = new GUIStyle(EditorStyles.numberField);
                    s_sizeFieldStyle.alignment = TextAnchor.MiddleRight;
                }
                return s_sizeFieldStyle;
            }
        }

        private static GUIStyle s_sizeLabelStyle;
        public static GUIStyle SizeLabelStyle
        {
            get
            {
                if (s_sizeLabelStyle == null)
                {
                    s_sizeLabelStyle = new GUIStyle(EditorStyles.label);
                    s_sizeLabelStyle.alignment = TextAnchor.MiddleRight;
                }
                return s_sizeLabelStyle;
            }
        }

        private static GUIStyle s_dragHandleStyle;
        public static GUIStyle DragHandleStyle
        {
            get
            {
                if (s_dragHandleStyle == null)
                {
                    s_dragHandleStyle = new GUIStyle("RL DragHandle");
                    s_dragHandleStyle.alignment = TextAnchor.MiddleCenter;
                }
                return s_dragHandleStyle;
            }
        }

        private static GUIStyle s_headerStyle;
        public static GUIStyle HeaderStyle
        {
            get
            {
                if (s_headerStyle == null)
                {
                    s_headerStyle = new GUIStyle("RL Header");
                    s_headerStyle.normal.background = ReorderableListSkin.Textures.HeaderBackground;
                    s_headerStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackground;
                    s_headerStyle.margin = new RectOffset(4, 4, 4, 0);
                    s_headerStyle.border = new RectOffset(2, 2, 2, 2);
                    s_headerStyle.padding = new RectOffset(2, 2, 2, 2);
                    s_headerStyle.alignment = TextAnchor.MiddleCenter;
                    s_headerStyle.normal.textColor = EditorGUIUtility.isProSkin
                        ? new Color(0.8f, 0.8f, 0.8f)
                        : new Color(0.2f, 0.2f, 0.2f);
                    s_headerStyle.stretchWidth = true;
                    s_headerStyle.stretchHeight = false;
                    s_headerStyle.fixedWidth = 0;
                    s_headerStyle.fixedHeight = 0;
                }
                return s_headerStyle;
            }
        }

        private static GUIStyle s_headerNoMarginsStyle;
        public static GUIStyle HeaderNoMarginsStyle
        {
            get
            {
                if (s_headerNoMarginsStyle == null)
                {
                    s_headerNoMarginsStyle = new GUIStyle(HeaderStyle);
                    s_headerNoMarginsStyle.margin = new RectOffset(0, 0, 0, 0);
                }
                return s_headerNoMarginsStyle;
            }
        }

        private static GUIStyle s_headerLeftStyle;
        public static GUIStyle HeaderLeftStyle
        {
            get
            {
                if (s_headerLeftStyle == null)
                {
                    s_headerLeftStyle = new GUIStyle(HeaderStyle);
                    s_headerLeftStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundLeft;
                    s_headerLeftStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundLeft;
                }
                return s_headerLeftStyle;
            }
        }

        private static GUIStyle s_headerMiddleStyle;
        public static GUIStyle HeaderMiddleStyle
        {
            get
            {
                if (s_headerMiddleStyle == null)
                {
                    s_headerMiddleStyle = new GUIStyle(HeaderStyle);
                    s_headerMiddleStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundMiddle;
                    s_headerMiddleStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundMiddle;
                }
                return s_headerMiddleStyle;
            }
        }

        private static GUIStyle s_headerRightStyle;
        public static GUIStyle HeaderRightStyle
        {
            get
            {
                if (s_headerRightStyle == null)
                {
                    s_headerRightStyle = new GUIStyle(HeaderStyle);
                    s_headerRightStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundRight;
                    s_headerRightStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundRight;
                }
                return s_headerRightStyle;
            }
        }

        private static GUIStyle s_headerCollapsedStyle;
        public static GUIStyle HeaderCollapsedStyle
        {
            get
            {
                if (s_headerCollapsedStyle == null)
                {
                    s_headerCollapsedStyle = new GUIStyle(HeaderStyle);
                    s_headerCollapsedStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundCollapsed;
                    s_headerCollapsedStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundCollapsed;
                    s_headerCollapsedStyle.margin = new RectOffset(4, 4, 4, 4);
                }
                return s_headerCollapsedStyle;
            }
        }

        private static GUIStyle s_headerLeftCollapsedStyle;
        public static GUIStyle HeaderLeftCollapsedStyle
        {
            get
            {
                if (s_headerLeftCollapsedStyle == null)
                {
                    s_headerLeftCollapsedStyle = new GUIStyle(HeaderStyle);
                    s_headerLeftCollapsedStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundLeftCollapsed;
                    s_headerLeftCollapsedStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundLeftCollapsed;
                    s_headerLeftCollapsedStyle.margin = new RectOffset(4, 4, 4, 4);
                }
                return s_headerLeftCollapsedStyle;
            }
        }

        private static GUIStyle s_headerMiddleCollapsedStyle;
        public static GUIStyle HeaderMiddleCollapsedStyle
        {
            get
            {
                if (s_headerMiddleCollapsedStyle == null)
                {
                    s_headerMiddleCollapsedStyle = new GUIStyle(HeaderStyle);
                    s_headerMiddleCollapsedStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundMiddleCollapsed;
                    s_headerMiddleCollapsedStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundMiddleCollapsed;
                    s_headerMiddleCollapsedStyle.margin = new RectOffset(4, 4, 4, 4);
                }
                return s_headerMiddleCollapsedStyle;
            }
        }

        private static GUIStyle s_headerRightCollapsedStyle;
        public static GUIStyle HeaderRightCollapsedStyle
        {
            get
            {
                if (s_headerRightCollapsedStyle == null)
                {
                    s_headerRightCollapsedStyle = new GUIStyle(HeaderStyle);
                    s_headerRightCollapsedStyle.normal.background = ReorderableListSkin.Textures.HeaderBackgroundRightCollapsed;
                    s_headerRightCollapsedStyle.onNormal.background = ReorderableListSkin.Textures.HeaderBackgroundRightCollapsed;
                    s_headerRightCollapsedStyle.margin = new RectOffset(4, 4, 4, 4);
                }
                return s_headerRightCollapsedStyle;
            }
        }

        private static GUIStyle s_toolbarSearchTypeStyle1;
        public static GUIStyle ToolbarSearchTypeStyle1
        {
            get
            {
                if (s_toolbarSearchTypeStyle1 == null)
                {
                    Texture2D icon = EditorGUIUtility.FindTexture("sv_icon_dot0_sml");
                    s_toolbarSearchTypeStyle1 = new GUIStyle();
                    s_toolbarSearchTypeStyle1.imagePosition = ImagePosition.ImageOnly;
                    s_toolbarSearchTypeStyle1.normal.background = icon;
                    s_toolbarSearchTypeStyle1.active.background = icon;
                    s_toolbarSearchTypeStyle1.focused.background = icon;
                    s_toolbarSearchTypeStyle1.hover.background = icon;
                    s_toolbarSearchTypeStyle1.stretchWidth = false;
                    s_toolbarSearchTypeStyle1.stretchHeight = false;
                    s_toolbarSearchTypeStyle1.fixedWidth = icon.width;
                    s_toolbarSearchTypeStyle1.fixedHeight = icon.height;
                    s_toolbarSearchTypeStyle1.alignment = TextAnchor.MiddleCenter;
                }

                return s_toolbarSearchTypeStyle1;
            }
        }

        private static GUIStyle s_toolbarSearchTypeStyle2;
        public static GUIStyle ToolbarSearchTypeStyle2
        {
            get
            {
                if (s_toolbarSearchTypeStyle2 == null)
                {
                    Texture2D icon = EditorGUIUtility.FindTexture("sv_icon_dot8_sml");
                    s_toolbarSearchTypeStyle2 = new GUIStyle();
                    s_toolbarSearchTypeStyle2.imagePosition = ImagePosition.ImageOnly;
                    s_toolbarSearchTypeStyle2.normal.background = icon;
                    s_toolbarSearchTypeStyle2.active.background = icon;
                    s_toolbarSearchTypeStyle2.focused.background = icon;
                    s_toolbarSearchTypeStyle2.hover.background = icon;
                    s_toolbarSearchTypeStyle2.stretchWidth = false;
                    s_toolbarSearchTypeStyle2.stretchHeight = false;
                    s_toolbarSearchTypeStyle2.fixedWidth = icon.width;
                    s_toolbarSearchTypeStyle2.fixedHeight = icon.height;
                    s_toolbarSearchTypeStyle2.alignment = TextAnchor.MiddleCenter;
                }

                return s_toolbarSearchTypeStyle2;
            }
        }

        private static GUIStyle s_toolbarStyle;
        public static GUIStyle ToolbarStyle
        {
            get
            {
                if (s_toolbarStyle == null)
                {
                    s_toolbarStyle = new GUIStyle("RL Background");
                    s_toolbarStyle.normal.background = ReorderableListSkin.Textures.Container2Background;
                    s_toolbarStyle.onNormal.background = ReorderableListSkin.Textures.Container2Background;
                    s_toolbarStyle.margin = new RectOffset(4, 4, 0, 0);
                    s_toolbarStyle.border = new RectOffset(2, 2, 0, 0);
                    s_toolbarStyle.padding = new RectOffset(2, 2, 2, 2);
                    s_toolbarStyle.stretchWidth = true;
                    s_toolbarStyle.stretchHeight = false;
                    s_toolbarStyle.fixedWidth = 0;
                    s_toolbarStyle.fixedHeight = 0;
                }
                return s_toolbarStyle;
            }
        }

        private static GUIStyle s_containerStyle;
        public static GUIStyle ContainerStyle
        {
            get
            {
                if (s_containerStyle == null)
                {
                    s_containerStyle = new GUIStyle("RL Background");
                    s_containerStyle.normal.background = ReorderableListSkin.Textures.ContainerBackground;
                    s_containerStyle.onNormal.background = ReorderableListSkin.Textures.ContainerBackground;
                    s_containerStyle.margin = new RectOffset(4, 4, 0, 4);
                    s_containerStyle.border = new RectOffset(2, 2, 2, 2);
                    s_containerStyle.padding = new RectOffset(2, 2, 1, 2);
                    s_containerStyle.stretchWidth = true;
                    s_containerStyle.stretchHeight = false;
                    s_containerStyle.fixedWidth = 0;
                    s_containerStyle.fixedHeight = 0;
                }
                return s_containerStyle;
            }
        }

        private static GUIStyle s_containerNoMarginsStyle;
        public static GUIStyle ContainerNoMarginsStyle
        {
            get
            {
                if (s_containerNoMarginsStyle == null)
                {
                    s_containerNoMarginsStyle = new GUIStyle(ContainerStyle);
                    s_containerNoMarginsStyle.margin = new RectOffset(0, 0, 0, 0);
                }
                return s_containerNoMarginsStyle;
            }
        }

        private static GUIStyle s_containerNoPaddingStyle;
        public static GUIStyle ContainerNoPaddingStyle
        {
            get
            {
                if (s_containerNoPaddingStyle == null)
                {
                    s_containerNoPaddingStyle = new GUIStyle(ContainerStyle);
                    s_containerNoPaddingStyle.padding = new RectOffset(0, 0, 0, 0);
                }
                return s_containerNoPaddingStyle;
            }
        }

        private static GUIStyle s_headerLabelStyle;
        public static GUIStyle HeaderLabelStyle
        {
            get
            {
                if (s_headerLabelStyle == null)
                {
                    s_headerLabelStyle = new GUIStyle(EditorStyles.label);
                    s_headerLabelStyle.alignment = TextAnchor.MiddleLeft;
                }
                return s_headerLabelStyle;
            }
        }

        private static GUIStyle s_elementButtonStyle;
        public static GUIStyle ElementButtonStyle
        {
            get
            {
                if (s_elementButtonStyle == null)
                {
                    s_elementButtonStyle = new GUIStyle();
                    s_elementButtonStyle.border = new RectOffset(2, 2, 2, 2);
                    s_elementButtonStyle.active.background = ReorderableListSkin.Textures.ElementButton_Active;
                    s_elementButtonStyle.imagePosition = ImagePosition.ImageOnly;
                    s_elementButtonStyle.alignment = TextAnchor.MiddleCenter;
                }
                return s_elementButtonStyle;
            }

        }

        private static GUIStyle s_headerButtonStyle;
        public static GUIStyle HeaderButtonStyle
        {
            get
            {
                if (s_headerButtonStyle == null)
                {
                    s_headerButtonStyle = new GUIStyle();
                    s_headerButtonStyle.border = new RectOffset(2, 2, 2, 2);
                    s_headerButtonStyle.active.background = ReorderableListSkin.Textures.HeaderButton_Active;
                    s_headerButtonStyle.imagePosition = ImagePosition.ImageOnly;
                    s_headerButtonStyle.alignment = TextAnchor.MiddleCenter;
                }
                return s_headerButtonStyle;
            }
        }

        private static GUIStyle s_indexLabelStyle;
        public static GUIStyle IndexLabelStyle
        {
            get
            {
                if (s_indexLabelStyle == null)
                {
                    s_indexLabelStyle = new GUIStyle(EditorStyles.label);
                    s_indexLabelStyle.alignment = TextAnchor.MiddleLeft;
                }
                return s_indexLabelStyle;
            }
        }

        private static GUIStyle s_elementBackgroundStyle;
        public static GUIStyle ElementBackgroundStyle
        {
            get
            {
                if (s_elementBackgroundStyle == null)
                {
                    s_elementBackgroundStyle = new GUIStyle();
                    s_elementBackgroundStyle.normal.background = EditorGUIUtility.whiteTexture;
                    s_elementBackgroundStyle.alignment = TextAnchor.MiddleCenter;
                    s_elementBackgroundStyle.stretchWidth = true;
                    s_elementBackgroundStyle.stretchHeight = true;
                    s_elementBackgroundStyle.fixedWidth = 0;
                    s_elementBackgroundStyle.fixedHeight = 0;
                }
                return s_elementBackgroundStyle;
            }
        }

        private static GUIStyle s_separatorStyle;
        public static GUIStyle SeparatorStyle
        {
            get
            {
                if (s_separatorStyle == null)
                {
                    s_separatorStyle = new GUIStyle("RL Background");
                    s_separatorStyle.normal.background = ReorderableListSkin.Textures.Container2Background;
                    s_separatorStyle.onNormal.background = ReorderableListSkin.Textures.Container2Background;
                    s_separatorStyle.margin = new RectOffset(4, 4, 0, 0);
                    s_separatorStyle.border = new RectOffset(2, 2, 0, 0);
                    s_separatorStyle.padding = new RectOffset(2, 2, 0, 0);
                    s_separatorStyle.stretchWidth = true;
                    s_separatorStyle.stretchHeight = false;
                    s_separatorStyle.fixedWidth = 0;
                    s_separatorStyle.fixedHeight = 0;
                }
                return s_separatorStyle;
            }
        }

        private static GUIStyle s_whiteTextureStyle;
        public static GUIStyle WhiteTextureStyle
        {
            get
            {
                if (s_whiteTextureStyle == null)
                {
                    s_whiteTextureStyle = new GUIStyle();
                    s_whiteTextureStyle.normal.background = EditorGUIUtility.whiteTexture;
                    s_whiteTextureStyle.stretchWidth = true;
                    s_whiteTextureStyle.stretchHeight = true;
                    s_whiteTextureStyle.fixedWidth = 0;
                    s_whiteTextureStyle.fixedHeight = 0;
                }
                return s_whiteTextureStyle;
            }
        }

        /// <summary>
        /// Gets color for the horizontal lines that appear between list items.
        /// </summary>
        public static Color SeparatorColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color(1f, 1f, 1f, 0.14f) : new Color(0.59f, 0.59f, 0.59f, 0.55f);
            }
        }

        /// <summary>
        /// Gets background color for items.
        /// </summary>
        public static Color NormalBackgroundColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color(0.3f, 0.3f, 0.3f, 1f) : new Color(0.83f, 0.83f, 0.83f, 1f);
            }
        }

        /// <summary>
        /// Gets background color for selected items.
        /// </summary>
        public static Color SelectedBackgroundColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color(0.24f, 0.37f, 0.59f, 1f) : new Color(0.70f, 0.80f, 0.90f, 1f);
            }
        }

        /// <summary>
        /// Gets background color for selected items.
        /// </summary>
        public static Color SelectedTintColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color(0.64f, 0.77f, 0.99f, 1f) : new Color(0.70f, 0.80f, 0.90f, 1f);
            }
        }

        /// <summary>
        /// Gets background color for focused items.
        /// </summary>
        public static Color FocusedBackgroundColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color32(62, 62, 62, 255) : new Color32(62, 62, 62, 255);
            }
        }

        /// <summary>
        /// Background color of target box slot when dragging list item.
        /// </summary>
        public static Color BoxDragTargetBackgroundColor
        {
            get
            {
                return new Color(0, 0, 0, 0.5f);
            }
        }

        /// <summary>
        /// Background color of target separator when dragging list item.
        /// </summary>
        public static Color SeparatorDragTargetBackgroundColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? new Color(0.20f, 0.30f, 0.70f, 1f) : new Color(0.40f, 0.60f, 0.90f, 1f);
            }
        }

        private static int ButtonWidth
        {
            get
            {
                return 25;
            }
        }

        public static int FilterButtonWidth
        {
            get
            {
                return 27;
            }
        }

        public static int CreateButtonWidth
        {
            get
            {
                return 27;
            }
        }

        public static int DeleteButtonWidth
        {
            get
            {
                return 27;
            }
        }

        public static int DragHandleWidth
        {
            get
            {
                return 27;
            }
        }

        public static int SizeFieldWidth
        {
            get
            {
                return 32;
            }
        }

        public static int HeaderHeight
        {
            get
            {
                return 16;
            }
        }

        public static int ToolbarHeight
        {
            get
            {
                return 14;
            }
        }

        public static int FooterHeight
        {
            get
            {
                return 0;
            }
        }

        public static int ElementPadding
        {
            get
            {
                return 2;
            }
        }

        public static int ElementContentHeight
        {
            get
            {
                return 16;
            }
        }

        public static int SeparatorHeight
        {
            get
            {
                return 1;
            }
        }

        public static void Separator(Rect position)
        {
            Separator(position, SeparatorColor);
        }

        public static void Separator(Rect position, Color color)
        {
            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = color;
                WhiteTextureStyle.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        public static void DragTarget(Rect position, Color color)
        {
            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = color;
                WhiteTextureStyle.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        public static GUIStyle GetHeaderStyle(bool expanded)
        {
            GUIStyle headerStyle = HeaderStyle;
            if (!expanded)
            {
                headerStyle = HeaderCollapsedStyle;
            }
            return headerStyle;
        }
    }

}

#endif
