#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{

    public static class ReorderableListEditorGUI
    {
        public static void ListLineSeparator()
        {
            Rect position = EditorGUILayout.GetControlRect(GUILayout.Height(1f));
            ReorderableListStyles.Separator(position);
        }

        public static void ListLineSeparator(Color color)
        {
            Rect position = EditorGUILayout.GetControlRect(GUILayout.Height(1f));
            ReorderableListStyles.Separator(position, color);
        }

        public delegate T ItemDrawer<T>(T item);

        public static void DisplayList<T>(GUIContent title, List<T> list, ItemDrawer<T> itemDrawer)
        {
            using (new EditorGUILayout.VerticalScope(ReorderableListStyles.HeaderStyle))
            {
                EditorGUILayout.LabelField(title);
            }
            using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ContainerStyle))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (i != 0)
                    {
                        ListLineSeparator();
                    }
                    itemDrawer(list[i]);
                }
            }
        }
    }

}
#endif