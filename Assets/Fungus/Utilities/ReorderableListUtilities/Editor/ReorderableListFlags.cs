#if UNITY_EDITOR
using System;

namespace ImprobableStudios.ReorderableListUtilities
{

    [Flags]
    public enum ReorderableListFlags
    {
        /// <summary>
        /// Show zero-based index of array elements.
        /// </summary>
        ShowIndices = 0x000001,
        /// <summary>
        /// Show "Size" field in header.
        /// </summary>
        ShowSizeField = 0x000002,
        /// <summary>
        /// Hide header at top of list.
        /// </summary>
        HideHeader = 0x000004,
        /// <summary>
        /// Hide toolbar at top of list elements.
        /// </summary>
        HideToolbar = 0x000008,
        /// <summary>
        /// Hide "Search" field in toolbar.
        /// </summary>
        HideSearchField = 0x000010,
        /// <summary>
        /// Hide add button in toolbar.
        /// </summary>
        HideCreateButton = 0x000020,
        /// <summary>
        /// Hide delete buttons from list items.
        /// </summary>
        HideDeleteButtons = 0x00040,
        /// <summary>
        /// Hide drag handle next to each list element.
        /// </summary>
        HideDragHandles = 0x000080,
        /// <summary>
        /// Do not display context menu upon right-clicking grab handle.
        /// </summary>
        DisableContextMenu = 0x000100,
        /// <summary>
        /// Do not attempt to automatically scroll when list is inside a scroll view and
        /// the mouse pointer is dragged outside of the visible portion of the list.
        /// </summary>
        DisableAutoScroll = 0x000400,
        /// <summary>
        /// Don't optimize list by clipping elements that are out of view.
        /// </summary>
        DisableClipping = 0x000800,
        /// <summary>
        /// Hide grab handles and disable reordering of list items.
        /// </summary>
        DisableReorderingElements = 0x001000,
        /// <summary>
        /// Don't allow drag and dropping elements from other lists into this list
        /// </summary>
        DisableDropInserting = 0x002000,
        /// <summary>
        /// Don't allow adding elements to this list
        /// </summary>
        DisableCreatingElements = 0x004000,
        /// <summary>
        /// Don't allow deleting elements from this list
        /// </summary>
        DisableDeletingElements = 0x008000,
        /// <summary>
        /// Hide "Duplicate" option from context menu.
        /// </summary>
        DisableDuplicatingElements = 0x010000,
        /// <summary>
        /// Don't allow cutting, copying, or pasting elements from this list
        /// </summary>
        DisableCutCopyPastingElements = 0x020000,
        /// <summary>
        /// Don't allow the list to be collapsed.
        /// </summary>
        DisableCollapsingList = 0x040000,
        /// <summary>
        /// Don't allow elements in this list to be collapsed.
        /// </summary>
        DisableCollapsingElements = 0x080000,
        /// <summary>
        /// Don't show the list length.
        /// </summary>
        HideListLength = 0x100000,
        /// <summary>
        /// Show the list collapsed at first
        /// </summary>
        CollapseByDefault = 0x20000,
    }

}

#endif
