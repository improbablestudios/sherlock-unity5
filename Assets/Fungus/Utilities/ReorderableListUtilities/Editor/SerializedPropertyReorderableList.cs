#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SerializedPropertyUtilities;
using System.Collections.Generic;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class SerializedPropertyReorderableList : ReorderableList
    {
        public delegate void ElementDrawerDelegate(Rect position, SerializedProperty property, GUIContent label);

        public delegate float ElementHeightDelegate(SerializedProperty property, GUIContent label);

        public delegate bool ElementBooleanCondition(SerializedProperty property);

        public delegate int ElementIntegerCondition(SerializedProperty property);

        protected SerializedProperty m_listProperty;

        protected string m_listPropertyPath;

        protected ElementDrawerDelegate m_expandedElementDrawerCallback;

        protected ElementHeightDelegate m_expandedElementHeightCallback;

        protected ElementDrawerDelegate m_collapsedElementDrawerCallback;

        protected ElementHeightDelegate m_collapsedElementHeightCallback;

        protected ElementBooleanCondition m_isPropertyVisibleCallback;

        protected ElementBooleanCondition m_isPropertyEditableCallback;

        protected ElementIntegerCondition m_getPropertyIndentLevelOffsetCallback;

        private SerializedProperty[] m_cachedElements;

        private SerializedPropertyReorderableListCache m_reorderableListCache;

        /// <summary>
		/// Gets element from list.
		/// </summary>
		/// <param name="index">Zero-based index of element.</param>
		/// <returns>
		/// Serialized property wrapper for array element.
		/// </returns>
		public SerializedProperty this[int index]
        {
            get
            {
                return GetCachedElement(index);
            }
        }

        public SerializedProperty ListProperty
        {
            get
            {
                return m_listProperty;
            }
            set
            {
                Init(value);
            }
        }

        public ElementDrawerDelegate ExpandedElementDrawerCallback
        {
            get
            {
                return m_expandedElementDrawerCallback;
            }
            set
            {
                m_expandedElementDrawerCallback = value;
                m_reorderableListCache.ExpandedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate ExpandedElementHeightCallback
        {
            get
            {
                return m_expandedElementHeightCallback;
            }
            set
            {
                m_expandedElementHeightCallback = value;
                m_reorderableListCache.ExpandedElementHeightCallback = value;
            }
        }

        public ElementDrawerDelegate CollapsedElementDrawerCallback
        {
            get
            {
                return m_collapsedElementDrawerCallback;
            }
            set
            {
                m_collapsedElementDrawerCallback = value;
                m_reorderableListCache.CollapsedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate CollapsedElementHeightCallback
        {
            get
            {
                return m_collapsedElementHeightCallback;
            }
            set
            {
                m_collapsedElementHeightCallback = value;
                m_reorderableListCache.CollapsedElementHeightCallback = value;
            }
        }

        public ElementBooleanCondition IsPropertyVisibleCallback
        {
            get
            {
                return m_isPropertyVisibleCallback;
            }
            set
            {
                m_isPropertyVisibleCallback = value;
                m_reorderableListCache.IsPropertyVisibleCallback = value;
            }
        }

        public ElementBooleanCondition IsPropertyEditableCallback
        {
            get
            {
                return m_isPropertyEditableCallback;
            }
            set
            {
                m_isPropertyEditableCallback = value;
                m_reorderableListCache.IsPropertyEditableCallback = value;
            }
        }

        public ElementIntegerCondition GetPropertyIndentLevelOffsetCallback
        {
            get
            {
                return m_getPropertyIndentLevelOffsetCallback;
            }
            set
            {
                m_getPropertyIndentLevelOffsetCallback = value;
                m_reorderableListCache.GetPropertyIndentLevelOffsetCallback = value;
            }
        }

        public SerializedPropertyReorderableListCache ReorderableListCache
        {
            get
            {
                return m_reorderableListCache;
            }
            set
            {
                m_reorderableListCache = value;
            }
        }

        public SerializedPropertyReorderableList() : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache();
            SetFlags(0);
            Init();
        }

        public SerializedPropertyReorderableList(ReorderableListFlags flags) : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache(flags);
            SetFlags(flags);
            Init();
        }


        public SerializedPropertyReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache(
                elementDrawerCallback, elementHeightCallback);
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(flags);
            Init();
        }

        public SerializedPropertyReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache(
                elementDrawerCallback, elementHeightCallback);
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(0);
            Init();
        }

        public SerializedPropertyReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache(
                expandedElementDrawerCallback, expandedElementHeightCallback,
                collapsedElementDrawerCallback, collapsedElementHeightCallback);
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(0);
            Init();
        }

        public SerializedPropertyReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_reorderableListCache = new SerializedPropertyReorderableListCache(
                flags,
                expandedElementDrawerCallback, expandedElementHeightCallback,
                collapsedElementDrawerCallback, collapsedElementHeightCallback);
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(flags);
            Init();
        }
        
        public void Draw(SerializedProperty listProperty, params GUILayoutOption[] options)
        {
            Init(listProperty);
            base.Draw(options);
        }

        public void Draw(SerializedProperty listProperty, GUIContent label, params GUILayoutOption[] options)
        {
            Init(listProperty);
            base.Draw(label, options);
        }

        public void Draw(SerializedProperty listProperty, GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            Init(listProperty);
            base.Draw(guiStyle, options);
        }

        public void Draw(SerializedProperty listProperty, GUIContent label, GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            Init(listProperty);
            base.Draw(label, guiStyle, options);
        }
        
        public void Draw(Rect position, SerializedProperty listProperty)
        {
            Init(listProperty);
            base.Draw(position);
        }

        public void Draw(Rect position, SerializedProperty listProperty, GUIContent label)
        {
            Init(listProperty);
            base.Draw(position, label);
        }

        public float GetHeight(SerializedProperty listProperty, GUIContent label)
        {
            Init(listProperty);
            float height = GetHeight();
            return height;
        }

        public virtual void Init(SerializedProperty listProperty)
        {
            if (m_listProperty != listProperty)
            {
                if (listProperty == null)
                {
                    throw new ArgumentException(nameof(listProperty) + " cannot be null");
                }
                if (listProperty.serializedObject == null)
                {
                    throw new ArgumentException(nameof(listProperty) + " must come from a valid serialized object");
                }
                if (string.IsNullOrEmpty(listProperty.propertyPath))
                {
                    throw new ArgumentException(nameof(listProperty) + " SerializedProperty is invalid");
                }
                if (!listProperty.isArray)
                {
                    throw new ArgumentException(nameof(listProperty) + " must be an array or list");
                }
                m_listProperty = listProperty;
                m_listPropertyPath = listProperty.propertyPath;
                m_cachedElements = GetElements();
            }
        }

        private SerializedProperty[] GetElements()
        {
            SerializedProperty[] properties = null;
            if (m_listProperty != null)
            {
                int count = GetElementCount();
                properties = new SerializedProperty[count];
                for (int i = 0; i < count; i++)
                {
                    properties[i] = m_listProperty.GetArrayElementAtIndex(i);
                }
            }
            return properties;
        }

        private SerializedProperty GetCachedElement(int index)
        {
            if (m_cachedElements == null || index >= m_cachedElements.Length)
            {
                m_cachedElements = GetElements();
            }
            if (m_cachedElements != null)
            {
                if (index > -1 && index < m_cachedElements.Length)
                {
                    return m_cachedElements[index];
                }
            }
            return null;
        }

        protected void ApplyPropertyModifications()
        {
            if (m_listProperty.serializedObject.targetObject != null)
            {
                m_listProperty.serializedObject.ApplyModifiedProperties();
                m_listProperty.serializedObject.Update();
            }
            m_cachedElements = GetElements();
        }

        public override bool IsValid()
        {
            try
            {
                return m_listProperty != null && m_listProperty.serializedObject != null && m_listProperty.serializedObject.FindProperty(m_listPropertyPath) != null && !string.IsNullOrEmpty(m_listProperty.propertyPath) && m_listProperty.isArray;
            }
            catch
            {
                return false;
            }
        }

        public override int GetElementCount()
        {
            if (m_listProperty == null)
            {
                return 0;
            }
            if (m_listProperty.serializedObject != null)
            {
                if (!m_listProperty.hasMultipleDifferentValues)
                {
                    return m_listProperty.arraySize;
                }
                else
                {
                    int num = m_listProperty.arraySize;
                    UnityEngine.Object[] targetObjects = m_listProperty.serializedObject.targetObjects;
                    for (int i = 0; i < targetObjects.Length; i++)
                    {
                        UnityEngine.Object obj = targetObjects[i];
                        if (obj != null)
                        {
                            SerializedObject serializedObject = new SerializedObject(obj);
                            SerializedProperty serializedProperty = serializedObject.FindProperty(m_listProperty.propertyPath);
                            num = Math.Min(serializedProperty.arraySize, num);
                        }
                    }
                    return num;
                }
            }
            return 0;
        }

        public override Type GetElementType()
        {
            Type listType = m_listProperty.GetPropertyType();
            return listType.GetItemType();
        }

        public override object GetElementValue(int index)
        {
            return this[index].GetPropertyValue();
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            SerializedProperty elementProperty = this[index];

            if (elementProperty.hasVisibleChildren)
            {
                List<string> values = new List<string>();
                foreach (SerializedProperty childProperty in elementProperty.GetVisibleChildren(true))
                {
                    if (IsPropertyVisible(childProperty))
                    {
                        string valueString = GetPropertyIdentifier(childProperty);
                        if (valueString != null)
                        {
                            values.Add(valueString);
                        }
                    }
                }
                return values.ToArray();
            }
            else
            {
                object value = GetPropertyIdentifier(elementProperty);
                if (value != null)
                {
                    return new string[] { value.ToString() };
                }
            }
            return new string[0];
        }

        protected virtual string GetPropertyIdentifier(SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.ArraySize && !property.isArray)
            {
                object value = property.GetPropertyValue();
                if (value != null)
                {
                    string valueString = value.ToString();
                    if (value.GetType().IsPrimitive || valueString != value.GetType().ToString())
                    {
                        return valueString;
                    }
                }
            }
            return null;
        }
        protected override void Insert(int index, object element)
        {
            if (m_listProperty.arraySize == 0)
            {
                m_listProperty.arraySize++;
            }
            else
            {
                m_listProperty.InsertArrayElementAtIndex(index);
            }
            ApplyPropertyModifications();
            if (index < m_listProperty.arraySize)
            {
                m_listProperty.GetArrayElementAtIndex(index).SetPropertyValue(element);
                ApplyPropertyModifications();
            }
        }

        protected override object[] Copy(int[] indices)
        {
            object[] copies = new object[indices.Length];

            for (int i = 0; i < indices.Length; i++)
            {
                int indexToCopy = indices[i];
                
                SerializedProperty element = m_listProperty.GetArrayElementAtIndex(indexToCopy);

                object copy = element.GetPropertyValue();

                ICloneable existingItem = copy as ICloneable;
                if (existingItem != null)
                {
                    copy = existingItem.Clone();
                }

                copies[i] = copy;
            }
            
            return copies;
        }

        protected override void Paste(int index, object[] copyBuffer)
        {
            for (int i = 0; i < copyBuffer.Length; i++)
            {
                object obj = copyBuffer[i];
                Insert(i, obj);
            }
        }
        
        protected override void Delete(int[] indices)
        {
            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];

                SerializedProperty elementProperty = m_listProperty.GetArrayElementAtIndex(index);
                if (elementProperty.propertyType == SerializedPropertyType.ObjectReference)
                {
                    elementProperty.objectReferenceValue = null;
                }
                m_listProperty.DeleteArrayElementAtIndex(index);
            }
            ApplyPropertyModifications();
        }

        protected override void Move(int srcIndex, int dstIndex)
        {
            m_listProperty.MoveArrayElement(srcIndex, dstIndex);
            ApplyPropertyModifications();
        }
        
        protected override void OnChanged()
        {
            ApplyPropertyModifications();
        }

        protected override void OnBeginHeaderGUI(Rect position, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, m_listProperty);
        }

        protected override void OnEndHeaderGUI(Rect position, GUIContent label)
        {
            EditorGUI.EndProperty();
        }
        
        protected override void OnEndElementsGUI(Rect position)
        {
            ApplyPropertyModifications();
        }

        public override GUIContent GetDefaultHeaderLabel()
        {
            return new GUIContent(m_listProperty.displayName, m_listProperty.tooltip);
        }

        public override bool CanCollapse(int index)
        {
            SerializedProperty elementProperty = this[index];

            if (elementProperty.hasVisibleChildren && !elementProperty.HasPropertyDrawer())
            {
                int propertyCount = 0;
                SerializedProperty iterator = elementProperty.Copy();
                SerializedProperty endProperty = iterator.GetEndProperty();
                bool enterChildren = iterator.hasVisibleChildren;
                while (iterator.NextVisible(enterChildren) && !SerializedProperty.EqualContents(iterator, endProperty))
                {
                    propertyCount += 1;
                    if (propertyCount > 1)
                    {
                        return true;
                    }
                    if (iterator.isArray && iterator.propertyType != SerializedPropertyType.String)
                    {
                        enterChildren = false;
                    }
                    else
                    {
                        enterChildren = iterator.isExpanded && iterator.hasVisibleChildren;
                    }
                }
            }

            return false;
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            SerializedProperty elementProperty = this[index];

            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = GetChildLabelWidth(position);

            if (m_expandedElementDrawerCallback != null)
            {
                m_expandedElementDrawerCallback(position, elementProperty, GUIContent.none);
            }
            else
            {
                if (!elementProperty.hasVisibleChildren || elementProperty.HasPropertyDrawer())
                {
                    EditorGUI.PropertyField(position, elementProperty, GUIContent.none, true);
                }
                else
                {
                    ExtendedEditorGUI.DrawVisibleChildren(
                        position,
                        elementProperty,
                        IsPropertyVisible,
                        IsPropertyEditable,
                        GetPropertyIndentLevelOffset,
                        ChildPropertyField,
                        GetChildPropertyHeight);
                }
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected override void DrawElementCollapsedContent(Rect position, int index)
        {
            SerializedProperty elementProperty = this[index];

            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = GetChildLabelWidth(position);

            if (m_collapsedElementDrawerCallback != null)
            {
                m_collapsedElementDrawerCallback(position, elementProperty, GUIContent.none);
            }
            else
            {
                if (!elementProperty.hasVisibleChildren || elementProperty.HasPropertyDrawer())
                {
                    EditorGUI.PropertyField(position, elementProperty, GUIContent.none, true);
                }
                else
                {
                    ExtendedEditorGUI.DrawVisibleChildren(
                        position, 
                        elementProperty,
                        IsPropertyVisible,
                        IsPropertyEditable,
                        GetPropertyIndentLevelOffset,
                        ChildPropertyField, 
                        GetChildPropertyHeight,
                        1);
                }
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            SerializedProperty elementProperty = this[index];

            if (m_expandedElementHeightCallback != null)
            {
                return m_expandedElementHeightCallback(elementProperty, GUIContent.none);
            }
            else
            {
                if (!elementProperty.hasVisibleChildren || elementProperty.HasPropertyDrawer())
                {
                    return EditorGUI.GetPropertyHeight(elementProperty, GUIContent.none, true);
                }
                else
                {
                    return ExtendedEditorGUI.GetVisibleChildrenHeight(
                        elementProperty,
                        IsPropertyVisible,
                        GetChildPropertyHeight);
                }
            }
        }

        protected override float GetElementCollapsedContentHeight(int index)
        {
            SerializedProperty elementProperty = this[index];

            if (m_collapsedElementHeightCallback != null)
            {
                return m_collapsedElementHeightCallback(elementProperty, GUIContent.none);
            }
            else
            {
                if (!elementProperty.hasVisibleChildren || elementProperty.HasPropertyDrawer())
                {
                    return EditorGUI.GetPropertyHeight(elementProperty, GUIContent.none, true);
                }
                else
                {
                    return ExtendedEditorGUI.GetVisibleChildrenHeight(
                        elementProperty,
                        IsPropertyVisible,
                        GetChildPropertyHeight,
                        1);
                }
            }
        }

        protected virtual float GetChildLabelWidth(Rect position)
        {
            return Mathf.Max((float)((double)position.width * 0.4 - 40.0), 50f);
        }

        public virtual bool IsPropertyVisible(SerializedProperty property)
        {
            if (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal))
            {
                return false;
            }
            if (IsPropertyVisibleCallback != null)
            {
                return IsPropertyVisibleCallback(property);
            }
            return true;
        }

        public virtual bool IsPropertyEditable(SerializedProperty property)
        {
            if (IsPropertyEditableCallback != null)
            {
                return IsPropertyEditableCallback(property);
            }
            return true;
        }

        public virtual int GetPropertyIndentLevelOffset(SerializedProperty property)
        {
            if (GetPropertyIndentLevelOffsetCallback != null)
            {
                return GetPropertyIndentLevelOffsetCallback(property);
            }
            return 0;
        }

        public virtual bool ChildPropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal))
            {
                return false;
            }
            else if (property.isArray && property.propertyType != SerializedPropertyType.String)
            {
                SerializedPropertyReorderableList reorderableList = m_reorderableListCache[property];
                position.height = reorderableList.GetHeight(property, label);
                reorderableList.Draw(position, property, label);
                return false;
            }
            else
            {
                return EditorGUI.PropertyField(position, property, label, false);
            }
        }

        public virtual float GetChildPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren)
        {
            if (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal))
            {
                return 0f;
            }
            else if (property.isArray && property.propertyType != SerializedPropertyType.String)
            {
                SerializedPropertyReorderableList reorderableList = m_reorderableListCache[property];
                return reorderableList.GetHeight(property, label);
            }
            else
            {
                return EditorGUI.GetPropertyHeight(property, label, false);
            }
        }
    }
}
#endif
