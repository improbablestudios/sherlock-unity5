#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.ReorderableListUtilities
{
    public class ReorderableListSkin : ScriptableObject
    {
        private static ReorderableListSkin s_instance;
        private static SkinTextures s_textures;

        /// <summary>
        /// Gets the name of the one-and-only <see cref="ReorderableListSkin"/> instance.
        /// </summary>
        public static string SkinName
        {
            get
            {
                return typeof(ReorderableListSkin).Name;
            }
        }

        /// <summary>
        /// Gets the one-and-only <see cref="ReorderableListSkin"/> instance.
        /// </summary>
        public static ReorderableListSkin Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = Resources.Load<ReorderableListSkin>("Editor/~" + SkinName);
                }
                return s_instance;
            }
        }

        /// <summary>
        /// Gets the current skin.
        /// </summary>
        public static SkinTextures Textures
        {
            get
            {
                if (s_textures == null)
                {
                    s_textures = EditorGUIUtility.isProSkin ? Instance.m_DarkSkin : Instance.m_LightSkin;
                }
                return s_textures;
            }
        }

        [SerializeField]
        protected SkinTextures m_DarkSkin = new SkinTextures();

        [SerializeField]
        protected SkinTextures m_LightSkin = new SkinTextures();

        [System.Serializable]
        public class SkinTextures
        {
            [SerializeField]
            protected Texture2D m_Button_Active = null;

            [SerializeField]
            protected Texture2D m_Button_Normal = null;

            [SerializeField]
            protected Texture2D m_Button2_Active = null;

            [SerializeField]
            protected Texture2D m_Button2_Normal = null;

            [SerializeField]
            protected Texture2D m_Container2Background = null;

            [SerializeField]
            protected Texture2D m_ContainerBackground = null;

            [SerializeField]
            protected Texture2D m_GrabHandle = null;

            [SerializeField]
            protected Texture2D m_Icon_Create = null;

            [SerializeField]
            protected Texture2D m_Icon_CreateMenu = null;

            [SerializeField]
            protected Texture2D m_Icon_Menu = null;

            [SerializeField]
            protected Texture2D m_Icon_Delete = null;

            [SerializeField]
            protected Texture2D m_HeaderButton_Active = null;

            [SerializeField]
            protected Texture2D m_ElementButton_Active = null;

            [SerializeField]
            protected Texture2D m_SelectionBackground = null;

            [SerializeField]
            protected Texture2D m_HeaderBackground = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundLeft = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundMiddle = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundRight = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundCollapsed = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundLeftCollapsed = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundMiddleCollapsed = null;

            [SerializeField]
            protected Texture2D m_HeaderBackgroundRightCollapsed = null;

            public Texture2D Button_Active
            {
                get
                {
                    return m_Button_Active;
                }
            }

            public Texture2D Button_Normal
            {
                get
                {
                    return m_Button_Normal;
                }
            }

            public Texture2D Button2_Active
            {
                get
                {
                    return m_Button2_Active;
                }
            }

            public Texture2D Button2_Normal
            {
                get
                {
                    return m_Button2_Normal;
                }
            }

            public Texture2D Container2Background
            {
                get
                {
                    return m_Container2Background;
                }
            }

            public Texture2D ContainerBackground
            {
                get
                {
                    return m_ContainerBackground;
                }
            }

            public Texture2D GrabHandle
            {
                get
                {
                    return m_GrabHandle;
                }
            }

            public Texture2D Icon_Create
            {
                get
                {
                    return m_Icon_Create;
                }
            }

            public Texture2D Icon_CreateMenu
            {
                get
                {
                    return m_Icon_CreateMenu;
                }
            }

            public Texture2D Icon_Menu
            {
                get
                {
                    return m_Icon_Menu;
                }
            }

            public Texture2D Icon_Delete
            {
                get
                {
                    return m_Icon_Delete;
                }
            }

            public Texture2D HeaderButton_Active
            {
                get
                {
                    return m_HeaderButton_Active;
                }
            }

            public Texture2D ElementButton_Active
            {
                get
                {
                    return m_ElementButton_Active;
                }
            }

            public Texture2D SelectionBackground
            {
                get
                {
                    return m_SelectionBackground;
                }
            }

            public Texture2D HeaderBackground
            {
                get
                {
                    return m_HeaderBackground;
                }
            }

            public Texture2D HeaderBackgroundLeft
            {
                get
                {
                    return m_HeaderBackgroundLeft;
                }
            }

            public Texture2D HeaderBackgroundMiddle
            {
                get
                {
                    return m_HeaderBackgroundMiddle;
                }
            }

            public Texture2D HeaderBackgroundRight
            {
                get
                {
                    return m_HeaderBackgroundRight;
                }
            }

            public Texture2D HeaderBackgroundCollapsed
            {
                get
                {
                    return m_HeaderBackgroundCollapsed;
                }
            }

            public Texture2D HeaderBackgroundLeftCollapsed
            {
                get
                {
                    return m_HeaderBackgroundLeftCollapsed;
                }
            }

            public Texture2D HeaderBackgroundMiddleCollapsed
            {
                get
                {
                    return m_HeaderBackgroundMiddleCollapsed;
                }
            }

            public Texture2D HeaderBackgroundRightCollapsed
            {
                get
                {
                    return m_HeaderBackgroundRightCollapsed;
                }
            }
        }
    }
}

#endif