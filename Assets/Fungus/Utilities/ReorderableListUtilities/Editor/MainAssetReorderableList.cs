﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class MainAssetReorderableList : AssetReorderableList
    {
        public MainAssetReorderableList() : base()
        {
        }

        public MainAssetReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public MainAssetReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public MainAssetReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public MainAssetReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override object Create()
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            UnityEngine.Object mainAsset = ScriptableObject.CreateInstance(GetElementType());

            if (mainAsset != null)
            {
                string path = GetAssetSavePath();
                if (!string.IsNullOrEmpty(path))
                {
                    AssetDatabase.CreateAsset(mainAsset, path);
                }
            }

            return mainAsset;
        }

        protected override void Insert(int index)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            UnityEngine.Object mainAsset = ScriptableObject.CreateInstance(GetElementType());

            if (mainAsset != null)
            {
                string path = GetAssetSavePath();
                if (!string.IsNullOrEmpty(path))
                {
                    AssetDatabase.CreateAsset(mainAsset, path);
                    base.Insert(index, mainAsset);
                }
            }
        }

        protected override void Delete(int[] indices)
        {
            UnityEngine.Object target = m_listProperty.serializedObject.targetObject as UnityEngine.Object;

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                UnityEngine.Object mainAsset = this[index].objectReferenceValue as UnityEngine.Object;
                if (mainAsset != null)
                {
                    string path = AssetDatabase.GetAssetPath(mainAsset);
                    if (!string.IsNullOrEmpty(path))
                    {
                        if (EditorUtility.DisplayDialog("Delete Asset?", path + "\n\n" + "Delete this asset after removing it from the list?" + "\n\n" + "Deleting an asset cannot be undone.", "Yes", "No"))
                        {
                            AssetDatabase.DeleteAsset(path);
                        }
                    }
                }
            }

            base.Delete(indices);
        }
    }

}
#endif