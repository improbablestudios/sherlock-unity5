#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using static ImprobableStudios.ReorderableListUtilities.SerializedPropertyReorderableList;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class SerializedPropertyReorderableListCache
    {
        protected Dictionary<string, SerializedPropertyReorderableList> m_reorderableListCache = new Dictionary<string, SerializedPropertyReorderableList>();

        protected ReorderableListFlags m_flags = 0;

        protected ElementDrawerDelegate m_expandedElementDrawerCallback;

        protected ElementHeightDelegate m_expandedElementHeightCallback;

        protected ElementDrawerDelegate m_collapsedElementDrawerCallback;

        protected ElementHeightDelegate m_collapsedElementHeightCallback;

        protected ElementBooleanCondition m_isPropertyVisibleCallback;

        protected ElementBooleanCondition m_isPropertyEditableCallback;

        protected ElementIntegerCondition m_getPropertyIndentLevelOffsetCallback;

        public ElementDrawerDelegate ExpandedElementDrawerCallback
        {
            get
            {
                return m_expandedElementDrawerCallback;
            }
            set
            {
                m_expandedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate ExpandedElementHeightCallback
        {
            get
            {
                return m_expandedElementHeightCallback;
            }
            set
            {
                m_expandedElementHeightCallback = value;
            }
        }

        public ElementDrawerDelegate CollapsedElementDrawerCallback
        {
            get
            {
                return m_collapsedElementDrawerCallback;
            }
            set
            {
                m_collapsedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate CollapsedElementHeightCallback
        {
            get
            {
                return m_collapsedElementHeightCallback;
            }
            set
            {
                m_collapsedElementHeightCallback = value;
            }
        }

        public ElementBooleanCondition IsPropertyVisibleCallback
        {
            get
            {
                return m_isPropertyVisibleCallback;
            }
            set
            {
                m_isPropertyVisibleCallback = value;
            }
        }

        public ElementBooleanCondition IsPropertyEditableCallback
        {
            get
            {
                return m_isPropertyEditableCallback;
            }
            set
            {
                m_isPropertyEditableCallback = value;
            }
        }

        public ElementIntegerCondition GetPropertyIndentLevelOffsetCallback
        {
            get
            {
                return m_getPropertyIndentLevelOffsetCallback;
            }
            set
            {
                m_getPropertyIndentLevelOffsetCallback = value;
            }
        }

        public SerializedPropertyReorderableListCache()
        {
        }

        public SerializedPropertyReorderableListCache(ReorderableListFlags flags)
        {
            SetFlags(flags);
        }
        
        public SerializedPropertyReorderableListCache(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(0);
        }

        public SerializedPropertyReorderableListCache(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(0);
        }

        public SerializedPropertyReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(flags);
        }

        public void SetFlags(ReorderableListFlags flags)
        {
            m_flags = flags;
        }

        public SerializedPropertyReorderableList this[SerializedProperty property]
        {
            get
            {
                string key = GetKey(property);
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                    m_reorderableListCache[key].Init(property);
                }
                return m_reorderableListCache[key];
            }
            set
            {
                string key = GetKey(property);
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = value;
                    m_reorderableListCache[key].Init(property);
                }
            }
        }

        public SerializedPropertyReorderableList this[string key]
        {
            get
            {
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
                return m_reorderableListCache[key];
            }
            set
            {
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = value;
                }
            }
        }

        public SerializedPropertyReorderableList[] ReorderableLists
        {
            get
            {
                return m_reorderableListCache.Values.ToArray();
            }
        }
        
        public bool ContainsKey(SerializedProperty property)
        {
            return (m_reorderableListCache.ContainsKey(GetKey(property)));
        }

        protected string GetKey(SerializedProperty property)
        {
            string key = "";
            foreach (UnityEngine.Object targetObject in property.serializedObject.targetObjects)
            {
                if (targetObject != null)
                {
                    key += targetObject.GetHashCode() + "+";
                }
            }
            key += property.propertyPath;
            return key;
        }
        
        protected virtual SerializedPropertyReorderableList CreateReorderableList()
        {
            SerializedPropertyReorderableList reorderableList =
                new SerializedPropertyReorderableList(
                    m_flags,
                    m_expandedElementDrawerCallback, m_expandedElementHeightCallback,
                    m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback);
            reorderableList.IsPropertyVisibleCallback = m_isPropertyVisibleCallback;
            reorderableList.IsPropertyEditableCallback = m_isPropertyEditableCallback;
            reorderableList.GetPropertyIndentLevelOffsetCallback = m_getPropertyIndentLevelOffsetCallback;
            return reorderableList;
        }
    }

    public class SerializedPropertyReorderableListCache<T> : SerializedPropertyReorderableListCache where T : SerializedPropertyReorderableList
    {
        public SerializedPropertyReorderableListCache() : base()
        {
        }

        public SerializedPropertyReorderableListCache(ReorderableListFlags flags) : base(flags)
        {
        }

        public SerializedPropertyReorderableListCache(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public SerializedPropertyReorderableListCache(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                  collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public SerializedPropertyReorderableListCache(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(flags,
                  expandedElementDrawerCallback, expandedElementHeightCallback,
                  collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public new T this[SerializedProperty property]
        {
            get
            {
                string key = GetKey(property);
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                    m_reorderableListCache[key].Init(property);
                }
                return m_reorderableListCache[key] as T;
            }
            set
            {
                string key = GetKey(property);
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = value;
                    m_reorderableListCache[key].Init(property);
                }
            }
        }

        public new T this[string key]
        {
            get
            {
                if (!m_reorderableListCache.ContainsKey(key))
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
                return m_reorderableListCache[key] as T;
            }
            set
            {
                if (!m_reorderableListCache.ContainsKey(key) || m_reorderableListCache[key] != value)
                {
                    m_reorderableListCache[key] = CreateReorderableList();
                }
            }
        }

        public new T[] ReorderableLists
        {
            get
            {
                return m_reorderableListCache.Values.Cast<T>().ToArray();
            }
        }

        protected override SerializedPropertyReorderableList CreateReorderableList()
        {
            SerializedPropertyReorderableList reorderableList = 
                Activator.CreateInstance(typeof(T),
                    new object[] {
                        m_flags,
                        m_expandedElementDrawerCallback, m_expandedElementHeightCallback,
                        m_collapsedElementDrawerCallback, m_collapsedElementHeightCallback})
                        as T;
            return reorderableList;
        }
    }

}
#endif