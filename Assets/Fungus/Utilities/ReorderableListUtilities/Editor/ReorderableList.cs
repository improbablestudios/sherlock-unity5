#if UNITY_EDITOR
using ImprobableStudios.UnityEditorUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.ReorderableListUtilities
{

    public abstract class ReorderableList
    {
        public enum SearchType
        {
            Focus,
            Filter,
        }

        public struct ReorderableListCommandOperation
        {
            public string CommandName
            {
                get;
                set;
            }

            public int[] Indices
            {
                get;
                set;
            }
        }

        protected static List<ReorderableList> s_reorderableLists = new List<ReorderableList>();
        
        public delegate void OnSelectDelegate(int[] indices);

        public delegate string GetElementIndexLabelTextDelegate(int index);

        private static List<object> s_copyBuffer = new List<object>();

        private static bool s_clearCopyBufferOnNextPaste;

        private static int s_simulateMouseDragControlID;

        private static ReorderableList s_dragAndDropSourceList;

        private static ReorderableList s_dragAndDropTargetList;

        private static int s_dragAndDropTargetIndex;

        private static bool s_performedDrag;
        
        protected static readonly int s_controlIDHash = "s_SearchPopupWindowHash".GetHashCode();

        /// <summary>
        /// Content for "Move to Top" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandMoveToTop = new GUIContent("Move to Top");

        /// <summary>
        /// Content for "Move to Bottom" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandMoveToBottom = new GUIContent("Move to Bottom");

        /// <summary>
        /// Content for "Insert Above" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandInsertAbove = new GUIContent("Insert Above");

        /// <summary>
        /// Content for "Insert Below" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandInsertBelow = new GUIContent("Insert Below");

        /// <summary>
        /// Content for "Cut" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandCut = new GUIContent("Cut");

        /// <summary>
        /// Content for "Copy" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandCopy = new GUIContent("Copy");

        /// <summary>
        /// Content for "Paste" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandPaste = new GUIContent("Paste");

        /// <summary>
        /// Content for "Duplicate" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandDuplicate = new GUIContent("Duplicate");

        /// <summary>
        /// Content for "Delete" keyboard command.
        /// </summary>
        protected static readonly string s_commandSoftDelete = "SoftDelete";

        /// <summary>
        /// Content for "Delete" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandDelete = new GUIContent("Delete");

        /// <summary>
        /// Content for "Delete All" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandDeleteAll = new GUIContent("Delete All");

        /// <summary>
        /// Content for "Select All" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandSelectAll = new GUIContent("Select All");

        /// <summary>
        /// Content for "Select None" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandSelectNone = new GUIContent("Select None");

        /// <summary>
        /// Content for "Expand All" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandExpandAll = new GUIContent("Expand All");

        /// <summary>
        /// Content for "Collapse All" context menu command.
        /// </summary>
        protected static readonly GUIContent s_commandCollapseAll = new GUIContent("Collapse All");

        protected GetElementIndexLabelTextDelegate m_getElementIndexLabelTextCallback;
        
        protected GUIStyle m_headerStyle;

        protected GUIStyle m_headerLeftStyle;

        protected GUIStyle m_headerRightStyle;

        protected GUIStyle m_headerCollapsedStyle;

        protected GUIStyle m_headerLeftCollapsedStyle;

        protected GUIStyle m_headerRightCollapsedStyle;

        protected GUIStyle m_toolbarStyle;

        protected GUIStyle m_separatorStyle;

        protected GUIStyle m_containerStyle;
        
        protected string m_searchControlName;

        private event OnSelectDelegate m_onSelectCallback = delegate { };

        protected ReorderableListFlags m_flags;

        private bool m_willDisplaySizeField;

        private bool m_willDisplayHeader;

        private bool m_willDisplayToolbar;

        private bool m_willDisplaySearchField;

        private bool m_willDisplayCreateButton = true;

        private bool m_willDisplayDeleteButtons = true;

        private bool m_willDisplayDragHandles = true;

        private bool m_willDisplayIndices;

        private bool m_willDisplayContextMenu = true;

        private bool m_willDisplayListLength = true;
        
        private bool m_willAutoScroll = true;

        private bool m_willClip = true;

        private bool m_allowReorderingElements = true;

        private bool m_allowDropInsertingElements = true;

        private bool m_allowCreatingElements = true;

        private bool m_allowDeletingElements = true;

        private bool m_allowDuplicatingElements = true;

        private bool m_allowCutCopyPastingElements = true;

        private bool m_allowCollapsingList = true;

        private bool m_allowCollapsingElements = true;

        private bool m_expandByDefault = true;

        protected bool m_expanded = true;

        protected Rect m_headerRect;

        protected Rect m_headerContentRect;

        protected Rect m_toolbarContentRect;

        protected Rect m_toolbarRect;

        protected Rect m_elementsRect;

        protected Rect m_elementsContentRect;

        protected bool[] m_elementSearchStates;

        protected string[][] m_elementIdentifiers;

        protected int m_controlID = -1;

        protected Vector2 m_scrollPosition;

        protected string m_searchString;

        protected bool m_searchIsFocused;

        protected bool m_isDragging;

        protected Rect m_visibleRect;

        protected SearchType m_searchType;

        protected int m_createIndex = -1;

        private int m_dragTargetIndex = -1;

        private float m_dragOffset = 0f;

        private float m_draggedY;

        private List<int> m_nonDragTargetIndices = new List<int>();
        
        private SortedList<int, int> m_selectedIndices = new SortedList<int, int>();

        private int m_firstSelectedIndex = -1;

        private bool m_clickedAlreadySelectedElementOnMouseDown;

        private int m_focusIndex = -1;

        private int[] m_scrollIndices = null;

        private int[] m_selectedIndicesThisFrame = new int[0];

        private int m_mouseUpIndexThisFrame = -1;

        private bool m_refresh;

        private bool m_changedThisFrame;

        private bool m_showCreateMenuNextFrame;

        private bool m_showContextMenuNextFrame = false;

        private bool[] m_cachedElementIsExpandedStates;

        private bool[] m_cachedElementCanCollapseStates;

        private float[] m_cachedElementExpandedContentHeights;

        private float[] m_cachedElementCollapsedContentHeights;

        public static int ControlIDHash
        {
            get
            {
                return s_controlIDHash;
            }
        }

        public GUIStyle HeaderStyle
        {
            get
            {
                if (m_headerStyle == null)
                {
                    m_headerStyle = ReorderableListStyles.HeaderStyle;
                }
                return m_headerStyle;
            }
            set
            {
                m_headerStyle = value;
            }
        }

        public GUIStyle HeaderLeftStyle
        {
            get
            {
                if (m_headerLeftStyle == null)
                {
                    m_headerLeftStyle = ReorderableListStyles.HeaderLeftStyle;
                }
                return m_headerLeftStyle;
            }
            set
            {
                m_headerLeftStyle = value;
            }
        }

        public GUIStyle HeaderRightStyle
        {
            get
            {
                if (m_headerRightStyle == null)
                {
                    m_headerRightStyle = ReorderableListStyles.HeaderRightStyle;
                }
                return m_headerRightStyle;
            }
            set
            {
                m_headerRightStyle = value;
            }
        }

        public GUIStyle HeaderCollapsedStyle
        {
            get
            {
                if (m_headerCollapsedStyle == null)
                {
                    m_headerCollapsedStyle = ReorderableListStyles.HeaderCollapsedStyle;
                }
                return m_headerCollapsedStyle;
            }
            set
            {
                m_headerCollapsedStyle = value;
            }
        }

        public GUIStyle HeaderLeftCollapsedStyle
        {
            get
            {
                if (m_headerLeftCollapsedStyle == null)
                {
                    m_headerLeftCollapsedStyle = ReorderableListStyles.HeaderLeftCollapsedStyle;
                }
                return m_headerLeftCollapsedStyle;
            }
            set
            {
                m_headerLeftCollapsedStyle = value;
            }
        }

        public GUIStyle HeaderRightCollapsedStyle
        {
            get
            {
                if (m_headerRightCollapsedStyle == null)
                {
                    m_headerRightCollapsedStyle = ReorderableListStyles.HeaderRightCollapsedStyle;
                }
                return m_headerRightCollapsedStyle;
            }
            set
            {
                m_headerRightCollapsedStyle = value;
            }
        }

        public GUIStyle ToolbarStyle
        {
            get
            {
                if (m_toolbarStyle == null)
                {
                    m_toolbarStyle = ReorderableListStyles.ToolbarStyle;
                }
                return m_toolbarStyle;
            }
            set
            {
                m_toolbarStyle = value;
            }
        }

        public GUIStyle SeparatorStyle
        {
            get
            {
                if (m_separatorStyle == null)
                {
                    m_separatorStyle = ReorderableListStyles.SeparatorStyle;
                }
                return m_separatorStyle;
            }
            set
            {
                m_separatorStyle = value;
            }
        }

        public GUIStyle ContainerStyle
        {
            get
            {
                if (m_containerStyle == null)
                {
                    m_containerStyle = ReorderableListStyles.ContainerStyle;
                }
                return m_containerStyle;
            }
            set
            {
                m_containerStyle = value;
            }
        }

        public OnSelectDelegate OnSelectCallback
        {
            get
            {
                return m_onSelectCallback;
            }
        }

        public GetElementIndexLabelTextDelegate GetElementIndexLabelTextCallback
        {
            get
            {
                return m_getElementIndexLabelTextCallback;
            }
            set
            {
                m_getElementIndexLabelTextCallback = value;
            }
        }

        public ReorderableListFlags Flags
        {
            get
            {
                return m_flags;
            }
            set
            {
                SetFlags(value);
            }
        }

        public bool WillDisplayIndices
        {
            get
            {
                return m_willDisplayIndices;
            }
            set
            {
                m_willDisplayIndices = value;
            }
        }

        public bool WillDisplaySizeField
        {
            get
            {
                return m_willDisplaySizeField;
            }
            set
            {
                m_willDisplaySizeField = value;
            }
        }

        public bool WillDisplayHeader
        {
            get
            {
                return m_willDisplayHeader;
            }
            set
            {
                m_willDisplayHeader = value;
            }
        }

        public bool WillDisplayToolbar
        {
            get
            {
                return m_willDisplayToolbar;
            }
            set
            {
                m_willDisplayToolbar = value;
            }
        }

        public bool WillDisplaySearchField
        {
            get
            {
                return m_willDisplaySearchField;
            }
            set
            {
                m_willDisplaySearchField = value;
            }
        }

        public bool WillDisplayCreateButton
        {
            get
            {
                return m_willDisplayCreateButton;
            }
            set
            {
                m_willDisplayCreateButton = value;
            }
        }

        public bool WillDisplayDeleteButtons
        {
            get
            {
                return m_willDisplayDeleteButtons;
            }
            set
            {
                m_willDisplayDeleteButtons = value;
            }
        }

        public bool WillDisplayDragHandles
        {
            get
            {
                return m_willDisplayDragHandles;
            }
            set
            {
                m_willDisplayDragHandles = value;
            }
        }

        public bool WillDisplayContextMenu
        {
            get
            {
                return m_willDisplayContextMenu;
            }
            set
            {
                m_willDisplayContextMenu = value;
            }
        }
        
        public bool WillAutoScroll
        {
            get
            {
                return m_willAutoScroll;
            }
            set
            {
                m_willAutoScroll = value;
            }
        }

        public bool WillClip
        {
            get
            {
                return m_willClip;
            }
            set
            {
                m_willClip = value;
            }
        }

        public bool AllowReorderingElements
        {
            get
            {
                return m_allowReorderingElements;
            }
            set
            {
                m_allowReorderingElements = value;
            }
        }

        public bool AllowDropInsertingElements
        {
            get
            {
                return m_allowDropInsertingElements;
            }
            set
            {
                m_allowDropInsertingElements = value;
            }
        }

        public bool AllowCreatingElements
        {
            get
            {
                return m_allowCreatingElements;
            }
            set
            {
                m_allowCreatingElements = value;
            }
        }

        public bool AllowRemovingElements
        {
            get
            {
                return m_allowDeletingElements;
            }
            set
            {
                m_allowDeletingElements = value;
            }
        }

        public bool AllowDuplicatingElements
        {
            get
            {
                return m_allowDuplicatingElements;
            }
            set
            {
                m_allowDuplicatingElements = value;
            }
        }

        public bool AllowCutCopyPastingElements
        {
            get
            {
                return m_allowCutCopyPastingElements;
            }
            set
            {
                m_allowCutCopyPastingElements = value;
            }
        }

        public bool AllowCollapsingList
        {
            get
            {
                return m_allowCollapsingList;
            }
            set
            {
                m_allowCollapsingList = value;
            }
        }

        public bool AllowCollapsingElements
        {
            get
            {
                return m_allowCollapsingElements;
            }
            set
            {
                m_allowCollapsingElements = value;
            }
        }

        public bool WillDisplayListLength
        {
            get
            {
                return m_willDisplayListLength;
            }
            set
            {
                m_willDisplayListLength = value;
            }
        }

        public bool ExpandByDefault
        {
            get
            {
                return m_expandByDefault;
            }
            set
            {
                m_expandByDefault = value;
            }
        }

        public bool Expanded
        {
            get
            {
                return m_expanded;
            }
            set
            {
                m_expanded = value;
            }
        }

        public int ControlID
        {
            get
            {
                return m_controlID;
            }
        }

        public string SearchControlName
        {
            get
            {
                return m_searchControlName;
            }
        }

        public Vector2 ScrollPosition
        {
            get
            {
                return m_scrollPosition;
            }
            set
            {
                m_scrollPosition = value;
            }
        }

        public int[] SelectedIndices
        {
            get
            {
                return GetSelectedIndices();
            }
            set
            {
                DoSelect(value, true);
            }
        }

        public Rect HeaderRect
        {
            get
            {
                return m_headerRect;
            }
        }

        public Rect HeaderContentRect
        {
            get
            {
                return m_headerContentRect;
            }
        }

        public Rect ToolbarRect
        {
            get
            {
                return m_toolbarRect;
            }
        }

        public Rect ToolbarContentRect
        {
            get
            {
                return m_toolbarContentRect;
            }
        }

        public Rect ElementsRect
        {
            get
            {
                return m_elementsRect;
            }
        }

        public Rect ElementsContentRect
        {
            get
            {
                return m_elementsContentRect;
            }
        }

        public ReorderableList()
        {
            EditorApplication.update += Update;
            s_reorderableLists.Add(this);
        }

        ~ReorderableList()
        {
            EditorApplication.update -= Update;
            s_reorderableLists.Remove(this);
        }

        public void Init()
        {
            m_controlID = GUIUtility.GetControlID(s_controlIDHash + GetHashCode(), FocusType.Keyboard);
            m_isDragging = false;
            m_expanded = m_expandByDefault;
            m_searchControlName = GetSearchControlName();
        }
        
        protected void SetFlags(ReorderableListFlags flags)
        {
            m_flags = flags;
            m_willDisplayIndices = flags.HasFlag(ReorderableListFlags.ShowIndices);
            m_willDisplaySizeField = flags.HasFlag(ReorderableListFlags.ShowSizeField);
            m_willDisplaySearchField = !flags.HasFlag(ReorderableListFlags.HideSearchField);
            m_willDisplayHeader = !flags.HasFlag(ReorderableListFlags.HideHeader);
            m_willDisplayToolbar = !flags.HasFlag(ReorderableListFlags.HideToolbar);
            m_willDisplayCreateButton = !flags.HasFlag(ReorderableListFlags.HideCreateButton);
            m_willDisplayDeleteButtons = !flags.HasFlag(ReorderableListFlags.HideDeleteButtons);
            m_willDisplayDragHandles = !flags.HasFlag(ReorderableListFlags.HideDragHandles);
            m_willDisplayContextMenu = !flags.HasFlag(ReorderableListFlags.DisableContextMenu);
            m_willAutoScroll = !flags.HasFlag(ReorderableListFlags.DisableAutoScroll);
            m_willClip = !flags.HasFlag(ReorderableListFlags.DisableClipping);
            m_allowReorderingElements = !flags.HasFlag(ReorderableListFlags.DisableReorderingElements);
            m_allowDropInsertingElements = !flags.HasFlag(ReorderableListFlags.DisableDropInserting);
            m_allowCreatingElements = !flags.HasFlag(ReorderableListFlags.DisableCreatingElements);
            m_allowDeletingElements = !flags.HasFlag(ReorderableListFlags.DisableDeletingElements);
            m_allowDuplicatingElements = !flags.HasFlag(ReorderableListFlags.DisableDuplicatingElements);
            m_allowCutCopyPastingElements = !flags.HasFlag(ReorderableListFlags.DisableCutCopyPastingElements);
            m_allowCollapsingList = !flags.HasFlag(ReorderableListFlags.DisableCollapsingList);
            m_allowCollapsingElements = !flags.HasFlag(ReorderableListFlags.DisableCollapsingElements);
            m_willDisplayListLength = !flags.HasFlag(ReorderableListFlags.HideListLength);
            m_expandByDefault = !flags.HasFlag(ReorderableListFlags.CollapseByDefault);
        }

        protected virtual void Update()
        {
            if (IsValid())
            {
                RefreshCachedValues(false);
            }
        }

        #region ABSTRACT METHODS

        /// <summary>
        /// Get the number of elements in the list.
        /// </summary>
        /// <returns>
        /// Number of elements.
        /// </returns>
        public abstract int GetElementCount();

        /// <summary>
        /// Get the type of all elements in the list
        /// </summary>
        /// <returns>
        /// Element type.
        /// </returns>
        public abstract Type GetElementType();

        /// <summary>
        /// Get the value of the element at the specified index.
        /// </summary>
        /// <returns>
        /// Element value.
        /// </returns>
        public abstract object GetElementValue(int index);

        /// <summary>
        /// Gets identifiers for this element that can be searched for.
        /// </summary>
        /// <param name="index">Zero-based index of list element.</param>
        /// <returns>
        /// Identifiers that can be used to filter the list.
        /// </returns>
        protected abstract string[] GetElementIdentifiers(int index);
        
        /// <summary>
        /// Insert an element at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index of the list element.</param>
        /// <param name="element">Element to insert.</param>
        protected abstract void Insert(int index, object element);

        /// <summary>
        /// Copy elements to a buffer.
        /// </summary>
        /// <param name="index">Zero-based indices of list elements to be copied.</param>
        /// <returns>A copy of the element at the specified index.</returns>
        protected abstract object[] Copy(int[] indices);

        /// <summary>
        /// Paste elements from a buffer.
        /// </summary>
        /// <param name="index">Zero-based index where the elements will be pasted.</param>
        /// <param name="copyBuffer">The elements to paste.</param>
        protected abstract void Paste(int index, object[] copyBuffer);

        /// <summary>
        /// Delete the element at the specified indices.
        /// </summary>
        /// <param name="indices">Zero-based indices of list element.</param>
        protected abstract void Delete(int[] indices);

        /// <summary>
        /// Move an element from its source index to a destination index.
        /// </summary>
        /// <param name="sourceIndex">Zero-based index of source element.</param>
        /// <param name="destIndex">Zero-based index of destination.</param>
        protected abstract void Move(int srcIndex, int dstIndex);
        
        /// <summary>
        /// Draws the main interface for a list element when it is expanded.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected abstract void DrawElementExpandedContent(Rect position, int index);

        /// <summary>
        /// Draws the main interface for a list item when it is collapsed.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected abstract void DrawElementCollapsedContent(Rect position, int index);

        /// <summary>
        /// Gets the height of the expanded list element interface.
        /// </summary>
        /// <param name="index">Zero-based index of list element.</param>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        protected abstract float GetElementExpandedContentHeight(int index);

        /// <summary>
        /// Gets the height of the collapsed list element interface.
        /// </summary>
        /// <param name="index">Zero-based index of list element.</param>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        protected abstract float GetElementCollapsedContentHeight(int index);

        #endregion


        #region VIRTUAL METHODS

        /// <summary>
        /// Insert an element at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index of the list element.</param>
        protected virtual void Insert(int index)
        {
            Insert(index, Create());
        }

        /// <summary>
        /// Create a new element.
        /// </summary>
        /// <returns>The new element.</returns>
        protected virtual object Create()
        {
            Type elementType = GetElementType();

            if (elementType != null)
            {
                if (elementType == typeof(string))
                {
                    return "";
                }
                else if (elementType.IsValueType)
                {
                    return Activator.CreateInstance(elementType);
                }
            }

            return null;
        }
        
        public virtual bool IsValid()
        {
            return true;
        }

        #region HEADER

        public void DrawHeaderBackground(params GUILayoutOption[] options)
        {
            DrawHeaderBackground(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), HeaderStyle, options));
        }

        public void DrawHeaderBackground(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawHeaderBackground(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the background of the list header.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// <para>Background spans the header.</para>
        /// </remarks>
        /// <param name="position">Total position of list header in GUI.</param>
        public void DrawHeaderBackground(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            if (Event.current.type == EventType.Repaint)
            {
                HeaderStyle.Draw(position, false, false, false, false);
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        public void DrawHeaderContent(GUIContent label, params GUILayoutOption[] options)
        {
            DrawHeaderContent(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), HeaderStyle, options), label);
        }

        public void DrawHeaderContent(GUIContent label, GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawHeaderContent(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), guiStyle, options), label);
        }

        /// <summary>
        /// Draws the main interface for the list header.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="label">The label to display in the list header.</param>
        public void DrawHeaderContent(Rect position, GUIContent label)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            m_headerRect = position;
            
            position.xMin += HeaderStyle.padding.left;
            position.xMax -= HeaderStyle.padding.right;
            position.yMin += HeaderStyle.padding.top;
            position.yMax -= HeaderStyle.padding.bottom;

            m_headerContentRect = position;

            OnBeginHeaderGUI(position, label);

            bool showSizeField = m_willDisplaySizeField && m_allowCreatingElements && m_allowDeletingElements;

            float spacingLeft = HeaderStyle.padding.left;
            float spacingRight = HeaderStyle.padding.right;
            float spacingTop = HeaderStyle.padding.top;
            float spacingBottom = HeaderStyle.padding.bottom;

            GUIContent sizeFieldLabel = new GUIContent("[" + GetElementCount() + "]", "The number of elements in the list");
            
            float sizeFieldWidth = ReorderableListStyles.SizeFieldWidth;
            if (!showSizeField && m_willDisplayListLength)
            {
                sizeFieldWidth = EditorStyles.label.CalcSize(sizeFieldLabel).x + spacingLeft;
            }

            Rect leftPosition = position;
            Rect middlePosition = position;
            Rect rightPosition = position;

            leftPosition.xMax -= sizeFieldWidth;

            middlePosition.xMin += leftPosition.width + spacingRight;

            rightPosition.xMin += leftPosition.width + spacingRight + spacingLeft;
            rightPosition.xMin += middlePosition.width + spacingRight;
            rightPosition.xMin -= spacingLeft;
            rightPosition.xMax += spacingRight;
            rightPosition.yMin -= spacingTop;
            rightPosition.yMax += spacingBottom;

            if (m_allowCollapsingList)
            {
                m_expanded = DrawHeaderFoldout(leftPosition, label, m_expanded);
            }
            else
            {
                EditorGUI.LabelField(leftPosition, label);
            }

            if (showSizeField)
            {
                Color restoreColor = GUI.backgroundColor;
                GUI.backgroundColor = ReorderableListStyles.NormalBackgroundColor;

                EditorGUI.BeginChangeCheck();
                int newSizeInput = GetElementCount();
                newSizeInput = EditorGUI.DelayedIntField(middlePosition, GUIContent.none, newSizeInput, ReorderableListStyles.SizeFieldStyle);
                if (EditorGUI.EndChangeCheck())
                {
                    SetElementCount(newSizeInput);
                }

                GUI.backgroundColor = restoreColor;
            }
            else if (m_willDisplayListLength)
            {
                EditorGUI.LabelField(middlePosition, sizeFieldLabel, ReorderableListStyles.SizeLabelStyle);
            }

            OnEndHeaderGUI(position, label);

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        /// <summary>
        /// Gets the default header label that will be displayed if no label is specified
        /// </summary>
        /// <returns>The default label.</returns>
        public virtual GUIContent GetDefaultHeaderLabel()
        {
            return new GUIContent("List");
        }

        /// <summary>
        /// Draws the foldout that allows the user to show or hide the list container.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="label">Label to display in header.</param>
        /// <param name="expanded">The current expanded state of the list container.</param>
        /// <returns>The new expanded state of the list container.</returns>
        protected virtual bool DrawHeaderFoldout(Rect position, GUIContent label, bool expanded)
        {
            bool restoreHierarchyMode = EditorGUIUtility.hierarchyMode;
            EditorGUIUtility.hierarchyMode = false;

            expanded = EditorGUI.Foldout(position, expanded, label, true, EditorStyles.foldout);

            EditorGUIUtility.hierarchyMode = restoreHierarchyMode;

            return expanded;
        }

        /// <summary>
        /// Get the height of the header interface
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        public virtual float GetHeaderContentHeight()
        {
            return ReorderableListStyles.HeaderHeight;
        }
        #endregion


        #region FOOTER
        /// <summary>
        /// Draws the main interface for the list footer.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void DrawFooterContent(Rect position)
        {
        }

        /// <summary>
        /// Get the height of the footer interface
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        public virtual float GetFooterContentHeight()
        {
            return ReorderableListStyles.FooterHeight;
        }
        #endregion


        #region FOLDOUT

        /// <summary>
        /// Get the width of the element foldout
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        public virtual float GetFoldoutWidth()
        {
            return EditorStyles.foldout.normal.background.width + 4f;
        }

        /// <summary>
        /// Get the height of the element foldout
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        public virtual float GetFoldoutHeight()
        {
            return EditorStyles.foldout.normal.background.height + 4f;
        }

        #endregion


        #region TOOLBAR

        public void DrawToolbarBackground(params GUILayoutOption[] options)
        {
            DrawToolbarBackground(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), ToolbarStyle, options));
        }

        public void DrawToolbarBackground(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawToolbarBackground(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the background of the toolbar interface.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Total position in GUI.</param>
        public void DrawToolbarBackground(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            if (Event.current.type == EventType.Repaint)
            {
                ToolbarStyle.Draw(position, false, false, false, false);
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        public void DrawToolbarContent(params GUILayoutOption[] options)
        {
            DrawToolbarContent(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), ToolbarStyle, options));
        }

        public void DrawToolbarContent(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawToolbarContent(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the main interface for the toolbar.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        public void DrawToolbarContent(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            m_toolbarRect = position;

            position.xMin += ToolbarStyle.padding.left;
            position.xMax -= ToolbarStyle.padding.right;
            position.yMin += ToolbarStyle.padding.top;
            position.yMax -= ToolbarStyle.padding.bottom;
            
            m_toolbarContentRect = position;

            OnBeginToolbarGUI(position);

            bool showSearchField = m_willDisplaySearchField;
            bool showCreateButton = m_willDisplayCreateButton && m_allowCreatingElements;

            float filterButtonWidth = GetFilterButtonWidth();
            float createButtonWidth = GetCreateButtonWidth();
            
            Rect leftPosition = position;
            Rect middlePosition = position;
            Rect rightPosition = position;

            leftPosition.xMax = leftPosition.xMin + filterButtonWidth;

            middlePosition.xMin += filterButtonWidth;
            middlePosition.xMax -= createButtonWidth;

            rightPosition.xMin = rightPosition.xMax - createButtonWidth;

            if (showSearchField)
            {
                EditorGUI.BeginChangeCheck();
                m_searchType = ToolbarSearchTypeField(leftPosition, m_searchType);
                if (EditorGUI.EndChangeCheck())
                {
                    InvokeRefresh();
                }
            }

            if (showSearchField)
            {
                EditorGUI.BeginChangeCheck();
                GUI.SetNextControlName(m_searchControlName);
                m_searchString = ToolbarSearchField(middlePosition, m_searchString);
                bool searchIsFocused = GUI.GetNameOfFocusedControl() == m_searchControlName;
                if (searchIsFocused != m_searchIsFocused)
                {
                    m_searchIsFocused = searchIsFocused;
                    if (searchIsFocused)
                    {
                        UpdateCachedElementIdentifiers();
                    }
                }
                if (EditorGUI.EndChangeCheck())
                {
                    UpdateCachedElementSearchStates();
                    int[] matchIndices = GetSearchMatchIndices();
                    if (matchIndices == null)
                    {
                        DoSelectNone(false);
                    }
                    else
                    {
                        DoSelect(matchIndices, false);
                    }
                    OnSearchMatchesUpdated(matchIndices);
                }
            }

            if (showCreateButton)
            {
                rightPosition.xMin += ToolbarStyle.padding.left;
                rightPosition.xMax -= ToolbarStyle.padding.right;
                DrawElementCreateButton(rightPosition);
            }

            OnEndToolbarGUI(position);
            
            EditorGUI.indentLevel = restoreIndentLevel;
        }

        protected virtual string GetSearchControlName()
        {
            return GetType().AssemblyQualifiedName + GetHashCode() + "ItemSearchString";
        }

        protected virtual void OnSearchMatchesUpdated(int[] matchIndices)
        {
        }

        /// <summary>
        /// Get the height of the toolbar interface
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        public virtual float GetToolbarContentHeight()
        {
            return ReorderableListStyles.ToolbarHeight;
        }

        protected bool[] GetElementSearchStates()
        {
            if (string.IsNullOrEmpty(m_searchString))
            {
                return null;
            }

            int count = GetElementCount();
            bool[] searchStates = new bool[count];
            for (int i = 0; i < count; i++)
            {
                searchStates[i] = m_elementIdentifiers[i].Any(x => !string.IsNullOrEmpty(x) && x.ToLower().Contains(m_searchString.ToLower()));
            }
            return searchStates;
        }

        protected string[][] GetElementIdentifiers()
        {
            int count = GetElementCount();
            string[][] identifiers = new string[count][];
            for (int i = 0; i < count; i++)
            {
                identifiers[i] = GetElementIdentifiers(i);
            }
            return identifiers;
        }

        public int[] GetSearchMatchIndices()
        {
            if (m_elementSearchStates == null)
            {
                return null;
            }
            List<int> indices = new List<int>(m_elementSearchStates.Length);
            for (int i = 0; i < m_elementSearchStates.Length; ++i)
            {
                if (m_elementSearchStates[i])
                {
                    indices.Add(i);
                }
            }
            return indices.ToArray();
        }

        public virtual SearchType ToolbarSearchTypeField(Rect position, SearchType searchType)
        {
            GUIStyle style = (searchType == SearchType.Focus) ? ReorderableListStyles.ToolbarSearchTypeStyle1 : ReorderableListStyles.ToolbarSearchTypeStyle2;
            float halfWidth = (position.width - style.fixedWidth) / 2;
            position.xMin += halfWidth;
            position.xMax -= halfWidth;

            float halfHeight = (position.height - style.fixedHeight) / 2;
            position.yMin += halfHeight;
            position.yMax -= halfHeight;

            Color color = GUI.color;
            if (searchType == SearchType.Focus)
            {
                GUI.color = new Color(1, 1, 1, 0.25f);
            }
            else
            {
                GUI.color = new Color(1, 1, 1, 0.5f);
            }
            searchType = (SearchType)EditorGUI.EnumPopup(position, searchType, style);
            GUI.color = color;

            return searchType;
        }

        public virtual string ToolbarSearchField(Rect position, string text)
        {
            GUIStyle searchTextFieldStyle = GUI.skin.FindStyle("ToolbarSeachTextField");
            GUIStyle searchCancelButtonStyle = GUI.skin.FindStyle("ToolbarSeachCancelButton");
            GUIStyle searchCancelButtonEmptyStyle = GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty");
            return SearchField(position, text, searchTextFieldStyle, searchCancelButtonStyle, searchCancelButtonEmptyStyle);
        }

        public string SearchField(Rect position, string text, GUIStyle searchTextFieldStyle, GUIStyle searchCancelButtonStyle, GUIStyle searchCancelButtonEmptyStyle)
        {
            text = EditorGUI.TextField(GetSearchTextFieldPosition(position), text, searchTextFieldStyle);
            if (GUI.Button(GetSearchButtonPosition(position), GUIContent.none, string.IsNullOrEmpty(text) ? searchCancelButtonEmptyStyle : searchCancelButtonStyle) && !string.IsNullOrEmpty(text))
            {
                text = "";
                ReleaseKeyboardFocus();
                GUI.changed = true;
            }
            return text;
        }

        public static Rect GetSearchTextFieldPosition(Rect position)
        {
            position.width -= 15f;
            return position;
        }

        public static Rect GetSearchButtonPosition(Rect position)
        {
            position.x += position.width - 15f;
            position.width = 15f;
            position.xMin -= 1;
            return position;
        }
        #endregion


        #region LIST ELEMENTS

        public void DrawElementsBackground(params GUILayoutOption[] options)
        {
            DrawElementsBackground(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), ContainerStyle, options));
        }

        public void DrawElementsBackground(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawElementsBackground(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the background of the list container.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// <para>Background spans the entire list, including all elements.</para>
        /// </remarks>
        /// <param name="position">Total position of list container in GUI.</param>
        public void DrawElementsBackground(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            if (Event.current.type == EventType.Repaint)
            {
                ContainerStyle.Draw(position, false, false, false, false);
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        public void DrawElementsContent(params GUILayoutOption[] options)
        {
            DrawElementsContent(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), ContainerStyle, options));
        }

        public void DrawElementsContent(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawElementsContent(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the main interface for the list container.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        public void DrawElementsContent(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            m_elementsRect = position;

            float requiredHeight = GetCachedElementsHeight();
            bool displayScrollBar = position.height < requiredHeight;

            position.xMin += ContainerStyle.padding.left;
            position.xMax -= ContainerStyle.padding.right;
            position.yMin += ContainerStyle.padding.top;
            position.yMax -= ContainerStyle.padding.bottom;

            if (displayScrollBar)
            {
                Rect viewRect = position;
                float scrollBarWidth = 15;
                viewRect.xMax -= scrollBarWidth;
                viewRect.height = requiredHeight;
                m_scrollPosition = GUI.BeginScrollView(position, m_scrollPosition, viewRect);
                position.xMax -= scrollBarWidth;
                position.xMax -= ContainerStyle.padding.right;
            }

            Rect listRect = position;
            listRect.yMax -= GetFooterContentHeight();

            m_elementsContentRect = listRect;

            Rect elementRect = listRect;

            m_visibleRect = UnityInternalGUIClip.visibleRect;

            OnBeginElementsGUI(position);

            if (GetElementCount() > 0)
            {
                if (m_isDragging && Event.current.type == EventType.Repaint && m_selectedIndices.Count == 1)
                {
                    int selectedIndex = GetFirstSelectedIndex();

                    m_dragTargetIndex = GetBoxTargetRowIndex(listRect, m_draggedY - m_dragOffset);
                    m_nonDragTargetIndices.Clear();
                    for (int i = 0; i < GetElementCount(); i++)
                    {
                        if (m_dragTargetIndex < 0 || i != selectedIndex)
                        {
                            m_nonDragTargetIndices.Add(i);
                        }
                    }
                    if (m_dragTargetIndex >= 0)
                    {
                        m_nonDragTargetIndices.Insert(m_dragTargetIndex, -1);
                    }
                    bool isAfterElementBeingDragged = false;

                    float elementYOffset = 0f;
                    float elementHeight = 0f;
                    for (int j = 0; j < m_nonDragTargetIndices.Count; j++)
                    {
                        if (IsFiltered(j))
                        {
                            continue;
                        }
                        if (m_nonDragTargetIndices[j] != -1)
                        {
                            elementHeight = GetCachedElementHeight(m_nonDragTargetIndices[j]);
                            elementRect.height = elementHeight;
                            elementRect.y = listRect.y + elementYOffset;
                            if (isAfterElementBeingDragged)
                            {
                                elementRect.y += GetSeparatorHeight() + GetCachedElementHeight(selectedIndex);
                            }

                            DrawElement(elementRect, m_nonDragTargetIndices[j]);

                            if (m_nonDragTargetIndices[j] > 0 || isAfterElementBeingDragged)
                            {
                                DrawSeparatorContent(GetSeparatorRect(elementRect));
                            }

                            elementYOffset += GetSeparatorHeight() + elementHeight;
                        }
                        else
                        {
                            if (Event.current.type == EventType.Repaint)
                            {
                                Rect targetRect = GetBoxDragTargetRect(listRect, m_dragTargetIndex, selectedIndex);
                                DrawDragTarget(targetRect, GetBoxDragTargetColor());

                                if (m_dragTargetIndex > 0)
                                {
                                    DrawSeparatorContent(GetSeparatorRect(targetRect));
                                }
                            }

                            isAfterElementBeingDragged = true;
                        }
                    }

                    elementRect.height = GetCachedElementHeight(selectedIndex);
                    elementRect.y = m_draggedY - m_dragOffset + listRect.y;

                    DrawElement(elementRect, selectedIndex);
                }
                else
                {
                    if (m_isDragging && m_selectedIndices.Count > 1)
                    {
                        m_dragTargetIndex = GetSeparatorTargetRowIndex(listRect.y);
                    }

                    float elementYOffset = 0f;
                    float elementHeight = 0f;
                    for (int j = 0; j < GetElementCount(); j++)
                    {
                        if (IsFiltered(j))
                        {
                            continue;
                        }

                        elementHeight = GetCachedElementHeight(j);
                        elementRect.height = elementHeight;
                        elementRect.y = listRect.y + elementYOffset;

                        DrawElement(elementRect, j);

                        if (m_dragTargetIndex == j)
                        {
                            DrawDragTarget(GetSeparatorDragTargetRect(elementRect), GetSeparatorDragTargetColor());
                        }
                        else
                        {
                            if (j > 0)
                            {
                                DrawSeparatorContent(GetSeparatorRect(elementRect));
                            }
                        }

                        elementYOffset += GetSeparatorHeight() + elementHeight;
                    }

                    if (m_dragTargetIndex == GetElementCount())
                    {
                        elementRect.height = 0;
                        elementRect.y = listRect.y + elementYOffset;

                        DrawDragTarget(GetSeparatorDragTargetRect(elementRect), GetSeparatorDragTargetColor());
                    }
                }
            }
            else
            {
                elementRect.y = listRect.y;

                DrawEmptyElement(elementRect);
            }

            HandleUserInput(position);

            HandleCallbacks();

            Rect footerPosition = position;
            footerPosition.yMin = position.yMax - GetFooterContentHeight();

            DrawFooterContent(footerPosition);

            OnEndElementsGUI(position);

            if (displayScrollBar)
            {
                GUI.EndScrollView(true);
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        #endregion


        #region ELEMENT

        /// <summary>
        /// Draws the background of a list element.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// <para>Background of list element spans the entire list element
        /// including the drag handle, list content, and delete button.</para>
        /// </remarks>
        /// <param name="position">Total position of list element in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected virtual void DrawElementBackground(Rect position, int index, bool isSelected, bool isDraggable)
        {
            Color restoreColor = GUI.color;
            Color color = GetNormalBackgroundColor(index);
            if (isSelected)
            {
                color = GetSelectedBackgroundColor(index);
            }
            GUI.color = color;
            ReorderableListStyles.ElementBackgroundStyle.Draw(position, false, false, false, false);
            GUI.color = restoreColor;
        }

        /// <summary>
        /// Draws the interface for when the list is empty (contains no elements).
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void DrawEmptyElementContent(Rect position)
        {
            EditorGUI.LabelField(position, new GUIContent("List is Empty"));
        }

        /// <summary>
        /// Get the color of the element background when the element
        /// </summary>
        /// <returns>
        /// The background color
        /// </returns>
        public virtual Color GetNormalBackgroundColor(int index)
        {
            Color color = Color.white;
            color.a = 0;
            return color;
        }

        /// <summary>
        /// Get the color of the element background when the element is selected
        /// </summary>
        /// <returns>
        /// The background color when selected
        /// </returns>
        public virtual Color GetSelectedBackgroundColor(int index)
        {
            return ReorderableListStyles.SelectedBackgroundColor;
        }

        /// <summary>
        /// Draws the drag handle next to each list element.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected virtual void DrawElementDragHandle(Rect position, int index)
        {
            float iconWidth = 10;
            position.x += (position.width / 2f) - (iconWidth / 2f);
            position.width = iconWidth;
            position.y += (position.height / 2f) - (ReorderableListStyles.DragHandleStyle.fixedHeight / 2f);
            ReorderableListStyles.DragHandleStyle.Draw(position, false, false, false, false);
        }

        /// <summary>
        /// Draws the index next to each list element.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected virtual void DrawElementIndex(Rect position, int index)
        {
            ReorderableListStyles.IndexLabelStyle.Draw(position, GetElementIndexLabelText(index), false, false, false, false);
        }

        /// <summary>
        /// The label next to each list element.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected virtual string GetElementIndexLabelText(int index)
        {
            if (m_getElementIndexLabelTextCallback != null)
            {
                return m_getElementIndexLabelTextCallback(index);
            }
            else
            {
                int padding = (GetElementCount() - 1).ToString().Length;
                return string.Format("{0}:", index.ToString("D" + padding.ToString()));
            }
        }

        /// <summary>
        /// Draws the foldout next to each list element.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        /// <param name="expanded">The current expanded state of the foldout.</param>
        /// <returns>The new expanded state of the foldout.</returns>
        protected virtual bool DrawElementFoldout(Rect position, int index, bool expanded)
        {
            bool restoreHierarchyMode = EditorGUIUtility.hierarchyMode;
            EditorGUIUtility.hierarchyMode = false;

            expanded = EditorGUI.Foldout(position, expanded, GUIContent.none, true, EditorStyles.foldout);

            EditorGUIUtility.hierarchyMode = restoreHierarchyMode;

            return expanded;
        }

        /// <summary>
        /// Draws the delete button next to each list element.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        /// <param name="index">Zero-based index of list element.</param>
        protected virtual void DrawElementDeleteButton(Rect position, int index)
        {
            using (new EditorGUI.DisabledScope(m_allowDeletingElements && !CanDelete(index)))
            {
                if (GUI.Button(position, ReorderableListStyles.DeleteIcon, ReorderableListStyles.ElementButtonStyle))
                {
                    DoDelete(index);
                }
            }
        }

        /// <summary>
        /// Draws create button in the list header.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void DrawElementCreateButton(Rect position)
        {
            using (new EditorGUI.DisabledScope(!CanCreate()))
            {
                GUIContent icon = (UseCreateMenu()) ? ReorderableListStyles.CreateMenuIcon : ReorderableListStyles.CreateIcon;
                
                if (GUI.Button(position, icon, ReorderableListStyles.ElementButtonStyle))
                {
                    DoCreate();
                }
            }
        }

        /// <summary>
        /// Get the height of the interface when the list is empty
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        protected virtual float GetEmptyElementContentHeight()
        {
            return ReorderableListStyles.ElementContentHeight;
        }

        /// <summary>
        /// Get the position of the element drag handle
        /// </summary>
        /// <returns>
        /// Element drag handle rect.
        /// </returns>
        protected virtual Rect GetElementDragHandleRect(Rect rect, int index)
        {
            rect.width = GetDragHandleWidth();
            rect.xMin += GetElementPadding();
            rect.xMax -= GetElementPadding();
            rect.yMin += GetElementPadding();
            rect.yMax -= GetElementPadding();
            return rect;
        }

        /// <summary>
        /// Get the position of the element index
        /// </summary>
        /// <returns>
        /// Element index rect.
        /// </returns>
        protected virtual Rect GetElementIndexRect(Rect rect, int index)
        {
            rect.xMin += GetDragHandleWidth();
            rect.width = GetIndexWidth();
            rect.xMin += GetElementPadding();
            rect.xMax -= GetElementPadding();
            rect.yMin += GetElementPadding();
            rect.yMax -= GetElementPadding();
            return rect;
        }

        /// <summary>
        /// Get the position of the element foldout
        /// </summary>
        /// <returns>
        /// Element foldout rect.
        /// </returns>
        protected virtual Rect GetElementFoldoutRect(Rect rect, int index)
        {
            rect.xMin += GetDragHandleWidth();
            if (m_willDisplayIndices)
            {
                rect.xMin += GetIndexWidth();
            }
            rect.width = GetFoldoutWidth();
            rect.height = GetFoldoutHeight();
            rect.xMin += GetElementPadding();
            rect.xMax -= GetElementPadding();
            rect.yMin += GetElementPadding();
            rect.yMax -= GetElementPadding();
            return rect;
        }

        /// <summary>
        /// Get the position of the element content
        /// </summary>
        /// <returns>
        /// Element content rect.
        /// </returns>
        protected virtual Rect GetElementContentRect(Rect rect, int index)
        {
            if (index > -1)
            {
                rect.xMin += GetDragHandleWidth();
                if (m_willDisplayIndices)
                {
                    rect.xMin += GetIndexWidth();
                }
                if (m_allowCollapsingElements && GetCachedElementCanCollapse(index))
                {
                    rect.xMin += GetFoldoutWidth();
                }
                rect.xMax -= GetDeleteButtonWidth();
            }
            rect.xMin += GetElementPadding();
            rect.xMax -= GetElementPadding();
            rect.yMin += GetElementPadding();
            rect.yMax -= GetElementPadding();
            return rect;
        }

        /// <summary>
        /// Get the position of the element button
        /// </summary>
        /// <returns>
        /// Element button rect.
        /// </returns>
        protected virtual Rect GetElementButtonRect(Rect rect)
        {
            float deleteButtonWidth = GetDeleteButtonWidth();
            rect.xMin = rect.xMax - deleteButtonWidth;
            rect.xMin += GetElementPadding();
            rect.xMax -= GetElementPadding();
            rect.yMin += GetElementPadding();
            rect.yMax -= GetElementPadding();
            return rect;
        }

        /// <summary>
        /// Get the width of the element drag handle
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        protected virtual float GetDragHandleWidth()
        {
            return ReorderableListStyles.DragHandleWidth;
        }

        /// <summary>
        /// Get the width of the element index
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        protected virtual float GetIndexWidth()
        {
            float max = -1;
            for (int i = 0; i < GetElementCount(); i++)
            {
                float current = ReorderableListStyles.IndexLabelStyle.CalcSize(new GUIContent(GetElementIndexLabelText(i))).x + (2f * GetElementPadding());
                if (current > max)
                {
                    max = current;
                }
            }

            return max;
        }

        /// <summary>
        /// Get the width of the element filter button
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        protected virtual float GetFilterButtonWidth()
        {
            return ReorderableListStyles.FilterButtonWidth;
        }

        /// <summary>
        /// Get the width of the element create button
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        protected virtual float GetCreateButtonWidth()
        {
            return ReorderableListStyles.CreateButtonWidth;
        }

        /// <summary>
        /// Get the width of the element delete button
        /// </summary>
        /// <returns>
        /// Width measurement in pixels.
        /// </returns>
        protected virtual float GetDeleteButtonWidth()
        {
            return ReorderableListStyles.DeleteButtonWidth;
        }

        /// <summary>
        /// Get the size of the padding for this element
        /// </summary>
        /// <returns>
        /// Size measurement in pixels.
        /// </returns>
        protected virtual float GetElementPadding()
        {
            return ReorderableListStyles.ElementPadding;
        }

        #endregion


        #region DRAG TARGET

        /// <summary>
        /// Draws the drag target for the currently dragged item as a box.
        /// </summary>
        protected virtual void DrawDragTarget(Rect position, Color color)
        {
            ReorderableListStyles.DragTarget(position, color);
        }

        /// <summary>
        /// Get the color of the drag target when displayed as a box.
        /// </summary>
        /// <returns>
        /// Box drag target color.
        /// </returns>
        public virtual Color GetBoxDragTargetColor()
        {
            return ReorderableListStyles.BoxDragTargetBackgroundColor;
        }

        /// <summary>
        /// Get the position of the drag target when displayed as a box.
        /// </summary>
        /// <returns>
        /// Box drag target rect.
        /// </returns>
        private Rect GetBoxDragTargetRect(Rect listRect, int targetIndex, int selectedIndex)
        {
            return new Rect(listRect.x, listRect.y + GetTargetYOffset(targetIndex, selectedIndex), listRect.width, GetCachedElementHeight(selectedIndex));
        }

        #endregion


        #region SEPARTOR

        public void DrawSeparatorBackground(params GUILayoutOption[] options)
        {
            DrawSeparatorBackground(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), SeparatorStyle, options));
        }

        public void DrawSeparatorBackground(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawSeparatorBackground(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the background of the toolbar interface.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Total position in GUI.</param>
        public void DrawSeparatorBackground(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            if (Event.current.type == EventType.Repaint)
            {
                SeparatorStyle.Draw(position, false, false, false, false);
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        public void DrawSeparatorContent(params GUILayoutOption[] options)
        {
            DrawSeparatorContent(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), SeparatorStyle, options));
        }

        public void DrawSeparatorContent(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawSeparatorContent(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), guiStyle, options));
        }

        /// <summary>
        /// Draws the separation between each element.
        /// </summary>
        /// <remarks>
        /// <para>This method is only used to handle GUI repaint events.</para>
        /// </remarks>
        /// <param name="position">Position in GUI.</param>
        protected virtual void DrawSeparatorContent(Rect position)
        {
            position = EditorGUI.IndentedRect(position);
            int restoreIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            
            position.xMin += SeparatorStyle.padding.left;
            position.xMax -= SeparatorStyle.padding.right;
            position.yMin += SeparatorStyle.padding.top;
            position.yMax -= SeparatorStyle.padding.bottom;

            if (position.height > 0)
            {
                ReorderableListStyles.Separator(position, GetSeparatorColor());
            }

            EditorGUI.indentLevel = restoreIndentLevel;
        }

        /// <summary>
        /// Get the size of the separation between each element.
        /// </summary>
        /// <returns>
        /// Height measurement in pixels.
        /// </returns>
        public virtual float GetSeparatorHeight()
        {
            return ReorderableListStyles.SeparatorHeight;
        }

        /// <summary>
        /// Get the color of the separation between each element.
        /// </summary>
        /// <returns>
        /// Separator color.
        /// </returns>
        public virtual Color GetSeparatorColor()
        {
            return ReorderableListStyles.SeparatorColor;
        }

        /// <summary>
        /// Get the color of the separation between each element when highlighting the separation as a drag target.
        /// </summary>
        /// <returns>
        /// Separator color.
        /// </returns>
        public virtual Color GetSeparatorDragTargetColor()
        {
            return ReorderableListStyles.SeparatorDragTargetBackgroundColor;
        }

        /// <summary>
        /// Get the position of the separation between each element.
        /// </summary>
        /// <returns>
        /// Separator rect.
        /// </returns>
        protected virtual Rect GetSeparatorRect(Rect elementRect)
        {
            float separatorHeight = GetSeparatorHeight();
            elementRect.height = separatorHeight;
            elementRect.y -= separatorHeight;
            elementRect.xMin -= ContainerStyle.padding.left;
            elementRect.xMax += ContainerStyle.padding.right;
            return elementRect;
        }

        /// <summary>
        /// Get the position of the drag target between each element when displayed as a separator.
        /// </summary>
        /// <returns>
        /// Separator drag target rect.
        /// </returns>
        protected virtual Rect GetSeparatorDragTargetRect(Rect elementRect)
        {
            elementRect = GetSeparatorRect(elementRect);
            elementRect.yMin -= 1;
            elementRect.yMax += 1;
            return elementRect;
        }

        #endregion


        #region CONTEXT MENU

        /// <summary>
		/// Invoked to respond to the user clicking on an item in the context menu.
		/// </summary>
        /// <remarks>
        /// Obj parameter should be cast to <see cref="ReorderableListCommandOperation"/> if the object passed in 
        /// <see cref="AddItemsToContextMenu"/> was of type <see cref="ReorderableListCommandOperation"/>
        /// </remarks>
		/// <param name="obj">Object that represents the context menu item that was clicked.</param>
        protected virtual void HandleContextMenuCommand(object obj)
        {
            ReorderableListCommandOperation commandOperation = (ReorderableListCommandOperation)obj;
            if (commandOperation.CommandName == s_commandMoveToTop.text)
            {
                DoMove(commandOperation.Indices.Where(i => CanMove(i)).ToArray(), 0);
            }
            else if (commandOperation.CommandName == s_commandMoveToBottom.text)
            {
                DoMove(commandOperation.Indices.Where(i => CanMove(i)).ToArray(), GetElementCount() - 1);
            }
            else if (commandOperation.CommandName == s_commandInsertAbove.text)
            {
                DoInsert(commandOperation.Indices.First());
            }
            else if (commandOperation.CommandName == s_commandInsertBelow.text)
            {
                DoInsert(commandOperation.Indices.Last() + 1);
            }
            else if (commandOperation.CommandName == s_commandCut.text)
            {
                DoCut(commandOperation.Indices.Where(i => CanDelete(i)).ToArray());
            }
            else if (commandOperation.CommandName == s_commandCopy.text)
            {
                DoCopy(commandOperation.Indices);
            }
            else if (commandOperation.CommandName == s_commandPaste.text)
            {
                int index = GetElementCount();
                if (commandOperation.Indices.Length > 0)
                {
                    index = commandOperation.Indices.Last() + 1;
                }
                DoPaste(index);
            }
            else if (commandOperation.CommandName == s_commandDuplicate.text)
            {
                DoDuplicate(commandOperation.Indices);
            }
            else if (commandOperation.CommandName == s_commandDelete.text)
            {
                DoDelete(commandOperation.Indices.Where(i => CanDelete(i)).ToArray());
            }
            else if (commandOperation.CommandName == s_commandDeleteAll.text)
            {
                DoDeleteAll();
            }
            else if (commandOperation.CommandName == s_commandSelectAll.text)
            {
                DoSelectAll();
            }
            else if (commandOperation.CommandName == s_commandSelectNone.text)
            {
                DoSelectNone();
            }
            else if (commandOperation.CommandName == s_commandExpandAll.text)
            {
                DoExpandAll();
            }
            else if (commandOperation.CommandName == s_commandCollapseAll.text)
            {
                DoCollapseAll();
            }
        }

        /// <summary>
		/// Generates a context menu for the selected list items.
		/// </summary>
		/// <param name="menu">Menu to be populated.</param>
		protected virtual void AddItemsToContextMenu(GenericMenu menu)
        {
            ReorderableListCommandOperation commandOperation = new ReorderableListCommandOperation();
            int[] selectedIndices = GetSelectedIndices();
            int[] indices = selectedIndices.ToArray();
            commandOperation.Indices = indices;

            if (m_allowReorderingElements && !IsFiltering())
            {
                if (selectedIndices.Any(i => CanMove(i)))
                {
                    if (menu.GetItemCount() > 0)
                    {
                        menu.AddSeparator("");
                    }

                    int count = GetElementCount();
                    bool allSelectedIndicesAreAtTop = IndicesAreWithinRange(indices, 0, indices.Length - 1);
                    bool allSelectedIndicesAreAtBottom = IndicesAreWithinRange(indices, count - indices.Length, count - 1);

                    if (!allSelectedIndicesAreAtTop)
                    {
                        commandOperation.CommandName = s_commandMoveToTop.text;
                        menu.AddItem(s_commandMoveToTop, false, HandleContextMenuCommand, commandOperation);
                    }
                    else
                    {
                        menu.AddDisabledItem(s_commandMoveToTop);
                    }

                    if (!allSelectedIndicesAreAtBottom)
                    {
                        commandOperation.CommandName = s_commandMoveToBottom.text;
                        menu.AddItem(s_commandMoveToBottom, false, HandleContextMenuCommand, commandOperation);
                    }
                    else
                    {
                        menu.AddDisabledItem(s_commandMoveToBottom);
                    }
                }
            }

            if (m_allowCreatingElements && !UseCreateMenu())
            {
                if (selectedIndices.Length > 0)
                {
                    if (CanCreate())
                    {
                        if (menu.GetItemCount() > 0)
                        {
                            menu.AddSeparator("");
                        }

                        commandOperation.CommandName = s_commandInsertAbove.text;
                        menu.AddItem(s_commandInsertAbove, false, HandleContextMenuCommand, commandOperation);

                        commandOperation.CommandName = s_commandInsertBelow.text;
                        menu.AddItem(s_commandInsertBelow, false, HandleContextMenuCommand, commandOperation);
                    }
                }
            }

            if (m_allowCutCopyPastingElements)
            {
                if (menu.GetItemCount() > 0)
                {
                    menu.AddSeparator("");
                }

                if (selectedIndices.Length > 0)
                {
                    if (m_allowDeletingElements && selectedIndices.Any(i => CanDelete(i)))
                    {
                        if (CanCutCopyPaste())
                        {
                            commandOperation.CommandName = s_commandCut.text;
                            menu.AddItem(s_commandCut, false, HandleContextMenuCommand, commandOperation);
                        }
                        else
                        {
                            menu.AddDisabledItem(s_commandCut);
                        }
                    }

                    if (CanCutCopyPaste())
                    {
                        commandOperation.CommandName = s_commandCopy.text;
                        menu.AddItem(s_commandCopy, false, HandleContextMenuCommand, commandOperation);
                    }
                    else
                    {
                        menu.AddDisabledItem(s_commandCopy);
                    }
                }

                if (s_copyBuffer.Count > 0 && CanCreate() && CanCutCopyPaste())
                {
                    commandOperation.CommandName = s_commandPaste.text;
                    menu.AddItem(s_commandPaste, false, HandleContextMenuCommand, commandOperation);
                }
                else
                {
                    menu.AddDisabledItem(s_commandPaste);
                }
            }

            if (m_allowDuplicatingElements)
            {
                if (selectedIndices.Length > 0)
                {
                    if (CanCreate())
                    {
                        if (menu.GetItemCount() > 0)
                        {
                            menu.AddSeparator("");
                        }

                        if (CanDuplicate())
                        {
                            commandOperation.CommandName = s_commandDuplicate.text;
                            menu.AddItem(s_commandDuplicate, false, HandleContextMenuCommand, commandOperation);
                        }
                        else
                        {
                            menu.AddDisabledItem(s_commandDuplicate);
                        }
                    }
                }
            }

            if (m_allowDeletingElements)
            {
                if (GetElementCount() > 0)
                {
                    if (selectedIndices.Any(i => CanDelete(i)))
                    {
                        if (menu.GetItemCount() > 0)
                        {
                            menu.AddSeparator("");
                        }

                        commandOperation.CommandName = s_commandDelete.text;
                        menu.AddItem(s_commandDelete, false, HandleContextMenuCommand, commandOperation);
                    }
                    
                    if (Enumerable.Range(0, GetElementCount()).Any(i => CanDelete(i)))
                    {
                        commandOperation.CommandName = s_commandDeleteAll.text;
                        menu.AddItem(s_commandDeleteAll, false, HandleContextMenuCommand, commandOperation);
                    }
                }
            }

            if (GetElementCount() > 0)
            {
                if (menu.GetItemCount() > 0)
                {
                    menu.AddSeparator("");
                }

                commandOperation.CommandName = s_commandSelectAll.text;
                menu.AddItem(s_commandSelectAll, false, HandleContextMenuCommand, commandOperation);

                commandOperation.CommandName = s_commandSelectNone.text;
                menu.AddItem(s_commandSelectNone, false, HandleContextMenuCommand, commandOperation);
            }

            if (m_allowCollapsingElements)
            {
                if (GetElementCount() > 0)
                {
                    if (Enumerable.Range(0, GetElementCount()).Any(i => CanCollapse(i)))
                    {
                        if (menu.GetItemCount() > 0)
                        {
                            menu.AddSeparator("");
                        }

                        commandOperation.CommandName = s_commandExpandAll.text;
                        menu.AddItem(s_commandExpandAll, false, HandleContextMenuCommand, commandOperation);

                        commandOperation.CommandName = s_commandCollapseAll.text;
                        menu.AddItem(s_commandCollapseAll, false, HandleContextMenuCommand, commandOperation);
                    }
                }
            }
        }

        #endregion


        #region EXTRA GUI

        /// <summary>
        /// Executed before header is drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnBeginHeaderGUI(Rect position, GUIContent label)
        {
        }

        /// <summary>
        /// Executed after header is drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnEndHeaderGUI(Rect position, GUIContent label)
        {
        }

        /// <summary>
        /// Executed before toolbar is drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnBeginToolbarGUI(Rect position)
        {
        }

        /// <summary>
        /// Executed after toolbar is drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnEndToolbarGUI(Rect position)
        {
        }

        /// <summary>
        /// Executed before any list elements are drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnBeginElementsGUI(Rect position)
        {
        }

        /// <summary>
        /// Executed after all list elements have been drawn.
        /// </summary>
        /// <param name="position">Position in GUI.</param>
        protected virtual void OnEndElementsGUI(Rect position)
        {
        }

        /// <summary>
        /// Executed before an element is drawn
        /// </summary>
        /// <param name="position">Element position in GUI.</param>
        /// <param name="index">Zero-based index of the list element.</param>
        protected virtual void OnBeginElementGUI(Rect position, int index)
        {
        }

        /// <summary>
        /// Occurs after all list items have been drawn.
        /// </summary>
        /// <param name="position">Element position in GUI.</param>
        /// <param name="index">Zero-based index of the list element.</param>
        protected virtual void OnEndElementGUI(Rect position, int index)
        {
        }

        /// <summary>
        /// Override to replace the standard create button with an create menu where 
        /// the user can select the type of element to create in the list from a menu.
        /// </summary>
        /// <returns>
        /// Whether or not a menu will be used to add items to the list.
        /// </returns>
        public virtual bool UseCreateMenu()
        /// The generic menu with items representing the types of elements the user can create in this list.
        /// </returns>
        {
            return false;
        }

        protected virtual void ShowCreateMenu()
        {
            GenericMenu menu = new GenericMenu();

            AddItemsToCreateMenu(menu);

            if (menu.GetItemCount() > 0)
            {
                menu.ShowAsContext();
            }
        }

        protected virtual void AddItemsToCreateMenu(GenericMenu menu)
        {
            int index = m_createIndex;

            menu.AddItem(new GUIContent("Create New"), false, HandleCreateMenu, new Action(() => Insert(index)));
            
            menu.AddItem(new GUIContent("Create Null"), false, HandleCreateMenu, new Action(() => Insert(index, null)));
        }
        
        protected virtual void HandleCreateMenu(object obj)
        {
            Action action = obj as Action;

            if (action != null)
            {
                action();
            }

            int index = m_createIndex;

            OnCreateMenuHandled(index, true);
        }

        #endregion


        #region CONDITIONS

        /// <summary>
        /// Determines whether an item can be reordered.
        /// </summary>
        /// <remarks>
        /// <para>This should be a light-weight method since it will be used to determine
        /// whether grab handle should be included for each item in a reorderable list.</para>
        /// <para>Please note that returning a value of <c>false</c> does not truly prevent movement
        /// on list item since other draggable items can be moved around it.</para>
        /// <para>This is redundant when <see cref="ReorderableListFlags.DisableReorderingElements"/>
        /// is specified, since none of the elements will be draggable.</para>
        /// </remarks>
        /// <param name="index">Zero-based index of the list element.</param>
        /// <returns>
        /// A value of <c>true</c> if item can be dragged; otherwise <c>false</c>.
        /// </returns>
        public virtual bool CanMove(int index)
        {
            return true;
        }

        /// <summary>
        /// Determines whether an item can be collapsed.
        /// </summary>
        /// <remarks>
        /// <para>This should be a light-weight method since it will be used to determine
        /// whether a foldout should be included for each item in a reorderable list.</para>
        /// <para>This is redundant when <see cref="ReorderableListFlags.DisableCollapsingElements"/>
        /// is specified, since the elements will alsways be shown expanded.</para>
        /// </remarks>
        /// <param name="index">Zero-based index of the list element.</param>
        /// <returns>
        /// A value of <c>true</c> if item can be collapsed; otherwise <c>false</c>.
        /// </returns>
        public virtual bool CanCollapse(int index)
        {
            return true;
        }

        /// <summary>
		/// Determines whether an item can be deleted from list.
		/// </summary>
		/// <remarks>
		/// <para>This should be a light-weight method since it will be used to determine
		/// whether delete button is shown in an active state or in a disabled state.</para>
		/// <para>This is redundant when <see cref="ReorderableListFlags.HideDeleteButtons"/>
		/// is specified, since the delete buttons will not be shown at all.</para>
		/// </remarks>
		/// <param name="index">Zero-based index of the list element.</param>
		/// <returns>
		/// A value of <c>true</c> if item can be deleted; otherwise <c>false</c>.
		/// </returns>
        public virtual bool CanDelete(int index)
        {
            return true;
        }

        /// <summary>
        /// Determines whether items can be added to a list.
        /// </summary>
        /// <remarks>
        /// <para>This should be a light-weight method since it will be used to determine
        /// whether create button is shown in an active state or in a disabled state.</para>
        /// <para>This is redundant when <see cref="ReorderableListFlags.HideCreateButton"/>
        /// is specified, since the create buton will not be shown at all.</para>
        /// </remarks>
        /// <returns>
        /// A value of <c>true</c> if items can be created; otherwise <c>false</c>.
        /// </returns>
        public virtual bool CanCreate()
        {
            return true;
        }

        /// <summary>
        /// Determines whether items can be copied in a list.
        /// </summary>
        /// <remarks>
        /// <para>This should be a light-weight method since it will be used to determine
        /// whether the cut/copy/paste options are shown in an active disabled state the context menu.</para>
        /// <para>This is redundant when <see cref="ReorderableListFlags.DisableCutCopyPastingElements"/>
        /// is specified, since the cut/copy/paste options will not be shown at all.</para>
        /// </remarks>
        /// <returns>
        /// A value of <c>true</c> if items can be cut/copy/pasted; otherwise <c>false</c>.
        /// </returns>
        public virtual bool CanCutCopyPaste()
        {
            return true;
        }

        /// <summary>
        /// Determines whether items can be duplicated in a list.
        /// </summary>
        /// <remarks>
        /// <para>This should be a light-weight method since it will be used to determine
        /// whether the duplicate option are shown in an active disabled state the context menu.</para>
        /// <para>This is redundant when <see cref="ReorderableListFlags.DisableDuplicatingElements"/>
        /// is specified, since the duplicate option will not be shown at all.</para>
        /// </remarks>
        /// <returns>
        /// A value of <c>true</c> if items can be duplicated; otherwise <c>false</c>.
        /// </returns>
        public virtual bool CanDuplicate()
        {
            return true;
        }
        #endregion


        #region CALLBACKS

        /// <summary>
        /// Invoked when a list item is selected or deselected.
        /// </summary>
        protected virtual void OnSelect(int[] indices)
        {
        }

        /// <summary>
        /// Invoked on mouse up when a list item is clicked (without dragging)
        /// </summary>
        protected virtual void OnMouseUp(int index)
        {
        }

        /// <summary>
        /// Invoked when the list elements are changed in some way (added, deleted, etc.)
        /// </summary>
        protected virtual void OnChanged()
        {
        }

        /// <summary>
        /// Invoked when the list gui is refreshed
        /// </summary>
        protected virtual void OnRefresh()
        {
        }

        #endregion

        #region ACTIONS

        protected virtual void SetClearCopyBufferOnNextPaste(bool clear)
        {
            s_clearCopyBufferOnNextPaste = clear;
        }

        #endregion

        #endregion


        #region PUBLIC METHODS

        public bool IsFiltered(int index)
        {
            return m_searchType == SearchType.Filter && m_elementSearchStates != null && !m_elementSearchStates[index];
        }

        public bool IsFiltering()
        {
            return m_searchType == SearchType.Filter && m_elementSearchStates != null;
        }

        public int[] GetSelectedIndices()
        {
            return m_selectedIndices.Keys.ToArray();
        }

        protected void SetSelectedIndices(int[] indices)
        {
            Select(indices);
        }

        public object[] GetSelectedElements()
        {
            return GetSelectedIndices().Where(i => i >= 0 && i < GetElementCount()).Select(i => GetElementValue(i)).ToArray();
        }

        public void SetSelectedElements(object[] elements)
        {
            List<int> indicesToSelect = new List<int>();
            for (int i = 0; i < GetElementCount(); i++)
            {
                object elementValue = GetElementValue(i);
                if (elements.Contains(elementValue))
                {
                    indicesToSelect.Add(i);
                }
            }
            Select(indicesToSelect.ToArray());
        }

        public bool IsIndexSelected(int index)
        {
            return m_selectedIndices.ContainsKey(index);
        }

        public int GetFirstSelectedIndex()
        {
            if (m_selectedIndices.Keys.Count > 0)
            {
                return m_selectedIndices.Keys[0];
            }
            return -1;
        }

        public int GetLastSelectedIndex()
        {
            if (m_selectedIndices.Keys.Count > 0)
            {
                return m_selectedIndices.Keys[m_selectedIndices.Keys.Count - 1];
            }
            return -1;
        }

        public bool SetElementCount(int newCount, bool focus = true)
        {
            if (newCount < 0)
            {
                // Do nothing when new count is negative.
                return true;
            }

            int deleteCount = Mathf.Max(0, GetElementCount() - newCount);
            int createCount = Mathf.Max(0, newCount - GetElementCount());

            while (deleteCount-- > 0)
            {
                DoDelete(GetElementCount() - 1, focus);
            }
            while (createCount-- > 0)
            {
                DoInsert(GetElementCount() - 1, focus);
            }

            InvokeOnChanged();

            return true;
        }

        public float GetHeight()
        {
            bool isExpanded = !m_allowCollapsingList || m_expanded;
            bool showHeader = m_willDisplayHeader;
            bool showToolbar = m_willDisplayToolbar && isExpanded;
            bool showElements = isExpanded;

            float height = 0f;
            if (showHeader)
            {
                height += GetHeaderHeight();
            }
            if (showToolbar)
            {
                height += GetToolbarHeight();
            }
            if (showElements)
            {
                height += GetCachedElementsHeight();
            }
            return height;
        }

        public float GetHeaderHeight()
        {
            float height = GetHeaderContentHeight();

            height += HeaderStyle.padding.top;
            height += HeaderStyle.padding.bottom;

            return height;
        }

        public float GetToolbarHeight()
        {
            float height = GetToolbarContentHeight();

            height += ToolbarStyle.padding.top;
            height += ToolbarStyle.padding.bottom;
            
            return height;
        }

        public float GetElementsHeight()
        {
            return GetCachedElementsHeight();
        }

        public float GetElementHeight(int index)
        {
            return (GetElementPadding() * 2f) + GetElementContentHeight(index);
        }

        public float GetElementContentHeight(int index)
        {
            if (index >= GetElementCount())
            {
                return 0f;
            }

            bool canCollapse = m_allowCollapsingElements && GetCachedElementCanCollapse(index);

            if (!canCollapse || GetCachedIsExpanded(index))
            {
                return GetElementExpandedContentHeight(index);
            }
            else
            {
                return GetElementCollapsedContentHeight(index);
            }
        }

        public void GrabControlFocus()
        {
            if (!HasControlFocus())
            {
                GUIUtility.hotControl = 0;
                GUIUtility.hotControl = m_controlID;
                m_isDragging = false;
                m_dragTargetIndex = -1;
                
            }
        }

        public void ReleaseControlFocus()
        {
            if (HasControlFocus())
            {
                GUIUtility.hotControl = 0;
            }
        }

        public void GrabKeyboardFocus()
        {
            if (!HasKeyboardFocus())
            {
                EditorGUIUtility.editingTextField = false;
                GUIUtility.keyboardControl = m_controlID;
            }
        }

        public void ReleaseKeyboardFocus()
        {
            if (HasKeyboardFocus())
            {
                GUIUtility.keyboardControl = 0;
            }
        }

        public bool HasControlFocus()
        {
            return GUIUtility.hotControl == m_controlID;
        }

        public bool HasKeyboardFocus()
        {
            return GUIUtility.keyboardControl == m_controlID;
        }

        public int GetDefaultCreateIndex()
        {
            int index = 0;
            int[] selectedIndices = GetSelectedIndices();
            if (selectedIndices.Length > 0)
            {
                index = selectedIndices[selectedIndices.Length - 1] + 1;
            }
            else
            {
                index = GetElementCount();
            }
            return index;
        }

        public void DoCreate(bool focus = true)
        {
            int index = GetDefaultCreateIndex();
            DoCreate(index, focus);
        }

        public void DoCreate(int index, bool focus = true)
        {
            m_createIndex = index;
            if (UseCreateMenu())
            {
                ShowCreateMenu();
            }
            else
            {
                DoInsert(index, focus);
                m_createIndex = -1;
            }
        }
        
        public void OnCreateMenuHandled(int index, bool focus = true)
        {
            OnCreateMenuHandled(new int[] { index }, focus);
        }

        public void OnCreateMenuHandled(int startIndex, int count, bool focus = true)
        {
            OnCreateMenuHandled(Enumerable.Range(startIndex, count).ToArray(), focus);
        }

        public void OnCreateMenuHandled(int[] indices, bool focus = true)
        {
            m_createIndex = -1;
            DoSelect(indices, focus);
            if (m_willAutoScroll)
            {
                DoScrollTowardIndices(indices);
            }
            InvokeOnChanged();
            m_expanded = true;
        }

        public void DoInsert(int index, bool focus = true)
        {
            Insert(index);
            DoSelect(index, focus);
            if (m_willAutoScroll)
            {
                DoScrollTowardIndex(index);
            }
            InvokeOnChanged();
            m_expanded = true;
        }

        public void DoInsert(int index, object element, bool focus = true)
        {
            DoInsert(index, new object[] { element }, focus);
        }

        public void DoInsert(int index, object[] elements, bool focus = true)
        {
            if (index >= 0 && elements.Length > 0)
            {
                int offset = 0;
                for (int i = 0; i < elements.Length; i++)
                {
                    object element = elements[i];
                    Insert(index + offset, element);
                    offset++;
                }

                if (elements.Length > 0)
                {
                    DoSelectRange(index, elements.Length, focus);
                    if (m_willAutoScroll)
                    {
                        DoScrollTowardRange(index, elements.Length);
                    }
                    InvokeOnChanged();
                    m_expanded = true;
                }
            }
        }

        public void DoCut(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                DoCopy(indices);
                DoDelete(indices, focus);
                SetClearCopyBufferOnNextPaste(true);
            }
        }

        public void DoCopy(int[] indices)
        {
            if (indices.Length > 0)
            {
                object[] copies = Copy(indices);
                foreach (object copy in copies)
                {
                    s_copyBuffer.Add(copy);
                }
            }
        }

        public void DoPaste(int index, bool focus = true)
        {
            if (index >= 0)
            {
                Type elementType = GetElementType();
                object[] copyBuffer = s_copyBuffer.Where(i => CanDropElementIntoList(i, elementType)).ToArray();
                bool clearCopyBuffer = s_clearCopyBufferOnNextPaste;
                SetClearCopyBufferOnNextPaste(false);
                Paste(index, copyBuffer);
                if (clearCopyBuffer)
                {
                    s_copyBuffer.Clear();
                }
                DoSelectRange(index, copyBuffer.Length, focus);
                InvokeOnChanged();
            }
        }

        public void DoDuplicate(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                DoCopy(indices);
                SetClearCopyBufferOnNextPaste(true);
                DoPaste(indices.Last() + 1, focus);
            }
        }

        public void DoDelete(int index, bool focus = true)
        {
            if (index >= 0)
            {
                Delete(new int[] { index });
                DoSelectNone(focus);
                InvokeOnChanged();
            }
        }

        public void DoDelete(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                Delete(indices.OrderByDescending(v => v).ToArray());
                DoSelectNone(focus);
                InvokeOnChanged();
            }
        }

        public void DoMove(int srcIndex, int dstIndex, bool focus = true)
        {
            if (srcIndex >= 0 && dstIndex >= 0)
            {
                Move(srcIndex, dstIndex);
                DoSelectRange(dstIndex, 1, focus);
                InvokeOnChanged();
            }
        }

        public void DoMove(int[] srcIndices, int dstIndex, bool focus = true)
        {
            if (srcIndices.Length == 1)
            {
                DoMove(srcIndices[0], dstIndex, focus);
            }
            else if (srcIndices.Length > 1)
            {
                int offset = 0;

                // Move all src elements to the bottom of the list in reverse order
                for (int i = srcIndices.Length - 1; i >= 0; i--)
                {
                    int srcIndex = srcIndices[i];
                    if (srcIndex < dstIndex)
                    {
                        offset--;
                    }
                    Move(srcIndex, GetElementCount() - 1);
                }

                int offsetDstIndex = dstIndex + offset;

                // Now move all src elements from the bottom of the list to the destination
                for (int i = 0; i < srcIndices.Length; i++)
                {
                    Move(GetElementCount() - 1, offsetDstIndex + i);
                }

                DoSelectRange(offsetDstIndex, srcIndices.Length, focus);
                InvokeOnChanged();
            }
        }

        public void DoDropInsert(ReorderableList srcList, int[] srcIndices, int dstIndex, bool focus = true)
        {
            if (srcIndices.Length > 0 && dstIndex >= 0)
            {
                Type dstElementType = GetElementType();

                List<int> compatibleSrcIndicies = new List<int>();
                List<object> compatibleSrcElements = new List<object>();
                foreach (int index in srcIndices)
                {
                    object element = srcList.GetElementValue(index);
                    if (CanDropElementIntoList(element, dstElementType))
                    {
                        compatibleSrcIndicies.Add(index);
                        compatibleSrcElements.Add(element);
                    }
                }

                srcList.DoDelete(compatibleSrcIndicies.ToArray(), focus);

                DoInsert(dstIndex, compatibleSrcElements.ToArray(), focus);
            }
        }

        public void DoDropInsert(UnityEngine.Object[] selectedObjects, int dstIndex, bool focus = true)
        {
            if (selectedObjects.Length > 0 && dstIndex >= 0)
            {
                Type dstElementType = GetElementType();

                List<object> compatibleSrcElements = new List<object>();
                foreach (UnityEngine.Object element in selectedObjects)
                {
                    if (CanDropElementIntoList(element, dstElementType))
                    {
                        compatibleSrcElements.Add(element);
                    }
                }

                DoInsert(dstIndex, compatibleSrcElements.ToArray(), focus);
            }
        }

        public void DoDeleteAll(bool focus = true)
        {
            DoDelete(Enumerable.Range(0, GetElementCount()).ToArray(), focus);
        }

        public void DoSelect(int index, bool focus = true)
        {
            if (index >= 0)
            {
                Select(index);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoSelectRange(int startIndex, int count, bool focus = true)
        {
            if (startIndex >= 0 && count > 0)
            {
                SelectRange(startIndex, count);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoSelectAll(bool focus = true)
        {
            SelectAll();
            InvokeOnSelect(GetSelectedIndices(), focus);
        }

        public void DoSelectNone(bool focus = true)
        {
            SelectNone();
            InvokeOnSelect(GetSelectedIndices(), focus);
        }

        public void DoAddRangeToCurrentSelection(int startIndex, int count, bool focus = true)
        {
            if (startIndex >= 0 && count > 0)
            {
                AddRangeToCurrentSelection(startIndex, count);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoAddToCurrentSelection(int index, bool focus = true)
        {
            if (index >= 0)
            {
                AddToCurrentSelection(index);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoRemoveFromCurrentSelection(int index, bool focus = true)
        {
            RemoveFromCurrentSelection(index);
            InvokeOnSelect(GetSelectedIndices(), focus);
        }

        public void DoSelect(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                Select(indices);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoAddToCurrentSelection(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                AddToCurrentSelection(indices);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoRemoveFromCurrentSelection(int[] indices, bool focus = true)
        {
            if (indices.Length > 0)
            {
                RemoveFromCurrentSelection(indices);
                InvokeOnSelect(GetSelectedIndices(), focus);
            }
        }

        public void DoScrollTowardIndex(int index)
        {
            m_scrollIndices = new int[] { index };
        }

        public void DoScrollTowardIndices(int[] indices)
        {
            m_scrollIndices = indices;
        }

        public void DoScrollTowardRange(int startIndex, int count)
        {
            m_scrollIndices = Enumerable.Range(startIndex, count).ToArray();
        }

        public void DoScrollTowardSelection()
        {
            m_scrollIndices = GetSelectedIndices();
        }

        #endregion


        #region PROTECTED METHODS

        protected void Draw(params GUILayoutOption[] options)
        {
            Draw(GetDefaultHeaderLabel(), options);
        }

        protected void Draw(GUIContent label, params GUILayoutOption[] options)
        {
            float height = GetHeight();
            Rect position = EditorGUILayout.GetControlRect(false, height, options);
            Draw(position, label);
        }

        protected void Draw(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            Draw(GetDefaultHeaderLabel(), guiStyle, options);
        }

        protected void Draw(GUIContent label, GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            float height = GetHeight();
            Rect position = EditorGUILayout.GetControlRect(false, height, guiStyle, options);
            Draw(position, label);
        }

        protected void Draw(Rect position)
        {
            Draw(position, GetDefaultHeaderLabel());
        }

        protected virtual void Draw(Rect position, GUIContent label)
        {
            bool isExpanded = !m_allowCollapsingList || m_expanded;
            bool showHeader = m_willDisplayHeader;
            bool showToolbar = isExpanded && m_willDisplayToolbar;
            bool showSeparator = isExpanded && m_willDisplayToolbar;
            bool showElements = isExpanded;

            float headerHeight = 0f;
            if (showHeader)
            {
                headerHeight = GetHeaderHeight();
            }

            float toolbarHeight = 0f;
            if (showToolbar)
            {
                toolbarHeight = GetToolbarHeight();
            }

            float separatorHeight = 0f;
            if (showSeparator)
            {
                separatorHeight = GetSeparatorHeight();
            }

            float elementsHeight = 0f;
            if (showElements)
            {
                elementsHeight = GetCachedElementsHeight();
            }

            Rect pos = position;
            pos.height = 0;
            if (showHeader)
            {
                pos.y += pos.height;
                pos.height = headerHeight;
                DrawHeader(pos, label);
            }

            if (showToolbar)
            {
                pos.y += pos.height;
                pos.height = toolbarHeight;
                DrawToolbar(pos);
            }

            if (showSeparator)
            {
                pos.y += pos.height;
                pos.height = separatorHeight;
                DrawSeparator(pos);
            }

            if (showElements)
            {
                pos.y += pos.height;
                pos.height = elementsHeight;
                DrawElements(pos);
            }
        }

        public void DrawHeader(params GUILayoutOption[] options)
        {
            GUIContent label = GetDefaultHeaderLabel();
            DrawHeader(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), HeaderStyle, options), label);
        }

        public void DrawHeader(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            GUIContent label = GetDefaultHeaderLabel();
            DrawHeader(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), guiStyle, options), label);
        }

        public void DrawHeader(GUIContent label, params GUILayoutOption[] options)
        {
            DrawHeader(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), HeaderStyle, options), label);
        }

        public void DrawHeader(GUIContent label, GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawHeader(EditorGUILayout.GetControlRect(false, GetHeaderHeight(), guiStyle, options), label);
        }

        public void DrawHeader(Rect position, GUIContent label)
        {
            DrawHeaderBackground(position);

            DrawHeaderContent(position, label);
        }

        public void DrawToolbar(params GUILayoutOption[] options)
        {
            DrawToolbar(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), ToolbarStyle, options));
        }

        public void DrawToolbar(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawToolbar(EditorGUILayout.GetControlRect(false, GetToolbarHeight(), guiStyle, options));
        }

        public void DrawToolbar(Rect position)
        {
            DrawToolbarBackground(position);

            DrawToolbarContent(position);
        }

        public void DrawSeparator(params GUILayoutOption[] options)
        {
            DrawSeparator(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), SeparatorStyle, options));
        }

        public void DrawSeparator(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawSeparator(EditorGUILayout.GetControlRect(false, GetSeparatorHeight(), guiStyle, options));
        }

        public void DrawSeparator(Rect position)
        {
            DrawSeparatorBackground(position);

            DrawSeparatorContent(position);
        }

        public void DrawElements(params GUILayoutOption[] options)
        {
            DrawElements(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), ContainerStyle, options));
        }

        public void DrawElements(GUIStyle guiStyle, params GUILayoutOption[] options)
        {
            DrawElements(EditorGUILayout.GetControlRect(false, GetCachedElementsHeight(), guiStyle, options));
        }

        public void DrawElements(Rect position)
        {
            DrawElementsBackground(position);

            DrawElementsContent(position);
        }

        public void DrawEmptyElement(Rect position)
        {
            if (Event.current.type == EventType.Repaint)
            {
                DrawElementBackground(position, -1, false, false);
            }

            DrawEmptyElementContent(GetElementContentRect(position, -1));
        }

        public void DrawElement(Rect position, int index)
        {
            OnBeginElementGUI(position, index);

            if (!m_willClip || m_visibleRect.Overlaps(position))
            {
                if (Event.current.type == EventType.Repaint)
                {
                    bool isSelected = IsIndexSelected(index);
                    DrawElementBackground(position, index, isSelected, m_allowReorderingElements && !IsFiltering() && CanMove(index));

                    if (m_willDisplayDragHandles && m_allowReorderingElements && !IsFiltering())
                    {
                        DrawElementDragHandle(GetElementDragHandleRect(position, index), index);
                    }

                    if (m_willDisplayIndices)
                    {
                        DrawElementIndex(GetElementIndexRect(position, index), index);
                    }
                }

                if (m_allowCollapsingElements && GetCachedElementCanCollapse(index))
                {
                    bool expanded = GetCachedIsExpanded(index);
                    EditorGUI.BeginChangeCheck();
                    expanded = DrawElementFoldout(GetElementFoldoutRect(position, index), index, expanded);
                    if (EditorGUI.EndChangeCheck())
                    {
                        SetCachedElementIsExpanded(index, expanded);
                    }
                }

                DrawElementContent(GetElementContentRect(position, index), index);

                if (m_willDisplayDeleteButtons && m_allowDeletingElements)
                {
                    DrawElementDeleteButton(GetElementButtonRect(position), index);
                }
                
                if (Event.current.type == EventType.Repaint)
                {
                    if (m_elementSearchStates != null && !m_elementSearchStates[index])
                    {
                        Color restoreColor = GUI.color;
                        Color searchNonMatchColor = ReorderableListStyles.SeparatorColor;
                        GUI.color = searchNonMatchColor;
                        ReorderableListStyles.ElementBackgroundStyle.Draw(position, false, false, false, false);
                        GUI.color = restoreColor;
                    }
                }
            }

            OnEndElementGUI(position, index);
        }

        public void DrawElementContent(Rect position, int index)
        {
            bool restoreHierarchyMode = EditorGUIUtility.hierarchyMode;
            EditorGUIUtility.hierarchyMode = false;

            bool canCollapse = m_allowCollapsingElements && GetCachedElementCanCollapse(index);
            
            if (!canCollapse || GetCachedIsExpanded(index))
            {
                DrawElementExpandedContent(position, index);
            }
            else
            {
                DrawElementCollapsedContent(position, index);
            }

            EditorGUIUtility.hierarchyMode = restoreHierarchyMode;
        }

        public void RefreshCachedValues(bool updateSearchInfo = true)
        {
            UpdateCachedElementIsExpandedStates();
            UpdateCachedElementCanCollapseStates();
            UpdateCachedElementExpandedContentHeights();
            UpdateCachedElementCollapsedContentHeights();
            if (updateSearchInfo)
            {
                UpdateCachedElementIdentifiers();
                UpdateCachedElementSearchStates();
            }
        }

        protected float GetCachedElementHeight(int index)
        {
            return (GetElementPadding() * 2f) + GetCachedElementContentHeight(index);
        }

        protected float GetCachedElementContentHeight(int index)
        {
            if (index < 0 || index >= GetElementCount())
            {
                return 0f;
            }

            bool canCollapse = m_allowCollapsingElements && GetCachedElementCanCollapse(index);

            if (!canCollapse || GetCachedIsExpanded(index))
            {
                return GetCachedExpandedElementContentHeight(index);
            }
            else
            {
                return GetCachedCollapsedElementContentHeight(index);
            }
        }

        protected bool IndicesAreWithinRange(int[] indices, int start, int end)
        {
            foreach (int index in GetSelectedIndices())
            {
                if (index < start || index > end)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion


        #region PRIVATE METHODS

        private void InvokeOnSelect(int[] indices, bool focus = true)
        {
            m_selectedIndicesThisFrame = indices;
            if (focus)
            {
                GrabKeyboardFocus();
            }
        }

        private void InvokeOnMouseUp(int index, bool focus = true)
        {
            m_mouseUpIndexThisFrame = index;
            if (focus)
            {
                GrabKeyboardFocus();
            }
        }

        private void InvokeRefresh()
        {
            m_refresh = true;
        }

        private void InvokeOnChanged()
        {
            m_changedThisFrame = true;
            InvokeRefresh();
        }

        private void InvokeShowContextMenu()
        {
            m_showContextMenuNextFrame = true;
        }

        private void InvokeShowCreateMenu()
        {
            m_showCreateMenuNextFrame = true;
        }

        private void SelectRange(int startIndex, int count)
        {
            Select(Enumerable.Range(startIndex, count).ToArray());
        }

        private void Select(int index)
        {
            m_selectedIndices = new SortedList<int, int>();
            m_firstSelectedIndex = index;
            m_selectedIndices.Add(index, index);
        }

        private void AddRangeToCurrentSelection(int startIndex, int count)
        {
            AddToCurrentSelection(Enumerable.Range(startIndex, count).ToArray());
        }

        private void AddToCurrentSelection(int index)
        {
            if (!IsIndexSelected(index))
            {
                if (m_selectedIndices.Count == 0)
                {
                    m_firstSelectedIndex = index;
                }
                m_selectedIndices.Add(index, index);
            }
        }

        private void RemoveFromCurrentSelection(int index)
        {
            if (IsIndexSelected(index))
            {
                if (m_firstSelectedIndex == index)
                {
                    m_firstSelectedIndex = -1;
                }
                m_selectedIndices.Remove(index);
            }
        }

        private void Select(int[] indices)
        {
            SelectNone();
            foreach (int index in indices)
            {
                AddToCurrentSelection(index);
            }
        }

        private void AddToCurrentSelection(int[] indices)
        {
            foreach (int index in indices)
            {
                AddToCurrentSelection(index);
            }
        }

        private void RemoveFromCurrentSelection(int[] indices)
        {
            foreach (int index in indices)
            {
                RemoveFromCurrentSelection(index);
            }
        }

        private void SelectAll()
        {
            for (int i = 0; i < GetElementCount(); i++)
            {
                AddToCurrentSelection(i);
            }
        }

        private void SelectNone()
        {
            m_selectedIndices.Clear();
        }

        private void Expand(int index)
        {
            SetCachedElementIsExpanded(index, true);
        }

        private void Collapse(int index)
        {
            SetCachedElementIsExpanded(index, false);
        }

        private void DoExpandAll()
        {
            for (int i = 0; i < GetElementCount(); i++)
            {
                Expand(i);
            }
        }

        private void DoCollapseAll()
        {
            for (int i = 0; i < GetElementCount(); i++)
            {
                Collapse(i);
            }
        }

        private void UpdateCachedElementIsExpandedStates()
        {
            int count = GetElementCount();
            bool[] states = new bool[count];
            for (int i = 0; i < count; i++)
            {
                if (m_cachedElementIsExpandedStates != null && i < m_cachedElementIsExpandedStates.Length)
                {
                    states[i] = m_cachedElementIsExpandedStates[i];
                }
                else
                {
                    states[i] = false;
                }
            }
            m_cachedElementIsExpandedStates = states;
        }

        private void UpdateCachedElementCanCollapseStates()
        {
            int count = GetElementCount();
            bool[] states = new bool[count];
            for (int i = 0; i < count; i++)
            {
                states[i] = CanCollapse(i);
            }
            m_cachedElementCanCollapseStates = states;
        }

        private void UpdateCachedElementExpandedContentHeights()
        {
            int count = GetElementCount();
            float[] heights = new float[count];
            for (int i = 0; i < count; i++)
            {
                heights[i] = GetElementExpandedContentHeight(i);
            }
            m_cachedElementExpandedContentHeights = heights;
        }

        private void UpdateCachedElementCollapsedContentHeights()
        {
            int count = GetElementCount();
            float[] heights = new float[count];
            for (int i = 0; i < count; i++)
            {
                heights[i] = GetElementCollapsedContentHeight(i);
            }
            m_cachedElementCollapsedContentHeights = heights;
        }

        protected void UpdateCachedElementIdentifiers()
        {
            m_elementIdentifiers = GetElementIdentifiers();
        }

        protected void UpdateCachedElementSearchStates()
        {
            m_elementSearchStates = GetElementSearchStates();
        }

        private bool CanDrag()
        {
            return m_allowReorderingElements && !IsFiltering() && m_selectedIndices.Count > 0 && GetSelectedIndices().ToList().Exists(i => CanMove(i));
        }

        private bool CanDropInsert(ReorderableList srcList, int[] srcIndices)
        {
            Type dstElementType = GetElementType();

            foreach (int index in srcIndices)
            {
                object element = srcList.GetElementValue(index);
                if (CanDropElementIntoList(element, dstElementType))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CanDropInsert(UnityEngine.Object[] selectedObjects)
        {
            Type dstElementType = GetElementType();

            foreach (UnityEngine.Object element in selectedObjects)
            {
                if (CanDropElementIntoList(element, dstElementType))
                {
                    return true;
                }
            }

            return false;
        }

        private void DoFocusOnFirstControl(int index)
        {
            m_focusIndex = index;
        }
        
        private void HandleScroll(Rect listRect)
        {
            if (Event.current.type != EventType.Layout)
            {
                if (m_scrollIndices != null)
                {
                    ScrollTowardIndices(listRect, m_scrollIndices);
                    m_scrollIndices = null;
                }
            }
        }

        private bool IsMouseOverAnyElementContent(Rect listRect)
        {
            Rect elementRect = listRect;

            float elementYOffset = 0f;
            for (int i = 0; i < GetElementCount(); i++)
            {
                float elementHeight = GetCachedElementHeight(i);
                elementRect.height = elementHeight;
                elementRect.y = listRect.y + elementYOffset;

                if (GetElementContentRect(elementRect, i).Contains(Event.current.mousePosition))
                {
                    return true;
                }
                elementYOffset += GetSeparatorHeight() + GetCachedElementHeight(i);
            }

            return false;
        }

        private void HandleCallbacks()
        {
            HandleSelectionCallbacks();

            HandleMouseUpCallbacks();

            HandleChangeCallbacks();

            HandleRefreshCallbacks();
        }

        private void HandleUserInput(Rect listRect)
        {
            HandleShowCreateMenu();

            HandleShowContextMenu();

            HandleDragAndDropDetection(listRect);

            HandleDraggingAndSelection(listRect);
            
            HandleScroll(listRect);
        }

        private void HandleSelectionCallbacks()
        {
            if (m_selectedIndicesThisFrame.Length > 0)
            {
                OnSelect(m_selectedIndicesThisFrame);
                OnSelectCallback.Invoke(m_selectedIndicesThisFrame);
                m_selectedIndicesThisFrame = new int[0];
            }
        }

        private void HandleMouseUpCallbacks()
        {
            if (m_mouseUpIndexThisFrame > -1)
            {
                OnMouseUp(m_mouseUpIndexThisFrame);
                m_mouseUpIndexThisFrame = -1;
            }
        }

        private void HandleChangeCallbacks()
        {
            if (m_changedThisFrame)
            {
                GUI.changed = true;
                OnChanged();
                m_changedThisFrame = false;
            }
        }

        private void HandleRefreshCallbacks()
        {
            if (m_refresh)
            {
                RefreshCachedValues();
                OnRefresh();
                m_refresh = false;
            }
        }

        private void HandleShowCreateMenu()
        {
            if (Event.current.type != EventType.Layout && Event.current.type != EventType.Repaint)
            {
                if (m_showCreateMenuNextFrame)
                {
                    ShowCreateMenu();
                    m_showCreateMenuNextFrame = false;
                }
            }
        }

        private void HandleShowContextMenu()
        {
            if (m_showContextMenuNextFrame)
            {
                m_showContextMenuNextFrame = false;
                ShowContextMenu();
            }
        }

        private void HandleDragAndDropDetection(Rect listRect)
        {
            if (s_dragAndDropSourceList != null && s_dragAndDropSourceList != this)
            {
                Rect dragAndDropBoundsRect = listRect;
                dragAndDropBoundsRect.yMin -= 8f;
                dragAndDropBoundsRect.yMax += 8f;

                if (s_dragAndDropTargetList == null)
                {
                    if (!dragAndDropBoundsRect.Overlaps(s_dragAndDropSourceList.m_elementsContentRect) && dragAndDropBoundsRect.Contains(Event.current.mousePosition) && !IsMouseOverAnyElementContent(listRect))
                    {
                        if (CanDropInsert(s_dragAndDropSourceList, s_dragAndDropSourceList.GetSelectedIndices()))
                        {
                            s_dragAndDropTargetList = this;
                        }
                    }
                }
            }
            else
            {
                if (DragAndDrop.objectReferences.Length > 0)
                {
                    if (!s_performedDrag)
                    {
                        if (s_dragAndDropTargetList == null)
                        {
                            Rect dragAndDropBoundsRect = listRect;
                            dragAndDropBoundsRect.yMin -= 8f;
                            dragAndDropBoundsRect.yMax += 8f;

                            if (dragAndDropBoundsRect.Contains(Event.current.mousePosition) && !IsMouseOverAnyElementContent(listRect))
                            {
                                if (CanDropInsert(DragAndDrop.objectReferences))
                                {
                                    s_dragAndDropTargetList = this;
                                }
                            }
                        }
                    }
                }
                else
                {
                    s_performedDrag = false;
                }
            }

            switch (Event.current.type)
            {
                case EventType.DragUpdated:
                    {
                        if (m_allowDropInsertingElements)
                        {
                            if (s_dragAndDropTargetList != null)
                            {
                                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                                Event.current.Use();
                            }
                        }

                        break;
                    }

                case EventType.DragPerform:
                    {
                        if (m_allowDropInsertingElements)
                        {
                            if (s_dragAndDropTargetList == this)
                            {
                                if (s_dragAndDropTargetIndex > -1)
                                {
                                    DoDropInsert(DragAndDrop.objectReferences, s_dragAndDropTargetIndex);

                                    s_dragAndDropSourceList = null;
                                    s_dragAndDropTargetList = null;
                                    s_dragAndDropTargetIndex = -1;

                                    DragAndDrop.AcceptDrag();

                                    s_performedDrag = true;

                                    Event.current.Use();
                                }
                            }
                        }

                        break;
                    }
            }
        }

        private static bool CanDropElementIntoList(object element, Type dstListElementType)
        {
            return ((element == null && (Nullable.GetUnderlyingType(dstListElementType) != null)) ||
                    (element != null && dstListElementType.IsAssignableFrom(element.GetType())));
        }

        private void HandleDraggingAndSelection(Rect listRect)
        {
            Event current = Event.current;
            EventType currentEventType = current.GetTypeForControl(m_controlID);
            EventType rawType = current.rawType;
            
            bool isMouseDragEvent = currentEventType == EventType.MouseDrag && current.button == 0;
            if (s_simulateMouseDragControlID == m_controlID && currentEventType == EventType.Repaint)
            {
                s_simulateMouseDragControlID = 0;
                isMouseDragEvent = true;
            }

            if (isMouseDragEvent)
            {
                if (GUIUtility.hotControl == m_controlID)
                {
                    if (!EditorGUI.actionKey && !current.shift && CanDrag())
                    {
                        m_isDragging = true;
                        
                        UpdateDraggedY(listRect);

                        if (m_willAutoScroll)
                        {
                            ScrollTowardMouse();
                        }
                    }
                }
            }

            if (rawType == EventType.MouseDown && currentEventType != EventType.MouseDown)
            {
                DoSelectNone();
                EditorWindow focusedWindow = EditorWindow.focusedWindow;
                if (focusedWindow != null)
                {
                    focusedWindow.Repaint();
                }
            }
            
            switch (currentEventType)
            {
                case EventType.Repaint:
                    {
                        if (m_allowDropInsertingElements)
                        {
                            if (s_dragAndDropTargetList == this)
                            {
                                Rect dragAndDropBoundsRect = listRect;
                                dragAndDropBoundsRect.yMin -= 8f;
                                dragAndDropBoundsRect.yMax += 8f;

                                if (dragAndDropBoundsRect.Contains(Event.current.mousePosition) && !IsMouseOverAnyElementContent(listRect))
                                {
                                    EditorGUIUtility.AddCursorRect(dragAndDropBoundsRect, MouseCursor.ArrowPlus);
                                    s_dragAndDropTargetIndex = GetSeparatorTargetRowIndex(listRect.y);
                                    Rect elementRect = GetRowRect(listRect, s_dragAndDropTargetIndex);
                                    DrawDragTarget(GetSeparatorDragTargetRect(elementRect), GetSeparatorDragTargetColor());
                                }
                                else
                                {
                                    s_dragAndDropTargetList = null;
                                    s_dragAndDropTargetIndex = -1;
                                }
                            }
                        }
                        break;
                    }
                case EventType.MouseDown:
                    {
                        if (listRect.Contains(Event.current.mousePosition))
                        {
                            EndEditingActiveTextField();

                            int clickedIndex = GetMouseRowIndex(listRect);
                            
                            HandleSelection(clickedIndex);

                            GrabControlFocus();
                            GrabKeyboardFocus();
                            if (Event.current.button == 0)
                            {
                                if (CanDrag())
                                {
                                    int firstSelectedIndex = GetFirstSelectedIndex();
                                    if (firstSelectedIndex >= 0)
                                    {
                                        m_dragOffset = Event.current.mousePosition.y - listRect.y - GetElementYOffset(firstSelectedIndex);
                                        UpdateDraggedY(listRect);
                                        s_dragAndDropSourceList = this;
                                        m_nonDragTargetIndices = new List<int>();
                                    }
                                }
                            }
                            current.Use();
                        }
                        else
                        {
                            HandleSelection(-1);
                        }
                        break;
                    }
                case EventType.MouseDrag:
                    {
                        if (GUIUtility.hotControl == m_controlID)
                        {
                            if (m_isDragging && CanDrag())
                            {
                                current.Use();
                            }
                            else
                            {
                                int clickedIndex = GetMouseRowIndex(listRect);

                                HandleSelection(clickedIndex);
                            }
                        }
                        break;
                    }
                case EventType.MouseUp:
                    {
                        if (Event.current.button == 0)
                        {
                            int clickedIndex = GetMouseRowIndex(listRect);

                            if (!m_allowReorderingElements || IsFiltering() || !GetSelectedIndices().ToList().Exists(i => CanMove(i)))
                            {
                                if (IsMouseInsideASelectedElement(listRect))
                                {
                                    if (clickedIndex > -1)
                                    {
                                        if (!EditorGUI.actionKey && !current.shift && !m_isDragging && m_clickedAlreadySelectedElementOnMouseDown)
                                        {
                                            DoSelect(clickedIndex);
                                        }
                                        InvokeOnMouseUp(clickedIndex, true);
                                        current.Use();
                                    }
                                }
                            }
                            else if (GUIUtility.hotControl == m_controlID)
                            {
                                try
                                {
                                    if (m_isDragging)
                                    {
                                        if (m_dragTargetIndex > -1)
                                        {
                                            DoMove(GetSelectedIndices().Where(i => CanMove(i)).ToArray(), m_dragTargetIndex);
                                        }
                                    }
                                    else
                                    {
                                        if (clickedIndex > -1)
                                        {
                                            if (!EditorGUI.actionKey && !current.shift && m_clickedAlreadySelectedElementOnMouseDown)
                                            {
                                                DoSelect(clickedIndex);
                                            }
                                            InvokeOnMouseUp(clickedIndex, true);
                                        }
                                    }
                                }
                                finally
                                {
                                    ReleaseControlFocus();
                                    m_nonDragTargetIndices = new List<int>();
                                    m_dragTargetIndex = -1;
                                }

                                current.Use();
                            }
                        }
                        m_isDragging = false;
                        m_clickedAlreadySelectedElementOnMouseDown = false;
                        if (m_allowDropInsertingElements)
                        {
                            if (s_dragAndDropSourceList != null && s_dragAndDropTargetList != null)
                            {
                                if (s_dragAndDropTargetIndex > -1)
                                {
                                    s_dragAndDropTargetList.DoDropInsert(s_dragAndDropSourceList, s_dragAndDropSourceList.GetSelectedIndices(), s_dragAndDropTargetIndex);
                                }
                            }
                        }
                        s_dragAndDropSourceList = null;
                        s_dragAndDropTargetList = null;
                        break;
                    }
                case EventType.KeyDown:
                    {
                        if (!HasKeyboardFocus())
                        {
                            return;
                        }

                        // DownArrow shortcut
                        if (current.keyCode == KeyCode.DownArrow)
                        {
                            int lastSelectedIndex = GetLastSelectedIndex();
                            if (lastSelectedIndex >= 0)
                            {
                                int nextSelectedIndex = lastSelectedIndex + 1;
                                if (nextSelectedIndex < GetElementCount())
                                {
                                    DoSelect(nextSelectedIndex);
                                    DoScrollTowardIndex(nextSelectedIndex);
                                    current.Use();
                                }
                            }
                        }

                        // UpArrow shortcut
                        if (current.keyCode == KeyCode.UpArrow)
                        {
                            int firstSelectedIndex = GetFirstSelectedIndex();
                            if (firstSelectedIndex >= 0)
                            {
                                int previousSelectedIndex = firstSelectedIndex - 1;
                                if (previousSelectedIndex >= 0)
                                {
                                    DoSelect(previousSelectedIndex);
                                    DoScrollTowardIndex(previousSelectedIndex);
                                    current.Use();
                                }
                            }
                        }

                        // Escape keyboard shortcut
                        if (current.keyCode == KeyCode.Escape && GUIUtility.hotControl == m_controlID)
                        {
                            ReleaseControlFocus();
                            m_isDragging = false;
                            current.Use();
                        }
                        break;
                    }
                case EventType.ValidateCommand:
                    {
                        if (!HasKeyboardFocus())
                        {
                            return;
                        }

                        // Delete keyboard shortcut
                        if (current.commandName == s_commandSoftDelete || current.commandName == s_commandDelete.text)
                        {
                            if (m_allowDeletingElements && m_selectedIndices.Count > 0 && GetSelectedIndices().ToList().Exists(i => CanDelete(i)))
                            {
                                current.Use();
                            }
                        }

                        // Cut keyboard shortcut
                        if (current.commandName == s_commandCut.text)
                        {
                            if (m_allowCutCopyPastingElements && m_allowDeletingElements && m_selectedIndices.Count > 0 && GetSelectedIndices().ToList().Exists(i => CanDelete(i)))
                            {
                                current.Use();
                            }
                        }

                        // Copy keyboard shortcut
                        if (current.commandName == s_commandCopy.text)
                        {
                            if (m_allowCutCopyPastingElements && m_selectedIndices.Count > 0)
                            {
                                current.Use();
                            }
                        }

                        // Paste keyboard shortcut
                        if (current.commandName == s_commandPaste.text)
                        {
                            if (m_allowCutCopyPastingElements && (m_selectedIndices.Count > 0 || GetElementCount() == 0))
                            {
                                current.Use();
                            }
                        }

                        // Duplicate keyboard shortcut
                        if (current.commandName == s_commandDuplicate.text)
                        {
                            if (m_allowDuplicatingElements && m_selectedIndices.Count > 0)
                            {
                                current.Use();
                            }
                        }

                        // SelectAll keyboard shortcut
                        if (current.commandName == s_commandSelectAll.text)
                        {
                            current.Use();
                        }
                        break;
                    }
                case EventType.ExecuteCommand:
                    {
                        if (!HasKeyboardFocus())
                        {
                            return;
                        }

                        // Delete keyboard shortcut
                        if (current.commandName == s_commandSoftDelete || current.commandName == s_commandDelete.text)
                        {
                            DoDelete(GetSelectedIndices().Where(i => CanDelete(i)).ToArray());
                            current.Use();
                        }

                        // Cut keyboard shortcut
                        if (current.commandName == s_commandCut.text)
                        {
                            DoCut(GetSelectedIndices().Where(i => CanDelete(i)).ToArray());
                            current.Use();
                        }

                        // Copy keyboard shortcut
                        if (current.commandName == s_commandCopy.text)
                        {
                            DoCopy(GetSelectedIndices().ToArray());
                            current.Use();
                        }

                        // Paste keyboard shortcut
                        if (current.commandName == s_commandPaste.text)
                        {
                            DoPaste(GetLastSelectedIndex() + 1);
                            current.Use();
                        }

                        // Duplicate keyboard shortcut
                        if (current.commandName == s_commandDuplicate.text)
                        {
                            DoDuplicate(GetSelectedIndices().ToArray());
                            current.Use();
                        }

                        // SelectAll keyboard shortcut
                        if (current.commandName == s_commandSelectAll.text)
                        {
                            DoSelectAll();
                            current.Use();
                        }
                        break;
                    }
                case EventType.ContextClick:
                    {
                        if (listRect.Contains(Event.current.mousePosition))
                        {
                            int clickedIndex = GetMouseRowIndex(listRect);

                            HandleSelection(clickedIndex);

                            EndEditingActiveTextField();

                            if (m_willDisplayContextMenu)
                            {
                                InvokeShowContextMenu();
                            }

                            current.Use();
                        }
                        break;
                    }
            }
        }

        private void HandleSelection(int clickedIndex)
        {
            if (clickedIndex > -1)
            {
                if (Event.current.shift)
                {
                    if (m_selectedIndices.Count > 0)
                    {
                        int firstSelectedIndex = GetFirstSelectedIndex();
                        int lastSelectedIndex = GetLastSelectedIndex();

                        if (m_firstSelectedIndex > -1)
                        {
                            if (clickedIndex < m_firstSelectedIndex)
                            {
                                firstSelectedIndex = clickedIndex;
                            }
                            else if (clickedIndex > m_firstSelectedIndex)
                            {
                                lastSelectedIndex = clickedIndex;
                            }
                        }

                        if (firstSelectedIndex != -1 && lastSelectedIndex != -1)
                        {
                            int startIndex = Math.Min(firstSelectedIndex, lastSelectedIndex);
                            int lastIndex = Math.Max(firstSelectedIndex, lastSelectedIndex);

                            DoSelectRange(startIndex, lastIndex - startIndex + 1);
                        }
                    }
                    else
                    {
                        DoSelect(clickedIndex);
                    }
                }
                else if (EditorGUI.actionKey)
                {
                    if (IsIndexSelected(clickedIndex))
                    {
                        DoRemoveFromCurrentSelection(clickedIndex);
                    }
                    else
                    {
                        DoAddToCurrentSelection(clickedIndex);
                    }
                }
                else
                {
                    if (!IsIndexSelected(clickedIndex))
                    {
                        DoSelect(clickedIndex);
                    }
                    else
                    {
                        m_clickedAlreadySelectedElementOnMouseDown = true;
                    }
                }
            }
            else
            {
                DoSelectNone();
                EditorWindow focusedWindow = EditorWindow.focusedWindow;
                if (focusedWindow != null)
                {
                    focusedWindow.Repaint();
                }
            }
        }

        public void ShowContextMenu()
        {
            GenericMenu menu = new GenericMenu();

            AddItemsToContextMenu(menu);

            if (menu.GetItemCount() > 0)
            {
                menu.ShowAsContext();
            }
        }

        private void ScrollTowardMouse()
        {
            const float triggerPaddingInPixels = 8f;
            const float maximumRangeInPixels = 4f;

            Vector2 mousePosition = Event.current.mousePosition;
            Rect mouseRect = new Rect(mousePosition.x - triggerPaddingInPixels, mousePosition.y - triggerPaddingInPixels, triggerPaddingInPixels * 2, triggerPaddingInPixels * 2);

            if (!ContainsRect(m_visibleRect, mouseRect))
            {
                if (mousePosition.y < m_visibleRect.center.y)
                {
                    mousePosition = new Vector2(mouseRect.xMin, mouseRect.yMin);
                }
                else
                {
                    mousePosition = new Vector2(mouseRect.xMax, mouseRect.yMax);
                }

                mousePosition.x = Mathf.Max(mousePosition.x - maximumRangeInPixels, mouseRect.xMax);
                mousePosition.y = Mathf.Min(mousePosition.y + maximumRangeInPixels, mouseRect.yMax);
                GUI.ScrollTo(new Rect(mousePosition.x, mousePosition.y, 1, 1));

                s_simulateMouseDragControlID = m_controlID;

                EditorWindow focusedWindow = EditorWindow.focusedWindow;
                if (focusedWindow != null)
                {
                    focusedWindow.Repaint();
                }
            }
        }

        private void ScrollTowardIndices(Rect listRect, int[] indices)
        {
            if (indices != null && indices.Length > 0)
            {
                Rect indicesRect = GetRowRangeRect(listRect, indices);

                if (!ContainsRect(m_visibleRect, indicesRect))
                {
                    float scrollPositionY = 0f;

                    if (indicesRect.y < m_visibleRect.y)
                    {
                        scrollPositionY = indicesRect.y;
                    }
                    else
                    {
                        scrollPositionY = indicesRect.yMax;
                    }
                    
                    GUI.ScrollTo(new Rect(indicesRect.x, scrollPositionY, 1, 1));

                    EditorWindow focusedWindow = EditorWindow.focusedWindow;
                    if (focusedWindow != null)
                    {
                        focusedWindow.Repaint();
                    }
                }
            }
        }
        
        private void SetCachedElementIsExpanded(int index, bool value)
        {
            if (m_cachedElementIsExpandedStates == null || m_cachedElementIsExpandedStates.Length != GetElementCount())
            {
                UpdateCachedElementIsExpandedStates();
            }
            if (m_cachedElementIsExpandedStates != null && index >= 0 && index < m_cachedElementIsExpandedStates.Length)
            {
                m_cachedElementIsExpandedStates[index] = value;
            }
        }

        private bool GetCachedIsExpanded(int index)
        {
            if (m_cachedElementIsExpandedStates == null || m_cachedElementIsExpandedStates.Length != GetElementCount())
            {
                UpdateCachedElementIsExpandedStates();
            }
            if (m_cachedElementIsExpandedStates != null && index >= 0 && index < m_cachedElementIsExpandedStates.Length)
            {
                return m_cachedElementIsExpandedStates[index];
            }
            return false;
        }

        private bool GetCachedElementCanCollapse(int index)
        {
            if (m_cachedElementCanCollapseStates == null || m_cachedElementCanCollapseStates.Length != GetElementCount())
            {
                UpdateCachedElementCanCollapseStates();
            }
            if (m_cachedElementCanCollapseStates != null && index >= 0 && index < m_cachedElementCanCollapseStates.Length)
            {
                return m_cachedElementCanCollapseStates[index];
            }
            return false;
        }

        private float GetCachedExpandedElementContentHeight(int index)
        {
            if (m_cachedElementExpandedContentHeights == null || m_cachedElementExpandedContentHeights.Length != GetElementCount())
            {
                UpdateCachedElementExpandedContentHeights();
            }
            if (m_cachedElementExpandedContentHeights != null && index >= 0 && index < m_cachedElementExpandedContentHeights.Length)
            {
                return m_cachedElementExpandedContentHeights[index];
            }
            return -1;
        }

        private float GetCachedCollapsedElementContentHeight(int index)
        {
            if (m_cachedElementCollapsedContentHeights == null || m_cachedElementCollapsedContentHeights.Length != GetElementCount())
            {
                UpdateCachedElementCollapsedContentHeights();
            }
            if (m_cachedElementCollapsedContentHeights != null && index >= 0 && index < m_cachedElementCollapsedContentHeights.Length)
            {
                return m_cachedElementCollapsedContentHeights[index];
            }
            return -1;
        }

        private float GetTargetYOffset(int index, int selectedIndex)
        {
            if (index > selectedIndex)
            {
                return GetElementYOffset(index) + GetCachedElementHeight(index) - GetCachedElementHeight(selectedIndex);
            }
            return GetElementYOffset(index);
        }

        private float GetElementYOffset(int index)
        {
            float num = 0f;
            for (int i = 0; i < index; i++)
            {
                num += GetCachedElementHeight(i);
                num += GetSeparatorHeight();
            }
            return num;
        }

        private float GetElementYOffset(int index, int selectedIndex)
        {
            float num = 0f;
            for (int i = 0; i < index; i++)
            {
                if (i != selectedIndex)
                {
                    if (i > 0)
                    {
                        num += GetSeparatorHeight();
                    }
                    num += GetCachedElementHeight(i);
                }
            }
            return num;
        }

        private Rect GetRowRect(Rect listRect, int index)
        {
            float separatorOffset = (index > 0) ? GetSeparatorHeight() : 0f;
            return new Rect(listRect.x, listRect.y + GetElementYOffset(index) + separatorOffset, listRect.width, GetCachedElementHeight(index));
        }

        public Rect GetRowRect(int index)
        {
            return GetRowRect(m_elementsContentRect, index);
        }

        private Rect GetRowRangeRect(Rect listRect, int[] indices)
        {
            int startIndex = 0;
            float height = 0f;
            if (indices.Length > 0)
            {
                startIndex = indices[0];
                int endIndex = indices[indices.Length - 1];
                for (int i = startIndex; i <= endIndex; i++)
                {
                    if (i > 0)
                    {
                        height += GetSeparatorHeight();
                    }
                    height += GetCachedElementHeight(i);
                }
            }
            return new Rect(listRect.x, listRect.y + GetElementYOffset(startIndex), listRect.width, height);
        }

        public Rect GetRowRangeRect(int[] indices)
        {
            return GetRowRangeRect(m_elementsContentRect, indices);
        }

        private float GetCachedElementsHeight()
        {
            int count = GetElementCount();
            float height = 0;
            if (count == 0)
            {
                height = (GetElementPadding() * 2f) + GetEmptyElementContentHeight();
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    if (IsFiltered(i))
                    {
                        continue;
                    }
                    if (i > 0)
                    {
                        height += GetSeparatorHeight();
                    }
                    height += (GetElementPadding() * 2f) + GetCachedElementContentHeight(i);
                }
            }

            height += GetFooterContentHeight();

            height += ContainerStyle.padding.top;
            height += ContainerStyle.padding.bottom;

            return height;
        }

        private static bool ContainsRect(Rect a, Rect b)
        {
            return a.Contains(new Vector2(b.xMin, b.yMin)) && a.Contains(new Vector2(b.xMax, b.yMax));
        }

        private bool IsMouseInsideASelectedElement(Rect listRect)
        {
            int hoverIndex = GetMouseRowIndex(listRect);
            return IsIndexSelected(hoverIndex) && GetRowRect(listRect, hoverIndex).Contains(Event.current.mousePosition);
        }

        private void UpdateDraggedY(Rect listRect)
        {
            m_draggedY = Mathf.Clamp(Event.current.mousePosition.y - listRect.y, m_dragOffset, listRect.height - (GetRowRangeRect(listRect, GetSelectedIndices()).height - m_dragOffset) + GetSeparatorHeight());
        }

        private int GetRowIndex(float localY)
        {
            float yMin = 0f;
            int[] visibleIndices = (IsFiltering()) ? GetSearchMatchIndices() : Enumerable.Range(0, GetElementCount()).ToArray();
            foreach (int index in visibleIndices)
            {
                float totalHeight = GetSeparatorHeight() + GetCachedElementHeight(index);
                float yMax = yMin + totalHeight;
                if (localY >= yMin && localY < yMax)
                {
                    return index;
                }
                yMin += totalHeight;
            }
            return GetElementCount() - 1;
        }

        private int GetMouseRowIndex(Rect listRect)
        {
            return GetRowIndex(Event.current.mousePosition.y - listRect.y);
        }

        private int GetSeparatorTargetRowIndex(float globalYMin)
        {
            float localMouseY = Event.current.mousePosition.y - globalYMin;

            float yMin = 0f;
            for (int k = 0; k < GetElementCount() + 1; k++)
            {
                float elementHeight = 0;
                if (k < GetElementCount())
                {
                    elementHeight = GetCachedElementHeight(k);
                }

                float totalHeight = GetSeparatorHeight() + elementHeight;
                float currentMid = yMin + (totalHeight / 2f);

                float prevMid = yMin;
                if (k - 1 >= 0)
                {
                    prevMid -= ((GetSeparatorHeight() + GetCachedElementHeight(k - 1)) / 2f);
                }

                if (localMouseY >= prevMid && localMouseY < currentMid)
                {
                    if (k < GetElementCount())
                    {
                        return k;
                    }
                }

                yMin += totalHeight;
            }

            if (localMouseY < 0)
            {
                return 0;
            }

            return GetElementCount();
        }

        private int GetBoxTargetRowIndex(Rect listRect, float localYMin)
        {
            int index = GetFirstSelectedIndex();

            float selectedElementHeight = GetRowRangeRect(listRect, GetSelectedIndices()).height;
            float localYMax = localYMin + selectedElementHeight;

            float yMin = 0f;
            for (int i = 0; i < GetElementCount(); i++)
            {
                float totalHeight = GetSeparatorHeight() + GetCachedElementHeight(i);
                float yMax = yMin + totalHeight;
                float currentMid = yMin + (totalHeight / 2f);

                if (localYMin < GetElementYOffset(index))
                {
                    float prevMid = yMin;
                    if (i - 1 >= 0)
                    {
                        prevMid -= ((GetSeparatorHeight() + GetCachedElementHeight(i - 1)) / 2f);
                    }
                    if (localYMin >= prevMid && localYMin < currentMid)
                    {
                        return i;
                    }
                }
                else
                {
                    float nextMid = yMax;
                    if (i + 1 < GetElementCount())
                    {
                        nextMid += ((GetSeparatorHeight() + GetCachedElementHeight(i + 1)) / 2f);
                    }
                    if (localYMax > currentMid && localYMax <= nextMid)
                    {
                        return i;
                    }
                }
                yMin += totalHeight;
            }
            
            if (localYMin < 0)
            {
                return 0;
            }

            return -1;
        }

        private void EndEditingActiveTextField()
        {
            GUI.FocusControl("");
        }
        #endregion
    }

}

#endif
