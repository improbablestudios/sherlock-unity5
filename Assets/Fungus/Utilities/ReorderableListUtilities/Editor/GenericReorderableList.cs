#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SerializedPropertyUtilities;

namespace ImprobableStudios.ReorderableListUtilities
{

    public class GenericReorderableList : ReorderableList
    {
        public delegate object ElementDrawerDelegate(Rect position, object value, GUIContent label);

        public delegate float ElementHeightDelegate(object value, GUIContent label);

        public delegate UnityEngine.Object ObjectFieldDelegate(Rect position, UnityEngine.Object obj, Type objType, bool allowSceneObjects);

        protected ElementDrawerDelegate m_expandedElementDrawerCallback;

        protected ElementHeightDelegate m_expandedElementHeightCallback;

        protected ElementDrawerDelegate m_collapsedElementDrawerCallback;

        protected ElementHeightDelegate m_collapsedElementHeightCallback;

        protected ObjectFieldDelegate m_objectFieldCallback;

        protected IList m_list;

        protected UnityEngine.Object m_targetObject;

        private GenericReorderableListCache m_reorderableListCache;

        public IList List
        {
            get
            {
                return m_list;
            }
        }

        public ElementDrawerDelegate ExpandedElementDrawerCallback
        {
            get
            {
                return m_expandedElementDrawerCallback;
            }
            set
            {
                m_expandedElementDrawerCallback = value;
                m_reorderableListCache.ExpandedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate ExpandedElementHeightCallback
        {
            get
            {
                return m_expandedElementHeightCallback;
            }
            set
            {
                m_expandedElementHeightCallback = value;
                m_reorderableListCache.ExpandedElementHeightCallback = value;
            }
        }

        public ElementDrawerDelegate CollapsedElementDrawerCallback
        {
            get
            {
                return m_collapsedElementDrawerCallback;
            }
            set
            {
                m_collapsedElementDrawerCallback = value;
                m_reorderableListCache.CollapsedElementDrawerCallback = value;
            }
        }

        public ElementHeightDelegate CollapsedElementHeightCallback
        {
            get
            {
                return m_collapsedElementHeightCallback;
            }
            set
            {
                m_collapsedElementHeightCallback = value;
                m_reorderableListCache.CollapsedElementHeightCallback = value;
            }
        }

        public ObjectFieldDelegate ObjectFieldCallback
        {
            get
            {
                return m_objectFieldCallback;
            }
            set
            {
                m_objectFieldCallback = value;
                m_reorderableListCache.ObjectFieldCallback = value;
            }
        }

        public GenericReorderableListCache ReorderableListCache
        {
            get
            {
                return m_reorderableListCache;
            }
            set
            {
                m_reorderableListCache = value;
            }
        }

        public GenericReorderableList() : base()
        {
            m_reorderableListCache = new GenericReorderableListCache();
            SetFlags(0);
            Init();
        }

        public GenericReorderableList(ReorderableListFlags flags) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(flags);
            SetFlags(flags);
            Init();
        }

        public GenericReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(elementDrawerCallback, elementHeightCallback);
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(flags);
            Init();
        }

        public GenericReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(elementDrawerCallback, elementHeightCallback);
            m_expandedElementDrawerCallback = elementDrawerCallback;
            m_expandedElementHeightCallback = elementHeightCallback;
            SetFlags(0);
            Init();
        }

        public GenericReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(
                expandedElementDrawerCallback, expandedElementHeightCallback,
                collapsedElementDrawerCallback, collapsedElementHeightCallback);
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(0);
            Init();
        }

        public GenericReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(
                flags,
                expandedElementDrawerCallback, expandedElementHeightCallback,
                collapsedElementDrawerCallback, collapsedElementHeightCallback);
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            SetFlags(flags);
            Init();
        }

        public GenericReorderableList(ReorderableListFlags flags, ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(flags, objectFieldCallback);
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(flags);
            Init();
        }

        public GenericReorderableList(ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(objectFieldCallback);
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(0);
            Init();
        }

        public GenericReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback,
            ObjectFieldDelegate objectFieldCallback) : base()
        {
            m_reorderableListCache = new GenericReorderableListCache(
                flags,
                expandedElementDrawerCallback, expandedElementHeightCallback,
                collapsedElementDrawerCallback, collapsedElementHeightCallback,
                objectFieldCallback);
            m_expandedElementDrawerCallback = expandedElementDrawerCallback;
            m_expandedElementHeightCallback = expandedElementHeightCallback;
            m_collapsedElementDrawerCallback = collapsedElementDrawerCallback;
            m_collapsedElementHeightCallback = collapsedElementHeightCallback;
            m_objectFieldCallback = objectFieldCallback;
            SetFlags(flags);
            Init();
        }

        public virtual void Init(IList list, UnityEngine.Object targetObject = null)
        {
            m_list = list;
            m_targetObject = targetObject;
        }
        
        public void Draw(IList list, UnityEngine.Object targetObject = null, params GUILayoutOption[] options)
        {
            Init(list, targetObject);
            base.Draw(options);
        }

        public void Draw(IList list, GUIContent label, UnityEngine.Object targetObject = null, params GUILayoutOption[] options)
        {
            Init(list, targetObject);
            base.Draw(label, options);
        }

        public void Draw(IList list, GUIStyle guiStyle, UnityEngine.Object targetObject = null, params GUILayoutOption[] options)
        {
            Init(list, targetObject);
            base.Draw(guiStyle, options);
        }

        public void Draw(IList list, GUIContent label, GUIStyle guiStyle, UnityEngine.Object targetObject = null, params GUILayoutOption[] options)
        {
            Init(list, targetObject);
            base.Draw(label, guiStyle, options);
        }

        public void Draw(Rect position, IList list, UnityEngine.Object targetObject = null)
        {
            Init(list, targetObject);
            base.Draw(position);
        }

        public void Draw(Rect position, IList list, GUIContent label, UnityEngine.Object targetObject = null)
        {
            Init(list, targetObject);
            base.Draw(position, label);
        }

        public override int GetElementCount()
        {
            if (m_list == null)
            {
                return 0;
            }

            return m_list.Count;
        }

        public override Type GetElementType()
        {
            Type listType = m_list.GetType();
            return listType.GetItemType();
        }

        public override object GetElementValue(int index)
        {
            return m_list[index];
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            object value = GetElementValue(index);
            if (value != null)
            {
                return new string[] { value.ToString() };
            }
            return new string[0];
        }
        
        protected override void Insert(int index, object element)
        {
            m_list.Insert(index, element);
        }

        protected override object[] Copy(int[] indices)
        {
            object[] copies = new object[indices.Length];

            for (int i = 0; i < copies.Length; i++)
            {
                int indexToCopy = indices[i];

                object copy = m_list[indexToCopy];

                ICloneable existingItem = copy as ICloneable;
                if (existingItem != null)
                {
                    copy = existingItem.Clone();
                }

                copies[i] = copy;
            }
            
            return copies;
        }

        protected override void Paste(int index, object[] copyBuffer)
        {
            foreach (object copy in copyBuffer)
            {
                m_list.Insert(index, copy);
                index++;
            }
        }
        
        protected override void Delete(int[] indices)
        {
            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];

                if (m_list.GetType().IsArray)
                {
                    IList newList = Activator.CreateInstance(m_list.GetType(), m_list.Count - 1) as IList;
                    for (int j = 0; j < m_list.Count; j++)
                    {
                        if (j < index)
                        {
                            newList[j] = m_list[j];
                        }
                        else if (j > index)
                        {
                            newList[j - 1] = m_list[j];
                        }
                    }
                    m_list = newList;
                }
                else
                {
                    m_list.RemoveAt(index);
                }
            }
        }

        protected override void Move(int srcIndex, int dstIndex)
        {
            object value = m_list[srcIndex];
            for (int i = 0; i < m_list.Count - 1; i++)
            {
                if (i >= srcIndex)
                {
                    m_list[i] = m_list[i + 1];
                }
            }
            for (int j = m_list.Count - 1; j > 0; j--)
            {
                if (j > dstIndex)
                {
                    m_list[j] = m_list[j - 1];
                }
            }
            m_list[dstIndex] = value;
        }
        
        public override GUIContent GetDefaultHeaderLabel()
        {
            Type elementType = GetElementType();

            return new GUIContent(ObjectNames.NicifyVariableName(elementType.Name) + " " + "List");
        }

        public override bool CanCollapse(int index)
        {
            return false;
        }

        protected virtual UnityEngine.Object ObjectField(Rect position, UnityEngine.Object obj, Type objType, bool allowSceneObjects)
        {
            if (m_objectFieldCallback != null)
            {
                return m_objectFieldCallback(position, obj, objType, allowSceneObjects);
            }
            else
            {
                return EditorGUI.ObjectField(position, obj, objType, allowSceneObjects);
            }
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            if (m_expandedElementDrawerCallback != null)
            {
                m_list[index] = m_expandedElementDrawerCallback(position, m_list[index], GUIContent.none);
            }
            else
            {
                if (typeof(IList).IsAssignableFrom(GetElementType()))
                {
                    GenericReorderableList reorderableList = m_reorderableListCache[index.ToString()];
                    position.height = reorderableList.GetHeight();
                    reorderableList.Draw(position, GUIContent.none);
                }
                else
                {
                    Type itemType = m_list.GetType().GetItemType();
                    m_list[index] = ExtendedEditorGUI.ValueField(position, m_list[index], itemType, GUIContent.none);
                }
            }
        }

        protected override void DrawElementCollapsedContent(Rect position, int index)
        {
            if (m_collapsedElementDrawerCallback != null)
            {
                m_list[index] = m_collapsedElementDrawerCallback(position, m_list[index], GUIContent.none);
            }
            else
            {
                if (typeof(IList).IsAssignableFrom(GetElementType()))
                {
                    GenericReorderableList reorderableList = m_reorderableListCache[index.ToString()];
                    position.height = reorderableList.GetHeight();
                    reorderableList.Draw(position, GUIContent.none);
                }
                else
                {
                    Type itemType = m_list.GetType().GetItemType();
                    m_list[index] = ExtendedEditorGUI.ValueField(position, m_list[index], itemType, GUIContent.none);
                }
            }
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            Type elementType = GetElementType();

            if (m_expandedElementHeightCallback != null)
            {
                return m_expandedElementHeightCallback(m_list[index], GUIContent.none);
            }
            else
            {
                if (typeof(IList).IsAssignableFrom(GetElementType()))
                {
                    GenericReorderableList reorderableList = m_reorderableListCache[index.ToString()];
                    return reorderableList.GetHeight();
                }
                else
                {
                    return EditorGUI.GetPropertyHeight(elementType.ConvertTypeToSerializedPropertyType(), GUIContent.none);
                }
            }
        }

        protected override float GetElementCollapsedContentHeight(int index)
        {
            Type elementType = GetElementType();

            if (m_collapsedElementHeightCallback != null)
            {
                GenericReorderableList reorderableList = m_reorderableListCache[index.ToString()];
                return reorderableList.GetHeight();
            }
            else
            {
                return EditorGUI.GetPropertyHeight(elementType.ConvertTypeToSerializedPropertyType(), GUIContent.none);
            }
        }
    }

}
#endif
