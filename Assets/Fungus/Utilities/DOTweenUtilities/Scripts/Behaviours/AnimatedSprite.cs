﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using DG.Tweening;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.DOTweenUtilities
{

    [AddComponentMenu("Fungus/Behaviours/Sprite/Animated Sprite")]
    public class AnimatedSprite : PersistentBehaviour
    {
        [Tooltip("If true, the animation will automatically start playing on awake")]
        [SerializeField]
        public bool m_PlayOnEnable = true;
        
        [Tooltip("The speed of the animation (in frames/second)")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("fps")]
        [FormerlySerializedAs("m_Fps")]
        protected int m_FramesPerSecond = 24;

        [Tooltip("The number of times this animation will loop. Setting to -1 will make the animation loop infinitely")]
        [Min(-1)]
        [SerializeField]
        [FormerlySerializedAs("loop")]
        protected int m_LoopCount = -1;

        [Tooltip("The loop type")]
        [SerializeField]
        [FormerlySerializedAs("loopType")]
        protected LoopType m_LoopType;

        [Tooltip("The sprite frames that make up this animation")]
        [SerializeField]
        [FormerlySerializedAs("frames")]
        [CheckNotEmpty]
        protected Sprite[] m_Frames = new Sprite[0];

        protected Tweener m_tweener;

        private SpriteRenderer m_spriteRenderer;

        private Image m_image;

        public bool PlayOnEnable
        {
            get
            {
                return m_PlayOnEnable;
            }
            set
            {
                m_PlayOnEnable = value;
            }
        }
        
        public int FramesPerSecond
        {
            get
            {
                return m_FramesPerSecond;
            }
            set
            {
                m_FramesPerSecond = value;
            }
        }
        
        public int LoopCount
        {
            get
            {
                return m_LoopCount;
            }
            set
            {
                m_LoopCount = value;
            }
        }

        public LoopType LoopType
        {
            get
            {
                return m_LoopType;
            }
            set
            {
                m_LoopType = value;
            }
        }

        public Sprite[] Frames
        {
            get
            {
                return m_Frames;
            }
            set
            {
                m_Frames = value;
            }
        }

        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (m_spriteRenderer == null)
                {
                    m_spriteRenderer = GetComponent<SpriteRenderer>();
                }
                return m_spriteRenderer;
            }
        }

        public Image Image
        {
            get
            {
                if (m_image == null)
                {
                    m_image = GetComponent<Image>();
                }
                return m_image;
            }
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_PlayOnEnable)
            {
                PlayAnimationForward();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_tweener != null)
            {
                m_tweener.Kill();
                m_tweener = null;
            }
        }

        public void PlayAnimationForward()
        {
            DOAnimation(true);
        }

        public void PlayAnimationBackwards()
        {
            DOAnimation(false);
        }

        private void DOAnimation(bool forward = true)
        {
            if (SpriteRenderer != null)
            {
                m_tweener = SpriteRenderer.DOFrameAnimation(m_Frames, m_FramesPerSecond)
                    .SetEase(Ease.Linear)
                    .SetLoops(m_LoopCount, m_LoopType);
                if (forward)
                {
                    m_tweener.PlayForward();
                }
                else
                {
                    m_tweener.PlayBackwards();
                }
            }
            if (Image != null)
            {
                m_tweener = Image.DOFrameAnimation(m_Frames, m_FramesPerSecond)
                    .SetEase(Ease.Linear)
                    .SetLoops(m_LoopCount, m_LoopType);
                if (forward)
                {
                    m_tweener.PlayForward();
                }
                else
                {
                    m_tweener.PlayBackwards();
                }
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_LoopType))
            {
                if (m_LoopCount == 0)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
