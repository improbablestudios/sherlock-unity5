﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins;
using UnityEngine;

namespace ImprobableStudios.DOTweenUtilities
{

    public static class DODrawExtensions
    {
        public static Sequence DODraw(this LineRenderer target, float duration)
        {
            Vector3[] path = new Vector3[target.positionCount];
            target.GetPositions(path);

            return DODraw(target, path, duration);
        }

        public static Sequence DODraw(this LineRenderer target, Vector3[] path, float duration)
        {
            Sequence s = DOTween.Sequence();

            if (duration > 0)
            {
                target.positionCount = 1;

                for (int i = 0; i < path.Length - 1; i++)
                {
                    s.Append(DODrawSegment(target, duration / path.Length, i, path[i], path[i + 1]).SetEase(Ease.Linear));
                }
            }

            return s.SetTarget(target);
        }

        private static Tweener DODrawSegment(this LineRenderer target, float duration, int startPositionIndex, Vector3 startPosition, Vector3 endPosition)
        {
            DOGetter<Vector3> getter = () => startPosition;
            DOSetter<Vector3> setter = 
                x => 
                {
                    target.positionCount = startPositionIndex + 2;
                    target.SetPosition(startPositionIndex + 1, x);
                };

            return DOTween.To(new Vector3Plugin(), getter, setter, endPosition, duration).SetTarget(target);
        }

        public static Tweener DOStartColor(this LineRenderer target, Color endValue, float duration)
        {
            return DOTween.To(() => target.startColor, x => target.startColor = x, endValue, duration).SetTarget(target);
        }

        public static Tweener DOEndColor(this LineRenderer target, Color endValue, float duration)
        {
            return DOTween.To(() => target.endColor, x => target.endColor = x, endValue, duration).SetTarget(target);
        }

    }

}
