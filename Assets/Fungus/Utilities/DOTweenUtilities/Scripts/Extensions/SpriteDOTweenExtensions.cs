﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.DOTweenUtilities
{
    
    public static class DOFrameAnimationExtentions
    {
        public static Tweener DOFrameAnimation(this SpriteRenderer target, Sprite[] frames, int fps)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.sprite = frames[x];
            int endValue = frames.Length - 1;
            float duration = (1f / fps) * frames.Length;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this SpriteRenderer target, Sprite[] frames, float duration)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.sprite = frames[x];
            int endValue = frames.Length - 1;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this Image target, Sprite[] frames, int fps)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.sprite = frames[x];
            int endValue = frames.Length - 1;
            float duration = (1f / fps) * frames.Length;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this Image target, Sprite[] frames, float duration)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.sprite = frames[x];
            int endValue = frames.Length - 1;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this ISpriteComponent target, Sprite[] frames, int fps)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.SetSprite(frames[x]);
            int endValue = frames.Length - 1;
            float duration = (1f / fps) * frames.Length;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this ISpriteComponent target, Sprite[] frames, float duration)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.SetSprite(frames[x]);
            int endValue = frames.Length - 1;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this Component target, Sprite[] frames, int fps)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.SetSprite(frames[x]);
            int endValue = frames.Length - 1;
            float duration = (1f / fps) * frames.Length;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }

        public static Tweener DOFrameAnimation(this Component target, Sprite[] frames, float duration)
        {
            int frameNumber = 0;
            DOGetter<int> getter = () => frameNumber;
            DOSetter<int> setter = x => target.SetSprite(frames[x]);
            int endValue = frames.Length - 1;
            return DOTween.To(new IntPlugin(), getter, setter, endValue, duration).SetTarget(target);
        }
    }

}