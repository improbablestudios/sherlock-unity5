#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.DOTweenUtilities
{

    [CanEditMultipleObjects, CustomEditor(typeof(AnimatedSprite), true)]
    public class AnimatedSpriteEditor : PersistentBehaviourEditor
    {
        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            AnimatedSprite t = target as AnimatedSprite;

            if (property.name == "m_" + nameof(t.Frames))
            {
                base.DrawProperty(property, label);

                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("Load Frames From Sprite Sheet"))
                    {
                        string texturePath = EditorUtility.OpenFilePanelWithFilters("Sprite Sheet Texture", "", new string[] { "Image files", "png,jpg,jpeg" });

                        if (!string.IsNullOrEmpty(texturePath))
                        {
                            t.Frames = AssetDatabase.LoadAllAssetsAtPath(texturePath.Replace(Application.dataPath, "Assets")).OfType<Sprite>().ToArray();
                            EditorUtility.SetDirty(t);
                        }
                    }

                    if (GUILayout.Button("Load Frames From Folder"))
                    {
                        string folderPath = EditorUtility.OpenFolderPanel("Sprite Sheet Texture", "", "");

                        if (!string.IsNullOrEmpty(folderPath))
                        {
                            List<Sprite> sprites = new List<Sprite>();
                            string[] fileEntries = Directory.GetFiles(folderPath);
                            foreach (string fileName in fileEntries)
                            {
                                int assetPathIndex = fileName.IndexOf("Assets", StringComparison.Ordinal);
                                string localPath = fileName.Substring(assetPathIndex);

                                Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(localPath);

                                if (sprite != null)
                                {
                                    sprites.Add(sprite);
                                }
                            }
                            t.Frames = sprites.ToArray();
                            EditorUtility.SetDirty(t);
                        }
                    }

                    if (GUILayout.Button("Clear All Frames"))
                    {
                        t.Frames = new Sprite[0];
                        EditorUtility.SetDirty(t);
                    }
                }
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }
    }

}

#endif