#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace ImprobableStudios.SerializationUtilities
{

    public class SerializationMenuItems
    {
        [MenuItem("Serialization/Reserialize All", false, 0)]
        static void ReserializeAll()
        {
            AssetDatabase.ForceReserializeAssets();
        }

        [MenuItem("Serialization/Reserialize Current Scene", false, 0)]
        static void ReserializeCurrentScene()
        {
            string path = EditorSceneManager.GetActiveScene().path;
            ReserializeAssets(new string[] { path });
        }
        
        [MenuItem("Serialization/Reserialize Selected Assets", false, 0)]
        static void ReserializeSelectedAssets()
        {
            ReserializeSelectedAssetsOfType(null);
        }

        [MenuItem("Serialization/Set Dirty Current Scene", false, 200)]
        static void SetDirtyCurrentScene()
        {
            string path = EditorSceneManager.GetActiveScene().path;
            SetDirtyAssets(new string[] { path });
        }

        [MenuItem("Serialization/Set Dirty Selected Assets", false, 200)]
        static void SetDirtySelectedAssets()
        {
            SetDirtySelectedAssetsOfType(null);
        }

        public static void ReserializeAllAssetsOfType(Type assetType)
        {
            string searchString = "";
            if (assetType != null)
            {
                searchString = "t:" + assetType.Name;
            }
            IEnumerable<string> paths = AssetDatabase.FindAssets(searchString, new string[] { "Assets" }).Select(x => AssetDatabase.GUIDToAssetPath(x));
            ReserializeAssets(paths);
        }
        
        public static void SetDirtyAllAssetsOfType(Type assetType)
        {
            string searchString = "";
            if (assetType != null)
            {
                searchString = "t:" + assetType.Name;
            }
            IEnumerable<string> paths = AssetDatabase.FindAssets(searchString, new string[] { "Assets" }).Select(x => AssetDatabase.GUIDToAssetPath(x));
            SetDirtyAssets(paths);
        }
        
        public static void ReserializeSelectedAssetsOfType(Type assetType)
        {
            if (assetType == null)
            {
                assetType = typeof(UnityEngine.Object);
            }
            IEnumerable<string> paths = Selection.GetFiltered(assetType, SelectionMode.DeepAssets).Select(x => AssetDatabase.GetAssetPath(x));
            ReserializeAssets(paths);
        }

        public static void SetDirtySelectedAssetsOfType(Type assetType)
        {
            if (assetType == null)
            {
                assetType = typeof(UnityEngine.Object);
            }
            IEnumerable<string> paths = Selection.GetFiltered(assetType, SelectionMode.DeepAssets).Select(x => AssetDatabase.GetAssetPath(x));
            SetDirtyAssets(paths);
        }

        public static void ReserializeAssets(IEnumerable<string> paths)
        {
            AssetDatabase.ForceReserializeAssets(paths, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        }

        public static void SetDirtyAssets(IEnumerable<string> paths)
        {
            foreach (string path in paths)
            {
                EditorUtility.SetDirty(AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path));
            }
        }
    }

}

#endif