﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.SerializationUtilities
{
    
    public static class UnityValueSerializer
    {
        [Serializable]
        public class Wrapper<T>
        {
            public T Value;

            public Wrapper()
            {
            }

            public Wrapper(T value)
            {
                Value = value;
            }
        }

        private static string m_wrapperPrefix;

        private static string m_wrapperSuffix;

        private static string WrapperPrefix
        {
            get
            {
                if (m_wrapperPrefix == null)
                {
                    m_wrapperPrefix = GetWrapperPrefix();
                }
                return m_wrapperPrefix;
            }
        }

        private static string WrapperSuffix
        {
            get
            {
                if (m_wrapperSuffix == null)
                {
                    m_wrapperSuffix = GetWrapperSuffix();
                }
                return m_wrapperSuffix;
            }
        }

        public static string Serialize(Type type, object value)
        {
            return Serialize(type, value, "Serialize");
        }

#if UNITY_EDITOR
        public static string EditorSerialize(Type type, object value)
        {
            return Serialize(type, value, "EditorSerialize");
        }
#endif

        private static string Serialize(Type type, object value, string methodName)
        {
            if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                return Serialize((UnityEngine.Object)value);
            }
            
            return (string)typeof(UnityValueSerializer).InvokeGenericMethod(methodName, BindingFlags.Public | BindingFlags.Static, type, null, value);
        }

        public static string Serialize<T>(T value)
        {
            return Serialize<T>(value, JsonUtility.ToJson);
        }

#if UNITY_EDITOR
        public static string EditorSerialize<T>(T value)
        {
            return Serialize<T>(value, UnityEditor.EditorJsonUtility.ToJson);
        }
#endif

        private static string Serialize<T>(T value, Func<object, string> serializer)
        {
            if (typeof(Type).IsAssignableFrom(typeof(T)))
            {
                Type type = value as Type;
                string typeName = "";
                if (type != null)
                {
                    typeName = type.AssemblyQualifiedName;
                }
                return SerializeWrapperValue(typeName, serializer);
            }
            else if (typeof(IList).IsAssignableFrom(typeof(T)) && typeof(Type).IsAssignableFrom(typeof(T).GetItemType()))
            {
                List<string> typeNames = new List<string>();
                if (value != null)
                {
                    IList list = value as IList;
                    foreach (Type type in list)
                    {
                        string typeName = "";
                        if (type != null)
                        {
                            typeName = type.AssemblyQualifiedName;
                        }
                        typeNames.Add(typeName);
                    }
                }
                return SerializeWrapperValue(typeNames, serializer);
            }

            return SerializeWrapperValue(value, serializer);
        }

        private static string SerializeWrapperValue<T>(T value, Func<object, string> serializer)
        {
            Wrapper<T> wrapper = new Wrapper<T>(value);

            string wrappedJson = null;

            wrappedJson = serializer(wrapper);

            if (wrappedJson.Length > 2)
            {
                return UnwrapJson(wrappedJson);
            }
            else
            {
                return "";
            }
        }

        public static object Deserialize(Type type, string json)
        {
            return Deserialize(type, json, "Deserialize");
        }

#if UNITY_EDITOR
        public static object EditorDeserialize(Type type, string json)
        {
            return Deserialize(type, json, "EditorDeserialize");
        }
#endif

        private static object Deserialize(Type type, string json, string methodName)
        {
            if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                return Deserialize<UnityEngine.Object>(json);
            }
            
            return typeof(UnityValueSerializer).InvokeGenericMethod(methodName, BindingFlags.Public | BindingFlags.Static, type, null, json);
        }

        public static T Deserialize<T>(string json)
        {
            return Deserialize<T>(json, JsonUtility.FromJson);
        }

#if UNITY_EDITOR
        public static T EditorDeserialize<T>(string json)
        {
            return Deserialize<T>(json, EditorFromJson);
        }
        
        private static object EditorFromJson(string json, Type type)
        {
            object obj = Activator.CreateInstance(type);

#if UNITY_EDITOR
            UnityEditor.EditorJsonUtility.FromJsonOverwrite(json, obj);
#endif
            return obj;
        }
#endif

        private static T Deserialize<T>(string json, Func<string, Type, object> deserializer)
        {
            if (string.IsNullOrEmpty(json))
            {
                return default(T);
            }

            if (typeof(Type).IsAssignableFrom(typeof(T)))
            {
                string typeName = DeserializeWrapperValue<string>(json, deserializer);
                object obj = Type.GetType(typeName, false);
                if (obj == null)
                {
                    return default(T);
                }
                else
                {
                    return (T)obj;
                }
            }
            else if (typeof(IList).IsAssignableFrom(typeof(T)) && typeof(Type).IsAssignableFrom(typeof(T).GetItemType()))
            {
                List<string> typeNames = DeserializeWrapperValue<List<string>>(json, deserializer);
                object obj = typeNames.Select(x => Type.GetType(x, false)).ToList();
                return (T)obj;
            }

            return DeserializeWrapperValue<T>(json, deserializer);
        }

        private static T DeserializeWrapperValue<T>(string json, Func<string, Type, object> deserializer)
        {
            string wrappedJson = WrapJson(json);
            try
            {
                Wrapper<T> wrapper = null;
                wrapper = deserializer(wrappedJson, typeof(Wrapper<T>)) as Wrapper<T>;
                object value = wrapper.Value;
                if (value == null)
                {
                    return default(T);
                }
                else
                {
                    return (T)value;
                }
            }
            catch
            {
                return default(T);
            }
        }

        private static string GetWrapperPrefix()
        {
            Wrapper<int> wrapper = new Wrapper<int>(0);
            string str = 0.ToString();
            string wrappedJson = JsonUtility.ToJson(wrapper);
            int prefixEndIndex = wrappedJson.IndexOf(str, StringComparison.Ordinal);
            return wrappedJson.Remove(prefixEndIndex);
        }

        private static string GetWrapperSuffix()
        {
            Wrapper<int> wrapper = new Wrapper<int>(0);
            string str = 0.ToString();
            string wrappedJson = JsonUtility.ToJson(wrapper);
            int suffixStartIndex = wrappedJson.IndexOf(str, StringComparison.Ordinal) + 1;
            return wrappedJson.Substring(suffixStartIndex);
        }

        private static string WrapJson(string json)
        {
            string prefix = WrapperPrefix;
            string suffix = WrapperSuffix;
            return prefix + json + suffix;
        }

        private static string UnwrapJson(string wrappedJson)
        {
            string prefix = WrapperPrefix;
            string suffix = WrapperSuffix;
            return wrappedJson.Substring(prefix.Length, wrappedJson.Length - prefix.Length - suffix.Length);
        }
    }

}
