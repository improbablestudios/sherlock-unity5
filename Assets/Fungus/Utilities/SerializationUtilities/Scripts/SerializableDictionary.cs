﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.SerializationUtilities
{

    [Serializable]
    public class StringStringDict : SerializableDictionary<string, string>
    {
        public StringStringDict()
        {
        }

        public StringStringDict(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField]
        [FormerlySerializedAs("keys")]
        List<TKey> m_keys = new List<TKey>();

        [SerializeField]
        [FormerlySerializedAs("values")]
        List<TValue> m_values = new List<TValue>();

        public SerializableDictionary()
        {
        }

        public SerializableDictionary(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        // save the dictionary to lists
        public void OnBeforeSerialize()
        {
            if (m_keys == null || m_values == null)
            {
                return;
            }
            m_keys.Clear();
            m_values.Clear();
            foreach (KeyValuePair<TKey, TValue> pair in this)
            {
                m_keys.Add(pair.Key);
                m_values.Add(pair.Value);
            }
        }

        // load dictionary from lists
        public void OnAfterDeserialize()
        {
            this.Clear();

            if (m_keys.Count != m_values.Count)
            {
                throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable.", m_keys.Count, m_values.Count));
            }

            for (int i = 0; i < m_keys.Count; i++)
            {
                this.Add(m_keys[i], m_values[i]);
            }
        }
    }

}