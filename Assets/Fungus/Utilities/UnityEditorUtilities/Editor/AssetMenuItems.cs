#if UNITY_EDITOR
using UnityEditor;

namespace ImprobableStudios.UnityEditorUtilities
{

    public static class AssetMenuItems
    {
        [MenuItem("Assets/Duplicate", false, 19)]
        public static void Duplicate()
        {
            EditorWindow.focusedWindow.SendEvent (EditorGUIUtility.CommandEvent ("Duplicate"));
        }
    }

}
#endif