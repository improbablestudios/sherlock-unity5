#if UNITY_EDITOR
using System;
using System.Reflection;
using UnityEngine;

namespace ImprobableStudios.UnityEditorUtilities
{

    public static class UnityInternalGUIClip
    {
        private static Type m_type;

        public static Type Type
        {
            get
            {
                return Type.GetType("UnityEngine.GUIClip,UnityEngine");
            }
        }

        static UnityInternalGUIClip()
        {
            m_type = Type;
        }

        public static bool enabled
        {
            get
            {
                PropertyInfo property = m_type.GetProperty("enabled", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                return (bool)property.GetValue(null, null);
            }
        }

        public static Rect visibleRect
        {
            get
            {
                PropertyInfo property = m_type.GetProperty("visibleRect", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                return (Rect)property.GetValue(null, null);
            }
        }
    }

}
#endif