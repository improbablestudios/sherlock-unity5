#if UNITY_EDITOR
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.SerializedPropertyUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace ImprobableStudios.UnityEditorUtilities
{

    public class ExtendedEditorGUI
    {
        protected const float kHorizontalFieldSpacing = 5f;
        public static float HorizontalFieldSpacing
        {
            get
            {
                return kHorizontalFieldSpacing;
            }
        }

        public delegate void PropertyDrawerDelegate(Rect position, SerializedProperty property, GUIContent label);
        public delegate float PropertyHeightDelegate(SerializedProperty property, GUIContent label);

        public delegate bool ChildPropertyDrawerDelegate(Rect position, SerializedProperty property, GUIContent label, bool includeChildren);
        public delegate float ChildPropertyHeightDelegate(SerializedProperty property, GUIContent label, bool includeChildren);

        public delegate object GenericDrawerDelegate(Rect position, object value, Type type, GUIContent label);
        public delegate float GenericHeightDelegate(object value, Type type, GUIContent label);

        public delegate bool SerializedPropertyBooleanCondition(SerializedProperty property);
        public delegate int SerializedPropertyIntegerCondition(SerializedProperty property);

        public delegate bool MemberDrawerDelegate(Rect position, SerializedMember serializedMember, GUIContent label);
        public delegate float MemberHeightDelegate(SerializedMember serializedMember, GUIContent label);

        public delegate bool MemberBooleanCondition(SerializedMember serializedMember);
        public delegate int MemberIntegerCondition(SerializedMember serializedMember);
        
        public delegate UnityEngine.Object ObjectFieldDelegate(Rect position, UnityEngine.Object obj, Type objType, bool allowSceneObjects);

        private static FieldInfo s_lastControlIDField = typeof(EditorGUIUtility).GetField("s_LastControlID", BindingFlags.Static | BindingFlags.NonPublic);
        private static FieldInfo s_mixedValueContentField = typeof(EditorGUI).GetField("s_MixedValueContent", BindingFlags.Static | BindingFlags.NonPublic);
        private static FieldInfo s_mixedValueContentColorTempField = typeof(EditorGUI).GetField("s_MixedValueContentColorTemp", BindingFlags.Static | BindingFlags.NonPublic);
        
        public static void DisplayObjectContextMenu(Rect position, UnityEngine.Object[] context, int contextUserData)
        {
            typeof(EditorUtility).GetMethod("DisplayObjectContextMenu", BindingFlags.NonPublic | BindingFlags.Static, null, new Type[] { typeof(Rect), typeof(UnityEngine.Object[]), typeof(int) }, null).Invoke(null, new object[] { position, context, contextUserData });
        }

        public static void DrawEditorHeaderItems(Rect position, UnityEngine.Object[] context)
        {
            typeof(EditorGUIUtility).GetMethod("DrawEditorHeaderItems", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { position, context });
        }

        public static void SetIconForObject(UnityEngine.Object obj, Texture2D icon)
        {
            typeof(UnityEditor.EditorGUIUtility).GetMethod("SetIconForObject", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { obj, icon });
        }

        public static Texture2D GetIconForObject(UnityEngine.Object obj)
        {
            return typeof(UnityEditor.EditorGUIUtility).GetMethod("GetIconForObject", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { obj }) as Texture2D;
        }

        internal static bool GetBoldDefaultFont()
        {
            return (bool)typeof(EditorGUIUtility).GetField("s_FontIsBold", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null);
        }

        public static GUIContent GetMixedValueContent()
        {
            return (GUIContent)s_mixedValueContentField.GetValue(null);
        }

        public static Color GetMixedValueColor()
        {
            return (Color)s_mixedValueContentColorTempField.GetValue(null);
        }

        public static void SetMixedValueColor(Color color)
        {
            s_mixedValueContentColorTempField.SetValue(null, color);
        }

        public static T[] GetComponentsInGameObjectSelection<T>() where T : UnityEngine.Component
        {
            List<T> nodesInGameObjectSelection = new List<T>();
            foreach (UnityEngine.Object selectedObj in Selection.objects)
            {
                GameObject selectedGameObject = selectedObj as GameObject;
                if (selectedGameObject != null)
                {
                    foreach (T component in selectedGameObject.GetComponents<T>())
                    {
                        nodesInGameObjectSelection.Add(component);
                    }
                }
            }
            return nodesInGameObjectSelection.ToArray();
        }

        public static int GetLastControlId()
        {
            return (int)s_lastControlIDField.GetValue(null);
        }

        public static GUIContent[] GetGUIContents(string[] strings, Texture image, string tooltip)
        {
            return strings.Select(s => new GUIContent(s, image, tooltip)).ToArray();
        }

        public static GUIContent[] GetGUIContents(string[] strings, string tooltip)
        {
            return GetGUIContents(strings, null, tooltip);
        }

        public static GUIContent[] GetGUIContents(string[] strings, Texture image)
        {
            return GetGUIContents(strings, image, null);
        }

        public static GUIContent[] GetGUIContents(string[] strings)
        {
            return GetGUIContents(strings, null, null);
        }

        public static Rect GetContentRect(Rect totalPosition, GUIContent label)
        {
            if (string.IsNullOrEmpty(label.text))
            {
                totalPosition = EditorGUI.IndentedRect(totalPosition);
            }
            else
            {
                totalPosition = new Rect(totalPosition.x + EditorGUIUtility.labelWidth, totalPosition.y, totalPosition.width - EditorGUIUtility.labelWidth, totalPosition.height);
            }
            return totalPosition;
        }

        public static float GetContentWidth(float totalWidth, GUIContent label)
        {
            return GetContentRect(new Rect(Vector2.zero, new Vector2(totalWidth, 0)), label).width;
        }

        public static float GetNextControlWidth()
        {
            GUIStyle guiStyle = new GUIStyle();
            guiStyle.padding = new RectOffset(0, 0, 0, 0);
            guiStyle.margin = new RectOffset(0, 0, 0, 0);
            return EditorGUILayout.GetControlRect(false, 0f, guiStyle).width;
        }

        public static Rect[] GetRects(Rect position, float[] widths, float spacing = kHorizontalFieldSpacing)
        {
            Rect[] rects = new Rect[widths.Length];
            float fillerWidth = spacing / rects.Length;
            float xPos = position.x;
            for (int i = 0; i < rects.Length; ++i)
            {
                rects[i] = position;
                rects[i].width = widths[i] - spacing + fillerWidth;
                rects[i].x = xPos;
                xPos += widths[i] + fillerWidth;
            }
            return rects;
        }

        public static Rect[] GetRelativeWidthRects(Rect position, float[] relativeWidths, float spacing = kHorizontalFieldSpacing, float minWidth = 0)
        {
            float[] widths = new float[relativeWidths.Length];
            for (int i = 0; i < widths.Length; i++)
            {
                widths[i] = position.width * relativeWidths[i];
                widths[i] = Math.Max(minWidth, widths[i]);
            }
            return GetRects(position, widths, spacing);
        }

        public static Rect[] GetEqualWidthRects(Rect position, int numberOfRects, float spacing = kHorizontalFieldSpacing, float minWidth = 50)
        {
            float[] widths = new float[numberOfRects];
            for (int i = 0; i < widths.Length; i++)
            {
                widths[i] = position.width * (1f / numberOfRects);
                widths[i] = Math.Max(minWidth, widths[i]);
            }
            return GetRects(position, widths, spacing);
        }

        public static void FlexibleTextField(SerializedProperty property, int minLines, int maxLines)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            FlexibleTextField(GetFlexibleTextFieldRect(property.stringValue, label, minLines, maxLines), property, label);
        }

        public static void FlexibleTextField(SerializedProperty property, GUIContent label, int minLines, int maxLines)
        {
            FlexibleTextField(GetFlexibleTextFieldRect(property.stringValue, label, minLines, maxLines), property, label);
        }

        public static void FlexibleTextField(Rect position, SerializedProperty property)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            FlexibleTextField(position, property, label);
        }

        public static void FlexibleTextField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string stringValue = FlexibleTextField(position, property.stringValue, label);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = stringValue;
            }

            EditorGUI.EndProperty();
        }

        public static string FlexibleTextField(string text, GUIContent label, int minLines, int maxLines)
        {
            return FlexibleTextField(GetFlexibleTextFieldRect(text, label, minLines, maxLines), text, label);
        }

        public static string FlexibleTextField(Rect position, string text, GUIContent label)
        {
            GUIStyle style = new GUIStyle(EditorStyles.textArea);
            style.wordWrap = true;
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            return EditorGUI.TextArea(contentPosition, text, style);
        }

        public static Rect GetFlexibleTextFieldRect(string text, GUIContent label, int minLines, int maxLines)
        {
            Rect nextControlRect = EditorGUILayout.GetControlRect(GUILayout.Height(0f));
            float contentWidth = nextControlRect.width;
            if (!string.IsNullOrEmpty(label.text))
            {
                contentWidth -= EditorGUIUtility.labelWidth;
            }
            float contentHeight = GetFlexibleTextAreaHeight(text, contentWidth, minLines, maxLines);
            return EditorGUILayout.GetControlRect(GUILayout.Height(contentHeight));
        }

        public static void FlexibleTextArea(SerializedProperty property, int minLines, int maxLines)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            FlexibleTextArea(GetFlexibleTextAreaRect(property.stringValue, label, minLines, maxLines), property, label);
        }

        public static void FlexibleTextArea(SerializedProperty property, GUIContent label, int minLines, int maxLines)
        {
            FlexibleTextArea(GetFlexibleTextAreaRect(property.stringValue, label, minLines, maxLines), property, label);
        }

        public static void FlexibleTextArea(Rect position, SerializedProperty property)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            FlexibleTextArea(position, property, label);
        }

        public static void FlexibleTextArea(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string stringValue = FlexibleTextArea(position, property.stringValue, label);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = stringValue;
            }

            EditorGUI.EndProperty();
        }

        public static string FlexibleTextArea(string text, GUIContent label, int minLines, int maxLines)
        {
            return FlexibleTextArea(GetFlexibleTextAreaRect(text, label, minLines, maxLines), text, label);
        }

        public static string FlexibleTextArea(Rect position, string text, GUIContent label)
        {
            GUIStyle style = new GUIStyle(EditorStyles.textArea);
            style.wordWrap = true;
            Rect labelPosition = EditorGUI.IndentedRect(position);
            labelPosition.height = 16f;
            position.yMin += labelPosition.height;
            EditorGUI.HandlePrefixLabel(position, labelPosition, label);
            return EditorGUI.TextArea(position, text, style);
        }

        public static Rect GetFlexibleTextAreaRect(string text, GUIContent label, int minLines, int maxLines)
        {
            Rect nextControlRect = EditorGUILayout.GetControlRect(GUILayout.Height(0f));
            float contentWidth = nextControlRect.width;
            float contentHeight = GetFlexibleTextAreaHeight(text, contentWidth, minLines, maxLines);
            if (!string.IsNullOrEmpty(label.text))
            {
                contentHeight += EditorGUIUtility.singleLineHeight + 2f;
            }
            return EditorGUILayout.GetControlRect(GUILayout.Height(contentHeight));
        }

        public static float GetFlexibleTextAreaHeight(string text, float contentWidth, int minLines, int maxLines)
        {
            GUIStyle style = new GUIStyle(EditorStyles.textArea);
            style.wordWrap = true;
            float lineHeight = style.lineHeight;
            float textAreaPadding = style.padding.top + style.padding.bottom;
            float idealTextAreaHeight = style.CalcHeight(new GUIContent(text), contentWidth);
            int idealLines = Mathf.CeilToInt((idealTextAreaHeight - textAreaPadding) / lineHeight);
            int numLinesToDisplay = Mathf.Clamp(idealLines, minLines, maxLines);

            return textAreaPadding + (float)(numLinesToDisplay * lineHeight);
        }

        public static void ScrollableTextArea(SerializedProperty property, int minLines, int maxLines, ref Vector2 scrollPosition)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            Rect position = GetScrollableTextAreaRect(property.stringValue, label, minLines, maxLines);
            EditorGUI.BeginProperty(position, label, property);
            ScrollableTextArea(position, property, label, ref scrollPosition);
            EditorGUI.EndProperty();
        }

        public static void ScrollableTextArea(SerializedProperty property, GUIContent label, int minLines, int maxLines, ref Vector2 scrollPosition)
        {
            ScrollableTextArea(GetScrollableTextAreaRect(property.stringValue, label, minLines, maxLines), property, label, ref scrollPosition);
        }

        public static void ScrollableTextArea(Rect position, SerializedProperty property, ref Vector2 scrollPosition)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            EditorGUI.BeginProperty(position, label, property);
            ScrollableTextArea(position, property, label, ref scrollPosition);
            EditorGUI.EndProperty();
        }

        public static void ScrollableTextArea(Rect position, SerializedProperty property, GUIContent label, ref Vector2 scrollPosition)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string text = property.stringValue;

            text = ScrollableTextArea(position, text, label, ref scrollPosition);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = text;
            }

            EditorGUI.EndProperty();
        }

        public static string ScrollableTextArea(string text, GUIContent label, int minLines, int maxLines, ref Vector2 scrollPosition)
        {
            return ScrollableTextArea(GetScrollableTextAreaRect(text, label, minLines, maxLines), text, label, ref scrollPosition);
        }

        public static string ScrollableTextArea(Rect position, string text, GUIContent label, ref Vector2 scrollPosition)
        {
            GUIStyle style = new GUIStyle(EditorStyles.textArea);
            style.wordWrap = true;
            if (!string.IsNullOrEmpty(label.text))
            {
                Rect labelPosition = position;
                labelPosition.height = EditorGUIUtility.singleLineHeight;
                position.yMin += labelPosition.height;
                EditorGUI.HandlePrefixLabel(position, labelPosition, label);
            }

            object[] arguments = new object[] { position, text, scrollPosition, style };
            MethodInfo method = typeof(EditorGUI).GetMethod("ScrollableTextAreaInternal", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            text = (string)method.Invoke(null, arguments);
            scrollPosition = (Vector2)arguments[2];

            return text;
        }

        public static Rect GetScrollableTextAreaRect(string text, GUIContent label, int minLines, int maxLines)
        {
            float nextControlRectWidth = EditorGUILayout.GetControlRect(GUILayout.Height(0f)).width;
            return EditorGUILayout.GetControlRect(GUILayout.Height(GetScrollableTextAreaHeight(text, label, nextControlRectWidth, minLines, maxLines)));
        }

        public static float GetScrollableTextAreaHeight(string text, GUIContent label, float width, int minLines, int maxLines)
        {
            float labelHeight = 0f;
            if (!string.IsNullOrEmpty(label.text))
            {
                labelHeight = EditorGUIUtility.singleLineHeight;
            }
            return labelHeight + GetFlexibleTextAreaHeight(text, width, minLines, maxLines);
        }

        public static void SortingLayerField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            object[] arguments = new object[] { position, label, property, EditorStyles.popup, EditorStyles.label };
            MethodInfo method = typeof(EditorGUI).GetMethod("SortingLayerField", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            method.Invoke(null, arguments);
            EditorGUI.EndProperty();
        }

        public static void AssetGuidField(Rect position, SerializedProperty property, GUIContent label)
        {
            UnityEngine.Object cachedAssetObject = null;
            AssetGuidField(position, property, label, typeof(UnityEngine.Object), ref cachedAssetObject);
        }

        public static void AssetGuidField(Rect position, SerializedProperty property, GUIContent label, Type objType)
        {
            UnityEngine.Object cachedAssetObject = null;
            AssetGuidField(position, property, label, objType, ref cachedAssetObject);
        }

        public static void AssetGuidField(Rect position, SerializedProperty property, GUIContent label, Type objType, ref UnityEngine.Object cachedAssetObject)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            string guid = property.stringValue;

            guid = AssetGuidField(position, guid, label, objType, ref cachedAssetObject);

            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = guid;
            }

            EditorGUI.EndProperty();
        }

        public static string AssetGuidField(Rect position, string guid, GUIContent label, Type objType)
        {
            UnityEngine.Object cachedAssetObject = null;
            return AssetGuidField(position, guid, label, objType, ref cachedAssetObject);
        }

        public static string AssetGuidField(Rect position, string guid, GUIContent label, Type objType, ref UnityEngine.Object cachedAssetObject)
        {
            EditorGUI.BeginChangeCheck();

            if (cachedAssetObject == null)
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                    if (assetPath != null)
                    {
                        cachedAssetObject = AssetDatabase.LoadAssetAtPath(assetPath, objType);
                    }
                }
            }

            cachedAssetObject = EditorGUI.ObjectField(position, label, cachedAssetObject, objType, false);

            if (EditorGUI.EndChangeCheck())
            {
                if (cachedAssetObject != null)
                {
                    string assetPath = AssetDatabase.GetAssetPath(cachedAssetObject);
                    guid = AssetDatabase.AssetPathToGUID(assetPath);
                }
                else
                {
                    guid = "";
                }
            }

            return guid;
        }

        public static Type UnityObjectTypeField(Rect position, Type type, GUIContent label, Type baseComponentType)
        {
            Type[] componentTypes = baseComponentType.FindAllInstantiableDerivedTypes();
            return UnityObjectTypeField(position, type, label, componentTypes);
        }

        public static Type UnityObjectTypeField(Rect position, Type type, GUIContent label, Type[] componentTypes)
        {
            EditorGUI.BeginChangeCheck();
            Type[] orderedTypes = componentTypes.OrderBy(i => i.Name).ToArray();
            GUIContent[] orderedLabels = orderedTypes.Select(i => new GUIContent(i.Name)).ToArray();
            int selectedIndex = Array.IndexOf(orderedTypes, type);
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, orderedLabels);
            if (EditorGUI.EndChangeCheck())
            {
                type = orderedTypes[selectedIndex];
            }
            return type;
        }
        
        public static void CharField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            char charValue = (char)property.intValue;
            charValue = CharField(position, label, charValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = charValue;
            }
            EditorGUI.EndProperty();
        }

        public static char CharField(Rect position, GUIContent label, char value)
        {
            char[] charArray = new char[] { value };
            EditorGUI.BeginChangeCheck();
            string text = EditorGUI.TextField(position, label, new string(charArray));
            if (EditorGUI.EndChangeCheck())
            {
                if (text.Length > 0)
                {
                    value = text[0];
                }
                else
                {
                    value = default(char);
                }
            }
            return value;
        }

        public static void QuaternionField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            Quaternion value = property.quaternionValue;
            value = QuaternionField(position, label, value);
            if (EditorGUI.EndChangeCheck())
            {
                property.quaternionValue = value;
            }
            EditorGUI.EndProperty();
        }

        public static Quaternion QuaternionField(Rect position, GUIContent label, Quaternion value)
        {
            Vector4 vector4 = new Vector4(value.x, value.y, value.z, value.w);
            EditorGUI.BeginChangeCheck();
            vector4 = EditorGUI.Vector4Field(position, label, vector4);
            if (EditorGUI.EndChangeCheck())
            {
                value = new Quaternion(vector4.x, vector4.y, vector4.z, vector4.w);
            }
            return value;
        }

        public static void GradientField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            Gradient value = property.GetGradientValue();
            value = GradientField(position, label, value);
            if (EditorGUI.EndChangeCheck())
            {
                property.SetGradientValue(value);
            }
            EditorGUI.EndProperty();
        }

        public static Gradient GradientField(Rect position, GUIContent label, Gradient value)
        {
            object[] arguments = new object[] { label, position, value };
            MethodInfo method = typeof(EditorGUI).GetMethod("GradientField", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static, null, new Type[] { typeof(GUIContent), typeof(Rect), typeof(Gradient) }, null);
            return (Gradient)method.Invoke(null, arguments);
        }

        public static void AnimationCurveField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            AnimationCurve curveValue = property.animationCurveValue;
            curveValue = EditorGUI.CurveField(position, label, curveValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.animationCurveValue = curveValue;
            }
            EditorGUI.EndProperty();
        }

        public static void BoundsField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            Bounds boundsValue = property.boundsValue;
            boundsValue = EditorGUI.BoundsField(position, label, boundsValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.boundsValue = boundsValue;
            }
            EditorGUI.EndProperty();
        }

        public static void IntEnumField(Rect position, SerializedProperty property, Type type, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            int intValue = property.intValue;
            Enum selectedEnum = Enum.ToObject(type, intValue) as Enum;
            intValue = (int)Convert.ChangeType(EditorGUI.EnumPopup(position, label, selectedEnum), typeof(int));
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = intValue;
            }
            EditorGUI.EndProperty();
        }

        public static void LayerMaskField(Rect position, SerializedProperty property, Type type, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            int intValue = property.intValue;
            intValue = EditorGUILayout.MaskField(intValue, InternalEditorUtility.layers);
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = intValue;
            }
            EditorGUI.EndProperty();
        }

        public static LayerMask LayerMaskField(Rect position, GUIContent label, LayerMask value)
        {
            EditorGUI.BeginChangeCheck();
            int intValue = InternalEditorUtility.LayerMaskToConcatenatedLayersMask(value);
            intValue = EditorGUI.MaskField(position, intValue, InternalEditorUtility.layers);
            if (EditorGUI.EndChangeCheck())
            {
                value = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(intValue);
            }
            return value;
        }

        public static IList ArraySizeField(Rect position, GUIContent label, IList value, Type type)
        {
            Type itemType = type.GetItemType();
            if (value == null)
            {
                if (type.IsArray)
                {
                    value = Activator.CreateInstance(type, new object[] { 0 }) as IList;
                }
                else
                {
                    value = Activator.CreateInstance(typeof(List<>).MakeGenericType(itemType)) as IList;
                }
            }

            type = value.GetType();
            itemType = type.GetItemType();

            int oldSize = (value != null) ? value.Count : -1;
            int arraySize = (value != null) ? value.Count : -1;
            EditorGUI.BeginChangeCheck();
            arraySize = EditorGUI.DelayedIntField(position, label, arraySize);
            if (EditorGUI.EndChangeCheck())
            {
                int deleteCount = Mathf.Max(0, oldSize - arraySize);
                int createCount = Mathf.Max(0, arraySize - oldSize);
                
                if (type.IsArray)
                {
                    IList newArray = Activator.CreateInstance(type, new object[] { value.Count }) as IList;
                    for (int i = 0; i < arraySize; i++)
                    {
                        if (i < value.Count)
                        {
                            newArray[i] = value[i];
                        }
                        else
                        {
                            object newElement = null;
                            if (itemType != null)
                            {
                                if (itemType == typeof(string))
                                {
                                    newElement = "";
                                }
                                else if (itemType.IsValueType)
                                {
                                    newElement = Activator.CreateInstance(itemType);
                                }
                            }

                            newArray[i] = newElement;
                        }
                    }
                    return newArray;
                }
                else
                {
                    while (value.Count > arraySize)
                    {
                        value.RemoveAt(value.Count - 1);
                    }
                    while (value.Count < arraySize)
                    {
                        object newElement = null;
                        if (itemType != null)
                        {
                            if (itemType == typeof(string))
                            {
                                newElement = "";
                            }
                            else if (itemType.IsValueType)
                            {
                                newElement = Activator.CreateInstance(itemType);
                            }
                        }

                        value.Add(newElement);
                    }
                }
            }

            return value;
        }
        public static void ToggleButton(SerializedProperty property, params GUILayoutOption[] options)
        {
            ToggleButton(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip));
        }

        public static void ToggleButton(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            ToggleButton(EditorGUILayout.GetControlRect(options), property, label);
        }

        public static void ToggleButton(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            bool value = property.boolValue;

            value = ToggleButton(position, label, value);

            if (EditorGUI.EndChangeCheck())
            {
                property.boolValue = value;
            }

            EditorGUI.EndProperty();
        }

        public static bool ToggleButton(string label, bool value, params GUILayoutOption[] options)
        {
            return ToggleButton(EditorGUILayout.GetControlRect(options), new GUIContent(label), value);
        }

        public static bool ToggleButton(GUIContent label, bool value, params GUILayoutOption[] options)
        {
            return ToggleButton(EditorGUILayout.GetControlRect(options), label, value);
        }

        public static bool ToggleButton(Rect position, GUIContent label, bool value)
        {
            GUIStyle toggleStyle = new GUIStyle(GUI.skin.button);
            if (value)
            {
                toggleStyle.normal = GUI.skin.button.active;
            }
            if (GUI.Button(position, label, toggleStyle))
            {
                value = !value;
            }
            return value;
        }

        public static void TogglePopupField(SerializedProperty property, GUIContent label, string falseLabel, string trueLabel, params GUILayoutOption[] options)
        {
            TogglePopupField(EditorGUILayout.GetControlRect(options), property, label, falseLabel, trueLabel);
        }

        public static void TogglePopupField(SerializedProperty property, string falseLabel, string trueLabel, params GUILayoutOption[] options)
        {
            TogglePopupField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), falseLabel, trueLabel);
        }

        public static void TogglePopupField(Rect position, SerializedProperty property, GUIContent label, string falseLabel, string trueLabel)
        {
            EditorGUI.BeginProperty(position, label, property);
            bool value = property.boolValue;
            int boolInt = (value) ? 1 : 0;
            boolInt = EditorGUI.Popup(position, label, boolInt, new GUIContent[] { new GUIContent(falseLabel), new GUIContent(trueLabel) });
            property.boolValue = (boolInt == 1) ? true : false;
            EditorGUI.EndProperty();
        }

        public static bool TogglePopupField(bool value, GUIContent label, string falseLabel, string trueLabel, params GUILayoutOption[] options)
        {
            return TogglePopupField(EditorGUILayout.GetControlRect(options), value, label, trueLabel, falseLabel);
        }

        public static bool TogglePopupField(Rect position, bool value, GUIContent label, string falseLabel, string trueLabel)
        {
            int boolInt = (value) ? 1 : 0;
            boolInt = EditorGUI.Popup(position, label, boolInt, new GUIContent[] { new GUIContent(falseLabel), new GUIContent(trueLabel) });
            return (boolInt == 1) ? true : false;
        }

        public static void MemberField(Rect position, SerializedProperty property, GUIContent label, object obj, Type objType, Type memberType)
        {
            List<string> memberNames = obj.GetMemberPaths(objType, memberType).ToList();

            if (typeof(float).IsAssignableFrom(memberType))
            {
                memberNames.AddRange(obj.GetMemberPaths(objType, typeof(int)));
            }
            else if (typeof(int).IsAssignableFrom(memberType))
            {
                memberNames.AddRange(obj.GetMemberPaths(objType, typeof(float)));
            }

            if (memberNames.Count > 0)
            {
                GUIContent[] memberLabels = memberNames.Select(i => new GUIContent(i.Replace(".", "/"))).ToArray();
                EditorGUI.BeginProperty(position, label, property);
                EditorGUI.BeginChangeCheck();
                int selectedIndex = memberNames.IndexOf(property.stringValue);
                selectedIndex = EditorGUI.Popup(position, label, selectedIndex, memberLabels);
                if (EditorGUI.EndChangeCheck())
                {
                    property.stringValue = memberNames[selectedIndex];
                }
                EditorGUI.EndProperty();
            }
            else
            {
                Rect contentPosition = EditorGUI.PrefixLabel(position, new GUIContent(label));
                EditorGUI.HelpBox(contentPosition, string.Format("No property of type '{0}' found in {1}", memberType, obj), MessageType.Warning);
            }
        }

        public static void ObjectCycleToolbar(SerializedProperty property, UnityEngine.Object[] list, GUIContent nullLabel = null, params GUILayoutOption[] options)
        {
            ObjectCycleToolbar(EditorGUILayout.GetControlRect(options), property, list);
        }

        public static void ObjectCycleToolbar(Rect position, SerializedProperty property, UnityEngine.Object[] list, GUIContent nullLabel = null)
        {
            if (nullLabel == null)
            {
                nullLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent label = null;
            if (property.objectReferenceValue == null)
            {
                label = nullLabel;
            }
            else
            {
                label = new GUIContent(property.objectReferenceValue.name);
            }
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            UnityEngine.Object obj = property.objectReferenceValue;
            obj = ExtendedEditorGUI.ObjectCycleToolbar(position, obj, list);
            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = obj;
            }
            EditorGUI.EndProperty();
        }

        public static UnityEngine.Object ObjectCycleToolbar(UnityEngine.Object selectedObject, UnityEngine.Object[] list, params GUILayoutOption[] options)
        {
            return ObjectCycleToolbar(EditorGUILayout.GetControlRect(options), selectedObject, list);
        }

        public static UnityEngine.Object ObjectCycleToolbar(Rect position, UnityEngine.Object selectedObject, UnityEngine.Object[] list, GUIContent nullLabel = null)
        {
            if (nullLabel == null)
            {
                nullLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent label = null;
            if (selectedObject == null)
            {
                label = nullLabel;
            }
            else
            {
                label = new GUIContent(selectedObject.name);
            }
            int toolbarInt = 1;
            GUIContent[] toolbarStrings = { new GUIContent("<--"), label, new GUIContent("-->") };
            toolbarInt = GUI.Toolbar(position, toolbarInt, toolbarStrings);
            int index = Array.IndexOf(list, selectedObject);
            if (toolbarInt == 0)
            {
                if (index > 0)
                {
                    return list[--index];
                }
            }
            if (toolbarInt == 2)
            {
                if (index < list.Length - 1)
                {
                    return list[++index];
                }
            }
            return selectedObject;
        }

        public static void SelectableBox(string text, params GUILayoutOption[] options)
        {
            GUIStyle selectableBoxStyle = new GUIStyle(EditorStyles.textField);
            selectableBoxStyle.fixedHeight = selectableBoxStyle.lineHeight + selectableBoxStyle.padding.top + selectableBoxStyle.padding.bottom;
            List<GUILayoutOption> paramList = options.ToList();
            paramList.Add(GUILayout.MaxHeight(selectableBoxStyle.fixedHeight));
            EditorGUILayout.SelectableLabel(text, selectableBoxStyle, paramList.ToArray());
        }

        public static void LineSeparator()
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("", EditorStyles.numberField, GUILayout.Height(3f));
            EditorGUILayout.Separator();
        }

        public static void LineSeparator(Rect position)
        {
            float height = 3f;
            float heightDifference = EditorGUIUtility.singleLineHeight - height;
            position.height = height;
            position.y += heightDifference / 2;
            GUI.Label(position, "", EditorStyles.numberField);
        }

        public static void DrawCroppedTexture(Rect position, Texture2D texture, float normalizedFrameWidth, float normalizedFrameHeight, float normalizedFrameXOffset, float normalizedFrameYOffset, float scale)
        {
            Rect textureCrop = new Rect(normalizedFrameXOffset, normalizedFrameYOffset, normalizedFrameWidth, normalizedFrameHeight);
            float frameHeight = normalizedFrameHeight * texture.height;
            float frameWidth = normalizedFrameWidth * texture.width;
            position.y += (position.height / 2) - (frameHeight / 2);
            position.x += (position.width / 2) - (frameWidth / 2);
            using (new GUI.GroupScope(new Rect(position.x, position.y, texture.width * textureCrop.width, texture.height * textureCrop.height)))
            {
                GUI.DrawTexture(new Rect(-texture.width * textureCrop.x, -texture.height * textureCrop.y, texture.width * scale, texture.height * scale), texture);
            }
        }

        public static void MonoBehaviourEnableToggle(MonoBehaviour monoBehaviour, GUIContent label, params GUILayoutOption[] options)
        {
            MonoBehaviourEnableToggle(EditorGUILayout.GetControlRect(options), monoBehaviour, label);
        }

        public static void MonoBehaviourEnableToggle(MonoBehaviour monoBehaviour, params GUILayoutOption[] options)
        {
            MonoBehaviourEnableToggle(EditorGUILayout.GetControlRect(options), monoBehaviour, GUIContent.none);
        }

        public static void MonoBehaviourEnableToggle(Rect position, MonoBehaviour monoBehaviour)
        {
            MonoBehaviourEnableToggle(position, monoBehaviour, GUIContent.none);
        }

        public static void MonoBehaviourEnableToggle(Rect position, MonoBehaviour monoBehaviour, GUIContent label)
        {
            bool enabled = monoBehaviour.enabled;
            EditorGUI.BeginChangeCheck();
            enabled = EditorGUI.Toggle(position, label, enabled);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(monoBehaviour, "Set Enabled");
                monoBehaviour.enabled = enabled;
            }
        }

        public static string[] GetDisplayNames(string[] names, string firstLabel = null)
        {
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = ObjectNames.NicifyVariableName(names[i]);
            }
            if (firstLabel != null)
            {
                names[0] = firstLabel;
            }
            return names;
        }

        public static void DrawVisibleChildren(Rect position, SerializedProperty property)
        {
            DrawVisibleChildren(position, property, null, null, null, null, null, -1);
        }
        
        public static void DrawVisibleChildren(Rect position, SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset)
        {
            DrawVisibleChildren(position, property, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, null, null, -1);
        }

        public static void DrawVisibleChildren(Rect position, SerializedProperty property, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            DrawVisibleChildren(position, property, null, null, null, propertyDrawerDelegate, propertyHeightDelegate, -1);
        }

        public static void DrawVisibleChildren(Rect position, SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            DrawVisibleChildren(position, property, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, -1);
        }

        public static void DrawVisibleChildren(Rect position, SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            if (property.hasVisibleChildren)
            {
                int prevIndentLevel = EditorGUI.indentLevel;
                int indentLevel = prevIndentLevel - property.depth - 1;
                EditorGUI.indentLevel = property.depth + indentLevel;

                SerializedProperty iterator = property.Copy();
                DrawVisibleProperties(position, iterator, iterator.GetEndProperty(), indentLevel, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, childLimit);
               
                EditorGUI.indentLevel = prevIndentLevel;
            }
        }

        public static void DrawVisibleProperties(Rect position, SerializedObject serializedObject)
        {
            DrawVisibleProperties(position, serializedObject, null, null, null, null, null, -1);
        }

        public static void DrawVisibleProperties(Rect position, SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset)
        {
            DrawVisibleProperties(position, serializedObject, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, null, null, -1);
        }

        public static void DrawVisibleProperties(Rect position, SerializedObject serializedObject, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            DrawVisibleProperties(position, serializedObject, null, null, null, propertyDrawerDelegate, propertyHeightDelegate, -1);
        }

        public static void DrawVisibleProperties(Rect position, SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            DrawVisibleProperties(position, serializedObject, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, -1);
        }

        public static void DrawVisibleProperties(Rect position, SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            DrawVisibleProperties(position, serializedObject.GetIterator(), null, 0, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, childLimit);
        }

        private static void DrawVisibleProperties(Rect position, SerializedProperty iterator, SerializedProperty endProperty, int indentLevel, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            bool enterChildren = true;
            float margin = 2f;
            position.height = 0f;
            
            int childIndex = 0;
            while ((childLimit < 0 || childIndex < childLimit) && iterator.NextVisible(enterChildren) && (endProperty == null || !SerializedProperty.EqualContents(iterator, endProperty)))
            {
                SerializedProperty childProperty = iterator.Copy();

                if (isPropertyVisible != null && !isPropertyVisible(childProperty))
                {
                    continue;
                }

                EditorGUI.indentLevel = childProperty.depth + indentLevel;

                int childIndentLevel = EditorGUI.indentLevel;
                if (getPropertyIndentLevelOffset != null)
                {
                    EditorGUI.indentLevel += getPropertyIndentLevelOffset(childProperty);
                }

                bool childEnabled = GUI.enabled;
                if (isPropertyEditable != null && !isPropertyEditable(childProperty))
                {
                    GUI.enabled = false;
                }

                GUIContent childLabel = new GUIContent(childProperty.displayName, childProperty.tooltip);
                if (propertyDrawerDelegate != null && propertyHeightDelegate != null)
                {
                    position.height = propertyHeightDelegate(childProperty, childLabel, false);
                    EditorGUI.BeginChangeCheck();
                    enterChildren = propertyDrawerDelegate(position, childProperty, childLabel, false) && childProperty.hasVisibleChildren;
                    if (EditorGUI.EndChangeCheck())
                    {
                        break;
                    }
                }
                else
                {
                    position.height = EditorGUI.GetPropertyHeight(childProperty, childLabel, false);
                    EditorGUI.BeginChangeCheck();
                    enterChildren = EditorGUI.PropertyField(position, childProperty, childLabel, false) && childProperty.hasVisibleChildren;
                    if (EditorGUI.EndChangeCheck())
                    {
                        break;
                    }
                }

                position.y += position.height + margin;

                EditorGUI.indentLevel = childIndentLevel;

                GUI.enabled = childEnabled;

                childIndex++;
            }
        }
        public static string GetSerializedMemberDisplayName(SerializedMember serializedMember)
        {
            string displayName = serializedMember.DisplayName;
            if (displayName == null)
            {
                displayName = ObjectNames.NicifyVariableName(serializedMember.MemberName);
            }
            return displayName;
        }

        public static string GetSerializedMemberDescription(SerializedMember serializedMember)
        {
            string description = serializedMember.Description;
            if (description == null)
            {
                if (serializedMember.Member != null)
                {
                    TooltipAttribute tooltipAttribute = serializedMember.Member.GetCustomAttribute<TooltipAttribute>();
                    if (tooltipAttribute != null)
                    {
                        description = tooltipAttribute.tooltip;
                    }
                }
            }
            return description;
        }

        public static void DrawSerializedMembers(Rect position, SerializedMember serializedMember, GUIContent label)
        {
            DrawSerializedMembers(position, serializedMember, label, null, null, null, null, null, -1);
        }

        public static void DrawSerializedMembers(Rect position, SerializedMember serializedMember, GUIContent label, MemberBooleanCondition isMemberVisible, MemberBooleanCondition isMemberEditable, MemberIntegerCondition getMemberIndentLevelOffset, MemberDrawerDelegate memberDrawerDelegate, MemberHeightDelegate memberHeightDelegate, int childLimit)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            SerializedMemberField(position, serializedMember, label);
            if (serializedMember.IsExpanded && serializedMember.HasChildren)
            {
                float margin = 2f;
                position.y += position.height + margin;
                DrawSerializedMemberChildren(position, serializedMember, 1, isMemberVisible, isMemberEditable, getMemberIndentLevelOffset, memberDrawerDelegate, memberHeightDelegate, -1);
            }
        }

        public static void DrawSerializedMemberChildren(Rect position, SerializedMember serializedMember)
        {
            DrawSerializedMemberChildren(position, serializedMember, 0, null, null, null, null, null, -1);
        }

        public static void DrawSerializedMemberChildren(Rect position, SerializedMember serializedMember, MemberBooleanCondition isMemberVisible, MemberBooleanCondition isMemberEditable, MemberIntegerCondition getMemberIndentLevelOffset, MemberDrawerDelegate memberDrawerDelegate, MemberHeightDelegate memberHeightDelegate, int childLimit)
        {
            DrawSerializedMemberChildren(position, serializedMember, 0, isMemberVisible, isMemberEditable, getMemberIndentLevelOffset, memberDrawerDelegate, memberHeightDelegate, childLimit);
        }

        private static void DrawSerializedMemberChildren(Rect position, SerializedMember serializedMember, int indentLevel, MemberBooleanCondition isMemberVisible, MemberBooleanCondition isMemberEditable, MemberIntegerCondition getMemberIndentLevelOffset, MemberDrawerDelegate memberDrawerDelegate, MemberHeightDelegate memberHeightDelegate, int childLimit)
        {
            int prevIndentLevel = EditorGUI.indentLevel;
            
            bool enterChildren = true;
            float margin = 2f;
            position.height = 0f;

            int childIndex = 0;
            SerializedMember childMember = serializedMember;
            while ((childMember = childMember.Next(enterChildren)) != null)
            {
                if (childLimit >= 0 && childIndex > childLimit)
                {
                    break;
                }
                
                if (isMemberVisible != null && !isMemberVisible(childMember))
                {
                    continue;
                }
                
                EditorGUI.indentLevel = childMember.MemberDepth + indentLevel;

                int childIndentLevel = EditorGUI.indentLevel;
                if (getMemberIndentLevelOffset != null)
                {
                    EditorGUI.indentLevel += getMemberIndentLevelOffset(childMember);
                }

                bool childEnabled = GUI.enabled;
                if (isMemberEditable != null && !isMemberEditable(childMember))
                {
                    GUI.enabled = false;
                }

                string displayName = GetSerializedMemberDisplayName(childMember);

                string description = GetSerializedMemberDescription(childMember);
                
                GUIContent childMemberLabel = new GUIContent(displayName, description);
                if (memberDrawerDelegate != null && memberHeightDelegate != null)
                {
                    position.height = memberHeightDelegate(childMember, childMemberLabel);
                    EditorGUI.BeginChangeCheck();
                    enterChildren = memberDrawerDelegate(position, childMember, childMemberLabel) && childMember.HasChildren;
                    if (EditorGUI.EndChangeCheck())
                    {
                        break;
                    }
                }
                else
                {
                    position.height = GetSerializedMemberHeight(childMember, childMemberLabel);
                    EditorGUI.BeginChangeCheck();
                    enterChildren = SerializedMemberField(position, childMember, childMemberLabel) && childMember.HasChildren;
                    if (EditorGUI.EndChangeCheck())
                    {
                        break;
                    }
                }

                position.y += position.height + margin;

                EditorGUI.indentLevel = childIndentLevel;

                GUI.enabled = childEnabled;

                childIndex++;
            }

            EditorGUI.indentLevel = prevIndentLevel;
        }

        public static float GetVisibleChildrenHeight(SerializedProperty property)
        {
            return GetVisibleChildrenHeight(property, null, null, -1);
        }

        public static float GetVisibleChildrenHeight(SerializedProperty property, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetVisibleChildrenHeight(property, null, propertyHeightDelegate, -1);
        }

        public static float GetVisibleChildrenHeight(SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible)
        {
            return GetVisibleChildrenHeight(property, isPropertyVisible, null, -1);
        }

        public static float GetVisibleChildrenHeight(SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetVisibleChildrenHeight(property, isPropertyVisible, propertyHeightDelegate, -1);
        }

        public static float GetVisibleChildrenHeight(SerializedProperty property, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            if (property.hasVisibleChildren)
            {
                SerializedProperty iterator = property.Copy();
                return GetVisiblePropertiesHeight(iterator, iterator.GetEndProperty(), isPropertyVisible, propertyHeightDelegate, childLimit);
            }

            return 0;
        }

        public static float GetVisiblePropertiesHeight(SerializedObject serializedObject)
        {
            return GetVisiblePropertiesHeight(serializedObject, null, null, -1);
        }

        public static float GetVisiblePropertiesHeight(SerializedObject serializedObject, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetVisiblePropertiesHeight(serializedObject, null, propertyHeightDelegate, -1);
        }

        public static float GetVisiblePropertiesHeight(SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible)
        {
            return GetVisiblePropertiesHeight(serializedObject, isPropertyVisible, null, -1);
        }

        public static float GetVisiblePropertiesHeight(SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetVisiblePropertiesHeight(serializedObject, isPropertyVisible, propertyHeightDelegate, -1);
        }

        public static float GetVisiblePropertiesHeight(SerializedObject serializedObject, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            return GetVisiblePropertiesHeight(serializedObject.GetIterator(), null, isPropertyVisible, propertyHeightDelegate, childLimit);
        }

        private static float GetVisiblePropertiesHeight(SerializedProperty iterator, SerializedProperty endProperty, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate, int childLimit)
        {
            bool enterChildren = true;

            float height = 0;
            float margin = 2f;

            int childIndex = 0;
            while ((childLimit < 0 || childIndex < childLimit) && iterator.NextVisible(enterChildren) && (endProperty == null || !SerializedProperty.EqualContents(iterator, endProperty)))
            {
                SerializedProperty childProperty = iterator.Copy();

                if (isPropertyVisible != null && !isPropertyVisible(childProperty))
                {
                    continue;
                }

                GUIContent childLabel = new GUIContent(childProperty.displayName, childProperty.tooltip);
                if (propertyHeightDelegate != null)
                {
                    height += propertyHeightDelegate(childProperty, childLabel, false);
                }
                else
                {
                    height += EditorGUI.GetPropertyHeight(childProperty, childLabel, false);
                }
                enterChildren = childProperty.isExpanded && childProperty.hasVisibleChildren;

                height += margin;

                childIndex++;
            }
            return height - margin;
        }

        public static float GetSerializedMembersHeight(SerializedMember serializedMember, GUIContent label)
        {
            return GetSerializedMembersHeight(serializedMember, label, null, null, -1);
        }

        public static float GetSerializedMembersHeight(SerializedMember serializedMember, GUIContent label, MemberBooleanCondition isMemberVisible, MemberHeightDelegate memberHeightDelegate, int childLimit)
        {
            float height = EditorGUIUtility.singleLineHeight;
            if (serializedMember.IsExpanded && serializedMember.HasChildren)
            {
                float margin = 2f;
                height += margin;
                height += GetSerializedMemberChildrenHeight(serializedMember, isMemberVisible, memberHeightDelegate, -1);
            }
            return height;
        }

        public static float GetSerializedMemberChildrenHeight(SerializedMember serializedMember)
        {
            return GetSerializedMemberChildrenHeight(serializedMember, null, null, -1);
        }

        public static float GetSerializedMemberChildrenHeight(SerializedMember serializedMember, MemberBooleanCondition isMemberVisible, MemberHeightDelegate memberHeightDelegate, int childLimit)
        {
            bool enterChildren = true;

            float height = 0;
            float margin = 2f;

            int childIndex = 0;
            SerializedMember childMember = serializedMember;
            while ((childMember = childMember.Next(enterChildren)) != null)
            {
                if (childLimit >= 0 && childIndex > childLimit)
                {
                    break;
                }

                if (isMemberVisible != null && !isMemberVisible(childMember))
                {
                    continue;
                }
                
                string displayName = childMember.DisplayName;
                if (displayName == null)
                {
                    displayName = ObjectNames.NicifyVariableName(childMember.MemberName);
                }
                
                GUIContent childMemberLabel = new GUIContent(displayName);
                if (memberHeightDelegate != null)
                {
                    height += memberHeightDelegate(childMember, childMemberLabel);
                }
                else
                {
                    height += GetSerializedMemberHeight(childMember, childMemberLabel);
                }
                enterChildren = childMember.IsExpanded && childMember.HasChildren;

                height += margin;
                
                childIndex++;
            }

            return height - margin;
        }

        public static void PropertyField(Rect position, SerializedProperty property, GUIContent label)
        {
            PropertyField(position, property, label, false, null, null, null, null, null);
        }

        public static void PropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            PropertyField(position, property, label, includeChildren, null, null, null, null, null);
        }
        
        public static void PropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset)
        {
            PropertyField(position, property, label, includeChildren, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, null, null);
        }

        public static void PropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            PropertyField(position, property, label, includeChildren, null, null, null, propertyDrawerDelegate, propertyHeightDelegate);
        }

        public static void PropertyField(Rect position, SerializedProperty property, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            if (!includeChildren || !property.hasVisibleChildren || property.HasPropertyDrawer())
            {
                EditorGUI.PropertyField(position, property, label, includeChildren);
            }
            else
            {
                position.height = GetDefaultPropertyHeight(property, label);
                DrawDefaultPropertyField(position, property, label);
                if (property.isExpanded && property.hasVisibleChildren)
                {
                    float margin = 2f;
                    position.y += position.height + margin;
                    EditorGUI.indentLevel++;
                    DrawVisibleChildren(position, property, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, -1);
                    EditorGUI.indentLevel--;
                }
            }
        }

        public static float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return GetPropertyHeight(property, label, false, null, null);
        }

        public static float GetPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren)
        {
            return GetPropertyHeight(property, label, includeChildren, null, null);
        }

        public static float GetPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible)
        {
            return GetPropertyHeight(property, label, includeChildren, isPropertyVisible, null);
        }

        public static float GetPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetPropertyHeight(property, label, includeChildren, null, propertyHeightDelegate);
        }

        public static float GetPropertyHeight(SerializedProperty property, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            if (!includeChildren || !property.hasVisibleChildren || property.HasPropertyDrawer())
            {
                return EditorGUI.GetPropertyHeight(property, label, includeChildren);
            }
            else
            {
                float height = GetDefaultPropertyHeight(property, label);
                if (property.isExpanded && property.hasVisibleChildren)
                {
                    float margin = 2f;
                    height += margin;
                    height += GetVisibleChildrenHeight(property, isPropertyVisible, propertyHeightDelegate, -1);
                }
                return height;
            }
        }
        
        public static void EditorField(Rect position, SerializedObject serializedObject, GUIContent label)
        {
            EditorField(position, serializedObject, label, false, null, null, null, null, null);
        }

        public static void EditorField(Rect position, SerializedObject serializedObject, GUIContent label, bool includeChildren)
        {
            EditorField(position, serializedObject, label, includeChildren, null, null, null, null, null);
        }

        public static void EditorField(Rect position, SerializedObject serializedObject, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset)
        {
            EditorField(position, serializedObject, label, includeChildren, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, null, null);
        }

        public static void EditorField(Rect position, SerializedObject serializedObject, GUIContent label, bool includeChildren, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            EditorField(position, serializedObject, label, includeChildren, null, null, null, propertyDrawerDelegate, propertyHeightDelegate);
        }

        public static void EditorField(Rect position, SerializedObject serializedObject, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, SerializedPropertyBooleanCondition isPropertyEditable, SerializedPropertyIntegerCondition getPropertyIndentLevelOffset, ChildPropertyDrawerDelegate propertyDrawerDelegate, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            DrawVisibleProperties(position, serializedObject, isPropertyVisible, isPropertyEditable, getPropertyIndentLevelOffset, propertyDrawerDelegate, propertyHeightDelegate, -1);
        }

        public static float GetEditorHeight(SerializedObject serializedObject, GUIContent label)
        {
            return GetEditorHeight(serializedObject, label, false, null, null);
        }

        public static float GetEditorHeight(SerializedObject serializedObject, GUIContent label, bool includeChildren)
        {
            return GetEditorHeight(serializedObject, label, includeChildren, null, null);
        }

        public static float GetEditorHeight(SerializedObject serializedObject, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible)
        {
            return GetEditorHeight(serializedObject, label, includeChildren, isPropertyVisible, null);
        }

        public static float GetEditorHeight(SerializedObject serializedObject, GUIContent label, bool includeChildren, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetEditorHeight(serializedObject, label, includeChildren, null, propertyHeightDelegate);
        }

        public static float GetEditorHeight(SerializedObject serializedObject, GUIContent label, bool includeChildren, SerializedPropertyBooleanCondition isPropertyVisible, ChildPropertyHeightDelegate propertyHeightDelegate)
        {
            return GetVisiblePropertiesHeight(serializedObject, isPropertyVisible, propertyHeightDelegate, -1);
        }

        public static bool DrawDefaultPropertyField(Rect position, SerializedProperty property, GUIContent label)
        {
            object[] arguments = new object[] { position, property, label };
            MethodInfo method = typeof(EditorGUI).GetMethod("DefaultPropertyField", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            return (bool)method.Invoke(null, arguments);
        }

        public static float GetDefaultPropertyHeight(SerializedProperty property, GUIContent label)
        {
            object[] arguments = new object[] { property, label };
            MethodInfo method = typeof(EditorGUI).GetMethod("GetSinglePropertyHeight", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            return (float)method.Invoke(null, arguments);
        }

        public static float GetSerializedMemberHeight(SerializedMember member, GUIContent label)
        {
            if (member.HasChildren)
            {
                float height = EditorGUIUtility.singleLineHeight;
                if (member.IsExpanded)
                {
                    Type memberType = member.MemberType;
                    if (typeof(IList).IsAssignableFrom(memberType) && !typeof(string).IsAssignableFrom(memberType))
                    {
                        height += 2f;
                        height += EditorGUIUtility.singleLineHeight;
                    }
                }
                return height;
            }
            else
            {
                return GetValueHeight(member.Value, member.MemberType, label);
            }
        }

        public static bool SerializedMemberField(Rect position, SerializedMember member, GUIContent label)
        {
            if (member.HasChildren)
            {
                position.height = EditorGUIUtility.singleLineHeight;
                member.IsExpanded = EditorGUI.Foldout(position, member.IsExpanded, label);
                if (member.IsExpanded)
                {
                    Type memberType = member.MemberType;
                    if (typeof(IList).IsAssignableFrom(memberType) && !typeof(string).IsAssignableFrom(memberType))
                    {
                        position.y += position.height + 2f;
                        position.height = EditorGUIUtility.singleLineHeight;
                        EditorGUI.indentLevel++;

                        EditorGUI.BeginChangeCheck();

                        label.text = "Size";
                        object value = ValueField(position, member.Value, member.MemberType, label);

                        if (EditorGUI.EndChangeCheck())
                        {
                            member.Value = value;
                        }

                        EditorGUI.indentLevel--;
                    }
                }
            }
            else
            {
                EditorGUI.BeginChangeCheck();

                object value = ValueField(position, member.Value, member.MemberType, label);

                if (EditorGUI.EndChangeCheck())
                {
                    member.Value = value;
                }
            }
            return member.IsExpanded;
        }

        public static float GetValueHeight(object obj, Type type, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        public static object ValueField(Rect position, object obj, Type type, GUIContent label)
        {
            if (typeof(IList).IsAssignableFrom(type) && !typeof(string).IsAssignableFrom(type))
            {
                obj = ArraySizeField(position, label, obj as IList, type);
            }
            else if (typeof(int).IsAssignableFrom(type))
            {
                obj = EditorGUI.IntField(position, label, (int)obj);
            }
            else if (typeof(bool).IsAssignableFrom(type))
            {
                obj = EditorGUI.Toggle(position, label, (bool)obj);
            }
            else if (typeof(float).IsAssignableFrom(type))
            {
                obj = EditorGUI.FloatField(position, label, (float)obj);
            }
            else if (typeof(string).IsAssignableFrom(type))
            {
                obj = EditorGUI.TextField(position, label, (string)obj);
            }
            else if (typeof(Color).IsAssignableFrom(type))
            {
                obj = EditorGUI.ColorField(position, label, (Color)obj);
            }
            else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                obj = EditorGUI.ObjectField(position, obj as UnityEngine.Object, type, true);
            }
            else if (typeof(LayerMask).IsAssignableFrom(type))
            {
                obj = ExtendedEditorGUI.LayerMaskField(position, label, (LayerMask)obj);
            }
            else if (type != null && type.IsEnum)
            {
                obj = EditorGUI.EnumPopup(position, label, (Enum)obj);
            }
            else if (typeof(Vector2).IsAssignableFrom(type))
            {
                obj = EditorGUI.Vector2Field(position, label, (Vector2)obj);
            }
            else if (typeof(Vector3).IsAssignableFrom(type))
            {
                obj = EditorGUI.Vector3Field(position, label, (Vector3)obj);
            }
            else if (typeof(Vector4).IsAssignableFrom(type))
            {
                obj = EditorGUI.Vector4Field(position, label, (Vector4)obj);
            }
            else if (typeof(Rect).IsAssignableFrom(type))
            {
                obj = EditorGUI.RectField(position, label, (Rect)obj);
            }
            else if (typeof(char).IsAssignableFrom(type))
            {
                obj = ExtendedEditorGUI.CharField(position, label, (char)obj);
            }
            else if (typeof(AnimationCurve).IsAssignableFrom(type))
            {
                obj = EditorGUI.CurveField(position, label, (AnimationCurve)obj);
            }
            else if (typeof(Bounds).IsAssignableFrom(type))
            {
                obj = EditorGUI.BoundsField(position, label, (Bounds)obj);
            }
            else if (typeof(Gradient).IsAssignableFrom(type))
            {
                obj = ExtendedEditorGUI.GradientField(position, label, (Gradient)obj);
            }
            else if (typeof(Quaternion).IsAssignableFrom(type))
            {
                obj = ExtendedEditorGUI.QuaternionField(position, label, (Quaternion)obj);
            }
            else if (typeof(Vector2Int).IsAssignableFrom(type))
            {
                obj = EditorGUI.Vector2IntField(position, label, (Vector2Int)obj);
            }
            else if (typeof(Vector3Int).IsAssignableFrom(type))
            {
                obj = EditorGUI.Vector3IntField(position, label, (Vector3Int)obj);
            }
            else if (typeof(RectInt).IsAssignableFrom(type))
            {
                obj = EditorGUI.RectIntField(position, label, (RectInt)obj);
            }
            else if (typeof(BoundsInt).IsAssignableFrom(type))
            {
                obj = EditorGUI.BoundsIntField(position, label, (BoundsInt)obj);
            }
            else if (typeof(Type).IsAssignableFrom(type))
            {
                obj = SearchPopupGUI.TypeSearchPopupField(position, (Type)obj, label, null, null);
            }
            else
            {
                if (obj != null)
                {
                    Type actualType = obj.GetType();
                    MethodInfo serializeMethod = actualType.GetMethod("Serialize", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[] { }, null);
                    MethodInfo deserializeMethod = actualType.GetMethod("Deserialize", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[] { typeof(string) }, null);
                    if (serializeMethod != null && deserializeMethod != null)
                    {
                        string serializedString = (string)serializeMethod.Invoke(obj, null);
                        serializedString = EditorGUI.TextField(position, label, serializedString);
                        return deserializeMethod.Invoke(obj, new object[] { serializedString });
                    }
                    else
                    {
                        EditorGUI.LabelField(position, label, new GUIContent(obj.ToString()));
                    }
                }
                else
                {
                    EditorGUI.LabelField(position, label, new GUIContent("null"));
                }
            }

            return obj;
        }
    }
}
#endif