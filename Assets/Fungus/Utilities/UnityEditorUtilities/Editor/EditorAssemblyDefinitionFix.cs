#if UNITY_EDITOR
using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.UnityEditorUtilities
{

    public class EditorAssemblyDefinitionFix
    {
        [MenuItem("Assembly/Surround Editor Files With Compile Flag", false, 0)]
        public static void FixEditorFiles()
        {
            UpdateEditorFiles(new DirectoryInfo(Application.dataPath));
            AssetDatabase.Refresh();
        }

        static void UpdateEditorFiles(DirectoryInfo Dir)
        {
            string compileFlagStart = "#if UNITY_EDITOR";
            string compileFlagEnd = "#endif";

            try
            {
                FileInfo[] editorfiles = Dir.GetDirectories("Editor", SearchOption.AllDirectories).SelectMany(x => x.GetFiles("*", SearchOption.AllDirectories)).Where(x => x.Extension == ".cs").ToArray();
                for (int i = 0; i < editorfiles.Length; i++)
                {
                    FileInfo file = editorfiles[i];
                    if (EditorUtility.DisplayCancelableProgressBar("Updating Editor Files", string.Format("Updating '{0}'", file.Name), (float)i/editorfiles.Length))
                    {
                        break;
                    }

                    StreamReader sr = file.OpenText();
                    string firstLine = sr.ReadLine();
                    sr.Close();
                    if (firstLine.Trim() != compileFlagStart.Trim())
                    {
                        string fileContent = File.ReadAllText(file.FullName);
                        File.WriteAllText(file.FullName, compileFlagStart + Environment.NewLine + fileContent + Environment.NewLine + compileFlagEnd);
                    }
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }
    }

}
#endif