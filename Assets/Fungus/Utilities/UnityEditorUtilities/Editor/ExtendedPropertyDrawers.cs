﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ImprobableStudios.SearchPopupUtilities;
using System.Collections;
using ImprobableStudios.SerializedPropertyUtilities;

namespace ImprobableStudios.UnityEditorUtilities
{

    [CustomPropertyDrawer(typeof(MinAttribute))]
    public class MinDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            MinAttribute minAttribute = attribute as MinAttribute;
            DrawProperty(position, property, label, minAttribute.min);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, float min)
        {
            EditorGUI.BeginProperty(position, label, property);
            if (property.propertyType == SerializedPropertyType.Integer)
            {
                EditorGUI.BeginChangeCheck();
                int value = property.intValue;
                value = EditorGUI.IntField(position, label, value);
                if (EditorGUI.EndChangeCheck())
                {
                    value = Math.Max((int)min, value);
                    property.intValue = value;
                }
            }
            else if (property.propertyType == SerializedPropertyType.Float)
            {
                EditorGUI.BeginChangeCheck();
                float value = property.floatValue;
                value = EditorGUI.FloatField(position, label, value);
                if (EditorGUI.EndChangeCheck())
                {
                    value = Math.Max(min, value);
                    property.floatValue = value;
                }
            }
            EditorGUI.EndProperty();
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(int), typeof(float) };
        }
    }

    [CustomPropertyDrawer(typeof(TypeNameFieldAttribute))]
    public class TypeNameFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            TypeNameFieldAttribute typeNameAttribute = attribute as TypeNameFieldAttribute;
            DrawProperty(position, property, label, typeNameAttribute.NullLabel, typeNameAttribute.BaseTypes);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, GUIContent nullLabel, Type[] baseTypes)
        {
            string nameSuffix = " Name";
            if (label.text.EndsWith(nameSuffix, StringComparison.Ordinal))
            {
                label.text = label.text.Substring(0, label.text.Length - nameSuffix.Length);
            }

            SearchPopupGUI.TypeSearchPopupField(position, property, label, nullLabel, baseTypes);
        }
    }
    
    [CustomPropertyDrawer(typeof(AssetGuidFieldAttribute))]
    public class AssetGuidFieldDrawer : PropertyAttributeDrawer
    {
        UnityEngine.Object m_cachedAssetObject;

        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            AssetGuidFieldAttribute a = attribute as AssetGuidFieldAttribute;
            DrawProperty(position, property, label, a.ObjType, ref m_cachedAssetObject);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, Type objType, ref UnityEngine.Object cachedAssetObject)
        {
            ExtendedEditorGUI.AssetGuidField(position, property, label, objType, ref cachedAssetObject);
        }
    }

    [CustomPropertyDrawer(typeof(TagFieldAttribute))]
    public class TagFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            string tagValue = property.stringValue;
            tagValue = EditorGUI.TagField(position, label, tagValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = tagValue;
            }
            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(SceneNameFieldAttribute))]
    public class SceneNameFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            List<string> scenePaths = GetScenePaths(true).ToList();
            int selectedIndex = scenePaths.FindIndex(i => GetSceneName(i) == property.stringValue);
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, scenePaths.Select(i => new GUIContent(i)).ToArray());
            if (EditorGUI.EndChangeCheck())
            {
                property.stringValue = GetSceneName(scenePaths[selectedIndex]);
            }
            EditorGUI.EndProperty();
        }

        public static string[] GetScenePaths(bool onlyEnabled)
        {
            List<string> sceneList = new List<string>();
            foreach (UnityEditor.EditorBuildSettingsScene scene in GetScenesInBuild(onlyEnabled))
            {
                if (scene.enabled)
                {
                    sceneList.Add(scene.path);
                }
            }
            return sceneList.ToArray();
        }
        
        public static string GetSceneName(EditorBuildSettingsScene scene)
        {
            return GetSceneName(scene.path);
        }

        public static string GetSceneName(string scenePath)
        {
            return Path.GetFileNameWithoutExtension(scenePath);
        }

        public static EditorBuildSettingsScene[] GetScenesInBuild(bool onlyEnabled)
        {
            if (onlyEnabled)
            {
                return (from scene in EditorBuildSettings.scenes where scene.enabled select scene).ToArray();
            }
            else
            {
                return EditorBuildSettings.scenes;
            }
        }
    }
    
    [CustomPropertyDrawer(typeof(FlexibleTextFieldAttribute))]
    public class FlexibleTextFieldDrawer : PropertyAttributeDrawer
    {
        protected float m_positionWidth = 0f;

        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label, ref m_positionWidth);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            FlexibleTextFieldAttribute flexibleTextFieldAttribute = base.attribute as FlexibleTextFieldAttribute;
            float contentWidth = m_positionWidth;
            if (!string.IsNullOrEmpty(label.text))
            {
                contentWidth -= EditorGUIUtility.labelWidth;
            }
            return ExtendedEditorGUI.GetFlexibleTextAreaHeight(property.stringValue, contentWidth, flexibleTextFieldAttribute.MinLines, flexibleTextFieldAttribute.MaxLines);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(string) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, ref float positionWidth)
        {
            if (Event.current.type != EventType.Layout)
            {
                positionWidth = position.width;
            }

            ExtendedEditorGUI.FlexibleTextField(position, property, label);
        }
    }

    [CustomPropertyDrawer(typeof(LayerFieldAttribute))]
    public class LayerFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(int) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            int layerValue = property.intValue;
            layerValue = EditorGUI.LayerField(position, label, layerValue);
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = layerValue;
            }
            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(SortingLayerFieldAttribute))]
    public class SortingLayerFieldDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(int) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ExtendedEditorGUI.SortingLayerField(position, property, label);
        }
    }

    [CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
    public class EnumFlagsDrawer : PropertyAttributeDrawer
    {
        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(Enum) };
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Enum targetEnum = property.GetPropertyValue() as Enum;

            EditorGUI.BeginProperty(position, label, property);
            Enum enumNew = EditorGUI.EnumFlagsField(position, label, targetEnum);
            property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());
            EditorGUI.EndProperty();
        }
    }


    [CustomPropertyDrawer(typeof(FoldoutAttribute))]
    public class FoldoutDrawer : PropertyAttributeDrawer
    {
        protected PropertyDrawer m_propertyDrawer;

        public override void OnPropertyGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_propertyDrawer == null)
            {
                m_propertyDrawer = fieldInfo.CreatePropertyDrawer();
            }

            DrawProperty(position, property, label, m_propertyDrawer);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (m_propertyDrawer == null)
            {
                m_propertyDrawer = fieldInfo.CreatePropertyDrawer();
            }

            float height = 0;
            if (property.isExpanded)
            {
                if (m_propertyDrawer != null)
                {
                    height += m_propertyDrawer.GetPropertyHeight(property, label);
                }
                else
                {
                    if (property.hasVisibleChildren)
                    {
                        height += ExtendedEditorGUI.GetVisibleChildrenHeight(property);
                    }
                    else
                    {
                        height += ExtendedEditorGUI.GetDefaultPropertyHeight(property, label);
                    }
                }
            }

            return EditorGUIUtility.singleLineHeight + height;
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(object) };
        }
        
        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label, PropertyDrawer propertyDrawer)
        {
            Rect foldoutRect = EditorGUI.IndentedRect(position);
            foldoutRect.height = EditorGUIUtility.singleLineHeight;

            Rect propertyRect = EditorGUI.IndentedRect(position);
            propertyRect.yMin += EditorGUIUtility.singleLineHeight;

            property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label, true);
            if (property.isExpanded)
            {
                if (propertyDrawer != null)
                {
                    propertyDrawer.OnGUI(propertyRect, property, label);
                }
                else
                {
                    if (property.hasVisibleChildren)
                    {
                        ExtendedEditorGUI.DrawVisibleChildren(position, property);
                    }
                    else
                    {
                        ExtendedEditorGUI.DrawDefaultPropertyField(position, property, label);
                    }
                }
            }
        }
    }

}
#endif