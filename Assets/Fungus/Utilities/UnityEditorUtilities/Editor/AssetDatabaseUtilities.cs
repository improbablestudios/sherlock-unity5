﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ImprobableStudios.UnityEditorUtilities
{

    public static class AssetDatabaseUtilities
    {
        public static bool GenerateObjectFromTemplate(string templatePath, string destinationPath)
        {
            string destinationFilename = destinationPath;
            string destinationFolder = destinationPath;
            int folderStartIndex = destinationFilename.LastIndexOf("/");
            if (folderStartIndex > -1 && folderStartIndex < destinationFilename.Length)
            {
                destinationFilename = destinationFilename.Substring(folderStartIndex + 1);
            }
            destinationFolder = destinationFolder.Replace("/" + destinationFilename, "");
            int extensionStartIndex = destinationFilename.LastIndexOf(".");
            if (extensionStartIndex > -1 && extensionStartIndex < destinationFilename.Length)
            {
                destinationFilename = destinationFilename.Remove(extensionStartIndex);
            }
            if (AssetDatabase.FindAssets(destinationFilename, new string[] { destinationFolder }).Length == 0)
            {
                AssetDatabase.CopyAsset(templatePath, destinationPath);
                AssetDatabase.Refresh();
            }
            return true;
        }

        public static void PingObject(UnityEngine.Object objectToPing)
        {
            EditorUtility.FocusProjectWindow();
            if (!Application.isPlaying)
            {
                EditorGUIUtility.PingObject(objectToPing);
            }
            Selection.activeGameObject = objectToPing as GameObject;
            GUIUtility.ExitGUI();
        }

        public static void PingObjectAtPath(string path)
        {
            GameObject objectToPing = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            PingObject(objectToPing);
            Selection.activeGameObject = objectToPing;
        }

        public static T FindObject<T>(string searchString, string folder) where T : UnityEngine.Object
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                string[] guids = AssetDatabase.FindAssets(searchString, new string[] { folder });
                if (guids.Length > 0)
                {
                    string objectPath = AssetDatabase.GUIDToAssetPath(guids[0]);
                    return AssetDatabase.LoadAssetAtPath(objectPath, typeof(T)) as T;
                }
            }
            return null;
        }

        public static string[] FindObjectGUIDs<T>(string searchString, string[] searchFolders) where T : UnityEngine.Object
        {
            return FindObjectGUIDs(typeof(T), searchString, searchFolders);
        }

        public static string[] FindObjectGUIDs(Type type, string searchString, string[] searchFolders)
        {
            string typeString = type.Name;
            string searchTypeString = "t:" + typeString;
            return AssetDatabase.FindAssets(searchString + " " + searchTypeString, searchFolders);
        }

        public static string[] FindObjectPaths<T>(string searchString, string[] searchFolders) where T : UnityEngine.Object
        {
            return FindObjectPaths(typeof(T), searchString, searchFolders);
        }

        public static string[] FindObjectPaths(Type type, string searchString, string[] searchFolders)
        {
            string[] matchGUIDs = FindObjectGUIDs(type, searchString, searchFolders);
            List<string> matchObjectPaths = new List<string>();
            for (int i = 0; i < matchGUIDs.Length; i++)
            {
                matchObjectPaths.Add(AssetDatabase.GUIDToAssetPath(matchGUIDs[i]));
            }
            return matchObjectPaths.ToArray();
        }

        public static T[] FindObjects<T>(string searchString, string[] searchFolders) where T : UnityEngine.Object
        {
            return FindObjects(typeof(T), searchString, searchFolders).Cast<T>().ToArray();
        }

        public static UnityEngine.Object[] FindObjects(Type type, string searchString, string[] searchFolders)
        {
            string[] matchObjectPaths = FindObjectPaths(type, searchString, searchFolders);
            List<UnityEngine.Object> matchObjects = new List<UnityEngine.Object>();
            for (int i = 0; i < matchObjectPaths.Length; i++)
            {
                matchObjects.Add(AssetDatabase.LoadAssetAtPath(matchObjectPaths[i], type));
            }
            return matchObjects.ToArray();
        }

        public static T[] FindComponent<T>() where T : Component
        {
            return FindComponents(typeof(T)).Cast<T>().ToArray();
        }

        public static Component[] FindComponents(Type type)
        {
            return FindComponents(new Type[] { type });
        }

        public static Component[] FindComponents(Type[] types)
        {
            List<Component> list = new List<Component>();
            
            foreach (GameObject prefab in FindPrefabs())
            {
                foreach (Type type in types)
                {
                    Component[] components = prefab.GetComponentsInChildren(type, true);
                    if (components.Length > 0)
                    {
                        list.AddRange(components);
                    }
                }
            }

            return list.ToArray();
        }

        public static GameObject[] FindGameObjectsWithComponents(Type[] types)
        {
            return FindComponents(types).Select(x => x.gameObject).ToArray();
        }

        public static GameObject[] FindGameObjectsWithComponent(Type type)
        {
            return FindComponents(type).Select(x => x.gameObject).ToArray();
        }

        public static GameObject[] FindGameObjects()
        {
            return FindComponents(typeof(Transform)).Select(x => x.gameObject).ToArray();
        }

        public static GameObject[] FindPrefabsWithComponent(Type type)
        {
            return FindPrefabsWithComponents(new Type[] { type });
        }

        public static GameObject[] FindPrefabs()
        {
            List<GameObject> list = new List<GameObject>();

            string[] paths = AssetDatabaseUtilities.FindObjectPaths<GameObject>("", new string[] { "Assets" });

            foreach (string path in paths)
            {
                if (path.EndsWith(".prefab"))
                {
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    if (go != null)
                    {
                        list.Add(go);
                    }
                }
            }

            return list.ToArray();
        }

        public static GameObject[] FindPrefabsWithComponents(Type[] types)
        {
            List<GameObject> list = new List<GameObject>();
            
            foreach (GameObject prefab in FindPrefabs())
            {
                if (types.Any(x => prefab.GetComponentInChildren(x, true) != null))
                {
                    list.Add(prefab);
                }
            }

            return list.ToArray();
        }

        public static bool FileExists(string searchString, string folder)
        {
            if (searchString.Length > 0)
            {
                if (AssetDatabase.FindAssets(searchString, new string[] { folder }).Length > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
#endif