﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace ImprobableStudios.UnityEditorUtilities
{
    
    [CustomPropertyDrawer(typeof(SeparatorAttribute))]
    public class SeparatorDrawer : DecoratorDrawer
    {
        public override void OnGUI(Rect position)
        {
            position.y += GetHeight() - base.GetHeight();
            ExtendedEditorGUI.LineSeparator(position);
        }

        public override float GetHeight()
        {
            SeparatorAttribute separatorAttribute = attribute as SeparatorAttribute;
            if (separatorAttribute != null)
            {
                return separatorAttribute.Height;
            }
            return base.GetHeight();
        }
    }

}
#endif