#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

namespace ImprobableStudios.UnityEditorUtilities
{

    public static class EditorWindowUtilities
    {
        [System.Flags]
        public enum ViewEdge
        {
            None = 0,
            Left = 1,
            Bottom = 2,
            Top = 4,
            Right = 8,
            BottomLeft = Bottom | Left, // 0x00000003
            BottomRight = Right | Bottom, // 0x0000000A
            TopLeft = Top | Left, // 0x00000005
            TopRight = Right | Top, // 0x0000000C
            FitsVertical = Top | Bottom, // 0x00000006
            FitsHorizontal = Right | Left, // 0x00000009
            Before = TopLeft, // 0x00000005
            After = BottomRight, // 0x0000000A
        }

        #region Reflection Types
        private class _EditorWindow
        {
            private EditorWindow m_instance;
            private Type m_type;

            public _EditorWindow(EditorWindow instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public object m_Parent
            {
                get
                {
                    FieldInfo field = m_type.GetField("m_Parent", BindingFlags.Instance | BindingFlags.NonPublic);
                    return field.GetValue(m_instance);
                }
            }
        }

        private class _DockArea
        {
            public static Type DockAreaType
            {
                get
                {
                    return Type.GetType("UnityEditor.DockArea,UnityEditor");
                }
            }

            private object m_instance;
            private Type m_type;

            public _DockArea(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public object window
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("window", BindingFlags.Instance | BindingFlags.Public);
                    return property.GetValue(m_instance, null);
                }
            }

            public object s_OriginalDragSource
            {
                get
                {
                    FieldInfo field = m_type.GetField("s_OriginalDragSource", BindingFlags.Static | BindingFlags.NonPublic);
                    return field.GetValue(null);
                }
                set
                {
                    FieldInfo field = m_type.GetField("s_OriginalDragSource", BindingFlags.Static | BindingFlags.NonPublic);
                    field.SetValue(null, value);
                }
            }

            public object DragOver(EditorWindow window, Vector2 mouseScreenPosition)
            {
                MethodInfo method = m_type.GetMethod("DragOver", BindingFlags.Instance | BindingFlags.Public);
                return method.Invoke(m_instance, new object[] { window, mouseScreenPosition });
            }

            public void PerformDrop(EditorWindow window, object dropInfo, Vector2 mouseScreenPosition)
            {
                MethodInfo method = m_type.GetMethod("PerformDrop", BindingFlags.Instance | BindingFlags.Public);
                method.Invoke(m_instance, new object[] { window, dropInfo, mouseScreenPosition });
            }
        }

        private class _ContainerWindow
        {
            private object m_instance;
            private Type m_type;

            public _ContainerWindow(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public object rootView
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("rootView", BindingFlags.Instance | BindingFlags.Public);
                    return property.GetValue(m_instance, null);
                }
            }

            public object rootSplitView
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("rootSplitView", BindingFlags.Instance | BindingFlags.Public);
                    return property.GetValue(m_instance, null);
                }
            }

            public void ToggleMaximize()
            {
                MethodInfo method = m_type.GetMethod("ToggleMaximize", BindingFlags.Instance | BindingFlags.Public);
                method.Invoke(m_instance, null);
            }
        }

        private class _SplitView
        {
            private object m_instance;
            private Type m_type;

            public _SplitView(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public Rect screenPosition
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("screenPosition", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    return (Rect)property.GetValue(m_instance, null);
                }
            }

            public object DragOverRootView(Vector2 mouseScreenPosition)
            {
                MethodInfo method = m_type.GetMethod("DragOverRootView", BindingFlags.Instance | BindingFlags.Public);
                return method.Invoke(m_instance, new object[] { mouseScreenPosition });
            }

            public object RootViewDropZone(ViewEdge edge, Vector2 mousePos, Rect screenRect)
            {
                MethodInfo method = m_type.GetMethod("RootViewDropZone", BindingFlags.Instance | BindingFlags.NonPublic);
                object splitViewEdgeNum = Convert.ChangeType(edge, typeof(ulong));
                return method.Invoke(m_instance, new object[] { splitViewEdgeNum, mousePos, screenRect });
            }
        }

        private class _ExtraDropInfo
        {
            private object m_instance;

            public static Type ViewEdgeType
            {
                get
                {
                    return Type.GetType("UnityEditor.SplitView+ViewEdge,UnityEditor");
                }
            }

            public static Type ExtraDropInfoType
            {
                get
                {
                    return Type.GetType("UnityEditor.SplitView+ExtraDropInfo,UnityEditor");
                }
            }

            public object instance
            {
                get
                {
                    return m_instance;
                }
            }

            public _ExtraDropInfo(object instance)
            {
                m_instance = instance;
            }

            public _ExtraDropInfo(bool rootWindow, ViewEdge edge, int index)
            {
                string splitViewEdgeName = edge.ToString();
                object splitViewEdgeEnum = Enum.Parse(ViewEdgeType, splitViewEdgeName);
                m_instance = Activator.CreateInstance(ExtraDropInfoType, new object[] { rootWindow, splitViewEdgeEnum, index });
            }
        }

        private class _View
        {
            private object m_instance;
            private Type m_type;

            public _View(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public Rect screenPosition
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("screenPosition", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    return (Rect)property.GetValue(m_instance, null);
                }
            }

            public object m_Window
            {
                get
                {
                    FieldInfo field = m_type.GetField("m_Window", BindingFlags.Instance | BindingFlags.NonPublic);
                    return field.GetValue(m_instance);
                }
            }

            public object[] allChildren
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("allChildren", BindingFlags.Instance | BindingFlags.Public);
                    return (object[])property.GetValue(m_instance, null);
                }
            }

            public object[] children
            {
                get
                {
                    PropertyInfo property = m_type.GetProperty("children", BindingFlags.Instance | BindingFlags.Public);
                    return (object[])property.GetValue(m_instance, null);
                }
            }
        }

        private class _IDropArea
        {
            private object m_instance;
            private Type m_type;

            public static Type IDropAreaType
            {
                get
                {
                    return Type.GetType("UnityEditor.IDropArea,UnityEditor");
                }
            }

            public _IDropArea(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public object DragOver(EditorWindow window, Vector2 mouseScreenPosition)
            {
                MethodInfo method = m_type.GetMethod("DragOver", BindingFlags.Instance | BindingFlags.Public);
                return method.Invoke(m_instance, new object[] { window, mouseScreenPosition });
            }

            public void PerformDrop(EditorWindow window, object dropInfo, Vector2 mouseScreenPosition)
            {
                MethodInfo method = m_type.GetMethod("PerformDrop", BindingFlags.Instance | BindingFlags.Public);
                method.Invoke(m_instance, new object[] { window, dropInfo, mouseScreenPosition });
            }
        }

        private class _DropInfo
        {
            private object m_instance;
            private Type m_type;

            public _DropInfo(object instance)
            {
                m_instance = instance;
                m_type = instance.GetType();
            }

            public object dropArea
            {
                get
                {
                    FieldInfo field = m_type.GetField("dropArea", BindingFlags.Instance | BindingFlags.Public);
                    return field.GetValue(m_instance);
                }
            }

            public object userData
            {
                get
                {
                    FieldInfo field = m_type.GetField("userData", BindingFlags.Instance | BindingFlags.Public);
                    return field.GetValue(m_instance);
                }
                set
                {
                    FieldInfo field = m_type.GetField("userData", BindingFlags.Instance | BindingFlags.Public);
                    field.SetValue(m_instance, value);
                }
            }
        }
        #endregion

        public static Type GetProjectWindowType()
        {
            return typeof(Editor).Assembly.GetType("UnityEditor.ProjectBrowser");
        }
        public static EditorWindow GetProjectWindow()
        {
            return EditorWindow.GetWindow(GetProjectWindowType());
        }

        public static Type GetInspectorWindowType()
        {
            return typeof(Editor).Assembly.GetType("UnityEditor.InspectorWindow");
        }

        public static EditorWindow GetInspectorWindow()
        {
            return EditorWindow.GetWindow(GetInspectorWindowType());
        }

        private static FieldInfo GetInspectorWindowMode()
        {
            return GetInspectorWindowType().GetField("m_InspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
        }

        public static InspectorMode GetInspectorWindowMode(EditorWindow inspectorWindow)
        {
            return (InspectorMode)GetInspectorWindowMode().GetValue(inspectorWindow);
        }

        public static InspectorMode SetInspectorWindowMode(EditorWindow inspectorWindow)
        {
            return (InspectorMode)GetInspectorWindowMode().GetValue(inspectorWindow);
        }

        public static Type GetSettingsWindowType()
        {
            return typeof(Editor).Assembly.GetType("UnityEditor.SettingsWindow");
        }

        public static EditorWindow GetSettingsWindow()
        {
            return EditorWindow.GetWindow(GetSettingsWindowType());
        }

        public static GUI.Scope GetSettingsWindowGUIScope()
        {
            Type settingsWindowGUIScopeType = typeof(Editor).Assembly.GetType("UnityEditor.SettingsWindow+GUIScope");
            return Activator.CreateInstance(settingsWindowGUIScopeType) as GUI.Scope;
        }

        public static EditorWindow GetCurrentEditorWindow()
        {
            Type type = typeof(Editor).Assembly.GetType("UnityEditor.GUIView");
            PropertyInfo current = type.GetProperty("current", BindingFlags.Public | BindingFlags.Static);
            object currentView = current.GetValue(null, null);
            if (currentView != null)
            {
                type = typeof(Editor).Assembly.GetType("UnityEditor.HostView");
                PropertyInfo actualView = type.GetProperty("actualView", BindingFlags.NonPublic | BindingFlags.Instance);
                return (EditorWindow)actualView.GetValue(currentView, null);
            }
            return null;
        }

        public static void Dock(this EditorWindow window1, EditorWindow window2, ViewEdge dockEdge)
        {
            _EditorWindow _window1 = new _EditorWindow(window1);
            _EditorWindow _window2 = new _EditorWindow(window2);
            object hostView = _window1.m_Parent;
            _DockArea _dockArea = new _DockArea(hostView);
            _dockArea.s_OriginalDragSource = _window2.m_Parent;
            _ContainerWindow _containerWindow = new _ContainerWindow(_dockArea.window);

            Vector2 mousePos = GetDockPosition(window1, dockEdge);

            object dropInfo = null;
            object dropArea = null;
            _ExtraDropInfo _extraDropInfo = null;

            object rootSplitView = _containerWindow.rootSplitView;

            _SplitView _rootSplitView = new _SplitView(rootSplitView);

            if (rootSplitView != null)
            {
                dropInfo = _rootSplitView.DragOverRootView(mousePos);
                dropArea = rootSplitView;
                _extraDropInfo = new _ExtraDropInfo(true, dockEdge, 0);
            }
            if (dropInfo == null)
            {
                _View _containerWindowRootView = new _View(_containerWindow.rootView);
                object[] children = _containerWindowRootView.children;
                for (int i = 0; i < children.Length; i++)
                {
                    object childView = children[i];
                    _IDropArea _childDropArea = new _IDropArea(childView);
                    if (_IDropArea.IDropAreaType.IsAssignableFrom(childView.GetType()))
                    {
                        dropInfo = _childDropArea.DragOver(window2, mousePos);
                        dropArea = childView;
                        _extraDropInfo = new _ExtraDropInfo(false, dockEdge, i);
                    }
                }
            }

            if (dropInfo != null && dropArea != null)
            {
                _DropInfo _dropInfo = new _DropInfo(dropInfo);
                _IDropArea _dropArea = new _IDropArea(dropArea);
                _dropInfo.userData = _extraDropInfo.instance;
                _dropArea.PerformDrop(window2, dropInfo, mousePos);
            }
            else
            {
                Debug.Log("DropInfo or DropArea was null");
            }
        }

        private static Vector2 GetDockPosition(EditorWindow window, ViewEdge viewEdge)
        {
            Vector2 mousePosition = Vector2.zero;

            // The 20 is required to make the docking work.
            // Smaller values might not work when faking the mouse position.
            switch (viewEdge)
            {
                case ViewEdge.Left:
                    mousePosition = new Vector2(20, window.position.size.y / 2);
                    break;
                case ViewEdge.Top:
                    mousePosition = new Vector2(window.position.size.x / 2, 20);
                    break;
                case ViewEdge.Right:
                    mousePosition = new Vector2(window.position.size.x - 20, window.position.size.y / 2);
                    break;
                case ViewEdge.Bottom:
                    mousePosition = new Vector2(window.position.size.x / 2, window.position.size.y - 20);
                    break;
            }

            return GUIUtility.GUIToScreenPoint(mousePosition);
        }

        public static void MaximizeWindowParent(EditorWindow window)
        {
            _EditorWindow editorWindow = new _EditorWindow(window);
            _View view = new _View(editorWindow.m_Parent);
            _ContainerWindow containerWindow = new _ContainerWindow(view.m_Window);
            containerWindow.ToggleMaximize();
        }
    }
}
#endif
