﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace ImprobableStudios.UnityEditorUtilities
{

    public static class ScriptableObjectUtility
    {
        private static ScriptableObject m_copyBuffer;
        
        [MenuItem("CONTEXT/ScriptableObject/Remove Scriptable Object", false)]
        public static void Remove(MenuCommand menuCommand)
        {
            UnityEngine.Object target = menuCommand.context;

            if (target != null)
            {
                string path = AssetDatabase.GetAssetPath(target);
                Undo.DestroyObjectImmediate(target);
                AssetDatabase.ImportAsset(path);
            }
        }

        [MenuItem("CONTEXT/ScriptableObject/Copy Scriptable Object", false)]
        public static void Copy(MenuCommand menuCommand)
        {
            ScriptableObject target = menuCommand.context as ScriptableObject;

            if (target != null)
            {
                m_copyBuffer = ScriptableObject.CreateInstance(target.GetType());

                EditorUtility.CopySerializedManagedFieldsOnly(target, m_copyBuffer);
            }
        }

        [MenuItem("CONTEXT/ScriptableObject/Paste Scriptable Object Values", false)]
        public static void Paste(MenuCommand menuCommand)
        {
            ScriptableObject target = menuCommand.context as ScriptableObject;

            if (target != null)
            {
                if (m_copyBuffer != null)
                {
                    string path = AssetDatabase.GetAssetPath(target);
                    Undo.RecordObject(target, "Paste Scriptable Object Values");
                    EditorUtility.CopySerializedManagedFieldsOnly(m_copyBuffer, target);
                    AssetDatabase.ImportAsset(path);
                }
            }
        }

        [MenuItem("CONTEXT/ScriptableObject/Paste Scriptable Object Values", true)]
        public static bool ValidatePaste(MenuCommand menuCommand)
        {
            ScriptableObject target = menuCommand.context as ScriptableObject;

            if (target != null)
            {
                if (m_copyBuffer != null)
                {
                    return true;
                }
            }

            return false;
        }
    }

}
#endif