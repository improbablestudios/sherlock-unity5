﻿using UnityEngine;
using System;

namespace ImprobableStudios.UnityEditorUtilities
{

    public class FoldoutAttribute : PropertyAttribute
    {
    }

    public class SeparatorAttribute : PropertyAttribute
    {
        public SeparatorAttribute()
        {
#if UNITY_EDITOR
            Height = UnityEditor.EditorGUIUtility.singleLineHeight;
#endif
        }

        public SeparatorAttribute(float height)
        {
            Height = height;
        }

        public float Height
        {
            get;
            set;
        }
    }
    
    public class AssetGuidFieldAttribute : PropertyAttribute
    {
        public AssetGuidFieldAttribute(Type objType)
        {
            ObjType = objType;
        }

        public Type ObjType
        {
            get;
            set;
        }
    }

    public class TagFieldAttribute : PropertyAttribute
    {
        public TagFieldAttribute()
        {
        }
    }

    public class LayerFieldAttribute : PropertyAttribute
    {
        public LayerFieldAttribute()
        {
        }
    }

    public class SortingLayerFieldAttribute : PropertyAttribute
    {
        public SortingLayerFieldAttribute()
        {
        }
    }

    public class SceneNameFieldAttribute : PropertyAttribute
    {
        public SceneNameFieldAttribute()
        {
        }
    }

    public class FlexibleTextFieldAttribute : PropertyAttribute
    {
        public FlexibleTextFieldAttribute(int minLines, int maxLines)
        {
            this.MinLines = minLines;
            this.MaxLines = maxLines;
        }

        public int MinLines
        {
            get;
            set;
        }

        public int MaxLines
        {
            get;
            set;
        }
    }

    public class EnumFlagsAttribute : PropertyAttribute
    {
        public EnumFlagsAttribute()
        {
        }
    }
    
}