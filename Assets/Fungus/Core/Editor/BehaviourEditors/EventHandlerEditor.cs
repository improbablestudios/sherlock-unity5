#if UNITY_EDITOR
using UnityEditor;
using System;
using ImprobableStudios.SearchPopupUtilities;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor (typeof(EventHandler), true)]
    public class EventHandlerEditor : FlowchartItemEditor
    {
        private BlockWindow m_blockWindow;

        public BlockWindow BlockWindow
        {
            get
            {
                return m_blockWindow;
            }
            set
            {
                m_blockWindow = value;
            }
        }

        public override Type GetParentWindowType()
        {
            return typeof(BlockWindow);
        }

        public override EditorWindow GetParentWindow()
        {
            return m_blockWindow;
        }

        protected override bool WillShowFlowchartItemSelectionMenu()
        {
            return false;
        }
    }

}

#endif