#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(Label), true)]
    public class LabelEditor : CommandEditor
    {
        static public void LabelField(Rect position, SerializedProperty property, GUIContent labelText, Block block)
        {
            List<string> labelKeys = new List<string>();
            List<Label> labelObjects = new List<Label>();
            
            labelKeys.Add("<None>");
            labelObjects.Add(null);
            
            Label selectedLabel = property.objectReferenceValue as Label;

            int index = 0;
            int selectedIndex = 0;
            if (block != null)
            {
                foreach (Command command in block.Commands)
                {
                    Label label = command as Label;
                    if (label == null)
                    {
                        continue;
                    }

                    labelKeys.Add(label.m_LabelName);
                    labelObjects.Add(label);

                    index++;

                    if (label == selectedLabel)
                    {
                        selectedIndex = index;
                    }
                }
            }

            selectedIndex = EditorGUI.Popup(position, labelText.text, selectedIndex, labelKeys.ToArray());

            property.objectReferenceValue = labelObjects[selectedIndex];
        }

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Label t = target as Label;

            if (property.name == nameof(t.m_LabelName))
            {
                Flowchart flowchart = t.GetFlowchart();
                if (flowchart != null)
                {
                    EditorGUI.BeginChangeCheck();
                    base.DrawProperty(position, property, label);
                    if (EditorGUI.EndChangeCheck())
                    {
                        property.stringValue = t.GetUniqueLabelName(property.stringValue);
                    }
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }
    
}
#endif