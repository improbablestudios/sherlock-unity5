#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(If), true)]
    public class IfEditor : CommandEditor
    {
        protected ConditionReorderableList m_conditionReorderableList;

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            If t = target as If;

            if (property.name == nameof(t.m_Conditions))
            {
                if (m_conditionReorderableList == null)
                {
                    m_conditionReorderableList = new ConditionReorderableList();
                }
                m_conditionReorderableList.Draw(position, property);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            If t = target as If;

            if (property.name == nameof(t.m_Conditions))
            {
                if (m_conditionReorderableList == null)
                {
                    m_conditionReorderableList = new ConditionReorderableList();
                }
                return m_conditionReorderableList.GetHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }
    }

}

#endif