#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(Return), true)]
    public class ReturnEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Return t = target as Return;

            if (property.name == nameof(t.m_Value))
            {
                if (t.m_Value.ValueType != t.ParentBlock.GetReturnType())
                {
                    t.m_Value.SetValueType(t.ParentBlock.GetReturnType());
                    EditorUtility.SetDirty(t);
                }
                base.DrawProperty(position, property, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif