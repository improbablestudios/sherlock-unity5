#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using Fungus.Commands;
using System.Collections.Generic;
using ImprobableStudios.ReorderableListUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(Call), true)]
    public class CallEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Call t = target as Call;

            if (property.name == nameof(t.m_Parameters))
            {
                if (t.m_TargetBlock.IsConstant && t.m_TargetBlock.Value != null)
                {
                    m_reorderableListCache[property].Flags = (ReorderableListFlags.HideCreateButton | ReorderableListFlags.HideDeleteButtons | ReorderableListFlags.ShowIndices);
                    m_reorderableListCache[property].GetElementIndexLabelTextCallback = GetParameterIndexLabelText;
                }
                else
                {
                    m_reorderableListCache[property].Flags = 0;
                }
                base.DrawProperty(position, property, label);
            }
            else if (property.name == nameof(t.m_ReturnVariable))
            {
                if (t.m_TargetBlock.IsConstant && t.m_TargetBlock.Value != null)
                {
                    Type[] valueTypes = new Type[] { t.m_TargetBlock.Value.GetReturnType() };
                    FungusGUI.VariablePopup(position, property, label, null, valueTypes);
                }
                else
                {
                    base.DrawProperty(position, property, label);
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected string GetParameterIndexLabelText(int index)
        {
            Call t = target as Call;

            List<Variable> parameters = t.m_TargetBlock.Value.GetParameters();
            return parameters[index].VariableName;
        }
    }

}

#endif