#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(GetVariableValueByTemplate), true)]
    public class GetVariableValueByTemplateEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            GetVariableValueByTemplate t = target as GetVariableValueByTemplate;

            if (property.name == nameof(t.m_TemplateVariable))
            {
                if (t.m_Flowchart.IsConstant && t.m_Flowchart.Value != null)
                {
                    if (t.m_Flowchart.Value.TemplateFlowchart != null)
                    {
                        VariableEditor.VariableField(position, property, label, t.m_Flowchart.Value.TemplateFlowchart);
                    }
                    else
                    {
                        EditorGUI.HelpBox(position, t.GetPropertyError(property.propertyPath), MessageType.Error);
                    }
                }
                else
                {
                    FungusGUI.VariablePopup(position, property, label, null, null, true);
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif