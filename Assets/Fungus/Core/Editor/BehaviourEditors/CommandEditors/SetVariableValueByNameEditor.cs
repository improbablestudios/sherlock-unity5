#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetVariableValueByName), true)]
    public class SetVariableValueByNameEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SetVariableValueByName t = target as SetVariableValueByName;

            if (property.name == nameof(t.m_Value))
            {
                Variable variable = null;
                if (t.m_Flowchart.IsConstant && t.m_VariableName.IsConstant)
                {
                    if (t.m_Flowchart.Value != null && !string.IsNullOrEmpty(t.m_VariableName.Value))
                    {
                        variable = t.m_Flowchart.Value.GetVariable(t.m_VariableName.Value);
                    }
                }
                if (variable != null)
                {
                    t.m_Value.GenericValue.ValueType = variable.ValueType;
                    t.m_Value.GenericValue.UseDynamicValueType = false;
                }
                else
                {
                    t.m_Value.GenericValue.UseDynamicValueType = true;
                }
                base.DrawProperty(position, property, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif