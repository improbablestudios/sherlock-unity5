#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ForEach), true)]
    public class ForEachEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ForEach t = target as ForEach;

            if (property.name == nameof(t.m_ListVariable))
            {
                FungusGUI.VariablePopup(position, property, label, null, new Type[] { typeof(List<>).MakeGenericType(t.m_IteratorVariable.ValueType) });
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif