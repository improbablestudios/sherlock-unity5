#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(Jump), true)]
    public class JumpEditor : CommandEditor 
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Jump t = target as Jump;

            if (property.name == nameof(t.m_TargetLabel))
            {
                LabelEditor.LabelField(position, property, label, t.ParentBlock);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif