#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetVariableValueByTemplate), true)]
    public class SetVariableValueByTemplateEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SetVariableValueByTemplate t = target as SetVariableValueByTemplate;

            if (property.name == nameof(t.m_Operation))
            {
                FungusGUI.SetOperatorField(position, property, label, t.m_TemplateVariable);
            }
            else if (property.name == nameof(t.m_TemplateVariable))
            {
                if (t.m_Flowchart.IsConstant && t.m_Flowchart.Value != null)
                {
                    if (t.m_Flowchart.Value.TemplateFlowchart != null)
                    {
                        VariableEditor.VariableField(position, property, label, t.m_Flowchart.Value.TemplateFlowchart);
                    }
                    else
                    {
                        EditorGUI.HelpBox(position, t.GetPropertyError(nameof(t.m_Flowchart)), MessageType.Error);
                    }
                }
                else
                {
                    FungusGUI.VariablePopup(position, property, label, null, null, true);
                }
            }
            else if (property.name == nameof(t.m_Value))
            {
                if (t.m_TemplateVariable != null)
                {
                    t.m_Value.GenericValue.ValueType = t.m_TemplateVariable.ValueType;
                    t.m_Value.GenericValue.UseDynamicValueType = false;
                }
                else
                {
                    t.m_Value.GenericValue.UseDynamicValueType = true;
                }
                base.DrawProperty(position, property, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif