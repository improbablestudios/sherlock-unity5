#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(GetBlockByTemplate), true)]
    public class GetBlockByTemplateEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            GetBlockByTemplate t = target as GetBlockByTemplate;

            if (property.name == nameof(t.m_TemplateBlock))
            {
                if (t.m_Flowchart.IsConstant && t.m_Flowchart.Value != null)
                {
                    if (t.m_Flowchart.Value.TemplateFlowchart != null)
                    {
                        FungusGUI.BlockField(position, property, label, null, t.m_Flowchart.Value.TemplateFlowchart);
                    }
                    else
                    {
                        EditorGUI.HelpBox(position, t.GetPropertyError(property.propertyPath), MessageType.Error);
                    }
                }
                else
                {
                    FungusGUI.BlockPopup(position, property, label, null, true);
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif