#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetVariableValue), true)]
    public class SetVariableValueEditor : CommandEditor 
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SetVariableValue t = target as SetVariableValue;

            if (property.name == nameof(t.m_Operation))
            {
                FungusGUI.SetOperatorField(position, property, label, t.m_Variable);
            }
            else if (property.name == nameof(t.m_Value))
            {
                if (t.m_Value.ValueType != t.m_Variable.ValueType)
                {
                    t.m_Value.SetValueType(t.m_Variable.ValueType);
                    EditorUtility.SetDirty(t);
                }
                base.DrawProperty(position, property, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}

#endif