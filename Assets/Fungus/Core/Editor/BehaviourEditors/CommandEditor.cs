#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Fungus.Variables;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.SerializedPropertyUtilities;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor(typeof(Command), true)]
    public class CommandEditor : FlowchartItemEditor
    {
        private CommandWindow m_commandWindow;

        private BlockWindow m_blockWindow;

        protected Dictionary<string, string[]> s_invalidTags;

        public CommandWindow CommandWindow
        {
            get
            {
                return m_commandWindow;
            }
            set
            {
                m_commandWindow = value;
            }
        }

        public BlockWindow BlockWindow
        {
            get
            {
                return m_blockWindow;
            }
            set
            {
                m_blockWindow = value;
            }
        }

        public bool CanEditMultipleObjects
        {
            get
            {
                return base.GetType().GetCustomAttributes(typeof(CanEditMultipleObjects), false).Length > 0;
            }
        }

        public override Type GetParentWindowType()
        {
            return typeof(CommandWindow);
        }

        public override EditorWindow GetParentWindow()
        {
            return m_commandWindow;
        }

        public override void DrawNormalInspector()
        {
            Command t = target as Command;

            bool guiWasEnabled = GUI.enabled;

            ObsoleteAttribute obsoleteAttribute = t.GetType().GetCustomAttributes(typeof(ObsoleteAttribute), true).FirstOrDefault() as ObsoleteAttribute;
            if (obsoleteAttribute != null)
            {
                string message = string.Format("'{0}' is obsolete: ", t.GetType().Name) + obsoleteAttribute.Message;
                if (obsoleteAttribute.IsError)
                {
                    EditorGUILayout.HelpBox(message, MessageType.Error);
                    GUI.enabled = false;
                }
                else
                {
                    EditorGUILayout.HelpBox(message, MessageType.Warning);
                }
            }

            base.DrawNormalInspector();

            serializedObject.Update();

            SerializedProperty returnVariableProp = serializedObject.FindProperty("m_ReturnVariable");
            if (returnVariableProp != null)
            {
                if (t.IsReturnVariablePropertyVisible())
                {
                    OnPropertyGUI(returnVariableProp);
                }
            }

            SerializedProperty waitUntilFinishedProp = serializedObject.FindProperty("m_" + nameof(t.WaitUntilFinished));
            if (t.IsWaitUntilFinishedPropertyVisible())
            {
                OnPropertyGUI(waitUntilFinishedProp);
            }

            SerializedProperty descriptionProp = serializedObject.FindProperty("m_Description");
            if (descriptionProp != null)
            {
                if (t.IsDescriptionPropertyVisible())
                {
                    OnPropertyGUI(descriptionProp);
                }
            }

            DrawPreview();

            serializedObject.ApplyModifiedProperties();

            GUI.enabled = guiWasEnabled;
        }

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.GetPropertyType() == typeof(StringData))
            {
                StringData stringData = property.GetPropertyValue() as StringData;

                Rect propertyPosition = position;
                Rect invalidTagsPosition = position;

                if (s_invalidTags != null && s_invalidTags.ContainsKey(property.propertyPath))
                {
                    string[] invalidTags = s_invalidTags[property.propertyPath];
                    if (invalidTags.Length > 0)
                    {
                        if (s_invalidTags != null && invalidTags.Length > 0)
                        {
                            float invalidTagsHeight = GetInvalidTagsHeight(invalidTags);
                            propertyPosition.yMax -= invalidTagsHeight;
                            invalidTagsPosition.y += propertyPosition.height;
                            invalidTagsPosition.height = invalidTagsHeight;
                        }
                    }
                }

                EditorGUI.BeginChangeCheck();
                base.DrawProperty(propertyPosition, property, label);
                if (EditorGUI.EndChangeCheck() || s_invalidTags == null)
                {
                    if (stringData.IsConstant)
                    {
                        property.serializedObject.ApplyModifiedProperties();
                        property.serializedObject.Update();
                        s_invalidTags = new Dictionary<string, string[]>();
                        s_invalidTags[property.propertyPath] = GetInvalidTokens(stringData.Value);
                    }
                }

                if (s_invalidTags != null && s_invalidTags.ContainsKey(property.propertyPath))
                {
                    string[] invalidTags = s_invalidTags[property.propertyPath];
                    if (invalidTags.Length > 0)
                    {
                        DrawInvalidTags(invalidTagsPosition, invalidTags);
                    }
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (property.GetPropertyType() == typeof(StringData))
            {
                if (s_invalidTags != null && s_invalidTags.ContainsKey(property.propertyPath))
                {
                    string[] invalidTags = s_invalidTags[property.propertyPath];
                    float height = base.GetPropertyHeight(property, label);
                    height += GetInvalidTagsHeight(invalidTags);
                    return height;
                }
            }

            return base.GetPropertyHeight(property, label);
        }

        public virtual bool ShouldDrawCommandArgumentsGUI()
        {
            Command t = target as Command;

            if (serializedObject != null)
            {
                if (GetInspectorMode() == InspectorMode.Normal)
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        if (!t.IsPropertyArgument(property.propertyPath))
                        {
                            continue;
                        }
                        return true;
                    }
                }
                else
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public virtual float GetCommandArgumentsHeight()
        {
            Command t = target as Command;

            float height = 0f;

            if (serializedObject != null)
            {
                if (GetInspectorMode() == InspectorMode.Normal)
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        if (!t.IsPropertyVisible(property.propertyPath) || !t.IsPropertyArgument(property.propertyPath))
                        {
                            continue;
                        }

                        GUIContent label = GetDefaultPropertyLabel(property);

                        height += GetPropertyHeight(property, label);
                    }
                }
                else
                {
                    foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                    {
                        GUIContent label = GetDefaultPropertyLabel(property);

                        height += GetPropertyHeight(property, label);
                    }
                }
            }

            if (height > 0)
            {
                float padding = 5f;
                height += padding * 2;
            }

            return height;
        }

        public virtual void DrawCommandArgumentsGUI(Rect position)
        {
            if (serializedObject != null)
            {
                Command t = target as Command;

                if (position.height > 0)
                {
                    serializedObject.Update();

                    float padding = 5f;

                    GUI.Box(position, GUIContent.none, ReorderableListStyles.ContainerStyle);

                    position.x += padding;
                    position.y += padding;
                    position.width -= padding * 2;
                    float difference = position.xMax - position.xMin - 200f;
                    bool showLabelsOnly = (difference < 0);

                    GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
                    labelStyle.fixedHeight = EditorGUIUtility.singleLineHeight;

                    if (GetInspectorMode() == InspectorMode.Normal)
                    {
                        foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                        {
                            if (!t.IsPropertyVisible(property.propertyPath) || !t.IsPropertyArgument(property.propertyPath))
                            {
                                continue;
                            }

                            GUIContent label = GetDefaultPropertyLabel(property);

                            position.height = GetPropertyHeight(property, label);
                            if (showLabelsOnly)
                            {
                                EditorGUI.LabelField(position, label, labelStyle);
                            }
                            else
                            {
                                DrawProperty(position, property, label);
                            }

                            position.y += position.height;
                        }
                    }
                    else
                    {
                        foreach (SerializedProperty property in m_serializedPropertyCache[serializedObject])
                        {
                            GUIContent label = GetDefaultPropertyLabel(property);

                            position.height = GetPropertyHeight(property, label);
                            if (showLabelsOnly)
                            {
                                EditorGUI.LabelField(position, label, labelStyle);
                            }
                            else
                            {
                                DrawProperty(position, property, label);
                            }

                            position.y += position.height;
                        }
                    }

                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        public string[] GetInvalidTokens(string text)
        {
            Command command = target as Command;
            text = command.GetFlowchart().SubstituteVariables(text);
            string[] invalidTokens = TextTagParser.FindInvalidTokens(text);
            return invalidTokens;
        }

        public string GetInvalidTokensText(string[] invalidTokens)
        {
            string invalidTokensText = string.Join(", ", invalidTokens);
            return "Invalid text tag(s): " + invalidTokensText;
        }

        public void DrawInvalidTags(Rect position, string[] invalidTokens)
        {
            float rightIndent = FungusGUI.VariableDataTypeFieldWidth;
            position.xMax -= rightIndent;

            if (invalidTokens != null && invalidTokens.Length > 0)
            {
                EditorGUI.HelpBox(position, GetInvalidTokensText(invalidTokens), MessageType.Warning);
            }
        }

        public float GetInvalidTagsHeight(string[] invalidTokens)
        {
            if (invalidTokens != null && invalidTokens.Length > 0)
            {
                return EditorStyles.helpBox.CalcSize(new GUIContent(GetInvalidTokensText(invalidTokens))).y;
            }
            return 0;
        }
    }

}

#endif