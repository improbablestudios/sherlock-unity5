#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor(typeof(MatchVariableValue), true)]
    public class MatchVariableValueEditor : PersistentBehaviourEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            MatchVariableValue t = target as MatchVariableValue;

            if (property.name == "m_" + nameof(t.ComponentTypeName))
            {
                Component[] components = t.GetComponents<Component>();
                Type[] baseTypes = components.Where(i => i != null).Select(i => i.GetType()).ToArray();
                SearchPopupGUI.TypeSearchPopupField(position, property, label, null, baseTypes);
            }
            else if (property.name == "m_" + nameof(t.ComponentPropertyName))
            {
                ExtendedEditorGUI.MemberField(position, property, label, t.Component, t.ComponentType, t.Variable.ValueType);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }
    }

}
#endif