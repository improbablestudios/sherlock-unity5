#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.SearchPopupUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using UnityEngine;

namespace Fungus
{
    [CanEditMultipleObjects, CustomEditor(typeof(Flowchart), true)]
    public class FlowchartEditor : PersistentBehaviourEditor
    {
        protected BlockReorderableList m_blockReorderableList;

        protected FlowchartVariableReorderableList m_variableReorderableList;

        protected SerializedPropertyReorderableListCache<FlowchartVariableReorderableList> m_variableReorderableListCache;

        protected Vector2 m_variablesScrollPos;
        
        public delegate void SelectedVariablesChangedDelegate();

        protected event SelectedVariablesChangedDelegate m_onSelectedVariablesChangedCallback = delegate { };

        public bool IsVariablesListExpanded
        {
            get
            {
                return TargetFlowchartVariableReorderableList.Expanded;
            }
            set
            {
                TargetFlowchartVariableReorderableList.Expanded = value;
            }
        }

        public int TargetFlowchartInsertVariableIndex
        {
            get
            {
                Flowchart t = target as Flowchart;

                if (t.Variables.Count == 0)
                {
                    return 0;
                }

                int[] selectedIndices = TargetFlowchartSelectedVariableIndices;
                if (selectedIndices.Length > 0)
                {
                    return selectedIndices[selectedIndices.Length - 1] + 1;
                }

                int index = t.Variables.Count - 1;
                if (index >= 0 && index < t.Variables.Count)
                {
                    return index + 1;
                }

                return 0;
            }
        }

        public int[] TargetFlowchartSelectedVariableIndices
        {
            get
            {
                Flowchart t = target as Flowchart;

                SerializedProperty variablesProp = serializedObject.FindProperty("m_" + nameof(t.Variables));

                SerializedPropertyReorderableList reorderableList = VariableReorderableListCache[variablesProp];
                if (reorderableList != null)
                {
                    return reorderableList.GetSelectedIndices();
                }

                return new int[0];
            }
            set
            {
                Flowchart t = target as Flowchart;

                SerializedProperty variablesProp = serializedObject.FindProperty("m_" + nameof(t.Variables));

                SerializedPropertyReorderableList reorderableList = VariableReorderableListCache[variablesProp];
                if (reorderableList != null)
                {
                    reorderableList.DoSelect(value);
                    reorderableList.DoScrollTowardSelection();
                }
            }
        }

        public SelectedVariablesChangedDelegate OnSelectedVariablesChangedCallback
        {
            get
            {
                return m_onSelectedVariablesChangedCallback;
            }
            set
            {
                m_onSelectedVariablesChangedCallback = value;
            }
        }

        public SerializedPropertyReorderableListCache<FlowchartVariableReorderableList> VariableReorderableListCache
        {
            get
            {
                if (m_variableReorderableListCache == null)
                {
                    m_variableReorderableListCache = GetVariableReorderableListCache();
                }

                return m_variableReorderableListCache;
            }
        }

        public FlowchartVariableReorderableList TargetFlowchartVariableReorderableList
        {
            get
            {
                Flowchart t = target as Flowchart;

                SerializedProperty variablesProp = serializedObject.FindProperty("m_" + nameof(t.Variables));

                if (VariableReorderableListCache != null)
                {
                    FlowchartVariableReorderableList reorderableList = VariableReorderableListCache[variablesProp];

                    reorderableList.ListProperty = variablesProp;

                    if (reorderableList != null)
                    {
                        return reorderableList;
                    }
                }

                return null;
            }
        }

        public FlowchartVariableReorderableList[] VariableReorderableLists
        {
            get
            {
                return VariableReorderableListCache.ReorderableLists;
            }
        }

        public FlowchartVariableReorderableList[] OrderedVariableReorderableLists
        {
            get
            {
                Flowchart targetFlowchart = target as Flowchart;
                List<Flowchart> allFlowchartsInChain = new List<Flowchart>() { targetFlowchart };
                allFlowchartsInChain.AddRange(targetFlowchart.GetTemplateFlowchartChain());
                return VariableReorderableListCache.ReorderableLists
                    .OrderBy(i =>
                    {
                        Flowchart listFlowchart = i.ListProperty.serializedObject.targetObject as Flowchart;
                        return allFlowchartsInChain.IndexOf(listFlowchart);
                    })
                    .ToArray();
            }
        }

        public override void DrawNormalInspector()
        {
            Flowchart t = target as Flowchart;

            base.DrawNormalInspector();

            DrawButtons();
        }
        
        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            Flowchart t = target as Flowchart;

            if (property.name == "m_" + nameof(t.HideComponents))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUI.BeginChangeCheck();

                    base.DrawProperty(property, label);

                    if (EditorGUI.EndChangeCheck())
                    {
                        foreach (Flowchart ts in targets)
                        {
                            ts.UpdateHideFlags();
                        }
                    }

                    if (GUILayout.Button("Rescan Components", EditorStyles.miniButton))
                    {
                        foreach (Flowchart ts in targets)
                        {
                            ts.RescanComponents();
                        }
                    }
                }
            }
            else if (property.name == "m_" + nameof(t.Blocks))
            {
                if (m_blockReorderableList == null)
                {
                    m_blockReorderableList = new BlockReorderableList();
                }
                m_blockReorderableList.Draw(property, label);
            }
            else if (property.name == "m_" + nameof(t.Variables))
            {
                if (m_variableReorderableList == null)
                {
                    m_variableReorderableList = new FlowchartVariableReorderableList();
                }
                m_variableReorderableList.Draw(property, label);
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }

        protected void DrawButtons()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.FlexibleSpace();

                if (GUILayout.Button(new GUIContent("Load Fungus Layout", "Opens the windows necessary to edit flowcharts")))
                {
                    FungusLayout.LoadFungusLayout();
                }

                GUILayout.FlexibleSpace();
            }
        }

        public virtual void DrawVariablesGUI()
        {
            Flowchart t = target as Flowchart;
            
            using (new EditorGUILayout.VerticalScope(GUIStyle.none))
            {
                SerializedProperty variablesProp = new SerializedObject(t).FindProperty("m_" + nameof(t.Variables));
                VariableReorderableList reorderableList = VariableReorderableListCache[variablesProp];
                reorderableList.ListProperty = variablesProp;

                reorderableList.DrawHeader(GUIStyle.none);

                if (reorderableList.Expanded)
                {
                    reorderableList.DrawToolbar(GUIStyle.none);
                    reorderableList.DrawSeparator(GUIStyle.none);

                    using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ScrollViewNoMarginStyle))
                    {
                        using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_variablesScrollPos, GUIStyle.none))
                        {
                            m_variablesScrollPos = scrollView.scrollPosition;
                            
                            DrawFlowchartVariableListProperty(variablesProp);

                            if (t.TemplateFlowchart != null)
                            {
                                DrawFlowchartTemplateVariableList(t.TemplateFlowchart);
                            }
                        }
                    }
                }
            }
        }
        
        public virtual float GetVariablesHeight()
        {
            ReorderableList firstReorderableList = (VariableReorderableListCache.ReorderableLists.Length > 0) ? VariableReorderableListCache.ReorderableLists[0] : null;
            if (firstReorderableList != null)
            {
                float height = firstReorderableList.GetHeaderHeight();
                if (firstReorderableList.Expanded)
                {
                    height += firstReorderableList.GetSeparatorHeight();
                    height += firstReorderableList.GetToolbarHeight();
                    height += firstReorderableList.GetSeparatorHeight();
                    height += firstReorderableList.GetElementsHeight();
                    foreach (VariableReorderableList reorderableList in VariableReorderableListCache.ReorderableLists)
                    {
                        if (reorderableList != firstReorderableList && reorderableList.GetElementCount() > 0 &&  GetPrivateVariables(reorderableList.ListProperty).Length > 0)
                        {
                            height += firstReorderableList.GetSeparatorHeight();
                            height += reorderableList.GetElementsHeight();
                        }
                    }
                }
                return height;
            }
            return 0f;
        }

        protected Variable[] GetPrivateVariables(SerializedProperty variableListProperty)
        {
            List<Variable> privateVariables = new List<Variable>();
            for (int i = 0; i < variableListProperty.arraySize; i++)
            {
                Variable variable = variableListProperty.GetArrayElementAtIndex(i).objectReferenceValue as Variable;
                if (variable.Scope == Scope.Private)
                {
                    privateVariables.Add(variable);
                }
            }
            return privateVariables.ToArray();
        }

        protected SerializedPropertyReorderableListCache<FlowchartVariableReorderableList> GetVariableReorderableListCache()
        {
            return new SerializedPropertyReorderableListCache<FlowchartVariableReorderableList>();
        }
        
        protected void DrawFlowchartTemplateVariableList(Flowchart flowchart)
        {
            if (flowchart.Variables.Count > 0)
            {
                DrawFlowchartTemplatePrivateVariableList(flowchart);
            }

            if (flowchart.TemplateFlowchart != null)
            {
                DrawFlowchartTemplateVariableList(flowchart.TemplateFlowchart);
            }
        }

        protected void DrawFlowchartTemplatePrivateVariableList(Flowchart flowchart)
        {
            SerializedObject serializedFlowchart = new SerializedObject(flowchart);

            SerializedProperty privateVariablesProp = serializedFlowchart.FindProperty("m_" + nameof(flowchart.Variables));

            Variable[] privateVariables = GetPrivateVariables(privateVariablesProp);

            privateVariablesProp.ClearArray();
            privateVariablesProp.arraySize = privateVariables.Length;
            for (int i = 0; i < privateVariablesProp.arraySize; i++)
            {
                privateVariablesProp.GetArrayElementAtIndex(i).objectReferenceValue = privateVariables[i];
            }

            if (privateVariablesProp.arraySize > 0)
            {
                DrawFlowchartVariableListProperty(privateVariablesProp);
                VariableReorderableList reorderableList = VariableReorderableListCache[privateVariablesProp];
                reorderableList.Flags = reorderableList.Flags | ReorderableListFlags.HideCreateButton | ReorderableListFlags.DisableCreatingElements;
            }
        }

        protected virtual void DrawFlowchartVariableListProperty(SerializedProperty property)
        {
            VariableReorderableList reorderableList = VariableReorderableListCache[property] as VariableReorderableList;
            reorderableList.ListProperty = property;

            if (property.serializedObject.targetObject != serializedObject.targetObject)
            {
                reorderableList.DrawSeparatorContent(GUIStyle.none);
            }
            reorderableList.DrawElementsContent(GUIStyle.none);
        }
        
        public void InvokeSelectedVariablesChanged()
        {
            m_onSelectedVariablesChangedCallback.Invoke();
        }
    }

}
#endif
