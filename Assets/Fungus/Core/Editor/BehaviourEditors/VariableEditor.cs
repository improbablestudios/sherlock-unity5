#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ImprobableStudios.Localization;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{
    [CanEditMultipleObjects, CustomEditor(typeof(Variable), true)]
    public class VariableEditor : FlowchartItemEditor
    {
        private FlowchartWindow m_flowchartWindow;

        public FlowchartWindow FlowchartWindow
        {
            get
            {
                return m_flowchartWindow;
            }
            set
            {
                m_flowchartWindow = value;
            }
        }

        public override Type GetParentWindowType()
        {
            return typeof(FlowchartWindow);
        }

        public override EditorWindow GetParentWindow()
        {
            return m_flowchartWindow;
        }

        private static bool VariableSupportsTypes(Variable v, Type[] types)
        {
            return (types == null ||
                    Array.Find(types, i =>
                        i.IsAssignableFrom(v.ValueType) ||
                        CanCastBetweenNumericTypes(i, v.ValueType) ||
                        CanCastBetweenListTypes(i, v.ValueType)
                    ) != null);
        }

        private static bool CanCastBetweenNumericTypes(Type typeA, Type typeB)
        {
            return
                (typeA.IsNumeric() && typeB.IsNumeric());
        }

        private static bool CanCastBetweenListTypes(Type typeA, Type typeB)
        {
            return
                (IsListType(typeA) && IsListType(typeB)) &&
                (GetGenericArgumentType(typeA).IsAssignableFrom(GetGenericArgumentType(typeB)));
        }

        private static bool IsListType(Type type)
        {
            return typeof(IList).IsAssignableFrom(type) && type.GetBaseGenericType() != null;
        }

        private static Type GetGenericArgumentType(Type type)
        {
            return type.GetBaseGenericType().GetGenericArguments()[0];
        }

        public static void VariableField(SerializedProperty property, Flowchart flowchart, params GUILayoutOption[] options)
        {
            VariableField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), flowchart);
        }

        public static void VariableField(SerializedProperty property, GUIContent label, Flowchart flowchart, params GUILayoutOption[] options)
        {
            VariableField(EditorGUILayout.GetControlRect(options), property, label, flowchart);
        }

        public static void VariableField(Rect position, SerializedProperty property, Flowchart flowchart)
        {
            VariableField(position, property, new GUIContent(property.displayName, property.tooltip), flowchart);
        }

        public static void VariableField(Rect position, SerializedProperty property, GUIContent label, Flowchart flowchart)
        {
            List<Variable> variableObjects = flowchart.Variables;
            GUIContent[] variableLabels = variableObjects.Select(i => new GUIContent(i.GetName())).ToArray();
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            Variable selectedVariable = property.objectReferenceValue as Variable;
            int selectedIndex = variableObjects.IndexOf(selectedVariable);
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, variableLabels.ToArray());
            if (EditorGUI.EndChangeCheck())
            {
                selectedVariable = variableObjects[selectedIndex];
                property.objectReferenceValue = selectedVariable;
            }
            EditorGUI.EndProperty();
        }

        public static void GenericValueField(Rect position, SerializedProperty property, GUIContent label, ExtendedEditorGUI.GenericDrawerDelegate valuePropertyDrawer)
        {
            EditorGUI.BeginProperty(position, label, property);

            GenericValue t = property.GetPropertyValue() as GenericValue;

            SerializedProperty valueTypeNameProp = property.FindPropertyRelative("m_" + nameof(t.ValueTypeName));
            SerializedProperty useDynamicValueTypeProp = property.FindPropertyRelative("m_" + nameof(t.UseDynamicValueType));
            SerializedProperty serializedValueProp = property.FindPropertyRelative("m_" + nameof(t.SerializedValue));
            SerializedProperty objectReferenceValueProp = property.FindPropertyRelative("m_" + nameof(t.ObjectReferenceValue));
            SerializedProperty objectReferenceListValueProp = property.FindPropertyRelative("m_" + nameof(t.ObjectReferenceListValue));

            Rect contentPosition = new Rect();

            if (useDynamicValueTypeProp.boolValue)
            {
                if (string.IsNullOrEmpty(valueTypeNameProp.stringValue))
                {
                    label.text = label.text + " (Type)";
                    contentPosition = EditorGUI.PrefixLabel(position, label);
                    EditorGUI.BeginChangeCheck();
                    FungusGUI.VariableValueTypeNameField(contentPosition, valueTypeNameProp, GUIContent.none);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Type selectedType = Type.GetType(valueTypeNameProp.stringValue, false);
                        serializedValueProp.stringValue = GenericValue.GetDefaultSerializedValue(selectedType);
                        objectReferenceValueProp.objectReferenceValue = GenericValue.GetDefaultObjectReferenceValue();
                        objectReferenceListValueProp.arraySize = 0;
                    }
                }
                else
                {
                    contentPosition = EditorGUI.PrefixLabel(position, label);
                    Rect[] rects = ExtendedEditorGUI.GetEqualWidthRects(contentPosition, 2);
                    contentPosition = rects[0];
                    EditorGUI.BeginChangeCheck();
                    FungusGUI.VariableValueTypeNameField(contentPosition, valueTypeNameProp, GUIContent.none);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Type selectedType = Type.GetType(valueTypeNameProp.stringValue, false);
                        serializedValueProp.stringValue = GenericValue.GetDefaultSerializedValue(selectedType);
                        objectReferenceValueProp.objectReferenceValue = GenericValue.GetDefaultObjectReferenceValue();
                        objectReferenceListValueProp.arraySize = 0;
                    }
                    contentPosition = rects[1];
                    if (typeof(Component).IsAssignableFrom(t.ValueType) || typeof(GameObject).IsAssignableFrom(t.ValueType))
                    {
                        t.Value = SearchPopupGUI.ObjectSearchPopupField(contentPosition, t.Value as UnityEngine.Object, GUIContent.none, null, t.ValueType, property.serializedObject.targetObject, true, 0);
                    }
                    else
                    {
                        t.Value = valuePropertyDrawer(contentPosition, t.Value, t.ValueType, GUIContent.none);
                    }
                }
            }
            else
            {
                contentPosition = EditorGUI.PrefixLabel(position, label);
                if (string.IsNullOrEmpty(valueTypeNameProp.stringValue))
                {
                    EditorGUI.HelpBox(contentPosition, "Type not set", MessageType.Warning);
                    return;
                }
                else
                {
                    if (typeof(Component).IsAssignableFrom(t.ValueType) || typeof(GameObject).IsAssignableFrom(t.ValueType))
                    {
                        t.Value = SearchPopupGUI.ObjectSearchPopupField(contentPosition, t.Value as UnityEngine.Object, GUIContent.none, null, t.ValueType, property.serializedObject.targetObject, true, 0);
                    }
                    else
                    {
                        t.Value = valuePropertyDrawer(contentPosition, t.Value, t.ValueType, GUIContent.none);
                    }
                }
            }
            EditorGUI.EndProperty();
        }
        
        public static void VariableDataField<T>(SerializedProperty property, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), new Type[] { typeof(T) }, null, null, allowLocalization);
        }

        public static void VariableDataField<T>(SerializedProperty property, GUIContent label, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, label, new Type[] { typeof(T) }, null, null, allowLocalization);
        }

        public static void VariableDataField<T>(SerializedProperty property, ExtendedEditorGUI.PropertyDrawerDelegate valueDrawer, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), new Type[] { typeof(T) }, valueDrawer, null, allowLocalization);
        }

        public static void VariableDataField<T>(SerializedProperty property, GUIContent label, ExtendedEditorGUI.PropertyDrawerDelegate valueDrawer, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, label, new Type[] { typeof(T) }, valueDrawer, null, allowLocalization);
        }

        public static void VariableDataField(SerializedProperty property, Type type, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), new Type[] { type }, null, null, allowLocalization);
        }

        public static void VariableDataField(SerializedProperty property, GUIContent label, Type type, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, label, new Type[] { type }, null, null, allowLocalization);
        }

        public static void VariableDataField(SerializedProperty property, Type[] types, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, new GUIContent(property.displayName, property.tooltip), types, null, null, allowLocalization);
        }

        public static void VariableDataField(SerializedProperty property, GUIContent label, Type[] types, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(EditorGUILayout.GetControlRect(options), property, label, types, null, null, allowLocalization);
        }

        public static void VariableDataField<T>(Rect position, SerializedProperty property, bool allowLocalization = false)
        {
            VariableDataField(position, property, new GUIContent(property.displayName, property.tooltip), new Type[] { typeof(T) }, null, null, allowLocalization);
        }


        public static void VariableDataField<T>(Rect position, SerializedProperty property, GUIContent label, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, label, new Type[] { typeof(T) }, null, null, allowLocalization);
        }

        public static void VariableDataField<T>(Rect position, SerializedProperty property, ExtendedEditorGUI.PropertyDrawerDelegate valueDrawer, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, new GUIContent(property.displayName, property.tooltip), new Type[] { typeof(T) }, valueDrawer, null, allowLocalization);
        }

        public static void VariableDataField<T>(Rect position, SerializedProperty property, GUIContent label, ExtendedEditorGUI.PropertyDrawerDelegate valueDrawer, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, label, new Type[] { typeof(T) }, valueDrawer, null, allowLocalization);
        }

        public static void VariableDataField(Rect position, SerializedProperty property, Type type, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, new GUIContent(property.displayName, property.tooltip), new Type[] { type }, null, null, allowLocalization);
        }

        public static void VariableDataField(Rect position, SerializedProperty property, GUIContent label, Type type, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, label, new Type[] { type }, null, null, allowLocalization);
        }

        public static void VariableDataField(Rect position, SerializedProperty property, Type[] types, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, new GUIContent(property.displayName, property.tooltip), types, null, null, allowLocalization);
        }

        public static void VariableDataField(Rect position, SerializedProperty property, GUIContent label, Type[] types, bool allowLocalization = false, params GUILayoutOption[] options)
        {
            VariableDataField(position, property, label, types, null, null, allowLocalization);
        }

        public static void VariableDataField(Rect position, SerializedProperty property, GUIContent label, Type[] types, ExtendedEditorGUI.PropertyDrawerDelegate valueDrawer = null, Action valueAction = null, bool allowLocalization = false)
        {
            GUIStyle popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
            popupStyle.imagePosition = ImagePosition.ImageOnly;

            EditorGUI.BeginProperty(position, label, property);

            FlowchartItem flowchartItem = property.serializedObject.targetObject as FlowchartItem;

            Flowchart flowchart = null;
            if (flowchartItem != null)
            {
                flowchart = flowchartItem.GetFlowchart();
            }

            VariableData t = property.GetPropertyValue() as VariableData;
            
            SerializedProperty variableDataTypeProp = property.FindPropertyRelative("m_" + nameof(t.VariableDataType));
            SerializedProperty variableRefProp = property.FindPropertyRelative("m_" + nameof(t.Variable));
            SerializedProperty commandRefProp = property.FindPropertyRelative("m_" + nameof(t.Command));
            SerializedProperty dataValProp = property.FindPropertyRelative(VariableData.GetDataValPropertyName());
            SerializedProperty localizeProp = property.FindPropertyRelative("m_" + nameof(t.Localize));
            
            Command command = property.serializedObject.targetObject as Command;
            EventHandler eventHandler = property.serializedObject.targetObject as EventHandler;
            if (command == null && eventHandler == null && flowchart == null)
            {
                return;
            }

            GUIContent[] popupDisplayOptions = null;
            int selectedIndex = 0;

            if (dataValProp != null)
            {
                popupDisplayOptions = ExtendedEditorGUI.GetGUIContents(new string[] { "Use Constant Value", "Use Variable Value", "Use Command Value" }, "Populate this property with a constant value, a variable's value, or a command's return value");
                selectedIndex = variableDataTypeProp.enumValueIndex;
            }
            else
            {
                popupDisplayOptions = ExtendedEditorGUI.GetGUIContents(new string[] { "Use Variable Value", "Use Command Value" }, "Populate this property with a variable's value, or a command's return value");
                selectedIndex = variableDataTypeProp.enumValueIndex - 1;
                if (selectedIndex < 0)
                {
                    selectedIndex = 0;
                    variableDataTypeProp.enumValueIndex = 1;
                }
            }

            Rect pos = position;

            float variableDataTypePopupWidth = FungusGUI.VariableDataTypeFieldWidth;
            float variableDataTypePopupMargin = 2f;

            float fieldWidth = position.width - variableDataTypePopupWidth;

            pos.width = fieldWidth;
            
            VariableData variableData = property.GetPropertyValue() as VariableData;
            if (variableData != null)
            {
                if (types != null)
                {
                    variableData.SupportedValueTypes = types;
                }
            }

            switch (variableDataTypeProp.enumValueIndex)
            {
                case 0:
                    if (dataValProp != null)
                    {
                        float localizeButtonWidth = 0;
                        if (allowLocalization && LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(variableData.ValueType))
                        {
                            localizeButtonWidth = 18f;
                        }

                        pos.width = fieldWidth - localizeButtonWidth;

                        if (valueAction != null)
                        {
                            valueAction();
                        }
                        else if (valueDrawer != null)
                        {
                            if (string.IsNullOrEmpty(label.tooltip))
                            {
                                label.tooltip = property.GetPropertyTooltip();
                            }
                            valueDrawer(pos, dataValProp, label);
                        }
                        else
                        {
                            EditorGUI.PropertyField(pos, dataValProp, label, true);
                        }

                        pos.x += pos.width;
                        pos.width = localizeButtonWidth;

                        if (allowLocalization && LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(variableData.ValueType))
                        {
                            FungusGUI.LocalizeToggle(pos, localizeProp);
                        }
                    }
                    break;
                case 1:
                    string variableLabelText = label.text;
                    if (!string.IsNullOrEmpty(variableLabelText))
                    {
                        variableLabelText += " (Variable)";
                    }
                    label = new GUIContent(variableLabelText, label.image, label.tooltip);
                    FungusGUI.VariablePopup(pos, variableRefProp, label, null, types);
                    break;
                case 2:
                    string commandLabelText = label.text;
                    if (!string.IsNullOrEmpty(commandLabelText))
                    {
                        commandLabelText += " (Command)";
                    }
                    label = new GUIContent(commandLabelText, label.image, label.tooltip);
                    
                    EditorGUI.PropertyField(pos, commandRefProp, label, true);
                    break;
            }

            pos.x += pos.width;
            pos.width = variableDataTypePopupWidth - variableDataTypePopupMargin;
            pos.y += (pos.height / 2f) - (popupStyle.normal.background.height / 2f);

            EditorGUI.BeginChangeCheck();
            
            selectedIndex = EditorGUI.Popup(pos, selectedIndex, popupDisplayOptions, popupStyle);
            if (EditorGUI.EndChangeCheck())
            {
                if (dataValProp != null)
                {
                    variableDataTypeProp.enumValueIndex = selectedIndex;
                }
                else
                {
                    variableDataTypeProp.enumValueIndex = selectedIndex + 1;
                }

                Command commandRef = commandRefProp.objectReferenceValue as Command;
                switch (variableDataTypeProp.enumValueIndex)
                {
                    case 0:
                        variableRefProp.objectReferenceValue = null;
                        if (commandRef != null)
                        {
                            commandRef.DestroyReferences(true);
                            Undo.DestroyObjectImmediate(commandRefProp.objectReferenceValue);
                        }
                        commandRefProp.objectReferenceValue = null;
                        break;
                    case 1:
                        if (dataValProp != null)
                        {
                            dataValProp.ResetPropertyValue();
                        }
                        if (commandRef != null)
                        {
                            commandRef.DestroyReferences(true);
                            Undo.DestroyObjectImmediate(commandRefProp.objectReferenceValue);
                        }
                        commandRefProp.objectReferenceValue = null;
                        break;
                    case 2:
                        if (dataValProp != null)
                        {
                            dataValProp.ResetPropertyValue();
                        }
                        variableRefProp.objectReferenceValue = null;
                        break;
                }
            }

            EditorGUI.EndProperty();
        }
    }
}

#endif