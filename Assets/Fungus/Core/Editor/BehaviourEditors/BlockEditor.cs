#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor(typeof(Block), true)]
    public class BlockEditor : FlowchartItemEditor
    {
        protected class SetEventHandlerOperation
        {
            public Block m_Block;
            public Type m_EventHandlerType;
        }

        public class AddCommandOperation
        {
            public Block m_Block;
            public Type m_CommandType;
            public int m_Index;
        }

        protected BlockWindow m_blockWindow;

        protected EventHandler m_targetEventHandler;

        protected EventHandlerEditor m_eventHandlerEditor;

        protected Vector2 m_headerScrollPosition;

        protected Vector2 m_commandScrollPosition;
        
        protected static string m_commandSearchString = "";
        
        protected bool m_isEventHandlerExpanded;

        protected bool m_isInputOutputExpanded;
        
        protected BlockParameterReorderableList m_parentParameterReorderableList;

        protected BlockParameterReorderableList m_childParameterReorderableList;

        protected SerializedPropertyReorderableListCache<CommandReorderableList> m_commandReorderableListCache;

        protected static Command s_lastExecutedCommand;

        public delegate void SelectedParametersChangedDelegate();

        public delegate void SelectedCommandsChangedDelegate();

        protected event SelectedParametersChangedDelegate m_onSelectedParametersChangedCallback = delegate { };

        protected event SelectedCommandsChangedDelegate m_onSelectedCommandsChangedCallback = delegate { };

        public BlockWindow BlockWindow
        {
            get
            {
                return m_blockWindow;
            }
            set
            {
                m_blockWindow = value;
            }
        }

        public EventHandler TargetEventHandler
        {
            get
            {
                UpdateTargetEventHandler();

                return m_targetEventHandler;
            }
        }

        public EventHandlerEditor EventHandlerEditor
        {
            get
            {
                if (TargetEventHandler != null)
                {
                    if (m_eventHandlerEditor != null &&
                       (m_eventHandlerEditor.target == null || m_eventHandlerEditor.target != TargetEventHandler))
                    {
                        GUI.FocusControl("");
                        DestroyImmediate(m_eventHandlerEditor);
                        m_eventHandlerEditor = null;
                    }
                    if (m_eventHandlerEditor == null)
                    {
                        m_eventHandlerEditor = Editor.CreateEditor(TargetEventHandler) as EventHandlerEditor;
                        m_eventHandlerEditor.BlockWindow = m_blockWindow;
                    }
                    return m_eventHandlerEditor as EventHandlerEditor;
                }
                return null;
            }
        }

        public BlockParameterReorderableList TargetBlockParameterReorderableList
        {
            get
            {
                Block t = target as Block;
                SerializedProperty parametersProp = serializedObject.FindProperty("m_" + nameof(t.Parameters));
                if (m_parentParameterReorderableList == null)
                {
                    m_parentParameterReorderableList = new BlockParameterReorderableList();
                    m_parentParameterReorderableList.Init(parametersProp);
                }
                if (m_parentParameterReorderableList != null)
                {
                    return m_parentParameterReorderableList;
                }
                return null;
            }
        }

        public CommandReorderableList[] CommandReorderableLists
        {
            get
            {
                return CommandReorderableListCache.ReorderableLists;
            }
        }

        public CommandReorderableList[] OrderedCommandReorderableLists
        {
            get
            {
                Block targetBlock = target as Block;
                List<Block> allBlocksInChain = new List<Block>() { targetBlock };
                allBlocksInChain.AddRange(targetBlock.GetTemplateBlockChain());
                return CommandReorderableListCache.ReorderableLists
                    .OrderBy(i =>
                    {
                        Block listBlock = i.ListProperty.serializedObject.targetObject as Block;
                        return allBlocksInChain.IndexOf(listBlock);
                    })
                    .ToArray();
            }
        }

        public SerializedPropertyReorderableListCache<CommandReorderableList> CommandReorderableListCache
        {
            get
            {
                Block t = target as Block;
                
                if (m_commandReorderableListCache == null)
                {
                    m_commandReorderableListCache = GetCommandReorderableListCache();
                    
                    if (t.LastExecutedCommand != null)
                    {
                        int index = t.Commands.IndexOf(t.LastExecutedCommand);
                        if (index >= 0)
                        {
                            SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
                            SerializedPropertyReorderableList reorderableList = m_commandReorderableListCache[commandsProp];
                            if (reorderableList != null)
                            {
                                reorderableList.DoSelect(index);
                                reorderableList.DoScrollTowardIndex(index);
                            }
                        }
                    }
                }

                return m_commandReorderableListCache;
            }
        }

        public CommandReorderableList TargetBlockCommandReorderableList
        {
            get
            {
                Block t = target as Block;

                SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
                
                if (CommandReorderableListCache != null)
                {
                    CommandReorderableList reorderableList = CommandReorderableListCache[commandsProp];

                    reorderableList.ListProperty = commandsProp;

                    if (reorderableList != null)
                    {
                        return reorderableList;
                    }
                }
                return null;
            }
        }

        public int[] TargetBlockSelectedCommandIndices
        {
            get
            {
                Block t = target as Block;
                SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
                if (CommandReorderableListCache != null)
                {
                    SerializedPropertyReorderableList reorderableList = CommandReorderableListCache[commandsProp];
                    if (reorderableList != null)
                    {
                        return reorderableList.GetSelectedIndices();
                    }
                }
                return new int[0];
            }
            set
            {
                Block t = target as Block;
                SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
                if (CommandReorderableListCache != null)
                {
                    SerializedPropertyReorderableList reorderableList = CommandReorderableListCache[commandsProp];
                    if (reorderableList != null)
                    {
                        reorderableList.DoSelect(value);
                        reorderableList.DoScrollTowardSelection();
                    }
                }
            }
        }
        
        public Command SelectedCommand
        {
            get
            {
                foreach (CommandReorderableList commandReorderableList in CommandReorderableLists)
                {
                    foreach (Command selectedCommand in commandReorderableList.GetSelectedElements())
                    {
                        if (selectedCommand != null)
                        {
                            return selectedCommand;
                        }
                    }
                }
                return null;
            }
            set
            {
                Block t = target as Block;
                
                SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
                
                CommandReorderableList commandReorderableList = CommandReorderableListCache[commandsProp];

                Command targetCommand = value;
                
                for (int i = 0; i < commandReorderableList.GetElementCount(); i++)
                {
                    Command command = commandReorderableList.GetElementValue(i) as Command;
                    if (command == targetCommand || command.GetReturnValueCommandReferences().Contains(targetCommand))
                    {
                        commandReorderableList.Expanded = true;
                        commandReorderableList.DoSelect(i);
                        commandReorderableList.DoScrollTowardIndex(i);
                    }
                }
            }
        }
        
        public Command[] SelectedCommands
        {
            get
            {
                return GetSelectedCommands();
            }
        }

        public Vector2 HeaderScrollPosition
        {
            get
            {
                return m_headerScrollPosition;
            }
            set
            {
                m_headerScrollPosition = value;
            }
        }

        public Vector2 CommandScrollPosition
        {
            get
            {
                return m_commandScrollPosition;
            }
            set
            {
                m_commandScrollPosition = value;
            }
        }
        
        public bool IsInputOutputExpanded
        {
            get
            {
                return m_isInputOutputExpanded;
            }
            set
            {
                m_isInputOutputExpanded = value;
            }
        }

        public bool IsEventHandlerExpanded
        {
            get
            {
                return m_isEventHandlerExpanded;
            }
            set
            {
                m_isEventHandlerExpanded = value;
            }
        }

        public bool IsCommandsExpanded
        {
            get
            {
                return TargetBlockCommandReorderableList.Expanded;
            }
            set
            {
                TargetBlockCommandReorderableList.Expanded = value;
            }
        }

        public SelectedParametersChangedDelegate OnSelectedParametersChangedCallback
        {
            get
            {
                return m_onSelectedParametersChangedCallback;
            }
            set
            {
                m_onSelectedParametersChangedCallback = value;
            }
        }

        public SelectedCommandsChangedDelegate OnSelectedCommandsChangedCallback
        {
            get
            {
                return m_onSelectedCommandsChangedCallback;
            }
            set
            {
                m_onSelectedCommandsChangedCallback = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            FlowchartItem.OnExecuteAnyCallback += OnExecuteFlowchartItem;
        }

        protected virtual void OnDisable()
        {
            FlowchartItem.OnExecuteAnyCallback -= OnExecuteFlowchartItem;
        }

        public override Type GetParentWindowType()
        {
            return typeof(BlockWindow);
        }

        public override EditorWindow GetParentWindow()
        {
            return m_blockWindow;
        }

        protected virtual void OnExecuteFlowchartItem(FlowchartItem flowchartItem)
        {
            Command command = flowchartItem as Command;
            if (command != null)
            {
                if (command != s_lastExecutedCommand)
                {
                    Block t = target as Block;

                    if (t != null && t.Commands != null)
                    {
                        int index = t.Commands.IndexOf(command);
                        if (index >= 0)
                        {
                            TargetBlockSelectedCommandIndices = new int[] { index };
                        }
                    }
                }
                s_lastExecutedCommand = command;
            }
        }

        protected void UpdateTargetEventHandler()
        {
            Block t = target as Block;

            EventHandler eventHandler = null;
            if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
            {
                eventHandler = t.GetRootTemplateBlock().EventHandler;
            }
            else
            {
                eventHandler = t.EventHandler;
            }

            if (m_targetEventHandler != eventHandler)
            {
                m_targetEventHandler = eventHandler;
                m_isEventHandlerExpanded = true;
            }
        }

        protected Command[] GetSelectedCommands()
        {
            List<Command> selectedCommands = new List<Command>();
            foreach (CommandReorderableList commandReorderableList in CommandReorderableLists)
            {
                foreach (Command selectedCommand in commandReorderableList.GetSelectedElements())
                {
                    if (selectedCommand != null)
                    {
                        selectedCommands.Add(selectedCommand);
                    }
                }
            }
            return selectedCommands.ToArray();
        }
        
        public virtual void OnBlockGUI()
        {
            Block t = target as Block;

            serializedObject.Update();

            SerializedProperty commandsProp = serializedObject.FindProperty("m_" + nameof(t.Commands));
            CommandReorderableList reorderableList = CommandReorderableListCache[commandsProp];
            reorderableList.ListProperty = commandsProp;
            
            DrawBlockHeader();

            reorderableList.DrawHeader();

            if (reorderableList.Expanded)
            {
                reorderableList.DrawToolbar();
                reorderableList.DrawSeparator();

                using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ScrollViewStyle))
                {
                    using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_commandScrollPosition, GUIStyle.none))
                    {
                        m_commandScrollPosition = scrollView.scrollPosition;

                        DrawBlockListProperty(commandsProp);

                        if (t.TemplateBlock != null)
                        {
                            DrawBlockTemplateCommandList(t.TemplateBlock);
                        }
                    }
                }

                if (FlowchartWindow.Instance == null || FlowchartWindow.Instance.TargetFlowchart == t.GetFlowchart() || (t.TemplateBlock != null && t.TemplateBlock.Scope != Scope.Private))
                {
                    EditorGUI.BeginChangeCheck();
                    AddCommandButton();
                    if (EditorGUI.EndChangeCheck())
                    {
                        serializedObject.ApplyModifiedProperties();
                        serializedObject.Update();
                        t.InvokePropertyChanged(commandsProp.propertyPath);
                    }
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        public virtual void DrawBlockHeader()
        {
            Block t = target as Block;
            
            t.Parameters.RemoveAll(i => i == null);

            DrawBlockNameGUI();

            DrawDescriptionGUI();

            DrawInputOutputGUI();

            DrawEventHandlerGUI();

            UpdateIndentLevels(t);
        }

        protected virtual void DrawBlockNameGUI()
        {
            Block t = target as Block;

            SerializedProperty blockNameProperty = serializedObject.FindProperty("m_" + nameof(t.BlockName));

            using (new EditorGUILayout.HorizontalScope(GUI.skin.button))
            {
                if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
                {
                    bool guiWasEnabled = GUI.enabled;
                    GUI.enabled = false;
                    EditorGUILayout.TextField(t.GetName());
                    GUI.enabled = guiWasEnabled;
                }
                else
                {
                    EditorGUI.BeginChangeCheck();
                    string name = blockNameProperty.stringValue.ToUpper();
                    name = GUILayout.TextField(name).ToUpper();
                    if (EditorGUI.EndChangeCheck())
                    {
                        blockNameProperty.stringValue = name;

                        string uniqueName = t.GetUniqueBlockName(t.GetFlowchart().Blocks, blockNameProperty.stringValue, Block.DEFAULT_BLOCK_NAME);
                        if (uniqueName != t.GetName())
                        {
                            blockNameProperty.stringValue = uniqueName;
                        }
                    }
                }

                float scopeWidth = 68f;
                SerializedProperty scopeProp = serializedObject.FindProperty("m_" + nameof(t.Scope));
                if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart())
                {
                    bool guiWasEnabled = GUI.enabled;
                    GUI.enabled = false;
                    EditorGUILayout.EnumPopup(GUIContent.none, t.GetScope(), GUILayout.Width(scopeWidth));
                    GUI.enabled = guiWasEnabled;
                }
                else if (t.TemplateBlock != null)
                {
                    FungusGUI.ScopeProperty(EditorGUILayout.GetControlRect(GUILayout.Width(scopeWidth)), scopeProp, GUIContent.none, t.TemplateBlock.Scope);
                }
                else
                {
                    EditorGUILayout.PropertyField(scopeProp, GUIContent.none, true, GUILayout.Width(scopeWidth));
                }

                DrawHeaderHelpAndSettingsGUI();
            }
        }

        protected override bool WillDrawPresetButton()
        {
            return false;
        }

        protected virtual void DrawEventHandlerGUI()
        {
            Block t = target as Block;

            if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                DrawEventHandler(t.GetRootTemplateBlock());
                GUI.enabled = guiWasEnabled;
            }
            else
            {
                DrawEventHandler(t);
            }
        }

        protected virtual void DrawDescriptionGUI()
        {
            Block t = target as Block;

            SerializedProperty descriptionProp = serializedObject.FindProperty("m_" + nameof(t.Description));

            int minLines = 2;
            int maxLines = 5;

            if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                ExtendedEditorGUI.FlexibleTextArea(t.GetDescription(), new GUIContent(descriptionProp.displayName, descriptionProp.tooltip), minLines, maxLines);
                GUI.enabled = guiWasEnabled;
            }
            else
            {
                ExtendedEditorGUI.FlexibleTextArea(descriptionProp, minLines, maxLines);
            }
        }
        
        protected virtual void DrawInputOutputGUI()
        {
            Block t = target as Block;
            
            using (new EditorGUILayout.VerticalScope())
            {
                using (new EditorGUILayout.HorizontalScope(ReorderableListStyles.GetHeaderStyle(m_isInputOutputExpanded)))
                {
                    string inputNumberString = "";
                    if (t.Parameters.Count > 0)
                    {
                        inputNumberString = string.Format(" ({0})", string.Join(",", t.Parameters.Select(i => i.ValueType.GetNiceTypeName()).ToArray()));
                    }
                    string outputNumberString = "";
                    if (t.GetReturnType() != null)
                    {
                        outputNumberString = string.Format(" ({0})", t.GetReturnType().GetNiceTypeName());
                    }
                    string inputOutputLabel = string.Format("Input{0} / Output{1}", inputNumberString, outputNumberString);

                    m_isInputOutputExpanded = EditorGUILayout.Foldout(m_isInputOutputExpanded, new GUIContent(inputOutputLabel, "This block's input parameters and output return value type"), true);
                }
                if (m_isInputOutputExpanded)
                {
                    using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ContainerStyle))
                    {
                        using (new EditorGUILayout.VerticalScope())
                        {
                            SerializedProperty parametersProp = serializedObject.FindProperty("m_" + nameof(t.Parameters));
                            if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
                            {
                                if (m_childParameterReorderableList == null)
                                {
                                    m_childParameterReorderableList = new BlockParameterReorderableList(ReorderableListFlags.HideCreateButton | ReorderableListFlags.HideDeleteButtons);
                                }
                                m_childParameterReorderableList.Draw(parametersProp);
                            }
                            else
                            {
                                if (m_parentParameterReorderableList == null)
                                {
                                    m_parentParameterReorderableList = new BlockParameterReorderableList();
                                }
                                m_parentParameterReorderableList.Draw(parametersProp);
                            }
                        }

                        using (new EditorGUILayout.VerticalScope())
                        {
                            SerializedProperty returnTypeNameProp = serializedObject.FindProperty("m_" + nameof(t.ReturnTypeName));
                            if ((FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != t.GetFlowchart()) || t.TemplateBlock != null)
                            {
                                bool guiWasEnabled = GUI.enabled;
                                GUI.enabled = false;
                                GUIContent returnTypeContent = new GUIContent("<None>");
                                if (t.GetReturnType() != null)
                                {
                                    returnTypeContent = new GUIContent(t.GetReturnType().Name);
                                }
                                EditorGUILayout.Popup(new GUIContent(returnTypeNameProp.displayName, returnTypeNameProp.tooltip), 0, new GUIContent[] { returnTypeContent });
                                GUI.enabled = guiWasEnabled;
                            }
                            else
                            {
                                EditorGUILayout.PropertyField(returnTypeNameProp, true);
                            }
                        }
                    }
                }
            }
        }

        public virtual void DrawBlockTemplateCommandList(Block block)
        {
            if (block.Commands.Count > 0)
            {
                DrawBlockTemplateProtectedCommandList(block);
            }

            if (block.TemplateBlock != null)
            {
                DrawBlockTemplateCommandList(block.TemplateBlock);
            }
        }

        protected SerializedPropertyReorderableListCache<CommandReorderableList> GetCommandReorderableListCache()
        {
            return
                new SerializedPropertyReorderableListCache<CommandReorderableList>(
                    ReorderableListFlags.ShowIndices |
                    ReorderableListFlags.HideHeader |
                    ReorderableListFlags.HideCreateButton |
                    ReorderableListFlags.HideDeleteButtons |
                    ReorderableListFlags.HideDragHandles |
                    ReorderableListFlags.DisableCreatingElements);
        }

        protected virtual void DrawBlockListProperty(SerializedProperty property)
        {
            Block t = property.serializedObject.targetObject as Block;

            t.SetExecutionInfo();

            CommandReorderableList reorderableList = CommandReorderableListCache[property];
            reorderableList.ListProperty = property;
            if (property.serializedObject.targetObject != serializedObject.targetObject)
            {
                reorderableList.DrawSeparatorContent(GUIStyle.none);
            }
            reorderableList.DrawElementsContent(GUIStyle.none);
        }

        protected void DrawBlockTemplateProtectedCommandList(Block block)
        {
            SerializedObject serializedBlock = new SerializedObject(block);

            serializedBlock.Update();

            SerializedProperty commandsProp = serializedBlock.FindProperty("m_" + nameof(Block.Commands));
            
            if (commandsProp.arraySize > 0)
            {
                DrawBlockListProperty(commandsProp);
            }

            serializedBlock.ApplyModifiedProperties();
        }
        
        protected void AddCommandButton(params GUILayoutOption[] options)
        {
            if (this.target != null)
            {
                GUIContent addComponentLabel = new GUIContent("Add Command");
                GUIStyle style = GUI.skin.button;
                Rect position = EditorGUILayout.GetControlRect(false, 24, style, options);

                if (EditorGUI.DropdownButton(position, addComponentLabel, FocusType.Keyboard, style))
                {
                    TargetBlockCommandReorderableList.CreateMenuPosition = position;
                    TargetBlockCommandReorderableList.DoCreate();
                }
            }
        }
        
        protected virtual void DrawEventHandler(Block block)
        {
            SerializedObject serializedBlock = new SerializedObject(block);

            SerializedProperty eventHandlerProp = serializedBlock.FindProperty("m_" + nameof(Block.EventHandler));

            serializedBlock.Update();

            UpdateTargetEventHandler();

            using (new EditorGUILayout.HorizontalScope(ReorderableListStyles.GetHeaderStyle(m_isEventHandlerExpanded)))
            {
                GUIContent label = new GUIContent("Execute On Event");
                Rect totalPosition = EditorGUILayout.GetControlRect();
                Rect contentPosition = ExtendedEditorGUI.GetContentRect(totalPosition, label);
                Rect labelPosition = totalPosition;
                labelPosition.xMax -= contentPosition.width;
                m_isEventHandlerExpanded = EditorGUI.Foldout(labelPosition, m_isEventHandlerExpanded, label, true);

                EditorGUI.BeginChangeCheck();
                FungusGUI.EventHandlerPopup(contentPosition, eventHandlerProp, GUIContent.none, null);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedBlock.ApplyModifiedProperties();
                    serializedBlock.Update();
                    block.InvokePropertyChanged(eventHandlerProp.propertyPath);
                }
            }
            
            serializedBlock.ApplyModifiedProperties();

            if (m_isEventHandlerExpanded)
            {
                using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ContainerStyle))
                {
                    if (block.GetEventHandler() != null)
                    {
                        if (EventHandlerEditor != null)
                        {
                            EventHandlerEditor.OnFlowchartItemGUI();
                        }
                    }
                }
            }
        }
        
        protected virtual void UpdateIndentLevels(Block block)
        {
            int indentLevel = 0;
            foreach (Command command in block.Commands)
            {
                if (command == null)
                {
                    continue;
                }

                if (command.CloseBlock())
                {
                    indentLevel--;
                }

                // Negative indent level is not permitted
                indentLevel = Math.Max(indentLevel, 0);

                command.IndentLevel = indentLevel;

                if (command.OpenBlock())
                {
                    indentLevel++;
                }
            }
        }
        
        public static void ExportCommandInfo(string path)
        {
            List<Type> commandTypes = typeof(Command).FindAllInstantiableDerivedTypes().ToList();
            List<string> commandCategories = new List<string>();
            commandCategories.Add("Core");
            foreach (Type type in commandTypes)
            {
                FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);

                if (info != null)
                {
                    string category = info.GetCategory(type);

                    if (category != "" && !commandCategories.Contains(category))
                    {
                        commandCategories.Add(category);
                    }
                }
            }
            commandCategories.Sort();

            // Output the event handlers in each catagory
            foreach (string category in commandCategories)
            {
                string markdown = "";

                foreach (Type type in commandTypes)
                {
                    FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);

                    if (info != null)
                    {
                        string infoCategory = info.GetCategory(type);
                        string infoName = info.GetName(type);
                        string infoDescription = info.GetDescription(type);

                        if (infoCategory == category || infoCategory == "" && category == "Core")
                        {
                            markdown += "## " + infoName + "\n";
                            markdown += infoDescription + "\n";
                            markdown += GetPropertyInfo(type);
                        }
                    }
                }

                string filePath = path + "/commands/" + category.ToLower() + "_commands.md";

                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                File.WriteAllText(filePath, markdown);
            }
        }

        public static void ExportEventHandlerInfo(string path)
        {
            List<Type> eventHandlerTypes = typeof(EventHandler).FindAllInstantiableDerivedTypes().ToList();
            List<string> eventHandlerCategories = new List<string>();
            eventHandlerCategories.Add("Core");
            foreach (Type type in eventHandlerTypes)
            {
                FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);

                if (info != null)
                {
                    string category = info.GetCategory(type);

                    if (category != "" && !eventHandlerCategories.Contains(category))
                    {
                        eventHandlerCategories.Add(category);
                    }
                }
            }
            eventHandlerCategories.Sort();

            // Output the event handlers in each catagory
            foreach (string category in eventHandlerCategories)
            {
                string markdown = "";

                foreach (Type type in eventHandlerTypes)
                {
                    FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);

                    if (info != null)
                    {
                        string infoCategory = info.GetCategory(type);
                        string infoName = info.GetName(type);
                        string infoDescription = info.GetDescription(type);

                        if (infoCategory == category || infoCategory == "" && category == "Core")
                        {
                            markdown += "## " + infoName + "\n";
                            markdown += infoDescription + "\n";
                            markdown += GetPropertyInfo(type);
                        }
                    }
                }

                string filePath = path + "/event_handlers/" + category.ToLower() + "_events.md";

                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                File.WriteAllText(filePath, markdown);
            }
        }

        protected static string GetPropertyInfo(Type type)
        {
            string markdown = "";
            foreach (FieldInfo field in type.GetFields())
            {
                TooltipAttribute attribute = (TooltipAttribute)Attribute.GetCustomAttribute(field, typeof(TooltipAttribute));
                if (attribute == null)
                {
                    continue;
                }

                // Change field name to how it's displayed in the inspector
                string propertyName = Regex.Replace(field.Name, "(\\B[A-Z])", " $1");
                if (propertyName.Length > 1)
                {
                    propertyName = propertyName.Substring(0, 1).ToUpper() + propertyName.Substring(1);
                }
                else
                {
                    propertyName = propertyName.ToUpper();
                }

                markdown += propertyName + " | " + field.FieldType + " | " + attribute.tooltip + "\n";
            }

            if (markdown.Length > 0)
            {
                markdown = "\nProperty | Type | Description\n --- | --- | ---\n" + markdown + "\n";
            }

            return markdown;
        }

        public void InvokeSelectedCommandsChanged()
        {
            m_onSelectedCommandsChangedCallback.Invoke();
        }
        
        public void InvokeSelectedParametersChanged()
        {
            m_onSelectedParametersChangedCallback.Invoke();
        }
    }

}

#endif