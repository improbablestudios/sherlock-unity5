#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using System;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using UnityEngine;
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.BaseWindowUtilities;
using System.Linq;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor(typeof(FlowchartItem), true)]
    public abstract class FlowchartItemEditor : PersistentBehaviourEditor
    {
        public static GUIStyle CenteredLabelStyle
        {
            get
            {
                GUIStyle centeredLabel = new GUIStyle(EditorStyles.largeLabel);
                centeredLabel.alignment = TextAnchor.MiddleCenter;
                centeredLabel.fontStyle = FontStyle.Bold;
                return centeredLabel;
            }
        }

        protected int m_headerControlId;

        public abstract Type GetParentWindowType();

        public abstract EditorWindow GetParentWindow();

        public override void DrawNormalInspector()
        {
            FlowchartItem t = target as FlowchartItem;

            t.UpdateHideFlags();

            if (GetParentWindow() == null)
            {
                EditorGUILayout.ObjectField(serializedObject.FindProperty("m_Script"));
                if (GUILayout.Button(new GUIContent("Edit In Fungus")))
                {
                    BaseWindow window = BaseWindow.ShowWindow(GetParentWindowType());
                    window.SetTarget(target);
                }
            }
            else
            {
                base.DrawNormalInspector();
            }
        }

        public override RectOffset GetPropertyPadding(SerializedProperty property)
        {
            RectOffset padding = base.GetPropertyPadding(property);
            if (!typeof(VariableData).IsAssignableFrom(property.GetPropertyType()))
            {
                padding.right = (int)FungusGUI.VariableDataTypeFieldWidth;
            }
            return padding;
        }

        public static void DrawPreviewToolbar(SerializedProperty property, UnityEngine.Object[] list, GUIContent nullLabel = null)
        {
            ExtendedEditorGUI.LineSeparator();
            EditorGUILayout.LabelField("Preview " + property.displayName, CenteredLabelStyle);
            EditorGUILayout.Separator();
            SerializedProperty variableValProp = property.FindPropertyRelative(VariableData.GetDataValPropertyName());
            if (variableValProp != null)
            {
                property = variableValProp;
            }
            ExtendedEditorGUI.ObjectCycleToolbar(variableValProp, list, nullLabel);
        }

        public virtual void OnFlowchartItemGUI()
        {
            FlowchartItem t = target as FlowchartItem;
            if (t == null)
            {
                return;
            }

            Flowchart flowchart = t.GetFlowchart();
            if (flowchart == null)
            {
                return;
            }

            bool guiWasEnabled = GUI.enabled;
            if (FlowchartWindow.Instance != null)
            {
                if (!targets.All(x => (x as FlowchartItem).GetFlowchart() == FlowchartWindow.Instance.TargetFlowchart))
                {
                    GUI.enabled = false;
                }
            }

            DrawFlowchartItemHeader();

            if (GetInspectorHeight() > 0f)
            {
                EditorGUILayout.Separator();
            }

            OnInspectorGUI();

            if (GetInspectorHeight() > 0f)
            {
                EditorGUILayout.Separator();
            }

            DrawFlowchartItemDescription();

            GUI.enabled = guiWasEnabled;

            if (WillShowFlowchartItemSelectionMenu())
            {
                EditorGUI.BeginChangeCheck();
                HandleFlowchartItemSelectionMenu(m_headerControlId);
                if (EditorGUI.EndChangeCheck())
                {
                    EditorGUIUtility.ExitGUI();
                }
            }
        }

        public virtual void DrawFlowchartItemHeader()
        {
            FlowchartItem t = target as FlowchartItem;
            if (t == null)
            {
                return;
            }

            Flowchart flowchart = t.GetFlowchart();
            if (flowchart == null)
            {
                return;
            }

            Color backgroundColor = GUI.backgroundColor;
            if (t.enabled)
            {
                GUI.backgroundColor = t.GetHeaderColor();
            }
            else
            {
                GUI.backgroundColor = FungusColorPalette.GetColor("");
            }

            using (EditorGUILayout.HorizontalScope scope = new EditorGUILayout.HorizontalScope(GUI.skin.button))
            {
                GUI.backgroundColor = backgroundColor;

                Rect rect = scope.rect;
                m_headerControlId = GUIUtility.GetControlID(GetHeaderControlIdHash(), FocusType.Keyboard, rect);

                Event current = Event.current;
                if (this != null && current.type == EventType.MouseDown && rect.Contains(current.mousePosition))
                {
                    if (current.button == 0)
                    {
                        if (WillShowFlowchartItemSelectionMenu())
                        {
                            ShowFlowchartItemSelectionMenu(rect, m_headerControlId);
                            current.Use();
                        }
                    }
                    if (current.button == 1)
                    {
                        ShowSettingsContextMenu(new Rect(current.mousePosition.x, current.mousePosition.y, 0.0f, 0.0f));
                        current.Use();
                    }
                }

                bool enabled = t.enabled;
                EditorGUI.BeginChangeCheck();
                enabled = EditorGUILayout.Toggle(enabled, GUILayout.Width(EditorStyles.toggle.CalcSize(GUIContent.none).x));
                if (EditorGUI.EndChangeCheck())
                {
                    foreach (UnityEngine.Object targetObj in targets)
                    {
                        MonoBehaviour ts = targetObj as MonoBehaviour;
                        Undo.RecordObject(ts, "Set Enabled");
                        ts.enabled = enabled;
                    }
                }

                string name = FlowchartItemInfoAttribute.GetInfo(t.GetType()).GetName(t.GetType());
                GUIContent nameContent = new GUIContent(name);
                EditorGUILayout.LabelField(nameContent, GUILayout.Width(EditorStyles.label.CalcSize(nameContent).x));

                GUILayout.FlexibleSpace();

                string[] itemIds = targets.Select(i => (i as FlowchartItem).ItemId.ToString()).ToArray();
                string itemId = string.Join(", ", itemIds);
                GUIContent idContent = new GUIContent("(" + itemId + ")");
                GUILayout.Label(idContent, GUILayout.Width(EditorStyles.label.CalcSize(idContent).x + 2f));
                
                DrawHeaderHelpAndSettingsGUI();
            }
        }

        public virtual void DrawFlowchartItemDescription()
        {
            FlowchartItem t = target as FlowchartItem;
            if (t == null)
            {
                return;
            }

            string description = FlowchartItemInfoAttribute.GetInfo(t.GetType()).GetDescription(t.GetType());

            // Display help text
            if (!string.IsNullOrEmpty(description))
            {
                EditorGUILayout.HelpBox(description, MessageType.Info, true);
            }
        }

        protected virtual int GetHeaderControlIdHash()
        {
            return 0;
        }

        protected override bool WillDrawHelpButton()
        {
            return false;
        }

        protected virtual bool WillShowFlowchartItemSelectionMenu()
        {
            return false;
        }

        protected virtual SerializedProperty GetFlowchartItemSelectionProperty()
        {
            return null;
        }

        protected virtual void ShowFlowchartItemSelectionMenu(Rect position, int controlId)
        {
        }

        protected virtual void HandleFlowchartItemSelectionMenu(int controlId)
        {
        }
    }
}
#endif