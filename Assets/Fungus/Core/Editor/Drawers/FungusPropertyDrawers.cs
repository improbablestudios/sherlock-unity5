#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityEditorUtilities;
using System;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.ReorderableListUtilities;
using System.Collections;
using System.Collections.Generic;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.SerializedPropertyUtilities;
using ImprobableStudios.ColorPaletteUtilities;

namespace Fungus
{

    [CustomPropertyDrawer(typeof(TypeNameDataFieldAttribute))]
    public class TypeNameDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            TypeNameDataFieldAttribute typeNameDataAttribute = attribute as TypeNameDataFieldAttribute;
            TypeNameFieldDrawer.DrawProperty(position, property, label, typeNameDataAttribute.NullLabel, typeNameDataAttribute.BaseTypes);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }
    
    [CustomPropertyDrawer(typeof(TagDataFieldAttribute))]
    public class TagDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            TagFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }

    [CustomPropertyDrawer(typeof(SceneNameDataFieldAttribute))]
    public class SceneNameDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SceneNameFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }
    
    [CustomPropertyDrawer(typeof(LanguageDataFieldAttribute))]
    public class LanguageDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            LanguageFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }

    [CustomPropertyDrawer(typeof(VariableValueTypeNameFieldAttribute))]
    public class VariableValueTypeNameFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            string nameSuffix = " Name";
            if (label.text.EndsWith(nameSuffix, StringComparison.Ordinal))
            {
                label.text = label.text.Substring(0, label.text.Length - nameSuffix.Length);
            }

            VariableValueTypeNameFieldAttribute typeNameAttribute = attribute as VariableValueTypeNameFieldAttribute;
            if (typeNameAttribute.BaseTypes == null || typeNameAttribute.BaseTypes.Length == 0)
            {
                FungusGUI.VariableValueTypeNameField(position, property, label);
            }
            else
            {
                FungusGUI.VariableValueTypeNameField(position, property, label, typeNameAttribute.NullLabel, typeNameAttribute.BaseTypes);
            }
        }
    }

    [CustomPropertyDrawer(typeof(VariableValueTypeNameDataFieldAttribute))]
    public class VariableValueTypeNameDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            string nameSuffix = " Name";
            if (label.text.EndsWith(nameSuffix, StringComparison.Ordinal))
            {
                label.text = label.text.Substring(0, label.text.Length - nameSuffix.Length);
            }

            VariableValueTypeNameDataFieldAttribute typeNameDataAttribute = attribute as VariableValueTypeNameDataFieldAttribute;
            if (typeNameDataAttribute.BaseTypes == null || typeNameDataAttribute.BaseTypes.Length == 0)
            {
                FungusGUI.VariableValueTypeNameField(position, property, label);
            }
            else
            {
                FungusGUI.VariableValueTypeNameField(position, property, label, typeNameDataAttribute.NullLabel, typeNameDataAttribute.BaseTypes);
            }
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }

    [CustomPropertyDrawer(typeof(StringDataAreaAttribute))]
    public class StringDataAreaDrawer : VariableDataPropertyAttributeDrawer
    {
        private Vector2 m_scrollPosition = default(Vector2);

        protected float m_positionWidth = 0f;

        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            if (Event.current.type != EventType.Layout)
            {
                m_positionWidth = position.width;
            }

            ExtendedEditorGUI.ScrollableTextArea(position, property, label, ref m_scrollPosition);

            EditorGUI.EndProperty();
        }

        public override float GetValuePropertyHeight(SerializedProperty property, GUIContent label)
        {
            StringDataAreaAttribute stringDataAreaAttribute = base.attribute as StringDataAreaAttribute;
            return ExtendedEditorGUI.GetScrollableTextAreaHeight(property.stringValue, label, m_positionWidth, stringDataAreaAttribute.MinLines, stringDataAreaAttribute.MaxLines);
        }

        public override Type GetValueType()
        {
            return typeof(string);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }

    [CustomPropertyDrawer(typeof(LayerDataFieldAttribute))]
    public class LayerDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            LayerFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(IntegerData) };
        }
    }

    [CustomPropertyDrawer(typeof(SortingLayerDataFieldAttribute))]
    public class SortingLayerDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SortingLayerFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(IntegerData) };
        }
    }

    [CustomPropertyDrawer(typeof(DataMinAttribute))]
    public class DataMinDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            DataMinAttribute dataMinAttribute = attribute as DataMinAttribute;
            MinDrawer.DrawProperty(position, property, label, dataMinAttribute.Min);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(IntegerData), typeof(FloatData) };
        }
    }

    [CustomPropertyDrawer(typeof(DataRangeAttribute))]
    public class DataRangeDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            DataRangeAttribute dataRangeAttribute = attribute as DataRangeAttribute;
            if (property.propertyType == SerializedPropertyType.Integer)
            {
                EditorGUI.BeginChangeCheck();
                int value = property.intValue;
                value = EditorGUI.IntSlider(position, label, value, (int)dataRangeAttribute.Min, (int)dataRangeAttribute.Max);
                if (EditorGUI.EndChangeCheck())
                {
                    value = Math.Max((int)dataRangeAttribute.Min, value);
                    property.intValue = value;
                }
            }
            else if (property.propertyType == SerializedPropertyType.Float)
            {
                EditorGUI.BeginChangeCheck();
                float value = property.floatValue;
                value = EditorGUI.Slider(position, label, value, dataRangeAttribute.Min, dataRangeAttribute.Max);
                if (EditorGUI.EndChangeCheck())
                {
                    value = Math.Max(dataRangeAttribute.Min, value);
                    property.floatValue = value;
                }
            }
            EditorGUI.EndProperty();
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(IntegerData), typeof(FloatData) };
        }
    }


    [CustomPropertyDrawer(typeof(Block), true)]
    public class BlockDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            FlowchartItem flowchartItem = property.serializedObject.targetObject as FlowchartItem;
            if (flowchartItem == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            else
            {
                Type type = property.GetPropertyFieldType(fieldInfo);
                FungusGUI.BlockPopup(type, position, property, label, null);
            }
        }
    }

    [CustomPropertyDrawer(typeof(Variable), true)]
    public class VariableDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            FlowchartItem flowchartItem = property.serializedObject.targetObject as FlowchartItem;
            if (flowchartItem == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            else
            {
                Type type = property.GetPropertyFieldType(fieldInfo);
                Type[] valueTypes = new Type[] { Variable.GetVariableValueType(type) };
                FungusGUI.VariablePopup(type, position, property, label, null, valueTypes);
            }
        }
    }

    [CustomPropertyDrawer(typeof(SetOperator), true)]
    public class SetOperatorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUIContent[] displayedOptions = new GUIContent[] 
            {
                new GUIContent("="),
                new GUIContent("=!"),
                new GUIContent("+="),
                new GUIContent("-="),
                new GUIContent("*="),
                new GUIContent("/=")
            };
            property.intValue = EditorGUI.Popup(position, label, property.intValue, displayedOptions);
        }
    }

   [CustomPropertyDrawer(typeof(GameObjectWithComponentDataFieldAttribute), true)]
    public class GameObjectDataWithComponentDrawer : VariableDataDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = fieldInfo.FieldType;
            type = VariableData.GetVariableDataValueType(type);

            GameObjectWithComponentDataFieldAttribute componentDataFieldAttribute = attribute as GameObjectWithComponentDataFieldAttribute;
            Type[] types = componentDataFieldAttribute.ComponentTypes;
            GUIContent nullLabel = componentDataFieldAttribute.NullLabel;
            bool allowNone = !componentDataFieldAttribute.DefaultToNonNull;

            SearchPopupGUI.ObjectSearchPopupField(position, property, label, nullLabel, type, types, allowNone);
        }
    }

    [CustomPropertyDrawer(typeof(ComponentDataFieldAttribute), true)]
    public class ComponentDataFieldDrawer : VariableDataDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = fieldInfo.FieldType;
            type = VariableData.GetVariableDataValueType(type);

            ComponentDataFieldAttribute componentDataFieldAttribute = attribute as ComponentDataFieldAttribute;
            Type[] types = componentDataFieldAttribute.ComponentTypes;
            GUIContent nullLabel = componentDataFieldAttribute.NullLabel;
            bool allowNone = !componentDataFieldAttribute.DefaultToNonNull;
            SearchPopupGUI.ObjectSearchPopupField(position, property, label, nullLabel, type, types, allowNone);
        }
    }

    [CustomPropertyDrawer(typeof(ObjectDataPopupAttribute), true)]
    public class ObjectDataPopupDrawer : VariableDataDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ObjectDataPopupAttribute popupDefaultAttribute = attribute as ObjectDataPopupAttribute;
            Type type = fieldInfo.FieldType;
            type = VariableData.GetVariableDataValueType(type);
            GUIContent nullLabel = popupDefaultAttribute.NullLabel;
            bool allowNone = !popupDefaultAttribute.DefaultToNonNull;

            SearchPopupGUI.ObjectSearchPopupField(position, property, label, nullLabel, type, allowNone);
        }
    }
    
    [CustomPropertyDrawer(typeof(ColorSwatchDataFieldAttributeAttribute), true)]
    public class ColorSwatchDataFieldDrawer : VariableDataPropertyAttributeDrawer
    {
        public override void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ColorSwatchFieldDrawer.DrawProperty(position, property, label);
        }

        public override Type[] GetSupportedPropertyTypes()
        {
            return new Type[] { typeof(StringData) };
        }
    }
    
    [CustomPropertyDrawer(typeof(VariableData), true)]
    public class VariableDataDrawer : PropertyDrawer
    {
        private SerializedPropertyReorderableList m_valuePropertyReorderableList;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type type = GetValueType();

            Type[] types = null;
            if (type != null)
            {
                types = new Type[] { type };
            }

            bool allowLocalization = Attribute.IsDefined(fieldInfo, typeof(LocalizeAttribute));

            VariableEditor.VariableDataField(position, property, label, types, DrawValueProperty, null, allowLocalization);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty variableDataTypeProp = property.FindPropertyRelative("m_" + nameof(VariableData.VariableDataType));
            SerializedProperty variableRefProp = property.FindPropertyRelative("m_" + nameof(VariableData.Variable));
            SerializedProperty commandRefProp = property.FindPropertyRelative("m_" + nameof(VariableData.Command));

            SerializedProperty dataValProp = property.FindPropertyRelative(VariableData.GetDataValPropertyName());
            if (variableDataTypeProp != null)
            {
                if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Variable)
                {
                    return EditorGUI.GetPropertyHeight(variableRefProp, label, true);
                }
                if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Command)
                {
                    return EditorGUI.GetPropertyHeight(commandRefProp, label, true);
                }
                if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Value)
                {
                    if (dataValProp != null)
                    {
                        return GetValuePropertyHeight(dataValProp, label);
                    }
                }
            }
            return base.GetPropertyHeight(property, label);
        }

        public virtual void DrawValueProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (m_valuePropertyReorderableList == null)
                {
                    m_valuePropertyReorderableList = new SerializedPropertyReorderableList();
                }
                position.height = m_valuePropertyReorderableList.GetHeight();
                m_valuePropertyReorderableList.Draw(position, property, label);
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        public virtual float GetValuePropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (BaseEditorSettings.UseReorderableLists
                && (property.propertyType == SerializedPropertyType.ArraySize || property.propertyPath.EndsWith("]", StringComparison.Ordinal)))
            {
                return 0f;
            }
            else if (BaseEditorSettings.UseReorderableLists
                && (property.isArray && property.propertyType != SerializedPropertyType.String))
            {
                if (m_valuePropertyReorderableList == null)
                {
                    m_valuePropertyReorderableList = new SerializedPropertyReorderableList();
                }
                return m_valuePropertyReorderableList.GetHeight();
            }
            else
            {
                return EditorGUI.GetPropertyHeight(property, label, true);
            }
        }

        public virtual Type GetValueType()
        {
            return VariableData.GetVariableDataValueType(fieldInfo.FieldType);
        }
    }

    public abstract class VariableDataPropertyAttributeDrawer : VariableDataDrawer
    {
        public override sealed void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (PropertyAttributeDrawer.GuiSupportsSerializedPropertyType(position, property, label, attribute.GetType(), GetSupportedPropertyTypes()))
            {
                base.OnGUI(position, property, label);
            }
        }

        public abstract Type[] GetSupportedPropertyTypes();
    }

    [CustomPropertyDrawer(typeof(GenericValue))]
    public class GenericPropertyDrawerDelegate : PropertyDrawer
    {
        GenericReorderableList m_reorderableList;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            VariableEditor.GenericValueField(position, property, label, (Rect p, object v, Type t, GUIContent l) => DrawValueProperty(p, v, t, l, property.serializedObject.targetObject));
        }

        public object DrawValueProperty(Rect position, object value, Type type, GUIContent label, UnityEngine.Object targetObject = null)
        {
            if (typeof(IList).IsAssignableFrom(type))
            {
                if (m_reorderableList == null)
                {
                    m_reorderableList = new GenericReorderableList((Rect p, UnityEngine.Object o, Type t, bool s) => SearchPopupGUI.ObjectSearchPopupField(p, o as UnityEngine.Object, label, null, t, null, targetObject, true));
                }
                position.height = m_reorderableList.GetHeight();
                m_reorderableList.Draw(position, value as IList, label, targetObject);
                return value;
            }
            else
            {
                if (typeof(Component).IsAssignableFrom(type) || typeof(GameObject).IsAssignableFrom(type))
                {
                    return SearchPopupGUI.ObjectSearchPopupField(position, value as UnityEngine.Object, label, null, type, targetObject, true, 0);
                }
                else
                {
                    return ExtendedEditorGUI.ValueField(position, value, type, label);
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            GenericValue t = property.GetPropertyValue() as GenericValue;

            if (t == null)
            {
                return EditorGUIUtility.singleLineHeight;
            }

            return GetValuePropertyHeight(t.ValueType, label);
        }

        public float GetValuePropertyHeight(Type type, GUIContent label)
        {
            if (type == null)
            {
                return EditorGUIUtility.singleLineHeight;
            }

            if (typeof(IList).IsAssignableFrom(type))
            {
                if (m_reorderableList == null)
                {
                    m_reorderableList = new GenericReorderableList();
                }
                return m_reorderableList.GetHeight();
            }
            else
            {
                return EditorGUI.GetPropertyHeight(SerializedPropertyExtensions.ConvertTypeToSerializedPropertyType(type), label);
            }
        }
    }

    [CustomPropertyDrawer(typeof(GenericVariableData))]
    public class GenericVariableDataDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty genericValueProp = property.FindPropertyRelative(VariableData.GetDataValPropertyName());

            GenericValue t = genericValueProp.GetPropertyValue() as GenericValue;

            Type[] types = null;
            Type type = t.ValueType;
            if (type == typeof(float))
            {
                types = new Type[] { typeof(float), typeof(int) };
            }
            if (type == null)
            {
                types = new Type[] { typeof(object) };
            }
            else
            {
                types = new Type[] { type };
            }

            bool allowLocalization = Attribute.IsDefined(fieldInfo, typeof(LocalizeAttribute));

            VariableEditor.VariableDataField(position, property, label, types, null, null, allowLocalization);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty variableDataTypeProp = property.FindPropertyRelative("m_" + nameof(VariableData.VariableDataType));
            SerializedProperty variableRefProp = property.FindPropertyRelative("m_" + nameof(VariableData.Variable));
            SerializedProperty commandRefProp = property.FindPropertyRelative("m_" + nameof(VariableData.Command));
            SerializedProperty dataValProp = property.FindPropertyRelative(VariableData.GetDataValPropertyName());

            if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Variable)
            {
                return EditorGUI.GetPropertyHeight(variableRefProp, label, true);
            }
            if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Command)
            {
                return EditorGUI.GetPropertyHeight(commandRefProp, label, true);
            }
            if (variableDataTypeProp.enumValueIndex == (int)VariableDataType.Value)
            {
                if (dataValProp != null)
                {
                    return EditorGUI.GetPropertyHeight(dataValProp, label, true);
                }
            }
            return base.GetPropertyHeight(property, label);
        }
    }

    [CustomPropertyDrawer(typeof(VariableOfTypeFieldAttribute))]
    public class VariableOfTypeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            VariableOfTypeFieldAttribute variableProperty = attribute as VariableOfTypeFieldAttribute;
            if (variableProperty == null)
            {
                return;
            }

            Type type = property.GetPropertyFieldType(fieldInfo);

            FungusGUI.VariablePopup(type, position, property, label, variableProperty.NullLabel, variableProperty.VariableValueTypes);
        }
    }

    [CustomPropertyDrawer(typeof(ReturnValueCommand), true)]
    public class ReturnValueCommandDrawer : PropertyDrawer
    {
        protected CommandEditor m_commandEditor;

        protected bool m_foldout;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type[] valueTypes = new Type[] { typeof(object) };
            SerializedProperty parentProperty = property.GetParentProperty();
            if (parentProperty != null)
            {
                VariableData variableData = parentProperty.GetPropertyValue() as VariableData;
                if (variableData != null)
                {
                    valueTypes = variableData.SupportedValueTypes;
                }
            }

            m_foldout = FungusGUI.DrawReturnValueCommand(position, property, label, null, m_foldout, ref m_commandEditor, valueTypes);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = EditorGUIUtility.singleLineHeight;

            if (m_commandEditor != null && m_foldout)
            {
                height += m_commandEditor.GetCommandArgumentsHeight();
            }

            return height;
        }
    }
    
    [CustomPropertyDrawer(typeof(EventHandler.TriggerCondition))]
    public class TriggerConditionPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            List<GUIContent> displayedOptions = new List<GUIContent>();
            foreach (string enumName in ExtendedEditorGUI.GetDisplayNames(Enum.GetNames(typeof(EventHandler.TriggerCondition))))
            {
                displayedOptions.Add(new GUIContent(enumName + " " + label.text));
            }
            label = new GUIContent("When", label.image, label.tooltip);
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            int value = EditorGUI.Popup(position, label, property.intValue, displayedOptions.ToArray());
            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = value;
            }
            EditorGUI.EndProperty();
        }
    }

}

#endif