#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.ReorderableListUtilities;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Fungus
{
    public class BlockReorderableList : SerializedPropertyReorderableList
    {
        private readonly Color m_errorColor = new Color(1, 0.8f, 0.8f, 1);

        public BlockReorderableList() : base()
        {
        }

        public BlockReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public BlockReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public BlockReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public BlockReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override object Create()
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            return flowchart.AddBlock();
        }

        protected override void Insert(int index)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            flowchart.AddBlock(index);
        }

        protected override void Delete(int[] indices)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                Block block = this[index].objectReferenceValue as Block;
                if (block != null)
                {
                    Block.RemoveBlockFromList(flowchart, flowchart.Blocks, block);
                    Block.DestroyBlock(flowchart, block);
                }
                else
                {
                    base.Delete(new int[] { index });
                }
            }
        }

        protected override object[] Copy(int[] indices)
        {
            Block[] components = new Block[indices.Length];

            for (int i = 0; i < components.Length; i++)
            {
                int indexToCopy = indices[i];
                SerializedProperty componentProperty = this[indexToCopy];
                components[i] = componentProperty.objectReferenceValue as Block;
            }

            return ComponentClipboard.CopyToBuffer(components);
        }

        protected override void SetClearCopyBufferOnNextPaste(bool clear)
        {
            base.SetClearCopyBufferOnNextPaste(clear);
            ComponentClipboard.ClearClipboardOnNextPaste = clear;
        }

        protected override void Paste(int index, object[] copyBuffer)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            Block[] pastedBlocks = ComponentClipboard.PasteFromBuffer<Block>(flowchart.gameObject);

            foreach (Block pastedBlock in pastedBlocks)
            {
                Block.AddBlockToList(flowchart, flowchart.Blocks, pastedBlock, index, Block.DEFAULT_BLOCK_NAME);
                index++;
            }

            ApplyPropertyModifications();
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            SerializedProperty blockProp = this[index];
            Block block = blockProp.objectReferenceValue as Block;

            if (block != null)
            {
                return new string[] { block.GetName() };
            }
            return new string[0];
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            SerializedProperty sp = this[index];

            Block block = sp.objectReferenceValue as Block;

            if (block == null)
            {
                return;
            }

            if (block != null && block.ContainsError())
            {
                GUI.backgroundColor = m_errorColor;
            }

            SerializedObject so = new SerializedObject(sp.objectReferenceValue);
            SerializedProperty blockNameProp = so.FindProperty("m_" + nameof(Block.BlockName));

            if (block.TemplateBlock == null)
            {
                EditorGUI.BeginChangeCheck();
                EditorGUI.PropertyField(position, blockNameProp, GUIContent.none, true);
                if (EditorGUI.EndChangeCheck())
                {
                    blockNameProp.stringValue = blockNameProp.stringValue.ToUpper();
                }
            }
            else
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                EditorGUI.TextField(position, block.GetName());
                GUI.enabled = guiWasEnabled;
            }

            so.ApplyModifiedProperties();
            sp.objectReferenceValue = so.targetObject;
            sp.serializedObject.ApplyModifiedProperties();

            GUI.backgroundColor = Color.white;
        }

        protected override void OnSearchMatchesUpdated(int[] matchIndices)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart == flowchart)
            {
                INode[] nodes = null;
                if (matchIndices != null)
                {
                    nodes = new INode[matchIndices.Length];
                    for (int i = 0; i < nodes.Length; i++)
                    {
                        nodes[i] = flowchart.Blocks[matchIndices[i]];
                    }
                }
                FlowchartWindow.Instance.FocusNodes(nodes);
            }
        }

        protected override void OnMouseUp(int index)
        {
            SerializedProperty sp = this[index];

            Block block = sp.objectReferenceValue as Block;

            if (block == null)
            {
                return;
            }

            Flowchart flowchart = block.GetFlowchart();

            if (FlowchartWindow.Instance != null)
            {
                FlowchartWindow.Instance.CenterViewOnNode(block, FlowchartWindow.Instance.ZoomLevel);
                flowchart.SetSelectedBlock(block);
            }
        }
    }
}
#endif