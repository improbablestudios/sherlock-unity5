#if UNITY_EDITOR
// Copyright (c) 2012-2013 Rotorz Limited. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

using System;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.Localization;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.UnityEditorUtilities;
using System.Collections.Generic;

namespace Fungus
{
    public abstract class VariableReorderableList : SerializedPropertyReorderableList
    {
        SerializedPropertyReorderableListCache m_reorderableListCache = new SerializedPropertyReorderableListCache();

        private bool m_guiWasEnabled = true;

        public VariableReorderableList() : base()
        {
        }

        public VariableReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public VariableReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public VariableReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public VariableReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            SerializedProperty variableProp = this[index];
            Variable variable = variableProp.objectReferenceValue as Variable;

            if (variable != null)
            {
                Type type = variable.GetType();
                string name = type.Name;
                FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);
                if (info != null)
                {
                    name = info.GetName(type);
                }

                return new string[] { variable.GetName(), name, (variable.Value != null) ? variable.Value.ToString() : "" };
            }
            return new string[0];
        }

        public override bool UseCreateMenu()
        {
            return true;
        }

        protected override void ShowCreateMenu()
        {
            VariableTypeSearchPopupWindow.Show(m_controlID, m_headerRect);
        }

        protected override void HandleCreateMenu(object obj)
        {
            Type type = obj as Type;

            int index = m_createIndex;

            Component component = m_listProperty.serializedObject.targetObject as Component;

            Variable variable = Variable.CreateVariable(component, type, GetNewVariableDefaultName(), GetNewVariableParentBlock(), GetNewVariableScope());
            Variable.AddVariableToList(component, GetVariableList(), variable, index, GetNewVariableDefaultName());

            OnCreateMenuHandled(index);
        }

        protected override void Delete(int[] indices)
        {
            Component component = m_listProperty.serializedObject.targetObject as Component;

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                Variable variable = this[index].objectReferenceValue as Variable;
                if (variable != null)
                {
                    Variable.RemoveVariableFromList(component, GetVariableList(), variable);
                    Variable.DestroyVariable(component, variable);
                }
                else
                {
                    base.Delete(new int[] { index });
                }
            }
        }

        protected override object[] Copy(int[] indices)
        {
            Variable[] components = new Variable[indices.Length];

            for (int i = 0; i < components.Length; i++)
            {
                int indexToCopy = indices[i];
                SerializedProperty componentProperty = this[indexToCopy];
                components[i] = componentProperty.objectReferenceValue as Variable;
            }

            return ComponentClipboard.CopyToBuffer(components);
        }

        protected override void SetClearCopyBufferOnNextPaste(bool clear)
        {
            base.SetClearCopyBufferOnNextPaste(clear);
            ComponentClipboard.ClearClipboardOnNextPaste = clear;
        }

        protected override void Paste(int index, object[] copyBuffer)
        {
            Component component = m_listProperty.serializedObject.targetObject as Component;

            Variable[] pastedVariables = ComponentClipboard.PasteFromBuffer<Variable>(component.gameObject);

            foreach (Variable pastedVariable in pastedVariables)
            {
                Variable.AddVariableToList(component, GetVariableList(), pastedVariable, index, GetNewVariableDefaultName());
                index++;
            }

            ApplyPropertyModifications();
        }

        protected override void OnEndElementsGUI(Rect position)
        {
            VariableTypeSearchPopupWindow.ProcessSelectedItem(m_controlID, HandleCreateMenu);
        }

        protected override void OnBeginElementGUI(Rect position, int index)
        {
            SerializedProperty variableProp = this[index];

            if (variableProp == null)
            {
                return;
            }

            Variable variable = variableProp.objectReferenceValue as Variable;

            if (variable == null)
            {
                return;
            }

            m_guiWasEnabled = GUI.enabled;
            if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != variable.GetFlowchart())
            {
                GUI.enabled = false;
            }

        }

        protected override void OnEndElementGUI(Rect position, int index)
        {
            GUI.enabled = m_guiWasEnabled;
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            Variable variable = this[index].objectReferenceValue as Variable;

            if (variable == null)
            {
                return;
            }

            float[] widths = GetItemRectWidths(position.width);
            Rect[] rects = ExtendedEditorGUI.GetRects(position, widths);

            // To access properties in a monobehavior, you have to new a SerializedObject
            // http://answers.unity3d.com/questions/629803/findrelativeproperty-never-worked-for-me-how-does.html
            SerializedObject variableObject = new SerializedObject(this[index].objectReferenceValue);

            variableObject.Update();

            Variable t = variableObject.targetObject as Variable;

            TypeField(rects[0], variable);

            SerializedProperty nameProp = variableObject.FindProperty("m_" + nameof(t.VariableName));
            Rect variableNameRect = rects[1];
            variableNameRect.height = EditorGUIUtility.singleLineHeight;
            NameField(variableNameRect, nameProp, GUIContent.none, variable);

            SerializedProperty valueProp = variableObject.FindProperty("m_" + nameof(t.Value));
            if (valueProp != null)
            {
                ValueField(rects[2], valueProp, GUIContent.none);
            }
            else
            {
                EditorGUI.HelpBox(rects[2], "Type is not serializable", MessageType.Error);
            }

            SerializedProperty scopeProp = variableObject.FindProperty("m_" + nameof(t.Scope));
            ScopeField(rects[3], scopeProp, GUIContent.none, variable);

            SerializedProperty localizeProp = variableObject.FindProperty("m_" + nameof(t.Localize));
            LocalizeField(rects[4], localizeProp, GUIContent.none, variable);

            variableObject.ApplyModifiedProperties();

            EditorGUIUtility.labelWidth = 0f;
        }

        public override Color GetSelectedBackgroundColor(int index)
        {
            return EditorGUIUtility.isProSkin ? new Color(0, 0.25f, 0, 1) : new Color(0, 0.85f, 0, 1);
        }

        protected virtual float GetScopeWidth()
        {
            return 63f;
        }

        protected virtual float GetLocalizeWidth()
        {
            return 18f;
        }

        private float[] GetItemRectWidths(float totalWidth)
        {
            float scopeWidth = GetScopeWidth() + ExtendedEditorGUI.HorizontalFieldSpacing;
            float localizeWidth = GetLocalizeWidth() + ExtendedEditorGUI.HorizontalFieldSpacing;
            float flexibleWidth = (totalWidth - (scopeWidth + localizeWidth));
            float typeNameWidth = (flexibleWidth * (1f / 6f));
            float variableNameWidth = (flexibleWidth * (2f / 6f));
            float valueWidth = (flexibleWidth * (3f / 6f));
            return new float[] { typeNameWidth, variableNameWidth, valueWidth, scopeWidth, localizeWidth };
        }

        protected virtual void TypeField(Rect position, Variable variable)
        {
            string typeName = FlowchartItemInfoAttribute.GetInfo(variable.GetType()).GetName(variable.GetType());

            if (variable.m_TemplateVariable == null)
            {
                EditorGUI.LabelField(position, typeName);
            }
            else
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                EditorGUI.LabelField(position, typeName);
                GUI.enabled = guiWasEnabled;
            }
        }

        protected virtual void NameField(Rect position, SerializedProperty property, GUIContent label, Variable variable)
        {
            if (variable.m_TemplateVariable == null)
            {
                EditorGUI.BeginProperty(position, label, property);
                EditorGUI.BeginChangeCheck();
                string oldVariableName = property.stringValue;
                string newVariableName = property.stringValue;
                newVariableName = EditorGUI.DelayedTextField(position, newVariableName);
                if (EditorGUI.EndChangeCheck())
                {
                    newVariableName = variable.GetUniqueVariableName(variable.GetFlowchart().Variables, newVariableName, Flowchart.DEFAULT_FLOWCHART_VARIABLE_NAME);
                    property.stringValue = newVariableName;
                    variable.GetFlowchart().UpdateVariableNameReferences(oldVariableName, newVariableName, variable.Scope);
                }
                EditorGUI.EndProperty();
            }
            else
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                EditorGUI.TextField(position, variable.GetName());
                GUI.enabled = guiWasEnabled;
            }
        }

        protected virtual void ValueField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUIUtility.labelWidth = 32;
            if (property.isArray && property.propertyType != SerializedPropertyType.String)
            {
                m_reorderableListCache[property].Draw(position, property, label);
            }
            else
            {
                if (property.propertyType == SerializedPropertyType.String)
                {
                    ExtendedEditorGUI.FlexibleTextField(position, property, label);
                }
                else if (property.propertyType == SerializedPropertyType.Vector4)
                {
                    property.vector4Value = EditorGUI.Vector4Field(position, label, property.vector4Value);
                }
                else if (property.propertyType == SerializedPropertyType.Quaternion)
                {
                    ExtendedEditorGUI.QuaternionField(position, property, label);
                }
                else
                {
                    EditorGUI.PropertyField(position, property, label, true);
                }
            }
            EditorGUIUtility.labelWidth = 0;
        }

        protected virtual void ScopeField(Rect position, SerializedProperty property, GUIContent label, Variable variable)
        {
            if (variable.m_TemplateVariable == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            else
            {
                FungusGUI.ScopeProperty(position, property, label, variable.m_TemplateVariable.Scope);
            }
        }

        protected virtual void LocalizeField(Rect position, SerializedProperty property, GUIContent label, Variable variable)
        {
            if (variable.m_TemplateVariable == null)
            {
                if (LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(variable.ValueType))
                {
                    FungusGUI.LocalizeToggle(position, property);
                }
            }
            else
            {
                bool guiWasEnabled = GUI.enabled;
                GUI.enabled = false;
                FungusGUI.LocalizeToggle(position, variable.GetLocalize());
                GUI.enabled = guiWasEnabled;
            }
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            SerializedProperty property = this[index];
            if (property == null)
            {
                return base.GetElementExpandedContentHeight(index);
            }

            if (property.objectReferenceValue == null)
            {
                return base.GetElementExpandedContentHeight(index);
            }

            SerializedObject variableObject = new SerializedObject(property.objectReferenceValue);
            SerializedProperty valueProperty = variableObject.FindProperty("m_" + nameof(Variable.Value));
            if (valueProperty != null)
            {
                if (valueProperty.isArray && valueProperty.propertyType != SerializedPropertyType.String)
                {
                    return m_reorderableListCache[valueProperty].GetHeight(valueProperty, GUIContent.none);
                }
                else if (valueProperty.propertyType == SerializedPropertyType.String)
                {
                    Rect position = GetElementContentRect(m_elementsContentRect, index);
                    float totalWidth = position.width;
                    float[] widths = GetItemRectWidths(totalWidth);
                    Rect[] rects = ExtendedEditorGUI.GetRects(position, widths);
                    return ExtendedEditorGUI.GetFlexibleTextAreaHeight(valueProperty.stringValue, rects[2].width, 1, 10);
                }
                else if (valueProperty.propertyType == SerializedPropertyType.Vector4)
                {
                    return EditorGUIUtility.singleLineHeight;
                }
                else if (valueProperty.propertyType == SerializedPropertyType.Quaternion)
                {
                    return EditorGUIUtility.singleLineHeight;
                }
                else
                {
                    return EditorGUI.GetPropertyHeight(valueProperty, GUIContent.none, true);
                }
            }
            return base.GetElementExpandedContentHeight(index);
        }

        protected abstract Flowchart GetFlowchart();

        protected abstract List<Variable> GetVariableList();

        protected abstract string GetNewVariableDefaultName();

        protected abstract Block GetNewVariableParentBlock();

        protected abstract Scope GetNewVariableScope();
    }

}


#endif
