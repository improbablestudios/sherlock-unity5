#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Fungus.Commands;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    public class CommandReorderableList : SerializedPropertyReorderableList
    {
        protected const float INDENT_WIDTH = 20;

        protected float m_cachedIndexWidth = -1;

        private bool m_guiWasEnabled = true;

        private Rect m_createMenuPosition;
        public Rect CreateMenuPosition
        {
            get
            {
                if (m_createMenuPosition.height == 0 && m_createMenuPosition.width == 0)
                {
                    return m_headerRect;
                }
                return m_createMenuPosition;
            }
            set
            {
                m_createMenuPosition = value;
            }
        }

        private Texture2D m_errorIcon = null;
        public Texture2D ErrorIcon
        {
            get
            {
                if (m_errorIcon == null)
                {
                    m_errorIcon = EditorGUIUtility.FindTexture("console.erroricon.sml");
                }
                return m_errorIcon;
            }
        }

        private GUIStyle m_whiteBackgroundBoxStyle = null;
        public GUIStyle WhiteBackgroundBoxStyle
        {
            get
            {
                if (m_whiteBackgroundBoxStyle == null)
                {
                    m_whiteBackgroundBoxStyle = new GUIStyle();
                    m_whiteBackgroundBoxStyle.normal.background = EditorGUIUtility.whiteTexture;
                    m_whiteBackgroundBoxStyle.alignment = TextAnchor.MiddleCenter;
                    m_whiteBackgroundBoxStyle.stretchWidth = true;
                    m_whiteBackgroundBoxStyle.stretchHeight = true;
                    m_whiteBackgroundBoxStyle.fixedWidth = 0;
                    m_whiteBackgroundBoxStyle.fixedHeight = 0;
                }
                return m_whiteBackgroundBoxStyle;
            }
        }

        private GUIStyle m_commandBackgroundBoxStyle = null;
        public GUIStyle CommandBackgroundBoxStyle
        {
            get
            {
                if (m_commandBackgroundBoxStyle == null)
                {
                    m_commandBackgroundBoxStyle = new GUIStyle("CN CountBadge");
                    m_commandBackgroundBoxStyle.alignment = TextAnchor.MiddleLeft;
                    m_commandBackgroundBoxStyle.stretchWidth = true;
                    m_commandBackgroundBoxStyle.stretchHeight = true;
                    m_commandBackgroundBoxStyle.fixedWidth = 0;
                    m_commandBackgroundBoxStyle.fixedHeight = 0;
                }
                return m_commandBackgroundBoxStyle;
            }
        }

        private GUIStyle m_commandIconLabelStyle = null;
        public GUIStyle CommandIconLabelStyle
        {
            get
            {
                if (m_commandIconLabelStyle == null)
                {
                    m_commandIconLabelStyle = new GUIStyle();
                    m_commandIconLabelStyle.fontStyle = FontStyle.Normal;
                    m_commandIconLabelStyle.alignment = TextAnchor.MiddleCenter;
                    m_commandIconLabelStyle.fontSize = 11;
                    m_commandIconLabelStyle.clipping = TextClipping.Clip;
                }
                return m_commandIconLabelStyle;
            }
        }

        private GUIStyle m_commandNameLabelStyle = null;
        public GUIStyle CommandNameLabelStyle
        {
            get
            {
                if (m_commandNameLabelStyle == null)
                {
                    m_commandNameLabelStyle = new GUIStyle(EditorStyles.label);
                    m_commandNameLabelStyle.fontStyle = FontStyle.Normal;
                    m_commandNameLabelStyle.alignment = TextAnchor.MiddleLeft;
                    m_commandNameLabelStyle.fontSize = 11;
                    m_commandNameLabelStyle.clipping = TextClipping.Clip;
                }
                return m_commandNameLabelStyle;
            }
        }

        private GUIStyle m_commandSummaryLabelStyle = null;
        public GUIStyle CommandSummaryLabelStyle
        {
            get
            {
                if (m_commandSummaryLabelStyle == null)
                {
                    m_commandSummaryLabelStyle = new GUIStyle(EditorStyles.label);
                    m_commandSummaryLabelStyle.fontStyle = FontStyle.Italic;
                    m_commandSummaryLabelStyle.alignment = TextAnchor.MiddleLeft;
                    m_commandSummaryLabelStyle.fontSize = 10;
                    m_commandSummaryLabelStyle.clipping = TextClipping.Clip;
                    m_commandSummaryLabelStyle.richText = true;
                }
                return m_commandSummaryLabelStyle;
            }
        }

        public CommandReorderableList() : base()
        {
        }

        public CommandReorderableList( ReorderableListFlags flags) : base(flags)
        {
        }

        public CommandReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public CommandReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public CommandReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public override void Init(SerializedProperty listProperty)
        {
            if (m_listProperty != listProperty)
            {
                base.Init(listProperty);

                float maxWidth = base.GetIndexWidth();
                BlockWindow blockWindow = BlockWindow.Instance;
                if (blockWindow != null)
                {
                    BlockEditor blockEditor = blockWindow.BlockEditor;
                    if (blockEditor != null)
                    {
                        foreach (CommandReorderableList commandReorderableList in blockEditor.CommandReorderableLists)
                        {
                            float width = commandReorderableList.CalculateIndexWidth();
                            if (width > maxWidth)
                            {
                                maxWidth = width;
                            }
                        }
                    }
                }
                m_cachedIndexWidth = maxWidth;
            }
        }

        protected override string[] GetElementIdentifiers(int index)
        {
            SerializedProperty commandProp = this[index];
            Command command = commandProp.objectReferenceValue as Command;

            if (command != null)
            {
                Type type = command.GetType();
                string name = type.Name;
                FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);
                if (info != null)
                {
                    name = info.GetName(type);
                }

                return new string[] { name };
            }
            return new string[0];
        }

        public override bool UseCreateMenu()
        {
            return true;
        }

        protected override void ShowCreateMenu()
        {
            CommandTypeSearchPopupWindow.Show(m_controlID, CreateMenuPosition, SearchPopupWindowFlags.HideDefaultOption);
        }

        protected override void Delete(int[] indices)
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                Command command = this[index].objectReferenceValue as Command;
                if (command != null)
                {
                    block.RemoveCommand(command);
                }
                else
                {
                    base.Delete(new int[] { index });
                }
            }
        }

        protected override object[] Copy(int[] indices)
        {
            Command[] components = new Command[indices.Length];

            for (int i = 0; i < components.Length; i++)
            {
                int indexToCopy = indices[i];
                SerializedProperty componentProperty = this[indexToCopy];
                components[i] = componentProperty.objectReferenceValue as Command;
            }

            return ComponentClipboard.CopyToBuffer(components);
        }

        protected override void SetClearCopyBufferOnNextPaste(bool clear)
        {
            base.SetClearCopyBufferOnNextPaste(clear);
            ComponentClipboard.ClearClipboardOnNextPaste = clear;
        }

        protected override void Paste(int index, object[] copyBuffer)
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            Flowchart flowchart = block.GetFlowchart();

            Command[] pastedCommands = ComponentClipboard.PasteFromBuffer<Command>(flowchart.gameObject);

            foreach (Command pastedCommand in pastedCommands)
            {
                pastedCommand.Setup(block);
                Insert(index, pastedCommand);
                index++;
            }

            ApplyPropertyModifications();

            block.RecordChanges();
        }
        
        public override bool CanCreate()
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;
            
            if (block != null)
            {
                return (FlowchartWindow.Instance == null || FlowchartWindow.Instance.TargetFlowchart == block.GetFlowchart());
            }
            return true;
        }

        public override bool CanMove(int index)
        {
            SerializedProperty commandProp = this[index];

            if (commandProp != null)
            {
                Command command = commandProp.objectReferenceValue as Command;

                if (command != null)
                {
                    return (FlowchartWindow.Instance == null || FlowchartWindow.Instance.TargetFlowchart == command.GetFlowchart());
                }
            }
            return true;
        }

        public override bool CanDelete(int index)
        {
            SerializedProperty commandProp = this[index];
            
            return commandProp == null || FlowchartWindow.Instance == null || (commandProp.objectReferenceValue as Command) == null || (FlowchartWindow.Instance.TargetFlowchart == (commandProp.objectReferenceValue as Command).GetFlowchart());
        }

        protected override void OnEndElementsGUI(Rect position)
        {
            CommandTypeSearchPopupWindow.ProcessSelectedItem(m_controlID, HandleCreateMenu);
            HandleKeyboardShortcuts(position);
        }

        protected override void HandleCreateMenu(object obj)
        {
            Type type = obj as Type;

            int index = m_createIndex;

            Block block = m_listProperty.serializedObject.targetObject as Block;

            block.AddCommand(type, index);

            OnCreateMenuHandled(index);
        }

        protected void HandleKeyboardShortcuts(Rect position)
        {
            Event current = Event.current;
            EventType currentEventType = current.GetTypeForControl(ControlID);

            if (!HasKeyboardFocus())
            {
                return;
            }
            
            if (currentEventType == EventType.KeyDown)
            {
                BlockWindow blockWindow = BlockWindow.Instance;
                if (blockWindow != null)
                {
                    Block blockWindowTargetBlock = blockWindow.TargetBlock;
                    if (blockWindowTargetBlock != null)
                    {
                        BlockEditor blockEditor = blockWindow.BlockEditor;
                        if (blockEditor != null)
                        {
                            // DownArrow shortcut
                            if (current.keyCode == KeyCode.DownArrow)
                            {
                                int lastSelectedIndex = GetLastSelectedIndex();
                                if (lastSelectedIndex >= 0)
                                {
                                    int nextSelectedIndex = lastSelectedIndex + 1;
                                    if (nextSelectedIndex >= GetElementCount())
                                    {
                                        CommandReorderableList[] orderedCommandReorderableLists = blockEditor.OrderedCommandReorderableLists;

                                        int indexOfThisList = Array.IndexOf(orderedCommandReorderableLists, this);
                                        int indexOfNextList = indexOfThisList + 1;
                                        if (indexOfNextList < orderedCommandReorderableLists.Length)
                                        {
                                            CommandReorderableList nextList = orderedCommandReorderableLists[indexOfNextList];
                                            if (nextList != null)
                                            {
                                                int index = 0;
                                                nextList.DoSelect(index);
                                                nextList.DoScrollTowardIndex(index);
                                                current.Use();
                                            }
                                        }
                                    }
                                }
                            }

                            // UpArrow shortcut
                            if (current.keyCode == KeyCode.UpArrow)
                            {
                                int firstSelectedIndex = GetFirstSelectedIndex();
                                if (firstSelectedIndex >= 0)
                                {
                                    int previousSelectedIndex = firstSelectedIndex - 1;
                                    if (previousSelectedIndex < 0)
                                    {
                                        CommandReorderableList[] orderedCommandReorderableLists = blockEditor.OrderedCommandReorderableLists;

                                        int indexOfThisList = Array.IndexOf(orderedCommandReorderableLists, this);
                                        int indexOfPreviousList = indexOfThisList - 1;
                                        if (indexOfPreviousList >= 0)
                                        {
                                            CommandReorderableList previousList = orderedCommandReorderableLists[indexOfPreviousList];
                                            if (previousList != null)
                                            {
                                                int index = previousList.GetElementCount() - 1;
                                                previousList.DoSelect(index);
                                                previousList.DoScrollTowardIndex(index);
                                                current.Use();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void OnBeginElementGUI(Rect position, int index)
        {
            SerializedProperty commandProp = this[index];

            if (commandProp == null)
            {
                return;
            }

            Command command = commandProp.objectReferenceValue as Command;

            if (command == null)
            {
                return;
            }

            m_guiWasEnabled = GUI.enabled;
            if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != command.GetFlowchart())
            {
                GUI.enabled = false;
            }

        }

        protected override void OnEndElementGUI(Rect position, int index)
        {
            GUI.enabled = m_guiWasEnabled;
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            SerializedProperty commandProp = this[index];

            if (commandProp == null)
            {
                return;
            }

            Command command = commandProp.objectReferenceValue as Command;

            if (command == null)
            {
                return;
            }

            Flowchart flowchart = command.GetFlowchart();
            if (flowchart == null)
            {
                return;
            }

            FlowchartItemInfoAttribute commandInfoAttr = FlowchartItemInfoAttribute.GetInfo(command.GetType());

            if (Application.isPlaying)
            {
                // Keep the block inspector centered on the active command
                if (BlockWindow.Instance.TargetBlock.ActiveCommand != BlockWindow.Instance.ActiveCommand && BlockWindow.Instance.TargetBlock.ActiveCommand == command)
                {
                    BlockWindow.Instance.ActiveCommand = BlockWindow.Instance.TargetBlock.ActiveCommand;
                    BlockWindow.Instance.Repaint();
                    DoSelect(index);
                    DoScrollTowardIndex(index);
                }
            }

            bool isInert = (command is InertCommand);
            bool isComment = (command is Comment);
            bool isLabel = (command is Label);
            string error = command.GetFirstCachedError();
            bool hasError = error != null;

            string summary = "";
            if (hasError)
            {
                summary = "<b>Error: </b>" + error;
            }
            else
            {
                summary = command.GetSummary();
                if (summary == null)
                {
                    summary = "";
                }
                else
                {
                    summary = summary.Replace("\n", "").Replace("\r", "");
                }
            }
            
            string commandName = commandInfoAttr.GetName(command.GetType());
            
            Rect pos = position;

            pos.width = 0;
            for (int i = 0; i < command.IndentLevel; ++i)
            {
                pos.x += pos.width;
                pos.width = INDENT_WIDTH;
                GUI.Box(pos, "", CommandNameLabelStyle);
            }

            Texture2D icon = null;
            if (!isInert)
            {
                icon = EditorGUIUtility.FindTexture("icon dropdown");
                if (command.ShowWaitIcon())
                {
                    icon = EditorGUIUtility.FindTexture("UnityEditor.AnimationWindow");
                }
            }

            pos.x += pos.width;
            pos.width = INDENT_WIDTH;
            GUI.Label(pos, new GUIContent(icon), CommandIconLabelStyle);

            GUIContent commandLabel = isComment ? GUIContent.none : new GUIContent(commandName);
            pos.xMin += INDENT_WIDTH;
            pos.width = Mathf.Max(CommandNameLabelStyle.CalcSize(commandLabel).x + EditorGUIUtility.singleLineHeight, position.width * 0.3f);
            GUI.Label(pos, commandLabel, CommandNameLabelStyle);

            if (command.ExecutingIconTimer > Time.realtimeSinceStartup)
            {
                Rect iconRect = position;
                iconRect.x -= INDENT_WIDTH;
                iconRect.width = INDENT_WIDTH;

                Color storeColor = GUI.color;

                float alpha = (command.ExecutingIconTimer - Time.realtimeSinceStartup) / GraphWindow.EXECUTING_ICON_FADE_TIME;
                alpha = Mathf.Clamp01(alpha);

                GUI.color = new Color(1f, 1f, 1f, alpha);

                GUI.Label(iconRect, GraphWindowStyles.PlaySmallIcon, GUIStyle.none);

                GUI.color = storeColor;
            }

            Rect summaryRect;
            if (isComment)
            {
                summaryRect = position;
                summaryRect.xMin += 5;
                summaryRect.xMax -= 5;
            }
            else
            {
                summaryRect = pos;
                summaryRect.x += pos.width;
                summaryRect.width = position.xMax - pos.xMax - 8f;
                if (hasError)
                {
                    summaryRect.width -= ErrorIcon.width;
                }
            }
            summaryRect.xMin += 5;

            if (isComment || isLabel)
            {
                CommandSummaryLabelStyle.fontStyle = FontStyle.BoldAndItalic;
            }
            else
            {
                CommandSummaryLabelStyle.fontStyle = FontStyle.Italic;
            }

            GUI.Label(summaryRect, summary, CommandSummaryLabelStyle);

            if (hasError)
            {
                Rect errorRect = new Rect(position);
                errorRect.xMin = errorRect.xMax - INDENT_WIDTH;
                float padding = 4f;
                errorRect.xMin += padding;
                errorRect.xMax -= padding;
                errorRect.yMin += padding;
                errorRect.yMax -= padding;
                GUI.Label(errorRect, ErrorIcon, CommandIconLabelStyle);
            }
        }

        protected override void DrawEmptyElementContent(Rect position)
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            if (FlowchartWindow.Instance == null || FlowchartWindow.Instance.TargetFlowchart == block.GetFlowchart())
            {
                EditorGUI.HelpBox(position, "Press the \"Add Command\" button below to add a command to the list.", MessageType.Info);
            }
            else
            {
                EditorGUI.HelpBox(position, "No commands.", MessageType.Info);
            }
        }

        protected override float GetEmptyElementContentHeight()
        {
            return 38;
        }

        protected override void DrawElementBackground(Rect position, int index, bool isSelected, bool isDraggable)
        {
            SerializedProperty commandProp = this[index];

            if (commandProp != null)
            {
                Command command = commandProp.objectReferenceValue as Command;

                if (command == null)
                {
                    return;
                }

                float contentXPos = GetElementContentRect(position, index).xMin - GetElementPadding();
                Rect commandPosition = position;
                commandPosition.xMin = contentXPos + INDENT_WIDTH * command.IndentLevel;
                commandPosition.yMin += 2;
                commandPosition.yMax -= 2;

                Color restoreColor = GUI.color;
                
                GUI.color = isSelected ? GetSelectedBackgroundColor(index) : ReorderableListStyles.NormalBackgroundColor;
                WhiteBackgroundBoxStyle.Draw(position, false, isSelected, isSelected, false);
                
                if (!(command is Comment))
                {
                    GUI.color = isSelected ? Color.green : GetNormalBackgroundColor(index);
                    CommandBackgroundBoxStyle.Draw(commandPosition, false, isSelected, isSelected, false);
                }

                GUI.color = restoreColor;

                Color indentSeparatorColor = ReorderableListStyles.NormalBackgroundColor;
                float colorAdjustment = EditorGUIUtility.isProSkin ? 0.2f : -0.2f;
                indentSeparatorColor.r += colorAdjustment;
                indentSeparatorColor.g += colorAdjustment;
                indentSeparatorColor.b += colorAdjustment;
                Rect seperatorPosition = new Rect(contentXPos, commandPosition.y + (commandPosition.height / 2), 2, 2);
                for (int i = 0; i < command.IndentLevel - 1; i++)
                {
                    seperatorPosition.x += INDENT_WIDTH;
                    ReorderableListStyles.Separator(seperatorPosition, indentSeparatorColor);
                }
            }
            else
            {
                base.DrawElementBackground(position, index, isSelected, isDraggable);
            }
        }

        public override Color GetNormalBackgroundColor(int index)
        {
            SerializedProperty commandProp = this[index];
            
            if (commandProp != null)
            {
                Command command = commandProp.objectReferenceValue as Command;

                if (!command.enabled)
                {
                    return FungusColorPalette.GetColor("");
                }

                return command.GetHeaderColor();
            }

            return base.GetNormalBackgroundColor(index);
        }
        
        public override Color GetSelectedBackgroundColor(int index)
        {
            return EditorGUIUtility.isProSkin ? new Color(0, 0.25f, 0, 1) : new Color(0, 0.85f, 0, 1);
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            return 24f;
        }

        protected override void DrawElementIndex(Rect position, int index)
        {
            base.DrawElementIndex(position, index);
        }

        public override float GetSeparatorHeight()
        {
            return 0f;
        }
        
        protected override float GetIndexWidth()
        {
            return m_cachedIndexWidth;
        }

        protected override float GetDragHandleWidth()
        {
            return 0f;
        }
        
        protected override float GetDeleteButtonWidth()
        {
            return 0f;
        }

        protected float CalculateIndexWidth()
        {
            return base.GetIndexWidth();
        }
        
        protected override void OnSelect(int[] indices)
        {
            BlockWindow blockWindow = BlockWindow.Instance;
            if (blockWindow != null)
            {
                BlockEditor blockEditor = blockWindow.BlockEditor;
                if (blockEditor != null)
                {
                    foreach (CommandReorderableList reorderableList in blockEditor.CommandReorderableLists)
                    {
                        if (reorderableList != this)
                        {
                            reorderableList.SetSelectedIndices(new int[0]);
                        }
                    }
                    blockEditor.InvokeSelectedCommandsChanged();
                }
            }

            FlowchartWindow flowchartWindow = FlowchartWindow.Instance;
            if (flowchartWindow != null)
            {
                FlowchartEditor flowchartEditor = flowchartWindow.FlowchartEditor;
                if (flowchartEditor != null)
                {
                    foreach (FlowchartVariableReorderableList variableReorderableList in flowchartEditor.VariableReorderableLists)
                    {
                        variableReorderableList.SetSelectedElements(GetSelectedElements().Where(x => x != null).SelectMany(i => ((Command)i).GetVariableReferences()).ToArray());
                    }
                }
            }
        }
    }

}

#endif