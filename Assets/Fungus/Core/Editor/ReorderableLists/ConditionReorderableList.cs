#if UNITY_EDITOR
using UnityEditor;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.SerializedPropertyUtilities;

namespace Fungus
{

    public class ConditionReorderableList : SerializedPropertyReorderableList
    {
        public ConditionReorderableList() : base()
        {
        }

        public ConditionReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public ConditionReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public ConditionReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public ConditionReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public override bool CanCollapse(int index)
        {
            return false;
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            SerializedProperty sp = this[index];

            SerializedProperty variableDataProp = sp.FindPropertyRelative(nameof(Condition.m_VariableData));
            SerializedProperty compareOperatorProp = sp.FindPropertyRelative(nameof(Condition.m_CompareOperator));
            SerializedProperty valueProp = sp.FindPropertyRelative(nameof(Condition.m_Value));
            
            SerializedProperty variableDataTypeProp = variableDataProp.FindPropertyRelative("m_" + nameof(VariableData.VariableDataType));
            SerializedProperty variableRefProp = variableDataProp.FindPropertyRelative("m_" + nameof(VariableData.Variable));
            SerializedProperty commandRefProp = variableDataProp.FindPropertyRelative("m_" + nameof(VariableData.Command));

            VariableDataType variableDataType = (VariableDataType)variableDataTypeProp.enumValueIndex;
            Variable variable = variableRefProp.objectReferenceValue as Variable;
            ReturnValueCommand command = commandRefProp.objectReferenceValue as ReturnValueCommand;

            if (variableDataType == VariableDataType.Value)
            {
                variableDataTypeProp.enumValueIndex = (int)VariableDataType.Variable;
                sp.serializedObject.ApplyModifiedProperties();
                return;
            }
            
            Condition t = sp.GetPropertyValue() as Condition;

            if (Application.isPlaying)
            {
                float margin = 5f;

                if (t.m_EvaluationState == Condition.EvaluationState.Pass)
                {
                    Texture2D icon = EditorGUIUtility.FindTexture("TestPassed");
                    Rect iconPosition = position;
                    iconPosition.size = new Vector2(icon.width, icon.height);
                    iconPosition.y += (position.height / 2f) - (iconPosition.height / 2f);

                    if (Event.current.type == EventType.Repaint)
                    {
                        GUI.DrawTexture(iconPosition, icon);
                    }

                    position.xMin += iconPosition.width + margin;
                }
                else if (t.m_EvaluationState == Condition.EvaluationState.Fail)
                {
                    Texture2D icon = EditorGUIUtility.FindTexture("TestFailed");
                    Rect iconPosition = position;
                    iconPosition.size = new Vector2(icon.width, icon.height);
                    iconPosition.y += (position.height / 2f) - (iconPosition.height / 2f);

                    if (Event.current.type == EventType.Repaint)
                    {
                        GUI.DrawTexture(iconPosition, icon);
                    }

                    position.xMin += iconPosition.width + margin;
                }
            }

            List<GUIContent> operatorList = new List<GUIContent>();
            if ((variableDataType == VariableDataType.Variable && variable == null) ||
                (variableDataType == VariableDataType.Command && command == null))
            {
                position.height = EditorGUI.GetPropertyHeight(variableDataProp, GUIContent.none, true);
                EditorGUI.PropertyField(position, variableDataProp, GUIContent.none, true);
            }
            else
            {
                float compareOperatorWidth = 50;
                float leftoverWidth = position.width - compareOperatorWidth;
                float lhsWidth = (0.5f * (leftoverWidth));
                float rhsWidth = (0.5f * (leftoverWidth));
                float[] widths = { lhsWidth, compareOperatorWidth, rhsWidth };
                Rect[] rects = ExtendedEditorGUI.GetRects(position, widths);
                
                EditorGUIUtility.labelWidth = GetLabelWidth(rects[0]);
                rects[0].height = EditorGUI.GetPropertyHeight(variableDataProp, GUIContent.none, true);
                EditorGUI.PropertyField(rects[0], variableDataProp, GUIContent.none, true);
                EditorGUIUtility.labelWidth = 0f;

                Type[] lhsTypes = new Type[0];
                if (variableDataType == VariableDataType.Variable)
                {
                    lhsTypes = new Type[] { variable.ValueType };
                }
                else if (variableDataType == VariableDataType.Command)
                {
                    lhsTypes = command.SpecificReturnTypes();
                }

                operatorList.Add(new GUIContent("=="));
                operatorList.Add(new GUIContent("!="));
                if (lhsTypes.Contains(typeof(int)) || lhsTypes.Contains(typeof(float)))
                {
                    operatorList.Add(new GUIContent("<"));
                    operatorList.Add(new GUIContent(">"));
                    operatorList.Add(new GUIContent("<="));
                    operatorList.Add(new GUIContent(">="));
                }

                rects[1].height = EditorGUI.GetPropertyHeight(compareOperatorProp, GUIContent.none, true);
                compareOperatorProp.enumValueIndex = EditorGUI.Popup(rects[1], GUIContent.none, compareOperatorProp.enumValueIndex, operatorList.ToArray());

                t.SetupValueType();

                rects[2].height = EditorGUI.GetPropertyHeight(valueProp, GUIContent.none, true);
                VariableEditor.VariableDataField(rects[2], valueProp, GUIContent.none, lhsTypes);
            }

            sp.serializedObject.ApplyModifiedProperties();

            EditorGUIUtility.labelWidth = 0;
        }

        public static float GetLabelWidth(Rect position)
        {
            return Mathf.Max(position.width * 0.45f - 40f, 120f);
        }

        protected override float GetElementExpandedContentHeight(int index)
        {
            SerializedProperty sp = this[index];

            SerializedProperty variableDataProp = sp.FindPropertyRelative(nameof(Condition.m_VariableData));
            SerializedProperty valueProp = sp.FindPropertyRelative(nameof(Condition.m_Value));

            float variableDataPropHeight = EditorGUI.GetPropertyHeight(variableDataProp, GUIContent.none, true);
            float valuePropHeight = EditorGUI.GetPropertyHeight(valueProp, GUIContent.none, true);

            return Math.Max(variableDataPropHeight, valuePropHeight);
        }
    }

}
#endif