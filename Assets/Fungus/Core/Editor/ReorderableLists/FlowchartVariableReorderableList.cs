#if UNITY_EDITOR
// Copyright (c) 2012-2013 Rotorz Limited. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

using System;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.ReorderableListUtilities;
using System.Collections.Generic;

namespace Fungus
{
    public class FlowchartVariableReorderableList : VariableReorderableList
    {
        public FlowchartVariableReorderableList() : base()
        {
        }

        public FlowchartVariableReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public FlowchartVariableReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public FlowchartVariableReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public FlowchartVariableReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public override bool CanCreate()
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            if (flowchart != null)
            {
                return (flowchart.TemplateFlowchart == null);
            }

            return true;
        }

        public override bool CanMove(int index)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            if (flowchart != null)
            {
                return (flowchart.TemplateFlowchart == null);
            }

            return true;
        }

        public override bool CanDelete(int index)
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            if (flowchart != null)
            {
                return (flowchart.TemplateFlowchart == null);
            }

            return true;
        }

        protected override void OnEndElementsGUI(Rect position)
        {
            base.OnEndElementsGUI(position);

            HandleKeyboardShortcuts(position);
        }

        protected void HandleKeyboardShortcuts(Rect position)
        {
            Event current = Event.current;
            EventType currentEventType = current.GetTypeForControl(ControlID);

            if (!HasKeyboardFocus())
            {
                return;
            }

            if (currentEventType == EventType.KeyDown)
            {
                FlowchartWindow flowchartWindow = FlowchartWindow.Instance;
                if (flowchartWindow != null)
                {
                    Flowchart flowchartWindowTargetFlowchart = flowchartWindow.TargetFlowchart;
                    if (flowchartWindowTargetFlowchart != null)
                    {
                        FlowchartEditor flowchartEditor = flowchartWindow.FlowchartEditor;
                        if (flowchartEditor != null)
                        {
                            // DownArrow shortcut
                            if (current.keyCode == KeyCode.DownArrow)
                            {
                                int lastSelectedIndex = GetLastSelectedIndex();
                                if (lastSelectedIndex >= 0)
                                {
                                    int nextSelectedIndex = lastSelectedIndex + 1;
                                    if (nextSelectedIndex >= GetElementCount())
                                    {
                                        FlowchartVariableReorderableList[] orderedVariableReorderableLists = flowchartEditor.OrderedVariableReorderableLists;

                                        int indexOfThisList = Array.IndexOf(orderedVariableReorderableLists, this);
                                        int indexOfNextList = indexOfThisList + 1;
                                        if (indexOfNextList < orderedVariableReorderableLists.Length)
                                        {
                                            FlowchartVariableReorderableList nextList = orderedVariableReorderableLists[indexOfNextList];
                                            if (nextList != null)
                                            {
                                                int index = 0;
                                                nextList.DoSelect(index);
                                                nextList.DoScrollTowardIndex(index);
                                                current.Use();
                                            }
                                        }
                                    }
                                }
                            }

                            // UpArrow shortcut
                            if (current.keyCode == KeyCode.UpArrow)
                            {
                                int firstSelectedIndex = GetFirstSelectedIndex();
                                if (firstSelectedIndex >= 0)
                                {
                                    int previousSelectedIndex = firstSelectedIndex - 1;
                                    if (previousSelectedIndex < 0)
                                    {
                                        FlowchartVariableReorderableList[] orderedVariableReorderableLists = flowchartEditor.OrderedVariableReorderableLists;

                                        int indexOfThisList = Array.IndexOf(orderedVariableReorderableLists, this);
                                        int indexOfPreviousList = indexOfThisList - 1;
                                        if (indexOfPreviousList >= 0)
                                        {
                                            FlowchartVariableReorderableList previousList = orderedVariableReorderableLists[indexOfPreviousList];
                                            if (previousList != null)
                                            {
                                                int index = previousList.GetElementCount() - 1;
                                                previousList.DoSelect(index);
                                                previousList.DoScrollTowardIndex(index);
                                                current.Use();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void OnSelect(int[] indices)
        {
            FlowchartWindow flowchartWindow = FlowchartWindow.Instance;
            if (flowchartWindow != null)
            {
                FlowchartEditor flowchartEditor = flowchartWindow.FlowchartEditor;
                if (flowchartEditor != null)
                {
                    foreach (FlowchartVariableReorderableList reorderableList in flowchartEditor.VariableReorderableLists)
                    {
                        if (reorderableList != this)
                        {
                            reorderableList.SetSelectedIndices(new int[0]);
                        }
                    }
                    flowchartEditor.InvokeSelectedVariablesChanged();
                }
            }
        }

        protected override void OnChanged()
        {
            base.OnChanged();

            if (FlowchartWindow.Instance != null)
            {
                FlowchartWindow.Instance.Repaint();
            }
        }

        protected override Flowchart GetFlowchart()
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            return flowchart;
        }

        protected override List<Variable> GetVariableList()
        {
            Flowchart flowchart = m_listProperty.serializedObject.targetObject as Flowchart;

            return flowchart.Variables;
        }

        protected override string GetNewVariableDefaultName()
        {
            return Flowchart.DEFAULT_FLOWCHART_VARIABLE_NAME;
        }
        
        protected override Block GetNewVariableParentBlock()
        {
            return null;
        }

        protected override Scope GetNewVariableScope()
        {
            return Scope.Private;
        }
    }
}


#endif