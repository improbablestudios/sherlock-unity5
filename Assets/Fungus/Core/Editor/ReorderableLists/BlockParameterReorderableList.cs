#if UNITY_EDITOR
// Copyright (c) 2012-2013 Rotorz Limited. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

using System;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.ReorderableListUtilities;
using System.Collections.Generic;

namespace Fungus
{
    public class BlockParameterReorderableList : VariableReorderableList
    {
        public BlockParameterReorderableList() : base()
        {
        }

        public BlockParameterReorderableList(ReorderableListFlags flags) : base(flags)
        {
        }

        public BlockParameterReorderableList(
            ElementDrawerDelegate elementDrawerCallback, ElementHeightDelegate elementHeightCallback)
            : base(elementDrawerCallback, elementHeightCallback)
        {
        }

        public BlockParameterReorderableList(
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
            : base(expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        public BlockParameterReorderableList(
            ReorderableListFlags flags,
            ElementDrawerDelegate expandedElementDrawerCallback, ElementHeightDelegate expandedElementHeightCallback,
            ElementDrawerDelegate collapsedElementDrawerCallback, ElementHeightDelegate collapsedElementHeightCallback)
             : base(flags,
                   expandedElementDrawerCallback, expandedElementHeightCallback,
                   collapsedElementDrawerCallback, collapsedElementHeightCallback)
        {
        }

        protected override float GetScopeWidth()
        {
            return 0f;
        }

        protected override void ScopeField(Rect position, SerializedProperty property, GUIContent label, Variable variable)
        {
        }
        
        public override bool CanCreate()
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            if (block != null)
            {
                return (block.TemplateBlock == null);
            }

            return true;
        }

        public override bool CanMove(int index)
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            if (block != null)
            {
                return (block.TemplateBlock == null);
            }

            return true;
        }

        public override bool CanDelete(int index)
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            if (block != null)
            {
                return (block.TemplateBlock == null);
            }

            return true;
        }

        protected override void OnSelect(int[] indices)
        {
            BlockWindow blockWindow = BlockWindow.Instance;
            if (blockWindow != null)
            {
                BlockEditor blockEditor = blockWindow.BlockEditor;
                if (blockEditor != null)
                {
                    blockEditor.InvokeSelectedParametersChanged();
                }
            }
        }

        protected override Flowchart GetFlowchart()
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            return block.GetFlowchart();
        }

        protected override List<Variable> GetVariableList()
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            return block.Parameters;
        }

        protected override string GetNewVariableDefaultName()
        {
            return Block.DEFAULT_BLOCK_PARAMETER_NAME;
        }
        
        protected override Block GetNewVariableParentBlock()
        {
            Block block = m_listProperty.serializedObject.targetObject as Block;

            return block;
        }

        protected override Scope GetNewVariableScope()
        {
            return Scope.Protected;
        }
    }
}


#endif