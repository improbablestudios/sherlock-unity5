﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.GraphWindowUtilities;

namespace Fungus
{

    public class FungusGUI
    {
        public static float VariableDataTypeFieldWidth
        {
            get
            {
                return 18f;
            }
        }

        public static Type VariableValueTypeField(Rect position, Type type, GUIContent label, Type[] valueTypes)
        {
            EditorGUI.BeginChangeCheck();
            Type[] orderedValueTypes = valueTypes.OrderBy(i => GetFlowchartItemPopupOptionContent(Variable.GetVariableType(i)).text).ToArray();
            GUIContent[] orderedValueTypeLabels = orderedValueTypes.Select(i => new GUIContent(GetFlowchartItemPopupOptionContent(Variable.GetVariableType(i)))).ToArray();
            int selectedIndex = Array.IndexOf(orderedValueTypes, type);
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, orderedValueTypeLabels);
            if (EditorGUI.EndChangeCheck())
            {
                type = orderedValueTypes[selectedIndex];
            }
            return type;
        }

        public static void LocalizeToggle(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            GUIContent label = GUIContent.none;

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            bool value = property.boolValue;

            value = LocalizeToggle(position, value);

            if (EditorGUI.EndChangeCheck())
            {
                property.boolValue = value;
            }

            EditorGUI.EndProperty();
        }

        public static bool LocalizeToggle(Rect position, bool value)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            GUI.contentColor = EditorStyles.label.normal.textColor;
            value = ExtendedEditorGUI.ToggleButton(position, new GUIContent("文", "Toggle on or off localization of this property"), value);
            GUI.contentColor = Color.white;

            return value;
        }
        
        public static void VariableValueTypeNameField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] baseValueTypes = null)
        {
            string selectedVariableTypeName = property.stringValue;

            Type selectedType = null;
            if (!string.IsNullOrEmpty(selectedVariableTypeName))
            {
                selectedType = Type.GetType(selectedVariableTypeName);
            }

            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent popupSelectedDisplayOption = GUIContent.none;
            if (!string.IsNullOrEmpty(selectedVariableTypeName))
            {
                popupSelectedDisplayOption = new GUIContent(selectedType.GetNiceTypeName());
            }
            else
            {
                popupSelectedDisplayOption = defaultOptionLabel;
            }

            EditorGUI.BeginProperty(position, label, property);

            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash, FocusType.Keyboard, position);
            Rect contentPosition = EditorGUI.PrefixLabel(position, controlId, label);

            if (EditorGUI.DropdownButton(contentPosition, popupSelectedDisplayOption, FocusType.Keyboard))
            {
                Type[] variableTypes = null;

                if (baseValueTypes != null)
                {
                    variableTypes = typeof(Variable).FindAllInstantiableDerivedTypes((Type type) => baseValueTypes.Any(t => t.IsAssignableFrom(Variable.GetVariableValueType(type)))).ToArray();
                }
                else
                {
                    variableTypes = typeof(Variable).FindAllInstantiableDerivedTypes();
                }

                VariableTypeSearchPopupWindow.Show(controlId, contentPosition, selectedType, defaultOptionLabel, variableTypes);
                Event.current.Use();
                GUIUtility.ExitGUI();
            }

            EditorGUI.BeginChangeCheck();
            selectedType = VariableTypeSearchPopupWindow.ProcessSelectedType(controlId, selectedType);
            if (EditorGUI.EndChangeCheck())
            {
                Type selectedValueType = Variable.GetVariableValueType(selectedType);
                if (selectedValueType != null)
                {
                    property.stringValue = selectedValueType.AssemblyQualifiedName;
                }
                else
                {
                    property.stringValue = "";
                }
            }

            EditorGUI.EndProperty();
        }
        
        public static void BlockPopup(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, bool ignoreScope = false, params GUILayoutOption[] options)
        {
            BlockPopup(typeof(Block), EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, ignoreScope);
        }

        public static void BlockPopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, bool ignoreScope = false)
        {
            BlockPopup(typeof(Block), position, property, label, defaultOptionLabel, ignoreScope);
        }

        public static void BlockPopup(Type type, Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, bool ignoreScope = false)
        {
            EditorGUI.BeginProperty(position, label, property);

            Block selectedObject = property.objectReferenceValue as Block;

            UnityEngine.Object targetObject = property.serializedObject.targetObject;

            EditorGUI.BeginChangeCheck();
            
            selectedObject = BlockPopup(type, position, selectedObject, label, defaultOptionLabel, ignoreScope, targetObject);

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }

        public static Block BlockPopup(Type type, Rect position, Block value, GUIContent label, GUIContent defaultOptionLabel, bool ignoreScope, UnityEngine.Object targetObject)
        {
            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => BlockSearchPopupWindow.Show(i, r, v as Block, d, ignoreScope, targetObject, f);

            return SearchPopupGUI.ObjectSearchPopupField(position, value, label, defaultOptionLabel, type, showSearchPopupWindow, false) as Block;
        }

        public static void VariablePopup(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] valueTypes = null, bool ignoreScope = false, params GUILayoutOption[] options)
        {
            VariablePopup(typeof(Variable), EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, valueTypes, ignoreScope);
        }

        public static void VariablePopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] valueTypes = null, bool ignoreScope = false)
        {
            VariablePopup(typeof(Variable), position, property, label, defaultOptionLabel, valueTypes, ignoreScope);
        }

        public static void VariablePopup(Type type, Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] valueTypes = null, bool ignoreScope = false)
        {
            EditorGUI.BeginProperty(position, label, property);

            Variable selectedObject = property.objectReferenceValue as Variable;

            UnityEngine.Object targetObject = property.serializedObject.targetObject;

            EditorGUI.BeginChangeCheck();

            selectedObject = VariablePopup(type, position, selectedObject, label, defaultOptionLabel, valueTypes, ignoreScope, property.serializedObject.targetObject);

            if (EditorGUI.EndChangeCheck())
            {
                property.objectReferenceValue = selectedObject;
            }

            EditorGUI.EndProperty();
        }
        
        public static Variable VariablePopup(Type type, Rect position, Variable value, GUIContent label, GUIContent defaultOptionLabel, Type[] valueTypes, bool ignoreScope, UnityEngine.Object targetObject)
        {
            if (valueTypes == null)
            {
                valueTypes = new Type[] { typeof(object) };
            }
            
            ShowSearchPopupWindow showSearchPopupWindow = (int i, Rect r, object v, GUIContent d, SearchPopupWindowFlags f) => VariableSearchPopupWindow.Show(i, r, v as Variable, d, valueTypes, ignoreScope, targetObject, f);

            return SearchPopupGUI.ObjectSearchPopupField(position, value, label, defaultOptionLabel, type, showSearchPopupWindow, false) as Variable;
        }

        public static GUIContent GetFlowchartItemPopupOptionContent(Type type, GUIContent defaultOptionLabel = null)
        {
            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            if (type != null)
            {
                FlowchartItemInfoAttribute infoAttr = FlowchartItemInfoAttribute.GetInfo(type);

                if (infoAttr != null)
                {
                    if (!string.IsNullOrEmpty(infoAttr.GetCategory(type)))
                    {
                        return new GUIContent(infoAttr.GetCategory(type) + "/" + infoAttr.GetName(type));
                    }
                    else
                    {
                        return new GUIContent(infoAttr.GetName(type));
                    }
                }
            }
            return defaultOptionLabel;
        }

        public static bool DrawReturnValueCommand(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, bool foldout, ref CommandEditor commandEditor, Type[] valueTypes = null)
        {
            EditorGUI.BeginProperty(position, label, property);

            Command selectedCommand = property.objectReferenceValue as Command;

            Rect contentPosition = EditorGUI.PrefixLabel(position, label);

            if (selectedCommand != null)
            {
                if (commandEditor != null &&
                   (commandEditor.target == null || commandEditor.target != selectedCommand))
                {
                    GUI.FocusControl("");
                    UnityEngine.Object.DestroyImmediate(commandEditor);
                    commandEditor = null;
                }
                if (commandEditor == null)
                {
                    commandEditor = Editor.CreateEditor(selectedCommand) as CommandEditor;
                    if (property.serializedObject.targetObject is EventHandler)
                    {
                        commandEditor.CommandWindow = null;
                        commandEditor.BlockWindow = BlockWindow.Instance;
                    }
                    else if (property.serializedObject.targetObject is Command)
                    {
                        commandEditor.CommandWindow = CommandWindow.Instance;
                        commandEditor.BlockWindow = null;
                    }
                }

                if (commandEditor.ShouldDrawCommandArgumentsGUI())
                {
                    float foldoutWidth = EditorStyles.foldout.normal.background.width + 4f;

                    Rect commandFoldoutPosition = contentPosition;
                    commandFoldoutPosition.width = foldoutWidth;
                    commandFoldoutPosition.height = EditorGUIUtility.singleLineHeight;
                    bool restoreHierarchyMode = EditorGUIUtility.hierarchyMode;
                    EditorGUIUtility.hierarchyMode = false;
                    foldout = EditorGUI.Foldout(commandFoldoutPosition, foldout, GUIContent.none);
                    EditorGUIUtility.hierarchyMode = restoreHierarchyMode;

                    contentPosition.xMin += foldoutWidth;
                }
            }

            Rect commandPopupPosition = contentPosition;
            commandPopupPosition.height = EditorGUIUtility.singleLineHeight;
            ReturnValueCommandPopup(commandPopupPosition, property, GUIContent.none, defaultOptionLabel, valueTypes);

            if (foldout)
            {
                if (selectedCommand != null)
                {
                    contentPosition.yMin += EditorGUIUtility.singleLineHeight;

                    commandEditor.DrawCommandArgumentsGUI(contentPosition);
                }
            }

            EditorGUI.EndProperty();

            return foldout;
        }

        public static void BlockField(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Flowchart flowchart = null)
        {
            if (flowchart != null)
            {
                Block selectedBlock = property.objectReferenceValue as Block;
                EditorGUI.BeginChangeCheck();
                selectedBlock = SearchPopupGUI.ObjectSearchPopupField<Block>(position, selectedBlock, label, defaultOptionLabel, flowchart.Blocks.ToArray());
                if (EditorGUI.EndChangeCheck())
                {
                    property.objectReferenceValue = selectedBlock;
                }
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        public static void CommandPopup<T>(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel) where T : Command
        {
            CommandPopup(position, property, label, defaultOptionLabel, typeof(T));
        }

        public static void CommandPopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel, Type baseType)
        {
            EditorGUI.BeginProperty(position, label, property);

            Command selectedCommand = property.objectReferenceValue as Command;

            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent popupSelectedDisplayOption = GUIContent.none;
            if (selectedCommand != null)
            {
                Type type = selectedCommand.GetType();
                FlowchartItemInfoAttribute infoAttr = FlowchartItemInfoAttribute.GetInfo(type);
                if (infoAttr != null)
                {
                    popupSelectedDisplayOption = new GUIContent(infoAttr.GetName(type));
                }
            }
            else
            {
                popupSelectedDisplayOption = defaultOptionLabel;
            }

            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash, FocusType.Keyboard, position);
            Rect contentPosition = EditorGUI.PrefixLabel(position, controlId, label);

            if (EditorGUI.DropdownButton(contentPosition, popupSelectedDisplayOption, FocusType.Keyboard))
            {
                Type selectedType = null;
                if (selectedCommand != null)
                {
                    selectedType = selectedCommand.GetType();
                }
                CommandTypeSearchPopupWindow.Show(controlId, contentPosition, selectedType, defaultOptionLabel, baseType.FindAllInstantiableDerivedTypes());
                Event.current.Use();
                GUIUtility.ExitGUI();
            }

            CommandTypeSearchPopupWindow.ProcessAddAndSetObjectProperty(controlId, property);

            EditorGUI.EndProperty();
        }

        public static void ReturnValueCommandPopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] valueTypes = null)
        {
            EditorGUI.BeginProperty(position, label, property);

            ReturnValueCommand selectedCommand = property.objectReferenceValue as ReturnValueCommand;

            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent popupSelectedDisplayOption = GUIContent.none;
            if (selectedCommand != null)
            {
                Type type = selectedCommand.GetType();
                FlowchartItemInfoAttribute infoAttr = FlowchartItemInfoAttribute.GetInfo(type);
                if (infoAttr != null)
                {
                    popupSelectedDisplayOption = new GUIContent(infoAttr.GetName(type));
                }
            }
            else
            {
                popupSelectedDisplayOption = defaultOptionLabel;
            }

            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash, FocusType.Keyboard, position);
            Rect contentPosition = EditorGUI.PrefixLabel(position, controlId, label);

            if (EditorGUI.DropdownButton(contentPosition, popupSelectedDisplayOption, FocusType.Keyboard))
            {
                Type[] commandTypes = new Type[0];
                if (valueTypes == null)
                {
                    commandTypes = null;
                }
                else
                {
                    commandTypes = typeof(ReturnValueCommand).FindAllInstantiableDerivedTypes((Type t) => t.GetBaseGenericType() != null && valueTypes.Any(vt => vt.IsAssignableFrom(t.GetBaseGenericType().GetGenericArguments()[0]))).ToArray();
                }

                Type selectedType = null;
                if (selectedCommand != null)
                {
                    selectedType = selectedCommand.GetType();
                }
                ReturnValueCommandTypeSearchPopupWindow.Show(controlId, contentPosition, selectedType, defaultOptionLabel, commandTypes);
                Event.current.Use();
                GUIUtility.ExitGUI();
            }
            
            ReturnValueCommandTypeSearchPopupWindow.ProcessAddAndSetObjectProperty(controlId, property);

            EditorGUI.EndProperty();
        }

        public static void EventHandlerPopup(SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] types = null, params GUILayoutOption[] options)
        {
            EventHandlerPopup(EditorGUILayout.GetControlRect(options), property, label, defaultOptionLabel, types);
        }

        public static void EventHandlerPopup(Rect position, SerializedProperty property, GUIContent label, GUIContent defaultOptionLabel = null, Type[] types = null)
        {
            EditorGUI.BeginProperty(position, label, property);

            EventHandler selectedEventHandler = property.objectReferenceValue as EventHandler;

            if (defaultOptionLabel == null)
            {
                defaultOptionLabel = SearchPopupWindow.DEFAULT_OPTION_LABEL;
            }

            GUIContent popupSelectedDisplayOption = GUIContent.none;
            if (selectedEventHandler != null)
            {
                Type type = selectedEventHandler.GetType();
                FlowchartItemInfoAttribute infoAttr = FlowchartItemInfoAttribute.GetInfo(type);
                if (infoAttr != null)
                {
                    popupSelectedDisplayOption = new GUIContent(infoAttr.GetName(type));
                }
            }
            else
            {
                popupSelectedDisplayOption = defaultOptionLabel;
            }

            int controlId = GUIUtility.GetControlID(SearchPopupWindow.ControlIDHash, FocusType.Keyboard, position);
            Rect contentPosition = EditorGUI.PrefixLabel(position, controlId, label);

            if (EditorGUI.DropdownButton(contentPosition, popupSelectedDisplayOption, FocusType.Keyboard))
            {
                Type selectedType = null;
                if (selectedEventHandler != null)
                {
                    selectedType = selectedEventHandler.GetType();
                }
                EventHandlerTypeSearchPopupWindow.Show(controlId, contentPosition, selectedType, defaultOptionLabel, types);
            }

            EventHandlerTypeSearchPopupWindow.ProcessAddAndSetObjectProperty(controlId, property);

            EditorGUI.EndProperty();
        }

        public static void SetOperatorField(Rect position, SerializedProperty property, GUIContent label, Variable variableToSet)
        {
            List<GUIContent> operatorsList = new List<GUIContent>();
            operatorsList.Add(new GUIContent("="));
            if (variableToSet != null)
            {
                if (variableToSet.ValueType == typeof(bool))
                {
                    operatorsList.Add(new GUIContent("=!"));
                }
                if (variableToSet.ValueType == typeof(int) ||
                    variableToSet.ValueType == typeof(float) ||
                    variableToSet.ValueType == typeof(string) ||
                    variableToSet.ValueType == typeof(Vector2) ||
                    variableToSet.ValueType == typeof(Vector3))
                {
                    operatorsList.Add(new GUIContent("+="));
                }
                if (variableToSet.ValueType == typeof(int) ||
                    variableToSet.ValueType == typeof(float) ||
                    variableToSet.ValueType == typeof(Vector2) ||
                    variableToSet.ValueType == typeof(Vector3))
                {
                    operatorsList.Add(new GUIContent("-="));
                }
                if (variableToSet.ValueType == typeof(int) ||
                    variableToSet.ValueType == typeof(float))
                {
                    operatorsList.Add(new GUIContent("*="));
                    operatorsList.Add(new GUIContent("\\="));
                }
            }

            int selectedIndex = 0;
            switch ((SetOperator)property.enumValueIndex)
            {
                case SetOperator.Assign:
                    selectedIndex = 0;
                    break;
                case SetOperator.Negate:
                    selectedIndex = 1;
                    break;
                case SetOperator.Add:
                    selectedIndex = 1;
                    break;
                case SetOperator.Subtract:
                    selectedIndex = 2;
                    break;
                case SetOperator.Multiply:
                    selectedIndex = 3;
                    break;
                case SetOperator.Divide:
                    selectedIndex = 4;
                    break;
            }

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            selectedIndex = EditorGUI.Popup(position, label, selectedIndex, operatorsList.ToArray());
            if (EditorGUI.EndChangeCheck())
            {
                SetOperator setOperator = SetOperator.Assign;
                switch (selectedIndex)
                {
                    case 0:
                        setOperator = SetOperator.Assign;
                        break;
                    case 1:
                        if (variableToSet.ValueType == typeof(bool))
                        {
                            setOperator = SetOperator.Negate;
                        }
                        else
                        {
                            setOperator = SetOperator.Add;
                        }
                        break;
                    case 2:
                        setOperator = SetOperator.Subtract;
                        break;
                    case 3:
                        setOperator = SetOperator.Multiply;
                        break;
                    case 4:
                        setOperator = SetOperator.Divide;
                        break;
                }

                property.enumValueIndex = (int)setOperator;
            }
            EditorGUI.EndProperty();
        }

        public static void ScopeProperty(Rect position, SerializedProperty property, GUIContent label, Scope widestScopePossible)
        {
            List<Scope> scopeOptions = new List<Scope>();
            foreach (Scope scopeOption in Enum.GetValues(typeof(Scope)))
            {
                if (scopeOption <= widestScopePossible)
                {
                    scopeOptions.Add(scopeOption);
                }
            }

            GUIContent[] scopeLabels = scopeOptions.Select(i => new GUIContent(ObjectNames.NicifyVariableName(i.ToString()))).ToArray();

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            Scope selectedScope = (Scope)property.enumValueIndex;

            int selectedIndex = scopeOptions.IndexOf(selectedScope);

            selectedIndex = EditorGUI.Popup(position, selectedIndex, scopeLabels);

            if (EditorGUI.EndChangeCheck())
            {
                selectedScope = scopeOptions[selectedIndex];

                property.enumValueIndex = (int)selectedScope;
            }

            EditorGUI.EndProperty();
        }
    }
}
#endif