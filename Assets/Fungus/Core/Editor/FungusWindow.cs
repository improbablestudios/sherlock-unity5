#if UNITY_EDITOR
using ImprobableStudios.BaseWindowUtilities;

namespace Fungus
{
    
    public class FungusWindow : BaseWindow
    {
        public static void ShowBasicFungusWindows()
        {
            ShowWindow<CommandWindow>();
            ShowWindow<BlockWindow>();
            ShowWindow<FlowchartWindow>();
        }
    }

}
#endif