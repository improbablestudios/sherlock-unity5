#if UNITY_EDITOR
using System.IO;
using UnityEditor;

namespace Fungus
{
    
    public static class FungusLayout
    {
        const string LAYOUT_NAME = "FungusLayout";

        static string s_layoutPath;
        static string LayoutPath
        {
            get
            {
                if (s_layoutPath == null)
                {
                    string[] foldersToSearch = new string[] { "Assets" };
                    string[] guids = AssetDatabase.FindAssets(LAYOUT_NAME, foldersToSearch);
                    foreach (string guid in guids)
                    {
                        string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                        string extension = Path.GetExtension(assetPath);
                        UnityEngine.Object textAsset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(UnityEngine.Object)) as UnityEngine.Object;

                        if (textAsset.name == LAYOUT_NAME && extension == ".wlt")
                        {
                            s_layoutPath = assetPath;
                        }
                    }
                }

                return s_layoutPath;
            }
        }

        [MenuItem("Tools/Fungus/Load Fungus Layout", false, 990)]
        [MenuItem("Window/Layouts/Fungus", false, 0)]
        public static void LoadFungusLayout()
        {
            if (LayoutPath != null)
            {
                string path = Path.Combine(Directory.GetCurrentDirectory(), LayoutPath);
                EditorUtility.LoadWindowLayout(path);
            }
        }
    }

}
#endif