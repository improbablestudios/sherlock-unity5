#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using ImprobableStudios.ReflectionUtilities;
using UnityEditor.Experimental.SceneManagement;

namespace Fungus
{
    public class FlowchartWindow : GraphWindow
    {
        private static FlowchartWindow s_instance;
        
        protected Flowchart m_targetFlowchart;

        protected FlowchartEditor m_flowchartEditor;

        protected string m_newScriptTargetPath;

        protected Flowchart m_flowchartToAddNewScriptTo;

        protected const int FLOWCHART_NODE_HEIGHT = 20;

        protected BlockEditor m_previousBlockEditor;

        protected Flowchart m_previouslyDisplayedFlowchart;

        protected Dictionary<INode, GUIContent> m_nodeTopLabels = new Dictionary<INode, GUIContent>();

        public static FlowchartWindow Instance
        {
            get
            {
                return s_instance;
            }
        }

        public Flowchart TargetFlowchart
        {
            get
            {
                if (!m_locked)
                {
                    if (Selection.activeGameObject != null)
                    {
                        Flowchart targetFlowchart = Selection.activeGameObject.GetComponent<Flowchart>();
                        if (targetFlowchart != null)
                        {
                            if (m_targetFlowchart == null || m_targetFlowchart != targetFlowchart)
                            {
                                m_targetFlowchart = targetFlowchart;
                            }
                        }
                    }
                }
                return m_targetFlowchart;
            }
            set
            {
                Flowchart flowchart = value;

                FungusWindow.ShowBasicFungusWindows();

                if (flowchart != null)
                {
                    Flowchart.SetSelectedFlowchart(flowchart);
                    if (AppFlowWindow.Instance != null)
                    {
                        AppFlowWindow.Instance.CenterViewOnNodes(new INode[] { flowchart }, true);
                    }
                    m_targetFlowchart = flowchart;
                }
            }
        }

        public FlowchartEditor FlowchartEditor
        {
            get
            {
                if (TargetFlowchart != null)
                {
                    if (m_flowchartEditor != null &&
                       (m_flowchartEditor.target == null || m_flowchartEditor.target != TargetFlowchart))
                    {
                        GUI.FocusControl("");
                        UnityEngine.Object.DestroyImmediate(m_flowchartEditor);
                        m_flowchartEditor = null;
                    }
                    if (m_flowchartEditor == null)
                    {
                        m_flowchartEditor = Editor.CreateEditor(TargetFlowchart) as FlowchartEditor;
                    }

                    return m_flowchartEditor as FlowchartEditor;
                }
                return null;
            }
        }

        public string NewScriptTargetPath
        {
            get
            {
                return m_newScriptTargetPath;
            }
            set
            {
                m_newScriptTargetPath = value;
            }
        }

        public Flowchart FlowchartToAddNewScriptTo
        {
            get
            {
                return m_flowchartToAddNewScriptTo;
            }
            set
            {
                m_flowchartToAddNewScriptTo = value;
            }
        }

        protected BlockEditor BlockEditor
        {
            get
            {
                BlockWindow blockWindow = BlockWindow.Instance;
                if (blockWindow != null)
                {
                    return blockWindow.BlockEditor;
                }
                return null;
            }
        }

        protected Command[] SelectedCommands
        {
            get
            {
                if (BlockEditor != null)
                {
                    return BlockEditor.SelectedCommands;
                }
                return new Command[0];
            }
        }

        [MenuItem("Tools/Fungus/Flowchart Window", false, 10008)]
        [MenuItem("Window/Fungus/Flowchart", false, 10008)]
        private static void Init()
        {
            ShowWindow<FlowchartWindow>();
        }

        public override string GetWindowName()
        {
            return GetGraphTitle();
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            s_instance = this;

            OnDisplayedFlowchartChanged(null, TargetFlowchart);

            RefreshWindow();
        }
        
        protected override void Update()
        {
            if (FlowchartEditor != null)
            {
                if (!string.IsNullOrEmpty(m_newScriptTargetPath))
                {
                    MonoScript monoScript = AssetDatabase.LoadAssetAtPath(m_newScriptTargetPath, typeof(MonoScript)) as MonoScript;
                    if (monoScript != null && monoScript.GetClass() != null)
                    {
                        if (m_flowchartToAddNewScriptTo != null)
                        {
                            int index = FlowchartEditor.TargetFlowchartInsertVariableIndex;
                            m_flowchartToAddNewScriptTo.AddVariable(monoScript.GetClass(), index);
                            FlowchartEditor.TargetFlowchartSelectedVariableIndices = new int[] { index };
                        }
                    }
                    m_newScriptTargetPath = null;
                }
            }

            base.Update();
        }

        protected void OnSelectionChange()
        {
            RefreshWindow();
        }

        protected override void OnGUI()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode && !Application.isPlaying)
            {
                return;
            }

            if (!m_locked)
            {
                if (Flowchart.AppFlowGraph.GetSelectedNodes().Length > 1)
                {
                    DrawMessage("Multi-editing flowcharts is not supported", MessageType.Warning);
                    return;
                }
            }
            if (m_targetFlowchart == null)
            {
                DrawMessage("No Flowchart selected", MessageType.Info);
                return;
            }
            if (PrefabUtility.IsPartOfPrefabAsset(m_targetFlowchart) && !m_targetFlowchart.IsEditable())
            {
                BeginDrawMessage();

                DrawMessageLabel("Open Prefab to begin editing", MessageType.Info);

                if (DrawMessageButton("Open Prefab"))
                {
                    AssetDatabase.OpenAsset(m_targetFlowchart.gameObject);
                }

                EndDrawMessage();
                return;
            }

            Flowchart flowchart = TargetFlowchart;
            if (m_previouslyDisplayedFlowchart != flowchart)
            {
                OnDisplayedFlowchartChanged(m_previouslyDisplayedFlowchart, flowchart);
            }
            m_previouslyDisplayedFlowchart = flowchart;

            BlockEditor blockEditor = BlockEditor;
            if (m_previousBlockEditor != blockEditor)
            {
                OnBlockEditorChanged(m_previousBlockEditor, blockEditor);
            }
            m_previousBlockEditor = blockEditor;


            base.OnGUI();
        }

        public override Graph GetGraph()
        {
            Flowchart flowchart = TargetFlowchart;
            if (flowchart != null)
            {
                return flowchart.Graph;
            }
            return null;
        }

        public override string GetGraphTitle()
        {
            Flowchart flowchart = TargetFlowchart;
            if (flowchart != null)
            {
                return flowchart.name;
            }
            return "";
        }

        public override string GetGraphDescription()
        {
            Flowchart flowchart = TargetFlowchart;
            if (flowchart != null)
            {
                return flowchart.Description;
            }
            return "";
        }

        protected override bool CanCreateNodes
        {
            get
            {
                return true;
            }
        }

        protected override void PopulateGraphNodes()
        {
            Flowchart flowchart = TargetFlowchart;

            if (flowchart == null)
            {
                return;
            }

            List<INode> nodes = new List<INode>();
            foreach (Flowchart flowchartTemplate in flowchart.GetTemplateFlowchartChain())
            {
                nodes.AddRange(flowchartTemplate.Blocks.Where(i => i.Scope == Scope.Private).Cast<INode>());
            }
            nodes.AddRange(flowchart.Blocks.Where(i => i != null && i.GetFlowchart() == TargetFlowchart).Cast<INode>());
            nodes = nodes.Where(i => i.GetUnityObject() != null).ToList();

            flowchart.Graph.m_Nodes = nodes;
        }

        protected override void RefreshWindow()
        {
            PopulateGraph();
            RefreshNodeTopLabels();

            base.RefreshWindow();
        }

        protected void RefreshNodeTopLabels()
        {
            m_nodeTopLabels.Clear();
        }

        protected override bool CanDeleteNode(INode node)
        {
            return !IsInheritedBlock(node as Block);
        }

        protected override bool CanMoveNode(INode node)
        {
            return !IsInheritedBlock(node as Block);
        }

        protected override bool CanRenameNode(INode node)
        {
            return !IsInheritedBlock(node as Block);
        }

        public override INode CreateNode(Vector2 position)
        {
            Flowchart flowchart = TargetFlowchart;

            Block block = flowchart.AddBlock(position);
            flowchart.ValidateFlowchart();

            OnCreateNode(block);

            ResourceTracker.CacheLoadedObjects();

            return block;
        }

        public override void DeleteNode(INode node)
        {
            Flowchart flowchart = TargetFlowchart;
            Block block = node as Block;

            OnDeleteNode(node);

            flowchart.RemoveBlock(block);

            ResourceTracker.RemoveFromCache(block);
        }

        public virtual void CutNodes(INode[] nodes)
        {
            Flowchart flowchart = TargetFlowchart;
            Block[] blocks = nodes.Cast<Block>().ToArray();

            flowchart.CutBlocks(blocks);
        }

        public virtual void CopyNodes(INode[] nodes)
        {
            Flowchart flowchart = TargetFlowchart;
            Block[] blocks = nodes.Cast<Block>().ToArray();

            flowchart.CopyBlocks(blocks);
        }

        public virtual void PasteNodes(Vector2 position)
        {
            Flowchart flowchart = TargetFlowchart;

            flowchart.PasteBlocks(position);
        }

        public virtual void DuplicateNodes(INode[] nodes)
        {
            Flowchart flowchart = TargetFlowchart;
            Block[] blocks = nodes.Cast<Block>().ToArray();

            flowchart.DuplicateBlocks(blocks);
        }

        protected override void DrawFooterContents()
        {
            Flowchart flowchart = TargetFlowchart;

            if (FlowchartEditor != null && FlowchartEditor.target != null)
            {
                FlowchartEditor.DrawVariablesGUI();
            }
        }

        protected override float GetFooterHeight()
        {
            return FlowchartEditor.GetVariablesHeight();
        }

        protected override string NodeTypeName()
        {
            return typeof(Block).Name;
        }

        protected void OnBlockEditorChanged(BlockEditor oldBlockEditor, BlockEditor newBlockEditor)
        {
            if (oldBlockEditor != null)
            {
                oldBlockEditor.OnSelectedCommandsChangedCallback -= RefreshWindow;
            }
            if (newBlockEditor != null)
            {
                newBlockEditor.OnSelectedCommandsChangedCallback += RefreshWindow;
            }
        }

        protected void OnDisplayedFlowchartChanged(Flowchart oldFlowchart, Flowchart newFlowchart)
        {
            if (oldFlowchart != null)
            {
                oldFlowchart.PropertyChanged -= RefreshWindow;
            }
            if (newFlowchart != null)
            {
                newFlowchart.ValidateFlowchart();
                newFlowchart.PropertyChanged += RefreshWindow;
            }
        }

        protected override void OnContextClickGrid()
        {
            if (CanCreateNodes)
            {
                bool showPaste = false;

                if (ComponentClipboard.HasCopiedComponents<Block>())
                {
                    showPaste = true;
                }

                GenericMenu menu = new GenericMenu();

                Vector2 newPosition = GetNewNodePositionAtMouse();

                menu.AddItem(new GUIContent("Create"), false, CreateMenuFunction, newPosition);

                menu.AddSeparator("");

                if (showPaste)
                {
                    menu.AddItem(new GUIContent("Paste"), false, PasteMenuFunction, newPosition);
                }
                else
                {
                    menu.AddDisabledItem(new GUIContent("Paste"));
                }

                menu.ShowAsContext();
            }
        }

        protected override void OnContextClickNodes()
        {
            bool showDelete = false;
            bool showCut = false;
            bool showCopy = false;
            bool showDuplicate = false;

            if (TargetGraph.GetSelectedNodes().Length > 0)
            {
                if (TargetGraph.GetSelectedNodes().ToList().Find(i => CanDeleteNode(i)) != null)
                {
                    showDelete = true;
                    showCut = true;
                }
                showCopy = true;
                showDuplicate = true;
            }

            GenericMenu menu = new GenericMenu();

            if (showCut)
            {
                menu.AddItem(new GUIContent("Cut"), false, CutMenuFunction);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent("Cut"));
            }

            if (showCopy)
            {
                menu.AddItem(new GUIContent("Copy"), false, CopyMenuFunction);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent("Copy"));
            }

            menu.AddSeparator("");

            if (showDuplicate)
            {
                menu.AddItem(new GUIContent("Duplicate"), false, DuplicateMenuFunction);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent("Duplicate"));
            }

            menu.AddSeparator("");

            if (showDelete)
            {
                menu.AddItem(new GUIContent("Delete"), false, DeleteMenuFunction);
            }
            else
            {
                menu.AddDisabledItem(new GUIContent("Delete"));
            }

            menu.ShowAsContext();
        }

        protected override void ProcessKeyboardShortcuts()
        {
            if (GUIUtility.keyboardControl == 0) //Only call keyboard shortcuts when not typing in a text field
            {
                Event e = Event.current;

                // Copy keyboard shortcut
                if (e.type == EventType.ValidateCommand && e.commandName == "Copy")
                {
                    if (TargetGraph.GetSelectedNodes().Length > 0)
                    {
                        e.Use();
                    }
                }

                if (e.type == EventType.ExecuteCommand && e.commandName == "Copy")
                {
                    CopyMenuFunction();
                    e.Use();
                }

                // Cut keyboard shortcut
                if (e.type == EventType.ValidateCommand && e.commandName == "Cut")
                {
                    if (TargetGraph.GetSelectedNodes().Length > 0)
                    {
                        e.Use();
                    }
                }

                if (e.type == EventType.ExecuteCommand && e.commandName == "Cut")
                {
                    CutMenuFunction();
                    e.Use();
                }

                // Paste keyboard shortcut
                if (e.type == EventType.ValidateCommand && e.commandName == "Paste")
                {
                    if (ComponentClipboard.HasCopiedComponents<Block>())
                    {
                        e.Use();
                    }
                }

                if (e.type == EventType.ExecuteCommand && e.commandName == "Paste")
                {
                    Vector2 newPosition = GetNewNodePositionInGraph();
                    PasteMenuFunction(newPosition);
                    e.Use();
                }

                // Duplicate keyboard shortcut
                if (e.type == EventType.ValidateCommand && e.commandName == "Duplicate")
                {
                    if (TargetGraph.GetSelectedNodes().Length > 0)
                    {
                        e.Use();
                    }
                }

                if (e.type == EventType.ExecuteCommand && e.commandName == "Duplicate")
                {
                    DuplicateMenuFunction();
                    e.Use();
                }

                base.ProcessKeyboardShortcuts();
            }
        }

        protected virtual void CutMenuFunction()
        {
            CutNodes(TargetGraph.GetSelectedNodes().Where(i => CanDeleteNode(i)).ToArray());
        }

        protected virtual void CopyMenuFunction()
        {
            CopyNodes(TargetGraph.GetSelectedNodes());
        }

        protected virtual void PasteMenuFunction(object obj)
        {
            Vector2 position = (Vector2)obj;

            PasteNodes(position);
        }

        protected virtual void DuplicateMenuFunction()
        {
            DuplicateNodes(TargetGraph.GetSelectedNodes());
        }

        protected override GUIContent NodeTopLabel(INode node)
        {
            if (!m_nodeTopLabels.ContainsKey(node))
            {

                Block block = node as Block;

                EventHandler eventHandler = block.GetEventHandler();

                if (eventHandler != null)
                {
                    string labelString = "";

                    string error = null;
                    if (!Application.isPlaying || eventHandler.HasInitialized)
                    {
                        error = eventHandler.GetFirstCachedError();
                    }

                    string summary = eventHandler.GetSummary();

                    if (!string.IsNullOrEmpty(error))
                    {
                        labelString = "Error: " + error;
                    }
                    else if (!string.IsNullOrEmpty(summary))
                    {
                        labelString = summary;
                    }
                    else
                    {
                        Type type = eventHandler.GetType();
                        FlowchartItemInfoAttribute info = FlowchartItemInfoAttribute.GetInfo(type);
                        if (info != null)
                        {
                            labelString = info.GetName(type);
                        }
                    }

                    labelString = "<" + labelString + ">";

                    m_nodeTopLabels[node] = new GUIContent(labelString);
                }
                else
                {
                    m_nodeTopLabels[node] = GUIContent.none;
                }
            }

            return m_nodeTopLabels[node];
        }

        protected override void NodeNameTextField(INode node)
        {
            Rect nodeRect = GetNodeRect(node);
            GUIStyle nodeStyle = GetNodeStyle(node, ShouldHighlightNode(node));

            EditorGUI.BeginChangeCheck();
            string blockName = node.GetNodeName().ToUpper();
            blockName = GUILayout.TextField(node.GetNodeName(), nodeStyle, GUILayout.Width(nodeRect.width), GUILayout.Height(nodeRect.height)).ToUpper();
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(node.GetUnityObject(), "Node Name");

                node.SetNodeName(blockName);
            }
        }

        protected override void OnNodeNameEndEdit(INode node)
        {
            Flowchart flowchart = TargetFlowchart;
            Block block = node as Block;

            // Ensure block name is unique for this Flowchart
            string uniqueName = block.GetUniqueBlockName(flowchart.Blocks, block.GetName(), Block.DEFAULT_BLOCK_NAME);
            if (uniqueName != block.GetName())
            {
                block.SetName(uniqueName);
            }
        }

        protected override bool ShouldDrawSwitchConnectionButton(Connection connection)
        {
            Block block = connection.Node as Block;
            Block connectedBlock = connection.ConnectedNode as Block;

            Flowchart flowchart = block.GetFlowchart();
            Flowchart connectedFlowchart = (connectedBlock != null) ? connectedBlock.GetFlowchart() : null;

            return (base.ShouldDrawSwitchConnectionButton(connection) && !flowchart.GetTemplateFlowchartChain().Contains(connectedFlowchart));
        }

        protected override string GetConnectNodesButtonName(IConnectNodes connectNodes)
        {
            string name = "";

            Command command = connectNodes as Command;

            if (command != null)
            {
                name = command.GetSummary();

                if (string.IsNullOrEmpty(name))
                {
                    name = "[" + command.GetType().Name + "]";
                }
            }

            return name;
        }

        protected override GUIStyle GetNodeStyle(INode node, bool highlight)
        {
            Block block = node as Block;

            if (!m_nodeStyles.ContainsKey(node))
            {
                bool nodeHasError = GetNodeError(node);

                bool hasStartEventHandler = (block.GetEventHandler() != null && block.GetEventHandler().WillExecuteBlockOnFlowchartStart());

                // Count the number of unique connections (excluding self references)
                List<INode> uniqueConnectedNodeList = new List<INode>();
                foreach (Command command in block.GetBlockCommands())
                {
                    IConnectNodes connectNodes = command as IConnectNodes;
                    if (connectNodes != null && (connectNodes as UnityEngine.Object) != null)
                    {
                        SerializedObject serializedObject = new SerializedObject(command);
                        string[] switchPropertyPaths = connectNodes.GetSwitchNodePropertyPaths();
                        if (switchPropertyPaths != null)
                        {
                            foreach (string propertyPath in switchPropertyPaths)
                            {
                                INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                                if (connectedNode != null && connectedNode as Block != null && connectedNode as Block != block && !uniqueConnectedNodeList.Contains(connectedNode))
                                {
                                    uniqueConnectedNodeList.Add(connectedNode);
                                }
                            }
                        }
                    }
                }

                GUIStyle nodeStyle = null;

                if (block.GetEventHandler() != null)
                {
                    if (hasStartEventHandler)
                    {
                        if (uniqueConnectedNodeList.Count > 1)
                        {
                            if (nodeHasError)
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeHexRedOn : GraphWindowStyles.NodeHexRedOff;
                            }
                            else
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeHexOrangeOn : GraphWindowStyles.NodeHexOrangeOff;
                            }
                        }
                        else
                        {
                            if (nodeHasError)
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeSquareRedOn : GraphWindowStyles.NodeSquareRedOff;
                            }
                            else
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeSquareOrangeOn : GraphWindowStyles.NodeSquareOrangeOff;
                            }
                        }
                    }
                    else
                    {
                        if (uniqueConnectedNodeList.Count > 1)
                        {
                            if (nodeHasError)
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeHexRedOn : GraphWindowStyles.NodeHexRedOff;
                            }
                            else
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeHexBlueOn : GraphWindowStyles.NodeHexBlueOff;
                            }
                        }
                        else
                        {
                            if (nodeHasError)
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeSquareRedOn : GraphWindowStyles.NodeSquareRedOff;
                            }
                            else
                            {
                                nodeStyle = highlight ? GraphWindowStyles.NodeSquareBlueOn : GraphWindowStyles.NodeSquareBlueOff;
                            }
                        }
                    }
                }
                else
                {
                    if (uniqueConnectedNodeList.Count > 1)
                    {
                        if (nodeHasError)
                        {
                            nodeStyle = highlight ? GraphWindowStyles.NodeHexRedOn : GraphWindowStyles.NodeHexRedOff;
                        }
                        else
                        {
                            nodeStyle = highlight ? GraphWindowStyles.NodeHexYellowOn : GraphWindowStyles.NodeHexYellowOff;
                        }
                    }
                    else
                    {
                        if (nodeHasError)
                        {
                            nodeStyle = highlight ? GraphWindowStyles.NodeSquareRedOn : GraphWindowStyles.NodeSquareRedOff;
                        }
                        else
                        {
                            nodeStyle = highlight ? GraphWindowStyles.NodeSquareYellowOn : GraphWindowStyles.NodeSquareYellowOff;
                        }
                    }
                }

                m_nodeStyles[node] = nodeStyle;
            }

            return m_nodeStyles[node];
        }

        protected override bool GetNodeError(INode node)
        {
            Block block = node as Block;

            return block != null && block.ContainsError();
        }

        protected override Color GetNodeColor(INode node)
        {
            Block block = node as Block;

            Color color = base.GetNodeColor(node);

            if ((TargetFlowchart != block.GetFlowchart() && block.Scope == Scope.Private) ||
                (block.TemplateBlock != null && block.TemplateBlock.Scope == Scope.Private))
            {
                color.a = 0.3f;
            }

            return color;
        }

        public bool IsInheritedBlock(Block block)
        {
            Flowchart flowchart = TargetFlowchart;

            return (flowchart != block.GetFlowchart() || block.TemplateBlock != null);
        }

        protected override bool ShouldHighlightConnection(IConnectNodes[] connectNodesWithSameConnection, Connection connection)
        {
            foreach (IConnectNodes connectNodes in connectNodesWithSameConnection)
            {
                INode node = connection.Node;

                Command connectionCommand = connectNodes as Command;

                if (connectionCommand != null)
                {
                    bool nodeIsSelected = BlockIsSelected(node as Block);

                    bool commandIsSelected = (SelectedCommands.Contains(connectionCommand));

                    bool highlight = (nodeIsSelected && commandIsSelected) || (Application.isPlaying && connectionCommand.IsExecuting) || (!Application.isPlaying && (nodeIsSelected && SelectedCommands.Length == 0));

                    if (highlight)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected bool BlockIsSelected(Block block)
        {
            foreach (INode selectedNode in TargetGraph.GetSelectedNodes())
            {
                Block selectedBlock = selectedNode as Block;
                if (selectedBlock == block)
                {
                    return true;
                }
                if (selectedBlock.GetTemplateBlockChain().Contains(block))
                {
                    return true;
                }
            }

            return false;
        }

        public override void SetTarget(UnityEngine.Object target)
        {
            Variable variable = target as Variable;
            if (variable != null)
            {
                TargetFlowchart = variable.GetFlowchart();

                if (variable.ParentBlock == null)
                {
                    if (FlowchartEditor != null)
                    {
                        FlowchartEditor.IsVariablesListExpanded = true;
                        FlowchartEditor.TargetFlowchartVariableReorderableList.SetSelectedElements(new object[] { variable });
                    }
                }
                else
                {
                    if (BlockEditor != null)
                    {
                        BlockEditor.IsInputOutputExpanded = true;
                        BlockEditor.TargetBlockParameterReorderableList.SetSelectedElements(new object[] { variable });
                    }
                }
            }
        }
    }
}

#endif