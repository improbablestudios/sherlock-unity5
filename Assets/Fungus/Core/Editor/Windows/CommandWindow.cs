#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Fungus
{

    public class CommandWindow : FungusWindow
    {
        private static CommandWindow s_instance;

        protected Command m_targetCommand;

        protected Command[] m_targetCommands;

        protected CommandEditor m_commandEditor;

        protected Vector2 m_commandScrollPosition;
        
        public static CommandWindow Instance
        {
            get
            {
                return s_instance;
            }
        }

        public Command TargetCommand
        {
            get
            {
                if (!m_locked)
                {
                    Command targetCommand = null;

                    BlockWindow blockWindow = BlockWindow.Instance;
                    if (blockWindow != null)
                    {
                        BlockEditor blockEditor = blockWindow.BlockEditor;
                        if (blockEditor != null)
                        {
                            targetCommand = blockEditor.SelectedCommand;
                        }
                    }

                    m_targetCommand = targetCommand;
                }
                return m_targetCommand;
            }
            set
            {
                Command targetCommand = value;

                ShowBasicFungusWindows();

                BlockWindow blockWindow = BlockWindow.Instance;

                blockWindow.TargetBlock = targetCommand.ParentBlock;
                
                BlockEditor blockEditor = blockWindow.BlockEditor;
                if (blockEditor != null)
                {
                    blockEditor.SelectedCommand = targetCommand;
                }

                m_targetCommand = targetCommand;
            }
        }

        public Command[] TargetCommands
        {
            get
            {
                if (!m_locked)
                {
                    Command[] targetCommands = null;

                    BlockWindow blockWindow = BlockWindow.Instance;
                    if (blockWindow != null)
                    {
                        BlockEditor blockEditor = blockWindow.BlockEditor;
                        if (blockEditor != null)
                        {
                            targetCommands = blockEditor.SelectedCommands;
                        }
                    }

                    m_targetCommands = targetCommands;
                }
                return m_targetCommands;
            }
        }
        
        protected Command[] TargetCommandsOfSameType
        {
            get
            {
                Command targetCommand = TargetCommand as Command;
                List<Command> commandsOfSameType = new List<Command>();
                if (targetCommand != null)
                {
                    foreach (Command c in TargetCommands)
                    {
                        if (c != null)
                        {
                            if (c.GetType() == targetCommand.GetType())
                            {
                                commandsOfSameType.Add(c);
                            }
                        }
                    }
                    return commandsOfSameType.ToArray();
                }
                return null;
            }
        }

        public CommandEditor CommandEditor
        {
            get
            {
                if (TargetCommand != null)
                {
                    if (m_commandEditor != null &&
                       (m_commandEditor.target == null || !m_commandEditor.targets.ToList().SequenceEqual(TargetCommandsOfSameType)))
                    {
                        GUI.FocusControl("");
                        DestroyImmediate(m_commandEditor);
                        m_commandEditor = null;
                    }
                    if (m_commandEditor == null)
                    {
                        m_commandEditor = Editor.CreateEditor(TargetCommandsOfSameType) as CommandEditor;
                        if (m_commandEditor != null)
                        {
                            m_commandEditor.CommandWindow = this;
                        }
                    }
                    return m_commandEditor as CommandEditor;
                }
                return null;
            }
        }

        [MenuItem("Tools/Fungus/Command Window", false, 10009)]
        [MenuItem("Window/Fungus/Command", false, 10009)]
        private static void Init()
        {
            ShowWindow<CommandWindow>();
        }

        public override string GetWindowName()
        {
            if (TargetCommand != null)
            {
                return FlowchartItemInfoAttribute.GetInfo(TargetCommand.GetType()).GetName(TargetCommand.GetType());
            }

            return base.GetWindowName();
        }

        protected virtual void OnEnable()
        {
            s_instance = this;
        }

        protected virtual void OnInspectorUpdate()
        {
            Repaint();
        }

        protected override void OnGUI()
        {
            base.OnGUI();

            if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != null && FlowchartWindow.Instance.TargetFlowchart.gameObject.scene.IsValid())
            {
                if (BlockWindow.Instance != null && BlockWindow.Instance.TargetBlock != null && BlockWindow.Instance.TargetBlock.gameObject.scene.IsValid())
                {
                    if (TargetCommand == null)
                    {
                        DrawMessage("No command selected", MessageType.Info);
                        return;
                    }

                    if (CommandEditor == null)
                    {
                        return;
                    }

                    if (!TargetCommand.gameObject.scene.IsValid())
                    {
                        DrawMessageLabel("Open Prefab to begin editing", MessageType.Info);

                        if (DrawMessageButton("Open Prefab"))
                        {
                            AssetDatabase.OpenAsset(TargetCommand.gameObject);
                        }

                        return;
                    }

                    if (CommandEditor.CanEditMultipleObjects && TargetCommands.Length != TargetCommandsOfSameType.Length)
                    {
                        FungusWindow.DrawMessage("Multi-editing commands of different types is not supported", MessageType.Warning);
                        return;
                    }

                    if (!CommandEditor.CanEditMultipleObjects && TargetCommandsOfSameType.Length > 1)
                    {
                        FungusWindow.DrawMessage("Multi-editing this type of command is not supported", MessageType.Warning);
                        return;
                    }
                }
            }

            if (CommandEditor != null && CommandEditor.target != null)
            {
                using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(m_commandScrollPosition))
                {
                    m_commandScrollPosition = scrollView.scrollPosition;
                    using (new EditorGUILayout.VerticalScope(GUI.skin.box))
                    {
                        CommandEditor.OnFlowchartItemGUI();
                    }
                }
            }
        }

        public override void SetTarget(UnityEngine.Object target)
        {
            Command command = target as Command;
            if (command != null)
            {
                TargetCommand = command;
            }
        }
    }

}

#endif