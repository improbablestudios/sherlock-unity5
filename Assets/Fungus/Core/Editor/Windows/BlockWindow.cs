#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace Fungus
{

    public class BlockWindow : FungusWindow
    {
        private static BlockWindow s_instance;

        protected Block m_targetBlock;

        protected BlockEditor m_blockEditor;

        protected string m_newScriptTargetPath;

        protected Block m_blockToAddNewScriptTo;

        protected Command m_activeCommand;

        public static BlockWindow Instance
        {
            get
            {
                return s_instance;
            }
        }

        public Block TargetBlock
        {
            get
            {
                if (!m_locked)
                {
                    Block targetBlock = null;

                    if (FlowchartWindow.Instance != null)
                    {
                        Flowchart targetFlowchart = FlowchartWindow.Instance.TargetFlowchart;
                        if (targetFlowchart != null)
                        {
                            targetBlock = targetFlowchart.GetFirstSelectedBlock();

                            if (targetBlock == null)
                            {
                                foreach (Flowchart templateFlowchart in targetFlowchart.GetTemplateFlowchartChain())
                                {
                                    Block templateBlock = templateFlowchart.GetFirstSelectedBlock();

                                    if (templateBlock != null)
                                    {
                                        targetBlock = templateBlock;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    m_targetBlock = targetBlock;
                }
                return m_targetBlock;
            }
            set
            {
                Block targetBlock = value;

                if (targetBlock != null)
                {
                    ShowBasicFungusWindows();

                    Flowchart targetFlowchart = targetBlock.GetFlowchart();
                    if (FlowchartWindow.Instance != null)
                    {
                        FlowchartWindow.Instance.TargetFlowchart = targetFlowchart;
                    }

                    if (targetFlowchart != null)
                    {
                        targetFlowchart.SetSelectedBlock(targetBlock);
                    }
                }

                m_targetBlock = targetBlock;
            }
        }

        public BlockEditor BlockEditor
        {
            get
            {
                if (TargetBlock != null)
                {
                    if (m_blockEditor != null &&
                       (m_blockEditor.target == null || m_blockEditor.target != TargetBlock))
                    {
                        GUI.FocusControl("");
                        UnityEngine.Object.DestroyImmediate(m_blockEditor);
                        m_blockEditor = null;
                    }
                    if (m_blockEditor == null)
                    {
                        m_blockEditor = Editor.CreateEditor(TargetBlock) as BlockEditor;
                        if (m_blockEditor != null)
                        {
                            m_blockEditor.BlockWindow = this;
                        }
                    }

                    return m_blockEditor as BlockEditor;
                }
                return null;
            }
        }
        
        public string NewScriptTargetPath
        {
            get
            {
                return m_newScriptTargetPath;
            }
            set
            {
                m_newScriptTargetPath = value;
            }
        }

        public Block BlockToAddNewScriptTo
        {
            get
            {
                return m_blockToAddNewScriptTo;
            }
            set
            {
                m_blockToAddNewScriptTo = value;
            }
        }

        public Command ActiveCommand
        {
            get
            {
                return m_activeCommand;
            }
            set
            {
                m_activeCommand = value;
            }
        }

        [MenuItem("Tools/Fungus/Block Window", false, 10009)]
        [MenuItem("Window/Fungus/Block", false, 10009)]
        private static void Init()
        {
            ShowWindow<BlockWindow>();
        }

        public override string GetWindowName()
        {
            if (TargetBlock != null)
            {
                return TargetBlock.BlockName;
            }

            return base.GetWindowName();
        }

        protected virtual void OnEnable()
        {
            s_instance = this;
        }

        protected virtual void Update()
        {
            if (BlockEditor != null)
            {
                if (!string.IsNullOrEmpty(m_newScriptTargetPath))
                {
                   MonoScript monoScript = AssetDatabase.LoadAssetAtPath(m_newScriptTargetPath, typeof(MonoScript)) as MonoScript;
                   if (monoScript != null && monoScript.GetClass() != null)
                   {
                        if (m_blockToAddNewScriptTo != null)
                        {
                            Type scriptType = monoScript.GetClass();
                            if (typeof(Command).IsAssignableFrom(scriptType))
                            {
                                int index = BlockEditor.TargetBlockCommandReorderableList.GetDefaultCreateIndex();
                                m_blockToAddNewScriptTo.AddCommand(scriptType, index);
                                m_blockToAddNewScriptTo.GetFlowchart().SetSelectedBlock(m_blockToAddNewScriptTo);
                                BlockEditor.TargetBlockSelectedCommandIndices = new int[] { index };
                            }
                            else if (typeof(EventHandler).IsAssignableFrom(scriptType))
                            {
                                m_blockToAddNewScriptTo.AddEventHandler(scriptType);
                                m_blockToAddNewScriptTo.GetFlowchart().SetSelectedBlock(m_blockToAddNewScriptTo);
                            }
                            m_blockToAddNewScriptTo = null;
                        }
                    }
                    m_newScriptTargetPath = null;
                }
            }
        }

        protected virtual void OnInspectorUpdate()
        {
            Repaint();
        }

        protected override void OnGUI()
        {
            base.OnGUI();

            if (FlowchartWindow.Instance != null && FlowchartWindow.Instance.TargetFlowchart != null && FlowchartWindow.Instance.TargetFlowchart.gameObject.scene.IsValid())
            {
                if (TargetBlock == null)
                {
                    DrawMessage("No block selected", MessageType.Info);
                    return;
                }
                if (!TargetBlock.gameObject.scene.IsValid())
                {
                    DrawMessageLabel("Open Prefab to begin editing", MessageType.Info);

                    if (DrawMessageButton("Open Prefab"))
                    {
                        AssetDatabase.OpenAsset(TargetBlock.gameObject);
                    }

                    return;
                }
                if (FlowchartWindow.Instance.TargetFlowchart.GetSelectedBlocks().Count > 1)
                {
                    DrawMessage("Multi-editing blocks is not supported", MessageType.Warning);
                    return;
                }
            }

            if (BlockEditor != null && BlockEditor.target != null)
            {
                BlockEditor.OnBlockGUI();
            }
        }
        
        public override void SetTarget(UnityEngine.Object target)
        {
            Block block = target as Block;
            if (block != null)
            {
                TargetBlock = block;
            }

            EventHandler eventHandler = target as EventHandler;
            if (eventHandler != null)
            {
                TargetBlock = eventHandler.ParentBlock;
                if (BlockEditor != null)
                {
                    BlockEditor.IsEventHandlerExpanded = true;
                }
            }

            Command command = target as Command;
            if (command != null)
            {
                TargetBlock = command.ParentBlock;
                if (BlockEditor != null)
                {
                    BlockEditor.IsCommandsExpanded = true;
                }
            }
        }
    }

}

#endif