#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
using UnityEditor.SceneManagement;

namespace Fungus
{
    public class AppFlowWindow : GraphWindow
    {
        [MenuItem("Tools/Fungus/App Flow Window", false, 10007)]
        [MenuItem("Window/Fungus/App Flow", false, 10007)]
        private static void Init()
        {
            ShowWindow<AppFlowWindow>();
        }

        public override string GetWindowName()
        {
            return GetGraphTitle();
        }

        public override Graph GetGraph()
        {
            return Flowchart.AppFlowGraph;
        }

        public override string GetGraphTitle()
        {
            return "App Flow";
        }

        public static AppFlowWindow Instance
        {
            get;
            private set;
        }
        
        protected override bool CanCreateNodes
        {
            get
            {
                return true;
            }
        }

        protected BlockEditor BlockEditor
        {
            get
            {
                BlockWindow blockWindow = BlockWindow.Instance;
                if (blockWindow != null)
                {
                    return blockWindow.BlockEditor;
                }
                return null;
            }
        }

        protected List<Flowchart> m_previousFlowcharts;

        protected Dictionary<INode, List<KeyValuePair<INode, Command>>> m_incomingConnectionButtonMap = new Dictionary<INode, List<KeyValuePair<INode, Command>>>();

        protected const int FLOWCHART_NODE_HEIGHT = 20;

        protected BlockEditor m_previousBlockEditor;

        protected override void OnEnable()
        {
            base.OnEnable();

            Instance = this;
            
            EditorApplication.hierarchyChanged += OnHierarchyWindowChanged;

            EditorApplication.delayCall += Setup;

            ResourceTracker.AddedObjectToCache += ObjectAddedOrRemovedFromCache;
            
            ResourceTracker.RemovedObjectFromCache += ObjectAddedOrRemovedFromCache;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            EditorApplication.hierarchyChanged -= OnHierarchyWindowChanged;
            
            ResourceTracker.AddedObjectToCache -= ObjectAddedOrRemovedFromCache;

            ResourceTracker.RemovedObjectFromCache -= ObjectAddedOrRemovedFromCache;
        }

        protected virtual void ObjectAddedOrRemovedFromCache(UnityEngine.Object obj)
        {
            if (obj is Flowchart)
            {
                Setup();
            }
        }

        protected void OnHierarchyWindowChanged()
        {
            PopulateGraph();
        }

        protected void OnSelectionChange()
        {
            Graph targetGraph = GetGraph();
            if (targetGraph != null)
            {
                INode[] nodes = targetGraph.GetNodesInGameObjectSelection();
                if (nodes.Length > 0)
                {
                    if (!(new HashSet<INode>(targetGraph.GetSelectedNodes())).SetEquals(new HashSet<INode>(nodes)))
                    {
                        targetGraph.SelectNodes(nodes, Graph.SelectionType.Normal);
                        CenterViewOnNodes(targetGraph.GetSelectedNodes(), true);
                    }
                }
            }

            RefreshWindow();
        }
        
        protected override void OnSelectedNodesChanged()
        {
            Graph targetGraph = GetGraph();
            if (targetGraph != null)
            {
                GameObject[] gameObjects = targetGraph.GetGameObjectsInNodeSelection();
                if (gameObjects.Length > 0)
                {
                    if (!(new HashSet<GameObject>(Selection.gameObjects)).SetEquals(new HashSet<GameObject>(gameObjects)))
                    {
                        List<UnityEngine.Object> selectedObjs = UnityEditor.Selection.objects.Where(i => !targetGraph.ObjectIsNodeInThisGraph(i)).ToList();
                        selectedObjs.AddRange(gameObjects);
                        UnityEditor.Selection.objects = selectedObjs.ToArray();
                    }
                }
            }

            base.OnSelectedNodesChanged();
        }
        
        protected virtual void Setup()
        {
            PopulateGraph();
            RefreshWindow();
        }

        protected override void PopulateGraphNodes()
        {
            List<Flowchart> flowcharts = GetFlowcharts();

            Graph targetGraph = GetGraph();
            targetGraph.m_Nodes.Clear();
            targetGraph.m_Nodes.AddRange(flowcharts.Cast<INode>().Where(i => i.GetUnityObject() != null));

            if (m_previousFlowcharts != null || flowcharts != null || !m_previousFlowcharts.SequenceEqual(flowcharts))
            {
                OnDisplayedFlowchartsChanged(m_previousFlowcharts, flowcharts);
            }
            m_previousFlowcharts = flowcharts;
        }

        public List<Flowchart> GetFlowcharts()
        {
            List<Flowchart> flowcharts = new List<Flowchart>();

            List<Flowchart> flowchartsInScene = ResourceTracker.FindLoadedInstanceObjectsOfType<Flowchart>().Where(x => !EditorSceneManager.IsPreviewScene(x.gameObject.scene)).ToList();
            List<Flowchart> flowchartsInAssets = ResourceTracker.GetCachedPrefabObjectsOfType<Flowchart>().ToList();
            List<Flowchart> flowchartsInAssetsAndNotInScene = flowchartsInAssets.Where(x => !flowchartsInScene.Any(s => s.Key == x.Key)).ToList();

            flowcharts.AddRange(flowchartsInScene);
            flowcharts.AddRange(flowchartsInAssetsAndNotInScene);
            flowcharts = flowcharts.Where(x => ResourceTracker.IsObjectSavedInBuild(x)).ToList();
            
            return flowcharts;
        }

        protected void OnDisplayedFlowchartsChanged(List<Flowchart> oldFlowcharts, List<Flowchart> newFlowcharts)
        {
            if (oldFlowcharts != null)
            {
                foreach (Flowchart oldFlowchart in oldFlowcharts)
                {
                    if (oldFlowchart != null)
                    {
                        oldFlowchart.PropertyChanged -= RefreshWindow;
                        oldFlowchart.Graph.SelectedNodesChanged -= RefreshWindow;
                    }
                }
            }
            if (newFlowcharts != null)
            {
                foreach (Flowchart newFlowchart in newFlowcharts)
                {
                    if (newFlowchart != null)
                    {
                        newFlowchart.PropertyChanged += RefreshWindow;
                        newFlowchart.Graph.SelectedNodesChanged += RefreshWindow;
                    }
                }
            }
            RefreshWindow();
        }
        
        protected override void RefreshNodeConnections()
        {
            base.RefreshNodeConnections();

            m_incomingConnectionButtonMap.Clear();

            if (TargetGraph != null)
            {
                foreach (INode node in TargetGraph.m_Nodes)
                {
                    Flowchart flowchart = node as Flowchart;

                    if (flowchart == null)
                    {
                        continue;
                    }

                    if (!m_incomingConnectionButtonMap.ContainsKey(node))
                    {
                        m_incomingConnectionButtonMap.Add(node, new List<KeyValuePair<INode, Command>>());
                    }
                }

                foreach (INode node in TargetGraph.m_Nodes)
                {
                    Flowchart flowchart = node as Flowchart;

                    if (flowchart == null)
                    {
                        continue;
                    }

                    foreach (IConnectNodes connectNodes in GetNodeConnections(node))
                    {
                        if (connectNodes != null)
                        {
                            Command command = connectNodes as Command;
                            if (command != null)
                            {
                                SerializedObject serializedObject = new SerializedObject(command);
                                string[] switchPropertyPaths = connectNodes.GetSwitchNodePropertyPaths();
                                if (switchPropertyPaths != null)
                                {
                                    foreach (string propertyPath in switchPropertyPaths)
                                    {
                                        INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                                        Connection connection = new Connection(node, connectedNode, connectNodes.GetDescription());
                                        if (ShouldDrawConnection(connection))
                                        {
                                            if (ShouldDrawSwitchConnectionButton(connection))
                                            {
                                                Flowchart connectedFlowchart = connection.ConnectedNode as Flowchart;

                                                if (connectedFlowchart != null)
                                                {
                                                    if (m_incomingConnectionButtonMap.ContainsKey(connectedFlowchart))
                                                    {
                                                        List<KeyValuePair<INode, Command>> incomingConnections = m_incomingConnectionButtonMap[connectedFlowchart];
                                                        if (incomingConnections.Where(i => i.Key == flowchart as INode).Count() == 0)
                                                        {
                                                            incomingConnections.Add(new KeyValuePair<INode, Command>(flowchart, command));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                string[] activatePropertyPaths = connectNodes.GetActivateNodePropertyPaths();
                                if (activatePropertyPaths != null)
                                {
                                    foreach (string propertyPath in activatePropertyPaths)
                                    {
                                        INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                                        Connection connection = new Connection(node, connectedNode, connectNodes.GetDescription());
                                        if (ShouldDrawConnection(connection))
                                        {
                                            Flowchart connectedFlowchart = connection.ConnectedNode as Flowchart;

                                            if (connectedFlowchart != null)
                                            {
                                                if (m_incomingConnectionButtonMap.ContainsKey(connectedFlowchart))
                                                {
                                                    List<KeyValuePair<INode, Command>> incomingConnections = m_incomingConnectionButtonMap[connectedFlowchart];
                                                    if (incomingConnections.Where(i => i.Key == flowchart as INode).Count() == 0)
                                                    {
                                                        incomingConnections.Add(new KeyValuePair<INode, Command>(flowchart, command));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void OnGUI()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode && !Application.isPlaying)
            {
                return;
            }
            
            BlockEditor blockEditor = BlockEditor;
            if (m_previousBlockEditor != blockEditor)
            {
                OnBlockEditorChanged(m_previousBlockEditor, blockEditor);
            }
            m_previousBlockEditor = blockEditor;

            base.OnGUI();
        }

        protected void OnBlockEditorChanged(BlockEditor oldBlockEditor, BlockEditor newBlockEditor)
        {
            if (oldBlockEditor != null)
            {
                oldBlockEditor.OnSelectedCommandsChangedCallback -= RefreshWindow;
            }
            if (newBlockEditor != null)
            {
                newBlockEditor.OnSelectedCommandsChangedCallback += RefreshWindow;
            }
        }

        protected override string NodeTypeName()
        {
            return typeof(Flowchart).Name;
        }

        protected override GUIContent NodeTopLabel(INode node)
        {
            string label = "";

            if (m_incomingConnectionButtonMap.ContainsKey(node) && m_incomingConnectionButtonMap[node].Count > 0)
            {
                foreach (KeyValuePair<INode, Command> incomingConnection in m_incomingConnectionButtonMap[node])
                {
                    Flowchart incomingFlowchart = incomingConnection.Key as Flowchart;
                    Command connectionCommand = incomingConnection.Value;

                    if (incomingFlowchart != null && connectionCommand != null)
                    {
                        if (!string.IsNullOrEmpty(label))
                        {
                            label += "\n";
                        }
                        label += "<" + incomingFlowchart.name + ">";
                    }
                }
            }

            return new GUIContent(label);
        }

        private string GetFlowchartNodeName(Command command)
        {
            string flowchartName = command.GetSummary();
            if (string.IsNullOrEmpty(flowchartName))
            {
                flowchartName = "[" + command.GetType().Name + "]";
            }
            return flowchartName;
        }

        protected override string GetConnectNodesButtonName(IConnectNodes connectNodes)
        {
            string name = "";

            Command command = connectNodes as Command;

            if (command != null)
            {
                name = command.GetSummary();

                if (string.IsNullOrEmpty(name))
                {
                    name = "[" + command.GetType().Name + "]";
                }
            }

            return name;
        }
        
        protected override bool ShouldHighlightConnection(IConnectNodes[] connectNodesWithSameConnection, Connection connection)
        {
            foreach (IConnectNodes connectNodes in connectNodesWithSameConnection)
            {
                INode node = connection.Node;
                Flowchart flowchart = node as Flowchart;

                Command connectionCommand = connectNodes as Command;

                if (connectionCommand != null)
                {
                    bool nodeIsSelected = (TargetGraph.IsNodeSelected(node));

                    foreach (Block block in flowchart.GetSelectedBlocks())
                    {
                        block.SetExecutionInfo();
                    }

                    bool commandBlockIsSelected = (flowchart.GetSelectedBlocks().Contains(connectionCommand.ParentBlock));

                    Command[] selectedCommands = new Command[0];
                    bool commandIsSelected = true;
                    if (BlockWindow.Instance != null && BlockWindow.Instance.BlockEditor != null)
                    {
                        selectedCommands = BlockWindow.Instance.BlockEditor.SelectedCommands;
                        commandIsSelected = (selectedCommands.Contains(connectionCommand));
                    }

                    bool highlight = (nodeIsSelected && commandBlockIsSelected && (selectedCommands.Length == 0 || commandIsSelected)) || (Application.isPlaying && connectionCommand.IsExecuting) || (!Application.isPlaying && (nodeIsSelected && flowchart.GetSelectedBlocks().Count == 0));

                    if (highlight)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        public virtual Flowchart CreateFlowchart(Vector2 position, string flowchartName)
        {
            GameObject newFlowchartGameObject = new GameObject(flowchartName, new Type[] { typeof(Flowchart) });
            Flowchart newFlowchart = newFlowchartGameObject.GetComponent<Flowchart>();

            newFlowchart.SetNodePosition(position);

            EditorUtility.SetDirty(newFlowchart);

            ResourceTracker.AddToCache(newFlowchart);

            return newFlowchart;
        }

        protected virtual void CreateNodeInSceneMenuFunction(object obj)
        {
            Vector2 newPosition = (Vector2)obj;

            Flowchart newFlowchart = CreateFlowchart(newPosition, "~Flowchart");
            Undo.RegisterCreatedObjectUndo(newFlowchart.gameObject, "Create Flowchart");
            newFlowchart.PingFlowchart();

            OnCreateNode(newFlowchart);

            OnNodeAddedToGraph(newFlowchart);

            PopulateGraph();
            Repaint();
        }

        protected virtual void CreateNodeInAssetsMenuFunction(object obj)
        {
            Vector2 newPosition = (Vector2)obj;

            string path = EditorUtility.SaveFilePanelInProject("Save Flowchart",
                                                               "~Flowchart" + ".prefab",
                                                               "prefab",
                                                               "Select where the flowchart should be saved",
                                                               "Assets");

            if (path.Length == 0)
            {
                return;
            }

            string flowchartName = Path.GetFileNameWithoutExtension(path);

            Flowchart newFlowchart = CreateFlowchart(newPosition, flowchartName);
            GameObject newFlowchartGameObjectPrefab = PrefabUtility.SaveAsPrefabAsset(newFlowchart.gameObject, path);
            Undo.DestroyObjectImmediate(newFlowchart.gameObject);
            newFlowchartGameObjectPrefab.GetComponent<Flowchart>().PingFlowchart();
            
            OnCreateNode(newFlowchart);

            OnNodeAddedToGraph(newFlowchart);

            PopulateGraph();
            Repaint();
        }

        public void DeleteFlowchart(Flowchart flowchart)
        {
            OnDeleteNode(flowchart);

            ResourceTracker.RemoveFromCache(flowchart);

            if (UnityEditor.PrefabUtility.IsPartOfPrefabAsset(flowchart) || UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(flowchart.gameObject) != null)
            {
                string flowchartAssetPath = AssetDatabase.GetAssetPath(flowchart.gameObject);
                AssetDatabase.DeleteAsset(flowchartAssetPath);
            }
            else
            {
                Undo.DestroyObjectImmediate(flowchart.gameObject);
            }

            PopulateGraph();
            Repaint();
        }

        public override void DeleteNode(INode node)
        {
            Flowchart flowchart = node as Flowchart;

            if (flowchart != null)
            {
                if (UnityEditor.PrefabUtility.IsPartOfPrefabAsset(flowchart) || UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(flowchart.gameObject) != null)
                {
                    string flowchartAssetPath = AssetDatabase.GetAssetPath(flowchart.gameObject);

                    if (EditorUtility.DisplayDialog("Delete selected flowchart?",
                    flowchartAssetPath + "\n\n" + "You cannot undo this action.", "Delete", "Cancel"))
                    {
                        DeleteFlowchart(flowchart);
                    }
                }
                else
                {
                    DeleteFlowchart(flowchart);
                }
            }
        }

        protected override GUIStyle GetNodeStyle(INode node, bool highlight)
        {
            if (!m_nodeStyles.ContainsKey(node))
            {
                Flowchart flowchart = node as Flowchart;

                bool nodeHasError = GetNodeError(node);

                // Count the number of unique connections (excluding self references)
                List<INode> uniqueConnectedNodeList = new List<INode>();
                foreach (Block block in flowchart.Blocks)
                {
                    if (block != null)
                    {
                        foreach (Command command in block.GetBlockCommands())
                        {
                            IConnectNodes connectNodes = command as IConnectNodes;
                            if (connectNodes != null && (connectNodes as UnityEngine.Object) != null)
                            {
                                SerializedObject serializedObject = new SerializedObject(command);
                                string[] switchPropertyPaths = connectNodes.GetSwitchNodePropertyPaths();
                                if (switchPropertyPaths != null)
                                {
                                    foreach (string propertyPath in switchPropertyPaths)
                                    {
                                        INode connectedNode = serializedObject.FindProperty(propertyPath).objectReferenceValue as INode;
                                        if (connectedNode != null && connectedNode as Flowchart != null && connectedNode as Flowchart != block && !uniqueConnectedNodeList.Contains(connectedNode))
                                        {
                                            uniqueConnectedNodeList.Add(connectedNode);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                GUIStyle nodeStyle = null;

                if (uniqueConnectedNodeList.Count > 1)
                {
                    if (nodeHasError)
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeHexRedOn : GraphWindowStyles.NodeHexRedOff;
                    }
                    else if (flowchart.gameObject.activeSelf)
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeHexPurpleOn : GraphWindowStyles.NodeHexPurpleOff;
                    }
                    else
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeHexGreyOn : GraphWindowStyles.NodeHexGreyOff;
                    }
                }
                else
                {
                    if (nodeHasError)
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeSquareRedOn : GraphWindowStyles.NodeSquareRedOff;
                    }
                    else if (flowchart.gameObject.activeSelf)
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeSquarePurpleOn : GraphWindowStyles.NodeSquarePurpleOff;
                    }
                    else
                    {
                        nodeStyle = highlight ? GraphWindowStyles.NodeSquareGreyOn : GraphWindowStyles.NodeSquareGreyOff;
                    }
                }
                
                m_nodeStyles[node] = nodeStyle;
            }

            return m_nodeStyles[node];
        }

        protected override bool GetNodeError(INode node)
        {
            Flowchart flowchart = node as Flowchart;

            return flowchart.ContainsError();
        }
        
        protected void AddCreateNodeContextMenuItems(GenericMenu menu, Vector2 newNodePosition)
        {
            menu.AddItem(new GUIContent("Create In Scene"), false, CreateNodeInSceneMenuFunction, newNodePosition);
            menu.AddItem(new GUIContent("Create In Assets"), false, CreateNodeInAssetsMenuFunction, newNodePosition);
        }

        protected override void OnLeftClickNode(INode node)
        {
            base.OnLeftClickNode(node);

            if (TargetGraph != null && TargetGraph.GetSelectedNodes().Length == 1)
            {
                if (!Application.isPlaying)
                {
                    GameObject flowchartGameObject = node.GetUnityObject().GetGameObject();
                    if (flowchartGameObject != null)
                    {
                        EditorGUIUtility.PingObject(flowchartGameObject);
                        Selection.activeGameObject = flowchartGameObject;
                    }
                }
            }
        }

        protected override void OnRightClickNode(INode node)
        {
            base.OnRightClickNode(node);
            
            if (TargetGraph != null && TargetGraph.GetSelectedNodes().Length == 1)
            {
                if (!Application.isPlaying)
                {
                    GameObject flowchartGameObject = node.GetUnityObject().GetGameObject();
                    if (flowchartGameObject != null)
                    {
                        EditorGUIUtility.PingObject(flowchartGameObject);
                        Selection.activeGameObject = flowchartGameObject;
                    }
                }
            }
        }

        protected override void OnContextClickGrid()
        {
            if (CanCreateNodes)
            {
                GenericMenu menu = new GenericMenu();

                AddCreateNodeContextMenuItems(menu, GetNewNodePositionAtMouse());

                menu.ShowAsContext();
            }
        }

        protected override void OnClickAddButton()
        {
            GenericMenu menu = new GenericMenu();

            AddCreateNodeContextMenuItems(menu, GetNewNodePositionInGraph());

            menu.ShowAsContext();
        }

        protected override void OnNodeMoveEnd(INode node)
        {
            Flowchart flowchart = node as Flowchart;

            if (flowchart.IsPartOfPrefabAsset())
            {
                flowchart.RecordChanges();
            }
        }

        protected override void OnNodeNameEndEdit(INode node)
        {
            Flowchart flowchart = node as Flowchart;

            if (flowchart.IsPartOfPrefabAsset())
            {
                string flowchartAssetPath = AssetDatabase.GetAssetPath(flowchart.gameObject);
                AssetDatabase.RenameAsset(flowchartAssetPath, node.GetNodeName());
            }
        }
    }
}

#endif