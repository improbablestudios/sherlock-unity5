#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    public class EventHandlerTypeSearchPopupWindow : FlowchartItemTypeSearchPopupWindow<EventHandler>
    {
        protected class NewEventHandlerTypeElement : NewFlowchartItemElement<EventHandler>
        {
            public NewEventHandlerTypeElement() : base()
            {
            }

            protected override MonoScript CreateScriptAssetFromTemplate(string targetPath, string templatePath)
            {
                BlockWindow.Instance.NewScriptTargetPath = targetPath;
                BlockWindow.Instance.BlockToAddNewScriptTo = BlockWindow.Instance.TargetBlock;

                return base.CreateScriptAssetFromTemplate(targetPath, templatePath);
            }
        }

        public new static EventHandlerTypeSearchPopupWindow Show(int controlID, Rect rect, SearchPopupWindowFlags flags = 0)
        {
            return Show<EventHandlerTypeSearchPopupWindow>(controlID, rect, null, null, null, flags);
        }

        public new static EventHandlerTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, SearchPopupWindowFlags flags = 0)
        {
            return Show<EventHandlerTypeSearchPopupWindow>(controlID, rect, selectedType, null, null, flags);
        }

        public static EventHandlerTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<EventHandlerTypeSearchPopupWindow>(controlID, rect, selectedType, null, types, flags);
        }

        public new static EventHandlerTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<EventHandlerTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, null, flags);
        }

        public static EventHandlerTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<EventHandlerTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, types, flags);
        }

        protected override NewItemElement GetNewItemElement()
        {
            return new NewEventHandlerTypeElement();
        }
    }

}
#endif