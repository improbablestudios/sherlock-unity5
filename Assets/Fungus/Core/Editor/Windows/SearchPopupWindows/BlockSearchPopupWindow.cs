#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections.Generic;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus
{

    public class BlockSearchPopupWindow : UnityObjectReferenceSearchPopupWindow
    {
        protected class NewBlockElement : NewUnityObjectReferenceElement
        {
            protected override void DrawControls()
            {
                DrawNameControl();

                DrawTypeControl();
            }

            protected override void DrawNameControl()
            {
                GUILayout.Label("Name");
                GUI.SetNextControlName("NewName");
                EditorGUI.BeginChangeCheck();
                string name = NewItemName.ToUpper();
                name = GUILayout.TextField(name).ToUpper();
                if (EditorGUI.EndChangeCheck())
                {
                    NewItemName = name;
                }

                if (GUI.GetNameOfFocusedControl() != "NewName")
                {
                    EditorGUI.FocusTextInControl("NewName");
                }
            }

            protected override object CreateNewItem()
            {
                FlowchartItem flowchartItem = s_targetObject as FlowchartItem;
                Flowchart flowchart = flowchartItem.GetFlowchart();
                
                Block block = flowchart.AddBlock() as Block;
                block.SetName(NewItemName);

                return block;
            }
        }

        protected static bool s_ignoreScope;

        public static BlockSearchPopupWindow Show(int controlID, Rect rect, SerializedProperty property, GUIContent defaultOptionLabel, bool ignoreScope, SearchPopupWindowFlags flags = 0)
        {
            return Show(controlID, rect, property.objectReferenceValue as Block, defaultOptionLabel, ignoreScope, property.serializedObject.targetObject, flags);
        }

        public static BlockSearchPopupWindow Show(int controlID, Rect rect, Block block, GUIContent defaultOptionLabel, bool ignoreScope, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            s_targetObject = targetObject;
            s_objects = null;
            s_ignoreScope = ignoreScope;

            return Show<BlockSearchPopupWindow>(controlID, rect, block, defaultOptionLabel, typeof(Block), null, targetObject, flags);
        }

        protected override NewItemElement GetNewItemElement()
        {
            return new NewBlockElement();
        }

        public static List<UnityEngine.Object> GetSelfBlockList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null, bool ignoreScope = false)
        {
            if (!ignoreScope)
            {
                FlowchartItem flowchartItem = targetObject as FlowchartItem;
                Flowchart flowchart = flowchartItem.GetFlowchart();
                return GetSelfObjectList(targetObject, baseType, componentTypes).Where(i => (i as Block).GetFlowchart() == flowchart || (i as Block).Scope == Scope.Public).ToList();
            }
            return GetSelfObjectList(targetObject, baseType, componentTypes);
        }

        public static List<UnityEngine.Object> GetSceneBlockList(Type baseType, Type[] componentTypes, bool ignoreScope)
        {
            if (!ignoreScope)
            {
                return GetSceneObjectList(baseType, componentTypes).Where(i => (i as Block).Scope == Scope.Public).ToList();
            }
            return GetSceneObjectList(baseType, componentTypes);
        }

        public static List<UnityEngine.Object> GetAssetBlockList(Type baseType, Type[] componentTypes, bool ignoreScope)
        {
            if (!ignoreScope)
            {
                return GetAssetObjectList(baseType, componentTypes).Where(i => (i as Block).Scope == Scope.Public).ToList();
            }
            return GetAssetObjectList(baseType, componentTypes);
        }
        
        public static List<UnityEngine.Object> GetBlockList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null, bool ignoreScope = false)
        {
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfBlockList(baseType, componentTypes, targetObject, ignoreScope).Distinct().Where(i => i != null).ToList();
                if (!UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneBlockList(baseType, componentTypes, ignoreScope).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneBlockList(baseType, componentTypes, ignoreScope).Distinct().Where(i => i != null).ToList();
            }
            List<UnityEngine.Object> assetObjectList = GetAssetBlockList(baseType, componentTypes, ignoreScope).Distinct().Where(i => i != null).ToList();

            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            objectList.AddRange(selfObjectList);
            objectList.AddRange(sceneObjectList);
            objectList.AddRange(assetObjectList);

            return objectList;
        }
        
        protected override Dictionary<string, List<UnityEngine.Object>> GetObjectDictionary(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfBlockList(baseType, componentTypes, targetObject, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                if (!UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneBlockList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneBlockList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
            }
            List<UnityEngine.Object> assetObjectList = GetAssetBlockList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();

            return new Dictionary<string, List<UnityEngine.Object>>
            {
                {
                    "Self", selfObjectList
                },
                {
                    "Scene", sceneObjectList
                },
                {
                    "Assets", assetObjectList
                },
            };
        }
    }

}

#endif