#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections.Generic;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus
{

    public class VariableSearchPopupWindow : UnityObjectReferenceSearchPopupWindow
    {
        protected class NewVariableElement : NewUnityObjectReferenceElement
        {
            protected override void DrawControls()
            {
                DrawNameControl();

                DrawTypeControl();
            }

            protected override void DrawTypeControl()
            {
                if (s_valueTypes.Length > 1)
                {
                    GUILayout.Label("Type");
                    GUI.SetNextControlName("NewType");
                    NewItemType = FungusGUI.VariableValueTypeField(EditorGUILayout.GetControlRect(), NewItemType, GUIContent.none, s_valueTypes);
                }
                else
                {
                    NewItemType = s_valueTypes[0];
                }
            }

            protected override object CreateNewItem()
            {
                FlowchartItem flowchartItem = s_targetObject as FlowchartItem;
                Flowchart flowchart = flowchartItem.GetFlowchart();

                int index = FlowchartWindow.Instance.FlowchartEditor.TargetFlowchartInsertVariableIndex;
                Variable variable = flowchart.AddVariable(Variable.GetVariableType(NewItemType), index);
                variable.SetName(variable.GetUniqueVariableName(flowchart.Variables, "", Flowchart.DEFAULT_FLOWCHART_VARIABLE_NAME));

                FlowchartWindow.Instance.FlowchartEditor.TargetFlowchartSelectedVariableIndices = new int[] { index };

                return variable;
            }
        }

        protected static bool s_ignoreScope;

        protected static Type[] s_valueTypes;

        public static VariableSearchPopupWindow Show(int controlID, Rect rect, SerializedProperty property, GUIContent defaultOptionLabel, Type[] valueTypes, bool ignoreScope, SearchPopupWindowFlags flags = 0)
        {
            return Show(controlID, rect, property.objectReferenceValue as Variable, defaultOptionLabel, valueTypes, ignoreScope, property.serializedObject.targetObject, flags);
        }

        public static VariableSearchPopupWindow Show(int controlID, Rect rect, Variable variable, GUIContent defaultOptionLabel, Type[] valueTypes, bool ignoreScope, UnityEngine.Object targetObject, SearchPopupWindowFlags flags = 0)
        {
            s_targetObject = targetObject;
            s_objects = null;
            s_ignoreScope = ignoreScope;
            s_valueTypes = valueTypes;

            return Show<VariableSearchPopupWindow>(controlID, rect, variable, defaultOptionLabel, typeof(Variable), null, targetObject, flags);
        }

        protected override NewItemElement GetNewItemElement()
        {
            if (s_valueTypes.Length > 1 || (s_valueTypes.Length == 1 && s_valueTypes[0] != typeof(object)))
            {
                return new NewVariableElement();
            }
            return null;
        }

        public static List<UnityEngine.Object> GetParameterVariableList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            FlowchartItem flowchartItem = targetObject as FlowchartItem;
            Command command = flowchartItem as Command;
            return GetSelfObjectList(targetObject, baseType, componentTypes).Where(i => (command != null && (i as Variable).ParentBlock == command.ParentBlock)).ToList();
        }

        public static List<UnityEngine.Object> GetSelfVariableList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null, bool ignoreScope = false)
        {
            if (!ignoreScope)
            {
                FlowchartItem flowchartItem = targetObject as FlowchartItem;
                Flowchart flowchart = flowchartItem.GetFlowchart();
                return GetSelfObjectList(targetObject, baseType, componentTypes).Where(i => (i as Variable).ParentBlock == null && ((i as Variable).GetFlowchart() == flowchart || (i as Variable).Scope == Scope.Public)).ToList();
            }
            return GetSelfObjectList(targetObject, baseType, componentTypes);
        }

        public static List<UnityEngine.Object> GetSceneVariableList(Type baseType, Type[] componentTypes, bool ignoreScope)
        {
            if (!ignoreScope)
            {
                return GetSceneObjectList(baseType, componentTypes).Where(i => (i as Variable).Scope == Scope.Public).ToList();
            }
            return GetSceneObjectList(baseType, componentTypes);
        }

        public static List<UnityEngine.Object> GetAssetVariableList(Type baseType, Type[] componentTypes, bool ignoreScope)
        {
            if (!ignoreScope)
            {
                return GetAssetObjectList(baseType, componentTypes).Where(i => (i as Variable) != null && (i as Variable).Scope == Scope.Public).ToList();
            }
            return GetAssetObjectList(baseType, componentTypes);
        }

        public static List<UnityEngine.Object> GetVariableList(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null, bool ignoreScope = false)
        {
            List<UnityEngine.Object> parameterList = GetParameterVariableList(baseType, componentTypes, targetObject).Distinct().Where(i => i != null).ToList();
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfVariableList(baseType, componentTypes, targetObject, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                if (!UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneVariableList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneVariableList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
            }
            List<UnityEngine.Object> assetObjectList = GetAssetVariableList(baseType, componentTypes, ignoreScope).Distinct().Where(i => i != null).ToList();

            List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
            objectList.AddRange(parameterList);
            objectList.AddRange(selfObjectList);
            objectList.AddRange(sceneObjectList);
            objectList.AddRange(assetObjectList);

            return objectList;
        }

        protected override Dictionary<string, List<UnityEngine.Object>> GetObjectDictionary(Type baseType, Type[] componentTypes, UnityEngine.Object targetObject = null)
        {
            List<UnityEngine.Object> parameterList = GetParameterVariableList(baseType, componentTypes, targetObject).Distinct().Where(i => i != null).ToList();
            
            List<UnityEngine.Object> selfObjectList = new List<UnityEngine.Object>();
            List<UnityEngine.Object> sceneObjectList = new List<UnityEngine.Object>();
            if (targetObject != null)
            {
                selfObjectList = GetSelfVariableList(baseType, componentTypes, targetObject, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                if (!UnityEditor.PrefabUtility.IsPartOfPrefabAsset(targetObject) && UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(targetObject.GetGameObject()) == null)
                {
                    sceneObjectList = GetSceneVariableList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
                    sceneObjectList = sceneObjectList.Where(i => selfObjectList.Find(s => s == i) == null).ToList();
                }
            }
            else
            {
                sceneObjectList = GetSceneVariableList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();
            }

            List<UnityEngine.Object> assetObjectList = GetAssetVariableList(baseType, componentTypes, s_ignoreScope).Distinct().Where(i => i != null).ToList();

            return new Dictionary<string, List<UnityEngine.Object>>
            {
                {
                    "Parameter", parameterList
                },
                {
                    "Self", selfObjectList
                },
                {
                    "Scene", sceneObjectList
                },
                {
                    "Assets", assetObjectList
                },
            };
        }
    }

}

#endif