#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SearchPopupUtilities;
using System.Text;

namespace Fungus
{

    public abstract class FlowchartItemTypeSearchPopupWindow<T> : FlowchartItemTypeSearchPopupWindow where T : FlowchartItem
    {
        protected abstract class NewFlowchartItemElement<W> : NewFlowchartItemElement where W : FlowchartItem
        {
            protected override Type GetFlowchartItemType()
            {
                return typeof(W);
            }

            protected override string GetNewItemTemplateFilename()
            {
                return string.Format("C# Script-New{0}Script.cs", typeof(W).Name);
            }
        }

        protected override string GetWindowName()
        {
            return ObjectNames.NicifyVariableName(typeof(T).Name);
        }

        protected override string DefaultCreateNewItemName()
        {
            return typeof(T).Name + "Name";
        }
        
        protected static W Show<W>(int controlID, Rect rect, Type selectedType, GUIContent defaultOptionLabel, Type[] types, SearchPopupWindowFlags flags = 0) where W : FlowchartItemTypeSearchPopupWindow<T>
        {
            if (types == null)
            {
                types = typeof(T).FindAllInstantiableDerivedTypes();
            }

            s_types = types;
            
            return ComponentTypeSearchPopupWindow.Show<W>(controlID, rect, selectedType, defaultOptionLabel, flags);
        }
        
        public static void ProcessAddAndSetObjectProperty(int controlID, SerializedProperty property)
        {
            SearchPopupWindow.ProcessSelectedItem(
                controlID,
                (object obj) =>
                {
                    Type selectedFlowchartItemType = obj as Type;

                    FlowchartItem selectedFlowchartItem = null;
                    FlowchartItem flowchartItem = property.serializedObject.targetObject as FlowchartItem;
                    Flowchart flowchart = flowchartItem.GetFlowchart();

                    FlowchartItem previousSelectedFlowchartItem = property.objectReferenceValue as FlowchartItem;

                    if (previousSelectedFlowchartItem != null)
                    {
                        if (selectedFlowchartItemType == null || previousSelectedFlowchartItem.GetType() != selectedFlowchartItemType)
                        {
                            previousSelectedFlowchartItem.DestroyReferences(true);
                            Undo.DestroyObjectImmediate(previousSelectedFlowchartItem);
                        }
                    }

                    if (selectedFlowchartItemType == null)
                    {
                        selectedFlowchartItem = null;
                    }
                    else
                    {
                        if (previousSelectedFlowchartItem == null || previousSelectedFlowchartItem.GetType() != selectedFlowchartItemType)
                        {
                            FlowchartItem newFlowchartItem = Undo.AddComponent(flowchart.gameObject, selectedFlowchartItemType) as FlowchartItem;
                            Command newCommand = newFlowchartItem as Command;
                            if (flowchartItem != null && newCommand != null)
                            {
                                newCommand.Setup(flowchartItem);
                            }
                            else
                            {
                                newFlowchartItem.Setup();
                            }
                            selectedFlowchartItem = newFlowchartItem;
                        }
                    }

                    Undo.RecordObject(property.serializedObject.targetObject, "Changed Flowchart Item Reference");
                    property.objectReferenceValue = selectedFlowchartItem;
                });
        }
    }

    public abstract class FlowchartItemTypeSearchPopupWindow : ComponentTypeSearchPopupWindow
    {
        protected abstract class NewFlowchartItemElement : NewComponentScriptElement
        {
            public NewFlowchartItemElement() : base()
            {
                NewFlowchartItemCategory = "[Custom]";
                NewItemName = ObjectNames.NicifyVariableName(NewItemName);
                NewFlowchartItemDescription = "";
                m_name = "Create New " + ObjectNames.NicifyVariableName(GetFlowchartItemType().Name) + " Type";
                m_content.text = m_name;
            }

            protected abstract Type GetFlowchartItemType();

            protected override string GetExtension()
            {
                return "cs";
            }

            protected override string GetNewItemTemplateFolder()
            {
                return "Editor/ScriptTemplates";
            }

            protected override void DrawControls()
            {
                DrawNameControl();

                DrawItemCategoryControl();
                
                DrawItemDescriptionControl();
            }

            public override float GetGUIHeight()
            {
                return 250f;
            }

            protected override void DrawNameControl()
            {
                GUILayout.Label("Filename");
                GUI.SetNextControlName("NewName");
                NewItemName = EditorGUILayout.TextField(NewItemName);
            }
            
            protected virtual void DrawItemCategoryControl()
            {
                GUILayout.Label("Category");
                GUI.SetNextControlName("NewFlowchartItemCategory");
                NewFlowchartItemCategory = EditorGUILayout.TextField(NewFlowchartItemCategory);
            }

            protected virtual void DrawItemDescriptionControl()
            {
                GUILayout.Label("Description");
                GUI.SetNextControlName("NewFlowchartItemDescription");
                NewFlowchartItemDescription = EditorGUILayout.TextField(NewFlowchartItemDescription);
            }

            protected override string[] GetControlNames()
            {
                return new string[] { "NewName", "NewFlowchartItemCategory", "NewFlowchartItemDescription" };
            }

            protected override MonoScript CreateScriptAssetFromTemplate(string targetPath, string templatePath)
            {
                string fullPath = System.IO.Path.GetFullPath(targetPath);
                string text = Resources.Load<TextAsset>(templatePath).text;
                UTF8Encoding encoding = new UTF8Encoding(true);
                File.WriteAllText(fullPath, text, encoding);
                AssetDatabase.ImportAsset(targetPath, ImportAssetOptions.ForceUpdate);
                return AssetDatabase.LoadAssetAtPath(targetPath, typeof(MonoScript)) as MonoScript;
            }

            protected override string ReplacePlaceholdersInTemplate(string text, string targetPath)
            {
                text = text.Replace("#SCRIPTNAME#", NewItemName);
                text = text.Replace("#CATEGORY#", NewFlowchartItemCategory);
                text = text.Replace("#NAME#", ObjectNames.NicifyVariableName(NewItemName));
                text = text.Replace("#DESCRIPTION#", NewFlowchartItemDescription);
                return text;
            }
        }

        protected static Type[] s_types;
        
        private static string s_newFlowchartItemCategory = "";
        
        private static string s_newFlowchartItemDescription = "";

        protected static string NewFlowchartItemCategory
        {
            get
            {
                return s_newFlowchartItemCategory;
            }
            set
            {
                s_newFlowchartItemCategory = value;
            }
        }

        protected static string NewFlowchartItemDescription
        {
            get
            {
                return s_newFlowchartItemDescription;
            }
            set
            {
                s_newFlowchartItemDescription = value;
            }
        }
        
        protected override object[] GetSortedItems()
        {
            if (s_types != null)
            {
                List<KeyValuePair<Type, FlowchartItemInfoAttribute>> sortedInfos = new List<KeyValuePair<Type, FlowchartItemInfoAttribute>>();
                foreach (Type type in s_types)
                {
                    if (type != null)
                    {
                        FlowchartItemInfoAttribute commandInfo = FlowchartItemInfoAttribute.GetInfo(type);
                        if (commandInfo != null)
                        {
                            sortedInfos.Add(new KeyValuePair<Type, FlowchartItemInfoAttribute>(type, commandInfo));
                        }
                    }
                }
                sortedInfos.Sort(FlowchartItemInfoAttribute.Compare);
                List<Type> sortedTypes = sortedInfos.Select(i => i.Key).ToList();
                return sortedTypes.ToArray();
            }
            return new object[0];
        }

        protected override GUIContent[] GetSortedMenus(object[] objs)
        {
            Type[] types = Array.ConvertAll(objs, x => x as Type);
            List<GUIContent> subMenuList = new List<GUIContent>();
            foreach (Type type in types)
            {
                GUIContent content = FungusGUI.GetFlowchartItemPopupOptionContent(type);
                if (content.image == null)
                {
                    if (s_monoScriptIcons.ContainsKey(type))
                    {
                        content.image = s_monoScriptIcons[type];
                    }
                }
                subMenuList.Add(content);
            }

            return subMenuList.ToArray();
        }
    }

}
#endif