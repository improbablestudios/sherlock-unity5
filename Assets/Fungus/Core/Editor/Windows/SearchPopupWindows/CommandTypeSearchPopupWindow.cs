#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    public class CommandTypeSearchPopupWindow : FlowchartItemTypeSearchPopupWindow<Command>
    {
        protected class NewCommandTypeElement<T> : NewFlowchartItemElement<T> where T : Command
        {
        }

        protected class NewCommandTypeElement : NewFlowchartItemElement<Command>
        {
            public NewCommandTypeElement() : base()
            {
            }
            
            protected override MonoScript CreateScriptAssetFromTemplate(string targetPath, string templatePath)
            {
                BlockWindow.Instance.NewScriptTargetPath = targetPath;
                BlockWindow.Instance.BlockToAddNewScriptTo = BlockWindow.Instance.TargetBlock;

                return base.CreateScriptAssetFromTemplate(targetPath, templatePath);
            }
        }

        public new static CommandTypeSearchPopupWindow Show(int controlID, Rect rect, SearchPopupWindowFlags flags = 0)
        {
            return Show<CommandTypeSearchPopupWindow>(controlID, rect, null, null, null, flags);
        }

        public new static CommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, SearchPopupWindowFlags flags = 0)
        {
            return Show<CommandTypeSearchPopupWindow>(controlID, rect, selectedType, null, null, flags);
        }

        public static CommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<CommandTypeSearchPopupWindow>(controlID, rect, selectedType, null, types, flags);
        }

        public new static CommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<CommandTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, null, flags);
        }

        public static CommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<CommandTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, types, flags);
        }
        
        protected override NewItemElement GetNewItemElement()
        {
            return new NewCommandTypeElement();
        }
    }

}
#endif