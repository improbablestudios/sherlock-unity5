#if UNITY_EDITOR
using UnityEngine;
using System;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SearchPopupUtilities;
using UnityEditor;
using System.IO;

namespace Fungus
{

    public class ReturnValueCommandTypeSearchPopupWindow : CommandTypeSearchPopupWindow
    {
        protected class NewReturnValueCommandTypeElement : NewCommandTypeElement<ReturnValueCommand>
        {
            protected override void DrawControls()
            {
                base.DrawControls();

                DrawTypeControl();
            }

            protected virtual void DrawTypeControl()
            {
                GUILayout.Label("Type");
                GUI.SetNextControlName("NewType");
                NewType = EditorGUILayout.TextField(NewType);
            }

            protected override string[] GetControlNames()
            {
                return new string[] { "NewName", "NewFlowchartItemCategory", "NewFlowchartItemDescription", "NewType" };
            }
            
            protected override string ReplacePlaceholdersInTemplate(string text, string targetPath)
            {
                text = text.Replace("#TYPE#", NewType);
                return base.ReplacePlaceholdersInTemplate(text, targetPath);
            }
        }
        
        protected static string s_newType;

        protected static string NewType
        {
            get
            {
                return s_newType;
            }
            set
            {
                s_newType = value;
            }
        }

        public new static ReturnValueCommandTypeSearchPopupWindow Show(int controlID, Rect rect, SearchPopupWindowFlags flags = 0)
        {
            return Show<ReturnValueCommandTypeSearchPopupWindow>(controlID, rect, null, null, null, flags);
        }

        public new static ReturnValueCommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, SearchPopupWindowFlags flags = 0)
        {
            return Show<ReturnValueCommandTypeSearchPopupWindow>(controlID, rect, selectedType, null, null, flags);
        }

        public new static ReturnValueCommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<ReturnValueCommandTypeSearchPopupWindow>(controlID, rect, selectedType, null, types, flags);
        }

        public new static ReturnValueCommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<ReturnValueCommandTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, null, flags);
        }

        public new static ReturnValueCommandTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<ReturnValueCommandTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, types, flags);
        }

        protected override NewItemElement GetNewItemElement()
        {
            return new NewReturnValueCommandTypeElement();
        }
    }

}
#endif