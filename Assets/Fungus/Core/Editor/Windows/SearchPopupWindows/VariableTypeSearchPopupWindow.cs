#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{

    public class VariableTypeSearchPopupWindow : FlowchartItemTypeSearchPopupWindow<Variable>
    {
        protected class NewVariableTypeElement : NewFlowchartItemElement<Variable>
        {
            protected override void DrawControls()
            {
                base.DrawControls();

                DrawTypeControl();
            }

            protected virtual void DrawTypeControl()
            {
                GUILayout.Label("Type");
                GUI.SetNextControlName("NewType");
                NewType = EditorGUILayout.TextField(NewType);
            }

            protected override string[] GetControlNames()
            {
                return new string[] { "NewName", "NewFlowchartItemCategory", "NewFlowchartItemDescription", "NewType" };
            }
            
            protected override string ReplacePlaceholdersInTemplate(string text, string targetPath)
            {
                text = text.Replace("#TYPE#", NewType);
                return base.ReplacePlaceholdersInTemplate(text, targetPath);
            }
        }

        protected static string s_newType;

        protected static string NewType
        {
            get
            {
                return s_newType;
            }
            set
            {
                s_newType = value;
            }
        }

        public new static VariableTypeSearchPopupWindow Show(int controlID, Rect rect, SearchPopupWindowFlags flags = 0)
        {
            return Show<VariableTypeSearchPopupWindow>(controlID, rect, null, null, null, flags);
        }

        public new static VariableTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, SearchPopupWindowFlags flags = 0)
        {
            return Show<VariableTypeSearchPopupWindow>(controlID, rect, selectedType, null, null, flags);
        }

        public static VariableTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<VariableTypeSearchPopupWindow>(controlID, rect, selectedType, null, types, flags);
        }

        public new static VariableTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, SearchPopupWindowFlags flags = 0)
        {
            return Show<VariableTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, null, flags);
        }

        public static VariableTypeSearchPopupWindow Show(int controlID, Rect rect, Type selectedType, GUIContent nullLabel, Type[] types, SearchPopupWindowFlags flags = 0)
        {
            return Show<VariableTypeSearchPopupWindow>(controlID, rect, selectedType, nullLabel, types, flags);
        }

        protected override NewItemElement GetNewItemElement()
        {
            return new NewVariableTypeElement();
        }
    }

}
#endif