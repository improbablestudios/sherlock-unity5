#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{

    public class FungusMenuItems
    {
        [MenuItem("Tools/Fungus/Utilities/Export Reference Docs", false, 100)]
        protected static void ExportReferenceDocs()
        {
            string path = EditorUtility.SaveFolderPanel("Export Reference Docs", "", "");
            if (path.Length == 0)
            {
                return;
            }

            BlockEditor.ExportCommandInfo(path);
            BlockEditor.ExportEventHandlerInfo(path);

            EditorUtility.DisplayDialog("Export Reference Docs", "Exported Reference Documentation to:\n\n" + path, "OK");
        }

        [MenuItem("Tools/Fungus/Utilities/Export Fungus Package", false, 100)]
        static void ExportFungusPackage()
        {
            string path = EditorUtility.SaveFilePanel("Export Fungus Package", "", "Fungus", "unitypackage");
            if (path.Length == 0)
            {
                return;
            }

            string[] folders = new string[] { "Assets/Fungus", "Assets/FungusExamples" };

            AssetDatabase.ExportPackage(folders, path, ExportPackageOptions.Recurse);
        }
        
        [MenuItem("Tools/Fungus/Create/Fungus Logo", false, 1000)]
        [MenuItem("GameObject/Fungus/Create/Fungus Logo", false, 1000)]
        static void CreateFungusLogo()
        {
            PrefabSpawner.SpawnPrefab<SpriteRenderer>("~FungusLogo", true);
        }
    }
}
#endif