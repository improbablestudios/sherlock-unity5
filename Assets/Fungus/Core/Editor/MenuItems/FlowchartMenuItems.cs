#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{

    public class FlowchartMenuItems
    {
        [MenuItem("Tools/Fungus/Create/Flowchart", false, -1008)]
        [MenuItem("GameObject/Fungus/Flowchart", false, -1008)]
        static void CreateFlowchart()
        {
            PrefabSpawner.SpawnPrefab<Flowchart>("~Flowchart", true);
        }
    }
}
#endif