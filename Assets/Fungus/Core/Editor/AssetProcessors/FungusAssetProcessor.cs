#if UNITY_EDITOR
using ImprobableStudios.ResourceTrackingUtilities;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Fungus
{
    public class DetectAssetModifications : UnityEditor.AssetModificationProcessor
    {
        static string[] OnWillSaveAssets(string[] paths)
        {
            List<string> savedAssetPaths = new List<string>();
            savedAssetPaths.AddRange(paths);

            bool updatedFlowchartPrefab = false;
            
            foreach (string path in savedAssetPaths)
            {
                if (path.EndsWith(".prefab", StringComparison.Ordinal))
                {
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    if (go != null)
                    {
                        Flowchart[] flowcharts = go.GetComponentsInChildren<Flowchart>(true);
                        foreach (Flowchart flowchart in flowcharts)
                        {
                            flowchart.transform.hideFlags = HideFlags.HideInHierarchy;
                            foreach (MonoBehaviour monoBehaviour in flowchart.GetComponentsInChildren<MonoBehaviour>(true))
                            {
                                if (monoBehaviour != null)
                                {
                                    monoBehaviour.hideFlags = HideFlags.HideInHierarchy;
                                }
                            }
                        }
                        if (flowcharts.Length > 0)
                        {
                            updatedFlowchartPrefab = true;
                        }
                    }
                }
                if (path.EndsWith(".unity", StringComparison.Ordinal))
                {
                    foreach (Flowchart flowchart in ResourceTracker.FindLoadedInstanceObjectsOfType<Flowchart>())
                    {
                        flowchart.ValidateFlowchart();
                    }
                }
            }
            if (updatedFlowchartPrefab)
            {
                foreach (Flowchart flowchart in ResourceTracker.GetCachedPrefabObjectsOfType<Flowchart>())
                {
                    if (flowchart.ValidateFlowchart())
                    {
                        EditorUtility.SetDirty(flowchart.gameObject);
                        EditorUtility.SetDirty(flowchart);
                        foreach (Block block in flowchart.Blocks)
                        {
                            EditorUtility.SetDirty(block);
                        }
                        foreach (Variable variable in flowchart.Variables)
                        {
                            EditorUtility.SetDirty(variable);
                        }
                        foreach (MonoBehaviour monoBehaviour in flowchart.GetComponentsInChildren<MonoBehaviour>(true))
                        {
                            monoBehaviour.hideFlags = HideFlags.HideInHierarchy;
                        }
                        savedAssetPaths.Add(AssetDatabase.GetAssetPath(flowchart));
                    }
                }
            }
            
            return savedAssetPaths.ToArray();
        }
    }
    
    public class PostProcessAssetChanges : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (importedAssets.Length > 0)
            {
                foreach (string path in importedAssets)
                {
                    if (path.EndsWith(".prefab", StringComparison.Ordinal))
                    {
                        GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                        if (go != null)
                        {
                            Flowchart[] flowcharts = go.GetComponentsInChildren<Flowchart>(true);
                            foreach (Flowchart flowchart in flowcharts)
                            {
                                flowchart.UpdateHideFlags();
                            }
                        }
                    }
                }
            }
        }
    }
}
#endif