﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[AddComponentMenu("Fungus/Variables/#TYPE#")]
public class #TYPE#Variable : Variable<#TYPE#>
{
}

[Serializable]
public class #TYPE#Data : VariableData<#TYPE#>
{
    public #TYPE#Data()
	{
	}

    public #TYPE#Data(#TYPE# v)
	{
	    m_DataVal = v;
	}
}