﻿using UnityEngine;
using Fungus;

[EventHandlerInfo("#CATEGORY#",
                  "#DESCRIPTION#")]
[AddComponentMenu("Fungus/Event Handlers/#CATEGORY#/#NAME#")]
public class #SCRIPTNAME# : EventHandler
{
    void Update()
    {
        //if (Insert Check Here)
        {
            ExecuteBlock();
        }
    }
}
