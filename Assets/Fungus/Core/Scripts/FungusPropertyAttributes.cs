﻿using System;
using UnityEngine;
using System.Reflection;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus
{
    public abstract class CheckFieldDataAttribute : CheckFieldAttribute
    {
        public CheckFieldDataAttribute() : base()
        {
        }

        public CheckFieldDataAttribute(string errorMessage) : base(errorMessage)
        {
        }

        public override sealed string GetError(FieldInfo field, object parentObj)
        {
            Type fieldType = field.FieldType;
            VariableData variableData = field.GetValue(parentObj) as VariableData;

            if (this != null && variableData != null)
            {
                if ((Application.isPlaying || variableData.IsConstant))
                {
                    return GetDataError(field.Name, variableData.Value);
                }
            }

            return null;
        }

        protected abstract string GetDataError(string fieldName, object value);
    }

    public class CheckNotNullDataAttribute : CheckFieldDataAttribute
    {
        public CheckNotNullDataAttribute() : base()
        {
        }

        public CheckNotNullDataAttribute(string errorMessage) : base(errorMessage)
        {
        }

        protected override string GetDataError(string fieldName, object value)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }

            if (value.IsValueNull())
            {
                return GetDefaultErrorMessage(fieldName);
            }

            return null;
        }

        public static string GetDefaultErrorMessage(string fieldName)
        {
            string errorFormat = "No {0} selected";
#if UNITY_EDITOR
            return string.Format(errorFormat, UnityEditor.ObjectNames.NicifyVariableName(fieldName));
#else
            return string.Format(errorFormat, fieldName);
#endif
        }
    }

    public class CheckNotEmptyDataAttribute : CheckFieldDataAttribute
    {
        public CheckNotEmptyDataAttribute() : base()
        {
        }

        public CheckNotEmptyDataAttribute(string errorMessage) : base(errorMessage)
        {
        }

        protected override string GetDataError(string fieldName, object value)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }

            if (value.IsValueEmpty())
            {
                return GetDefaultErrorMessage(fieldName);
            }

            return null;
        }

        public static string GetDefaultErrorMessage(string fieldName)
        {
            string errorFormat = "{0} is empty";
#if UNITY_EDITOR
            return string.Format(errorFormat, UnityEditor.ObjectNames.NicifyVariableName(fieldName));
#else
            return string.Format(errorFormat, fieldName);
#endif
        }
    }

    public class TypeNameDataFieldAttribute : NullLabelAttribute
    {
        public TypeNameDataFieldAttribute(params Type[] baseTypes) : base()
        {
            BaseTypes = baseTypes;
        }

        public TypeNameDataFieldAttribute(string nullLabel, params Type[] baseTypes) : base(nullLabel)
        {
            BaseTypes = baseTypes;
        }

        public Type[] BaseTypes
        {
            get;
            set;
        }
    }

    public class VariableValueTypeNameFieldAttribute : NullLabelAttribute
    {
        public VariableValueTypeNameFieldAttribute(params Type[] baseTypes) : base()
        {
            BaseTypes = baseTypes;
        }

        public VariableValueTypeNameFieldAttribute(string nullLabel, params Type[] baseTypes) : base(nullLabel)
        {
            BaseTypes = baseTypes;
        }

        public Type[] BaseTypes
        {
            get;
            set;
        }
    }

    public class VariableValueTypeNameDataFieldAttribute : NullLabelAttribute
    {
        public VariableValueTypeNameDataFieldAttribute(params Type[] baseTypes) : base()
        {
            BaseTypes = baseTypes;
        }

        public VariableValueTypeNameDataFieldAttribute(string nullLabel, params Type[] baseTypes) : base(nullLabel)
        {
            BaseTypes = baseTypes;
        }

        public Type[] BaseTypes
        {
            get;
            set;
        }
    }

    public class VariableOfTypeFieldAttribute : NullLabelAttribute
    {
        public VariableOfTypeFieldAttribute(params System.Type[] variableValueTypes) : base()
        {
            this.VariableValueTypes = variableValueTypes;
        }

        public VariableOfTypeFieldAttribute(string nullLabel, params System.Type[] variableValueTypes) : base(nullLabel)
        {
            this.VariableValueTypes = variableValueTypes;
        }

        public System.Type[] VariableValueTypes
        {
            get;
            set;
        }
    }
    
    public class AssetGuidDataFieldAttribute : PropertyAttribute
    {
        public AssetGuidDataFieldAttribute(Type objType)
        {
            ObjType = objType;
        }

        public Type ObjType
        {
            get;
            set;
        }
    }

    public class TagDataFieldAttribute : PropertyAttribute
    {
        public TagDataFieldAttribute()
        {
        }
    }

    public class LayerDataFieldAttribute : PropertyAttribute
    {
        public LayerDataFieldAttribute()
        {
        }
    }

    public class SortingLayerDataFieldAttribute : PropertyAttribute
    {
        public SortingLayerDataFieldAttribute()
        {
        }
    }

    public class SceneNameDataFieldAttribute : PropertyAttribute
    {
        public SceneNameDataFieldAttribute()
        {
        }
    }

    public class ObjectDataPopupAttribute : NullLabelWithDefaultAttribute
    {
        public ObjectDataPopupAttribute() : base()
        {
        }

        public ObjectDataPopupAttribute(string nullLabel) : base(nullLabel)
        {
        }

        public ObjectDataPopupAttribute(bool defaultToNonNull) : base(defaultToNonNull)
        {
        }

        public ObjectDataPopupAttribute(string nullLabel, bool defaultToNonNull) : base(nullLabel, defaultToNonNull)
        {
        }
    }

    public class DataMinAttribute : PropertyAttribute
    {
        public DataMinAttribute(float min)
        {
            this.Min = min;
        }

        public float Min
        {
            get;
            set;
        }
    }

    public class DataRangeAttribute : PropertyAttribute
    {
        public DataRangeAttribute(float min, float max)
        {
            this.Min = min;
            this.Max = max;
        }

        public float Min
        {
            get;
            set;
        }

        public float Max
        {
            get;
            set;
        }
    }

    public class StringDataAreaAttribute : PropertyAttribute
    {
        public StringDataAreaAttribute(int minLines, int maxLines)
        {
            this.MinLines = minLines;
            this.MaxLines = maxLines;
        }

        public int MinLines
        {
            get;
            set;
        }

        public int MaxLines
        {
            get;
            set;
        }
    }
    
    public class LanguageDataFieldAttribute : PropertyAttribute
    {
        public LanguageDataFieldAttribute()
        {
        }
    }

    public class GameObjectWithComponentDataFieldAttribute : NullLabelWithDefaultAttribute
    {
        public GameObjectWithComponentDataFieldAttribute(params Type[] componentTypes) : base()
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentDataFieldAttribute(string nullLabel, params Type[] componentTypes) : base(nullLabel)
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentDataFieldAttribute(bool defaultToNonNull, params Type[] componentTypes) : base(defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public GameObjectWithComponentDataFieldAttribute(string nullLabel, bool defaultToNonNull, params Type[] componentTypes) : base(nullLabel, defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public Type[] ComponentTypes
        {
            get;
            set;
        }
    }

    public class ComponentDataFieldAttribute : NullLabelWithDefaultAttribute
    {
        public ComponentDataFieldAttribute(params Type[] componentTypes) : base()
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentDataFieldAttribute(string nullLabel, params Type[] componentTypes) : base(nullLabel)
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentDataFieldAttribute(bool defaultToNonNull, params Type[] componentTypes) : base(defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public ComponentDataFieldAttribute(string nullLabel, bool defaultToNonNull, params Type[] componentTypes) : base(nullLabel, defaultToNonNull)
        {
            this.ComponentTypes = componentTypes;
        }

        public Type[] ComponentTypes
        {
            get;
            set;
        }
    }
    
    public class ColorSwatchDataFieldAttributeAttribute : PropertyAttribute
    {
        public ColorSwatchDataFieldAttributeAttribute()
        {
        }
    }
}