﻿using System.Collections.Generic;
using UnityEngine;

namespace Fungus
{
    public static class FungusColorPalette
    {
        private static readonly Dictionary<string, Color> k_commandColorDictionary =
            new Dictionary<string, Color>()
            {
                { "", Color.white },
                { "Animation",  new Color32(239,255,231,255) },
                { "Application",  new Color32(235,251,227,255) },
                { "Audio Source",  new Color32(252,219,186,255) },
                { "Camera",  new Color32(247,231,255,255) },
                { "Character",  new Color32(188,254,255,255) },
                { "Component",  new Color32(231,255,252,255) },
                { "Counter",  new Color32(244,253,211,255) },
                { "Dialog",  new Color32(207,246,255,255) },
                { "DOTween",  new Color32(225,255,239,255) },
                { "Flow",  new Color32(253,253,190,255) },
                { "Flow/Conditional",  new Color32(255,248,230,255) },
                { "Flow/Loop",  new Color32(255,248,230,255) },
                { "Game Object",  new Color32(250,233,240,255) },
                { "Input",  new Color32(255,255,214,255) },
                { "Localization",  new Color32(181,248,252,255) },
                { "World Map",  new Color32(255,218,188,255) },
                { "World Sprite",  new Color32(200,250,200,255) },
                { "Portrait",  new Color32(255,225,255,255) },
                { "Renderer",  new Color32(205,255,239,255) },
                { "Rigidbody",  new Color32(252,243,224,255) },
                { "Save",  new Color32(255,202,207,255) },
                { "Screen",  new Color32(220,220,250,255) },
                { "Scripting",  new Color32(255,222,211,255) },
                { "Sprite",  new Color32(246,230,255,255) },
                { "Text",  new Color32(207,245,255,255) },
                { "Transform",  new Color32(210,250,250,255) },
                { "UI",  new Color32(250,250,220,255) },
                { "Variable",  new Color32(235,252,207,255) },
                { "Collider",  new Color32(255,197,197,255) },
                { "Setting",  new Color32(253,231,231,255) },
            };

        public static bool HasColor(string category)
        {
            return (k_commandColorDictionary.ContainsKey(category));
        }

        public static Color GetColor(string category)
        {
            if (k_commandColorDictionary.ContainsKey(category))
            {
                return k_commandColorDictionary[category];
            }
            return Color.white;
        }
    }
}