﻿using UnityEngine;
using Fungus.Variables;
using UnityEngine.Events;
using ImprobableStudios.SaveSystem;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("",
                      "This block will execute the first time the flowchart is enabled.",
                       -3)]
    [AddComponentMenu("Fungus/Event Handlers/Flowchart Started")]
    public class FlowchartStarted : FlowchartEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return GetFlowchart().OnStarted;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            return !SaveSystemManager.IsLoading;
        }
    }

}
