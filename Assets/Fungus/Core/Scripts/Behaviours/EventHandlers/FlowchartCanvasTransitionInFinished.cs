﻿using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Flowchart Canvas",
                      "This block will execute when the Flowchart finishes fading in its canvas.")]
    [AddComponentMenu("Fungus/Event Handlers/Flowchart Canvas/Flowchart Canvas Transition In Completed")]
    public class FlowchartCanvasTransitionInFinished : FlowchartEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return GetFlowchart().UIConstruct.TransitionInCompleted;
        }
    }

}
