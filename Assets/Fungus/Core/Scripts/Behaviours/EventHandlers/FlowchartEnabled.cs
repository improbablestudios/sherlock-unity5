﻿using UnityEngine;
using Fungus.Variables;
using UnityEngine.Events;
using ImprobableStudios.SaveSystem;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("",
                      "This block will execute when the Flowchart game object is enabled.", 
                      -2)]
    [AddComponentMenu("Fungus/Event Handlers/Flowchart Enabled")]
    public class FlowchartEnabled : FlowchartEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return GetFlowchart().OnEnabled;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            return !SaveSystemManager.IsLoading;
        }
    }
}
