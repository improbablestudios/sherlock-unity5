﻿using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Flowchart Canvas",
                      "This block will execute when the Flowchart starts fading out its canvas.")]
    [AddComponentMenu("Fungus/Event Handlers/Flowchart Canvas/Flowchart Canvas Transition Out Started")]
    public class FlowchartCanvasTransitionOutStarted : FlowchartEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return GetFlowchart().UIConstruct.TransitionOutStarted;
        }
    }

}
