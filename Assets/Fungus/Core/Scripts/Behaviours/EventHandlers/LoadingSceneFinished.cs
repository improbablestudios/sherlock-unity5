﻿using UnityEngine;
using Fungus.Variables;
using UnityEngine.Events;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.SceneUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Scene",
                      "This block will execute when the current scene has been loaded and the loading overlay is hidden.",
                      -2)]
    [AddComponentMenu("Fungus/Event Handlers/Loading Scene Finished")]
    public class LoadingSceneFinished : UnityEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return SceneLoadingManager.Instance.OnOverlayHidden;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            return !SaveSystemManager.IsLoading;
        }
    }
}
