﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("",
                      "This block will execute when the specified message is received from a Send Message command.")]
    [AddComponentMenu("Fungus/Event Handlers/Message Received")]
    public class MessageReceived : EventHandler 
    {
        [Tooltip("Fungus message to listen for")]
        [FormerlySerializedAs("_message")]
        public StringData m_Message = new StringData("");

        protected override void AddListener()
        {
            if (Flowchart.OnMessageDetected != null)
            {
                Flowchart.OnMessageDetected.AddListener(OnReceivedFungusMessage);
            }
        }

        protected override void RemoveListener()
        {
            if (Flowchart.OnMessageDetected != null)
            {
                Flowchart.OnMessageDetected.RemoveListener(OnReceivedFungusMessage);
            }
        }
        
        public void OnReceivedFungusMessage(Flowchart flowchart, string message)
        {
            if (flowchart == null || flowchart == GetFlowchart())
            {
                if (m_Message.Value == message)
                {
                    ExecuteBlock();
                }
            }
        }

        public override string GetSummary()
        {
            return m_Message.ToString();
        }
    }

}