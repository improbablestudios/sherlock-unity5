﻿using UnityEngine;
using Fungus.Variables;
using UnityEngine.Events;
using ImprobableStudios.SaveSystem;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("",
                      "This block will execute every frame the flowchart is enabled.",
                       -1)]
    [AddComponentMenu("Fungus/Event Handlers/Flowchart Updated")]
    public class FlowchartUpdated : FlowchartEventHandler
    {
        protected override UnityEvent GetUnityEvent()
        {
            return GetFlowchart().OnUpdated;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            return !SaveSystemManager.IsLoading;
        }
    }

}
