﻿using System.Collections;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    public abstract class FlowchartEventHandler : UnityEventHandler
    {
        public override int ExecutionFrameDelay()
        {
            return 1;
        }

        public override bool WillExecuteBlockOnFlowchartStart()
        {
            return true;
        }
    }

}
