using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.Localization.SmartFormat;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus
{

    [Flags]
    public enum FlowchartLoggingOptions
    {
        Flowchart = 1,
        Block = 2,
        EventHandler = 4,
        Command = 8,
        Variable = 16,
    }

    public enum DebugLogType
    {
        Info,
        Warning,
        Error
    }

    public class FungusMessageEvent : UnityEvent<Flowchart, string>
    {
    }

    /// <summary>
    /// Visual scripting controller for the Flowchart programming language.
    /// Flowchart objects may be edited visually using the Flowchart editor window.
    /// </summary>
    [AddComponentMenu("Fungus/Flowchart")]
    public class Flowchart : PersistentBehaviour, ISavable, INode
    {
        [Serializable]
        public class FlowchartSaveState : BehaviourSaveState<Flowchart>
        {
        }

        /// <summary>
        /// The default name of a new flowchart variable.
        /// </summary>
        public const string DEFAULT_FLOWCHART_VARIABLE_NAME = "Var";

        /// <summary>
        /// Current version used to compare with the previous version so older versions can be custom-updated from previous versions.
        /// </summary>
        public const string CURRENT_VERSION = "1.0";

        protected static Graph s_appFlowGraph = new Graph();

        private static Flowchart s_previousActiveFlowchart;

        private static Flowchart s_latestActivatedFlowchart;

        private static Flowchart s_latestSwitchedToFlowchart;

        public delegate void OnLogDelegate(Flowchart flowchart, FlowchartLoggingOptions loggingOptions, DebugLogType logType, string logMessage);

        private static event OnLogDelegate s_onLog = delegate { };

        [Tooltip("Variable to track flowchart's version and if initial set up has completed")]
        [HideInInspector]
        [FormerlySerializedAs("version")]
        [SerializeField]
        protected string m_Version;

        [Tooltip("The flowchart that this flowchart will inherit public and protected blocks and variables from")]
        [SerializeField]
        protected Flowchart m_TemplateFlowchart;

        [Tooltip("The flowchart that inherits public and protected blocks and variables from this flowchart")]
        protected Flowchart m_ChildFlowchart;

        [Tooltip("Description text displayed in the Flowchart editor window")]
        [TextArea(3, 5)]
        [SerializeField]
        [FormerlySerializedAs("description")]
        protected string m_Description = "";

        [Tooltip("Pause after each command is executed to make it easier to visualise program flow (Editor only, has no effect in platform builds)")]
        [Range(0f, 5f)]
        [SerializeField]
        [FormerlySerializedAs("stepPause")]
        protected float m_StepPause = 0f;

        [Tooltip("Pause the application when this flowchart is enabled to make it easier to visualize the state of the application (Editor only, has no effect in platform builds)")]
        [SerializeField]
        protected bool m_PauseOnEnable = false;
        
        [Tooltip("Hides this flowchart's block, command, and variable components in the inspector")]
        [SerializeField]
        [FormerlySerializedAs("hideComponents")]
        protected bool m_HideComponents = true;

        [Tooltip("Write the following information to the console log")]
        [SerializeField]
        [EnumFlags]
        protected FlowchartLoggingOptions m_Logging;

        [Tooltip("The position of this flowchart in the App Flow Window")]
        [HideInInspector]
        [SerializeField]
        [FormerlySerializedAs("nodePosition")]
        protected Vector2 m_NodePosition = new Vector2(0, 0);

        [Tooltip("The list of blocks in this flowchart")]
        [SerializeField]
        [FormerlySerializedAs("blocks")]
        protected List<Block> m_Blocks = new List<Block>();

        [Tooltip("The list of variables in this flowchart")]
        [SerializeField]
        [FormerlySerializedAs("variables")]
        protected List<Variable> m_Variables = new List<Variable>();
        
        protected static FungusMessageEvent m_onMessageDetected = new FungusMessageEvent();

        /// <summary>
        /// The flowchart that activated this flowchart
        /// </summary>
        protected Flowchart m_previousFlowchart;

        /// <summary>
        /// The flowchart that will be activated after this flowchart is finished deactivating
        /// </summary>
        protected Flowchart m_nextFlowchart;

        /// <summary>
        /// The graph displayed in the FlowchartWindow
        /// </summary>
        protected Graph m_graph = new Graph();
        
        protected UnityEvent m_onStarted = new UnityEvent();
        
        protected UnityEvent m_onEnabled = new UnityEvent();
        
        protected UnityEvent m_onUpdated = new UnityEvent();

        protected bool m_isValidating;

        protected bool m_isActivating;

        protected bool m_isDeactivating;

        protected bool m_isSwitching;

        private float m_executingIconTimer;

        private static bool m_eventSystemPresent;
        
        private UIConstruct m_uiConstruct;

        public static Graph AppFlowGraph
        {
            get
            {
                return s_appFlowGraph;
            }
            set
            {
                s_appFlowGraph = value;
            }
        }

        public UIConstruct UIConstruct
        {
            get
            {
                if (m_uiConstruct == null)
                {
                    m_uiConstruct = GetComponentInChildren<UIConstruct>(true);
                }
                return m_uiConstruct;
            }
        }
        
        public static Flowchart PreviousActiveFlowchart
        {
            get
            {
                return s_previousActiveFlowchart;
            }
            set
            {
                s_previousActiveFlowchart = value;
            }
        }

        public static Flowchart LatestActivatedFlowchart
        {
            get
            {
                return s_latestActivatedFlowchart;
            }
            set
            {
                s_latestActivatedFlowchart = value;
            }
        }

        public static Flowchart LatestSwitchedToFlowchart
        {
            get
            {
                return s_latestSwitchedToFlowchart;
            }
            set
            {
                s_latestSwitchedToFlowchart = value;
            }
        }

        public string Version
        {
            get
            {
                return m_Version;
            }
            set
            {
                m_Version = value;
            }
        }

        public Flowchart TemplateFlowchart
        {
            get
            {
                return m_TemplateFlowchart;
            }
            set
            {
                m_TemplateFlowchart = value;
            }
        }

        public Flowchart ChildFlowchart
        {
            get
            {
                return m_ChildFlowchart;
            }
            set
            {
                m_ChildFlowchart = value;
            }
        }

        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }

        public float StepPause
        {
            get
            {
                return m_StepPause;
            }
            set
            {
                m_StepPause = value;
            }
        }

        public bool PauseOnEnable
        {
            get
            {
                return m_PauseOnEnable;
            }
            set
            {
                m_PauseOnEnable = value;
            }
        }

        public bool HideComponents
        {
            get
            {
                return m_HideComponents;
            }
            set
            {
                m_HideComponents = value;
            }
        }

        public FlowchartLoggingOptions Logging
        {
            get
            {
                return m_Logging;
            }
            set
            {
                m_Logging = value;
            }
        }

        public Vector2 NodePosition
        {
            get
            {
                return m_NodePosition;
            }
            set
            {
                m_NodePosition = value;
            }
        }

        public List<Block> Blocks
        {
            get
            {
                return m_Blocks;
            }
        }

        public List<Variable> Variables
        {
            get
            {
                return m_Variables;
            }
        }

        public static FungusMessageEvent OnMessageDetected
        {
            get
            {
                return m_onMessageDetected;
            }
        }

        public Flowchart PreviousFlowchart
        {
            get
            {
                return m_previousFlowchart;
            }
            set
            {
                m_previousFlowchart = value;
            }
        }

        public Flowchart NextFlowchart
        {
            get
            {
                return m_nextFlowchart;
            }
            set
            {
                m_nextFlowchart = value;
            }
        }

        public Graph Graph
        {
            get
            {
                return m_graph;
            }
        }

        public float ExecutingIconTimer
        {
            get
            {
                return ExecutingIconTimer;
            }
        }
        
        public bool IsActivating
        {
            get
            {
                return m_isActivating;
            }
        }

        public bool IsDeactivating
        {
            get
            {
                return m_isDeactivating;
            }
        }

        public static OnLogDelegate OnLog
        {
            get
            {
                return s_onLog;
            }
            set
            {
                s_onLog = value;
            }
        }

        public UnityEvent OnStarted
        {
            get
            {
                return m_onStarted;
            }
        }

        public UnityEvent OnEnabled
        {
            get
            {
                return m_onEnabled;
            }
        }

        public UnityEvent OnUpdated
        {
            get
            {
                return m_onUpdated;
            }
        }

        public virtual SaveState GetSaveState()
        {
            return new FlowchartSaveState();
        }


        /// <summary>
        /// Returns the next id to assign to a new flowchart item.
        /// Item ids increase monotically so they are guaranteed to
        /// be unique within a Flowchart.
        /// </summary>
        /// <returns>Next unique item id</returns>
        public int NextItemId()
        {
            int maxId = -1;
            foreach (Block block in m_Blocks)
            {
                maxId = Math.Max(maxId, block.ItemId);
            }

            Command[] commands = GetComponents<Command>();
            foreach (Command command in commands)
            {
                maxId = Math.Max(maxId, command.ItemId);
            }

            EventHandler[] eventHandlers = GetComponents<EventHandler>();
            foreach (EventHandler eventHandler in eventHandlers)
            {
                maxId = Math.Max(maxId, eventHandler.ItemId);
            }

            foreach (Variable variable in m_Variables)
            {
                maxId = Math.Max(maxId, variable.ItemId);
            }

            return maxId + 1;
        }

        protected virtual void OnSceneWasLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            // Reset the flag for checking for an event system as there may not be one in the newly loaded scene.
            m_eventSystemPresent = false;
        }

        public bool ValidateFlowchart()
        {
            bool changed = false;

            if (m_isValidating)
            {
                return false;
            }

#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                m_isValidating = true;

                UnityEditor.EditorApplication.delayCall += 
                    () =>
                    {
                        if (FlowchartHasInheritenceCycle())
                        {
                            m_TemplateFlowchart = null;

                            UnityEditor.EditorUtility.DisplayDialog("Inheritence cycle detected",
                                               "You cannot make this flowchart use a template that also uses this flowchart as a template.",
                                               "OK");
                            changed = true;
                        }

                        if (ValidateBlocks())
                        {
                            changed = true;
                        }

                        if (m_Blocks != null)
                        {
                            foreach (Block block in m_Blocks.ToList())
                            {
                                if (block.ValidateParameters())
                                {
                                    changed = true;
                                }
                            }
                        }

                        if (ValidateVariables())
                        {
                            changed = true;
                        }

                        if (changed)
                        {
                            if (this != null)
                            {
                                if (gameObject.scene.IsValid())
                                {
                                    foreach (Flowchart flowchart in ResourceTracker.FindLoadedInstanceObjectsOfType<Flowchart>())
                                    {
                                        if (flowchart.m_TemplateFlowchart == this)
                                        {
                                            flowchart.ValidateFlowchart();
                                        }
                                    }
                                }
                            }
                        }
                    };

                m_isValidating = false;
            }
#endif
            return changed;
        }

#if UNITY_EDITOR
        public bool ValidateBlocks()
        {
            List<Block> templateBlocks = new List<Block>();
            if (m_TemplateFlowchart != null)
            {
                templateBlocks = m_TemplateFlowchart.m_Blocks;
            }
            return Block.ValidateBlocksInList(this, m_Blocks, templateBlocks, Block.DEFAULT_BLOCK_NAME, Scope.Private, false);
        }

        public bool ValidateVariables()
        {
            List<Variable> templateVariables = new List<Variable>();
            if (m_TemplateFlowchart != null)
            {
                templateVariables = m_TemplateFlowchart.m_Variables;
            }
            return Variable.ValidateVariablesInList(this, m_Variables, templateVariables, DEFAULT_FLOWCHART_VARIABLE_NAME, Scope.Private, false, null);
        }
#endif

        protected override void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
            {
                if (m_ChildFlowchart == null)
                {
                    BakeFlowchartTemplate();
                }
            }
        }

        public void BakeFlowchartTemplate()
        {
            if (m_TemplateFlowchart != null)
            {
                GameObject templateGO = new GameObject("Templates");
                templateGO.transform.SetParent(this.transform);
                templateGO.transform.SetAsFirstSibling();
                templateGO.transform.localPosition = Vector3.zero;
                templateGO.transform.localEulerAngles = Vector3.zero;
                templateGO.transform.localScale = Vector3.one;
                templateGO.SetActive(false);

                m_TemplateFlowchart = InstantiateAndRegister(m_TemplateFlowchart, templateGO.transform);
                m_TemplateFlowchart.InstantiateFlowchartTemplates();

                ReassignTemplateReferences();
                RegenerateTemplateKeys();

                templateGO.SetActive(true);
            }
        }

        public void InstantiateFlowchartTemplates()
        {
            if (m_TemplateFlowchart != null)
            {
                m_TemplateFlowchart = InstantiateAndRegister(m_TemplateFlowchart, transform);
                m_TemplateFlowchart.m_ChildFlowchart = this;
                m_TemplateFlowchart.InstantiateFlowchartTemplates();
            }
        }

        public void ReassignTemplateReferences()
        {
            if (m_TemplateFlowchart != null)
            {
                foreach (Block block in m_Blocks)
                {
                    if (block.TemplateBlock != null)
                    {
                        block.TemplateBlock = m_TemplateFlowchart.m_Blocks.Find(i => i.Key == block.TemplateBlock.Key);
                        if (block.TemplateBlock != null)
                        {
                            block.TemplateBlock.ChildBlock = block;
                        }
                    }
                }
                foreach (Variable variable in m_Variables)
                {
                    if (variable.m_TemplateVariable != null)
                    {
                        variable.m_TemplateVariable = m_TemplateFlowchart.m_Variables.Find(i => i.Key == variable.m_TemplateVariable.Key);
                        if (variable.m_TemplateVariable != null)
                        {
                            variable.m_TemplateVariable.m_ChildVariable = variable;
                        }
                    }
                }
                m_TemplateFlowchart.ReassignTemplateReferences();
            }
        }
        
        public void RegenerateTemplateKeys()
        {
            if (m_TemplateFlowchart != null)
            {
                m_TemplateFlowchart.RegenerateKey();
                foreach (FlowchartItem flowchartItem in m_TemplateFlowchart.GetComponentsInChildren<FlowchartItem>())
                {
                    flowchartItem.RegenerateKey();
                }
                m_TemplateFlowchart.RegenerateTemplateKeys();
            }
        }

        // There must be an Event System in the scene for Say and Menu input to work.
        // This method will automatically instantiate one if none exists.
        protected virtual void CheckEventSystem()
        {
            if (m_eventSystemPresent)
            {
                return;
            }

            EventSystem eventSystem = GameObject.FindObjectOfType<EventSystem>();
            if (eventSystem == null)
            {
                // Auto spawn an Event System from the prefab
                GameObject prefab = Resources.Load<GameObject>("EventSystem");
                if (prefab != null)
                {
                    GameObject go = InstantiateAndRegister(prefab) as GameObject;
                    go.name = "EventSystem";
                }
            }

            m_eventSystemPresent = true;
        }
        
        protected virtual void Start()
        {
            SceneManager.sceneLoaded += OnSceneWasLoaded;
            CheckEventSystem();

            m_onStarted.Invoke();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Log(FlowchartLoggingOptions.Flowchart, DebugLogType.Info, "ENABLED", this);

            m_Blocks.RemoveAll(item => item == null);
            
            CheckItemIds();
            UpdateVersion();
            
#if UNITY_EDITOR
            if (m_PauseOnEnable)
            {
                Debug.Break();
            }
#endif
            
            m_onEnabled.Invoke();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Log(FlowchartLoggingOptions.Flowchart, DebugLogType.Info, "DEACTIVATE", this);
        }

        protected virtual void Update()
        {
            m_onUpdated.Invoke();
        }

        protected virtual void CheckItemIds()
        {
            // Make sure item ids are unique and monotonically increasing.
            // This should always be the case, but some legacy Flowcharts may have issues.
            List<int> usedIds = new List<int>();
            foreach (Block block in m_Blocks)
            {
                if (block.ItemId == -1 ||
                    usedIds.Contains(block.ItemId))
                {
                    block.ItemId = NextItemId();
                }
                usedIds.Add(block.ItemId);
            }

            Command[] commands = GetComponents<Command>();
            foreach (Command command in commands)
            {
                if (command.ItemId == -1 ||
                    usedIds.Contains(command.ItemId))
                {
                    command.ItemId = NextItemId();
                }
                usedIds.Add(command.ItemId);
            }
        }

        private void UpdateVersion()
        {
            // If versions match, then we are already using the latest.
            if (m_Version == CURRENT_VERSION) return;

            switch (m_Version)
            {
                // Version never set, so we are initializing on first creation or this flowchart is pre-versioning.
                case null:
                case "":
                    break;
            }

            m_Version = CURRENT_VERSION;
        }

#if UNITY_EDITOR
        public virtual Variable AddVariable(Type type, bool allowUndo = true)
        {
            return AddVariable(type, m_Variables.Count, allowUndo);
        }

        public virtual Variable AddVariable(Type type, int index, bool allowUndo = true)
        {
            Variable variable = Variable.CreateVariable(this, type, DEFAULT_FLOWCHART_VARIABLE_NAME, null, Scope.Private, allowUndo);

            Variable.AddVariableToList(this, m_Variables, variable, index, DEFAULT_FLOWCHART_VARIABLE_NAME, allowUndo);
            
            InvokePropertyChanged(nameof(m_Variables));

            return variable;
        }
        
        public virtual void RemoveVariable(Variable variable, bool allowUndo = true)
        {
            Variable.RemoveVariableFromList(this, m_Variables, variable, allowUndo);
            Variable.DestroyVariable(this, variable, allowUndo);
            
            InvokePropertyChanged(nameof(m_Variables));
        }
        
        public virtual Block AddBlock(bool allowUndo = true)
        {
            Vector2 position = Vector2.zero;
            if (FlowchartWindow.Instance != null)
            {
                position = FlowchartWindow.Instance.GetNewNodePositionInGraph();
            }
            return AddBlock(m_Blocks.Count, position, allowUndo);
        }

        public virtual Block AddBlock(Vector2 position, bool allowUndo = true)
        {
            return AddBlock(m_Blocks.Count, position, allowUndo);
        }

        public virtual Block AddBlock(int index, bool allowUndo = true)
        {
            Vector2 position = Vector2.zero;
            if (FlowchartWindow.Instance != null)
            {
                position = FlowchartWindow.Instance.GetNewNodePositionInGraph();
            }
            return AddBlock(index, position, allowUndo);
        }

        public virtual Block AddBlock(int index, Vector2 position, bool allowUndo = true)
        {
            Block block =  Block.CreateBlock(this, Block.DEFAULT_BLOCK_NAME, position, allowUndo);
            
            Block.AddBlockToList(this, m_Blocks, block, index, Block.DEFAULT_BLOCK_NAME, allowUndo);
            
            InvokePropertyChanged(nameof(m_Blocks));

            return block;
        }

        public virtual void RemoveBlock(Block block, bool allowUndo = true)
        {
            Block.RemoveBlockFromList(this, m_Blocks, block, allowUndo);
            Block.DestroyBlock(this, block, allowUndo);
            
            InvokePropertyChanged(nameof(m_Blocks));
        }

        public virtual void CutBlocks(Block[] blocks)
        {
            CopyBlocks(blocks.ToArray());

            foreach (Block block in blocks)
            {
                RemoveBlock(block);
            }

            ComponentClipboard.ClearClipboardOnNextPaste = true;
        }

        public virtual void CopyBlocks(Block[] blocks)
        {
            ComponentClipboard.CopyToBuffer(blocks);
        }

        public virtual void PasteBlocks(Vector2 position)
        {
            UnityEditor.Undo.RecordObject(this, "Paste Blocks");

            Block[] pastedBlocks = ComponentClipboard.PasteFromBuffer<Block>(gameObject);

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            foreach (Block block in pastedBlocks)
            {
                min.x = Mathf.Min(min.x, block.GetNodePosition().x);
                min.y = Mathf.Min(min.y, block.GetNodePosition().y);
            }

            Vector2[] pastedBlockOffsets = pastedBlocks.Select(i => i.GetNodePosition() - min).ToArray();

            for (int i = 0; i < pastedBlocks.Length; i++)
            {
                Block block = pastedBlocks[i] as Block;

                block.Setup();
                block.SetupName(m_Blocks, block.GetName(), Block.DEFAULT_BLOCK_NAME);
                block.SetNodePosition(position + pastedBlockOffsets[i]);

                if (m_Blocks == null)
                {
                    Debug.LogError("m_Blocks is null for this flowchart: " + this);
                }
                else if (block == null)
                {
                    Debug.LogError("A null block is attempting to be added to this flowchart: " + this);
                }
                else if (m_Blocks.Contains(block))
                {
                    Debug.LogError(string.Format("m_Blocks already contains the block {0} in this flowchart {1}: ", block.BlockName, this));
                }
                else
                {
                    // A new valid block has been found.  Add it.
                    m_Blocks.Add(block);
                }
            }

            this.m_graph.SelectNodes(pastedBlocks, Graph.SelectionType.Normal);
        }

        public virtual void DuplicateBlocks(Block[] blocks)
        {
            ComponentClipboard.CopyToBuffer(blocks);

            PasteBlocks(blocks[0].GetNodePosition() + new Vector2(140, 0));

            ComponentClipboard.ClearClipboard();
        }
#endif

        /// <summary>
        /// Find a Block by name.
        /// </summary>
        /// <param name="blockName">The name to search for</param>
        /// <returns>A block that has the specified name, or null if not found</returns>
        public virtual Block GetBlock(string blockName)
        {
            foreach (Block block in m_Blocks)
            {
                if (block.GetName() == blockName)
                {
                    return block;
                }
            }

            return null;
        }

        /// <summary>
        /// Find a Block by template.
        /// </summary>
        /// <param name="blockName">The template block to search for</param>
        /// <returns>A block that has the specified template block, or null if not found</returns>
        public virtual Block GetBlock(Block templateBlock)
        {
            Block block = GetBlock(templateBlock.GetName());
            if (block != null)
            {
                return block;
            }
            return null;
        }

        /// <summary>
        /// Find a Block by item id.
        /// </summary>
        /// <param name="blockItemId">The item id to search for</param>
        /// <returns>A block that has the specified item id, or null if not found</returns>
        public virtual Block GetBlock(int blockItemId)
        {
            foreach (Block block in m_Blocks)
            {
                if (block.ItemId == blockItemId)
                {
                    return block;
                }
            }

            return null;
        }

        public Flowchart GetRootTemplateFlowchart()
        {
            if (m_TemplateFlowchart == null)
            {
                return this;
            }

            return m_TemplateFlowchart.GetRootTemplateFlowchart();
        }

        public Flowchart GetRootChildFlowchart()
        {
            if (m_ChildFlowchart == null)
            {
                return this;
            }

            return m_ChildFlowchart.GetRootChildFlowchart();
        }

        public void RescanComponents()
        {
            m_Blocks.RemoveAll(i => i == null);
            m_Variables.RemoveAll(i => i == null);

            foreach (Block block in GetComponentsInChildren<Block>(true))
            {
                if (m_Blocks == null)
                {
                    Debug.LogError("m_Blocks is null this flowchart: " + this);
                }
                else if(block == null)
                {
                    Debug.LogError("A null block is attempting to be added to this flowchart: " + this);
                }
                else if (m_Blocks.Contains(block))
                {
                    Debug.LogError(string.Format("m_Blocks already contains the block {0} in this flowchart {1}: ", block.BlockName, this));
                }
                else
                {
                    // A new valid block has been found. Add it. 
                    m_Blocks.Add(block);
                }
            }
            foreach (Variable variable in GetComponentsInChildren<Variable>(true))
            {
                if (variable.ParentBlock != null)
                {
                    if (variable.VariableIndex >= 0 && !variable.ParentBlock.Parameters.Contains(variable))
                    {
                        variable.ParentBlock.Parameters.Add(variable);
                    }
                }
                else
                {
                    if (!m_Variables.Contains(variable))
                    {
                        m_Variables.Add(variable);
                    }
                }
            }
            foreach (Command command in GetComponentsInChildren<Command>(true))
            {
                if (command.ParentBlock != null)
                {
                    if (command.CommandIndex >= 0 && !command.ParentBlock.Commands.Contains(command))
                    {
                        command.ParentBlock.Commands.Add(command);
                    }
                }
            }

            m_Variables.OrderBy(x => x.VariableIndex).ToList();
            foreach (Block block in m_Blocks)
            {
                block.Commands.RemoveAll(x => x == null);
                block.Commands = block.Commands.OrderBy(x => x.CommandIndex).ToList();
                block.Parameters.RemoveAll(x => x == null);
                block.Parameters = block.Parameters.OrderBy(x => x.VariableIndex).ToList();
            }

            UpdateHideFlags();
#if UNITY_EDITOR
            RecordChanges();
#endif
        }        

       /// <summary>
       /// Sends a message to this Flowchart only.
       /// Any block with a matching MessageReceived event handler will start executing.
       /// </summary>
       /// <param name="message">The message to send</param>
        public virtual void SendFungusMessage(string message)
        {
            OnMessageDetected.Invoke(this, message);
        }

        /// <summary>
        /// Sends a message to all Flowchart objects in the current scene.
        /// Any block with a matching MessageReceived event handler will start executing.
        /// </summary>
        /// <param name="message">The message to send</param>
        public static void BroadcastFungusMessage(string message)
        {
            OnMessageDetected.Invoke(null, message);
        }

        /// <summary>
        /// Stop all executing Blocks in this Flowchart.
        /// </summary>
        public virtual void StopAllBlocks()
        {
            foreach (Block block in m_Blocks)
            {
                if (block.IsExecuting())
                {
                    block.Stop();
                }
            }
        }

        /// <summary>
        /// Gets a list of all variables with public scope in this Flowchart.
        /// </summary>
        /// <returns></returns>
        public virtual List<Variable> GetPublicVariables()
        {
            return m_Variables.FindAll(i => i != null && i.Scope == Scope.Public).ToList();
        }

        /// <summary>
        /// Finds a variable by name.
        /// The variable must already be added to the list of variables for this Flowchart.
        /// </summary>
        /// <param name="variableName">The name to search for</param>
        /// <returns>The variable that has the specified name, or null if not found</returns>
        public virtual Variable GetVariable(string variableName)
        {
            return Variable.GetVariableInList(m_Variables, variableName);
        }

        /// <summary>
        /// Gets the value of a variable.
        /// Returns false if the variable key does not exist.
        /// </summary>
        /// <param name="variableName">The name to search for</param>
        /// <returns>True, if variable exists. Otherwise, false.</returns>
        public virtual object GetVariableValue(string variableName)
        {
            Variable variable = GetVariable(variableName);
            if (variable != null)
            {
                return variable.Value;
            }
            return null;
        }

        public virtual object GetVariableValue(Variable templateVariable)
        {
            Variable variable = GetVariable(templateVariable.GetName());
            if (variable != null)
            {
                return variable.Value;
            }
            return null;
        }

        /// <summary>
        /// Sets the value of a variable.
        /// The variable must already be added to the list of variables for this Flowchart.
        /// </summary>
        /// <param name="variableName">The name to search for</param>
        /// <param name="value">The new value the variable will be set to</param>
        public virtual void SetVariableValue(string variableName, object value)
        {
            Variable variable = GetVariable(variableName);
            if (variable != null)
            {
                variable.Value = value;
            }
        }

        public virtual void SetVariableValue(Variable templateVariable, object value)
        {
            Variable variable = GetVariable(templateVariable.GetName());
            if (variable != null)
            {
                variable.Value = value;
            }
        }

        /// <summary>
        /// Set the block objects to be hidden or visible depending on the hideComponents property.
        /// </summary>
        public virtual void UpdateHideFlags()
        {
            if (m_HideComponents)
            {
                MonoBehaviour[] monoBehaviours = GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour monoBehaviour in monoBehaviours)
                {
                    if (monoBehaviour == null)
                    {
                        continue;
                    }

                    monoBehaviour.hideFlags = HideFlags.None;
                    monoBehaviour.gameObject.hideFlags = HideFlags.None;
                }

                HideFlags hideFlag = HideFlags.HideInInspector;

                foreach (Block block in m_Blocks)
                {
                    if (block != null)
                    {
                        block.hideFlags = hideFlag;

                        foreach (Variable parameter in block.Parameters)
                        {
                            if (parameter != null)
                            {
                                parameter.hideFlags = hideFlag;
                            }
                        }

                        if (block.GetEventHandler() != null)
                        {
                            block.GetEventHandler().hideFlags = hideFlag;
                        }

                        foreach (Command command in block.Commands)
                        {
                            if (command != null)
                            {
                                command.hideFlags = hideFlag;
                                foreach (ReturnValueCommand rvc in command.GetReturnValueCommandReferences())
                                {
                                    rvc.hideFlags = hideFlag;
                                }
                            }
                        }
                    }
                }

                foreach (Variable variable in m_Variables)
                {
                    if (variable != null)
                    {
                        variable.hideFlags = hideFlag;
                    }
                }
            }
            else
            {
                MonoBehaviour[] monoBehaviours = GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour monoBehaviour in monoBehaviours)
                {
                    if (monoBehaviour == null)
                    {
                        continue;
                    }

                    monoBehaviour.hideFlags = HideFlags.None;
                    monoBehaviour.gameObject.hideFlags = HideFlags.None;
                }
            }

        }

#if UNITY_EDITOR
        public virtual void PingFlowchart()
        {
            if (!Application.isPlaying)
            {
                UnityEditor.EditorGUIUtility.PingObject(gameObject);
            }
            UnityEditor.Selection.activeGameObject = gameObject;
        }
#endif

        public static void Activate(Flowchart targetFlowchart, bool instantly, UnityAction onComplete = null)
        {
            targetFlowchart.Log(FlowchartLoggingOptions.Flowchart, DebugLogType.Info, "ACTIVATE", targetFlowchart);

#if UNITY_EDITOR
            // Select the executing block & the first command
            if (targetFlowchart != s_previousActiveFlowchart)
            {
                SetSelectedFlowchart(targetFlowchart);
            }
#endif

            if (targetFlowchart != null && !targetFlowchart.isActiveAndEnabled)
            {
                s_previousActiveFlowchart = targetFlowchart;
                s_latestActivatedFlowchart = targetFlowchart;
            }

            UnityAction showComplete =
                () =>
                {
                    targetFlowchart.m_isActivating = false;
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                };

            targetFlowchart.m_isActivating = true;

            if (!targetFlowchart.gameObject.activeSelf)
            {
                if (targetFlowchart.UIConstruct != null)
                {
                    targetFlowchart.UIConstruct.gameObject.SetActive(false);
                }

                targetFlowchart.gameObject.SetActive(true);
            }

            if (targetFlowchart.UIConstruct != null)
            {
                if (instantly)
                {
                    targetFlowchart.UIConstruct.ShowInstantly(showComplete);
                }
                else
                {
                    targetFlowchart.UIConstruct.Show(showComplete);
                }
            }
            else
            {
                showComplete();
            }
        }

        public static void Deactivate(Flowchart targetFlowchart, bool instantly, UnityAction onComplete = null)
        {
            targetFlowchart.Log(FlowchartLoggingOptions.Flowchart, DebugLogType.Info, "DEACTIVATE", targetFlowchart);

            targetFlowchart.m_isDeactivating = true;
            UnityAction hideComplete =
                () =>
                {
                    targetFlowchart.gameObject.SetActive(false);
                    targetFlowchart.m_isDeactivating = false;

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                };

            if (targetFlowchart.UIConstruct != null)
            {
                if (instantly)
                {
                    targetFlowchart.UIConstruct.HideInstantly(true, hideComplete);
                }
                else
                {
                    targetFlowchart.UIConstruct.Hide(true, hideComplete);
                }
            }
            else
            {
                hideComplete();
            }
        }

        public static void SwitchToFlowchart(Flowchart deactivateFlowchart, Flowchart activateFlowchart, bool deactivateInstantly, bool activateInstantly, bool setPreviousFlowchart, UnityAction onComplete = null)
        {
            deactivateFlowchart.Log(FlowchartLoggingOptions.Flowchart, DebugLogType.Info, "SWITCH", activateFlowchart);

            if (activateFlowchart.gameObject.activeSelf && !activateFlowchart.m_isDeactivating)
            {
                if (onComplete != null)
                {
                    onComplete();
                }
                return;
            }
            if (activateFlowchart != null)
            {
                activateFlowchart.m_isSwitching = true;
                s_latestSwitchedToFlowchart = activateFlowchart;
            }
            if (deactivateFlowchart != null)
            {
                deactivateFlowchart.m_isSwitching = false;
                deactivateFlowchart.m_nextFlowchart = activateFlowchart;
            }
            if (setPreviousFlowchart)
            {
                activateFlowchart.m_previousFlowchart = deactivateFlowchart;
            }
            if ((deactivateFlowchart == activateFlowchart) || !deactivateFlowchart.gameObject.activeSelf)
            {
                Activate(activateFlowchart, activateInstantly, onComplete);
            }
            else
            {
                Deactivate(
                    deactivateFlowchart, 
                    deactivateInstantly,
                    () =>
                    {
                        if (activateFlowchart.m_isSwitching)
                        {
                            Activate(activateFlowchart, activateInstantly, onComplete);
                        }
                        else
                        {
                            if (onComplete != null)
                            {
                                onComplete();
                            }
                        }
                    });
            }
        }

        public virtual void Reset(bool resetVariables)
        {
            foreach (Block block in m_Blocks)
            {
                block.OnReset();
            }

            if (resetVariables)
            {
                foreach (Variable variable in m_Variables)
                {
                    variable.ResetValue();
                }
            }
        }

        /// <summary>
        /// Override this in a Flowchart subclass to filter which commands are shown in the Add Command list.
        /// </summary>
        /// <param name="commandInfo">Information about the command</param>
        /// <returns>True, if the command should be shown in the Add Command list. Otherwise, false.</returns>
        public virtual bool IsCommandSupported(CommandInfoAttribute commandInfo)
        {
            return true;
        }

        /// <summary>
        /// Returns true if there are any executing blocks in this Flowchart.
        /// </summary>
        /// <returns>True, if blocks are currently executing in this flowchart. Otherwise, false.</returns>
        public virtual bool HasExecutingBlocks()
        {
            foreach (Block block in m_Blocks)
            {
                if (block.IsExecuting())
                {
                    return true;
                }
            }
            return false;
        }

        public virtual Block[] GetExecutingBlocks()
        {
            return m_Blocks.Where(i => i.IsExecuting()).ToArray();
        }

        public virtual List<Block> GetSelectedBlocks()
        {
            return m_graph.GetSelectedNodes().Cast<Block>().ToList();
        }

        public virtual Block GetFirstSelectedBlock()
        {
            return m_graph.GetFirstSelectedNode() as Block;
        }

        public virtual void SetSelectedBlock(Block block)
        {
            m_graph.SelectNodes(new INode[] { block }, Graph.SelectionType.Normal);
        }

        public static void SetSelectedFlowchart(Flowchart flowchart)
        {
            if (flowchart != null)
            {
                s_appFlowGraph.SelectNodes(new INode[] { flowchart }, Graph.SelectionType.Normal);
#if UNITY_EDITOR
                UnityEditor.Selection.activeGameObject = flowchart.gameObject;
#endif
            }
        }

        private bool FlowchartHasInheritenceCycle()
        {
            Flowchart fastPtr = this;
            Flowchart slowPtr = this;

            bool isCircular = true;

            do
            {
                if (fastPtr.m_TemplateFlowchart == null || fastPtr.m_TemplateFlowchart.m_TemplateFlowchart == null) //List end found
                {
                    isCircular = false;
                    break;
                }

                fastPtr = fastPtr.m_TemplateFlowchart.m_TemplateFlowchart;
                slowPtr = slowPtr.m_TemplateFlowchart;
            } while (fastPtr != slowPtr);

            return isCircular;
        }

        public virtual List<Flowchart> GetTemplateFlowchartChain()
        {
            if (FlowchartHasInheritenceCycle())
            {
                return new List<Flowchart>();
            }
            List<Flowchart> flowchartsInChain = new List<Flowchart>();
            GetTemplateFlowchartChainInternal(flowchartsInChain);
            return flowchartsInChain;
        }

        private void GetTemplateFlowchartChainInternal(List<Flowchart> flowchartsInChain)
        {
            if (m_TemplateFlowchart != null)
            {
                flowchartsInChain.Add(m_TemplateFlowchart);
                m_TemplateFlowchart.GetTemplateFlowchartChainInternal(flowchartsInChain);
            }
        }
        
        public virtual string SubstituteVariables(string text)
        {
            if (text == null)
            {
                return null;
            }

            // Match the regular expression pattern against a text string.
            MatchCollection matches = TextTagParser.BALANCED_BRACKET_REGEX.Matches(text);
            object[] variableValues = new object[matches.Count];
            string[] variableNames = new string[matches.Count];
            for (int i = 0; i < matches.Count; i++)
            {
                Match match = matches[i];
                string variableTag = match.Value;
                if (variableTag.Length > 1 && variableTag[1] == '$')
                {
                    int startIndexOfVariableName = 2;
                    int endIndexOfVariableName = variableTag.IndexOf(":", StringComparison.Ordinal);
                    if (endIndexOfVariableName < 0)
                    {
                        endIndexOfVariableName = variableTag.Length - 1;
                    }
                    int variableNameLength = endIndexOfVariableName - startIndexOfVariableName;
                    string variableName = variableTag.Substring(startIndexOfVariableName, variableNameLength);
                    Variable variable = FindClosestVariable(this, variableName);
                    variableNames[i] = variableName;
                    if (variable != null)
                    {
                        variableValues[i] = variable.Value;
                        string indexModifiedVariableTag = variableTag.Substring(0, startIndexOfVariableName - 1) + i + variableTag.Substring(startIndexOfVariableName + variableNameLength);
                        text = text.Replace(variableTag, indexModifiedVariableTag);
                    }
                }
            }
            text = Smart.Format(text, variableValues);
            for (int i = 0; i < matches.Count; i ++)
            {
                text = text.Replace("{" + i + ":", "{$" + variableNames[i] + ":");
                text = text.Replace("{" + i + "}", "{$" + variableNames[i] + "}");
            }
            return text;
        }

        public virtual string FormatString(string text)
        {
            string newText = SubstituteVariables(text);
            newText = newText.ReplaceBraceTagsWithBracketTags();
            return newText;
        }

        /// <summary>
        /// Search for a variable by name, and find the one that's the closest match.
        /// Closeness is determined by distance from <paramref name="currentFlowchart"/>
        /// </summary>
        /// <param name="currentFlowchart">The flowchart to search first</param>
        /// <param name="variableName">The name to search for</param>
        /// <returns>
        /// The variable that has the specified name in the <paramref name="currentFlowchart"/>, 
        /// or a public variable that has the specified name in any flowchart in the scene, 
        /// or null if none found.</returns>
        public static Variable FindClosestVariable(Flowchart currentFlowchart, string variableName)
        {
            // Look for any matching variables in this Flowchart first (public or private)
            foreach (Variable variable in currentFlowchart.m_Variables)
            {
                if (variable == null)
                    continue;

                if (variable.GetName() == variableName)
                {
                    return variable;
                }
            }

            List<Flowchart> flowcharts = new List<Flowchart>();
            flowcharts.AddRange(GetAllActive<Flowchart>());
            flowcharts = flowcharts.Distinct().ToList();

            // Now search all public variables in all scene Flowcharts in the scene
            foreach (Flowchart flowchart in flowcharts)
            {
                if (flowchart == currentFlowchart)
                {
                    // We've already searched this flowchart
                    continue;
                }

                foreach (Variable variable in flowchart.m_Variables)
                {
                    if (variable == null)
                        continue;

                    if (variable.Scope == Scope.Public &&
                        variable.GetName() == variableName)
                    {
                        return variable;
                    }
                }
            }

            return null;
        }

        public virtual void Log(FlowchartLoggingOptions loggingOptions, DebugLogType logType, string logMessage, UnityEngine.Object obj, bool forceLog = false)
        {
            string message = string.Format("[{0}] {1} {2}", name, loggingOptions, logMessage);

            switch (logType)
            {
                case DebugLogType.Info:
                    if (forceLog || m_Logging.HasFlag(loggingOptions))
                    {
                        Debug.Log(message, obj);
                    }
                    break;
                case DebugLogType.Warning:
                    Debug.LogWarning(message, obj);
                    break;
                case DebugLogType.Error:
                    Debug.LogError(message, obj);
                    break;
            }

            OnLog.Invoke(this, loggingOptions, logType, message);
        }

#if UNITY_EDITOR
        public virtual void UpdateVariableNameReferences(string oldName, string newName, Scope scope)
        {
            List<Flowchart> flowcharts = new List<Flowchart>();
            flowcharts.Add(this);
            if (scope == Scope.Public)
            {
                flowcharts.AddRange(ResourceTracker.FindLoadedInstanceObjectsOfType<Flowchart>().ToList());
                flowcharts.AddRange(ResourceTracker.GetCachedPrefabObjectsOfType<Flowchart>().ToList());
                flowcharts = flowcharts.Distinct().ToList();
            }

            foreach (Flowchart flowchart in flowcharts)
            {
                foreach (Block block in flowchart.m_Blocks)
                {
                    if (block.EventHandler != null)
                    {
                        block.EventHandler.UpdateVariableNameReferences(oldName, newName);
                    }
                    foreach (Command command in block.Commands)
                    {
                        command.UpdateVariableNameReferences(oldName, newName);
                    }
                }
            }
        }

        public bool ContainsError()
        {
            foreach (Block block in m_Blocks)
            {
                if (block != null && block.ContainsError())
                {
                    return true;
                }
            }
            return false;
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TemplateFlowchart))
            {
                if (FlowchartHasInheritenceCycle())
                {
                    return "Inheritence cycle detected: You cannot make this flowchart use a template that also uses this flowchart as a template.";
                }
            }
            return base.GetPropertyError(propertyPath);
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            
            for (int i = 0; i < m_Variables.Count; i++)
            {
                Variable variable = m_Variables[i];
                if (variable != null)
                {
                    variable.VariableIndex = i;
                }
            }

            // Perform a check to remove any null blocks
            int removedNullBlocks = m_Blocks.RemoveAll(x => x == null);

            if (removedNullBlocks > 0)
            {
                Debug.LogError("Null block(s) found in m_Blocks. Removing them from this flowchart: " + this);
            }
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_Blocks))
            {
                return 1;
            }
            else if (propertyPath == nameof(m_Variables))
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
#endif

        // INode Implementation

        public Vector2 GetNodePosition()
        {
            return m_NodePosition;
        }

        public void SetNodePosition(Vector2 position)
        {
            m_NodePosition = position;
        }

        public float GetNodeExecutingIconTimer()
        {
            return m_executingIconTimer;
        }
        
        public void SetNodeExecutingIconTimer(float time)
        {
            m_executingIconTimer = time;
        }

        public string GetNodeName()
        {
            return name;
        }

        public void SetNodeName(string newNodeName)
        {
            name = newNodeName;
            
            InvokePropertyChanged("m_Name");
        }

        public string GetNodeDescription()
        {
            return m_Description;
        }

        public bool IsExecuting()
        {
            foreach (Block block in m_Blocks)
            {
                if (block.IsExecuting())
                {
                    return true;
                }
            }
            return false;
        }
        
        public IConnectNodes[] GetNodeConnections()
        {
            List<IConnectNodes> connections = new List<IConnectNodes>();
            if (this != null)
            {
                foreach (Block block in m_Blocks)
                {
                    if (block != null)
                    {
                        connections.AddRange(block.GetFlowchartCommands().OfType<IConnectNodes>());
                    }
                    else
                    {
                        Debug.LogError("A null block has been found in m_Blocks in this flowchart: " + this);
                    }
                }

            }
            return connections.ToArray();
        }
        
        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }
    }
}
