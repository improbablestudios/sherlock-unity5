using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;
using Fungus.Variables;
using System.Linq;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.SearchPopupUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{

    public class CommandInfoAttribute : FlowchartItemInfoAttribute
    {
        public CommandInfoAttribute(int priority = 0) : base(null, null, null, priority)
        {
        }

        public CommandInfoAttribute(string category, string description, int priority = 0) : base(null, category, description, priority)
        {
        }

        public CommandInfoAttribute(string name, string category, string description, int priority = 0) : base(category, name, description, priority)
        {
        }

        public static new CommandInfoAttribute GetInfo(Type variableType)
        {
            return GetInfo<CommandInfoAttribute>(variableType);
        }
    }

    public enum CallMode
    {
        Continue,           // Continue executing the current block after calling 
        WaitUntilFinished,  // Wait until the called block finishes executing, then continue executing current block
        Pause,              // Pause execution of the current block after calling 
        Stop,               // Stop execution of the current block after calling 
    }

    [CommandInfo]
    [AddComponentMenu("Fungus/Command")]
    public abstract class Command : FlowchartItem
    {
        [Serializable]
        public class CommandSaveState<T> : BehaviourSaveState<T> where T : Command
        {
        }

        [Tooltip("Wait until this command has finished before executing the next command")]
        [SerializeField]
        [FormerlySerializedAs("_waitUntilFinished")]
        protected BooleanData m_WaitUntilFinished = new BooleanData();

        [Tooltip("The indent level of this command")]
        [Min(0)]
        [HideInInspector]
        [SerializeField]
        protected int m_IndentLevel;

        [Tooltip("The position of this command in its parent block")]
        [HideInInspector]
        [SerializeField]
        private int m_CommandIndex = -1;

        protected int m_executionCount;
        
        protected bool m_isExecuting;

        /// <summary>
        /// Set to true the first time a command executes.
        /// </summary>
        protected bool m_hasExecuted;

        /// <summary>
        /// Timer used to control appearance of executing icon in inspector.
        /// </summary>
        private float m_executingIconTimer;

        public BooleanData WaitUntilFinished
        {
            get
            {
                return m_WaitUntilFinished;
            }
            set
            {
                m_WaitUntilFinished = value;
            }
        }

        public int IndentLevel
        {
            get
            {
                return m_IndentLevel;
            }
            set
            {
                m_IndentLevel = value;
            }
        }

        public int ExecutionCount
        {
            get
            {
                return m_executionCount;
            }
        }

        public int CommandIndex
        {
            get
            {
                return m_CommandIndex;
            }
            set
            {
                m_CommandIndex = value;
            }
        }

        public float ExecutingIconTimer
        {
            get
            {
                return m_executingIconTimer;
            }
            set
            {
                m_executingIconTimer = value;
            }
        }

        public bool HasExecuted
        {
            get
            {
                return m_hasExecuted;
            }
        }

        public bool IsExecuting
        {
            get
            {
                return m_isExecuting;
            }
            set
            {
                m_isExecuting = value;
            }
        }

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return true;
        }

        protected override bool WillInitializePersistableFieldsOnAwake()
        {
            return false;
        }

        public virtual void Execute()
        {
            m_hasExecuted = true;

            this.InitializePersistableFields();

            string error = GetFirstError();
            if (error != null)
            {
                Log(DebugLogType.Error, error);
                return;
            }

            try
            {
                Log(DebugLogType.Info, "EXECUTE");
                OnExecuteCallback.Invoke();
                OnExecuteAnyCallback.Invoke(this);
                OnEnter();
                m_executionCount++;
            }
            catch (System.Exception ex)
            {
                Log(DebugLogType.Error, ex.ToString());
            }
        }

        public void ContinueAfterFinished()
        {
            if (WillWait())
            {
                Continue();
            }
        }

        public void Continue()
        {
            // This is a noop if the Block has already been stopped
            if (m_isExecuting)
            {
                Continue(m_CommandIndex + 1);
            }
        }

        public void Continue(int nextCommandIndex)
        {
            OnExit();
            Block parentBlock = ParentBlock;
            if (parentBlock != null)
            {
                parentBlock.JumpToCommandIndex = nextCommandIndex;
            }
        }

        public virtual void CallBlock(Block targetBlock, CallMode callMode)
        {
            CallBlock(targetBlock, callMode, null, null);
        }

        public virtual void CallBlock(Block targetBlock, CallMode callMode, List<object> parameterValues, Variable returnVariable)
        {
            Block parentBlock = ParentBlock;
            // Check if calling your own parent block
            if (targetBlock == parentBlock)
            {
                // Just ignore the callmode in this case
                return;
            }

            // Callback action for Wait Until Finished mode
            UnityAction onComplete = null;
            if (callMode == CallMode.WaitUntilFinished)
            {
                onComplete = Continue;
            }

            targetBlock.Execute(parentBlock, parameterValues, returnVariable, onComplete);

            if (callMode == CallMode.Stop)
            {
                StopParentBlock();
            }
            else if (callMode == CallMode.Pause)
            {
                PauseParentBlock();
            }
            else if (callMode == CallMode.Continue)
            {
                Continue();
            }
        }

        public virtual void StopParentBlock()
        {
            Block parentBlock = ParentBlock;
            StopBlock(parentBlock);
        }

        public virtual void StopBlock(Block targetBlock)
        {
            Block parentBlock = ParentBlock;
            if (targetBlock == parentBlock)
            {
                OnExit();
                targetBlock.Stop();
            }
            else
            {
                targetBlock.Stop();
                Continue();
            }
        }

        public virtual void PauseParentBlock()
        {
            Block parentBlock = ParentBlock;
            PauseBlock(parentBlock);
        }

        public virtual void PauseBlock(Block targetBlock)
        {
            Block parentBlock = ParentBlock;
            if (targetBlock == parentBlock)
            {
                OnExit();
                targetBlock.Pause();
            }
            else
            {
                targetBlock.Pause();
                Continue();
            }
        }

        public virtual void ResumeBlock(Block targetBlock)
        {
            Block parentBlock = ParentBlock;
            if (targetBlock == parentBlock)
            {
                OnExit();
                targetBlock.Resume();
            }
            else
            {
                targetBlock.Resume();
                Continue();
            }
        }

        public virtual void RestartBlock(Block targetBlock)
        {
            Block parentBlock = ParentBlock;
            if (targetBlock == parentBlock)
            {
                OnExit();
                targetBlock.Restart(parentBlock);
            }
            else
            {
                targetBlock.Restart(parentBlock);
                Continue();
            }
        }

        /// <summary>
        /// Called when the new command is added to a block in the editor.
        /// </summary>
        public virtual void OnCommandAdded()
        {
        }

        /// <summary>
        /// Called when the command is deleted from a block in the editor.
        /// </summary>
        public virtual void OnCommandRemoved()
        {
        }

        public virtual void OnEnter()
        {
            Continue();
        }

        public virtual void OnExit()
        {
        }

        public override void OnReset()
        {
        }

        public virtual void OnPause()
        {
        }

        public virtual void OnResume()
        {
        }

        public virtual void OnStop()
        {
        }

        public virtual void OnComplete()
        {
        }

        public virtual bool ChangesScene()
        {
            return false;
        }

        public virtual bool QuitsApplication()
        {
            return false;
        }

        protected string GetDurationSummary(FloatData durationData, string prefix = "over")
        {
            if (!durationData.IsConstant || durationData.Value > 0)
            {
                return string.Format(" {0} {1} seconds", prefix, durationData);
            }
            return "";
        }

        /// <summary>
        /// This command starts a block of commands.
        /// </summary>
        /// <returns>Whether or not this command should start a new block</returns>
        public virtual bool OpenBlock()
        {
            return false;
        }

        /// <summary>
        /// This command ends a block of commands.
        /// </summary>
        /// <returns>Whether or not this command should close the current block</returns>
        public virtual bool CloseBlock()
        {
            return false;
        }

        public virtual bool WillSkipWhenLoading()
        {
            return false;
        }

        public virtual bool ShouldWait(bool runtime)
        {
            return false;
        }

        public bool WillWait()
        {
            return (ShouldWait(true) && (!IsWaitUntilFinishedPropertyVisible() || m_WaitUntilFinished.Value));
        }

        public bool ShowWaitIcon()
        {
            return (ShouldWait(false) && (!IsWaitUntilFinishedPropertyVisible() || (!m_WaitUntilFinished.IsConstant || m_WaitUntilFinished.Value)));
        }

        public virtual bool IsWaitUntilFinishedPropertyVisible()
        {
            return ShouldWait(false);
        }

        public virtual bool IsReturnVariablePropertyVisible()
        {
            return true;
        }

        public virtual bool IsDescriptionPropertyVisible()
        {
            return true;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_WaitUntilFinished))
            {
                return false;
            }
            if (propertyPath == "m_ReturnVariable")
            {
                return false;
            }
            if (propertyPath == "m_Description")
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (!Application.isPlaying || m_hasExecuted)
            {
                return base.GetPropertyError(propertyPath);
            }
            return null;
        }

        public override string[] GetErrors()
        {
            List<string> errors = new List<string>();

            foreach (ReturnValueCommand returnValueCommand in GetReturnValueCommandReferences())
            {
                string[] returnValueCommandErrors = returnValueCommand.GetErrors();
                if (returnValueCommandErrors.Length > 0)
                {
                    errors.AddRange(returnValueCommandErrors);
                }
            }

            return base.GetErrors();
        }

        public override string GetFirstError()
        {
            foreach (ReturnValueCommand returnValueCommand in GetReturnValueCommandReferences())
            {
                string returnValueCommandError = returnValueCommand.GetFirstError();
                if (returnValueCommandError != null)
                {
                    return returnValueCommandError;
                }
            }

            return base.GetFirstError();
        }

        public override string ToString()
        {
            return GetType().Name + "(" + GetArgumentsSummary() + ")";
        }

        public virtual string FormatString(string text)
        {
            Flowchart flowchart = GetFlowchart();

            if (flowchart != null)
            {
                return flowchart.SubstituteVariables(text);
            }

            return text;
        }

        public virtual string GetArgumentsSummary()
        {
            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
            FieldInfo[] fields = GetType().GetFields(flags);
            List<string> argumentStrings = new List<string>();
            foreach (FieldInfo field in fields)
            {
                if (!field.IsInitOnly &&
                    field.IsUnitySerializableField() &&
                    !Attribute.IsDefined(field, typeof(HideInInspector)))
                {
                    if (IsPropertyArgument(field.Name) && IsPropertyVisible(field.Name))
                    {
                        object value = field.GetValue(this);
                        string valueString = "<None>";
                        NullLabelAttribute[] nullLabelAttributes = field.GetCustomAttributes(true).OfType<NullLabelAttribute>().ToArray();
                        VariableData vd = value as VariableData;
                        if (value != null)
                        {
                            if (vd != null && vd.IsConstant && vd.Value == null && nullLabelAttributes.Length > 0)
                            {
                                valueString = nullLabelAttributes[0].NullLabel.text;
                            }
                            else if (!(typeof(UnityEngine.Object)).IsAssignableFrom(field.FieldType) || (UnityEngine.Object)value != null)
                            {
                                valueString = value.ToString();
                            }
                        }
                        else
                        {
                            if (nullLabelAttributes.Length > 0)
                            {
                                valueString = nullLabelAttributes[0].NullLabel.text;
                            }
                        }
                        argumentStrings.Add(valueString);
                    }
                }
            }
            return string.Join(",", argumentStrings.ToArray());
        }

        public virtual bool IsPropertyArgument(string propertyPath)
        {
            return (propertyPath != "m_Script" &&
                propertyPath != "m_Key" &&
                propertyPath != "m_ReturnVariable" &&
                propertyPath != "m_Description" &&
                propertyPath != nameof(m_Key) &&
                (propertyPath != nameof(m_WaitUntilFinished) || IsWaitUntilFinishedPropertyVisible()));
        }

        public void Log(DebugLogType logType, string logMessage)
        {
            Block parentBlock = ParentBlock;
            string message = string.Format("'{0} ({1}:{2})': {3}", GetType().Name, parentBlock, m_CommandIndex, logMessage);

            base.Log(FlowchartLoggingOptions.Command, logType, message);
        }

        public override string GetOrderedId()
        {
            Block block = GetFlowchart().Blocks.Find(i => i.Commands.Contains(this));
            if (block != null)
            {
                int blockIndex = GetFlowchart().Blocks.IndexOf(block);
                int commandIndex = block.Commands.IndexOf(this);
                return base.GetOrderedId() + "." + "COMMAND" + "." + blockIndex.ToString("D20") + "." + commandIndex.ToString("D20");
            }
            return null;
        }
    }

}
