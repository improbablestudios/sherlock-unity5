﻿namespace Fungus
{

    public abstract class InertCommand : Command
    {
        public sealed override void OnEnter()
        {
            Continue();
        }
    }

}