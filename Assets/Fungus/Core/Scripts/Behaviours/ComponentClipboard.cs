﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using System.Reflection;
using System.Collections;
using System.Linq;
using UnityEditor;
using ImprobableStudios.ReflectionUtilities;

namespace Fungus
{
    
    public class ComponentClipboard : MonoBehaviour
    {
        private static ComponentClipboard s_instance;

        private static bool s_clearClipboardOnNextPaste;

        private Dictionary<Component, Component> m_topLevelComponentsInBuffer = new Dictionary<Component, Component>();

        private Dictionary<Component, Component> m_originalToBufferedMap = new Dictionary<Component, Component>();

        private Dictionary<Component, Component> m_bufferedToNewMap = new Dictionary<Component, Component>();

        private Type m_copiedComponentType;

        protected static ComponentClipboard Instance
        {
            get
            {
                if (s_instance == null)
                {
                    ComponentClipboard[] clipboards = Resources.FindObjectsOfTypeAll<ComponentClipboard>();
                    foreach (ComponentClipboard clipboard in clipboards)
                    {
                        s_instance = clipboards[0];
                        break;
                    }
                }
                if (s_instance == null)
                {
                    s_instance = new GameObject("~Clipboard").AddComponent<ComponentClipboard>();
                    s_instance.gameObject.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
                    s_instance.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
                }
                return s_instance;
            }
        }

        public static bool ClearClipboardOnNextPaste
        {
            get
            {
                return s_clearClipboardOnNextPaste;
            }
            set
            {
                s_clearClipboardOnNextPaste = value;
            }
        }

        ComponentClipboard()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }
        
        protected virtual void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                ClearClipboard();
            }
        }

        public static bool HasCopiedComponents<T>() where T : Component
        {
            if (s_instance != null)
            {
                return s_instance.m_copiedComponentType == typeof(T) && s_instance.m_topLevelComponentsInBuffer.Count > 0;
            }

            return false;
        }

        public static T CopyToBuffer<T>(T component, bool copyDependencies = true) where T : Component
        {
            return CopyToBuffer<T>(new T[] { component }, copyDependencies)[0];
        }

        public static T[] CopyToBuffer<T>(T[] components, bool copyDependencies = true) where T : Component
        {
            List<T> pastedComponents = new List<T>();

            ClearClipboard();

            if (Instance != null)
            {
                s_instance.m_copiedComponentType = typeof(T);

                foreach (Component component in components)
                {
                    if (ComponentUtility.CopyComponent(component))
                    {
                        if (ComponentUtility.PasteComponentAsNew(s_instance.gameObject))
                        {
                            T pastedComponent = s_instance.GetComponents<T>().Last();

                            pastedComponents.Add(pastedComponent);

                            if (!s_instance.m_topLevelComponentsInBuffer.ContainsKey(pastedComponent))
                            {
                                s_instance.m_topLevelComponentsInBuffer.Add(pastedComponent, component);
                            }

                            if (!s_instance.m_originalToBufferedMap.ContainsKey(component))
                            {
                                s_instance.m_originalToBufferedMap.Add(component, pastedComponent);
                            }

                            pastedComponent.hideFlags = HideFlags.HideInInspector | HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
                        }
                    }
                }

                if (copyDependencies)
                {
                    for (int i = 0; i < pastedComponents.Count; i++)
                    {
                        s_instance.ReassignDependencies(false, s_instance.m_originalToBufferedMap, components[i].gameObject, components[i].gameObject, pastedComponents[i].gameObject, components[i], pastedComponents[i]);
                    }
                }

                // This stops the user pasting the command manually into another game object.
                ComponentUtility.CopyComponent(s_instance.transform);
            }

            return pastedComponents.ToArray();
        }

        public static T[] PasteFromBuffer<T>(GameObject targetGameObject, bool pasteDependencies = true) where T : Component
        {
            List<T> pastedComponents = new List<T>();

            if (Instance != null)
            {
                if (s_instance.m_copiedComponentType != typeof(T))
                {
                    return new Component[0].Cast<T>().ToArray();
                }

                foreach (Component component in s_instance.m_topLevelComponentsInBuffer.Keys)
                {
                    if (ComponentUtility.CopyComponent(component))
                    {
                        if (ComponentUtility.PasteComponentAsNew(targetGameObject))
                        {
                            T pastedComponent = targetGameObject.GetComponents<T>().Last();
                            
                            pastedComponents.Add(pastedComponent);

                            if (!s_instance.m_bufferedToNewMap.ContainsKey(component))
                            {
                                s_instance.m_bufferedToNewMap.Add(component, pastedComponent);
                            }
                        }
                    }
                }

                if (pasteDependencies)
                {
                    for (int i = 0; i < s_instance.m_topLevelComponentsInBuffer.Keys.Count; i++)
                    {
                        KeyValuePair<Component, Component> kvp = s_instance.m_topLevelComponentsInBuffer.ElementAt(i);
                        GameObject originalGameObject = null;
                        if (kvp.Value != null)
                        {
                            originalGameObject = kvp.Value.gameObject;
                        }
                        s_instance.ReassignDependencies(true, s_instance.m_bufferedToNewMap, originalGameObject, kvp.Key.gameObject, pastedComponents[i].gameObject, kvp.Key, pastedComponents[i]);
                    }
                }

                // This stops the user pasting the command manually into another game object.
                ComponentUtility.CopyComponent(targetGameObject.transform);
            }

            if (s_clearClipboardOnNextPaste)
            {
                ClearClipboard();
            }

            return pastedComponents.ToArray();
        }

        public static void ClearClipboard()
        {
            if (s_instance != null)
            {
                DestroyImmediate(s_instance.gameObject);
            }
            else
            {
                ComponentClipboard[] clipboards = Resources.FindObjectsOfTypeAll<ComponentClipboard>();
                foreach (ComponentClipboard clipboard in clipboards)
                {
                    DestroyImmediate(clipboard.gameObject);
                }
            }
        }

        private void ReassignDependencies(bool paste, Dictionary<Component, Component> fromToMap, GameObject originalGameObject, GameObject fromGameObject, GameObject toGameObject, object fromObj, object toObj, Type type = null, object toParentObj = null, FieldInfo field = null, PropertyInfo property = null, int index = -1)
        {
            //Debug.Log(fromObj + " -> " + toObj);
            if (fromObj == null)
            {
                return;
            }

            type = fromObj.GetType();

            if (type.IsPrimitive || type.IsEnum || type == typeof(string) || type == typeof(object))
            {
                return;
            }

            if (typeof(UnityEngine.Object).IsAssignableFrom(type) && !typeof(Component).IsAssignableFrom(type) && toParentObj == null)
            {
                return;
            }

            FlowchartItem flowchartItem = fromObj as FlowchartItem;

            if (flowchartItem != null && field != null && field.Name == "m_" + nameof(flowchartItem.ParentFlowchartItem))
            {
                return;
            }

            if (typeof(Component).IsAssignableFrom(type) && toParentObj != null)
            {
                Component component = fromObj as Component;

                if (component != null && component.gameObject != fromGameObject)
                {
                    return;
                }

                Block block = component as Block;
                Command command = component as Command;
                EventHandler eventHandler = component as EventHandler;
                Variable variable = component as Variable;

                Component componentInBuffer = null;

                if (paste &&
                   ((variable != null && typeof(Flowchart).IsAssignableFrom(toParentObj.GetType())) ||
                   (block != null && !typeof(EventHandler).IsAssignableFrom(toParentObj.GetType()) && (originalGameObject == toGameObject || !m_topLevelComponentsInBuffer.ContainsKey(component)))))
                {
                    componentInBuffer = m_originalToBufferedMap.FirstOrDefault(x => x.Value == component).Key;
                }
                else
                {
                    if (fromToMap.ContainsKey(component) && (command == null && eventHandler == null && variable == null))
                    {
                        componentInBuffer = fromToMap[component];
                    }
                    else
                    {
                        if (ComponentUtility.CopyComponent(component))
                        {
                            if (ComponentUtility.PasteComponentAsNew(toGameObject))
                            {
                                componentInBuffer = toGameObject.GetComponents(type).Last();

                                if (!fromToMap.ContainsKey(component))
                                {
                                    fromToMap.Add(component, componentInBuffer);
                                }

                                componentInBuffer.hideFlags = HideFlags.HideInInspector | HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;

                                ReassignDependencies(paste, fromToMap, originalGameObject, component.gameObject, componentInBuffer.gameObject, component, componentInBuffer);
                            }
                        }
                    }
                }

                if (field != null)
                {
                    field.SetValue(toParentObj, componentInBuffer);
                }
                else if (property != null)
                {
                    property.SetValue(toParentObj, componentInBuffer, new object[] { index });
                }
            }
            else if (typeof(IList).IsAssignableFrom(type))
            {
                IList fromList = (IList)fromObj;
                IList toList = (IList)toObj;

                if (fromList != null)
                {
                    Type itemType = type.GetItemType();

                    if (toList == null)
                    {
                        if (type.IsArray)
                        {
                            toList = Array.CreateInstance(itemType, fromList.Count);
                        }
                        else
                        {
                            toList = Activator.CreateInstance(type) as IList;
                        }
                    }

                    if (!type.IsArray)
                    {
                        while (fromList.Count > toList.Count)
                        {
                            toList.Add(null);
                        }
                    }

                    PropertyInfo itemProperty = type.GetProperty("Item");

                    for (int i = 0; i < fromList.Count; i++)
                    {
                        Type itemExactType = itemType;
                        if (fromList[i] != null)
                        {
                            itemExactType = fromList[i].GetType();
                        }
                        object fromListItem = fromList[i];
                        object toListItem = toList[i];
                        ReassignDependencies(paste, fromToMap, originalGameObject, fromGameObject, toGameObject, fromListItem, toListItem, itemExactType, toObj, null, itemProperty, i);
                    }
                }
            }
            else
            {
                FieldInfo[] fields = type.GetUnitySerializedFields();
                foreach (FieldInfo fieldInfo in fields)
                {
                    object fromFieldObj = null;
                    if (fromObj != null)
                    {
                        fromFieldObj = fieldInfo.GetValue(fromObj);
                    }

                    object toFieldObj = null;
                    if (toObj != null)
                    {
                        toFieldObj = fieldInfo.GetValue(toObj);
                    }

                    ReassignDependencies(paste, fromToMap, originalGameObject, fromGameObject, toGameObject, fromFieldObj, toFieldObj, fieldInfo.FieldType, toObj, fieldInfo, null);
                }
            }
        }
    }
}
#endif