﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImprobableStudios.ReflectionUtilities;

namespace Fungus
{

    public abstract class NumericReturnValueCommand : ReturnValueCommand
    {
        public abstract object GetReturnValue();

        public sealed override Type[] SpecificReturnTypes()
        {
            return new Type[] { typeof(float), typeof(int) };
        }

        public sealed override Type ReturnType()
        {
            return typeof(object);
        }

        public sealed override object ReturnValueObject()
        {
            return GetReturnValue();
        }
    }

    public abstract class ReturnValueCommand<T> : ReturnValueCommand
    {
        public abstract T GetReturnValue();

        public override Type[] SpecificReturnTypes()
        {
            return new Type[] { typeof(T) };
        }

        public sealed override Type ReturnType()
        {
            return typeof(T);
        }

        public sealed override object ReturnValueObject()
        {
            return GetReturnValue();
        }
    }

    public abstract class ReturnValueCommand : Command
    {
        public abstract Type[] SpecificReturnTypes();
        public abstract Type ReturnType();
        public abstract object ReturnValueObject();
        
        public static Type[] GetReturnValueCommandTypeEquivalents(Type[] types)
        {
            List<Type> commandTypes = new List<Type>();
            foreach (Type type in types)
            {
                foreach (Type t in (typeof(ReturnValueCommand<>)).FindAllDerivedTypesOfGenericType())
                {
                    Type baseGenericType = t.GetBaseGenericType();
                    if (baseGenericType != null)
                    {
                        if (type.IsAssignableFrom(baseGenericType.GetGenericArguments()[0]))
                        {
                            if (!commandTypes.Contains(baseGenericType))
                            {
                                commandTypes.Add(baseGenericType);
                            }
                        }
                    }
                }
            }
            if (types.Contains(typeof(float)) || types.Contains(typeof(int)))
            {
                commandTypes.Add(typeof(ReturnValueCommand<float>));
                commandTypes.Add(typeof(ReturnValueCommand<int>));
                commandTypes.Add(typeof(NumericReturnValueCommand));
            }
            return commandTypes.ToArray();
        }

        public override void OnEnter()
        {
            Variable returnVariable = GetReturnVariable();
            if (returnVariable == null)
            {
                Continue();
                return;
            }

            returnVariable.Value = ReturnValueObject();

            Continue();
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (m_ParentFlowchartItem is Block)
            {
                if (propertyPath == GetReturnVariablePropertyPath())
                {
                    if (GetReturnVariable() == null)
                    {
                        return "No Return Variable";
                    }
                }
            }
            return base.GetPropertyError(propertyPath);
        }

        public abstract Variable GetReturnVariable();

        public virtual string GetReturnVariablePropertyPath()
        {
            return "m_ReturnVariable";
        }
    }

}
