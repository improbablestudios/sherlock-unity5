using UnityEngine;
using UnityEngine.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Fungus
{
    
    [AddComponentMenu("Fungus/Block")]
    public class Block : FlowchartItem, ISavable, INode
    {
        public enum ExecutionState
        {
            Idle,
            Executing,
        }

        [Serializable]
        public class BlockSaveState : BehaviourSaveState<Block>
        {
            public override void Load(Block block)
            {
                base.Load(block);

                if (block.m_executionState == Block.ExecutionState.Executing)
                {
                    int commandIndex = block.ActiveCommand.CommandIndex;

                    while (commandIndex >= 0 && commandIndex < block.m_Commands.Count)
                    {
                        Command command = block.m_Commands[commandIndex];
                        if (!command.WillSkipWhenLoading())
                        {
                            break;
                        }
                        commandIndex++;
                    }
                    if (commandIndex < block.m_Commands.Count)
                    {
                        block.ExecuteAt(commandIndex, block.GetParameterValues(), block.GetReturnVariable());
                    }
                }
            }

            public override int LoadPriority()
            {
                return 100;
            }
        }

        ///<summary>
        ///The default name of a new block.
        ///</summary>
        public const string DEFAULT_BLOCK_NAME = "NEW BLOCK";

        ///<summary>
        ///The default name of a new flowchart variable.
        ///</summary>
        public const string DEFAULT_BLOCK_PARAMETER_NAME = "Param";

        [Tooltip("The block that this block inherits commands from")]
        [SerializeField]
        protected Block m_TemplateBlock;

        [Tooltip("The block that inherits commands from this block")]
        [NonSerialized]
        protected Block m_ChildBlock;
        
        [SerializeField]
        [FormerlySerializedAs("nodePosition")]
        protected Vector2 m_NodePosition;

        [Tooltip("The name of the block node as displayed in the Flowchart window")]
        [SerializeField]
        [FormerlySerializedAs("blockName")]
        protected string m_BlockName = "";

        [Tooltip("Determines which flowcharts can edit or reference this block")]
        [SerializeField]
        protected Scope m_Scope;

        [TextArea(2, 5)]
        [Tooltip("Editor only description text to display under the block node")]
        [SerializeField]
        [FormerlySerializedAs("description")]
        protected string m_Description = "";

        [Tooltip("The input parameters this block will take when called")]
        [SerializeField]
        protected List<Variable> m_Parameters = new List<Variable>();

        [Tooltip("The type of output this block will return")]
        [SerializeField]
        [VariableValueTypeNameField]
        protected string m_ReturnTypeName;

        [Tooltip("An optional Event Handler which can execute the block when an event occurs")]
        [ContextMenuItem("Copy", "CopyEventHandler")]
        [ContextMenuItem("Paste", "PasteEventHandler")]
        [SerializeField]
        [FormerlySerializedAs("eventHandler")]
        protected EventHandler m_EventHandler;

        [Tooltip("The list of commands that this block will execute")]
        [SerializeField]
        [FormerlySerializedAs("commandList")]
        [FormerlySerializedAs("m_CommandList")]
        protected List<Command> m_Commands = new List<Command>();
        
        protected UnityEvent m_onExecutionFinished = new UnityEvent();
        
        protected UnityEvent m_onNextExecutionFinished = new UnityEvent();
        
        protected int m_executionCount;
        
        protected Block.ExecutionState m_executionState;
        
        protected Command m_activeCommand;
        
        protected int m_previousActiveCommandIndex = -1;
        
        protected int m_jumpToCommandIndex = -1;

        protected int m_loopToCommandIndex = -1;
        
        protected Block m_callingBlock;
        
        protected Variable m_returnVariable;
        
        protected bool m_forceExecution;
        
        protected Coroutine m_executionCoroutine;

        private bool m_executionInfoSet = false;

        private float m_ExecutingIconTimer;

        private static Block s_lastExecutedBlock;

        private Command m_lastExecutedCommand;

        public Block TemplateBlock
        {
            get
            {
                return m_TemplateBlock;
            }
            set
            {
                m_TemplateBlock = value;
            }
        }

        public Block ChildBlock
        {
            get
            {
                return m_ChildBlock;
            }
            set
            {
                m_ChildBlock = value;
            }
        }

        public Vector2 NodePosition
        {
            get
            {
                return m_NodePosition;
            }
            set
            {
                m_NodePosition = value;
            }
        }

        public string BlockName
        {
            get
            {
                return m_BlockName;
            }
            set
            {
                m_BlockName = value;
            }
        }

        public Scope Scope
        {
            get
            {
                return m_Scope;
            }
            set
            {
                m_Scope = value;
            }
        }

        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }

        public List<Variable> Parameters
        {
            get
            {
                return m_Parameters;
            }
            set
            {
                m_Parameters = value;
            }
        }

        public string ReturnTypeName
        {
            get
            {
                return m_ReturnTypeName;
            }
            set
            {
                m_ReturnTypeName = value;
            }
        }

        public EventHandler EventHandler
        {
            get
            {
                return m_EventHandler;
            }
            set
            {
                SetEventHandler(value);
            }
        }

        public List<Command> Commands
        {
            get
            {
                return m_Commands;
            }
            set
            {
                m_Commands = value;
            }
        }

        public UnityEvent OnExecutionFinished
        {
            get
            {
                return GetRootChildBlock().m_onExecutionFinished;
            }
        }

        public UnityEvent OnNextExecutionFinished
        {
            get
            {
                return GetRootChildBlock().m_onNextExecutionFinished;
            }
        }

        public Command ActiveCommand
        {
            get
            {
                return m_activeCommand;
            }
        }

        public int ExecutionCount
        {
            get
            {
                return m_executionCount;
            }
        }
        
        public int LoopToCommandIndex
        {
            get
            {
                return m_loopToCommandIndex;
            }
            set
            {
                m_loopToCommandIndex = value;
            }
        }

        public int JumpToCommandIndex
        {
            get
            {
                return m_jumpToCommandIndex;
            }
            set
            {
                m_jumpToCommandIndex = value;
            }
        }

        public Variable ReturnVariable
        {
            get
            {
                return m_returnVariable;
            }
            set
            {
                m_returnVariable = value;
            }
        }

        public Command LastExecutedCommand
        {
            get
            {
                return m_lastExecutedCommand;
            }
        }

        public virtual SaveState GetSaveState()
        {
            if (m_ChildBlock == null)
            {
                return new BlockSaveState();
            }

            return null;
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            Flowchart flowchart = GetFlowchart();

            if (flowchart == null)
            {
                return;
            }

            if (!flowchart.Blocks.Contains(this))
            {
                flowchart.Blocks.Add(this);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            SetExecutionState(ExecutionState.Idle);
        }
        
        protected override void Awake()
        {
            base.Awake();

            SetExecutionInfo();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            
            for (int i = 0; i < m_Commands.Count; i++)
            {
                Command command = m_Commands[i];
                if (command != null)
                {
                    command.CommandIndex = i;
                    foreach (ReturnValueCommand returnValueCommand in command.GetReturnValueCommandReferences())
                    {
                        if (returnValueCommand != null)
                        {
                            returnValueCommand.CommandIndex = -1;
                        }
                    }
                }
            }
            for (int i = 0; i < m_Parameters.Count; i++)
            {
                Variable parameter = m_Parameters[i];
                if (parameter != null)
                {
                    parameter.VariableIndex = i;
                }
            }
        }

        public virtual void SetupName(List<Block> blockList, string name, string defaultName)
        {
            string uniqueName = name;
            if (name != null)
            {
                uniqueName = GetUniqueBlockName(blockList, name, defaultName);
            }
            SetName(uniqueName);
        }

#if UNITY_EDITOR
        public static bool ValidateBlocksInList(Component componentContainingList, List<Block> blockList, List<Block> templateBlockList, string defaultName, Scope defaultScope, bool deleteBlockIfTemplateInvalid)
        {
            bool changed = false;

            if (blockList == null)
            {
                return changed;
            }

            foreach (Block block in blockList.ToList())
            {
                if (block == null)
                {
                    blockList.Remove(block);
                    changed = true;
                }
                else
                {
                    if (!block.enabled)
                    {
                        block.enabled = true;
                        changed = true;
                    }

                    if (block.IsTemplateBlockPrivate() || block.IsTemplateBlockNotInTemplateFlowchart() || block.WasTemplateBlockDestroyed())
                    {
                        if (deleteBlockIfTemplateInvalid || block.Commands.Count == 0)
                        {
                            RemoveBlockFromList(componentContainingList, blockList, block);
                            DestroyBlock(componentContainingList, block);
                        }
                        else
                        {
                            block.m_NodePosition = block.GetNodePosition();
                            if (block.GetName() != null)
                            {
                                block.m_BlockName = block.GetName();
                            }
                            else
                            {
                                block.m_BlockName = defaultName;
                            }
                            block.m_Description = block.GetDescription();
                            if (block.GetRootTemplateBlock() != block)
                            {
                                if (block.GetRootTemplateBlock().EventHandler != null)
                                {
                                    block.GetRootTemplateBlock().CopyEventHandler();
                                    block.PasteEventHandler();
                                }
                                else
                                {
                                    block.RemoveEventHandler();
                                }
                            }
                            block.m_TemplateBlock = null;
                        }
                        changed = true;
                    }
                }
            }

            foreach (Block templateBlock in templateBlockList)
            {
                if (templateBlock.Scope != Scope.Private)
                {
                    if (blockList.Find(i => i.m_TemplateBlock == templateBlock) == null)
                    {
                        Block blockWithSameName = GetBlockInList(blockList, templateBlock.GetName());
                        if (blockWithSameName != null)
                        {
                            blockWithSameName.m_TemplateBlock = templateBlock;
                            blockWithSameName.m_NodePosition = Vector2.zero;
                            blockWithSameName.m_BlockName = null;
                            blockWithSameName.m_Scope = 0;
                            blockWithSameName.m_Description = "";
                            blockWithSameName.RemoveEventHandler();
                        }
                        else
                        {
                            if (componentContainingList != null)
                            {
                                Flowchart flowchart = componentContainingList.GetComponent<Flowchart>();
                                if (flowchart != null)
                                {
                                    Block overrideBlock = CreateBlock(componentContainingList, null, Vector2.zero);
                                    AddBlockToList(componentContainingList, blockList, overrideBlock, blockList.Count, defaultName);
                                    overrideBlock.m_TemplateBlock = templateBlock;
                                    overrideBlock.m_BlockName = null;
                                    if (overrideBlock.ValidateParameters())
                                    {
                                        changed = true;
                                    }
                                }
                            }
                        }
                        changed = true;
                    }
                }
            }

            return changed;
        }

        protected bool IsTemplateBlockPrivate()
        {
            return ((m_TemplateBlock != null) && (m_TemplateBlock.Scope == Scope.Private));
        }

        protected bool IsTemplateBlockNotInTemplateFlowchart()
        {
            return ((m_TemplateBlock != null) && (m_TemplateBlock.GetFlowchart() != GetFlowchart().TemplateFlowchart));
        }

        protected bool WasTemplateBlockDestroyed()
        {
            return ((m_TemplateBlock == null && m_BlockName == null));
        }

        public bool ValidateParameters()
        {
            List<Variable> templateParameters = new List<Variable>();
            if (m_TemplateBlock != null)
            {
                templateParameters = m_TemplateBlock.Parameters;
            }
            return Variable.ValidateVariablesInList(this, m_Parameters, templateParameters, DEFAULT_BLOCK_PARAMETER_NAME, Scope.Protected, true, this);
        }
#endif

        public Block GetRootTemplateBlock()
        {
            if (m_TemplateBlock == null)
            {
                return this;
            }

            return m_TemplateBlock.GetRootTemplateBlock();
        }

        public Block GetRootChildBlock()
        {
            if (m_Scope == Scope.Private || m_ChildBlock == null)
            {
                return this;
            }

            return m_ChildBlock.GetRootChildBlock();
        }

        public override string GetName()
        {
            return GetRootTemplateBlock().m_BlockName;
        }

        public void SetName(string blockName)
        {
            if (blockName != null)
            {
                m_BlockName = blockName.ToUpper();
            }
            else
            {
                m_BlockName = blockName;
            }
        }

        public Scope GetScope()
        {
            return GetRootChildBlock().GetScopeInternal();
        }

        protected Scope GetScopeInternal()
        {
            return m_Scope;
        }

        public string GetDescription()
        {
            return GetRootTemplateBlock().m_Description;
        }

        public void SetDescription(string description)
        {
            m_Description = description;
        }

        public EventHandler GetEventHandler()
        {
            return GetRootTemplateBlock().m_EventHandler;
        }

        public void SetEventHandler(EventHandler eventHandler)
        {
#if UNITY_EDITOR
            Undo.RecordObject(this, "Set Eventhandler");
#endif

            eventHandler.Setup(this);
            m_EventHandler = eventHandler;

            RecordChanges();
            InvokePropertyChanged(nameof(m_EventHandler));
        }

        public List<Variable> GetParameters()
        {
            return GetRootChildBlock().m_Parameters;
        }

        public List<object> GetParameterValues()
        {
            return GetParameters().Select(i => i.Value).ToList();
        }

        public Type GetReturnType()
        {
            string returnTypeName = GetRootTemplateBlock().m_ReturnTypeName;
            if (!string.IsNullOrEmpty(returnTypeName))
            {
                return Type.GetType(returnTypeName);
            }

            return null;
        }

        public void SetReturnType(Type returnType)
        {
            m_ReturnTypeName = returnType.AssemblyQualifiedName;
        }

        public Variable GetReturnVariable()
        {
            return GetRootChildBlock().m_returnVariable;
        }

        public void SetReturnVariable(Variable variable)
        {
            GetRootChildBlock().m_returnVariable = variable;
        }

        public List<Command> GetCombinedCommandList()
        {
            List<Command> commandList = new List<Command>();
            GetCombinedCommandListInternal(commandList);
            return commandList;
        }

        private void GetCombinedCommandListInternal(List<Command> commandList)
        {
            if (m_Commands != null)
            {
                commandList.AddRange(m_Commands);
            }
            if (m_TemplateBlock != null)
            {
                m_TemplateBlock.GetCombinedCommandListInternal(commandList);
            }
        }

        public virtual List<Block> GetTemplateBlockChain()
        {
            List<Block> blocksInChain = new List<Block>();
            GetTemplateBlockChainInternal(blocksInChain);
            return blocksInChain;
        }

        private void GetTemplateBlockChainInternal(List<Block> blocksInChain)
        {
            if (m_TemplateBlock != null)
            {
                blocksInChain.Add(m_TemplateBlock);
                m_TemplateBlock.GetTemplateBlockChainInternal(blocksInChain);
            }
        }

        public override string ToString()
        {
            return GetName();
        }

        public override void OnReset()
        {
            m_executionCount = 0;
        }

        public virtual void SetExecutionInfo()
        {
            // Give each child command a reference back to its parent block
            // and tell each command its index in the list.
            int index = 0;
            foreach (Command command in m_Commands)
            {
                if (command == null)
                {
                    continue;
                }

                command.ParentFlowchartItem = this;
                command.CommandIndex = index++;
            }

            if (m_EventHandler != null)
            {
                m_EventHandler.ParentFlowchartItem = this;
            }

            m_executionInfoSet = true;
        }

        public virtual int GetExecutionCount()
        {
            return GetRootChildBlock().GetExecutionCountInternal();
        }

        protected int GetExecutionCountInternal()
        {
            return m_executionCount;
        }

        public virtual void SetExecutionState(ExecutionState executionState)
        {
            m_executionState = executionState;

            if (executionState == ExecutionState.Idle)
            {
                m_activeCommand = null;
                m_jumpToCommandIndex = -1;

                if (m_TemplateBlock == null)
                {
                    InvokeExecutionFinishedEvent();
                }
            }
        }

        public bool ExecuteAt(int commandIndex, List<object> parameterValues, Variable returnVariable, UnityAction onComplete = null)
        {
            m_jumpToCommandIndex = commandIndex;
            return Execute(this, true, parameterValues, returnVariable, onComplete);
        }

        [ContextMenu("Execute", true)]
        private bool ExecuteValidation()
        {
            return Application.isPlaying;
        }

        [ContextMenu("Execute")]
        public bool Execute()
        {
            return Execute(null);
        }

        public bool Execute(UnityAction onComplete)
        {
            return Execute(this, false, null, null, onComplete);
        }

        public bool Execute(List<object> parameterValues, Variable returnVariable, UnityAction onComplete = null)
        {
            return Execute(this, false, parameterValues, returnVariable, onComplete);
        }

        public bool Execute(Block callingBlock, List<object> parameterValues, Variable returnVariable, UnityAction onComplete = null)
        {
            return Execute(callingBlock, false, parameterValues, returnVariable, onComplete);
        }

        protected virtual bool Execute(Block callingBlock, bool forceExecution, List<object> parameterValues, Variable returnVariable, UnityAction onComplete = null)
        {
            if (onComplete != null)
            {
                OnNextExecutionFinished.AddPersistentListener(onComplete);
            }
            return GetRootChildBlock().ExecuteInternal(callingBlock, forceExecution, parameterValues, returnVariable);
        }

        protected bool ExecuteInternal(Block callingBlock, bool forceExecution, List<object> parameterValues, Variable returnVariable)
        {
            if (!Application.isPlaying)
            {
                return false;
            }

            Flowchart flowchart = GetFlowchart();
            if (flowchart == null)
            {
                return false;
            }

            if (!isActiveAndEnabled)
            {
                return false;
            }

            // Can't restart a running block, have to wait until it's idle again
            if (!forceExecution && IsExecuting())
            {
                return false;
            }
            
            if (parameterValues != null)
            {
                List<Variable> parameters = GetParameters();
                for (int i = 0; i < parameters.Count; i++)
                {
                    Variable parameter = parameters[i];
                    object parameterValue = parameterValues[i];
                    parameter.Value = parameterValue;
                }
            }
            else
            {
                List<Variable> parameters = GetParameters();
                for (int i = 0; i < parameters.Count; i++)
                {
                    Variable parameter = parameters[i];
                    parameter.Value = parameter.OriginalValue;
                }
            }
            
            if (returnVariable != null)
            {
                returnVariable.Value = null;
                SetReturnVariable(returnVariable);
            }
            else
            {
                SetReturnVariable(null);
            }

            if (!m_executionInfoSet)
            {
                SetExecutionInfo();
            }

            m_callingBlock = callingBlock;
            m_forceExecution = forceExecution;

            m_executionCount++;
            
            UnityAction onExecuteComplete = null;
            if (m_TemplateBlock != null)
            {
                onExecuteComplete = m_TemplateBlock.ExecuteInternal;
            }
            
            Log(DebugLogType.Info, "EXECUTE");

            if (gameObject.activeInHierarchy)
            {
                m_executionCoroutine = StartCoroutine(ExecuteBlock(onExecuteComplete));
            }

            return true;
        }

        private void ExecuteInternal()
        {
            ExecuteInternal(m_callingBlock, m_forceExecution, GetParameterValues(), GetReturnVariable());
        }

        protected virtual IEnumerator ExecuteBlock(UnityAction onComplete = null)
        {
            SetExecutionState(ExecutionState.Executing);

#if UNITY_EDITOR
            Flowchart flowchart = GetFlowchart();
            // Select the executing block & the first command
            if (this != s_lastExecutedBlock)
            {
                flowchart.SetSelectedBlock(this);
                Flowchart.SetSelectedFlowchart(flowchart);
            }
            s_lastExecutedBlock = this;
#endif

            OnExecuteCallback.Invoke();
            OnExecuteAnyCallback.Invoke(this);

            int i = 0;
            while (true)
            {
                // Executing commands specify the next command to skip to by setting jumpToCommandIndex using Command.Continue()
                if (m_jumpToCommandIndex > -1)
                {
                    i = m_jumpToCommandIndex;
                    m_jumpToCommandIndex = -1;
                }

                // Skip disabled and inert commands
                while (i < m_Commands.Count && (!m_Commands[i].enabled || m_Commands[i] is InertCommand))
                {
                    i = m_Commands[i].CommandIndex + 1;
                }

                if (i >= m_Commands.Count)
                {
                    if (m_loopToCommandIndex > -1)
                    {
                        i = m_loopToCommandIndex;
                        m_loopToCommandIndex = -1;
                    }
                    else
                    {
                        break;
                    }
                }

                // The previous active command is needed for if / else / else if commands
                if (m_activeCommand == null)
                {
                    m_previousActiveCommandIndex = -1;
                }
                else
                {
                    m_previousActiveCommandIndex = m_activeCommand.CommandIndex;
                }

                Command command = m_Commands[i];
                m_activeCommand = command;
                
                command.IsExecuting = true;
#if UNITY_EDITOR
                // This icon timer is managed by the FlowchartWindow class, but we also need to
                // set it here in case a command starts and finishes execution before the next window update.
                command.ExecutingIconTimer = Time.realtimeSinceStartup + GraphWindow.EXECUTING_ICON_FADE_TIME;
                m_ExecutingIconTimer = Time.realtimeSinceStartup + GraphWindow.EXECUTING_ICON_FADE_TIME;
#endif

                SetExecutionState(ExecutionState.Executing);
                m_lastExecutedCommand = command;
                command.Execute();

                // Wait until the executing command sets another command to jump to via Command.Continue()
                while (m_jumpToCommandIndex == -1)
                {
                    yield return null;
                }

#if UNITY_EDITOR
                if (flowchart.StepPause > 0f)
                {
                    yield return new WaitForSeconds(flowchart.StepPause);
                }
#endif

                command.IsExecuting = false;
            }

            SetExecutionState(ExecutionState.Idle);

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public virtual void Stop()
        {
            GetRootChildBlock().StopInternal();
        }

        protected void StopInternal()
        {
            // Tell the executing command to stop
            if (m_activeCommand != null)
            {
                m_activeCommand.IsExecuting = false;
                m_activeCommand.OnStop();
            }

            if (m_executionCoroutine != null)
            {
                StopCoroutine(m_executionCoroutine);
                m_executionCoroutine = null;
            }

            SetExecutionState(ExecutionState.Idle);

            if (m_TemplateBlock != null)
            {
                m_TemplateBlock.StopInternal();
            }
        }

        public virtual void Pause()
        {
            GetRootChildBlock().PauseInternal();
        }

        protected void PauseInternal()
        {
            // Tell the executing command to pause
            if (m_activeCommand != null)
            {
                m_activeCommand.IsExecuting = false;
                m_activeCommand.OnPause();
            }

            if (m_TemplateBlock != null)
            {
                m_TemplateBlock.PauseInternal();
            }
        }

        public virtual void Resume()
        {
            GetRootChildBlock().ResumeInternal();
        }

        protected void ResumeInternal()
        {
            // Tell the executing command to resume
            if (m_activeCommand != null)
            {
                m_activeCommand.IsExecuting = true;
                m_activeCommand.OnResume();
            }
        }

        public virtual void Restart(Block callingBlock)
        {
            GetRootChildBlock().RestartInternal(callingBlock);
        }

        protected void RestartInternal(Block callingBlock)
        {
            // Tell the executing command to resume
            if (m_activeCommand != null)
            {
                m_activeCommand.OnStop();
                m_activeCommand.IsExecuting = false;
            }

            m_jumpToCommandIndex = 0;

            if (m_executionState == ExecutionState.Idle)
            {
                Execute(callingBlock, true, GetParameterValues(), GetReturnVariable());
            }
        }

        public virtual Block GetCallingBlock()
        {
            return GetRootChildBlock().GetCallingBlockInternal();
        }

        protected Block GetCallingBlockInternal()
        {
            return m_callingBlock;
        }

        public virtual Command[] GetBlockCommands()
        {
            List<Command> commands = new List<Command>();
            
            foreach (Command command in GetCombinedCommandList())
            {
                if (command != null)
                {
                    IConnectNodes connectedCommand = command as IConnectNodes;
                    if (connectedCommand != null)
                    {
                        if (typeof(Block).IsAssignableFrom(connectedCommand.GetNodeType()))
                        {
                            commands.Add(command);
                        }
                    }
                }
            }
            return commands.ToArray();
        }

        public virtual Command[] GetFlowchartCommands()
        {
            List<Command> commands = new List<Command>();

            foreach (Command command in GetCombinedCommandList())
            {
                if (command != null)
                {
                    IConnectNodes connectedCommand = command as IConnectNodes;
                    if (connectedCommand != null)
                    {
                        if (typeof(Flowchart).IsAssignableFrom(connectedCommand.GetNodeType()))
                        {
                            commands.Add(command);
                        }
                    }
                }
            }
            return commands.ToArray();
        }
        
        public virtual System.Type GetPreviousActiveCommandType()
        {
            if (m_previousActiveCommandIndex >= 0 &&
                m_previousActiveCommandIndex < m_Commands.Count)
            {
                return m_Commands[m_previousActiveCommandIndex].GetType();
            }

            return null;
        }

        private void InvokeExecutionFinishedEvent()
        {
            OnExecutionFinished.Invoke();
            OnNextExecutionFinished.Invoke();
            OnNextExecutionFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        public void Log(DebugLogType logType, string logMessage)
        {
            string message = string.Format("'({0})': {1}", this, logMessage);

            Log(FlowchartLoggingOptions.Block, logType, message);
        }

        public static Block GetBlockInList(List<Block> blockList, string blockName)
        {
            foreach (Block b in blockList)
            {
                if (b.GetName() == blockName)
                {
                    return b;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a new Block name that is guaranteed not to clash with any existing Block in a list.
        /// </summary>
        /// <param name="blocks">The block list</param>
        /// <param name="newName">The name you want to use</param>
        /// <param name="defaultName">The name that should be used if <paramref name="newName"/> is null or empty</param>
        /// <returns>A unique block name</returns>
        public string GetUniqueBlockName(List<Block> blocks, string newName, string defaultName)
        {
            newName = newName.ToUpper();

            string[] names = blocks.Where(x => x != null).Select(x => x.GetName()).ToArray();
            int ignoreIndex = blocks.IndexOf(this);

            return names.GetUniqueName(newName, defaultName, ignoreIndex);
        }

#if UNITY_EDITOR
        public virtual bool ContainsError()
        {
            foreach (Command command in m_Commands)
            {
                if (command != null)
                {
                    if (command.GetFirstCachedError() != null)
                    {
                        return true;
                    }
                }
            }

            EventHandler eventHandler = GetEventHandler();
            if (eventHandler != null)
            {
                if (eventHandler.GetFirstCachedError() != null)
                {
                    return true;
                }
            }

            return false;
        }

        public static Block CreateBlock(Component componentContainingList, string name, Vector2 position, bool allowUndo = true)
        {
            Block block = null;
            if (allowUndo)
            {
                block = Undo.AddComponent(componentContainingList.gameObject, typeof(Block)) as Block;
            }
            else
            {
                block = componentContainingList.gameObject.AddComponent(typeof(Block)) as Block;
            }

            if (block.IsValid())
            {
                block.SetName(name);
                block.SetNodePosition(position);
            }
            
            return block;
        }

        public static void AddBlockToList(Component componentContainingList, List<Block> blockList, Block block, int index, string defaultName, bool allowUndo = true)
        {
            if (blockList != null && block != null)
            {
                if (allowUndo)
                {
                    Undo.RecordObject(componentContainingList, "Add Block");
                }

                block.Setup();
                block.SetupName(blockList, block.BlockName, defaultName);

                if (!blockList.Contains(block))
                {
                    if (index >= 0 && index < blockList.Count)
                    {
                        blockList.Insert(index, block);
                    }
                    else
                    {
                        blockList.Add(block);
                    }
                }
                block.GetFlowchart().SetSelectedBlock(block);
                
                BaseBehaviour baseBehaviour = componentContainingList as BaseBehaviour;
                if (baseBehaviour != null)
                {
                    baseBehaviour.RecordChanges();
                }
            }
        }

        public static void DestroyBlock(Component componentContainingList, Block block, bool allowUndo = true)
        {
            if (block != null)
            {
                block.DestroyReferences(allowUndo);

                if (allowUndo)
                {
                    Undo.DestroyObjectImmediate(block);
                }
                else
                {
                    DestroyImmediate(block);
                }
            }
        }

        public static void RemoveBlockFromList(Component componentContainingList, List<Block> blockList, Block block, bool allowUndo = true)
        {
            if (blockList != null && block != null)
            {
                if (allowUndo)
                {
                    Undo.RecordObject(componentContainingList, "Remove Block");
                }

                if (blockList.Contains(block))
                {
                    blockList.Remove(block);
                }
            }
        }
        
        public virtual Command AddCommand(Type type, int index, bool allowUndo = true)
        {
            if (allowUndo)
            {
                Undo.RecordObject(this, "Add Command");
            }

            Command newCommand = Undo.AddComponent(gameObject, type) as Command;

            if (newCommand != null && newCommand.IsValid())
            {
                newCommand.Setup(this);

                if (index >= 0 && index < m_Commands.Count)
                {
                    m_Commands.Insert(index, newCommand);
                    newCommand.CommandIndex = index;
                }
                else
                {
                    m_Commands.Add(newCommand);
                    newCommand.CommandIndex = m_Commands.Count - 1;
                }

                // Let command know it has just been added to the block
                newCommand.OnCommandAdded();
            }
            
            InvokePropertyChanged(nameof(m_Commands));

            return newCommand;
        }
        
        public void RemoveCommand(Command command, bool allowUndo = true)
        {
            if (allowUndo)
            {
                Undo.RecordObject(this, "Delete Command");
            }

            if (command != null)
            {
                command.OnCommandRemoved();

                this.m_Commands.Remove(command);

                command.DestroyReferences(allowUndo);

                if (allowUndo)
                {
                    Undo.DestroyObjectImmediate(command);
                }
                else
                {
                    DestroyImmediate(command);
                }
            }
            
            InvokePropertyChanged(nameof(m_Commands));
        }

        public virtual EventHandler AddEventHandler(Type type, bool allowUndo = true)
        {
            if (allowUndo)
            {
                Undo.RecordObject(this, "Add EventHandler");
            }

            EventHandler newEventHandler = Undo.AddComponent(gameObject, type) as EventHandler;

            if (newEventHandler.IsValid())
            {
                SetEventHandler(newEventHandler);
            }
            
            return newEventHandler;
        }

        public virtual void RemoveEventHandler(bool allowUndo = true)
        {
            if (m_EventHandler != null)
            {
                if (allowUndo)
                {
                    Undo.RecordObject(this, "Remove EventHandler");
                }

                m_EventHandler.DestroyReferences(allowUndo);

                if (allowUndo)
                {
                    Undo.DestroyObjectImmediate(m_EventHandler);
                }
                else
                {
                    DestroyImmediate(m_EventHandler);
                }

                m_EventHandler = null;

                InvokePropertyChanged(nameof(m_EventHandler));
            }
        }

        /// <summary>
        /// Destroys all referenced components
        /// </summary>
        /// <param name="allowUndo">Allow the user to undo these deletions</param>
        public override void DestroyReferences(bool allowUndo)
        {
            RemoveEventHandler(allowUndo);
            foreach (Variable parameter in m_Parameters.ToList())
            {
                RemoveParameter(parameter, allowUndo);
            }
            foreach (Command command in m_Commands.ToList())
            {
                RemoveCommand(command, allowUndo);
            }
        }

        public void CopyEventHandler()
        {
            if (m_EventHandler != null)
            {
                ComponentClipboard.CopyToBuffer<EventHandler>(m_EventHandler, false);
            }
        }

        public void PasteEventHandler()
        {
            EventHandler[] pastedEventHandlers = ComponentClipboard.PasteFromBuffer<EventHandler>(gameObject, false);
            if (pastedEventHandlers.Length > 0)
            {
                Undo.RecordObject(this, "Paste EventHandler");
                if (m_EventHandler != null)
                {
                    Undo.DestroyObjectImmediate(m_EventHandler);
                }
                SetEventHandler(pastedEventHandlers[0]);
            }
        }

        public virtual Variable AddParameter(Type type, bool allowUndo = true)
        {
            return AddParameter(type, m_Parameters.Count, allowUndo);
        }

        public virtual Variable AddParameter(Type type, int index, bool allowUndo = true)
        {
            Variable parameter = Variable.CreateVariable(this, type, DEFAULT_BLOCK_PARAMETER_NAME, this, Scope.Protected, allowUndo);

            Variable.AddVariableToList(this, m_Parameters, parameter, index, DEFAULT_BLOCK_PARAMETER_NAME, allowUndo);
            
            InvokePropertyChanged(nameof(m_Parameters));

            return parameter;
        }
        
        public virtual void RemoveParameter(Variable variable, bool allowUndo = true)
        {
            Variable.RemoveVariableFromList(this, m_Parameters, variable, allowUndo);
            Variable.DestroyVariable(this, variable, allowUndo);
            
            InvokePropertyChanged(nameof(m_Parameters));
        }
#endif

        // INode Implementation

        public Vector2 GetNodePosition()
        {
            if (m_TemplateBlock == null)
            {
                return m_NodePosition;
            }
            else
            {
                return m_TemplateBlock.GetNodePosition();
            }
        }

        public void SetNodePosition(Vector2 position)
        {
            if (m_TemplateBlock == null)
            {
                m_NodePosition = position;
            }
        }

        public float GetNodeExecutingIconTimer()
        {
            return m_ExecutingIconTimer;
        }

        public void SetNodeExecutingIconTimer(float time)
        {
            m_ExecutingIconTimer = time;
#if UNITY_EDITOR
            if (ActiveCommand != null)
            {
                ActiveCommand.ExecutingIconTimer = Time.realtimeSinceStartup + GraphWindow.EXECUTING_ICON_FADE_TIME;
            }
#endif
        }

        public string GetNodeName()
        {
            return GetName();
        }

        public void SetNodeName(string newNodeName)
        {
            SetName(newNodeName);
            
            InvokePropertyChanged(nameof(m_BlockName));
        }

        public string GetNodeDescription()
        {
            return GetDescription();
        }

        public bool IsExecuting()
        {
            return Application.isPlaying && (m_executionState == ExecutionState.Executing);
        }
        
        public IConnectNodes[] GetNodeConnections()
        {
            return GetBlockCommands().OfType<IConnectNodes>().ToArray();
        }
        
        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }
    }
}
