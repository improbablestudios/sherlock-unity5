using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Stop execution of all blocks in a flowchart")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Stop Flowchart")]
    public class StopFlowchart : Command, IConnectNodes
    {
        [Tooltip("Stop all executing blocks in the target flowchart")]
        [ObjectDataPopup("<This>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Flowchart flowchart = m_Flowchart.Value;

            if (flowchart == null)
            {
                flowchart = GetFlowchart();
            }

            flowchart.StopAllBlocks();

            Continue();
        }

        public override string GetSummary()
        {
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                return "";
            }

            return string.Format("{0}", m_Flowchart);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Flowchart);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            if (enabled)
            {
                return new string[] { nameof(m_Flowchart) + "." + "m_" + nameof(m_Flowchart.DataVal) };
            }
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}