using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Loop", 
                 "Force a loop to terminate immediately.")]
    [AddComponentMenu("Fungus/Commands/Flow/Loop/Break")]
    public class Break : Command
    {
        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            // Find index of previous while command
            int whileIndex = -1;
            int whileIndentLevel = -1;
            for (int i = CommandIndex - 1; i >=0; --i)
            {
                While whileCommand = parentBlock.Commands[i] as While;
                if (whileCommand != null)
                {
                    whileIndex = i;
                    whileIndentLevel = whileCommand.IndentLevel;
                    break;
                }
            }

            if (whileIndex == -1)
            {
                // No enclosing While command found, just continue
                Continue();
                return;
            }

            // Find matching End statement at same indent level as While
            for (int i = whileIndex + 1; i < parentBlock.Commands.Count; ++i)
            {
                End endCommand = parentBlock.Commands[i] as End;
                
                if (endCommand != null && 
                    endCommand.IndentLevel == whileIndentLevel)
                {
                    // Sanity check that break command is actually between the While and End commands
                    if (CommandIndex > whileIndex && CommandIndex < endCommand.CommandIndex)
                    {
                        // Continue at next command after End
                        Continue (endCommand.CommandIndex + 1);
                        return;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            // No matching End command found so just continue
            Continue();
        }
    }
    
}