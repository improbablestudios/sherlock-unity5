﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using System;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow",
                 "Return a value from this block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Return")]
    public class Return : Command
    {
        [Tooltip("The value to return from this block")]
        public GenericVariableData m_Value = new GenericVariableData();
        
        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            Variable returnVariable = parentBlock.GetReturnVariable();
            if (returnVariable != null)
            {
                returnVariable.Value = m_Value.Value;
            }

            OnExit();
            
            StopBlock(parentBlock);
        }
        
        public override string GetSummary()
        {
            Block parentBlock = ParentBlock;

            if (parentBlock != null && parentBlock.GetReturnType() != null)
            {
                return m_Value.ToString();
            }

            return base.GetSummary();
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                Block parentBlock = ParentBlock;

                if (parentBlock != null)
                {
                    m_Value.SetValueType(parentBlock.GetReturnType());
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                Block parentBlock = ParentBlock;

                if (parentBlock != null && parentBlock.GetReturnType() == null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}