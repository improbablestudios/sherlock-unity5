﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Get the flowchart that was last switched to.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Get Latest Switched To Flowchart")]
    public class GetLatestSwitchedToFlowchart : ReturnValueCommand<Flowchart>
    {
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public FlowchartVariable m_ReturnVariable;
        
        public override Flowchart GetReturnValue()
        {
            return Flowchart.LatestSwitchedToFlowchart;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = Latest Switched To Flowchart", m_ReturnVariable);
        }
    }

}