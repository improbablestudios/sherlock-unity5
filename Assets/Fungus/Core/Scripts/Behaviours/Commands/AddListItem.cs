﻿using System;
using System.Collections;
using UnityEngine;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityEditorUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Add an item to the end of a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Add List Item")]
    public class AddListItem : Command
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;
        
        [Tooltip("The item to add")]
        public GenericVariableData m_Value = new GenericVariableData();

        public override void OnEnter()
        {
            IList list = m_List.Value as IList;
            object value = m_Value.Value;

            list.Add(value);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("'{0}' to \"{1}\"", m_Value, m_List);
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List != null)
                {
                    IList list = m_List.Value as IList;
                    Type listType = list.GetType();
                    Type itemType = listType.GetItemType();
                    m_Value.SetValueType(itemType);
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List == null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}