﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Block",
                 "Get the block that called this block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Block/Get Calling Block")]
    public class GetCallingBlock : ReturnValueCommand<Block>
    {
        [Tooltip("The variable to store the result in")]
        public BlockVariable m_ReturnVariable;
        
        public override Block GetReturnValue()
        {
            Block parentBlock = ParentBlock;
            if (parentBlock != null)
            {
                return parentBlock.GetCallingBlock();
            }

            return null;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = <Calling Block>", m_ReturnVariable);
        }
    }
}
