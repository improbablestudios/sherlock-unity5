﻿using ImprobableStudios.BaseUtilities;
using System.Collections;
using UnityEngine;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Remove all items from a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Clear List")]
    public class ClearList : Command
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;
        
        public override void OnEnter()
        {
            IList list = m_List.Value as IList;

            list.Clear();

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_List);
        }
    }

}