using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Conditional", 
                 "Mark the start of a command block to be executed when the preceding If statement is false.")]
    [AddComponentMenu("Fungus/Commands/Flow/Conditional/Else")]
    public class Else : Command
    {
        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            if (parentBlock == null)
            {
                return;
            }

            // Last command in the list
            if (CommandIndex >= parentBlock.Commands.Count - 1)
            {
                Continue(int.MaxValue);
                return;
            }

            // Find the next End command at the same indent level as this Else command
            int indent = IndentLevel;
            for (int i = CommandIndex + 1; i < parentBlock.Commands.Count; ++i)
            {
                Command command = parentBlock.Commands[i];
                
                if (command.IndentLevel == indent)
                {
                    System.Type type = command.GetType();
                    if (type == typeof(End))
                    {
                        // Execute command immediately after the EndIf command
                        Continue(command.CommandIndex + 1);
                        return;
                    }
                }
            }

            // No End command found
            Continue(int.MaxValue);
        }

        public override bool OpenBlock()
        {
            return true;
        }

        public override bool CloseBlock()
        {
            return true;
        }
    }

}