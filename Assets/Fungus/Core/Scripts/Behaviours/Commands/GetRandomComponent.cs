﻿using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random component value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Component")]
    public class GetRandomComponent : ReturnValueCommand<Component>
    {
        [Tooltip("The game object to get the components from")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("Include children")]
        public BooleanData m_IncludeChildren = new BooleanData(true);

        [Tooltip("Include inactive children")]
        public BooleanData m_IncludeInactive = new BooleanData();

        [Tooltip("The variable to store the result in")]
        public ComponentVariable m_ReturnVariable;
        
        public override Component GetReturnValue()
        {
            GameObject go = m_Object.Value;
            bool includeChildren = m_IncludeChildren.Value;
            bool includeInactive = m_IncludeInactive.Value;
            Component[] components = null;
            if (includeChildren)
            {
                components = go.GetComponentsInChildren(m_ReturnVariable.ValueType, includeInactive);
            }
            else
            {
                components = go.GetComponents(m_ReturnVariable.ValueType);
                if (!includeInactive)
                {
                    components = components.Where(i => ComponentIsEnabled(i)).ToArray();
                }
            }
            if (components.Length > 0)
            {
                return components[Random.Range(0, (components.Length - 1))];
            }
            return null;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        private bool ComponentIsEnabled(Component component)
        {
            Behaviour behaviour = component as Behaviour;
            Renderer renderer = component as Renderer;

            if (behaviour != null)
            {
                return behaviour.enabled;
            }
            if (renderer != null)
            {
                return renderer.enabled;
            }

            return false;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random component on {1}", m_ReturnVariable, m_Object);
        }
    }

}