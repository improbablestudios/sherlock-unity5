using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Block",
                 "Stop execution of a block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Block/Stop Block")]
    public class StopBlock : BlockCommand, IConnectNodes
    {
        [Tooltip("Editor only description text to display next to the connection node that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Block block = m_Block.Value;

            OnExit();

            if (block == null)
            {
                Block parentBlock = ParentBlock;

                block = parentBlock;
            }

            StopBlock(block);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            if (enabled)
            {
                return new string[] { nameof(m_Block) + "." + "m_" + nameof(m_Block.DataVal) };
            }
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}