﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random bounds value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Bounds")]
    public class GetRandomBounds : ReturnValueCommand<Bounds>
    {
        [Tooltip("Minimum value for random range")]
        public BoundsData m_MinValue = new BoundsData();

        [Tooltip("Maximum value for random range")]
        public BoundsData m_MaxValue = new BoundsData();

        [Tooltip("The variable to store the result in")]
        public BoundsVariable m_ReturnVariable;
        
        public override Bounds GetReturnValue()
        {
            return RandomBounds(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Bounds RandomBounds(Bounds min, Bounds max)
        {
            return new Bounds(RandomVector2(min.center, max.center), RandomVector2(min.size, max.size));
        }

        public static Vector2 RandomVector2(Vector2 min, Vector2 max)
        {
            return new Vector2(RandomFloat(min.x, max.x), RandomFloat(min.y, max.y));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random bounds", m_ReturnVariable);
        }
    }

}