﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{
    
    public abstract class BlockCommand : Command
    {
        [Tooltip("The target block")]
        [ObjectDataPopup("<This>")]
        [FormerlySerializedAs("m_TargetBlock")]
        public BlockData m_Block = new BlockData();
        
        public override string GetSummary()
        {
            if (m_Block.IsConstant && m_Block.Value == null)
            {
                return "";
            }

            return string.Format("{0}", m_Block);
        }
    }

}