﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Get the length of a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Get List Length")]
    public class GetListLength : ReturnValueCommand<int>
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        public Variable m_List;

        [Tooltip("The integer variable to store the result in")]
        public IntegerVariable m_ReturnVariable;
        
        public override int GetReturnValue()
        {
            IList list = m_List.Value as IList;
            
            return list.Count;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = length of \"{1}\"", m_ReturnVariable, m_List);
        }
    }

}