using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Application",
                 "Quit the application. (Does not work in Webplayer builds. Shouldn't generally be used on iOS.)")]
    [AddComponentMenu("Fungus/Commands/Flow/Application/Quit")]
    public class Quit : Command 
    {
        public override void OnEnter()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif

            // On platforms that don't support Quit we just continue onto the next command
            Continue();
        }

        public override bool QuitsApplication()
        {
            return true;
        }
    }

}