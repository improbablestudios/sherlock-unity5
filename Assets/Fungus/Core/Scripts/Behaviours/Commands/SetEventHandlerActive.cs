using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/EventHandler",
                 "Enable or disable an event handler.")]
    [AddComponentMenu("Fungus/Commands/Flow/EventHandler/Set Event Handler Active")]
    public class SetEventHandlerActive : Command
    {
        [Tooltip("The target event handler")]
        [FormerlySerializedAs("_eventHandler")]
        [ObjectDataPopup("<This>")]
        public EventHandlerData m_EventHandler = new EventHandlerData();

        [Tooltip("Enable the event handler")]
        [FormerlySerializedAs("_active")]
        public BooleanData m_Active = new BooleanData();

        public override void OnEnter ()
        {
            EventHandler eventHandler = m_EventHandler.Value;
            bool active = m_Active.Value;

            if (eventHandler == null)
            {
                eventHandler = ParentBlock.EventHandler;
            }

            eventHandler.enabled = active;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("{0} active = {1}", m_EventHandler, m_Active);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_EventHandler))
            {
                if ((Application.isPlaying || m_EventHandler.IsConstant) && m_EventHandler.Value == null && ParentBlock.EventHandler == null)
                {
                    return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                }
            }

            return base.GetPropertyError(propertyPath);
        }
    }
    
}