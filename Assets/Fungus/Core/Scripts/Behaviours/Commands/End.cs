using UnityEngine;
using System;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Conditional", 
                 "Mark the end of a conditional block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Conditional/End")]
    public class End : Command
    {
        [Serializable]
        public class EndState : CommandSaveState<End>
        {
        }
        
        protected int m_loopToCommandIndex = -1;

        public int LoopToCommandIndex
        {
            get
            {
                return m_loopToCommandIndex;
            }
            set
            {
                m_loopToCommandIndex = value;
            }
        }

        public virtual SaveState GetState()
        {
            return new EndState();
        }
        
        public override void OnEnter()
        {
            if (m_loopToCommandIndex > -1)
            {
                int nextCommandIndex = m_loopToCommandIndex;
                m_loopToCommandIndex = -1;
                Continue(nextCommandIndex);
            }
            else
            {
                m_loopToCommandIndex = -1;
                Continue();
            }
        }

        public override bool CloseBlock()
        {
            return true;
        }
    }

}