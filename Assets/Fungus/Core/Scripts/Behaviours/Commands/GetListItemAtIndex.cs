﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Get the item at a specified index in a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Get List Item At Index")]
    public class GetListItemAtIndex : ReturnValueCommand<object>
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;

        [Tooltip("The target index")]
        [DataMin(0)]
        public IntegerData m_Index;

        [Tooltip("The variable to store the result in")]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            IList list = m_List.Value as IList;
            int index = m_Index.Value;

            return list[index];
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("'{0}' in \"{1}\" and store in {2}", m_Index, m_List, m_ReturnVariable);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_List) || propertyPath == nameof(m_ReturnVariable))
            {
                if (m_ReturnVariable != null)
                {
                    if (m_List != null)
                    {
                        IList list = m_List.Value as IList;
                        Type listType = list.GetType();
                        Type itemType = listType.GetItemType();
                        Type returnVariableType = m_ReturnVariable.GetType();
                        if (itemType != null)
                        {
                            if (!m_ReturnVariable.ValueType.IsAssignableFrom(itemType))
                            {
                                return string.Format("List Item Type '{0}' and Return Variable type '{1}' don't match", itemType.Name, FlowchartItemInfoAttribute.GetInfo(returnVariableType).GetName(returnVariableType));
                            }
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
    }

}