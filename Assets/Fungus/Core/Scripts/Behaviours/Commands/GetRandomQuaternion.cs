﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random quaternion value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Quaternion")]
    public class GetRandomQuaternion : ReturnValueCommand<Quaternion>
    {
        [Tooltip("Minimum value for random range")]
        public QuaternionData m_MinValue = new QuaternionData();

        [Tooltip("Maximum value for random range")]
        public QuaternionData m_MaxValue = new QuaternionData();

        [Tooltip("The variable to store the result in")]
        public QuaternionVariable m_ReturnVariable;
        
        public override Quaternion GetReturnValue()
        {
            return RandomQuaternion(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Quaternion RandomQuaternion(Quaternion min, Quaternion max)
        {
            return new Quaternion(RandomFloat(min.x, max.x), RandomFloat(min.y, max.y), RandomFloat(min.z, max.z), RandomFloat(min.w, max.w));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random quaternion", m_ReturnVariable);
        }
    }

}