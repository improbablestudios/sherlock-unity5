﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Block",
                 "Get a flowchart block by template.")]
    [AddComponentMenu("Fungus/Commands/Flow/Block/Get Block By Template")]
    public class GetBlockByTemplate : ReturnValueCommand<Block>
    {
        [Tooltip("The target flowchart")]
        [CheckNotNullData]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The block to get from the target flowchart")]
        [DontInstantiate]
        [CheckNotNull]
        public Block m_TemplateBlock;

        [Tooltip("The variable to store the result in")]
        public BlockVariable m_ReturnVariable;

        public override Block GetReturnValue()
        {
            return m_Flowchart.Value.GetBlock(m_TemplateBlock);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = {1}.{2}", m_ReturnVariable, m_Flowchart, m_TemplateBlock);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TemplateBlock))
            {
                if (m_Flowchart.IsConstant && m_Flowchart.Value != null)
                {
                    if (m_Flowchart.Value.TemplateFlowchart == null)
                    {
                        return "The selected flowchart does not use a template";
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
    }
}
