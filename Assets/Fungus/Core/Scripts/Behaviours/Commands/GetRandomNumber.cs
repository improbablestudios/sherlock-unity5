﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random integer or float value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Number")]
    public class GetRandomNumber : NumericReturnValueCommand
    {
        [Tooltip("Minimum value for random range")]
        public FloatData m_MinValue = new FloatData();

        [Tooltip("Maximum value for random range")]
        public FloatData m_MaxValue = new FloatData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            if (m_ReturnVariable.ValueType == typeof(int))
            {
                return RandomInt((int)m_MinValue.Value, (int)m_MaxValue.Value);
            }
            else if (m_ReturnVariable.ValueType == typeof(float))
            {
                return RandomFloat(m_MinValue.Value, m_MaxValue.Value);
            }

            return null;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static float RandomInt(int min, int max)
        {
            return Random.Range(min, max);
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random number", m_ReturnVariable);
        }
    }

}