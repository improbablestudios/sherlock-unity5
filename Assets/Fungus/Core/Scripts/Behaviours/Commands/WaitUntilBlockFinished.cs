﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEventUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Wait",
                 "Wait until the target block has finished executing before executing the next command in the block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Wait/Wait Until Block Finished")]
    public class WaitUntilBlockFinished : Command
    {
        [Tooltip("The block to wait for")]
        [FormerlySerializedAs("m_TargetBlock")]
        [CheckNotNullData]
        public BlockData m_Block = new BlockData();

        public override void OnEnter()
        {
            Block block = m_Block.Value;

            block.OnNextExecutionFinished.AddPersistentListener(Continue);
        }
        
        public override string GetSummary()
        {
            return string.Format("{0}", m_Block);
        }
        
        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}