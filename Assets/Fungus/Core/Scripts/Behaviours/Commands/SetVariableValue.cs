using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using System.Linq;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable",
                 "Set a variable's value. The value can be a constant or reference another variable of the same type.")]
    [AddComponentMenu("Fungus/Commands/Variable/Set Variable Value")]
    public class SetVariableValue : Command, ILocalizable
    {
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("variable")]
        [CheckNotNull]
        public Variable m_Variable;

        [Tooltip("Arithmetic operator to use")]
        [FormerlySerializedAs("operation")]
        public SetOperator m_Operation;

        [Tooltip("The value that the variable will be set to")]
        [FormerlySerializedAs("_value")]
        [Localize]
        public GenericVariableData m_Value = new GenericVariableData();
        
        public override void OnEnter()
        {
            if (GetLocalizedDictionary() != null && LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(m_Variable.ValueType))
            {
                m_Variable.RegisterLocalizableObjectValue(GetLocalizedDictionary());
            }

            m_Value.DataVal.ValueType = m_Variable.ValueType;

            m_Variable.DoSetOperation(m_Operation, m_Value.Value);

            Continue();
        }

        public virtual Dictionary<string, object> GetLocalizedDictionary()
        {
            switch (m_Value.VariableDataType)
            {
                case VariableDataType.Value:
                    {
                        if (m_Variable.Localize && m_Value.Localize)
                        {
                            Dictionary<string, object> variableLocalizedDictionary = m_Variable.GetLocalizedObjectDictionary();
                            Dictionary<string, object> newValueLocalizedDictionary = this.SafeGetLocalizedDictionary(x => x.m_Value.Value);
                            if (variableLocalizedDictionary != null)
                            {
                                if (m_Operation == SetOperator.Add)
                                {
                                    foreach (string key in variableLocalizedDictionary.Keys.ToList())
                                    {
                                        if (variableLocalizedDictionary[key] is string)
                                        {
                                            string addString = "";
                                            if (newValueLocalizedDictionary != null && newValueLocalizedDictionary.ContainsKey(key))
                                            {
                                                addString = (string)newValueLocalizedDictionary[key];
                                            }
                                            variableLocalizedDictionary[key] = (string)variableLocalizedDictionary[key] + addString;
                                        }
                                    }
                                }
                                return variableLocalizedDictionary;
                            }
                        }
                    }
                    break;
                case VariableDataType.Variable:
                    {
                        if (m_Value.Variable != null)
                        {
                            if (m_Variable.Localize && m_Value.Variable.Localize)
                            {
                                Dictionary<string, object> variableLocalizedDictionary = m_Variable.GetLocalizedObjectDictionary();
                                Dictionary<string, object> newValueLocalizedDictionary = m_Value.Variable.GetLocalizedObjectDictionary();
                                if (variableLocalizedDictionary != null)
                                {
                                    if (m_Operation == SetOperator.Add)
                                    {
                                        foreach (string key in variableLocalizedDictionary.Keys.ToList())
                                        {
                                            if (variableLocalizedDictionary[key] is string)
                                            {
                                                string addString = "";
                                                if (newValueLocalizedDictionary != null && newValueLocalizedDictionary.ContainsKey(key))
                                                {
                                                    addString = (string)newValueLocalizedDictionary[key];
                                                }
                                                variableLocalizedDictionary[key] = (string)variableLocalizedDictionary[key] + addString;
                                            }
                                        }
                                        return variableLocalizedDictionary;
                                    }
                                }
                                return newValueLocalizedDictionary;
                            }
                        }
                    }
                    break;
            }
            return null;
        }

        public override string GetSummary()
        {
            return string.Format("{0} {1} {2}", m_Variable, Variable.GetSetOperatorDisplayString(m_Operation), m_Value);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Operation))
            {
                if (m_Variable == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Value))
            {
                if (m_Variable == null)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
        
        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}
