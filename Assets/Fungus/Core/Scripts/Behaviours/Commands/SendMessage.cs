using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow",
                 "Send a message to either the owner Flowchart or all Flowcharts in the scene. Blocks can listen for this message using a Message Received event handler.")]
    [AddComponentMenu("Fungus/Commands/Flow/Send Message")]
    public class SendMessage : Command
    {
        public enum MessageTarget
        {
            SameFlowchart,
            AllFlowcharts
        }

        [Tooltip("Target flowchart(s) to send the message to")]
        [FormerlySerializedAs("messageTarget")]
        public MessageTarget m_MessageTarget;

        [Tooltip("Name of the message to send")]
        [FormerlySerializedAs("_message")]
        [CheckNotEmptyData]
        public StringData m_Message = new StringData("");

        public override void OnEnter()
        {
            string message = m_Message.Value;
            
            Flowchart flowchart = GetFlowchart();
            
            if (m_MessageTarget == MessageTarget.SameFlowchart)
            {
                flowchart.SendFungusMessage(message);
            }
            else
            {
                Flowchart.BroadcastFungusMessage(message);
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("{0}", m_Message);
        }
    }

}