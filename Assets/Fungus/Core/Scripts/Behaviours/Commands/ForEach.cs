﻿using UnityEngine;
using UnityEngine.Serialization;
using System;
using System.Collections;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Loop",
                 "Continuously loop through a block of commands. For each element in the list, the Iterator Variable's value will be set equal to the element's value. Use the Break command to force the loop to terminate immediately.")]
    [AddComponentMenu("Fungus/Commands/Flow/Loop/For Each")]
    public class ForEach : Command, ISavable
    {
        [Serializable]
        public class ForEachSaveState : CommandSaveState<ForEach>
        {
        }

        [Tooltip("The variable that will contain the current iterated element")]
        [FormerlySerializedAs("iteratorVariable")]
        [CheckNotNull]
        public Variable m_IteratorVariable;

        [Tooltip("The list to iterate over")]
        [FormerlySerializedAs("listVariable")]
        [CheckNotNull]
        public Variable m_ListVariable;
        
        protected int m_iteratorIndex;

        public virtual SaveState GetSaveState()
        {
            return new ForEachSaveState();
        }
        
        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            // Find next End statement at same indent level
            End endCommand = null;
            for (int i = CommandIndex + 1; i < parentBlock.Commands.Count; ++i)
            {
                End command = parentBlock.Commands[i] as End;

                if (command != null &&
                    command.IndentLevel == m_IndentLevel)
                {
                    endCommand = command;
                    break;
                }
            }
            
            IList list = m_ListVariable.Value as IList;

            if (m_iteratorIndex < list.Count)
            {
                Type iteratorType = m_IteratorVariable.ValueType;
                Type listType = m_ListVariable.ValueType.GetGenericArguments()[0];

                object newValue = list[m_iteratorIndex];
                if (newValue != null && !iteratorType.IsAssignableFrom(listType) && listType.IsAssignableFrom(iteratorType))
                {
                    newValue = Convert.ChangeType(newValue, m_IteratorVariable.ValueType);
                }

                m_IteratorVariable.Value = newValue;

                m_iteratorIndex++;

                if (endCommand != null)
                {
                    // Tell the following end command to loop back
                    endCommand.LoopToCommandIndex = CommandIndex;
                }
                else
                {
                    // No matching End command found, so tell the block to loop back when it reaches the last command
                    parentBlock.LoopToCommandIndex = CommandIndex;
                }

                Continue();
            }
            else
            {
                m_iteratorIndex = 0;
                
                if (endCommand != null)
                {
                    // Continue at next command after End
                    Continue(endCommand.CommandIndex + 1);
                }
                else
                {
                    // No matching End command found, so just continue to the end of the block
                    Continue(int.MaxValue);
                }
            }
        }

        public override bool OpenBlock()
        {
            return true;
        }
        
        public override string GetSummary()
        {
            string iteratorVariableTypeName = "Object";
            if (m_IteratorVariable != null && m_IteratorVariable.ValueType != null)
            {
                iteratorVariableTypeName = m_IteratorVariable.ValueType.Name;
            }
            return string.Format("{0} in {1}", iteratorVariableTypeName, m_ListVariable);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_IteratorVariable) || propertyPath == nameof(m_ListVariable))
            {
                if (m_IteratorVariable != null && m_ListVariable != null)
                {
                    Type iteratorType = m_IteratorVariable.ValueType;
                    Type listType = m_ListVariable.ValueType.GetGenericArguments()[0];

                    if (!iteratorType.IsAssignableFrom(listType) && !listType.IsAssignableFrom(iteratorType))
                    {
                        return string.Format("Cannot cast from iterator type ({0}) to list type ({1})", iteratorType, listType);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ListVariable))
            {
                if (m_IteratorVariable == null)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}