﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Application",
                 "Unpauses the application.")]
    [AddComponentMenu("Fungus/Commands/Flow/Application/Unpause")]
    public class Unpause : Command
    {
        public override void OnEnter()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPaused = false;
#else
            Time.timeScale = 1;
#endif

            Continue();
        }
    }

}