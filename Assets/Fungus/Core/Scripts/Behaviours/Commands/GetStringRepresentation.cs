﻿using UnityEngine;
using System.Linq;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/String",
                 "Get the string representation of a value.")]
    [AddComponentMenu("Fungus/Commands/Variable/String/Get String Representation")]
    public class GetStringRepresentation : ReturnValueCommand<string>
    {
        [Tooltip("The target value")]
        public GenericVariableData m_Value = new GenericVariableData(true);
        
        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            object obj = m_Value.Value;
            return obj == null ? "" : obj.ToString();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" as a string", m_ReturnVariable, m_Value);
        }
    }

}