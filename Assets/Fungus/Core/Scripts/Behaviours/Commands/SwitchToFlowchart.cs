﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;

namespace Fungus.Commands
{
    
    [CommandInfo("Flow/Flowchart",
                 "Deactivate this flowchart, and then activate another flowchart.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Switch to Flowchart")]
    public class SwitchToFlowchart : Command, IConnectNodes
    {
        [Tooltip("The flowchart that will be deactivated")]
        [ObjectDataPopup("<This>")]
        public FlowchartData m_DeactivateFlowchart = new FlowchartData();

        [Tooltip("The flowchart that will be activated")]
        [ObjectDataPopup("<Previous>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        [FormerlySerializedAs("m_Flowchart")]
        public FlowchartData m_ActivateFlowchart = new FlowchartData();

        [Tooltip("Set the previous flowchart of the activated flowchart to the deactivated flowchart (This can be useful in SwitchToFlowchart or ActivateFlowchart commands when leaving the ActivateFlowchart field set to <Previous>)")]
        public BooleanData m_SetPrevious = new BooleanData(true);

        [Tooltip("If true, hide the flowchart's ui instantly")]
        public BooleanData m_DeactivateInstantly = new BooleanData();

        [Tooltip("If true, show the flowchart's ui instantly")]
        public BooleanData m_ActivateInstantly = new BooleanData();

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Flowchart deactivateFlowchart = m_DeactivateFlowchart.Value;
            Flowchart activateFlowchart = m_ActivateFlowchart.Value;
            bool setPreviousFlowchart = m_SetPrevious.Value;
            bool deactivateInstantly = m_DeactivateInstantly.Value;
            bool activateInstantly = m_ActivateInstantly.Value;

            if (deactivateFlowchart == null)
            {
                deactivateFlowchart = GetFlowchart();
            }

            if (activateFlowchart == null)
            {
                activateFlowchart = deactivateFlowchart.PreviousFlowchart;
                setPreviousFlowchart = false;
            }

            if (activateFlowchart == null)
            {
                Log(DebugLogType.Error, "No Target Flowchart");
                return;
            }
            
            Flowchart.SwitchToFlowchart(deactivateFlowchart, activateFlowchart, deactivateInstantly, activateInstantly, setPreviousFlowchart);

            Continue();
        }

        public override string GetSummary()
        {
            string deactivate = m_DeactivateFlowchart.ToString();
            if (m_DeactivateFlowchart.IsConstant && m_DeactivateFlowchart.Value == null)
            {
                deactivate = "<This>";
            }

            string activate = m_ActivateFlowchart.ToString();
            if (m_ActivateFlowchart.IsConstant && m_ActivateFlowchart.Value == null)
            {
                activate = "<Previous>";
            }

            if (m_DeactivateFlowchart.IsConstant && m_DeactivateFlowchart.Value == null)
            {
                return string.Format("{0}", activate);
            }
            else
            {
                return string.Format("{0} -> {1}", deactivate, activate);
            }
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_SetPrevious))
            {
                if (m_ActivateFlowchart.IsConstant && m_ActivateFlowchart.Value == null)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Flowchart);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (IsSwitch())
                {
                    return new string[] { nameof(m_ActivateFlowchart) + "." + "m_" + nameof(m_ActivateFlowchart.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            if (enabled)
            {
                if (!IsSwitch())
                {
                    return new string[] { nameof(m_ActivateFlowchart) + "." + "m_" + nameof(m_ActivateFlowchart.DataVal) };
                }
            }
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            if (enabled)
            {
                if (!IsSwitch())
                {
                    return new string[] { nameof(m_DeactivateFlowchart) + "." + "m_" + nameof(m_DeactivateFlowchart.DataVal) };
                }
            }
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }

        private bool IsSwitch()
        {
            return m_DeactivateFlowchart.IsConstant && (m_DeactivateFlowchart.Value == null || m_DeactivateFlowchart.Value == GetFlowchart());
        }
    }
}