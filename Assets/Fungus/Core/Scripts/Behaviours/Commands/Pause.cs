﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Application",
                 "Pauses the application.")]
    [AddComponentMenu("Fungus/Commands/Flow/Application/Pause")]
    public class Pause : Command
    {
        public override void OnEnter()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPaused = true;
#else
            Time.timeScale = 0;
#endif
            
            Continue();
        }
    }

}