﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Block",
                 "Pause execution of a block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Block/Pause Block")]
    public class PauseBlock : BlockCommand, IConnectNodes
    {
        [Tooltip("Editor only description text to display next to the connection node that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Block block = m_Block.Value;

            if (block == null)
            {
                Block parentBlock = ParentBlock;

                block = parentBlock;
            }

            PauseBlock(block);
        }

        public override void OnResume()
        {
            Continue();
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            if (enabled)
            {
                return new string[] { nameof(m_Block) + "." + "m_" + nameof(m_Block.DataVal) };
            }
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}