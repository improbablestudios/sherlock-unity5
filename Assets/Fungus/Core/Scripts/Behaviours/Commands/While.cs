using UnityEngine;
using Fungus.Variables;
using System.Collections;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Loop",
                 "Continuously loop through a block of commands while the condition is true. Use the Break command to force the loop to terminate immediately.")]
    [AddComponentMenu("Fungus/Commands/Flow/Loop/While")]
    public class While : If
    {
        protected Coroutine m_waitCoroutine;
        
        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            bool execute = EvaluateConditions();

            // Find next End statement at same indent level
            End endCommand = null;
            for (int i = CommandIndex + 1; i < parentBlock.Commands.Count; ++i)
            {
                End command = parentBlock.Commands[i] as End;
                
                if (command != null && 
                    command.IndentLevel == m_IndentLevel)
                {
                    endCommand = command;
                    break;
                }
            }

            if (execute)
            {
                if (endCommand != null)
                {
                    // Tell the following end command to loop back
                    endCommand.LoopToCommandIndex = CommandIndex;
                }
                else
                {
                    // No matching End command found, so tell the block to loop back when it reaches the last command
                    parentBlock.LoopToCommandIndex = CommandIndex;
                }
                m_waitCoroutine = StartCoroutine(DoWaitOneFrame());
            }
            else
            {
                if (endCommand != null)
                {
                    // Continue at next command after End
                    Continue(endCommand.CommandIndex + 1);
                }
                else
                {
                    // No matching End command found, so just stop the block
                    Continue(int.MaxValue);
                }
            }
        }

        public override void OnStop()
        {
            StopCoroutine(m_waitCoroutine);
        }

        protected IEnumerator DoWaitOneFrame()
        {
            yield return null;

            Continue();
        }

        public override bool OpenBlock()
        {
            return true;
        }
    }
    
}