﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random vector3 value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Vector3")]
    public class GetRandomVector3 : ReturnValueCommand<Vector3>
    {
        [Tooltip("Minimum value for random range")]
        public Vector3Data m_MinValue = new Vector3Data();

        [Tooltip("Maximum value for random range")]
        public Vector3Data m_MaxValue = new Vector3Data();

        [Tooltip("The variable to store the result in")]
        public Vector3Variable m_ReturnVariable;
        
        public override Vector3 GetReturnValue()
        {
            return RandomVector3(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Vector3 RandomVector3(Vector3 min, Vector3 max)
        {
            return new Vector3(RandomFloat(min.x, max.x), RandomFloat(min.y, max.y), RandomFloat(min.z, max.z));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random vector3", m_ReturnVariable);
        }
    }

}