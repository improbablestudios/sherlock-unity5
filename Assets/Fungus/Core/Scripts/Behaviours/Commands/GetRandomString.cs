﻿using UnityEngine;
using System.Linq;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random string value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random String")]
    public class GetRandomString : ReturnValueCommand<string>
    {
        [Tooltip("Possible characters for random string")]
        public StringData m_Characters = new StringData();

        [Tooltip("Length of random string")]
        [DataMin(0)]
        public IntegerData m_Length = new IntegerData();

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            return RandomString(m_Characters.Value, m_Length.Value); ;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static string RandomString(string characters, int length)
        {
            System.Random random = new System.Random();
            return new string(Enumerable.Repeat(characters, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random string", m_ReturnVariable);
        }
    }

}