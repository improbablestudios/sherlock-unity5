﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Get the flowchart that was last activated.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Get Latest Activated Flowchart")]
    public class GetLatestActivatedFlowchart : ReturnValueCommand<Flowchart>
    {
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public FlowchartVariable m_ReturnVariable;
        
        public override Flowchart GetReturnValue()
        {
            return Flowchart.LatestActivatedFlowchart;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = Latest Activated Flowchart", m_ReturnVariable);
        }
    }

}