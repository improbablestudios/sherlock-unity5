﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{

    [CommandInfo("Variable",
                 "Get a variable by its name and set its value")]
    [AddComponentMenu("Fungus/Commands/Variable/Set Variable Value By Name")]
    public class SetVariableValueByName : Command, ILocalizable
    {
        [Tooltip("The target flowchart")]
        [FormerlySerializedAs("_flowchart")]
        [CheckNotNullData]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The variable name to get from the target flowchart")]
        [FormerlySerializedAs("_variableName")]
        [CheckNotEmptyData]
        public StringData m_VariableName = new StringData();

        [Tooltip("Arithmetic operator to use")]
        public SetOperator m_Operation;

        [Tooltip("The value to set the variable to")]
        [FormerlySerializedAs("_value")]
        public GenericVariableData m_Value = new GenericVariableData();

        public override void OnEnter()
        {
            Variable variable = m_Flowchart.Value.GetVariable(m_VariableName.Value);
            if (variable != null)
            {
                if (GetLocalizedDictionary() != null && LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(variable.ValueType))
                {
                    variable.RegisterLocalizableObjectValue(GetLocalizedDictionary());
                }

                m_Value.DataVal.ValueType = variable.ValueType;

                variable.DoSetOperation(m_Operation, m_Value.Value);
            }

            Continue();
        }

        public virtual Dictionary<string, object> GetLocalizedDictionary()
        {
            switch (m_Value.VariableDataType)
            {
                case VariableDataType.Value:
                    return this.SafeGetLocalizedDictionary(x => x.m_Value.Value);
                case VariableDataType.Variable:
                    if (m_Value.Variable != null)
                    {
                        return m_Value.Variable.GetLocalizedObjectDictionary();
                    }
                    return null;
            }
            return null;
        }

        public override string GetSummary()
        {
            return string.Format("{0}.{1} {2} {3}", m_Flowchart, m_VariableName, Variable.GetSetOperatorDisplayString(m_Operation), m_Value);
        }

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }
}
