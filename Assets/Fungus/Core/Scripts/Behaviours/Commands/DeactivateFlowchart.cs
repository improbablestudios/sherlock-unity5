﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Deactivate a flowchart.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Deactivate Flowchart")]
    public class DeactivateFlowchart : Command, IConnectNodes
    {
        [Tooltip("The target flowchart")]
        [ObjectDataPopup("<Latest Activated Flowchart>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("If true, hide the flowchart's ui instantly")]
        public BooleanData m_DeactivateInstantly = new BooleanData();

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Flowchart flowchart = m_Flowchart.Value;
            bool deactivateInstantly = m_DeactivateInstantly.Value;

            if (flowchart == null)
            {
                flowchart = Flowchart.LatestActivatedFlowchart;
            }
            
            Flowchart.Deactivate(flowchart, deactivateInstantly, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                return "<Latest Activated Flowchart>";
            }

            return string.Format("{0}", m_Flowchart);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Connection[] GetSwitchConnections()
        {
            return new Connection[0];
        }

        public Connection[] GetActivateConnections()
        {
            return new Connection[0];
        }

        public Connection[] GetDeactivateConnections()
        {
            List<Connection> connections = new List<Connection>();

            if (m_Flowchart.IsConstant)
            {
                connections.Add(new Connection(GetFlowchart(), m_Flowchart.Value, m_Description));
            }

            return connections.ToArray();
        }

        public Type GetNodeType()
        {
            return typeof(Flowchart);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            if (enabled)
            {
                return new string[] { nameof(m_Flowchart) + "." + "m_" + nameof(m_Flowchart.DataVal) };
            }
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}