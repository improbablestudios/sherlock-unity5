﻿using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Wait",
                 "Wait for a number of frames before executing the next command in the block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Wait/Wait For Frames")]
    public class WaitForFrames : Command
    {
        [Tooltip("The number of frames to wait for")]
        [DataMin(-1)]
        public IntegerData m_NumberOfFrames = new IntegerData(1);

        [Tooltip("If true, wait until the end of the frame")]
        public BooleanData m_EndOfFrame = new BooleanData();

        protected Coroutine m_waitCoroutine;

        public override void OnEnter()
        {
            int numberOFrames = m_NumberOfFrames.Value;
            bool endOfFrame = m_EndOfFrame.Value;

            m_waitCoroutine = StartCoroutine(DoWait(numberOFrames, endOfFrame));
        }

        public override void OnStop()
        {
            StopCoroutine(m_waitCoroutine);
        }

        protected IEnumerator DoWait(int numberOfFrames, bool endOfFrame)
        {
            if (numberOfFrames > 0)
            {
                yield return new ImprobableStudios.UIUtilities.WaitForFrames(numberOfFrames);
            }

            if (endOfFrame)
            {
                yield return new WaitForEndOfFrame();
            }

            Continue();
        }

        public override string GetSummary()
        {
            return m_NumberOfFrames.ToString();
        }

        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_NumberOfFrames.IsConstant) || m_NumberOfFrames.Value > 0 ||
                (!runtime && !m_EndOfFrame.IsConstant) || m_EndOfFrame.Value)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }

        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }
    }

}