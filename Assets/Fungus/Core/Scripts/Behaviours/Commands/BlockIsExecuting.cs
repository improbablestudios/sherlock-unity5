﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Block",
                 "Check whether or not a block is currently executing.")]
    [AddComponentMenu("Fungus/Commands/Flow/Block/Block Is Executing")]
    public class BlockIsExecuting : ReturnValueCommand<bool>
    {
        [Tooltip("The target block")]
        [CheckNotNullData]
        public BlockData m_Block = new BlockData();

        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            Block targetBlock = m_Block.Value;

            return targetBlock.IsExecuting();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" is executing", m_ReturnVariable, m_Block);
        }
    }

}