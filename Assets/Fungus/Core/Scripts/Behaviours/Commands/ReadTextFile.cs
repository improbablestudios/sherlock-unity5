﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/String",
                 "Get the contents of a text file.")]
    [AddComponentMenu("Fungus/Commands/Variable/String/Read Text File")]
    public class ReadTextFile : ReturnValueCommand<string>
    {
        [Tooltip("Text file to read into the string variable")]
        [FormerlySerializedAs("textFile")]
        [CheckNotNullData]
        public TextAssetData m_TextFile;

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            return m_TextFile.Value.text;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = {1} text", m_ReturnVariable, m_TextFile);
        }
    }

}