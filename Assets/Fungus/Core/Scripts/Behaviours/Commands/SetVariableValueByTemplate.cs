﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable",
                 "Get a variable by its template and set its value")]
    [AddComponentMenu("Fungus/Commands/Variable/Set Variable Value By Template")]
    public class SetVariableValueByTemplate : Command, ILocalizable
    {
        [Tooltip("The target flowchart")]
        [CheckNotNullData]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The variable to get from the target flowchart")]
        [DontInstantiate]
        [CheckNotNull]
        public Variable m_TemplateVariable;

        [Tooltip("Arithmetic operator to use")]
        public SetOperator m_Operation;

        [Tooltip("The value to set the variable to")]
        [Localize]
        public GenericVariableData m_Value = new GenericVariableData();

        public override void OnEnter()
        {
            Variable variable = m_Flowchart.Value.GetVariable(m_TemplateVariable.GetName());
            if (variable != null)
            {
                if (GetLocalizedDictionary() != null && LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(variable.ValueType))
                {
                    variable.RegisterLocalizableObjectValue(GetLocalizedDictionary());
                }

                m_Value.DataVal.ValueType = variable.ValueType;

                variable.DoSetOperation(m_Operation, m_Value.Value);
            }

            Continue();
        }

        public virtual Dictionary<string, object> GetLocalizedDictionary()
        {
            switch (m_Value.VariableDataType)
            {
                case VariableDataType.Value:
                    return this.SafeGetLocalizedDictionary(x => x.m_Value.Value);
                case VariableDataType.Variable:
                    if (m_Value.Variable != null)
                    {
                        return m_Value.Variable.GetLocalizedObjectDictionary();
                    }
                    return null;
            }
            return null;
        }

        public override string GetSummary()
        {
            string templateVariableName = "<None>";
            if (m_TemplateVariable != null)
            {
                templateVariableName = m_TemplateVariable.GetName();
            }
            return string.Format("{0}.{1} {2} {3}", m_Flowchart, templateVariableName, Variable.GetSetOperatorDisplayString(m_Operation), m_Value);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Operation))
            {
                if (m_TemplateVariable == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Value))
            {
                if (m_TemplateVariable == null)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
        
        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }
}
