using UnityEngine;
using System;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [Serializable]
    public class Condition
    {
        public enum CompareOperator
        {
            Equals,                 // ==
            NotEquals,                 // !=
            LessThan,                // <
            GreaterThan,            // >
            LessThanOrEquals,        // <=
            GreaterThanOrEquals        // >=
        }

        public enum EvaluationState
        {
            NotEvaluated,
            Pass,
            Fail,
        }

        [Tooltip("Left value")]
        [FormerlySerializedAs("variableData")]
        public VariableData m_VariableData = new VariableData();

        [Tooltip("The type of comparison to be performed")]
        [FormerlySerializedAs("compareOperator")]
        public CompareOperator m_CompareOperator;

        [Tooltip("Right value")]
        [FormerlySerializedAs("value")]
        public GenericVariableData m_Value = new GenericVariableData();

        [NonSerialized]
        public EvaluationState m_EvaluationState;

        public void SetupValueType()
        {
            Type type = m_VariableData.ValueType;
            if (m_Value.ValueType != type)
            {
                if (typeof(bool).IsAssignableFrom(type))
                {
                    m_Value.SetValueType(type, true);
                }
                else
                {
                    m_Value.SetValueType(type);
                }
            }
        }

        public bool Evaluate()
        {
            SetupValueType();

            bool success = Evaluate(m_CompareOperator, m_VariableData.Value, m_Value.Value);

            if (success)
            {
                m_EvaluationState = EvaluationState.Pass;
            }
            else
            {
                m_EvaluationState = EvaluationState.Fail;
            }

            return success;
        }

        private bool Evaluate(CompareOperator compareOperator, object lhs, object rhs)
        {
            if (lhs != null)
            {
                if (lhs.GetType().IsEnum)
                {
                    lhs = Convert.ChangeType(lhs, typeof(int));
                }
            }
            if (rhs != null)
            {
                if (rhs.GetType().IsEnum)
                {
                    rhs = Convert.ChangeType(rhs, typeof(int));
                }
            }
            if (lhs != null && rhs != null)
            {
                if (lhs is int && rhs is float)
                {
                    rhs = (int)((float)rhs);
                }
                if (lhs is float && rhs is int)
                {
                    rhs = (float)((int)rhs);
                }
            }

            switch (compareOperator)
            {
                case CompareOperator.Equals:
                    if (lhs is Vector2 && rhs is Vector2)
                    {
                        if ((Vector2)lhs == (Vector2)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Vector3 && rhs is Vector3)
                    {
                        if ((Vector3)lhs == (Vector3)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Vector4 && rhs is Vector4)
                    {
                        if ((Vector4)lhs == (Vector4)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Quaternion && rhs is Quaternion)
                    {
                        if ((Quaternion)lhs == (Quaternion)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs != null && lhs.Equals(rhs))
                    {
                        return true;
                    }
                    if (rhs != null && rhs.Equals(lhs))
                    {
                        return true;
                    }
                    if (lhs == null && rhs == null)
                    {
                        return true;
                    }
                    break;
                case CompareOperator.NotEquals:
                    if (lhs is Vector2 && rhs is Vector2)
                    {
                        if ((Vector2)lhs != (Vector2)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Vector3 && rhs is Vector3)
                    {
                        if ((Vector3)lhs != (Vector3)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Vector4 && rhs is Vector4)
                    {
                        if ((Vector4)lhs != (Vector4)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs is Quaternion && rhs is Quaternion)
                    {
                        if ((Quaternion)lhs != (Quaternion)rhs)
                        {
                            return true;
                        }
                    }
                    if (lhs != null && !lhs.Equals(rhs))
                    {
                        return true;
                    }
                    if (rhs != null && !rhs.Equals(lhs))
                    {
                        return true;
                    }
                    if (lhs == null && rhs == null)
                    {
                        return false;
                    }
                    break;
                case CompareOperator.GreaterThan:
                    if ((lhs is int && rhs is int) && ((int)lhs > (int)rhs))
                    {
                        return true;
                    }
                    if ((lhs is float && rhs is float) && ((float)lhs > (float)rhs))
                    {
                        return true;
                    }
                    break;
                case CompareOperator.LessThan:
                    if ((lhs is int && rhs is int) && ((int)lhs < (int)rhs))
                    {
                        return true;
                    }
                    if ((lhs is float && rhs is float) && ((float)lhs < (float)rhs))
                    {
                        return true;
                    }
                    break;
                case CompareOperator.GreaterThanOrEquals:
                    if ((lhs is int && rhs is int) && ((int)lhs >= (int)rhs))
                    {
                        return true;
                    }
                    if ((lhs is float && rhs is float) && ((float)lhs >= (float)rhs))
                    {
                        return true;
                    }
                    break;
                case CompareOperator.LessThanOrEquals:
                    if ((lhs is int && rhs is int) && ((int)lhs <= (int)rhs))
                    {
                        return true;
                    }
                    if ((lhs is float && rhs is float) && ((float)lhs <= (float)rhs))
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        public static string GetOperatorDescription(CompareOperator compareOperator)
        {
            string summary = "";
            switch (compareOperator)
            {
                case CompareOperator.Equals:
                    summary += "==";
                    break;
                case CompareOperator.NotEquals:
                    summary += "!=";
                    break;
                case CompareOperator.LessThan:
                    summary += "<";
                    break;
                case CompareOperator.GreaterThan:
                    summary += ">";
                    break;
                case CompareOperator.LessThanOrEquals:
                    summary += "<=";
                    break;
                case CompareOperator.GreaterThanOrEquals:
                    summary += ">=";
                    break;
            }

            return summary;
        }
    }

}