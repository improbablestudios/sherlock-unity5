﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Remove the item at a specified index in a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Remove List Item At Index")]
    public class RemoveListItemAtIndex : Command
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;

        [Tooltip("The target index")]
        [DataMin(0)]
        public IntegerData m_Index;
        
        public override void OnEnter()
        {
            IList list = m_List.Value as IList;
            int index = m_Index.Value;

            list.RemoveAt(index);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("'{0}' from \"{1}\"", m_Index, m_List);
        }
    }

}