using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Conditional",
                 "Execute the following command block if the test condition is true.")]
    [AddComponentMenu("Fungus/Commands/Flow/Conditional/If")]
    public class If : Command
    {
        public enum CheckType
        {
            AllConditionsAreTrue,
            AnyConditionsAreTrue
        }

        [Tooltip("Conditions to check")]
        [FormerlySerializedAs("conditions")]
        [CheckNotEmpty]
        public List<Condition> m_Conditions = new List<Condition>();

        [Tooltip("Check if any are true or check if all are true")]
        [FormerlySerializedAs("checkIf")]
        public CheckType m_CheckIf;

        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            if (parentBlock == null)
            {
                return;
            }

            EvaluateAndContinue();
        }

        public bool EvaluateConditions()
        {
            bool result = false;
            switch (m_CheckIf)
            {
                case CheckType.AnyConditionsAreTrue:
                    result = EvaluateAny();
                    break;
                case CheckType.AllConditionsAreTrue:
                    result = EvaluateAll();
                    break;
            }
            return result;
        }

        protected bool EvaluateAny()
        {
            foreach(Condition condition in m_Conditions)
            {
                if (condition.Evaluate())
                {
                    return true;
                }
            }
            return false;
        }

        protected bool EvaluateAll()
        {
            foreach (Condition condition in m_Conditions)
            {
                if (!condition.Evaluate())
                {
                    return false;
                }
            }
            return true;
        }

        protected void EvaluateAndContinue()
        {
            if (EvaluateConditions())
            {
                OnTrue();
            }
            else
            {
                OnFalse();
            }
        }

        protected virtual void OnTrue()
        {
            Continue();
        }

        protected virtual void OnFalse()
        {
            Block parentBlock = ParentBlock;

            // Last command in block
            if (CommandIndex >= parentBlock.Commands.Count)
            {
                Continue(int.MaxValue);
                return;
            }

            // Find the next Else, ElseIf or End command at the same indent level as this If command
            for (int i = CommandIndex + 1; i < parentBlock.Commands.Count; ++i)
            {
                Command nextCommand = parentBlock.Commands[i];
                
                // Find next command at same indent level as this If command
                // Skip disabled commands, comments & labels
                if (!nextCommand.enabled || 
                    nextCommand.GetType() == typeof(Comment) ||
                    nextCommand.GetType() == typeof(Label) ||
                    nextCommand.IndentLevel != m_IndentLevel)
                {
                    continue;
                }
                
                System.Type type = nextCommand.GetType();
                if (type == typeof(Else) ||
                    type == typeof(End))
                {
                    if (i >= parentBlock.Commands.Count - 1)
                    {
                        Continue(int.MaxValue);
                    }
                    else
                    {
                        // Execute command immediately after the Else or End command
                        Continue(nextCommand.CommandIndex + 1);
                        return;
                    }
                }
                else if (type == typeof(ElseIf))
                {
                    // Execute the Else If command
                    Continue(i);

                    return;
                }
            }

            // No matching End command found, so just continue to the end of the block
            Continue(int.MaxValue);
        }

        public override bool OpenBlock()
        {
            return true;
        }

        public override string GetSummary()
        {
            string summary = "";
            for (int i = 0; i < m_Conditions.Count; i++)
            {
                Condition condition = m_Conditions[i];

                summary += "(" + condition.m_VariableData.ToString() + " ";
                summary += Condition.GetOperatorDescription(condition.m_CompareOperator) + " ";
                summary += condition.m_Value.ToString();
                summary += ")";

                if (m_Conditions.Count > 1 && i < m_Conditions.Count - 1)
                {
                    switch (m_CheckIf)
                    {
                        case CheckType.AllConditionsAreTrue:
                            summary += " AND ";
                            break;
                        case CheckType.AnyConditionsAreTrue:
                            summary += " OR ";
                            break;
                    }
                }
            }
            return summary;
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Conditions))
            {
                for (int i = 0; i < m_Conditions.Count; i++)
                {
                    Condition condition = m_Conditions[i];

                    condition.SetupValueType();

                    if (condition.m_VariableData.Variable == null && condition.m_VariableData.Command == null)
                    {
                        return "Condition has no variable or command selected";
                    }
                    if (condition.m_VariableData.Command != null)
                    {
                        string[] conditionErrors = condition.m_VariableData.Command.GetErrors();
                        if (conditionErrors.Length > 0)
                        {
                            return string.Join(", ", conditionErrors);
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_CheckIf))
            {
                if (m_Conditions.Count <= 1)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}