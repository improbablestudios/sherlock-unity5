using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using Fungus.Variables;
using System.Linq;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Flow",
                 "Execute another block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Call")]
    public class Call : Command, IConnectNodes
    {
        [Tooltip("Block to start executing")]
        [FormerlySerializedAs("_targetBlock")]
        [CheckNotNullData]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if the calling block should continue, pause, or stop executing commands, or wait until the called block finishes")]
        [FormerlySerializedAs("_callMode")]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);

        [Tooltip("Parameters to use when calling the block")]
        public List<GenericVariableData> m_Parameters = new List<GenericVariableData>();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public Variable m_ReturnVariable;

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            List<object> parameterValues = m_Parameters.Select(i => i.Value).ToList();
            CallBlock(m_TargetBlock.Value, m_CallMode.Value, parameterValues, m_ReturnVariable);
        }

        public override string GetSummary()
        {
            string callModeString = "";
            if (m_CallMode.IsConstant)
            {
                switch (m_CallMode.Value)
                {
                    case CallMode.Continue:
                        callModeString = "and continue this block";
                        break;
                    case CallMode.WaitUntilFinished:
                        callModeString = "wait until finished then continue this block";
                        break;
                    case CallMode.Pause:
                        callModeString = "and pause this block";
                        break;
                    case CallMode.Stop:
                        callModeString = "and stop this block";
                        break;
                }
            }
            else
            {
                callModeString = string.Format("then {0}", m_CallMode);
            }

            if (m_TargetBlock.IsConstant && m_TargetBlock.Value != null && m_TargetBlock.Value.GetFlowchart() != GetFlowchart())
            {
                return string.Format("{0}({1}) : {2}", m_TargetBlock.Value.GetFlowchart().name, m_TargetBlock, callModeString);
            }

            return string.Format("{0} : {1}", m_TargetBlock, callModeString);
        }

        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_CallMode.IsConstant) || m_CallMode.Value != CallMode.Continue)
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }

        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }

        public override bool IsReturnVariablePropertyVisible()
        {
            if (m_TargetBlock.IsConstant && (m_TargetBlock.Value == null || m_TargetBlock.Value.GetReturnType() == null))
            {
                return false;
            }

            return base.IsReturnVariablePropertyVisible();
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Parameters))
            {
                if (m_TargetBlock.IsConstant && m_TargetBlock.Value != null)
                {
                    List<Variable> parameters = m_TargetBlock.Value.GetParameters();
                    while (m_Parameters.Count < parameters.Count)
                    {
                        m_Parameters.Add(new GenericVariableData());
                    }
                    while (m_Parameters.Count > parameters.Count)
                    {
                        m_Parameters.RemoveAt(m_Parameters.Count - 1);
                    }
                    for (int i = 0; i < parameters.Count; i++)
                    {
                        if (m_Parameters[i].ValueType != parameters[i].ValueType)
                        {
                            m_Parameters[i].SetValueType(parameters[i].ValueType);
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Parameters))
            {
                if (m_TargetBlock.IsConstant && (m_TargetBlock.Value == null || m_TargetBlock.Value.GetParameters().Count == 0))
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}