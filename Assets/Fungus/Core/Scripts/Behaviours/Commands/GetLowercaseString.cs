﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/String",
                 "Get the lowercase version of a string.")]
    [AddComponentMenu("Fungus/Commands/Variable/String/Get Lowercase String")]
    public class GetLowercaseString : ReturnValueCommand<string>
    {
        [Tooltip("The target string")]
        public StringData m_String = new StringData();

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            string stringValue = m_String.Value;

            if (!string.IsNullOrEmpty(stringValue))
            {
                return stringValue.ToUpper();
            }

            return stringValue;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" lowercase", m_ReturnVariable, m_String);
        }
    }
}
