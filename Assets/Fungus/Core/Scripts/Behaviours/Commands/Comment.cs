using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("",
                 "Use comments to record design notes and reminders.")]
    [AddComponentMenu("Fungus/Commands/Comment")]
    public class Comment : InertCommand
    {
        [Tooltip("Text to display for this comment")]
        [TextArea(2,4)]
        [FormerlySerializedAs("commentText")]
        public string m_CommentText = "";

        public override string GetSummary()
        {
            return m_CommentText;
        }
    }

}