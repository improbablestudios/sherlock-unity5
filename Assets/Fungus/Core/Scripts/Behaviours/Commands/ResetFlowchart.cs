using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Reset the state of all commands and variables in a flowchart.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Reset Flowchart")]
    public class ResetFlowchart : Command
    {
        [Tooltip("Stop all executing blocks in the target flowchart")]
        [ObjectDataPopup("<This>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("Reset variables back to their default values")]
        [FormerlySerializedAs("_resetVariables")]
        public BooleanData m_ResetVariables = new BooleanData(true);

        public override void OnEnter()
        {
            Flowchart flowchart = m_Flowchart.Value;
            bool resetVariables = m_ResetVariables.Value;

            if (flowchart == null)
            {
                flowchart = GetFlowchart();
            }

            flowchart.Reset(resetVariables);
            Continue();
        }

        public override string GetSummary()
        {
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                return "";
            }

            return string.Format("{0}", m_Flowchart);
        }
    }

}