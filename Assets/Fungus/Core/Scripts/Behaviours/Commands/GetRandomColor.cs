﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random color value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Color")]
    public class GetRandomColor : ReturnValueCommand<Color>
    {
        [Tooltip("Minimum value for random range")]
        public ColorData m_MinValue = new ColorData();

        [Tooltip("Maximum value for random range")]
        public ColorData m_MaxValue = new ColorData();

        [Tooltip("The variable to store the result in")]
        public ColorVariable m_ReturnVariable;
        
        public override Color GetReturnValue()
        {
            return RandomColor(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Color RandomColor(Color min, Color max)
        {
            return new Color(RandomFloat(min.r, max.r), RandomFloat(min.g, max.g), RandomFloat(min.b, max.b), RandomFloat(min.a, max.a));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random color", m_ReturnVariable);
        }
    }

}