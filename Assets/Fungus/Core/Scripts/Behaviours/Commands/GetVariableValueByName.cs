﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable",
                 "Get a variable value by its name.")]
    [AddComponentMenu("Fungus/Commands/Variable/Get Variable Value By Name")]
    public class GetVariableValueByName : ReturnValueCommand<object>
    {
        [Tooltip("The target flowchart")]
        [FormerlySerializedAs("_flowchart")]
        [CheckNotNullData]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The variable name to get from the target flowchart")]
        [FormerlySerializedAs("_variableName")]
        public StringData m_VariableName = new StringData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            return m_Flowchart.Value.GetVariableValue(m_VariableName.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = {1}.{2}", m_ReturnVariable, m_Flowchart, m_VariableName);
        }
    }
}
