﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Number",
                 "Get the distance between two points in 3D space.")]
    [AddComponentMenu("Fungus/Commands/Variable/Number/Get 3D Distance")]
    public class Get3DDistance : NumericReturnValueCommand
    {
        [Tooltip("The first point")]
        public Vector3Data m_Point1 = new Vector3Data();

        [Tooltip("The second point")]
        public Vector3Data m_Point2 = new Vector3Data();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            Vector3 point1 = m_Point1.Value;
            Vector3 point2 = m_Point2.Value;

            return Vector3.Distance(point1, point2);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = distance between {1} and {2} ", m_ReturnVariable, m_Point1, m_Point2);
        }
    }

}