﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    public abstract class SettingCommand : Command
    {
        [Tooltip("If true, save settings to settings file.")]
        public BooleanData m_SaveSettings = new BooleanData(true);

        protected abstract void ChangeSetting();

        public override void OnEnter()
        {
            ChangeSetting();

            if (m_SaveSettings.Value)
            {
                SaveSystemManager.Instance.SaveSettings();
            }

            Continue();
        }
    }

}