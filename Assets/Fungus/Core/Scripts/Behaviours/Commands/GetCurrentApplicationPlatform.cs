﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Application",
                 "Get which platform the application is currently running on.")]
    [AddComponentMenu("Fungus/Commands/Application/Get Current Application Platform")]
    public class GetCurrentApplicationPlatform : ReturnValueCommand<RuntimePlatform>
    {
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public RuntimePlatformVariable m_ReturnVariable;
        
        public override RuntimePlatform GetReturnValue()
        {
            return Application.platform;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = platform", m_ReturnVariable);
        }
    }

}