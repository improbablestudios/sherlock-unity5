using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.SceneUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Scene",
                 "Load a new Unity scene and display an optional loading image. This is useful " +
                 "for splitting a large game across multiple scene files to reduce peak memory usage." +
                 "The scene to be loaded must be added to the scene list in Build Settings.")]
    [AddComponentMenu("Fungus/Commands/Flow/Scene/Load Scene")]
    public class LoadScene : Command
    {
        [Tooltip("Name of the scene to load (The scene must be added to the build settings)")]
        [SceneNameDataField]
        [FormerlySerializedAs("_sceneName")]
        [CheckNotEmptyData]
        public StringData m_SceneName = new StringData("");

        [Tooltip("Don't destroy objects in the current scene when loading the new scene")]
        [FormerlySerializedAs("_additive")]
        public BooleanData m_Additive = new BooleanData();

        [Tooltip("Don't destroy objects in the current scene when loading the new scene")]
        [FormerlySerializedAs("_showLoadingScreen")]
        public BooleanData m_ShowLoadingScreen = new BooleanData(true);

        public override void OnEnter()
        {
            string sceneName = m_SceneName.Value;
            bool additive = m_Additive.Value;
            bool showLoadingScreen = m_ShowLoadingScreen.Value;
            
            SceneLoadingManager.Instance.LoadScene(sceneName, additive, showLoadingScreen);

            Continue();
        }

        public override bool ChangesScene()
        {
            return true;
        }
        
        public override string GetSummary()
        {
            return string.Format("{0}", m_SceneName);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ShowLoadingScreen))
            {
                if (!m_Additive.Value)
                {
                    foreach (AudioSource audioSource in FindObjectsOfType<AudioSource>())
                    {
                        audioSource.Stop();
                    }
                }
                if (string.IsNullOrEmpty(SceneLoadingManager.Instance.LoadingSceneName))
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}