﻿using UnityEngine;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Scene",
                 "Reload the current scene.")]
    [AddComponentMenu("Fungus/Commands/Flow/Scene/Reload Scene")]
    public class ReloadScene : Command
    {
        public override void OnEnter()
        {
            SaveSystemManager.Instance.ReloadScene();
        }
        
        public override bool WillSkipWhenLoading()
        {
            return true;
        }
    }

}
