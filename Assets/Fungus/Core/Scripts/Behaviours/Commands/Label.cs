using System;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using System.Linq;
using ImprobableStudios.ResourceTrackingUtilities;
using System.Collections.Generic;

namespace Fungus.Commands
{

    [CommandInfo("Flow",
                 "Mark a position in the command list for execution to jump to.")]
    [AddComponentMenu("Fungus/Commands/Flow/Label")]
    public class Label : InertCommand
    {
        ///<summary>
        ///The default name of a new block.
        ///</summary>
        public const string DEFAULT_LABEL_NAME = "NEW LABEL";

        [Tooltip("Display name for the label")]
        [FormerlySerializedAs("labelName")]
        public string m_LabelName = "";

        public override string GetSummary()
        {
            return m_LabelName;
        }

        public override string GetName()
        {
            return m_LabelName;
        }

        /// <summary>
        /// Returns a new Label key that is guaranteed not to clash with any existing Label in the Block.
        /// </summary>
        /// <param name="newName">The name you want to use</param>
        /// <returns>A unique name</returns>
        public string GetUniqueLabelName(string newName)
        {
            List<Label> labels = ParentBlock.Commands.Where(x => x != null).OfType<Label>().ToList();

            string[] names = labels.Select(x => x.GetName()).ToArray();
            string defaultName = DEFAULT_LABEL_NAME;
            int ignoreIndex = labels.IndexOf(this);

            return names.GetUniqueName(newName, defaultName, ignoreIndex);
        }
    }

}