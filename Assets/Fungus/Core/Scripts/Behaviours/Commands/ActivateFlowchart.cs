﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Activate a flowchart.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Activate Flowchart")]
    public class ActivateFlowchart : Command, IConnectNodes
    {
        [Tooltip("The target flowchart")]
        [ObjectDataPopup("<Previous>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("If true, show the flowchart's ui instantly")]
        public BooleanData m_ActivateInstantly = new BooleanData();

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override void OnEnter()
        {
            Flowchart flowchart = m_Flowchart.Value;
            bool activateInstantly = m_ActivateInstantly.Value;

            if (flowchart == null)
            {
                flowchart = GetFlowchart().PreviousFlowchart;
            }

            if (flowchart == null)
            {
                Log(DebugLogType.Error, "No Target Flowchart");
                return;
            }

            Flowchart.Activate(flowchart, activateInstantly, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                return "<Previous>";
            }

            return string.Format("{0}", m_Flowchart);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Flowchart);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            if (enabled)
            {
                return new string[] { nameof(m_Flowchart) + "." + "m_" + nameof(m_Flowchart.DataVal) };
            }
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}