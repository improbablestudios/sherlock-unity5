using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Conditional",
                 "Mark the start of a command block to be executed when the preceding If statement is false and the test condition is true.")]
    [AddComponentMenu("Fungus/Commands/Flow/Conditional/Else If")]
    public class ElseIf : If
    {

        public override void OnEnter()
        {
            Block parentBlock = ParentBlock;

            System.Type previousCommandType = parentBlock.GetPreviousActiveCommandType();

            if (previousCommandType == typeof(If) ||
                previousCommandType == typeof(ElseIf) )
            {
                // Else If behaves the same as an If command
                EvaluateAndContinue();
            }
            else
            {
                // Else If behaves mostly like an Else command, 
                // but will also jump to a following Else command.

                // Last command in the list
                if (CommandIndex >= parentBlock.Commands.Count - 1)
                {
                    Continue(int.MaxValue);
                    return;
                }

                // Find the next End command at the same indent level as this Else If command
                int indent = m_IndentLevel;
                for (int i = CommandIndex + 1; i < parentBlock.Commands.Count; ++i)
                {
                    Command command = parentBlock.Commands[i];

                    if (command.IndentLevel == indent)
                    {
                        System.Type type = command.GetType();
                        if (type == typeof(End))
                        {
                            // Execute command immediately after the Else or End command
                            Continue(command.CommandIndex + 1);
                            return;
                        }
                    }
                }

                // No End command found
                Continue(int.MaxValue);
            }
        }

        public override bool OpenBlock()
        {
            return true;
        }

        public override bool CloseBlock()
        {
            return true;
        }
    }

}