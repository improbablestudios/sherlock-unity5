using Fungus.Variables;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Wait",
                 "Wait for a period of time before executing the next command in the block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Wait/Wait")]
    public class Wait : Command 
    {
        [Tooltip("The duration to wait for")]
        [DataMin(-1)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData(1f);

        protected Coroutine m_waitCoroutine;

        public override void OnEnter()
        {
            float duration = m_Duration.Value;

            m_waitCoroutine = StartCoroutine(DoWait(duration));
        }

        public override void OnStop()
        {
            StopCoroutine(m_waitCoroutine);
        }

        protected IEnumerator DoWait(float duration)
        {
            if (duration > 0)
            {
                yield return new WaitForSeconds(duration);
            }

            Continue();
        }

        public override string GetSummary()
        {
            return GetDurationSummary(m_Duration, "for");
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }

        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }
    }

}