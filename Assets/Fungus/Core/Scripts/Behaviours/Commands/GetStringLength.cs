﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/String",
                 "Get the length of a string.")]
    [AddComponentMenu("Fungus/Commands/Variable/String/Get String Length")]
    public class GetStringLength : ReturnValueCommand<int>
    {
        [Tooltip("The target string")]
        [FormerlySerializedAs("_string")]
        public StringData m_String = new StringData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public IntegerVariable m_ReturnVariable;
        
        public override int GetReturnValue()
        {
            string stringValue = m_String.Value;

            if (!string.IsNullOrEmpty(stringValue))
            {
                return stringValue.Length;
            }

            return 0;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" length", m_ReturnVariable, m_String);
        }
    }
}
