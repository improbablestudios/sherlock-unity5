﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Number", 
                 "Get the result of an arithmetic operation (e.g. \"{$Var1} + {$Var2}\").")]
    [AddComponentMenu("Fungus/Commands/Variable/Number/Compute Variable")]
    public class ComputeVariable : NumericReturnValueCommand
    {
        [Tooltip("The math operation to be performed. Supports variable substition (e.g. \"{$Var1} + {$Var2}\")")]
        [FormerlySerializedAs("_expression")]
        [CheckNotEmptyData]
        public StringData m_Expression = new StringData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        [FormerlySerializedAs("returnVariable")]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            string expression = m_Expression.Value;

            Flowchart flowchart = GetFlowchart();
            string substitutedExpression = flowchart.SubstituteVariables(expression);
            
            return Evaluate(substitutedExpression);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = {1}", m_ReturnVariable, m_Expression);
        }

        public float Evaluate(string expr)  
        {  
            var doc = new System.Xml.XPath.XPathDocument(new System.IO.StringReader("<r/>"));
            var nav = doc.CreateNavigator();
            var newString = expr;
            newString = (new System.Text.RegularExpressions.Regex(@"([\+\-\*])")).Replace(newString, " ${1} ");
            newString = newString.Replace("/", " div ").Replace("%", " mod ");
            return (float)(double)nav.Evaluate("number(" + newString + ")");
        }
    }
}
