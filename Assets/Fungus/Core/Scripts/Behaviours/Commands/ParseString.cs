﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/String",
                 "Convert a string into an integer or float.")]
    [AddComponentMenu("Fungus/Commands/Variable/String/Parse String")]
    public class ParseString : NumericReturnValueCommand
    {
        [Tooltip("The string that will be parsed")]
        [FormerlySerializedAs("_string")]
        [CheckNotEmptyData]
        public StringData m_String = new StringData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        [FormerlySerializedAs("returnVariable")]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            string stringValue = m_String.Value;

            if (typeof(int).IsAssignableFrom(m_ReturnVariable.ValueType))
            {
                int result = 0;
                int.TryParse(stringValue, out result);
                return result;
            }
            else if (typeof(float).IsAssignableFrom(m_ReturnVariable.ValueType))
            {
                float result = 0;
                float.TryParse(stringValue, out result);
                return result;
            }

            return null;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = {1}", m_ReturnVariable, m_String);
        }
    }
}
