﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Insert an item in a list at a specified index.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Insert List Item At Index")]
    public class InsertListItemAtIndex : Command
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;

        [Tooltip("The target index")]
        [DataMin(0)]
        public IntegerData m_Index;

        [Tooltip("The item to insert")]
        public GenericVariableData m_Value = new GenericVariableData();

        public override void OnEnter()
        {
            IList list = m_List.Value as IList;
            int index = m_Index.Value;
            object value = m_Value.Value;

            list.Insert(index, value);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("'{0}' in \"{1}\" to '{2}'", m_Index, m_List, m_Value);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List != null)
                {
                    IList list = m_List.Value as IList;
                    Type listType = list.GetType();
                    Type itemType = listType.GetItemType();
                    m_Value.SetValueType(itemType);
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List == null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}