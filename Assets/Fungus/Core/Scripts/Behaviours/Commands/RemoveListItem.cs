﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/List",
                 "Remove an item from a list.")]
    [AddComponentMenu("Fungus/Commands/Variable/List/Remove List Item")]
    public class RemoveListItem : Command
    {
        [Tooltip("The target list")]
        [VariableOfTypeField(typeof(IList))]
        [CheckNotNull]
        public Variable m_List;

        [Tooltip("The item to remove")]
        public GenericVariableData m_Value = new GenericVariableData();

        public override void OnEnter()
        {
            IList list = m_List.Value as IList;
            object value = m_Value.Value;

            list.Remove(value);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("'{0}' from \"{1}\"", m_Value, m_List);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List != null)
                {
                    IList list = m_List.Value as IList;
                    Type listType = list.GetType();
                    Type itemType = listType.GetItemType();
                    m_Value.SetValueType(itemType);
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_List == null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}