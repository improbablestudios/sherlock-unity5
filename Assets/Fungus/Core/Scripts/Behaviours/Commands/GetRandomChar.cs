﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random char value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Char")]
    public class GetRandomChar : ReturnValueCommand<char>
    {
        [Tooltip("Possible characters for random char")]
        public StringData m_Characters = new StringData();
        
        [Tooltip("The variable to store the result in")]
        public CharVariable m_ReturnVariable;
        
        public override char GetReturnValue()
        {
            return RandomChar(m_Characters.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static char RandomChar(string characters)
        {
            System.Random random = new System.Random();
            return characters[random.Next(characters.Length)];
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random char", m_ReturnVariable);
        }
    }

}