﻿using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random game object value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Game Object")]
    public class GetRandomGameObject : ReturnValueCommand<GameObject>
    {
        [Tooltip("Parent to get random child from")]
        [CheckNotNullData]
        public GameObjectData m_ParentObject = new GameObjectData();

        [Tooltip("Include inactive children")]
        public BooleanData m_IncludeInactive = new BooleanData();

        [Tooltip("The variable to store the result in")]
        public GameObjectVariable m_ReturnVariable;
        
        public override GameObject GetReturnValue()
        {
            GameObject parentObject = m_ParentObject.Value;
            bool includeInactive = m_IncludeInactive.Value;

            GameObject[] objects = parentObject.GetComponentsInChildren<Transform>(true)
                .Select(i => i.gameObject)
                .Where(i => i != parentObject)
                .ToArray();
            if (!includeInactive)
            {
                objects = objects.Where(i => i.activeSelf).ToArray();
            }
            if (objects.Length > 0)
            {
                return objects[Random.Range(0, (objects.Length - 1))];
            }
            return null;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random game object", m_ReturnVariable);
        }
    }

}