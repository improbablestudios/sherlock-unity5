using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Flow",
                 "Move execution to a specific Label command.")]
    [AddComponentMenu("Fungus/Commands/Flow/Jump")]
    public class Jump : Command
    {
        [Tooltip("Label to jump to")]
        [FormerlySerializedAs("targetLabel")]
        [CheckNotNull]
        public Label m_TargetLabel;

        public override void OnEnter()
        {
            if (m_TargetLabel == null)
            {
                Continue();
                return;
            }

            Block parentBlock = ParentBlock;

            foreach (Command command in parentBlock.Commands)
            {
                Label label = command as Label;
                if (label != null &&
                    label == m_TargetLabel)
                {
                    Continue(label.CommandIndex + 1);
                    break;
                }
            }
        }
        
        public override string GetSummary()
        {
            if (m_TargetLabel != null)
            {
                return m_TargetLabel.m_LabelName;
            }
            return "<None>";
        }
    }

}