﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Set the previous flowchart of a target flowchart. This will determine what screen that flowchart returns to when it activates or switches to the previous flowchart.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Set Previous Flowchart")]
    public class SetPreviousFlowchart : Command
    {
        [Tooltip("The target flowchart")]
        [ObjectDataPopup("<Latest Activated Flowchart>")]
        [FormerlySerializedAs("_targetFlowchart")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The previous flowchart")]
        [ObjectDataPopup("<Latest Activated Flowchart>")]
        [FormerlySerializedAs("_previousFlowchart")]
        public FlowchartData m_PreviousFlowchart = new FlowchartData();

        public override void OnEnter()
        {
            Flowchart flowchart = m_Flowchart.Value;
            Flowchart previousFlowchart = m_PreviousFlowchart.Value;

            if (flowchart == null)
            {
                flowchart = Flowchart.LatestActivatedFlowchart;
            }

            if (previousFlowchart == null)
            {
                previousFlowchart = Flowchart.LatestActivatedFlowchart;
            }

            flowchart.PreviousFlowchart = previousFlowchart;
            previousFlowchart.NextFlowchart = flowchart;

            Continue();
        }

        public override string GetSummary()
        {
            string targetFlowchartString = m_Flowchart.ToString();
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                targetFlowchartString = "<Latest Activated Flowchart>";
            }

            string previousFlowchartString = m_PreviousFlowchart.ToString();
            if (m_PreviousFlowchart.IsConstant && m_PreviousFlowchart.Value == null)
            {
                previousFlowchartString = "<Latest Activated Flowchart>";
            }

            return string.Format("of {0} to {1}", targetFlowchartString, previousFlowchartString);
        }
    }

}