﻿using UnityEngine;
using UnityEngine.Serialization;
using System;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable",
                 "Get a variable by its template.")]
    [AddComponentMenu("Fungus/Commands/Variable/Get Variable Value By Template")]
    public class GetVariableValueByTemplate : ReturnValueCommand<object>
    {
        [Tooltip("The target flowchart")]
        [CheckNotNullData]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The variable to get from the target flowchart")]
        [DontInstantiate]
        [CheckNotNull]
        public Variable m_TemplateVariable;

        [Tooltip("The variable to store the result in")]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            return m_Flowchart.Value.GetVariableValue(m_TemplateVariable);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            string templateVariableString = "<None>";
            if (m_TemplateVariable != null)
            {
                templateVariableString = m_TemplateVariable.GetName();
            }
            return string.Format("{0} = {1}.{2}", m_ReturnVariable, m_Flowchart, templateVariableString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TemplateVariable))
            {
                if (m_Flowchart.IsConstant && m_Flowchart.Value != null)
                {
                    if (m_Flowchart.Value.TemplateFlowchart == null)
                    {
                        return "The selected flowchart does not use a template";
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override Type[] SpecificReturnTypes()
        {
            if (m_TemplateVariable != null)
            {
                return new Type[] { m_TemplateVariable.ValueType };
            }
            return base.SpecificReturnTypes();
        }
    }
}
