﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random vector2 value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Vector2")]
    public class GetRandomVector2 : ReturnValueCommand<Vector2>
    {
        [Tooltip("Minimum value for random range")]
        public Vector2Data m_MinValue = new Vector2Data();

        [Tooltip("Maximum value for random range")]
        public Vector2Data m_MaxValue = new Vector2Data();

        [Tooltip("The variable to store the result in")]
        public Vector2Variable m_ReturnVariable;
        
        public override Vector2 GetReturnValue()
        {
            return RandomVector2(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Vector2 RandomVector2(Vector2 min, Vector2 max)
        {
            return new Vector2(RandomFloat(min.x, max.x), RandomFloat(min.y, max.y));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random vector2", m_ReturnVariable);
        }
    }

}