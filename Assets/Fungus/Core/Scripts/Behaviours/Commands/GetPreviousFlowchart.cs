﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Flowchart",
                 "Get the platform the application is currently running on.")]
    [AddComponentMenu("Fungus/Commands/Flow/Flowchart/Get Previous Flowchart")]
    public class GetPreviousFlowchart : ReturnValueCommand<Flowchart>
    {
        [Tooltip("The target flowchart")]
        [ObjectDataPopup("<This>")]
        [FormerlySerializedAs("m_TargetFlowchart")]
        public FlowchartData m_Flowchart = new FlowchartData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public FlowchartVariable m_ReturnVariable;
        
        public override Flowchart GetReturnValue()
        {
            Flowchart flowchart = m_Flowchart.Value;

            if (flowchart == null)
            {
                flowchart = GetFlowchart();
            }

            return flowchart.PreviousFlowchart;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            string flowchartString = m_Flowchart.ToString();
            if (m_Flowchart.IsConstant && m_Flowchart.Value == null)
            {
                flowchartString = "<This>";
            }

            return string.Format("{0} = Previous Flowchart of {1}", m_ReturnVariable, flowchartString);
        }
    }

}