﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Wait",
                 "Wait for user input before executing the next command in the block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Wait/Wait For Input")]
    public class WaitForInput : Command
    {
        public override void OnEnter()
        {
            StartCoroutine(CheckInput());
        }

        IEnumerator CheckInput()
        {
            yield return new WaitForEndOfFrame();

            while (!(Input.anyKeyDown || Input.GetMouseButtonDown(0)))
            {
                yield return null;
            }

            Continue();
        }

        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}

