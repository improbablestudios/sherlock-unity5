﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random boolean value.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Boolean")]
    public class GetRandomBoolean : ReturnValueCommand<bool>
    {
        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            return RandomBool();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static bool RandomBool()
        {
            System.Random rng = new System.Random();
            return rng.Next(0, 2) > 0;
        }
        
        public override string GetSummary()
        {
            return string.Format("{0} = random boolean", m_ReturnVariable);
        }
    }

}