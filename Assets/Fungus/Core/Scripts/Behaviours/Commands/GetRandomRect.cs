﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Random",
                 "Get a random rect value in the defined range.")]
    [AddComponentMenu("Fungus/Commands/Variable/Random/Get Random Rect")]
    public class GetRandomRect : ReturnValueCommand<Rect>
    {
        [Tooltip("Minimum value for random range")]
        public RectData m_MinValue = new RectData();

        [Tooltip("Maximum value for random range")]
        public RectData m_MaxValue = new RectData();

        [Tooltip("The variable to store the result in")]
        public RectVariable m_ReturnVariable;
        
        public override Rect GetReturnValue()
        {
            return RandomRect(m_MinValue.Value, m_MaxValue.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public static Rect RandomRect(Rect min, Rect max)
        {
            return new Rect(RandomVector2(min.position, max.position), RandomVector2(min.size, max.size));
        }

        public static Vector2 RandomVector2(Vector2 min, Vector2 max)
        {
            return new Vector2(RandomFloat(min.x, max.x), RandomFloat(min.y, max.y));
        }

        public static float RandomFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public override string GetSummary()
        {
            return string.Format("{0} = random rect", m_ReturnVariable);
        }
    }

}