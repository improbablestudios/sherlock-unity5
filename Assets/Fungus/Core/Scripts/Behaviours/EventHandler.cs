using UnityEngine;
using UnityEngine.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Reflection;
using System.Collections;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UIUtilities;

namespace Fungus
{

    public class EventHandlerInfoAttribute : FlowchartItemInfoAttribute
    {
        public EventHandlerInfoAttribute(int priority = 0) : base(null, null, null, priority)
        {
        }

        public EventHandlerInfoAttribute(string category, string description, int priority = 0) : base(null, category, description, priority)
        {
        }

        public EventHandlerInfoAttribute(string name, string category, string description, int priority = 0) : base(name, category, description, priority)
        {
        }
        
        public static new EventHandlerInfoAttribute GetInfo(Type variableType)
        {
            return GetInfo<EventHandlerInfoAttribute>(variableType);
        }
    }

    /// <summary>
    /// A Block may have an associated Event Handler which starts executing commands when
    /// a specific event occurs.
    /// To create a custom Event Handler, simply subclass EventHandler and call the ExecuteBlock() method
    /// when the event occurs.
    /// Add an EventHandlerInfo attibute and your new EventHandler class will automatically appear in the
    /// 'Execute On Event' dropdown menu when a block is selected.
    /// </summary>
    [EventHandlerInfo]
    [AddComponentMenu("Fungus/Event Handler")]
    public abstract class EventHandler : FlowchartItem, ISavable
    {
        public enum TriggerCondition
        {
            Starts,
            Stops,
            Is,
            IsNot
        }

        [Serializable]
        public class EventHandlerSaveState : BehaviourSaveState<EventHandler>
        {
        }
        
        protected bool m_hasInitialized;
        
        public bool HasInitialized
        {
            get
            {
                return m_hasInitialized;
            }
        }

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return true;
        }

        protected override void Awake()
        {
            base.Awake();
            
            if (Application.isPlaying)
            {
                m_hasInitialized = true;

                string errorMessage = GetFirstError();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    Log(DebugLogType.Error, errorMessage);
                    return;
                }
            }
        }

        public virtual SaveState GetSaveState()
        {
            return new EventHandlerSaveState();
        }

        public virtual bool WillExecuteBlockOnFlowchartStart()
        {
            return false;
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            AddListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            RemoveListener();
        }
        
        protected virtual void AddListener()
        {
        }

        protected virtual void RemoveListener()
        {
        }
        
        public override string[] GetErrors()
        {
            List<string> errors = new List<string>();

            foreach (ReturnValueCommand returnValueCommand in GetReturnValueCommandReferences())
            {
                string[] returnValueCommandErrors = returnValueCommand.GetErrors();
                if (returnValueCommandErrors.Length > 0)
                {
                    errors.AddRange(returnValueCommandErrors);
                }
            }

            return base.GetErrors();
        }

        public override string GetFirstError()
        {
            foreach (ReturnValueCommand returnValueCommand in GetReturnValueCommandReferences())
            {
                string returnValueCommandError = returnValueCommand.GetFirstError();
                if (returnValueCommandError != null)
                {
                    return returnValueCommandError;
                }
            }

            return base.GetFirstError();
        }

        public override string GetName()
        {
            string name = "";
            if (m_ParentFlowchartItem != null)
            {
                name += m_ParentFlowchartItem.GetName() + " ";
            }
            return name + "<" + this.GetType() + ">";
        }

        public override string ToString()
        {
            string name = "";
            if (m_ParentFlowchartItem != null)
            {
                name += m_ParentFlowchartItem.GetName() + " ";
            }
            return name + "<" + this.GetType() + ">";
        }

        public virtual int ExecutionFrameDelay()
        {
            return 0;
        }

        /// <summary>
        /// The Event Handler should call this method when the event is detected and the flowchart is not in the process of activating or deactivating.
        /// </summary>
        public void ExecuteBlock()
        {
            int delay = ExecutionFrameDelay();
            if (delay > 0)
            {
                StartCoroutine(ExecuteBlockDelayed(delay));
            }
            else
            {
                ExecuteBlockInternal();
            }
        }

        private IEnumerator ExecuteBlockDelayed(int frames)
        {
            yield return new WaitForFrames(frames);

            ExecuteBlockInternal();
        }

        private void ExecuteBlockInternal()
        {
            if (Application.isPlaying && !GetFlowchart().IsDeactivating)
            {
                Block parentBlock = ParentBlock;
                if (parentBlock == null)
                {
                    return;
                }

                Log(DebugLogType.Info, "TRIGGER");

                parentBlock.Execute();
            }
        }

        public void Log(DebugLogType logType, string logMessage)
        {
            Block parentBlock = GetRootParentFlowchartItem() as Block;

            string message = string.Format("'{0} ({1})': {2}", GetType().Name, parentBlock, logMessage);

            base.Log(FlowchartLoggingOptions.EventHandler, logType, message);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (!Application.isPlaying || m_hasInitialized)
            {
                return base.GetPropertyError(propertyPath);
            }
            return null;
        }
    }
}
