using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using System.IO;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif
using ImprobableStudios.Localization;

namespace Fungus
{
    public enum SetOperator
    {
        Assign,        // =
        Negate,        // =!
        Add,         // +=
        Subtract,    // -=
        Multiply,    // *=
        Divide        // /=
    }

    public class VariableInfoAttribute : FlowchartItemInfoAttribute
    {
        public VariableInfoAttribute(int priority = 0) : base(null, null, "", priority)
        {
        }

        public VariableInfoAttribute(string category, int priority = 0) : base(null, category, "", priority)
        {
        }

        public VariableInfoAttribute(string name, string category, int priority = 0) : base(name, category, "", priority)
        {
        }

        public static new VariableInfoAttribute GetInfo(Type variableType)
        {
            return GetInfo<VariableInfoAttribute>(variableType);
        }

        protected override string GetDefaultCategory(Type type)
        {
            Type variableValueType = Variable.GetVariableValueType(type);
                
            if (variableValueType != null)
            {
                string typeFullName = null;
                if (typeof(IList).IsAssignableFrom(variableValueType))
                {
                    Type itemType = variableValueType.GetItemType();
                    typeFullName += "List" + "." + itemType.GetNiceTypeFullName();
                }
                else
                {
                    typeFullName = variableValueType.GetNiceTypeFullName();
                }
                
                if (typeFullName != null)
                {
                    return Path.GetDirectoryName(typeFullName.Replace(".", "/").Replace("+", "/")).Replace("\\", "/");
                }
            }

            return base.GetDefaultCategory(type);
        }

        protected override string GetDefaultName(Type type)
        {
            Type variableValueType = Variable.GetVariableValueType(type);

            if (variableValueType != null)
            {
                return variableValueType.GetNiceTypeName();
            }

            return base.GetDefaultName(type);
        }
    }

    [VariableInfo]
    [AddComponentMenu("Fungus/Variable")]
    public abstract class Variable : FlowchartItem, ISavable, IExplicitLocalizable
    {
        public const string LOCALIZED_MEMBER_KEY = "Value";

        public Variable m_TemplateVariable;

        [NonSerialized]
        public Variable m_ChildVariable;
        
        [SerializeField]
        [FormerlySerializedAs("variableName")]
        protected string m_VariableName = "";

        [SerializeField]
        [FormerlySerializedAs("scope")]
        protected Scope m_Scope;

        [SerializeField]
        [FormerlySerializedAs("localize")]
        [FormerlySerializedAs("m_localize")]
        protected bool m_Localize;

        [Tooltip("The position of this variable in its parent flowchart")]
        [HideInInspector]
        [SerializeField]
        private int m_VariableIndex = -1;

        public string VariableName
        {
            get
            {
                return m_VariableName;
            }
            set
            {
                m_VariableName = value;
            }
        }

        public Scope Scope
        {
            get
            {
                return m_Scope;
            }
            set
            {
                m_Scope = value;
            }
        }

        public bool Localize
        {
            get
            {
                return m_Localize;
            }
            set
            {
                m_Localize = value;
            }
        }
        
        public int VariableIndex
        {
            get
            {
                return m_VariableIndex;
            }
            set
            {
                m_VariableIndex = value;
            }
        }

        public abstract SaveState GetSaveState();

        public object Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        public object OriginalValue
        {
            get
            {
                return GetOriginalValue();
            }
        }

        public abstract Type ValueType
        {
            get;
        }
        
        public virtual void SetupName(List<Variable> variableList, string name, string defaultName)
        {
            string uniqueName = name;
            if (name != null)
            {
                uniqueName = this.GetUniqueVariableName(variableList, name, defaultName);
            }
            SetName(uniqueName);
        }

        public override void OnReset()
        {
            ResetValue();
        }

        public abstract void ResetValue();

        public abstract object GetValue();

        public abstract void SetValue(object objValue);

        public abstract object GetOriginalValue();
        
        public static string GetSetOperatorDisplayString(SetOperator setOperator)
        {
            switch (setOperator)
            {
                default:
                case SetOperator.Assign:
                    return "=";
                case SetOperator.Negate:
                    return "=!";
                case SetOperator.Add:
                    return "+=";
                case SetOperator.Subtract:
                    return "-=";
                case SetOperator.Multiply:
                    return "*=";
                case SetOperator.Divide:
                    return "/=";
            }
        }

        public void DoSetOperation(SetOperator setOperator, object value)
        {
            if (typeof(bool).IsAssignableFrom(ValueType))
            {
                bool boolValue = (bool)value;

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = boolValue;
                        break;
                    case SetOperator.Negate:
                        Value = !boolValue;
                        break;
                }
            }
            else if (typeof(float).IsAssignableFrom(ValueType))
            {
                float floatValue = 0f;

                if (typeof(int).IsAssignableFrom(value.GetType()))
                {
                    floatValue = (float)(int)value;
                }
                else
                {
                    floatValue = (float)value;
                }

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = floatValue;
                        break;
                    case SetOperator.Add:
                        Value = (float)Value + floatValue;
                        break;
                    case SetOperator.Subtract:
                        Value = (float)Value - floatValue;
                        break;
                    case SetOperator.Multiply:
                        Value = (float)Value * floatValue;
                        break;
                    case SetOperator.Divide:
                        Value = (float)Value / floatValue;
                        break;
                }
            }
            else if (typeof(int).IsAssignableFrom(ValueType))
            {
                int intValue = 0;

                if (typeof(float).IsAssignableFrom(value.GetType()))
                {
                    intValue = (int)(float)value;
                }
                else
                {
                    intValue = (int)value;
                }

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = intValue;
                        break;
                    case SetOperator.Add:
                        Value = (int)Value + intValue;
                        break;
                    case SetOperator.Subtract:
                        Value = (int)Value - intValue;
                        break;
                    case SetOperator.Multiply:
                        Value = (int)Value * intValue;
                        break;
                    case SetOperator.Divide:
                        Value = (int)Value / intValue;
                        break;
                }
            }
            else if (typeof(Vector2).IsAssignableFrom(ValueType))
            {
                Vector2 vector2Value = (Vector2)value;

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = vector2Value;
                        break;
                    case SetOperator.Add:
                        Value = (Vector2)Value + vector2Value;
                        break;
                    case SetOperator.Subtract:
                        Value = (Vector2)Value - vector2Value;
                        break;
                }
            }
            else if (typeof(Vector3).IsAssignableFrom(ValueType))
            {
                Vector3 vector3Value = (Vector3)value;

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = vector3Value;
                        break;
                    case SetOperator.Add:
                        Value = (Vector3)Value + vector3Value;
                        break;
                    case SetOperator.Subtract:
                        Value = (Vector3)Value - vector3Value;
                        break;
                }
            }
            else if (typeof(string).IsAssignableFrom(ValueType))
            {
                string stringValue = (string)value;

                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = stringValue;
                        break;
                    case SetOperator.Add:
                        Value = (string)Value + stringValue;
                        break;
                }
            }
            else
            {
                switch (setOperator)
                {
                    case SetOperator.Assign:
                        Value = value;
                        break;
                }
            }
        }

        public Variable GetRootTemplateVariable()
        {
            if (m_TemplateVariable == null)
            {
                return this;
            }

            return m_TemplateVariable.GetRootTemplateVariable();
        }

        public Variable GetRootChildVariable()
        {
            if (m_Scope == Scope.Private || m_ChildVariable == null)
            {
                return this;
            }

            return m_ChildVariable.GetRootChildVariable();
        }

        public virtual List<Variable> GetTemplateVariableChain()
        {
            List<Variable> variablesInChain = new List<Variable>();
            GetTemplateVariableChainInternal(variablesInChain);
            return variablesInChain;
        }

        private void GetTemplateVariableChainInternal(List<Variable> variablesInChain)
        {
            if (m_TemplateVariable != null)
            {
                variablesInChain.Add(m_TemplateVariable);
                m_TemplateVariable.GetTemplateVariableChainInternal(variablesInChain);
            }
        }
        
        public override string GetName()
        {
            return GetRootTemplateVariable().m_VariableName;
        }

        public void SetName(string variableName)
        {
            m_VariableName = variableName;
        }

        public Scope GetScope()
        {
            return GetRootChildVariable().GetScopeInternal();
        }

        protected Scope GetScopeInternal()
        {
            return m_Scope;
        }

        public bool GetLocalize()
        {
            return GetRootTemplateVariable().m_Localize;
        }

        public void SetLocalize(bool localize)
        {
            m_Localize = localize;
        }

        public override string ToString()
        {
            return GetName();
        }

        public override string GetOrderedId()
        {
            return base.GetOrderedId() + "." + "VARIABLE" + "." + GetFlowchart().Variables.IndexOf(this);
        }

#if UNITY_EDITOR
        public static Variable CreateVariable(Component componentContainingList, Type variableType, string name, FlowchartItem parentFlowchartItem, Scope scope, bool allowUndo = true)
        {
            Variable variable = null;

            if (allowUndo)
            {
                variable = Undo.AddComponent(componentContainingList.gameObject, variableType) as Variable;
            }
            else
            {
                variable = componentContainingList.gameObject.AddComponent(variableType) as Variable;
            }

            if (variable.IsValid())
            {
                variable.VariableName = name;
                variable.m_ParentFlowchartItem = parentFlowchartItem;
                variable.Scope = scope;
            }
            
            return variable;
        }

        public static void AddVariableToList(Component componentContainingList, List<Variable> variableList, Variable variable, int index, string defaultName, bool allowUndo = true)
        {
            if (variableList != null && variable != null)
            {
                if (allowUndo)
                {
                    Undo.RecordObject(componentContainingList, "Add Variable");
                }

                variable.Setup();
                variable.SetupName(variableList, variable.VariableName, defaultName);

                if (variableList != null)
                {
                    if (index >= 0 && index < variableList.Count)
                    {
                        variableList.Insert(index, variable);
                    }
                    else
                    {
                        variableList.Add(variable);
                    }
                }

                BaseBehaviour baseBehaviour = componentContainingList as BaseBehaviour;
                if (baseBehaviour != null)
                {
                    baseBehaviour.RecordChanges();
                }
            }
        }

        public static void DestroyVariable(Component componentContainingList, Variable variable, bool allowUndo = true)
        {
            if (allowUndo)
            {
                Undo.DestroyObjectImmediate(variable);
            }
            else
            {
                DestroyImmediate(variable);
            }
        }

        public static void RemoveVariableFromList(Component componentContainingList, List<Variable> variableList, Variable variable, bool allowUndo = true)
        {
            if (variableList != null && variable != null)
            {
                if (allowUndo)
                {
                    Undo.RecordObject(componentContainingList, "Remove Variable");
                }

                if (variableList.Contains(variable))
                {
                    variableList.Remove(variable);
                }

                BaseBehaviour baseBehaviour = componentContainingList as BaseBehaviour;
                if (baseBehaviour != null)
                {
                    baseBehaviour.RecordChanges();
                }
            }
        }
        
        public static bool ValidateVariablesInList(Component componentContainingList, List<Variable> variableList, List<Variable> templateVariableList, string defaultName, Scope defaultScope, bool deleteVariableIfTemplateInvalid, Block parentBlock = null)
        {
            bool changed = false;

            if (variableList == null)
            {
                return changed;
            }

            foreach (Variable variable in variableList.ToList())
            {
                if (variable == null)
                {
                    variableList.Remove(variable);
                    changed = true;
                }
                else
                {
                    if (!variable.enabled)
                    {
                        variable.enabled = true;
                        changed = true;
                    }

                    if (variable.IsTemplateVariablePrivate() || variable.IsTemplateVariableNotInTemplateFlowchart() || variable.WasTemplateVariableDestroyed())
                    {
                        if (deleteVariableIfTemplateInvalid || variable.Value == variable.ValueType.GetDefaultValue())
                        {
                            RemoveVariableFromList(componentContainingList, variableList, variable);
                            DestroyVariable(componentContainingList, variable);
                        }
                        else
                        {
                            if (variable.GetName() != null)
                            {
                                variable.m_VariableName = variable.GetName();
                            }
                            else
                            {
                                variable.m_VariableName = defaultName;
                            }
                            variable.SetLocalize(variable.GetLocalize());
                            variable.m_TemplateVariable = null;
                        }
                        changed = true;
                    }
                }
            }

            foreach (Variable templateVariable in templateVariableList)
            {
                if (templateVariable.Scope != Scope.Private)
                {
                    if (variableList.Find(i => i.m_TemplateVariable == templateVariable) == null)
                    {
                        Variable variableWithSameName = GetVariableInList(variableList, templateVariable.GetName(), templateVariable.GetType());
                        if (variableWithSameName != null)
                        {
                            variableWithSameName.m_TemplateVariable = templateVariable;
                            variableWithSameName.m_VariableName = null;
                            variableWithSameName.m_Scope = defaultScope;
                            if (variableWithSameName.ValueType == typeof(string))
                            {
                                variableWithSameName.SetLocalize(true);
                            }
                            else
                            {
                                variableWithSameName.SetLocalize(false);
                            }
                        }
                        else
                        {
                            if (componentContainingList != null)
                            {
                                Flowchart flowchart = componentContainingList.GetComponent<Flowchart>();
                                if (flowchart != null)
                                {
                                    Variable overrideVariable = CreateVariable(componentContainingList, templateVariable.GetType(), null, parentBlock, defaultScope);
                                    AddVariableToList(componentContainingList, variableList, overrideVariable, variableList.Count, defaultName);
                                    overrideVariable.m_TemplateVariable = templateVariable;
                                    overrideVariable.m_VariableName = null;
                                    overrideVariable.m_Scope = defaultScope;
                                }
                            }
                        }
                        changed = true;
                    }
                }
            }

            return changed;
        }

        protected bool IsTemplateVariablePrivate()
        {
            return ((m_TemplateVariable != null) && (m_TemplateVariable.Scope == Scope.Private));
        }

        protected bool IsTemplateVariableNotInTemplateFlowchart()
        {
            return ((m_TemplateVariable != null) && (m_TemplateVariable.GetFlowchart() != GetFlowchart().TemplateFlowchart));
        }

        protected bool WasTemplateVariableDestroyed()
        {
            return ((m_TemplateVariable == null && m_VariableName == null));
        }
#endif

        public static Variable GetVariableInList(List<Variable> variableList, string variableName, Type variableType = null)
        {
            foreach (Variable v in variableList)
            {
                if (v.GetName() == variableName && (variableType == null || v.GetType() == variableType))
                {
                    return v;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a new variable key that is guaranteed not to clash with any existing variable in the list.
        /// </summary>
        /// <param name="variables">The variable list</param>
        /// <param name="newName">The name you want to use</param>
        /// <param name="defaultName">The name to use if <paramref name="newName"/> is null or empty</param>
        /// <returns>A unique variable name</returns>
        public string GetUniqueVariableName(List<Variable> variables, string newName, string defaultName)
        {
            // Only letters and digits allowed
            char[] arr = newName.Where(c => (char.IsLetterOrDigit(c) || c == '_')).ToArray();
            newName = new string(arr);

            // No leading digits allowed
            newName = newName.TrimStart('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            
            string[] names = variables.Where(x => x != null).Select(x => x.GetName()).ToArray();
            int ignoreIndex = variables.IndexOf(this);

            return names.GetUniqueName(newName, defaultName, ignoreIndex);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_ChildVariable == null)
            {
                LocalizationManager.Instance.RegisterLocalizable(this);
            }
        }

        public abstract Dictionary<string, object> GetLocalizedObjectDictionary();

        public abstract void RegisterLocalizableObjectValue(Dictionary<string, object> localizedDictionary);

        //
        // ILocalizable implementation
        //
        
        public abstract string GetLocalizableOrderedPriority();

        public abstract void OnLocalizeComplete();

        public abstract void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary);

        public abstract void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary);

        public abstract void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary);

        public abstract void ExplicitlyGetConverters(Dictionary<string, Type> dictionary);
        
        public static Type GetVariableValueType(Type type)
        {
            if (type == typeof(Variable))
            {
                return typeof(object);
            }
            Type baseVariableType = GetBaseVariableType(type);
            if (baseVariableType != null)
            {
                return baseVariableType.GetGenericArguments()[0];
            }
            return null;
        }

        protected static Type GetBaseVariableType(Type type)
        {
            while (type != null && type != typeof(object))
            {
                if (type.BaseType != null && type.BaseType == typeof(Variable) && type.IsGenericType)
                {
                    return type;
                }
                type = type.BaseType;
            }
            return null;
        }

        public static Type[] GetVariableTypeEquivalents(Type[] types)
        {
            HashSet<Type> variableTypes = new HashSet<Type>();
            foreach (Type t in (typeof(Variable<>)).FindAllDerivedTypesOfGenericType())
            {
                Type baseGenericType = t.GetBaseGenericType();
                if (baseGenericType != null)
                {
                    foreach (Type type in types)
                    {
                        if (type != null)
                        {
                            if (type == typeof(object))
                            {
                                variableTypes.Add(typeof(Variable));
                            }
                            else
                            {
                                Type valueType = baseGenericType.GetGenericArguments()[0];
                                if (type.IsAssignableFrom(valueType))
                                {
                                    variableTypes.Add(baseGenericType);
                                }
                            }
                        }
                    }
                }
            }
            
            if (types.Contains(typeof(float)) || types.Contains(typeof(int)))
            {
                variableTypes.Add(typeof(Variable<float>));
                variableTypes.Add(typeof(Variable<int>));
            }

            return variableTypes.ToArray();
        }

        public static Type GetVariableType(Type valueType)
        {
            foreach (Type t in (typeof(Variable<>)).FindAllDerivedTypesOfGenericType())
            {
                if (valueType == GetVariableValueType(t))
                {
                    return t;
                }
            }
            return null;
        }
        
        public void Log(DebugLogType logType, string logMessage)
        {
            string message = string.Format("'{0} {1}': {2}", FlowchartItemInfoAttribute.GetInfo(GetType()).GetName(GetType()), m_VariableName, logMessage);

            base.Log(FlowchartLoggingOptions.Variable, logType, message);
        }
    }

    public abstract class Variable<T> : Variable
    {
        [Serializable]
        public class VariableSaveState : BehaviourSaveState<Variable<T>>
        {
            public override void Save(Variable<T> variable)
            {
                base.Save(variable);

                if (!EqualityComparer<T>.Default.Equals(variable.m_originalValue, variable.Value))
                {
                    variable.m_valueChanged = true;
                }
            }

            public override void Load(Variable<T> variable)
            {
                base.Load(variable);

                if (variable.m_localizedDictionary != null)
                {
                    variable.Value = variable.m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage];
                }
            }
        }

        [Tooltip("The value stored in the variable")]
        [SerializeField]
        [FormerlySerializedAs("value")]
        [FormerlySerializedAs("m_value")]
        protected T m_Value;
        
        protected Dictionary<string, T> m_localizedDictionary;
        
        protected T m_originalValue;
        
        protected bool m_valueChanged;
        
        public override SaveState GetSaveState()
        {
            if (m_ChildVariable == null)
            {
                return new VariableSaveState();
            }

            return null;
        }

        public virtual new T Value
        {
            get
            {
                return (T)GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        public override Type ValueType
        {
            get
            {
                return typeof(T);
            }
        }

        public virtual new T OriginalValue
        {
            get
            {
                return (T)GetOriginalValue();
            }
        }

        public Variable<T> GetRootGenericChildVariable()
        {
            return ((Variable<T>)GetRootChildVariable());
        }

        public override void ResetValue()
        {
            GetRootGenericChildVariable().ResetValueInternal();
        }

        protected void ResetValueInternal()
        {
            SetValue((T)GetOriginalValue());
        }

        public override object GetOriginalValue()
        {
            return GetRootGenericChildVariable().GetOriginalValueInternal();
        }

        protected object GetOriginalValueInternal()
        {
            return m_originalValue;
        }

        public override object GetValue()
        {
            if (m_ChildVariable == null)
            {
                this.InitializePersistableField(x => x.m_Value);
            }
            return GetRootGenericChildVariable().GetValueInternal();
        }

        protected object GetValueInternal()
        {
            Log(DebugLogType.Info, string.Format("GET \"{0}\"", m_Value));
            return m_Value;
        }

        public override void SetValue(object objValue)
        {
            GetRootGenericChildVariable().SetValueInternal(objValue);
        }

        protected void SetValueInternal(object objValue)
        {
            Log(DebugLogType.Info, string.Format("SET = \"{0}\"", objValue));
            if (objValue == null)
            {
                m_Value = default(T);
            }
            else if (objValue is UnityEngine.Object && (objValue as UnityEngine.Object) == null)
            {
                m_Value = default(T);
            }
            else
            {
                object castObjValue = objValue;
                if (typeof(T) == typeof(int) && objValue is float)
                {
                    float floatVal = (float)objValue;
                    object intObj = (int)floatVal;
                    castObjValue = intObj;

                }
                else if (typeof(T) == typeof(float) && objValue is int)
                {
                    int intVal = (int)objValue;
                    object floatObj = (float)intVal;
                    castObjValue = floatObj;
                }
                m_Value = (T)castObjValue;
            }
        }

        public override Dictionary<string, object> GetLocalizedObjectDictionary()
        {
            if (GetLocalizedDictionary() != null)
            {
                return GetLocalizedDictionary().ToDictionary(k => k.Key, k => (object)k.Value);
            }
            return null;
        }

        public Dictionary<string, T> GetLocalizedDictionary()
        {
            return GetRootGenericChildVariable().GetLocalizedDictionaryInternal();
        }

        protected Dictionary<string, T> GetLocalizedDictionaryInternal()
        {
            if (m_localizedDictionary != null)
            {
                return m_localizedDictionary;
            }
            return this.SafeGetLocalizedDictionary(x => x.Value);
        }

        public override void RegisterLocalizableObjectValue(Dictionary<string, object> localizedDictionary)
        {
            Dictionary<string, T> genericLocalizedDictionary = localizedDictionary.ToDictionary(k => k.Key, k => (T)k.Value);
            RegisterLocalizableValue(genericLocalizedDictionary);
        }

        public void RegisterLocalizableValue(Dictionary<string, T> localizedDictionary)
        {
            GetRootGenericChildVariable().RegisterLocalizableValueInternal(localizedDictionary);
        }

        protected void RegisterLocalizableValueInternal(Dictionary<string, T> localizedDictionary)
        {
            m_localizedDictionary = localizedDictionary;
        }

        protected override void Awake()
        {
            base.Awake();

            if (m_ChildVariable == null)
            {
                this.LoadPersistableField(x => x.m_Value);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (!m_valueChanged)
            {
                m_originalValue = m_Value;
            }
        }

        //
        // ILocalizable implementation
        //
        
        public override string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public override void OnLocalizeComplete()
        {
            if (LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(ValueType))
            {
                if (m_localizedDictionary != null)
                {
                    Value = m_localizedDictionary[LocalizationManager.Instance.ActiveLanguage];
                }
            }
        }

        public override void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary)
        {
            if (LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(ValueType) && GetLocalize())
            {
                dictionary[LOCALIZED_MEMBER_KEY] = Value;
            }
        }

        public override void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary)
        {
            if (LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(ValueType) && GetLocalize())
            {
                if (m_localizedDictionary == null)
                {
                    if (dictionary.ContainsKey(LOCALIZED_MEMBER_KEY))
                    {
                        Value = (T)dictionary[LOCALIZED_MEMBER_KEY];
                    }
                }
            }
        }

        public override void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary)
        {
        }

        public override void ExplicitlyGetConverters(Dictionary<string, Type> dictionary)
        {
        }
    }

}
