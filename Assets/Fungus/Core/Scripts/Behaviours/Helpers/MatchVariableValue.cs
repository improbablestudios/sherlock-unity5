﻿using ImprobableStudios.BaseUtilities;
using DG.Tweening;
using ImprobableStudios.ReflectionUtilities;
using System;
using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.SaveSystem;

namespace Fungus
{

    [AddComponentMenu("Fungus/Behaviours/Match Variable Value")]
    public class MatchVariableValue : PersistentBehaviour
    {
        [Tooltip("The variable who's value will be quarried")]
        [SerializeField]
        [FormerlySerializedAs("variable")]
        [CheckNotNull]
        protected Variable m_Variable;

        [Tooltip("The component who's property will be set")]
        [SerializeField]
        [FormerlySerializedAs("componentTypeName")]
        [CheckNotEmpty]
        protected string m_ComponentTypeName = "";

        [Tooltip("The property that will be set")]
        [SerializeField]
        [FormerlySerializedAs("componentPropertyName")]
        [CheckNotEmpty]
        protected string m_ComponentPropertyName = "";

        [Tooltip("Animates the value changing (only applicable for integers and floats)")]
        [SerializeField]
        [FormerlySerializedAs("tweenValueOnChange")]
        protected bool m_TweenValueOnChange;

        [Tooltip("The speed of the value animation")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("tweenSpeed")]
        protected float m_TweenSpeed = 1f;
        
        protected bool m_isTweening;
        
        protected bool m_isFirstUpdate = true;

        private SerializedMember m_SerializedMember;

        public Variable Variable
        {
            get
            {
                return m_Variable;
            }
            set
            {
                m_Variable = value;
            }
        }

        public string ComponentTypeName
        {
            get
            {
                return m_ComponentTypeName;
            }
        }

        public string ComponentPropertyName
        {
            get
            {
                return m_ComponentPropertyName;
            }
        }

        public bool TweenValueOnChange
        {
            get
            {
                return m_TweenValueOnChange;
            }
            set
            {
                m_TweenValueOnChange = value;
            }
        }

        public float TweenSpeed
        {
            get
            {
                return m_TweenSpeed;
            }
            set
            {
                m_TweenSpeed = value;
            }
        }

        public Type ComponentType
        {
            get
            {
                if (!string.IsNullOrEmpty(m_ComponentTypeName))
                {
                    return Type.GetType(m_ComponentTypeName);
                }
                return null;
            }
            set
            {
                m_ComponentPropertyName = value.AssemblyQualifiedName;
                m_SerializedMember = Component.GetSerializableSerializedMember(m_ComponentPropertyName, ReflectionExtensions.GetMemberKey, UnityReflectionExtensions.IsReflectableUnityType);
            }
        }

        public Component Component
        {
            get
            {
                if (this != null)
                {
                    if (ComponentType != null)
                    {
                        return this.GetComponent(ComponentType);
                    }
                }
                return null;
            }
        }

        public SerializedMember SerializedMember
        {
            get
            {
                if (m_SerializedMember == null)
                {
                    m_SerializedMember = Component.GetSerializableSerializedMember(m_ComponentPropertyName, ReflectionExtensions.GetMemberKey, UnityReflectionExtensions.IsReflectableUnityType);
                }
                return m_SerializedMember;
            }
        }

        public object MemberValue
        {
            get
            {
                if (Component != null)
                {
                    return SerializedMember.Value;
                }
                return null;
            }
            set
            {
                if (Component != null)
                {
                    if (value != null && typeof(float).IsAssignableFrom(value.GetType()) && typeof(int).IsAssignableFrom(MemberValueType))
                    {
                        SerializedMember.Value = (int)(float)value;
                    }
                    else if (value != null && typeof(int).IsAssignableFrom(value.GetType()) && typeof(float).IsAssignableFrom(MemberValueType))
                    {
                        SerializedMember.Value = (float)(int)value;
                    }
                    else
                    {
                        SerializedMember.Value = value;
                    }
                }
            }
        }

        public Type MemberValueType
        {
            get
            {
                if (SerializedMember != null)
                {
                    return SerializedMember.ValueType;
                }
                return null;
            }
        }

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return true;
        }

        protected override void Awake()
        {
            base.Awake();
            Update();
        }

        public void Update()
        {
            if (Component != null && m_Variable != null)
            {
                if (m_Variable.Value != MemberValue)
                {
                    if (m_TweenValueOnChange && !m_isFirstUpdate && !MemberValue.Equals(m_Variable.Value) && (typeof(float).IsAssignableFrom(m_Variable.ValueType) || typeof(int).IsAssignableFrom(m_Variable.ValueType)))
                    {
                        if (!m_isTweening)
                        {
                            m_isTweening = true;

                            float valueStart = 0f;
                            float valueEnd = 0f;
                            if (typeof(float).IsAssignableFrom(MemberValue.GetType()) && typeof(int).IsAssignableFrom(m_Variable.ValueType))
                            {
                                valueStart = (float)MemberValue;
                                valueEnd = (int)m_Variable.Value;
                            }
                            if (typeof(int).IsAssignableFrom(MemberValue.GetType()) && typeof(float).IsAssignableFrom(m_Variable.ValueType))
                            {
                                valueStart = (int)MemberValue;
                                valueEnd = (float)m_Variable.Value;
                            }

                            DOVirtual.Float(
                                valueStart, 
                                valueEnd, 
                                m_TweenSpeed,
                                (float v) =>
                                {
                                    MemberValue = v;
                                })
                                .SetSpeedBased(true)
                                .OnComplete(
                                    () => 
                                    {
                                        m_isTweening = false;
                                    });
                        }
                    }
                    else
                    {
                        MemberValue = m_Variable.Value;

                        m_isFirstUpdate = false;
                    }
                }
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ComponentTypeName))
            {
                if (m_Variable == null)
                {
                    return false;
                }
            }
            else if (propertyPath == nameof(m_ComponentPropertyName))
            {
                if (m_Variable == null)
                {
                    return false;
                }
                if (ComponentType == null)
                {
                    return false;
                }
            }
            else if (propertyPath == nameof(m_TweenValueOnChange))
            {
                if (m_Variable == null)
                {
                    return false;
                }
                if (!typeof(float).IsAssignableFrom(m_Variable.ValueType) && !typeof(int).IsAssignableFrom(m_Variable.ValueType))
                {
                    return false;
                }
            }
            else if (propertyPath == nameof(m_TweenSpeed))
            {
                if (m_Variable == null)
                {
                    return false;
                }
                if (!typeof(float).IsAssignableFrom(m_Variable.ValueType) && !typeof(int).IsAssignableFrom(m_Variable.ValueType))
                {
                    return false;
                }
                if (!m_TweenValueOnChange)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}
