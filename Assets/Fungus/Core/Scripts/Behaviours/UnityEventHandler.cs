﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace Fungus
{

    public abstract class UnityEventHandler : EventHandler
    {
        protected abstract UnityEvent GetUnityEvent();

        protected sealed override void AddListener()
        {
            if (GetUnityEvent() != null)
            {
                GetUnityEvent().AddListener(OnUnityEventInvoked);
            }
        }

        protected sealed override void RemoveListener()
        {
            if (GetUnityEvent() != null)
            {
                GetUnityEvent().RemoveListener(OnUnityEventInvoked);
            }
        }

        protected virtual bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            return true;
        }

        private void OnUnityEventInvoked()
        {
            if (ShouldExecuteBlockWhenUnityEventInvoked())
            {
                ExecuteBlock();
            }
        }
    }

}
