﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.Reflection;
using Fungus.Variables;
using System.IO;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{
    public enum Scope
    {
        Private,
        Protected,
        Public
    }

    public abstract class FlowchartItemInfoAttribute : Attribute
    {
        private static Dictionary<Type, FlowchartItemInfoAttribute> m_cachedInfos = new Dictionary<Type, FlowchartItemInfoAttribute>();

        protected string Name
        {
            get;
            set;
        }

        protected string Category
        {
            get;
            set;
        }

        protected string Description
        {
            get;
            set;
        }

        protected int Priority
        {
            get;
            set;
        }

        ///<summary>
        ///</summary>
        ///<param name="name">The display name of the flowchart item.</param>
        ///<param name="category">The category to place this flowchart item in.</param>
        ///<param name="description">Help information to display in the inspector.</param>
        ///<param name="priority">If two command classes have the same category, the one with highest priority is listed. Negative priority removes the command from the list.</param>
        public FlowchartItemInfoAttribute(string name, string category, string description, int priority = 0)
        {
            this.Category = category;
            this.Name = name;
            this.Description = description;
            this.Priority = priority;
        }

        // Compare delegate for sorting the list of command attributes
        public static int Compare(KeyValuePair<Type, FlowchartItemInfoAttribute> x, KeyValuePair<Type, FlowchartItemInfoAttribute> y)
        {
            int compare = (x.Value.GetCategory(x.Key).CompareTo(y.Value.GetCategory(y.Key)));
            if (compare == 0)
            {
                compare = (x.Value.GetPriority(x.Key).CompareTo(y.Value.GetPriority(y.Key)));
            }
            if (compare == 0)
            {
                compare = (x.Value.GetName(x.Key).CompareTo(y.Value.GetName(y.Key)));
            }
            return compare;
        }

        public static FlowchartItemInfoAttribute GetInfo(Type type)
        {
            if (type != null)
            {
                if (!m_cachedInfos.ContainsKey(type))
                {
                    object[] attributes = type.GetCustomAttributes(typeof(FlowchartItemInfoAttribute), true);
                    foreach (object obj in attributes)
                    {
                        FlowchartItemInfoAttribute infoAttr = obj as FlowchartItemInfoAttribute;
                        if (infoAttr != null)
                        {
                            m_cachedInfos[type] = infoAttr;
                        }
                    }
                }
                if (m_cachedInfos.ContainsKey(type))
                {
                    return m_cachedInfos[type];
                }
            }
            return null;
        }

        protected static T GetInfo<T>(Type type) where T : FlowchartItemInfoAttribute
        {
            if (!m_cachedInfos.ContainsKey(type))
            {
                object[] attributes = type.GetCustomAttributes(typeof(T), true);
                foreach (object obj in attributes)
                {
                    T infoAttr = obj as T;
                    if (infoAttr != null)
                    {
                        m_cachedInfos[type] = infoAttr;
                    }
                }
            }

            return m_cachedInfos[type] as T;
        }
        
        public string GetCategory(Type type)
        {
            if (Category == null)
            {
                return GetDefaultCategory(type);
            }
            return Category;
        }

        public string GetName(Type type)
        {
            if (Name == null)
            {
                return GetDefaultName(type);
            }
            return Name;
        }

        public string GetDescription(Type type)
        {
            if (Description == null)
            {
                return GetDefaultDescription(type);
            }
            return Description;
        }

        public int GetPriority(Type type)
        {
            return Priority;
        }

        protected virtual string GetDefaultCategory(Type type)
        {
            return "";
        }

        protected virtual string GetDefaultName(Type type)
        {
#if UNITY_EDITOR
            return UnityEditor.ObjectNames.NicifyVariableName(type.Name);
#else
            return type.Name;
#endif
        }

        protected virtual string GetDefaultDescription(Type type)
        {
            return "";
        }
    }

    public abstract class FlowchartItem : PersistentBehaviour
    {
        public delegate void OnExecuteDelegate();

        public delegate void OnExecuteAnyDelegate(FlowchartItem item);

        [HideInInspector]
        [FormerlySerializedAs("itemId")]
        [SerializeField]
        protected int m_ItemId = -1; // Invalid flowchart item id

        [Tooltip("The flowchart item this flowchart item belongs to")]
        [HideInInspector]
        [SerializeField]
        [FormerlySerializedAs("parentBlock")]
        protected FlowchartItem m_ParentFlowchartItem;

        private Flowchart m_flowchart;

        private Variable[] m_variableReferences;

        private Block[] m_blockReferences;

        private Flowchart[] m_flowchartReferences;

        private ReturnValueCommand[] m_returnValueCommandReferences;

        private static event OnExecuteAnyDelegate m_onExecuteAnyCallback = delegate { };

        private event OnExecuteDelegate m_onExecuteCallback = delegate { };

        public FlowchartItem ParentFlowchartItem
        {
            get
            {
                if (m_ParentFlowchartItem == null)
                {
                    m_ParentFlowchartItem = FindParentFlowchartItem();
                }
                return m_ParentFlowchartItem;
            }
            set
            {
                m_ParentFlowchartItem = value;
            }
        }

        public Block ParentBlock
        {
            get
            {
                return GetRootParentFlowchartItem() as Block;
            }
        }

        public static OnExecuteAnyDelegate OnExecuteAnyCallback
        {
            get
            {
                return m_onExecuteAnyCallback;
            }
            set
            {
                m_onExecuteAnyCallback = value;
            }
        }

        public OnExecuteDelegate OnExecuteCallback
        {
            get
            {
                return m_onExecuteCallback;
            }
            set
            {
                m_onExecuteCallback = value;
            }
        }

        public int ItemId
        {
            get
            {
                return m_ItemId;
            }
            set
            {
                m_ItemId = value;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (m_ParentFlowchartItem == null)
            {
                m_ParentFlowchartItem = FindParentFlowchartItem();
            }
        }

        protected override void Reset()
        {
            base.Reset();

#if UNITY_EDITOR
            Flowchart flowchart = GetFlowchart();
            UnityEditor.EditorApplication.delayCall += 
                () =>
                {
                    if (flowchart != null)
                    {
                        flowchart.UpdateHideFlags();
                    }
                };
#endif
        }

        public virtual void Validate()
        {
        }

#if UNITY_EDITOR
        public virtual bool IsValid()
        {
            if (!DoesClassAndFileNameMatch())
            {
                string name = GetType().Name;
                string error = "The script's file name does not match the name of the class defined in the script!";
                UnityEditor.EditorUtility.DisplayDialog("Error adding script " + name, error, "OK");
                Debug.LogError(name + ": " + error);
                DestroyImmediate(this, true);
                return false;
            }

            return true;
        }

        public virtual bool DoesClassAndFileNameMatch()
        {
            UnityEditor.MonoScript ms = UnityEditor.MonoScript.FromMonoBehaviour(this);
            string filePath = UnityEditor.AssetDatabase.GetAssetPath(ms);
            string fileName = Path.GetFileNameWithoutExtension(filePath);
            string className = this.GetType().Name;
            if (fileName != className)
            {
                return false;
            }

            return true;
        }
#endif

        public virtual Flowchart GetFlowchart()
        {
            if (m_flowchart == null)
            {
                m_flowchart = GetComponent<Flowchart>();
            }
            return m_flowchart;
        }

        public FlowchartItem FindParentFlowchartItem()
        {
            if (GetFlowchart() != null)
            {
                Block thisBlock = this as Block;
                EventHandler thisEventHandler = this as EventHandler;
                Variable thisVariable = this as Variable;
                Command thisCommand = this as Command;
                if (thisBlock != null)
                {
                    return null;
                }
                if (thisEventHandler != null)
                {
                    foreach (Block block in GetFlowchart().Blocks)
                    {
                        if (block != null)
                        {
                            if (block.EventHandler == thisEventHandler)
                            {
                                return block;
                            }
                        }
                    }
                    return null;
                }
                if (thisCommand != null)
                {
                    foreach (Block block in GetFlowchart().Blocks)
                    {
                        if (block != null)
                        {
                            if (block.Commands.Contains(thisCommand))
                            {
                                return block;
                            }
                            foreach (Command command in block.Commands)
                            {
                                if (command != null)
                                {
                                    foreach (ReturnValueCommand returnValueCommand in command.GetReturnValueCommandReferences())
                                    {
                                        if (returnValueCommand == thisCommand)
                                        {
                                            return command;
                                        }
                                    }
                                }
                            }
                            if (block.EventHandler != null)
                            {
                                foreach (ReturnValueCommand returnValueCommand in block.EventHandler.GetReturnValueCommandReferences())
                                {
                                    if (returnValueCommand == thisCommand)
                                    {
                                        return block.EventHandler;
                                    }
                                }
                            }
                        }
                    }
                    return null;
                }
                if (thisVariable != null)
                {
                    foreach (Block block in GetFlowchart().Blocks)
                    {
                        if (block != null)
                        {
                            if (block.Parameters.Contains(thisVariable))
                            {
                                return block;
                            }
                            foreach (Command command in block.Commands)
                            {
                                if (command != null)
                                {
                                    foreach (Variable variable in command.GetVariableReferences())
                                    {
                                        if (variable == thisVariable)
                                        {
                                            return command;
                                        }
                                    }
                                }
                            }
                            if (block.EventHandler != null)
                            {
                                foreach (Variable variable in block.EventHandler.GetVariableReferences())
                                {
                                    if (variable == thisVariable)
                                    {
                                        return block.EventHandler;
                                    }
                                }
                            }
                        }
                    }
                    return null;
                }
            }

            return null;
        }

        public virtual void Log(FlowchartLoggingOptions loggingOptions, DebugLogType logType, string logMessage)
        {
            GetFlowchart().Log(loggingOptions, logType, logMessage, this);
        }

        public virtual void OnReset()
        {
        }

        public virtual string GetOrderedId()
        {
            return GetFlowchart().name;
        }

        public virtual void CacheVariableReferences()
        {
            m_variableReferences = GetVariableReferences();
        }

        public virtual Variable[] GetCachedVariableReferences()
        {
            if (m_variableReferences == null)
            {
                return m_variableReferences = GetVariableReferences(); 
            }

            return m_variableReferences;
        }

        public virtual Variable[] GetVariableReferences()
        {
            List<Variable> variableReferences = new List<Variable>();
            this.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IfVariable,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetVariableReference(md, mp, hc, m, mt, d, p, dm, variableReferences),
                false);
            return variableReferences.ToArray();
        }

        private void GetVariableReference(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, List<Variable> variableReferences)
        {
            object memberValue = member.GetMemberValue(declaringObj, propertyParameters);
            VariableData variableData = memberValue as VariableData;
            Variable variable = memberValue as Variable;
            if (variableData != null && variableData.VariableDataType == VariableDataType.Variable && variableData.Variable != null)
            {
                variableReferences.Add(variableData.Variable);
            }
            if (variable != null)
            {
                variableReferences.Add(variable);
            }
        }

        private bool IfVariable(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            if (typeof(VariableData).IsAssignableFrom(memberType))
            {
                return true;
            }
            if (typeof(Variable).IsAssignableFrom(memberType))
            {
                return true;
            }
            return false;
        }

        public virtual void CacheBlockReferences()
        {
            m_blockReferences = GetBlockReferences();
        }

        public virtual Block[] GetCachedBlockReferences()
        {
            if (m_blockReferences == null)
            {
                return m_blockReferences = GetBlockReferences();
            }

            return m_blockReferences;
        }

        public virtual Block[] GetBlockReferences()
        {
            List<Block> blockReferences = new List<Block>();
            this.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IfBlock,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetBlockReference(md, mp, hc, m, mt, d, p, dm, blockReferences),
                false);
            return blockReferences.ToArray();
        }

        private void GetBlockReference(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, List<Block> blockReferences)
        {
            object memberValue = member.GetMemberValue(declaringObj, propertyParameters);
            Block block = memberValue as Block;
            if (block != null)
            {
                blockReferences.Add(block);
            }
        }

        private bool IfBlock(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            if (typeof(Block).IsAssignableFrom(memberType))
            {
                return true;
            }
            return false;
        }

        public virtual void CacheFlowchartReferences()
        {
            m_flowchartReferences = GetFlowchartReferences();
        }

        public virtual Flowchart[] GetCachedFlowchartReferences()
        {
            if (m_flowchartReferences == null)
            {
                return m_flowchartReferences = GetFlowchartReferences();
            }

            return m_flowchartReferences;
        }

        public virtual Flowchart[] GetFlowchartReferences()
        {
            List<Flowchart> flowchartReferences = new List<Flowchart>();
            this.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IfFlowchart,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetFlowchartReference(md, mp, hc, m, mt, d, p, dm, flowchartReferences),
                false);
            return flowchartReferences.ToArray();
        }

        private void GetFlowchartReference(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, List<Flowchart> flowchartReferences)
        {
            object memberValue = member.GetMemberValue(declaringObj, propertyParameters);
            Flowchart flowchart = memberValue as Flowchart;
            if (flowchart != null)
            {
                flowchartReferences.Add(flowchart);
            }
        }

        private bool IfFlowchart(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            if (typeof(Flowchart).IsAssignableFrom(memberType))
            {
                return true;
            }
            return false;
        }

        public virtual void CacheReturnValueCommandReferences()
        {
            m_returnValueCommandReferences = GetReturnValueCommandReferences();
        }

        public virtual ReturnValueCommand[] GetCachedReturnValueCommandReferences()
        {
            if (m_returnValueCommandReferences == null)
            {
                return m_returnValueCommandReferences = GetReturnValueCommandReferences();
            }

            return m_returnValueCommandReferences;
        }

        public virtual ReturnValueCommand[] GetReturnValueCommandReferences()
        {
            List<ReturnValueCommand> returnValueCommandReferences = new List<ReturnValueCommand>();
            this.PerformActionOnAllUnitySerializedFieldsThatMeetCondition(
                IfReturnValueCommand,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetReturnValueCommandReference(md, mp, hc, m, mt, d, p, dm, returnValueCommandReferences),
                true);
            return returnValueCommandReferences.ToArray();
        }

        private void GetReturnValueCommandReference(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, List<ReturnValueCommand> returnValueCommandReferences)
        {
            object memberValue = member.GetMemberValue(declaringObj, propertyParameters);
            ReturnValueCommand returnValueCommand = memberValue as ReturnValueCommand;
            if (returnValueCommand != null)
            {
                returnValueCommandReferences.Add(returnValueCommand);
                returnValueCommandReferences.AddRange(returnValueCommand.GetReturnValueCommandReferences());
            }
        }

        private bool IfReturnValueCommand(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember)
        {
            if (typeof(ReturnValueCommand).IsAssignableFrom(memberType) && member.Name != nameof(m_ParentFlowchartItem))
            {
                return true;
            }
            return false;
        }

#if UNITY_EDITOR
        public virtual void UpdateVariableNameReferences(string oldName, string newName)
        {
            foreach (FieldInfo field in GetType().GetUnitySerializedFields())
            {
                if (typeof(StringData).IsAssignableFrom(field.FieldType))
                {
                    StringData stringData = field.GetValue(this) as StringData;
                    string newDataValue = UpdateVariableNameReference((string)stringData.DataValue, oldName, newName);
                    if ((string)stringData.DataValue != newDataValue)
                    {
                        UnityEditor.Undo.RecordObject(this, "Update Variable Names");
                        stringData.DataValue = newDataValue;
                        RecordChanges();
                    }
                }
            }
        }

        private string UpdateVariableNameReference(string text, string oldName, string newName)
        {
            return text.Replace(GetVariableTag(oldName), GetVariableTag(newName));
        }

        public virtual string GetVariableTag(string variableName)
        {
            return "{$" + variableName + "}";
        }
#endif
        
#if UNITY_EDITOR
        public virtual void DestroyReferences(bool allowUndo)
        {
            foreach (ReturnValueCommand rvc in GetReturnValueCommandReferences())
            {
                rvc.DestroyReferences(allowUndo);
                if (allowUndo)
                {
                    UnityEditor.Undo.DestroyObjectImmediate(rvc);
                }
                else
                {
                    DestroyImmediate(rvc);
                }
            }
        }
#endif

        public virtual void Setup(FlowchartItem flowchartItem)
        {
            m_ParentFlowchartItem = flowchartItem;
            Setup();
        }

        public virtual void Setup()
        {
            m_ItemId = GetFlowchart().NextItemId();
            RegenerateKey();
            Validate();
        }
        
        public FlowchartItem GetRootParentFlowchartItem()
        {
            if (m_ParentFlowchartItem == null)
            {
                return this;
            }

            return m_ParentFlowchartItem.GetRootParentFlowchartItem();
        }

        public virtual bool UpdateHideFlags()
        {
            if (GetFlowchart().HideComponents)
            {
                if (hideFlags != HideFlags.HideInInspector)
                {
                    hideFlags = HideFlags.HideInInspector;
                    return true;
                }
            }
            else
            {
                if (hideFlags != HideFlags.None)
                {
                    hideFlags = HideFlags.None;
                    return true;
                }
            }

            return false;
        }

        public virtual string GetSummary()
        {
            return "";
        }
        
        private string GetVariableDataError(VariableData vd)
        {
            if (vd.VariableDataType == VariableDataType.Command)
            {
                ReturnValueCommand variableDataCommand = vd.Command;

                if (variableDataCommand == null)
                {
                    return "No Command selected";
                }
                else
                {
                    foreach (FieldInfo variableDataCommandField in variableDataCommand.GetType().GetUnitySerializedFields())
                    {
                        string error = variableDataCommand.GetPropertyError(variableDataCommandField.Name);
                        if (error != null)
                        {
                            Type variableDataCommandType = variableDataCommand.GetType();
                            return "<b>[" + FlowchartItemInfoAttribute.GetInfo(variableDataCommandType).GetName(variableDataCommandType) + "]</b> " + error;
                        }
                    }
                }
            }
            else if (vd.VariableDataType == VariableDataType.Variable)
            {
                if (vd.Variable == null)
                {
                    return "No Variable selected";
                }
            }

            return null;
        }

        protected override string[] GetFieldErrors(FieldInfo field)
        {
            CheckNotNullDataAttribute checkNotNullDataAttribute = field.GetAttribute<CheckNotNullDataAttribute>(false);
            CheckNotEmptyDataAttribute checkNotEmptyDataAttribute = field.GetAttribute<CheckNotEmptyDataAttribute>(false);

            List<string> errorMessages = new List<string>();

            errorMessages.AddRange(base.GetFieldErrors(field));

            if (checkNotNullDataAttribute != null)
            {
                string errorMessage = checkNotNullDataAttribute.GetError(field, this);
                if (errorMessage != null)
                {
                    errorMessages.Add(errorMessage);
                }
            }

            if (checkNotEmptyDataAttribute != null)
            {
                string errorMessage = checkNotEmptyDataAttribute.GetError(field, this);
                if (errorMessage != null)
                {
                    errorMessages.Add(errorMessage);
                }
            }

            return errorMessages.ToArray();
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            FieldInfo field = GetType().GetField(propertyPath, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (field != null)
            {
                string error = base.GetPropertyError(propertyPath);
                if (error != null)
                {
                    return error;
                }

                object value = field.GetValue(this);

                VariableData variableData = value as VariableData;
                if (variableData != null)
                {
                    error = GetVariableDataError(variableData);
                    if (error != null)
                    {
                        return error;
                    }
                }
            }
            return null;
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                if (GetFlowchart() != null)
                {
                    GetFlowchart().ValidateFlowchart();
                    GetFlowchart().InvokePropertyChanged(propertyPath);
                }
            }
#endif
        }

        public override void InvokePropertyChanged(string propertyPath)
        {
            CacheVariableReferences();
            CacheBlockReferences();
            CacheReturnValueCommandReferences();
            CacheFlowchartReferences();

            base.InvokePropertyChanged(propertyPath);
        }

        /// <summary>
        /// Return the color for the flowchart item header background in the inspector.
        /// </summary>
        /// <returns>The color that should be used to represent this flowchart item an inspector window</returns>
        public virtual Color GetHeaderColor()
        {
            string category = FlowchartItemInfoAttribute.GetInfo(GetType()).GetCategory(GetType());
            if (category != null)
            {
                if (FungusColorPalette.HasColor(category))
                {
                    return FungusColorPalette.GetColor(category);
                }

                int indexOfFirstSlash = category.IndexOf("/", StringComparison.Ordinal);
                if (indexOfFirstSlash >= 0)
                {
                    category = category.Remove(indexOfFirstSlash);
                }

                if (FungusColorPalette.HasColor(category))
                {
                    return FungusColorPalette.GetColor(category);
                }
            }
            return Color.white;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == "m_Script" || propertyPath == nameof(m_Key))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }

    }

}