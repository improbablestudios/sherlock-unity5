﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Call Mode List Variable")]
    public class CallModeListVariable : Variable<List<CallMode>>
    {
    }

    [Serializable]
    public class CallModeListData : VariableData<List<CallMode>>
    {
        public CallModeListData()
        {
            m_DataVal = new List<CallMode>();
        }

        public CallModeListData(List<CallMode> v)
        {
            m_DataVal = v;
        }
    }

}