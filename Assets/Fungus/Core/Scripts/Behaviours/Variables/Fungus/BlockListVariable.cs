﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Block List Variable")]
    public class BlockListVariable : Variable<List<Block>>
    {
    }

    [Serializable]
    public class BlockListData : VariableData<List<Block>>
    {
        public BlockListData()
        {
            m_DataVal = new List<Block>();
        }

        public BlockListData(List<Block> v)
        {
            m_DataVal = v;
        }
    }

}
