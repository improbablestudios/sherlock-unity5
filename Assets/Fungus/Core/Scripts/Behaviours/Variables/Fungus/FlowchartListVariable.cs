﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Flowchart List Variable")]
    public class FlowchartListVariable : Variable<List<Flowchart>>
    {
    }

    [Serializable]
    public class FlowchartListData : VariableData<List<Flowchart>>
    {
        public FlowchartListData()
        {
            m_DataVal = new List<Flowchart>();
        }

        public FlowchartListData(List<Flowchart> v)
        {
            m_DataVal = v;
        }
    }

}
