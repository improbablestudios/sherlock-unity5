﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Block Variable")]
    public class BlockVariable : Variable<Block>
    {
    }

    [Serializable]
    public class BlockData : VariableData<Block>
    {
        public BlockData()
        {
        }

        public BlockData(Block v)
        {
            m_DataVal = v;
        }
    }
    
}
