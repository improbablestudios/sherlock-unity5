﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Call Mode Variable")]
    public class CallModeVariable : Variable<CallMode>
    {
    }

    [Serializable]
    public class CallModeData : VariableData<CallMode>
    {
        public CallModeData()
        {
        }

        public CallModeData(CallMode v)
        {
            m_DataVal = v;
        }
    }
    
}