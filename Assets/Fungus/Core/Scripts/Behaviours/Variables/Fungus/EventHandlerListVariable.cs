﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Event Handler List Variable")]
    public class EventHandlerListVariable : Variable<List<EventHandler>>
    {
    }

    [Serializable]
    public class EventHandlerListData : VariableData<List<EventHandler>>
    {
        public EventHandlerListData()
        {
            m_DataVal = new List<EventHandler>();
        }

        public EventHandlerListData(List<EventHandler> v)
        {
            m_DataVal = v;
        }
    }

}
