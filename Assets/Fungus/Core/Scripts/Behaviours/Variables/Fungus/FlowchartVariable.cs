﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Flowchart Variable")]
    public class FlowchartVariable : Variable<Flowchart>
    {
    }

    [Serializable]
    public class FlowchartData : VariableData<Flowchart>
    {
        public FlowchartData()
        {
        }

        public FlowchartData(Flowchart v)
        {
            m_DataVal = v;
        }
    }
    
}
