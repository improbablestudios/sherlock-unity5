﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Event Handler Variable")]
    public class EventHandlerVariable : Variable<EventHandler>
    {
    }

    [Serializable]
    public class EventHandlerData : VariableData<EventHandler>
    {
        public EventHandlerData()
        {
        }

        public EventHandlerData(EventHandler v)
        {
            m_DataVal = v;
        }
    }

}
