﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Bounds List Variable")]
    public class BoundsListVariable : Variable<List<Bounds>>
    {
    }

    [Serializable]
    public class BoundsListData : VariableData<List<Bounds>>
    {
        public BoundsListData()
        {
            m_DataVal = new List<Bounds>();
        }

        public BoundsListData(List<Bounds> v)
        {
            m_DataVal = v;
        }
    }

}