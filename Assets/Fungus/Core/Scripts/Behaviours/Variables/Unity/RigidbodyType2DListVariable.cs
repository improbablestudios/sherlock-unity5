﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Transform List Variable")]
    public class RigidbodyType2DListVariable : Variable<List<RigidbodyType2D>>
    {
    }

    [Serializable]
    public class RigidbodyType2DListData : VariableData<List<RigidbodyType2D>>
    {
        public RigidbodyType2DListData()
        {
            m_DataVal = new List<RigidbodyType2D>();
        }

        public RigidbodyType2DListData(List<RigidbodyType2D> v)
        {
            m_DataVal = v;
        }
    }

}