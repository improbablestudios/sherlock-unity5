﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Space Variable")]
    public class SpaceVariable : Variable<Space>
    {
    }

    [Serializable]
    public class SpaceData : VariableData<Space>
    {
        public SpaceData()
        {
        }

        public SpaceData(Space v)
        {
            m_DataVal = v;
        }
    }

}