﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Runtime Platform List Variable")]
    public class RuntimePlatformListVariable : Variable<List<RuntimePlatform>>
    {
    }

    [Serializable]
    public class RuntimePlatformListData : VariableData<List<RuntimePlatform>>
    {
        public RuntimePlatformListData()
        {
            m_DataVal = new List<RuntimePlatform>();
        }

        public RuntimePlatformListData(List<RuntimePlatform> v)
        {
            m_DataVal = v;
        }
    }

}