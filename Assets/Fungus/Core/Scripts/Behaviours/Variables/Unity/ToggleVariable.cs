﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Toggle Variable")]
    public class ToggleVariable : Variable<Toggle>
    {
    }

    [Serializable]
    public class ToggleData : VariableData<Toggle>
    {
        public ToggleData()
        {
        }

        public ToggleData(Toggle v)
        {
            m_DataVal = v;
        }
    }

}