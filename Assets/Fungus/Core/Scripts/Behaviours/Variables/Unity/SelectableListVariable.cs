﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Selectable List Variable")]
    public class SelectableListVariable : Variable<List<Selectable>>
    {
    }

    [Serializable]
    public class SelectableListData : VariableData<List<Selectable>>
    {
        public SelectableListData()
        {
            m_DataVal = new List<Selectable>();
        }

        public SelectableListData(List<Selectable> v)
        {
            m_DataVal = v;
        }
    }

}
