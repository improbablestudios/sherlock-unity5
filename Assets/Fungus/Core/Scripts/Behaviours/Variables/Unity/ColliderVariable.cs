﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Collider Variable")]
    public class ColliderVariable : Variable<Collider>
    {
    }

    [Serializable]
    public class ColliderData : VariableData<Collider>
    {
        public ColliderData()
        {
        }

        public ColliderData(Collider v)
        {
            m_DataVal = v;
        }
    }

}