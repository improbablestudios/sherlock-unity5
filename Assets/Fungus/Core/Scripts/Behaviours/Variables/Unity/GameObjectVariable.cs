using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Game Object Variable")]
    public class GameObjectVariable : Variable<GameObject>
    {
    }

    [Serializable]
    public class GameObjectData : VariableData<GameObject>
    {
        public GameObjectData()
        {
        }

        public GameObjectData(GameObject v)
        {
            m_DataVal = v;
        }
    }

}