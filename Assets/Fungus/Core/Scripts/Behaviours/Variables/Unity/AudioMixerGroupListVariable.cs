﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Audio Mixer Group List Variable")]
    public class AudioMixerGroupListVariable : Variable<List<AudioMixerGroup>>
    {
    }

    [Serializable]
    public class AudioMixerGroupListData : VariableData<List<AudioMixerGroup>>
    {
        public AudioMixerGroupListData()
        {
            m_DataVal = new List<AudioMixerGroup>();
        }

        public AudioMixerGroupListData(List<AudioMixerGroup> v)
        {
            m_DataVal = v;
        }
    }

}
