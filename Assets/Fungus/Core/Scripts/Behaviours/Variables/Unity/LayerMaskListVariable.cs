﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Layer Mask List Variable")]
    public class LayerMaskListVariable : Variable<List<LayerMask>>
    {
    }

    [Serializable]
    public class LayerMaskListData : VariableData<List<LayerMask>>
    {
        public LayerMaskListData()
        {
            m_DataVal = new List<LayerMask>();
        }

        public LayerMaskListData(List<LayerMask> v)
        {
            m_DataVal = v;
        }
    }

}