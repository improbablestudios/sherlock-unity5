﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Audio Mixer Group Variable")]
    public class AudioMixerGroupVariable : Variable<AudioMixerGroup>
    {
    }

    [Serializable]
    public class AudioMixerGroupData : VariableData<AudioMixerGroup>
    {
        public AudioMixerGroupData()
        {
        }

        public AudioMixerGroupData(AudioMixerGroup v)
        {
            m_DataVal = v;
        }
    }

}