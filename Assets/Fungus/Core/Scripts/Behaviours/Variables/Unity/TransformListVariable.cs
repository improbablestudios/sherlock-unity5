﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Transform List Variable")]
    public class TransformListVariable : Variable<List<Transform>>
    {
    }

    [Serializable]
    public class TransformListData : VariableData<List<Transform>>
    {
        public TransformListData()
        {
            m_DataVal = new List<Transform>();
        }

        public TransformListData(List<Transform> v)
        {
            m_DataVal = v;
        }
    }

}
