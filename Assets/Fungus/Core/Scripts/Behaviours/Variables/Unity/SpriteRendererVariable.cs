﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Sprite Renderer Variable")]
    public class SpriteRendererVariable : Variable<SpriteRenderer>
    {
    }

    [Serializable]
    public class SpriteRendererData : VariableData<SpriteRenderer>
    {
        public SpriteRendererData()
        {
        }

        public SpriteRendererData(SpriteRenderer v)
        {
            m_DataVal = v;
        }
    }

}