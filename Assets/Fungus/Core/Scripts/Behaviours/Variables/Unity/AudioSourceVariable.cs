﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Audio Source Variable")]
    public class AudioSourceVariable : Variable<AudioSource>
    {
    }

    [Serializable]
    public class AudioSourceData : VariableData<AudioSource>
    {
        public AudioSourceData()
        {
        }

        public AudioSourceData(AudioSource v)
        {
            m_DataVal = v;
        }
    }

}
