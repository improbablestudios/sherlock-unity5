﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Animation Curve Variable")]
    public class AnimationCurveVariable : Variable<AnimationCurve>
    {
    }

    [Serializable]
    public class AnimationCurveData : VariableData<AnimationCurve>
    {
        public AnimationCurveData()
        {
        }

        public AnimationCurveData(AnimationCurve v)
        {
            m_DataVal = v;
        }
    }

}