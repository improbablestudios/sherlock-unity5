using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Vector2 List Variable")]
    public class Vector2ListVariable : Variable<List<Vector2>>
    {
    }

    [Serializable]
    public class Vector2ListData : VariableData<List<Vector2>>
    {
        public Vector2ListData()
        {
            m_DataVal = new List<Vector2>();
        }

        public Vector2ListData(List<Vector2> v)
        {
            m_DataVal = v;
        }
    }

}