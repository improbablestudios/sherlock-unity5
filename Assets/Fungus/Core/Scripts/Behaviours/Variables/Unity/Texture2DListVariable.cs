﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Texture 2D List Variable")]
    public class Texture2DListVariable : Variable<List<Texture2D>>
    {
    }

    [Serializable]
    public class Texture2DListData : VariableData<List<Texture2D>>
    {
        public Texture2DListData()
        {
            m_DataVal = new List<Texture2D>();
        }

        public Texture2DListData(List<Texture2D> v)
        {
            m_DataVal = v;
        }
    }

}