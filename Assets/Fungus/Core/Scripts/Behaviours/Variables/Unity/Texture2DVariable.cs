﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Texture 2D Variable")]
    public class Texture2DVariable : Variable<Texture2D>
    {
    }

    [Serializable]
    public class Texture2DData : VariableData<Texture2D>
    {
        public Texture2DData()
        {
        }

        public Texture2DData(Texture2D v)
        {
            m_DataVal = v;
        }
    }

}