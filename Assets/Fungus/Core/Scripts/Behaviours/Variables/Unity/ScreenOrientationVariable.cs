﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Screen Orientation Variable")]
    public class ScreenOrientationVariable : Variable<ScreenOrientation>
    {
    }

    [Serializable]
    public class ScreenOrientationData : VariableData<ScreenOrientation>
    {
        public ScreenOrientationData()
        {
        }

        public ScreenOrientationData(ScreenOrientation v)
        {
            m_DataVal = v;
        }
    }

}
