﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Component List Variable")]
    public class ComponentListVariable : Variable<List<Component>>
    {
    }

    [Serializable]
    public class ComponentListData : VariableData<List<Component>>
    {
        public ComponentListData()
        {
            m_DataVal = new List<Component>();
        }

        public ComponentListData(List<Component> v)
        {
            m_DataVal = v;
        }
    }

}