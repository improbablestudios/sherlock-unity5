﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Screen/Fullscreen Mode Variable")]
    public class FullScreenModeVariable : Variable<FullScreenMode>
    {
    }

    [Serializable]
    public class FullScreenModeData : VariableData<FullScreenMode>
    {
        public FullScreenModeData()
        {
        }

        public FullScreenModeData(FullScreenMode v)
        {
            m_DataVal = v;
        }
    }

}