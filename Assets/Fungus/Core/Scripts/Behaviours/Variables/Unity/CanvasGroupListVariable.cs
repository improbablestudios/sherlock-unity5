﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Canvas Group List Variable")]
    public class CanvasGroupListVariable : Variable<List<CanvasGroup>>
    {
    }

    [Serializable]
    public class CanvasGroupListData : VariableData<List<CanvasGroup>>
    {
        public CanvasGroupListData()
        {
            m_DataVal = new List<CanvasGroup>();
        }

        public CanvasGroupListData(List<CanvasGroup> v)
        {
            m_DataVal = v;
        }
    }

}
