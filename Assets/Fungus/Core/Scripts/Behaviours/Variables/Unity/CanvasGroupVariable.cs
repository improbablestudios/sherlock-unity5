﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Canvas Group Variable")]
    public class CanvasGroupVariable : Variable<CanvasGroup>
    {
    }

    [Serializable]
    public class CanvasGroupData : VariableData<CanvasGroup>
    {
        public CanvasGroupData()
        {
        }

        public CanvasGroupData(CanvasGroup v)
        {
            m_DataVal = v;
        }
    }

}
