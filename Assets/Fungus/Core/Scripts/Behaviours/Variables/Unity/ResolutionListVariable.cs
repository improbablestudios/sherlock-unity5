﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Resolution List Variable")]
    public class ResolutionListVariable : Variable<List<Resolution>>
    {
    }

    [Serializable]
    public class ResolutionListData : VariableData<List<Resolution>>
    {
        public ResolutionListData()
        {
            m_DataVal = new List<Resolution>();
        }

        public ResolutionListData(List<Resolution> v)
        {
            m_DataVal = v;
        }
    }

}