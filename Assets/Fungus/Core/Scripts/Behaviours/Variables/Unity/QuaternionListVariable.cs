﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Quaternion List Variable")]
    public class QuaternionListVariable : Variable<List<Quaternion>>
    {
    }

    [Serializable]
    public class QuaternionListData : VariableData<List<Quaternion>>
    {
        public QuaternionListData()
        {
            m_DataVal = new List<Quaternion>();
        }

        public QuaternionListData(List<Quaternion> v)
        {
            m_DataVal = v;
        }
    }

}
