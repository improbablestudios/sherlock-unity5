﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Screen/Fullscreen Mode List Variable")]
    public class FullScreenModeListVariable : Variable<List<FullScreenMode>>
    {
    }

    [Serializable]
    public class FullScreenModeListData : VariableData<List<FullScreenMode>>
    {
        public FullScreenModeListData()
        {
            m_DataVal = new List<FullScreenMode>();
        }

        public FullScreenModeListData(List<FullScreenMode> v)
        {
            m_DataVal = v;
        }
    }

}