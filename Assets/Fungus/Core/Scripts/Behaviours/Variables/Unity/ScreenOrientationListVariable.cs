﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Screen Orientation List Variable")]
    public class ScreenOrientationListVariable : Variable<List<ScreenOrientation>>
    {
    }

    [Serializable]
    public class ScreenOrientationListData : VariableData<List<ScreenOrientation>>
    {
        public ScreenOrientationListData()
        {
            m_DataVal = new List<ScreenOrientation>();
        }

        public ScreenOrientationListData(List<ScreenOrientation> v)
        {
            m_DataVal = v;
        }
    }

}