﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Button Variable")]
    public class ButtonVariable : Variable<Button>
    {
    }

    [Serializable]
    public class ButtonData : VariableData<Button>
    {
        public ButtonData()
        {
        }

        public ButtonData(Button v)
        {
            m_DataVal = v;
        }
    }

}