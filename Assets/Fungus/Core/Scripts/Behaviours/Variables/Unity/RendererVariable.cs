﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Renderer Variable")]
    public class RendererVariable : Variable<Renderer>
    {
    }

    [Serializable]
    public class RendererData : VariableData<Renderer>
    {
        public RendererData()
        {
        }

        public RendererData(Renderer v)
        {
            m_DataVal = v;
        }
    }

}