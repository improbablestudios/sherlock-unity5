﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Rect Variable")]
    public class RectVariable : Variable<Rect>
    {
    }

    [Serializable]
    public class RectData : VariableData<Rect>
    {
        public RectData()
        {
            m_DataVal = new Rect(0, 0, 0, 0);
        }

        public RectData(Rect v)
        {
            m_DataVal = v;
        }
    }

}
