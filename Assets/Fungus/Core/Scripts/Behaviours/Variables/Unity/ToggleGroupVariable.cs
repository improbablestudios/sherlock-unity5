﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Toggle Group Variable")]
    public class ToggleGroupVariable : Variable<ToggleGroup>
    {
    }

    [Serializable]
    public class ToggleGroupData : VariableData<ToggleGroup>
    {
        public ToggleGroupData()
        {
        }

        public ToggleGroupData(ToggleGroup v)
        {
            m_DataVal = v;
        }
    }

}