﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/InputField/Input Field Content Type List Variable")]
    public class InputFieldContentTypeListVariable : Variable<List<InputField.ContentType>>
    {
    }

    [Serializable]
    public class InputFieldContentTypeListData : VariableData<List<InputField.ContentType>>
    {
        public InputFieldContentTypeListData()
        {
            m_DataVal = new List<InputField.ContentType>();
        }

        public InputFieldContentTypeListData(List<InputField.ContentType> v)
        {
            m_DataVal = v;
        }
    }

}