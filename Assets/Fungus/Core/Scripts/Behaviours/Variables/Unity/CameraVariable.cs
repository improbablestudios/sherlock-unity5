﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Camera Variable")]
    public class CameraVariable : Variable<Camera>
    {
    }

    [Serializable]
    public class CameraData : VariableData<Camera>
    {
        public CameraData()
        {
        }

        public CameraData(Camera v)
        {
            m_DataVal = v;
        }
    }

}
