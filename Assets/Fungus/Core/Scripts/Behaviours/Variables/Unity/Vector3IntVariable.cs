using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Vector3Int Variable")]
    public class Vector3IntVariable : Variable<Vector3Int>
    {
    }

    [Serializable]
    public class Vector3IntData : VariableData<Vector3Int>
    {
        public Vector3IntData()
        {
            m_DataVal = Vector3Int.zero;
        }

        public Vector3IntData(Vector3Int v)
        {
            m_DataVal = v;
        }
    }

}