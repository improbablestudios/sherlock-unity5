using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Vector3Int List Variable")]
    public class Vector3IntListVariable : Variable<List<Vector3Int>>
    {
    }

    [Serializable]
    public class Vector3IntListData : VariableData<List<Vector3Int>>
    {
        public Vector3IntListData()
        {
            m_DataVal = new List<Vector3Int>();
        }

        public Vector3IntListData(List<Vector3Int> v)
        {
            m_DataVal = v;
        }
    }

}