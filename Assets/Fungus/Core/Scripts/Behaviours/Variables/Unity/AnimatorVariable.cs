﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Animator Variable")]
    public class AnimatorVariable : Variable<Animator>
    {
    }

    [Serializable]
    public class AnimatorData : VariableData<Animator>
    {
        public AnimatorData()
        {
        }

        public AnimatorData(Animator v)
        {
            m_DataVal = v;
        }
    }

}
