﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Collider 2D Variable")]
    public class Collider2DVariable : Variable<Collider2D>
    {
    }

    [Serializable]
    public class Collider2DData : VariableData<Collider2D>
    {
        public Collider2DData()
        {
        }

        public Collider2DData(Collider2D v)
        {
            m_DataVal = v;
        }
    }

}