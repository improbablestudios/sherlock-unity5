﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Text Asset Variable")]
    public class TextAssetVariable : Variable<TextAsset>
    {
    }

    [Serializable]
    public class TextAssetData : VariableData<TextAsset>
    {
        public TextAssetData()
        {
        }

        public TextAssetData(TextAsset v)
        {
            m_DataVal = v;
        }
    }

}