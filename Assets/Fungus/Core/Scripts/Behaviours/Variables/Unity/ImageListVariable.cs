﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Image List Variable")]
    public class ImageListVariable : Variable<List<Image>>
    {
    }

    [Serializable]
    public class ImageListData : VariableData<List<Image>>
    {
        public ImageListData()
        {
            m_DataVal = new List<Image>();
        }

        public ImageListData(List<Image> v)
        {
            m_DataVal = v;
        }
    }

}