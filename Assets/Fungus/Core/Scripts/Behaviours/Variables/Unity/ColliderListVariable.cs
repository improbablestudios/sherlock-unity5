﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Collider List Variable")]
    public class ColliderListVariable : Variable<List<Collider>>
    {
    }

    [Serializable]
    public class ColliderListData : VariableData<List<Collider>>
    {
        public ColliderListData()
        {
            m_DataVal = new List<Collider>();
        }

        public ColliderListData(List<Collider> v)
        {
            m_DataVal = v;
        }
    }

}