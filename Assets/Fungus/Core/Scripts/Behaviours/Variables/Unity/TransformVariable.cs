﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Transform Variable")]
    public class TransformVariable : Variable<Transform>
    {
    }

    [Serializable]
    public class TransformData : VariableData<Transform>
    {
        public TransformData()
        {
        }

        public TransformData(Transform v)
        {
            m_DataVal = v;
        }
    }

}
