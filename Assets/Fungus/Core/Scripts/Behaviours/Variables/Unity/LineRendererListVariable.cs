﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/LineRenderer List Variable")]
    public class LineRendererListVariable : Variable<List<LineRenderer>>
    {
    }

    [Serializable]
    public class LineRendererListData : VariableData<List<LineRenderer>>
    {
        public LineRendererListData()
        {
            m_DataVal = new List<LineRenderer>();
        }

        public LineRendererListData(List<LineRenderer> v)
        {
            m_DataVal = v;
        }
    }

}
