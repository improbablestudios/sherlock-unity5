﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Graphic List Variable")]
    public class GraphicListVariable : Variable<List<Graphic>>
    {
    }

    [Serializable]
    public class GraphicListData : VariableData<List<Graphic>>
    {
        public GraphicListData()
        {
            m_DataVal = new List<Graphic>();
        }

        public GraphicListData(List<Graphic> v)
        {
            m_DataVal = v;
        }
    }

}