using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Vector4 List Variable")]
    public class Vector4ListVariable : Variable<List<Vector4>>
    {
    }

    [Serializable]
    public class Vector4ListData : VariableData<List<Vector4>>
    {
        public Vector4ListData()
        {
            m_DataVal = new List<Vector4>();
        }

        public Vector4ListData(List<Vector4> v)
        {
            m_DataVal = v;
        }
    }

}