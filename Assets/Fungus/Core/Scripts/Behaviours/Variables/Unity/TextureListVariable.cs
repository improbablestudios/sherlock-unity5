using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Texture List Variable")]
    public class TextureListVariable : Variable<List<Texture>>
    {
    }

    [Serializable]
    public class TextureListData : VariableData<List<Texture>>
    {
        public TextureListData()
        {
            m_DataVal = new List<Texture>();
        }

        public TextureListData(List<Texture> v)
        {
            m_DataVal = v;
        }
    }

}