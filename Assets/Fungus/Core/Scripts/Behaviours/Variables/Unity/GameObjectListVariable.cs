using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Game Object List Variable")]
    public class GameObjectListVariable : Variable<List<GameObject>>
    {
    }

    [Serializable]
    public class GameObjectListData : VariableData<List<GameObject>>
    {
        public GameObjectListData()
        {
            m_DataVal = new List<GameObject>();
        }

        public GameObjectListData(List<GameObject> v)
        {
            m_DataVal = v;
        }
    }

}