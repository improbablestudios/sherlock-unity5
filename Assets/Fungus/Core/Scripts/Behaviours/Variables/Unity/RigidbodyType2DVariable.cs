﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Rigidbody Type 2D Variable")]
    public class RigidbodyType2DVariable : Variable<RigidbodyType2D>
    {
    }

    [Serializable]
    public class RigidbodyType2DData : VariableData<RigidbodyType2D>
    {
        public RigidbodyType2DData()
        {
        }

        public RigidbodyType2DData(RigidbodyType2D v)
        {
            m_DataVal = v;
        }
    }

}