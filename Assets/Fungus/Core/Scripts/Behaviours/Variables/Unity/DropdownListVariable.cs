﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Dropdown List Variable")]
    public class DropdownListVariable : Variable<List<Dropdown>>
    {
    }

    [Serializable]
    public class DropdownListData : VariableData<List<Dropdown>>
    {
        public DropdownListData()
        {
            m_DataVal = new List<Dropdown>();
        }

        public DropdownListData(List<Dropdown> v)
        {
            m_DataVal = v;
        }
    }

}
