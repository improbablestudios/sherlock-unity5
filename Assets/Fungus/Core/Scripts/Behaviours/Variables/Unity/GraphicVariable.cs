﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Graphic Variable")]
    public class GraphicVariable : Variable<Graphic>
    {
    }

    [Serializable]
    public class GraphicData : VariableData<Graphic>
    {
        public GraphicData()
        {
        }

        public GraphicData(Graphic v)
        {
            m_DataVal = v;
        }
    }

}