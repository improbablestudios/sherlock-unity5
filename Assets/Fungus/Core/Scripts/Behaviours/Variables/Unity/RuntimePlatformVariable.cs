﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Runtime Platform Variable")]
    public class RuntimePlatformVariable : Variable<RuntimePlatform>
    {
    }

    [Serializable]
    public class RuntimePlatformData : VariableData<RuntimePlatform>
    {
        public RuntimePlatformData()
        {
        }

        public RuntimePlatformData(RuntimePlatform v)
        {
            m_DataVal = v;
        }
    }

}