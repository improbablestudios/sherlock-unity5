using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Vector2Int Variable")]
    public class Vector2IntVariable : Variable<Vector2Int>
    {
    }

    [Serializable]
    public class Vector2IntData : VariableData<Vector2Int>
    {
        public Vector2IntData()
        {
            m_DataVal = Vector2Int.zero;
        }

        public Vector2IntData(Vector2Int v)
        {
            m_DataVal = v;
        }
    }

}