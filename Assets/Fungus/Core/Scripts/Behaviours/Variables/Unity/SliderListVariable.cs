﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Slider List Variable")]
    public class SliderListVariable : Variable<List<Slider>>
    {
    }

    [Serializable]
    public class SliderListData : VariableData<List<Slider>>
    {
        public SliderListData()
        {
            m_DataVal = new List<Slider>();
        }

        public SliderListData(List<Slider> v)
        {
            m_DataVal = v;
        }
    }

}
