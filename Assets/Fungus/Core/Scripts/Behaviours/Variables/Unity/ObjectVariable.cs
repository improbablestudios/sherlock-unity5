using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Object Variable")]
    public class ObjectVariable : Variable<UnityEngine.Object>
    {
    }

    [Serializable]
    public class ObjectData : VariableData<UnityEngine.Object>
    {
        public ObjectData()
        {
        }

        public ObjectData(UnityEngine.Object v)
        {
            m_DataVal = v;
        }
    }

}