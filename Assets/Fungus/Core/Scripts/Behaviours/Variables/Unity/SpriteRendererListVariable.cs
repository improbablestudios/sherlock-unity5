﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Sprite Renderer List Variable")]
    public class SpriteRendererListVariable : Variable<List<SpriteRenderer>>
    {
    }

    [Serializable]
    public class SpriteRendererListData : VariableData<List<SpriteRenderer>>
    {
        public SpriteRendererListData()
        {
            m_DataVal = new List<SpriteRenderer>();
        }

        public SpriteRendererListData(List<SpriteRenderer> v)
        {
            m_DataVal = v;
        }
    }

}