using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Object List Variable")]
    public class ObjectListVariable : Variable<List<UnityEngine.Object>>
    {
    }

    [Serializable]
    public class ObjectListData : VariableData<List<UnityEngine.Object>>
    {
        public ObjectListData()
        {
            m_DataVal = new List<UnityEngine.Object>();
        }

        public ObjectListData(List<UnityEngine.Object> v)
        {
            m_DataVal = v;
        }
    }

}