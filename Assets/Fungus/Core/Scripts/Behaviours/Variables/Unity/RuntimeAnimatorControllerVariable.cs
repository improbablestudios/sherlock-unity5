﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Runtime Animator Controller Variable")]
    public class RuntimeAnimatorControllerVariable : Variable<RuntimeAnimatorController>
    {
    }

    [Serializable]
    public class RuntimeAnimatorControllerData : VariableData<RuntimeAnimatorController>
    {
        public RuntimeAnimatorControllerData()
        {
        }

        public RuntimeAnimatorControllerData(RuntimeAnimatorController v)
        {
            m_DataVal = v;
        }
    }

}
