﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Rect Transform Variable")]
    public class RectTransformVariable : Variable<RectTransform>
    {
    }
    
    [Serializable]
    public class RectTransformData : VariableData<RectTransform>
    {
        public RectTransformData()
        {
        }

        public RectTransformData(RectTransform v)
        {
            m_DataVal = v;
        }
    }

}
