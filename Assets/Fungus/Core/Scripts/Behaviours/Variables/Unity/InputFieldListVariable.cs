﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Input Field List Variable")]
    public class InputFieldListVariable : Variable<List<InputField>>
    {
    }

    [Serializable]
    public class InputFieldListData : VariableData<List<InputField>>
    {
        public InputFieldListData()
        {
            m_DataVal = new List<InputField>();
        }

        public InputFieldListData(List<InputField> v)
        {
            m_DataVal = v;
        }
    }

}