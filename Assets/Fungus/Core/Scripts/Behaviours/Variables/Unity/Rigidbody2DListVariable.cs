﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Rigidbody 2D List Variable")]
    public class Rigidbody2DListVariable : Variable<List<Rigidbody2D>>
    {
    }

    [Serializable]
    public class Rigidbody2DListData : VariableData<List<Rigidbody2D>>
    {
        public Rigidbody2DListData()
        {
            m_DataVal = new List<Rigidbody2D>();
        }

        public Rigidbody2DListData(List<Rigidbody2D> v)
        {
            m_DataVal = v;
        }
    }

}