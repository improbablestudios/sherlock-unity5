﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Button List Variable")]
    public class ButtonListVariable : Variable<List<Button>>
    {
    }

    [Serializable]
    public class ButtonListData : VariableData<List<Button>>
    {
        public ButtonListData()
        {
            m_DataVal = new List<Button>();
        }

        public ButtonListData(List<Button> v)
        {
            m_DataVal = v;
        }
    }

}