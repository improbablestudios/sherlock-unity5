using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Color List Variable")]
    public class ColorListVariable : Variable<List<Color>>
    {
    }

    [Serializable]
    public class ColorListData : VariableData<List<Color>>
    {
        public ColorListData()
        {
            m_DataVal = new List<Color>();
        }

        public ColorListData(List<Color> v)
        {
            m_DataVal = v;
        }
    }

}