﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Key Code List Variable")]
    public class KeyCodeListVariable : Variable<List<KeyCode>>
    {
    }

    [Serializable]
    public class KeyCodeListData : VariableData<List<KeyCode>>
    {
        public KeyCodeListData()
        {
            m_DataVal = new List<KeyCode>();
        }

        public KeyCodeListData(List<KeyCode> v)
        {
            m_DataVal = v;
        }
    }

}