﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Canvas Variable")]
    public class CanvasVariable : Variable<Canvas>
    {
    }

    [Serializable]
    public class CanvasData : VariableData<Canvas>
    {
        public CanvasData()
        {
        }

        public CanvasData(Canvas v)
        {
            m_DataVal = v;
        }
    }

}
