using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Material List Variable")]
    public class MaterialListVariable : Variable<List<Material>>
    {
    }

    [Serializable]
    public class MaterialListData : VariableData<List<Material>>
    {
        public MaterialListData()
        {
            m_DataVal = new List<Material>();
        }

        public MaterialListData(List<Material> v)
        {
            m_DataVal = v;
        }
    }

}