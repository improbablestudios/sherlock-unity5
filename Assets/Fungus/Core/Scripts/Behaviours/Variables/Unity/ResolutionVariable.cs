﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Resolution Variable")]
    public class ResolutionVariable : Variable<Resolution>
    {
    }

    [Serializable]
    public class ResolutionData : VariableData<Resolution>
    {
        public ResolutionData()
        {
        }

        public ResolutionData(Resolution v)
        {
            m_DataVal = v;
        }
    }

}