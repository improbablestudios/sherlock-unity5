﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Font Data Variable")]
    public class FontDataVariable : Variable<FontData>
    {
    }

    [Serializable]
    public class FontDataData : VariableData<FontData>
    {
        public FontDataData()
        {
            m_DataVal = FontData.defaultFontData;
        }

        public FontDataData(FontData v)
        {
            m_DataVal = v;
        }
    }

}
