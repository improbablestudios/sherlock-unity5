﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Text Variable")]
    public class TextVariable : Variable<Text>
    {
    }

    [Serializable]
    public class TextData : VariableData<Text>
    {
        public TextData()
        {
        }

        public TextData(Text v)
        {
            m_DataVal = v;
        }
    }

}