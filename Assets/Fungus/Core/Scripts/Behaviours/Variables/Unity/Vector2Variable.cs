using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Vector2 Variable")]
    public class Vector2Variable : Variable<Vector2>
    {
    }

    [Serializable]
    public class Vector2Data : VariableData<Vector2>
    {
        public Vector2Data()
        {
            m_DataVal = Vector2.zero;
        }

        public Vector2Data(Vector2 v)
        {
            m_DataVal = v;
        }
    }

}