using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Material Variable")]
    public class MaterialVariable : Variable<Material>
    {
    }

    [Serializable]
    public class MaterialData : VariableData<Material>
    {
        public MaterialData()
        {
        }

        public MaterialData(Material v)
        {
            m_DataVal = v;
        }
    }
    
}