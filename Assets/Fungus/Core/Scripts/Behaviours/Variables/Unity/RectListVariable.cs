﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Rect List Variable")]
    public class RectListVariable : Variable<List<Rect>>
    {
    }

    [Serializable]
    public class RectListData : VariableData<List<Rect>>
    {
        public RectListData()
        {
            m_DataVal = new List<Rect>();
        }

        public RectListData(List<Rect> v)
        {
            m_DataVal = v;
        }
    }

}
