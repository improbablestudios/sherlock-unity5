﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Dropdown Variable")]
    public class DropdownVariable : Variable<Dropdown>
    {
    }

    [Serializable]
    public class DropdownData : VariableData<Dropdown>
    {
        public DropdownData()
        {
        }

        public DropdownData(Dropdown v)
        {
            m_DataVal = v;
        }
    }
    
}
