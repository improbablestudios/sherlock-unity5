using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Vector4 Variable")]
    public class Vector4Variable : Variable<Vector4>
    {
    }

    [Serializable]
    public class Vector4Data : VariableData<Vector4>
    {
        public Vector4Data()
        {
            m_DataVal = Vector4.zero;
        }

        public Vector4Data(Vector4 v)
        {
            m_DataVal = v;
        }
    }

}