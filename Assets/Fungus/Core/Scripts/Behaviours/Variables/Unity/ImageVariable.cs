﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Image Variable")]
    public class ImageVariable : Variable<Image>
    {
    }

    [Serializable]
    public class ImageData : VariableData<Image>
    {
        public ImageData()
        {
        }

        public ImageData(Image v)
        {
            m_DataVal = v;
        }
    }

}