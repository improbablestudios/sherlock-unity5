﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Layer Mask Variable")]
    public class LayerMaskVariable : Variable<LayerMask>
    {
    }

    [Serializable]
    public class LayerMaskData : VariableData<LayerMask>
    {
        public LayerMaskData()
        {
        }

        public LayerMaskData(LayerMask v)
        {
            m_DataVal = v;
        }
    }

}