﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Gradient List Variable")]
    public class GradientListVariable : Variable<List<Gradient>>
    {
    }

    [Serializable]
    public class GradientListData : VariableData<List<Gradient>>
    {
        public GradientListData()
        {
            m_DataVal = new List<Gradient>();
        }

        public GradientListData(List<Gradient> v)
        {
            m_DataVal = v;
        }
    }

}