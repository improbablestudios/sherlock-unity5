﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/BoundsInt List Variable")]
    public class BoundsIntListVariable : Variable<List<BoundsInt>>
    {
    }

    [Serializable]
    public class BoundsIntListData : VariableData<List<BoundsInt>>
    {
        public BoundsIntListData()
        {
            m_DataVal = new List<BoundsInt>();
        }

        public BoundsIntListData(List<BoundsInt> v)
        {
            m_DataVal = v;
        }
    }

}