﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Collider 2D List Variable")]
    public class Collider2DListVariable : Variable<List<Collider2D>>
    {
    }

    [Serializable]
    public class Collider2DListData : VariableData<List<Collider2D>>
    {
        public Collider2DListData()
        {
            m_DataVal = new List<Collider2D>();
        }

        public Collider2DListData(List<Collider2D> v)
        {
            m_DataVal = v;
        }
    }

}