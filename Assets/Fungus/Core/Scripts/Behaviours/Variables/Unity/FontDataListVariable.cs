﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Font Data List Variable")]
    public class FontDataListVariable : Variable<List<FontData>>
    {
    }

    [Serializable]
    public class FontDataListData : VariableData<List<FontData>>
    {
        public FontDataListData()
        {
            m_DataVal = new List<FontData>();
        }

        public FontDataListData(List<FontData> v)
        {
            m_DataVal = v;
        }
    }

}
