﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Animation Curve List Variable")]
    public class AnimationCurveListVariable : Variable<List<AnimationCurve>>
    {
    }

    [Serializable]
    public class AnimationCurveListData : VariableData<List<AnimationCurve>>
    {
        public AnimationCurveListData()
        {
            m_DataVal = new List<AnimationCurve>();
        }

        public AnimationCurveListData(List<AnimationCurve> v)
        {
            m_DataVal = v;
        }
    }

}