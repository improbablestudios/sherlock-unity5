﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Toggle List Variable")]
    public class ToggleListVariable : Variable<List<Toggle>>
    {
    }

    [Serializable]
    public class ToggleListData : VariableData<List<Toggle>>
    {
        public ToggleListData()
        {
            m_DataVal = new List<Toggle>();
        }

        public ToggleListData(List<Toggle> v)
        {
            m_DataVal = v;
        }
    }

}