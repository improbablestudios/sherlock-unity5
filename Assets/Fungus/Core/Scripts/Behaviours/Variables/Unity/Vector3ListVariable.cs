using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Vector3 List Variable")]
    public class Vector3ListVariable : Variable<List<Vector3>>
    {
    }

    [Serializable]
    public class Vector3ListData : VariableData<List<Vector3>>
    {
        public Vector3ListData()
        {
            m_DataVal = new List<Vector3>();
        }

        public Vector3ListData(List<Vector3> v)
        {
            m_DataVal = v;
        }
    }

}