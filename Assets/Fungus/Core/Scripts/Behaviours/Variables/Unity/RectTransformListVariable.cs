﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Rect Transform List Variable")]
    public class RectTransformListVariable : Variable<List<RectTransform>>
    {
    }

    [Serializable]
    public class RectTransformListData : VariableData<List<RectTransform>>
    {
        public RectTransformListData()
        {
            m_DataVal = new List<RectTransform>();
        }

        public RectTransformListData(List<RectTransform> v)
        {
            m_DataVal = v;
        }
    }

}
