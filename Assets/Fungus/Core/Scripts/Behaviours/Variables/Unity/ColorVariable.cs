using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Color Variable")]
    public class ColorVariable : Variable<Color>
    {
    }

    [Serializable]
    public class ColorData : VariableData<Color>
    {
        public ColorData()
        {
            m_DataVal = Color.white;
        }

        public ColorData(Color v)
        {
            m_DataVal = v;
        }
    }
    
}