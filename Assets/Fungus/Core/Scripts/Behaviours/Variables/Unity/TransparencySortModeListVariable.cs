﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Transparency Sort Mode List Variable")]
    public class TransparencySortModeListVariable : Variable<List<TransparencySortMode>>
    {
    }

    [Serializable]
    public class TransparencySortModeListData : VariableData<List<TransparencySortMode>>
    {
        public TransparencySortModeListData()
        {
            m_DataVal = new List<TransparencySortMode>();
        }

        public TransparencySortModeListData(List<TransparencySortMode> v)
        {
            m_DataVal = v;
        }
    }

}