﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Bounds Variable")]
    public class BoundsVariable : Variable<Bounds>
    {
    }

    [Serializable]
    public class BoundsData : VariableData<Bounds>
    {
        public BoundsData()
        {
        }

        public BoundsData(Bounds v)
        {
            m_DataVal = v;
        }
    }

}