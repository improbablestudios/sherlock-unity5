﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Text List Variable")]
    public class TextListVariable : Variable<List<Text>>
    {
    }

    [Serializable]
    public class TextListData : VariableData<List<Text>>
    {
        public TextListData()
        {
            m_DataVal = new List<Text>();
        }

        public TextListData(List<Text> v)
        {
            m_DataVal = v;
        }
    }

}