﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Camera List Variable")]
    public class CameraListVariable : Variable<List<Camera>>
    {
    }

    [Serializable]
    public class CameraListData : VariableData<List<Camera>>
    {
        public CameraListData()
        {
            m_DataVal = new List<Camera>();
        }

        public CameraListData(List<Camera> v)
        {
            m_DataVal = v;
        }
    }

}
