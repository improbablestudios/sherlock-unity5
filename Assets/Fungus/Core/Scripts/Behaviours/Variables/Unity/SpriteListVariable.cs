using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Sprite List Variable")]
    public class SpriteListVariable : Variable<List<Sprite>>
    {
    }

    [Serializable]
    public class SpriteListData : VariableData<List<Sprite>>
    {
        public SpriteListData()
        {
            m_DataVal = new List<Sprite>();
        }

        public SpriteListData(List<Sprite> v)
        {
            m_DataVal = v;
        }
    }

}