using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Texture Variable")]
    public class TextureVariable : Variable<Texture>
    {
    }

    [Serializable]
    public class TextureData : VariableData<Texture>
    {
        public TextureData()
        {
        }

        public TextureData(Texture v)
        {
            m_DataVal = v;
        }
    }

}