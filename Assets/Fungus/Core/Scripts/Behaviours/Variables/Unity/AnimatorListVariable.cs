﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Animator List Variable")]
    public class AnimatorListVariable : Variable<List<Animator>>
    {
    }

    [Serializable]
    public class AnimatorListData : VariableData<List<Animator>>
    {
        public AnimatorListData()
        {
            m_DataVal = new List<Animator>();
        }

        public AnimatorListData(List<Animator> v)
        {
            m_DataVal = v;
        }
    }

}
