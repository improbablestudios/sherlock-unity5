﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Selectable Variable")]
    public class SelectableVariable : Variable<Selectable>
    {
    }

    [Serializable]
    public class SelectableData : VariableData<Selectable>
    {
        public SelectableData()
        {
        }

        public SelectableData(Selectable v)
        {
            m_DataVal = v;
        }
    }

}
