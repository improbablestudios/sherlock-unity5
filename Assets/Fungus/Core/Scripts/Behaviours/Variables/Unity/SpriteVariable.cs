using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Sprite Variable")]
    public class SpriteVariable : Variable<Sprite>
    {
    }
    
    [Serializable]
    public class SpriteData : VariableData<Sprite>
    {
        public SpriteData()
        {
        }

        public SpriteData(Sprite v)
        {
            m_DataVal = v;
        }
    }

}