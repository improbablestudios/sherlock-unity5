﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Input Field/Input Field Content Type Variable")]
    public class InputFieldContentTypeVariable : Variable<InputField.ContentType>
    {
    }

    [Serializable]
    public class InputFieldContentTypeData : VariableData<InputField.ContentType>
    {
        public InputFieldContentTypeData()
        {
        }

        public InputFieldContentTypeData(InputField.ContentType v)
        {
            m_DataVal = v;
        }
    }

}