﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Audio Source List Variable")]
    public class AudioSourceListVariable : Variable<List<AudioSource>>
    {
    }

    [Serializable]
    public class AudioSourceListData : VariableData<List<AudioSource>>
    {
        public AudioSourceListData()
        {
            m_DataVal = new List<AudioSource>();
        }

        public AudioSourceListData(List<AudioSource> v)
        {
            m_DataVal = v;
        }
    }

}
