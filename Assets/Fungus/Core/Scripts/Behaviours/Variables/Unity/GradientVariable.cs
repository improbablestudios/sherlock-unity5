﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Gradient Variable")]
    public class GradientVariable : Variable<Gradient>
    {
    }

    [Serializable]
    public class GradientData : VariableData<Gradient>
    {
        public GradientData()
        {
            m_DataVal = new Gradient();
        }

        public GradientData(Gradient v)
        {
            m_DataVal = v;
        }
    }

}
