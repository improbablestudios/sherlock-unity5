﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Audio Clip List Variable")]
    public class AudioClipListVariable : Variable<List<AudioClip>>
    {
    }

    [Serializable]
    public class AudioClipListData : VariableData<List<AudioClip>>
    {
        public AudioClipListData()
        {
            m_DataVal = new List<AudioClip>();
        }

        public AudioClipListData(List<AudioClip> v)
        {
            m_DataVal = v;
        }
    }

}
