﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/RectInt Variable")]
    public class RectIntVariable : Variable<RectInt>
    {
    }

    [Serializable]
    public class RectIntData : VariableData<RectInt>
    {
        public RectIntData()
        {
            m_DataVal = new RectInt(0, 0, 0, 0);
        }

        public RectIntData(RectInt v)
        {
            m_DataVal = v;
        }
    }

}
