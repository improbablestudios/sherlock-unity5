using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Vector2Int List Variable")]
    public class Vector2IntListVariable : Variable<List<Vector2Int>>
    {
    }

    [Serializable]
    public class Vector2IntListData : VariableData<List<Vector2Int>>
    {
        public Vector2IntListData()
        {
            m_DataVal = new List<Vector2Int>();
        }

        public Vector2IntListData(List<Vector2Int> v)
        {
            m_DataVal = v;
        }
    }

}