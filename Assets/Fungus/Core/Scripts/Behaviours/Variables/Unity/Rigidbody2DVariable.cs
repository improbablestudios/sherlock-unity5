﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Rigidbody 2D Variable")]
    public class Rigidbody2DVariable : Variable<Rigidbody2D>
    {
    }

    [Serializable]
    public class Rigidbody2DData : VariableData<Rigidbody2D>
    {
        public Rigidbody2DData()
        {
        }

        public Rigidbody2DData(Rigidbody2D v)
        {
            m_DataVal = v;
        }
    }

}