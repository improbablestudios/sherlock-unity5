﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Line Renderer Variable")]
    public class LineRendererVariable : Variable<LineRenderer>
    {
    }

    [Serializable]
    public class LineRendererData : VariableData<LineRenderer>
    {
        public LineRendererData()
        {
        }

        public LineRendererData(LineRenderer v)
        {
            m_DataVal = v;
        }
    }

}