﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Slider Variable")]
    public class SliderVariable : Variable<Slider>
    {
    }

    [Serializable]
    public class SliderData : VariableData<Slider>
    {
        public SliderData()
        {
        }

        public SliderData(Slider v)
        {
            m_DataVal = v;
        }
    }

}
