﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/UI/Toggle Group List Variable")]
    public class ToggleGroupListVariable : Variable<List<ToggleGroup>>
    {
    }

    [Serializable]
    public class ToggleGroupListData : VariableData<List<ToggleGroup>>
    {
        public ToggleGroupListData()
        {
            m_DataVal = new List<ToggleGroup>();
        }

        public ToggleGroupListData(List<ToggleGroup> v)
        {
            m_DataVal = v;
        }
    }

}