using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Vector3 Variable")]
    public class Vector3Variable : Variable<Vector3>
    {
    }

    [Serializable]
    public class Vector3Data : VariableData<Vector3>
    {
        public Vector3Data()
        {
            m_DataVal = Vector3.zero;
        }

        public Vector3Data(Vector3 v)
        {
            m_DataVal = v;
        }
    }

}