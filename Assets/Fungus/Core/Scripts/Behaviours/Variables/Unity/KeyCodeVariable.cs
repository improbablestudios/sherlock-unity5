﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Key Code Variable")]
    public class KeyCodeVariable : Variable<KeyCode>
    {
    }

    [Serializable]
    public class KeyCodeData : VariableData<KeyCode>
    {
        public KeyCodeData()
        {
        }

        public KeyCodeData(KeyCode v)
        {
            m_DataVal = v;
        }
    }

}