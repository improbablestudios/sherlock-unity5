﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Space List Variable")]
    public class SpaceListVariable : Variable<List<Space>>
    {
    }

    [Serializable]
    public class SpaceListData : VariableData<List<Space>>
    {
        public SpaceListData()
        {
            m_DataVal = new List<Space>();
        }

        public SpaceListData(List<Space> v)
        {
            m_DataVal = v;
        }
    }

}