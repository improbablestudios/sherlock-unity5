﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/RectInt List Variable")]
    public class RectIntListVariable : Variable<List<RectInt>>
    {
    }

    [Serializable]
    public class RectIntListData : VariableData<List<RectInt>>
    {
        public RectIntListData()
        {
            m_DataVal = new List<RectInt>();
        }

        public RectIntListData(List<RectInt> v)
        {
            m_DataVal = v;
        }
    }

}
