﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/UI/Input Field Variable")]
    public class InputFieldVariable : Variable<InputField>
    {
    }

    [Serializable]
    public class InputFieldData : VariableData<InputField>
    {
        public InputFieldData()
        {
        }

        public InputFieldData(InputField v)
        {
            m_DataVal = v;
        }
    }

}