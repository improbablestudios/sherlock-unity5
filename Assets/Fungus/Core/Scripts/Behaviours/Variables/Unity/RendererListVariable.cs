﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Renderer List Variable")]
    public class RendererListVariable : Variable<List<Renderer>>
    {
    }

    [Serializable]
    public class RendererListData : VariableData<List<Renderer>>
    {
        public RendererListData()
        {
            m_DataVal = new List<Renderer>();
        }

        public RendererListData(List<Renderer> v)
        {
            m_DataVal = v;
        }
    }

}