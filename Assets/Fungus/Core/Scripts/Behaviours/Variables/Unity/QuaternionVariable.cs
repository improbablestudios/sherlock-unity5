﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Quaternion Variable")]
    public class QuaternionVariable : Variable<Quaternion>
    {
    }
    
    [Serializable]
    public class QuaternionData : VariableData<Quaternion>
    {
        public QuaternionData()
        {
            m_DataVal = Quaternion.identity;
        }

        public QuaternionData(Quaternion v)
        {
            m_DataVal = v;
        }
    }
    
}
