﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Audio Clip Variable")]
    public class AudioClipVariable : Variable<AudioClip>
    {
    }

    [Serializable]
    public class AudioClipData : VariableData<AudioClip>
    {
        public AudioClipData()
        {
        }

        public AudioClipData(AudioClip v)
        {
            m_DataVal = v;
        }
    }

}
