﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/BoundsInt Variable")]
    public class BoundsIntVariable : Variable<BoundsInt>
    {
    }

    [Serializable]
    public class BoundsIntData : VariableData<BoundsInt>
    {
        public BoundsIntData()
        {
        }

        public BoundsIntData(BoundsInt v)
        {
            m_DataVal = v;
        }
    }

}