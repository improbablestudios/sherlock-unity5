﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Component Variable")]
    public class ComponentVariable : Variable<Component>
    {
    }

    [Serializable]
    public class ComponentData : VariableData<Component>
    {
        public ComponentData()
        {
        }

        public ComponentData(Component v)
        {
            m_DataVal = v;
        }
    }

}