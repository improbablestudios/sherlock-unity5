﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Canvas List Variable")]
    public class CanvasListVariable : Variable<List<Canvas>>
    {
    }

    [Serializable]
    public class CanvasListData : VariableData<List<Canvas>>
    {
        public CanvasListData()
        {
            m_DataVal = new List<Canvas>();
        }

        public CanvasListData(List<Canvas> v)
        {
            m_DataVal = v;
        }
    }

}
