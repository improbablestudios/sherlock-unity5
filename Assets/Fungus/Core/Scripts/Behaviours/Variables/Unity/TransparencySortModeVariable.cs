﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Unity/Transparency Sort Mode Variable")]
    public class TransparencySortModeVariable : Variable<TransparencySortMode>
    {
    }

    [Serializable]
    public class TransparencySortModeData : VariableData<TransparencySortMode>
    {
        public TransparencySortModeData()
        {
        }

        public TransparencySortModeData(TransparencySortMode v)
        {
            m_DataVal = v;
        }
    }

}