﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Text Asset List Variable")]
    public class TextAssetListVariable : Variable<List<TextAsset>>
    {
    }

    [Serializable]
    public class TextAssetListData : VariableData<List<TextAsset>>
    {
        public TextAssetListData()
        {
            m_DataVal = new List<TextAsset>();
        }

        public TextAssetListData(List<TextAsset> v)
        {
            m_DataVal = v;
        }
    }

}