﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Unity/Runtime Animator Controller List Variable")]
    public class RuntimeAnimatorControllerListVariable : Variable<List<RuntimeAnimatorController>>
    {
    }

    [Serializable]
    public class RuntimeAnimatorControllerListData : VariableData<List<RuntimeAnimatorController>>
    {
        public RuntimeAnimatorControllerListData()
        {
            m_DataVal = new List<RuntimeAnimatorController>();
        }

        public RuntimeAnimatorControllerListData(List<RuntimeAnimatorController> v)
        {
            m_DataVal = v;
        }
    }

}
