﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/System/Type Variable")]
    public class TypeVariable : Variable<Type>
    {
    }

    [Serializable]
    public class TypeData : VariableData<Type>
    {
        public TypeData()
        {
        }

        public TypeData(Type v)
        {
            m_DataVal = v;
        }
    }

}