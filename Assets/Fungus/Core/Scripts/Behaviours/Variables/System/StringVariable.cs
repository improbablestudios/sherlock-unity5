using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.Localization;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/System/String Variable")]
    public class StringVariable : Variable<string>
    {
        public Dictionary<string, string> GetLocalizedDictionary(StringFormatter stringFormatter)
        {
            Dictionary<string, string> localizedDictionary = GetLocalizedDictionary();
            localizedDictionary.FormatLocalizedDictionary(stringFormatter);
            return localizedDictionary;
        }
    }

    [Serializable]
    public class StringData : VariableData<string>
    {
        public virtual new StringVariable Variable
        {
            get
            {
                return m_Variable as StringVariable;
            }
            set
            {
                m_Variable = value;
            }
        }

        public StringData()
        {
            m_Localize = true;
            m_DataVal = "";
        }

        public StringData(string v)
        {
            m_Localize = true;
            m_DataVal = v;
        }
    }

}