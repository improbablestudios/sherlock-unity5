﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/System/Char List Variable")]
    public class CharListVariable : Variable<List<char>>
    {
    }

    [Serializable]
    public class CharListData : VariableData<List<char>>
    {
        public CharListData()
        {
            m_DataVal = new List<Char>();
        }

        public CharListData(List<Char> v)
        {
            m_DataVal = v;
        }
    }

}