using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/System/Boolean Variable")]
    public class BooleanVariable : Variable<bool>
    {
    }

    [Serializable]
    public class BooleanData : VariableData<bool>
    {
        public BooleanData()
        {
        }

        public BooleanData(bool v)
        {
            m_DataVal = v;
        }
    }

}