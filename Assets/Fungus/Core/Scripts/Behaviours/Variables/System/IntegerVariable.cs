using System;
using UnityEngine;

namespace Fungus.Variables
{
    
    [AddComponentMenu("Fungus/Variables/System/Integer Variable")]
    public class IntegerVariable : Variable<int>
    {
    }

    [Serializable]
    public class IntegerData : VariableData<int>
    {
        public IntegerData()
        {
        }

        public IntegerData(int v)
        {
            m_DataVal = v;
        }
    }
    
}