using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{
    
    [AddComponentMenu("Fungus/Variables/List/System/Integer List Variable")]
    public class IntegerListVariable : Variable<List<int>>
    {
    }

    [Serializable]
    public class IntegerListData : VariableData<List<int>>
    {
        public IntegerListData()
        {
            m_DataVal = new List<int>();
        }

        public IntegerListData(List<int> v)
        {
            m_DataVal = v;
        }
    }

}