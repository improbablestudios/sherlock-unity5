using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/System/String List Variable")]
    public class StringListVariable : Variable<List<string>>
    {
    }

    [Serializable]
    public class StringListData : VariableData<List<string>>
    {
        public StringListData()
        {
            m_DataVal = new List<string>();
        }

        public StringListData(List<string> v)
        {
            m_DataVal = v;
        }
    }

}