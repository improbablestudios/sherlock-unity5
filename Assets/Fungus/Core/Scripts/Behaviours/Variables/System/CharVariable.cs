﻿using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/System/Char Variable")]
    public class CharVariable : Variable<char>
    {
    }

    [Serializable]
    public class CharData : VariableData<char>
    {
        public CharData()
        {
        }

        public CharData(char v)
        {
            m_DataVal = v;
        }
    }
    
}