using System;
using UnityEngine;

namespace Fungus.Variables
{
    
    [AddComponentMenu("Fungus/Variables/System/Float Variable")]
    public class FloatVariable : Variable<float>
    {
    }

    [Serializable]
    public class FloatData : VariableData<float>
    {
        public FloatData()
        {
        }

        public FloatData(float v)
        {
            m_DataVal = v;
        }
    }

}