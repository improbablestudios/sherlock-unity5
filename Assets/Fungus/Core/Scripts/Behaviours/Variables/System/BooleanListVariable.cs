using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/System/Boolean List Variable")]
    public class BooleanListVariable : Variable<List<bool>>
    {
    }

    [Serializable]
    public class BooleanListData : VariableData<List<bool>>
    {
        public BooleanListData()
        {
            m_DataVal = new List<bool>();
        }

        public BooleanListData(List<bool> v)
        {
            m_DataVal = v;
        }
    }

}