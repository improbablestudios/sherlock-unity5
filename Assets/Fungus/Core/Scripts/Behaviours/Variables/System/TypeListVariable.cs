﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/System/Type List Variable")]
    public class TypeListVariable : Variable<List<Type>>
    {
    }

    [Serializable]
    public class TypeListData : VariableData<List<Type>>
    {
        public TypeListData()
        {
            m_DataVal = new List<Type>();
        }

        public TypeListData(List<Type> v)
        {
            m_DataVal = v;
        }
    }

}