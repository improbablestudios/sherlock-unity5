using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{
    
    [AddComponentMenu("Fungus/Variables/List/System/Float List Variable")]
    public class FloatListVariable : Variable<List<float>>
    {
    }

    [Serializable]
    public class FloatListData : VariableData<List<float>>
    {
        public FloatListData()
        {
            m_DataVal = new List<float>();
        }

        public FloatListData(List<float> v)
        {
            m_DataVal = v;
        }
    }

}