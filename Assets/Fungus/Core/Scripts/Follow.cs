﻿using UnityEngine;
using DG.Tweening;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{
    public class Follow : PersistentBehaviour
    {
        [Tooltip("The target transform that this transform should follow")]
        [SerializeField]
        protected Transform m_Target;

        private Vector3 m_targetLastPos;

        private Tweener m_tween;

        public Transform Target
        {
            get
            {
                return m_Target;
            }
            set
            {
                m_Target = value;
            }
        }

        void Start()
        {
            // First create the "move to target" tween and store it as a Tweener.
            // In this case I'm also setting autoKill to FALSE so the tween can go on forever
            // (otherwise it will stop executing if it reaches the target)
            m_tween = transform.DOMove(m_Target.position, 0).SetAutoKill(false);
            // Store the target's last position, so it can be used to know if it changes
            // (to prevent changing the tween if nothing actually changes)
            m_targetLastPos = m_Target.position;
        }

        void LateUpdate()
        {
            // Use an Update routine to change the tween's endValue each frame
            // so that it updates to the target's position if that changed
            if (m_targetLastPos == m_Target.position) return;
            // Add a Restart in the end, so that if the tween was completed it will play again
            m_tween.ChangeEndValue(m_Target.position, true).Restart();
            m_targetLastPos = m_Target.position;
        }
    }
}

