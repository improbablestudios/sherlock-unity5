﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.SerializationUtilities;

namespace Fungus
{

    [Serializable]
    public class GenericValue : ISerializationCallbackReceiver
    {
        [SerializeField]
        [FormerlySerializedAs("valueTypeName")]
        [FormerlySerializedAs("m_valueTypeName")]
        protected string m_ValueTypeName = "";

        [SerializeField]
        [FormerlySerializedAs("useDynamicValueType")]
        [FormerlySerializedAs("m_useDynamicValueType")]
        protected bool m_UseDynamicValueType = false;

        [SerializeField]
        protected string m_SerializedValue;
        
        [SerializeField]
        [FormerlySerializedAs("objectValue")]
        [FormerlySerializedAs("m_objectReferenceValue")]
        protected UnityEngine.Object m_ObjectReferenceValue;
        
        [SerializeField]
        protected List<UnityEngine.Object> m_ObjectReferenceListValue;


        public string ValueTypeName
        {
            get
            {
                return m_ValueTypeName;
            }
            set
            {
                m_ValueTypeName = value;
            }
        }

        public bool UseDynamicValueType
        {
            get
            {
                return m_UseDynamicValueType;
            }
            set
            {
                m_UseDynamicValueType = value;
            }
        }

        public string SerializedValue
        {
            get
            {
                return m_SerializedValue;
            }
            set
            {
                m_SerializedValue = value;
            }
        }
        
        public UnityEngine.Object ObjectReferenceValue
        {
            get
            {
                return m_ObjectReferenceValue;
            }
            set
            {
                m_ObjectReferenceValue = value;
            }
        }
        
        public List<UnityEngine.Object> ObjectReferenceListValue
        {
            get
            {
                return m_ObjectReferenceListValue;
            }
            set
            {
                m_ObjectReferenceListValue = value;
            }
        }

        public Type ValueType
        {
            get
            {
                if (!string.IsNullOrEmpty(m_ValueTypeName))
                {
                    Type valueType = m_ValueTypeName.DeserializeType();
                    if (valueType != null)
                    {
                        return valueType;
                    }
                    else
                    {
                        Debug.LogError("Type could not be deserialized: " + m_ValueTypeName);
                    }
                }
                return null;
            }
            set
            {
                string oldTypeString = m_ValueTypeName;
                string newTypeString = value.SerializeType();
                if (newTypeString != null)
                {
                    Type oldType = oldTypeString.DeserializeType();
                    if (oldType != null && oldType != value)
                    {
                        ResetValue(value);
                    }
                    m_ValueTypeName = newTypeString;
                }
            }
        }

        public object Value
        {
            get
            {
                return GetValue(ValueType);
            }
            set
            {
                SetValue(ValueType, value);
            }
        }
        
        public static string ConvertObjectToSerializedValue(Type type, object value)
        {
            if (type == null)
            {
                return null;
            }
            
            return UnityValueSerializer.Serialize(type, value);
        }

        public static object ConvertSerializedValueToObject(Type type, string str)
        {
            if (type == null)
            {
                return null;
            }

            return UnityValueSerializer.Deserialize(type, str);
        }

        public T GetValue<T>()
        {
            ValueType = typeof(T);
            return (T)GetValue(typeof(T));
        }

        public object GetValue(Type type)
        {
            if (type == null)
            {
                return null;
            }

            ValueType = type;
            
            if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                return m_ObjectReferenceValue;
            }
            else if (typeof(IList).IsAssignableFrom(type) && typeof(UnityEngine.Object).IsAssignableFrom(type.GetItemType()))
            {
                if (m_ObjectReferenceListValue == null)
                {
                    return null;
                }
                else
                {
                    return m_ObjectReferenceListValue.Cast(type.GetItemType());
                }
            }
            else
            {
                return ConvertSerializedValueToObject(type, m_SerializedValue);
            }
        }

        public void SetValue<T>(object value)
        {
            ValueType = typeof(T);
            SetValue(typeof(T), value);
        }

        public void SetValue(Type type, object value)
        {
            ValueType = type;

            if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                m_SerializedValue = null;
                m_ObjectReferenceValue = value as UnityEngine.Object;
                m_ObjectReferenceListValue = new List<UnityEngine.Object>();
            }
            else if (typeof(IList).IsAssignableFrom(type) && typeof(UnityEngine.Object).IsAssignableFrom(type.GetItemType()))
            {
                m_SerializedValue = null;
                m_ObjectReferenceValue = null;
                if (value == null)
                {
                    m_ObjectReferenceListValue = null;
                }
                else
                {
                    IList list = value as IList;
                    m_ObjectReferenceListValue = list.Cast<UnityEngine.Object>().ToList();
                }
            }
            else
            {
                m_SerializedValue = ConvertObjectToSerializedValue(type, value);
                m_ObjectReferenceValue = null;
                m_ObjectReferenceListValue = new List<UnityEngine.Object>();
            }
        }

        public void ResetValue(Type type)
        {
            m_SerializedValue = GetDefaultSerializedValue(type);
            m_ObjectReferenceValue = GetDefaultObjectReferenceValue();
            m_ObjectReferenceListValue = GetDefaultObjectReferenceListValue();
        }
        
        public static string GetDefaultSerializedValue(Type type)
        {
            if (type != null)
            {
                if (typeof(UnityEngine.Object).IsAssignableFrom(type))
                {
                    return null;
                }
                else if (typeof(IList).IsAssignableFrom(type) && typeof(UnityEngine.Object).IsAssignableFrom(type.GetItemType()))
                {
                    return null;
                }
                else
                {
                    object defaultValue = type.GetDefaultValue();
                    if (typeof(IList).IsAssignableFrom(type))
                    {
                        Type listType = typeof(List<>);
                        Type constructedListType = listType.MakeGenericType(type.GetItemType());
                        defaultValue = Activator.CreateInstance(constructedListType);
                    }
                    return ConvertObjectToSerializedValue(type, defaultValue);
                }
            }

            return null;
        }

        public static UnityEngine.Object GetDefaultObjectReferenceValue()
        {
            return null;
        }

        public static List<UnityEngine.Object> GetDefaultObjectReferenceListValue()
        {
            return new List<UnityEngine.Object>();
        }

        public override string ToString()
        {
            object value = GetValue(ValueType);
            if (value == null)
            {
                return "<None>";
            }
            return value.ToString();
        }

        public void OnBeforeSerialize()
        {
            // Reserialize value type name just in case type has moved to a different namespace or assembly
            if (!string.IsNullOrEmpty(m_ValueTypeName))
            {
                Type type = m_ValueTypeName.DeserializeType();
                if (type != null)
                {
                    m_ValueTypeName = type.SerializeType();
                }
            }
        }

        public void OnAfterDeserialize()
        {
        }
    }
    
    [Serializable]
    public class GenericVariableData : VariableData
    {
        [SerializeField]
        [FormerlySerializedAs("variableVal")]
        [FormerlySerializedAs("dataVal")]
        [FormerlySerializedAs("m_dataVal")]
        protected GenericValue m_DataVal = new GenericValue();

        public GenericValue DataVal
        {
            get
            {
                return m_DataVal;
            }
            set
            {
                m_DataVal = value;
            }
        }

        public override Type ValueType
        {
            get
            {
                return m_DataVal.ValueType;
            }
        }

        public GenericValue GenericValue
        {
            get
            {
                return m_DataVal;
            }
            set
            {
                m_DataVal = value;
            }
        }

        public override object DataValue
        {
            get
            {
                return m_DataVal.Value;
            }
            set
            {
                m_DataVal.Value = value;
            }
        }

        public override object Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        public override object GetValue()
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Value:
                    return m_DataVal.GetValue(ValueType);
                case VariableDataType.Variable:
                    if (m_Variable != null)
                    {
                        return m_Variable.Value;
                    }
                    return null;
                case VariableDataType.Command:
                    if (m_Command != null)
                    {
                        return m_Command.ReturnValueObject();
                    }
                    return null;
            }
            return null;
        }

        public override void SetValue(object objValue)
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Value:
                    m_DataVal.SetValue(ValueType, objValue);
                    break;
                case VariableDataType.Variable:
                    m_Variable.Value = objValue;
                    break;
            }
        }

        public void SetValueType(Type type)
        {
            m_DataVal.ValueType = type;
        }

        public void SetValueType(Type type, object value)
        {
            m_DataVal.ValueType = type;
            m_DataVal.Value = value;
        }
        
        public void SetValueType<T>(T value)
        {
            m_DataVal.ValueType = typeof(T);
            m_DataVal.Value = value;
        }

        public override string ToString()
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Value:
                    if (Value == null)
                    {
                        return "<None>";
                    }
                    if (ValueType.IsEnum)
                    {
                        return Enum.GetNames(ValueType)[(int)Value];
                    }
                    UnityEngine.Object obj = Value as UnityEngine.Object;
                    if (obj != null)
                    {
                        if (Value.ToString() == obj.name + " (" + obj.GetType().FullName + ")")
                        {
                            return obj.name;
                        }
                    }
                    if (typeof(IList).IsAssignableFrom(ValueType))
                    {
                        IList list = (IList)Value;
                        string[] listItemStrings = new string[list.Count];
                        for (int i = 0; i < list.Count; i++)
                        {
                            if (list[i] != null)
                            {
                                listItemStrings[i] = list[i].ToString();
                            }
                            else
                            {
                                listItemStrings[i] = "<None>";
                            }
                        }
                        return string.Format("{0}({1})", ValueType.GetNiceTypeName(), string.Join(",", listItemStrings));
                    }
                    return Value.ToString();
                case VariableDataType.Variable:
                    if (m_Variable == null)
                    {
                        return "<None>";
                    }
                    return m_Variable.ToString();
                case VariableDataType.Command:
                    if (m_Command == null)
                    {
                        return "<None>";
                    }
                    return m_Command.ToString();

            }
            return base.ToString();
        }

        public GenericVariableData()
        {
        }

        public GenericVariableData(bool useDynamicValueType)
        {
            DataVal.UseDynamicValueType = useDynamicValueType;
        }

        public GenericVariableData(Type type)
        {
            SetValueType(type);
        }

        public GenericVariableData(Type type, object value)
        {
            SetValueType(type);
            Value = value;
        }
    }
}
