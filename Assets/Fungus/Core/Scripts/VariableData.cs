﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using ImprobableStudios.Localization;
using ImprobableStudios.ReflectionUtilities;

namespace Fungus
{

    public enum VariableDataType
    {
        Value,
        Variable,
        Command
    }

    [Serializable]
    public class VariableData : IExplicitLocalizableValue
    {
        public const string LOCALIZED_MEMBER_KEY = "Value";

        [SerializeField]
        [FormerlySerializedAs("variableDataType")]
        [FormerlySerializedAs("m_variableDataType")]
        protected VariableDataType m_VariableDataType;

        [SerializeField]
        [FormerlySerializedAs("variableRef")]
        [FormerlySerializedAs("m_variableRef")]
        protected Variable m_Variable;

        [SerializeField]
        [FormerlySerializedAs("commandRef")]
        [FormerlySerializedAs("m_commandRef")]
        protected ReturnValueCommand m_Command;

        [SerializeField]
        [FormerlySerializedAs("localize")]
        [FormerlySerializedAs("m_localize")]
        protected bool m_Localize;
        
        public VariableDataType VariableDataType
        {
            get
            {
                return m_VariableDataType;
            }
            set
            {
                m_VariableDataType = value;
            }
        }

        public Variable Variable
        {
            get
            {
                return m_Variable;
            }
            set
            {
                m_Variable = value;
            }
        }

        public ReturnValueCommand Command
        {
            get
            {
                return m_Command;
            }
            set
            {
                m_Command = value;
            }
        }

        public bool Localize
        {
            get
            {
                return m_Localize;
            }
            set
            {
                m_Localize = value;
            }
        }

        protected Type[] m_supportedValueTypes;

        public virtual object Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        public virtual Type ValueType
        {
            get
            {
                if (VariableDataType == VariableDataType.Variable)
                {
                    if (Variable != null)
                    {
                        return Variable.ValueType;
                    }
                }
                else if (VariableDataType == VariableDataType.Command)
                {
                    if (Command != null)
                    {
                        return Command.SpecificReturnTypes()[0];
                    }
                }
                return null;
            }
        }

        public virtual Type[] SupportedValueTypes
        {
            get
            {
                if (m_supportedValueTypes == null)
                {
                    m_supportedValueTypes = new Type[] { typeof(object) };
                }
                return m_supportedValueTypes;
            }
            set
            {
                m_supportedValueTypes = value;
            }
        }

        public virtual object DataValue
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public bool IsConstant
        {
            get
            {
                return m_VariableDataType == VariableDataType.Value;
            }
        }

        public virtual object GetValue()
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Variable:
                    return m_Variable.Value;
                case VariableDataType.Command:
                    return m_Command.ReturnValueObject();
            }
            return null;
        }

        public virtual void SetValue(object objValue)
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Variable:
                    m_Variable.Value = objValue;
                    break;
            }
        }

        public override string ToString()
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Variable:
                    if (m_Variable == null)
                    {
                        return "<None>";
                    }
                    return m_Variable.ToString();
                case VariableDataType.Command:
                    if (m_Command == null)
                    {
                        return "<None>";
                    }
                    return m_Command.ToString();

            }
            return base.ToString();
        }

        public static Type GetVariableDataType(Type valueType)
        {
            foreach (Type t in (typeof(VariableData<>)).FindAllDerivedTypesOfGenericType())
            {
                if (valueType == GetVariableDataValueType(t))
                {
                    return t;
                }
            }
            return null;
        }

        public static Type GetVariableDataValueType(Type type)
        {
            Type genericVariableDataType = GetBaseVariableDataType(type);
            if (genericVariableDataType != null)
            {
                Type baseDataValueType = genericVariableDataType.GetGenericArguments()[0];
                if (baseDataValueType.FullName != null)
                {
                    return baseDataValueType;
                }
            }
            return null;
        }

        protected static Type GetBaseVariableDataType(Type type)
        {
            while (type != null && type != typeof(object))
            {
                if (type.BaseType != null && type.BaseType == typeof(VariableData) && type.IsGenericType)
                {
                    return type;
                }
                type = type.BaseType;
            }
            return null;
        }

        public static string GetDataValPropertyName()
        {
            return "m_" + nameof(VariableData<object>.DataVal);
        }

        //
        // IExplicitLocalizedValue Implementation
        //

        public virtual bool ShouldBeLocalized(MemberInfo memberInfo)
        {
            return
                Attribute.IsDefined(memberInfo, typeof(LocalizeAttribute)) &&
                IsConstant &&
                LocalizationManager.Instance.LocalizationSerializer.TypeCanBeLocalized(ValueType) &&
                m_Localize;
        }

        public virtual void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary)
        {
            dictionary[LOCALIZED_MEMBER_KEY] = DataValue;
        }

        public virtual void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary)
        {
            if (dictionary.ContainsKey(LOCALIZED_MEMBER_KEY))
            {
                DataValue = dictionary[LOCALIZED_MEMBER_KEY];
            }
        }

        public void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary)
        {
        }

        public void ExplicitlyGetConverters(Dictionary<string, Type> dictionary)
        {
        }
    }

    [Serializable]
    public abstract class VariableData<T> : VariableData
    {
        [SerializeField]
        [FormerlySerializedAs("dataVal")]
        [FormerlySerializedAs("m_dataVal")]
        protected T m_DataVal;

        public T DataVal
        {
            get
            {
                return m_DataVal;
            }
            set
            {
                m_DataVal = value;
            }
        }

        public virtual new T Value
        {
            get
            {
                switch (m_VariableDataType)
                {
                    case VariableDataType.Value:
                        return m_DataVal;
                    case VariableDataType.Variable:
                        if (m_Variable != null)
                        {
                            return (T)m_Variable.Value;
                        }
                        return default(T);
                    case VariableDataType.Command:
                        if (m_Command != null)
                        {
                            return (T)m_Command.ReturnValueObject();
                        }
                        return default(T);
                }
                return default(T);
            }
            set
            {
                switch (m_VariableDataType)
                {
                    case VariableDataType.Value:
                        m_DataVal = value;
                        break;
                    case VariableDataType.Variable:
                        m_Variable.Value = value;
                        break;
                }
            }
        }
        
        public virtual new Variable<T> Variable
        {
            get
            {
                return (Variable<T>)m_Variable;
            }
            set
            {
                m_Variable = value;
            }
        }

        public override object DataValue
        {
            get
            {
                return m_DataVal;
            }
            set
            {
                m_DataVal = (T)value;
            }
        }

        public override Type ValueType
        {
            get
            {
                return typeof(T);
            }
        }

        public override Type[] SupportedValueTypes
        {
            get
            {
                if (m_supportedValueTypes == null)
                {
                    m_supportedValueTypes = new Type[] { typeof(T) };
                }
                return m_supportedValueTypes;
            }
            set
            {
                m_supportedValueTypes = value;
            }
        }

        public override object GetValue()
        {
            return Value;
        }

        public override void SetValue(object value)
        {
            Value = (T)value;
        }

        public override string ToString()
        {
            switch (m_VariableDataType)
            {
                case VariableDataType.Value:
                    if (m_DataVal == null || (typeof(UnityEngine.Object).IsAssignableFrom(typeof(T)) && (m_DataVal as UnityEngine.Object) == null))
                    {
                        return "<None>";
                    }
                    UnityEngine.Object obj = m_DataVal as UnityEngine.Object;
                    if (obj != null)
                    {
                        if (m_DataVal.ToString() == obj.name + " (" + obj.GetType().FullName + ")")
                        {
                            return obj.name;
                        }
                    }
                    if (m_DataVal != null && typeof(IList).IsAssignableFrom(m_DataVal.GetType()))
                    {
                        return string.Format("List<{0}>", m_DataVal.GetType().GetGenericArguments()[0].Name);
                    }
                    return m_DataVal.ToString();
                case VariableDataType.Variable:
                    if (m_Variable == null)
                    {
                        return "<None>";
                    }
                    return m_Variable.ToString();
                case VariableDataType.Command:
                    if (m_Command == null)
                    {
                        return "<None>";
                    }
                    return m_Command.ToString();
            }
            return base.ToString();
        }

        public VariableData()
        {
            m_DataVal = default(T);
            m_Variable = null;
            m_Command = null;
        }

        public VariableData(T v)
        {
            m_DataVal = v;
            m_Variable = null;
            m_Command = null;
        }
    }

}
