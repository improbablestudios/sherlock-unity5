﻿using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.CharacterSystem;
using System;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus
{

    [RequireComponent(typeof(Flowchart))]
    public class FountainScript : PersistentBehaviour
    {
        [Tooltip("Text file containing a script in fountain format")]
        [SerializeField]
        [CheckNotNull]
        protected TextAsset m_ScriptFile;

        public TextAsset ScriptFile
        {
            get
            {
                return m_ScriptFile;
            }
            set
            {
                m_ScriptFile = value;
            }
        }

#if UNITY_EDITOR
        public static string ExportStrings(Flowchart flowchart)
        {
            string exportText = "Title: " + flowchart.name;

#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
            exportText += "\n\n" + "Source: " + EditorApplication.currentScene;
#else
            exportText += "\n\n" + "Source: " + UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name;
#endif
            exportText += "\n\n" + "Draft date: " + System.DateTime.UtcNow.ToString("g");

            // In every block, write out Say & Choice text in order
            foreach (Block block in flowchart.Blocks)
            {
                // Check for any Say, Choice or Comment commands
                bool hasText = false;
                foreach (Command c in block.Commands)
                {
                    System.Type t = c.GetType();
                    if ((typeof(AnimatePortraitCommand)).IsAssignableFrom(t) ||
                       t == typeof(Say) ||
                       t == typeof(Choice) ||
                       t == typeof(Comment) ||
                       t == typeof(Call))
                    {
                        hasText = true;
                    }
                }
                if (!hasText)
                {
                    continue;
                }

                #region BLOCK
                exportText += "\n\n" + "===";
                exportText += "\n\n" + "." + block.GetName().ToUpper();
                #endregion

                string portraitText = "";
                foreach (Command c in block.Commands)
                {
                    if (c is AnimatePortraitCommand)
                    {
                        AnimatePortraitCommand controlPortrait = c as AnimatePortraitCommand;
                        if (controlPortrait.m_Character.Value != null)
                        {
                            Sprite portrait = controlPortrait.m_Character.Value.GetPortrait(controlPortrait.m_PortraitIndex.Value);
                            if (portrait != null)
                            {
                                // If next command is a say, and characters match, assume portrait command was meant to accompany say command
                                for (int i = c.CommandIndex + 1; i < block.Commands.Count; i++)
                                {
                                    if (block.Commands[i].GetType() == typeof(Say))
                                    {
                                        Say nextSay = block.Commands[c.CommandIndex + 1] as Say;
                                        if (nextSay != null)
                                        {
                                            if (nextSay.m_Character.Value == controlPortrait.m_Character.Value)
                                            {
                                                if (controlPortrait is HidePortrait)
                                                {
                                                    portraitText = "\n" + "(HIDE)";
                                                }
                                                else
                                                {
                                                    portraitText = "\n" + "(" + portrait.name + ")";
                                                }
                                                portraitText += " " + "[[{" + c.ItemId + "}]]";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (c.GetType() == typeof(Say))
                    {
                        exportText += "\n\n";
                        Say say = c as Say;
                        if (say.m_Character.Value == null)
                        {
                            if (!string.IsNullOrEmpty(say.m_NameText.Value))
                            {
                                exportText += say.m_NameText.Value.ToUpper().Trim();
                            }
                            else
                            {
                                exportText += "NO CHARACTER";
                            }
                        }
                        else
                        {
                            exportText += say.m_Character.Value.DisplayName.ToUpper().Trim();
                        }
                        if (say.m_ContinuePrevious.Value)
                        {
                            exportText += " (CONT'D)";
                        }

                        exportText += " " + "[[{" + c.ItemId + "}]]";
                        exportText += portraitText;

                        // Fountain requires blank dialogue lines to contain 2 spaces or else
                        // they will be interpreted as ACTION text.
                        string trimmedText = say.m_DialogText.Value.Trim();
                        string[] lines = trimmedText.Split(new[] { '\r', '\n' });
                        foreach (string line in lines)
                        {
                            string trimmed = line.Trim();
                            if (line.Length == 0)
                            {
                                exportText += "\n" + "  ";
                            }
                            else
                            {
                                exportText += "\n" + trimmed;
                            }
                        }
                    }
                    else if (c.GetType() == typeof(Choice))
                    {
                        Choice choice = c as Choice;

                        exportText += "\n\n" + "CHOICE";
                        exportText += " " + "[[{" + c.ItemId + "}]]";
                        if (choice.m_TargetBlock.Value != null)
                        {
                            exportText += "\n" + "(" + choice.m_TargetBlock.Value.GetName().ToUpper() + ")";
                        }
                        else
                        {
                            exportText += "\n" + "(ERROR: SCENE NOT FOUND)";
                        }
                        exportText += "\n" + choice.m_Text.Value.Trim();
                    }
                    else if (c.GetType() == typeof(Comment))
                    {
                        Comment comment = c as Comment;

                        exportText += "\n\n" + "[[" + comment.m_CommentText.Trim() + "]]";
                        exportText += " " + "[[{" + comment.ItemId + "}]]";
                    }
                    else if (c.GetType() == typeof(Call))
                    {
                        Call call = c as Call;

                        exportText += "\n\n" + "CUT TO:";
                        exportText += " " + "[[{" + c.ItemId + "}]]";
                        if (call.m_TargetBlock.Value != null)
                        {
                            exportText += "\n\n" + "." + call.m_TargetBlock.Value.GetName().ToUpper();
                        }
                        else
                        {
                            exportText += "\n\n" + ".ERROR: SCENE NOT FOUND";
                        }
                    }
                }
            }
            exportText += "\n";

            return exportText;
        }

        public static void ImportStrings(string script, Flowchart flowchart)
        {
            FountainScriptParser parser = new FountainScriptParser();
            List<FountainScriptParser.StringItem> items = parser.ProcessText(script);

            // Build dict of commands
            Dictionary<int, Command> commandDict = new Dictionary<int, Command>();
            foreach (Command c in flowchart.gameObject.GetComponentsInChildren<Command>())
            {
                commandDict.Add(c.ItemId, c);
            }

            Block block = null;
            int index = 0;

            float blockPosOffset = 0;
            // FIRST CREATE ALL NECESSARY BLOCKS
            foreach (FountainScriptParser.StringItem item in items)
            {
                if (item.m_Parameters.Length < 2)
                {
                    continue;
                }
                string stringType = item.m_Parameters[0];

                if (stringType == typeof(Block).ToString())
                {
                    #region GET SAY BLOCK PARAMS
                    string blockParam = item.m_BodyText;
                    float xParam = 0;
                    float yParam = blockPosOffset;
                    #endregion

                    #region GET BLOCK
                    block = GetBlockByName(flowchart, blockParam);
                    if (block == null)
                    {
                        Vector2 newNodePosition = new Vector2(xParam, yParam);
                        block = flowchart.AddBlock(newNodePosition) as Block;
                        block.SetName(blockParam);
                    }
                    #endregion

                    blockPosOffset += 70;
                    index = 0;
                }
            }
            foreach (FountainScriptParser.StringItem item in items)
            {
                if (item.m_Parameters.Length < 2)
                {
                    continue;
                }
                string stringType = item.m_Parameters[0];

                int itemId = -1;
                int.TryParse(item.m_Parameters[1], out itemId);

                if (stringType == typeof(Block).ToString())
                {
                    string blockParam = item.m_BodyText;
                    block = GetBlockByName(flowchart, blockParam);
                    index = 0;
                }
                else if (stringType == typeof(Say).ToString())
                {
                    #region GET SAY COMMAND PARAMS
                    string[] commandParams = Regex.Split(item.m_BodyText + "\n", "(?<=\n)");
                    string characterParam = commandParams[0].Trim();
                    bool continuePrevious = false;
                    if (characterParam.Contains("(CONT'D)"))
                    {
                        characterParam = characterParam.Replace("(CONT'D)", "").Trim();
                        continuePrevious = true;
                    }
                    int portraitItemId = -1;
                    string portraitParam = "";
                    string dialogParam = commandParams[1];
                    // If portrait parenthetical exists, assign portraitParam and dialogParam accordingly.
                    if (commandParams[1].Trim().StartsWith("(", StringComparison.Ordinal))
                    {
                        string portraitItemIdString = FountainScriptParser.GetItemIdString(commandParams[1]);
                        int.TryParse(portraitItemIdString, out portraitItemId);
                        portraitParam = Regex.Match(commandParams[1], @"\(\s*(.*?)\s*\)").Groups[1].Value;
                        dialogParam = commandParams[2];
                    }
                    #endregion

                    #region GET SAY COMMAND FIELDS
                    Character character = GetCharacterByName(characterParam);
                    Portrait portrait = GetPortraitByName(character, portraitParam);
                    string storyText = dialogParam;
                    if (storyText == "  ")
                    {
                        storyText = "";
                    }
                    #endregion

                    if (portraitParam != "")
                    {
                        #region EDIT OR ADD PORTRAIT COMMAND
                        if (portraitParam.Trim().ToLower() == "hide")
                        {
                            HidePortrait portraitCommand = GetCommand<HidePortrait>(commandDict, portraitItemId, block, index + 1);
                            portraitCommand.m_Character.Value = character;
                            index = portraitCommand.CommandIndex;
                        }
                        else
                        {
                            ShowPortrait portraitCommand = GetCommand<ShowPortrait>(commandDict, portraitItemId, block, index + 1);
                            portraitCommand.m_Character.Value = character;
                            if (portraitCommand.m_Character.Value != null)
                            {
                                portraitCommand.m_PortraitIndex.Value = portraitCommand.m_Character.Value.GetPortraitIndex(portrait);
                            }
                            index = portraitCommand.CommandIndex;
                        }
                        #endregion
                    }

                    #region EDIT OR ADD SAY COMMAND
                    Say sayCommand = GetCommand<Say>(commandDict, itemId, block, index + 1);
                    sayCommand.m_Character.Value = character;
                    if (character == null)
                    {
                        sayCommand.m_NameText.Value = characterParam;
                    }
                    if (portrait != null)
                    {
                        sayCommand.m_Portrait.Value = portrait.m_Sprite;
                    }
                    sayCommand.m_DialogText.Value = storyText;
                    sayCommand.m_ContinuePrevious.Value = continuePrevious;
                    index = sayCommand.CommandIndex;
                    #endregion
                }
                else if (stringType == typeof(Choice).ToString())
                {
                    #region GET CHOICE COMMAND PARAMS
                    string[] commandParams = Regex.Split(item.m_BodyText + "\n", "(?<=\n)");
                    string targetBlockParam = "";
                    string textParam = commandParams[1];
                    // If portrait parenthetical exists, assign portraitParam and dialogParam accordingly.
                    if (commandParams[1].Trim().StartsWith("(", StringComparison.Ordinal) && commandParams[1].Trim().EndsWith(")", StringComparison.Ordinal))
                    {
                        targetBlockParam = commandParams[1].Replace("(", "").Replace(")", "");
                        textParam = commandParams[2];
                    }
                    #endregion

                    #region GET CHOICE COMMAND FIELDS
                    Block targetBlock = GetBlockByName(flowchart, targetBlockParam);
                    string text = "";
                    int textIndex = item.m_BodyText.IndexOf(textParam.Trim(), StringComparison.Ordinal);
                    if (textIndex >= 0)
                    {
                        text = item.m_BodyText.Substring(textIndex);
                    }
                    #endregion

                    #region EDIT OR ADD CHOICE COMMAND
                    Choice choiceCommand = GetCommand<Choice>(commandDict, itemId, block, index + 1);
                    choiceCommand.m_TargetBlock.Value = targetBlock;
                    choiceCommand.m_Text.Value = text;
                    index = choiceCommand.CommandIndex;
                    #endregion
                }
                else if (stringType == typeof(Comment).ToString())
                {
                    #region EDIT OR ADD CHOICE COMMAND
                    Comment commentCommand = GetCommand<Comment>(commandDict, itemId, block, index + 1);
                    commentCommand.m_CommentText = item.m_BodyText;
                    index = commentCommand.CommandIndex;
                    #endregion
                }
                else if (stringType == typeof(Call).ToString())
                {
                    #region EDIT OR ADD CALL COMMAND
                    Call callCommand = GetCommand<Call>(commandDict, itemId, block, index + 1);
                    callCommand.m_TargetBlock.Value = GetBlockByName(flowchart, item.m_BodyText);
                    index = callCommand.CommandIndex;
                    #endregion
                }
            }
        }

        protected static T GetCommand<T>(Dictionary<int, Command> commandDict, int itemId, Block block, int index) where T : Command
        {
            T command = null;

            // IF COMMAND EXISTS, STORE IT
            if (commandDict.ContainsKey(itemId))
            {
                command = commandDict[itemId] as T;
            }

            // IF COMMAND DOESN'T EXIST, ADD A NEW ONE AND STORE THAT
            if (command == null)
            {
                command = block.AddCommand(typeof(T), index) as T;
            }

            return command;
        }
        protected static Character GetCharacterByName(string name)
        {
            foreach (Character c in ResourceTracker.GetCachedPrefabObjectsOfType<Character>())
            {
                if (c.DisplayName.Trim().ToLower() == name.Trim().ToLower())
                {
                    return c;
                }
            }
            return null;
        }
        protected static Portrait GetPortraitByName(Character character, string name)
        {
            if (character != null)
            {
                foreach (Portrait p in character.Portraits)
                {
                    if (p.m_Sprite.name.Trim().ToLower() == name.Trim().ToLower())
                    {
                        return p;
                    }
                }
                foreach (Portrait p in character.Portraits)
                {
                    if (p.m_Sprite.name.Trim().ToLower().Contains(name.Trim().ToLower()))
                    {
                        return p;
                    }
                }
            }
            return null;
        }
        protected static Block GetBlockByName(Flowchart flowchart, string name)
        {
            Block block = null;
            foreach (Block b in flowchart.Blocks)
            {
                if (b.GetName().Trim().ToLower() == name.Trim().ToLower())
                {
                    block = flowchart.Blocks[flowchart.Blocks.IndexOf(b)];
                }
            }
            return block;
        }
#endif
    }
}
