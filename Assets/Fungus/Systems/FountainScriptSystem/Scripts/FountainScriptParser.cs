using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Fungus.Commands;
using System;

namespace Fungus
{
    public class FountainScriptParser
    {
        public static string s_TitlePage = @"(?i)^((?:title|credit|author[s]?|source|notes|draft date|date|contact|copyright):)";

        public static string s_SceneHeading = @"(?i)^((?:int|ext|est)\.)|^(?:\.(?!\.+))(.+)";
        public static string s_SceneNumber = @"( *#(.+)# *)";

        public static string s_Transition = @"^((?:FADE (?:TO BLACK|OUT)|CUT TO BLACK)\.|.+ TO\:)|^(?:> *)(.+)";

        public static string s_Character = @"^([0-9A-Z' \[\]\{\}\(\)]+)$";
        public static string s_CharacterSimultaneous = @"^([0-9A-Z' \[\]\{\}\(\)]+)(?:\^)$";
        public static string s_Parenthetical = @"^(\(.+\))";
        public static string s_Dialogue = "^(?<!\n\n)" + @"(?!\()" + "(?<=.\n).+$";
        
        public static string s_Action = @"^(.+)";
        public static string s_Centered = @"^(?:> *)(.+)(?: *<)(\n.+)*";
        
        public static string s_Section = @"^(#+)(?: *)(.*)/";
        public static string s_Synopsis = @"^(?:\=(?!\=+) *)(.*)";
        
        public static string s_Note = @"^(?:\[{2}(?!\[+))(.+)(?:\]{2}(?!\[+))$";
        public static string s_NoteInline = @"(?:\[{2}(?!\[+))([\s\S]+?)(?:\]{2}(?!\[+))";
        public static string s_Boneyard = @"(^\/\*|^\*\/)$";
        
        public static string s_PageBreak = @"^\={3,}$";
        public static string s_LineBreak = @"^ {2}$";

        public static string s_Emphasis = @"(_|\*{1,3}|_\*{1,3}|\*{1,3}_)(.+)(_|\*{1,3}|_\*{1,3}|\*{1,3}_)";
        public static string s_BoldItalicUnderline = @"(_{1}\*{3}(?=.+\*{3}_{1})|\*{3}_{1}(?=.+_{1}\*{3}))(.+?)(\*{3}_{1}|_{1}\*{3})";
        public static string s_BoldUnderline = @"(_{1}\*{2}(?=.+\*{2}_{1})|\*{2}_{1}(?=.+_{1}\*{2}))(.+?)(\*{2}_{1}|_{1}\*{2})";
        public static string s_ItalicUnderline = @"(?:_{1}\*{1}(?=.+\*{1}_{1})|\*{1}_{1}(?=.+_{1}\*{1}))(.+?)(\*{1}_{1}|_{1}\*{1})";
        public static string s_BoldItalic = @"(\*{3}(?=.+\*{3}))(.+?)(\*{3})";
        public static string s_Bold = @"(\*{2}(?=.+\*{2}))(.+?)(\*{2})";
        public static string s_Italic = @"(\*{1}(?=.+\*{1}))(.+?)(\*{1})";
        public static string s_Underline = @"(_{1}(?=.+_{1}))(.+?)(_{1})";

        public static string s_Splitter = @"\n{2,}";
        public static string s_Cleaner = @"^\n+|\n+$";
        public static string s_Standardizer = @"\r\n|\r";
        public static string s_Whitespacer = @"^\t+|^ {3,}";

        public class StringItem
        {
            public string[] m_Parameters;
            public string m_BodyText;
        }

        protected static string CleanScript(string script)
        {
            script = new Regex(FountainScriptParser.s_Boneyard).Replace(script,"");
            script = new Regex(FountainScriptParser.s_Standardizer).Replace(script,"\n");
            script = new Regex(FountainScriptParser.s_Cleaner).Replace(script,"");
            script = new Regex(FountainScriptParser.s_Whitespacer).Replace(script, "");
            return script;
        }
         
        public List<StringItem> ProcessText(string script)
        {
            List<StringItem> items = new List<StringItem>();
        
            string[] lines = script.Split(new string[] { "\r\n", "\n" }, System.StringSplitOptions.None);

            int i = 0;
            while(i < lines.Length)
            {
                //Debug.Log(lines[i]);
                string commandId = GetItemIdString(lines[i]);
                string commandBody = RemoveItemId(lines[i]);

                if (Regex.IsMatch(commandBody, FountainScriptParser.s_SceneHeading))
                {
                    commandId = typeof(Block) + ","  + commandId;
                    if(commandBody.StartsWith(".", StringComparison.Ordinal))
                    {
                        commandBody = commandBody.Substring(commandBody.IndexOf(".", StringComparison.Ordinal) +1).Trim();
                    }
                }
                else if(Regex.IsMatch(commandBody, FountainScriptParser.s_Transition))
                {
                    commandId = typeof(Call) + "," + commandId;
                    string targetBlock = "";
                    i++;
                    while(i < lines.Length)
                    {
                        if(Regex.IsMatch(lines[i], FountainScriptParser.s_SceneHeading))
                        {
                            targetBlock = RemoveItemId(lines[i]);
                            if(targetBlock.StartsWith(".", StringComparison.Ordinal))
                            {
                                targetBlock = targetBlock.Substring(targetBlock.IndexOf(".", StringComparison.Ordinal) +1).Trim();
                            }
                            break;
                        }
                        i++;
                    }
                    commandBody = targetBlock;
                }
                else if(Regex.IsMatch(commandBody, FountainScriptParser.s_Character) || Regex.IsMatch(commandBody, FountainScriptParser.s_CharacterSimultaneous))
                {
                    if (Regex.IsMatch(commandBody, FountainScriptParser.s_Character))
                    {
                        commandBody = Regex.Match(commandBody, FountainScriptParser.s_Character).Groups[1].Value.Trim();
                    }
                    else if (Regex.IsMatch(commandBody, FountainScriptParser.s_CharacterSimultaneous))
                    {
                        commandBody = Regex.Match(commandBody, FountainScriptParser.s_CharacterSimultaneous).Groups[1].Value.Trim();
                    }
                    if (lines[i - 1] == "" && lines[i + 1] != "")
                    {
                        if(commandBody.Trim().ToLower() == ("CHOICE").Trim().ToLower())
                        {
                            commandId = typeof(Choice) + "," + commandId;
                        }
                        else
                        {
                            commandId = typeof(Say) + "," + commandId;
                        }
                        i++;
                        while(i < lines.Length)
                        {
                            if(lines[i] == "") break;
                            commandBody += "\n" + lines[i];
                            i++;
                        }
                    }
                }
                else if(Regex.IsMatch(commandBody, FountainScriptParser.s_Note))
                {
                    commandId = typeof(Comment) + ","  + commandId;
                    commandBody = Regex.Match(commandBody, FountainScriptParser.s_Note).Groups[1].Value;
                    i++;
                }

                if(commandId != "")
                {
                    StringItem item = CreateItem(commandId, commandBody);
            
                    if(item != null)
                    {
                        items.Add(item);
                        string param = "";
                        foreach(string p in item.m_Parameters)
                        {
                            param += p + " ";
                        }
                    }
                }

                i++;
            }
            return items;
        }

        public static string GetItemIdString(string line)
        {
            return Regex.Match(line, @"\[\[\{\s*(.*?)\s*\}\]\]").Groups[1].Value;
        }

        public static string RemoveItemId(string line)
        {
            int removeIndex = line.IndexOf("[[{", StringComparison.Ordinal);
            if(removeIndex >= 0)
            {
                line = line.Remove(removeIndex).Trim();
            }
            return line;
        }

        public StringItem CreateItem(string commandInfo, string text)
        {
            string[] parameters = commandInfo.Split(new char[] { ',' });

            StringItem item = new StringItem();
            item.m_Parameters = parameters;
            item.m_BodyText = text;
            return item;
        }
    }
}
