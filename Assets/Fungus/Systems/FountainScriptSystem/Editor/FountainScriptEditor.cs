#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.IO;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus
{

    [CanEditMultipleObjects, CustomEditor(typeof(FountainScript), true)]
    public class FountainScriptEditor : PersistentBehaviourEditor
    {
        protected SerializedPropertyReorderableList m_localizedAssetGuidReorderableList;

        protected override void DrawProperty(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            FountainScript t = target as FountainScript;

            if (property.name == "m_" + nameof(t.ScriptFile))
            {
                Flowchart flowchart = t.GetComponent<Flowchart>();

                if (flowchart != null)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        base.DrawProperty(property, label);

                        if (t.ScriptFile == null)
                        {
                            GUIContent buttonLabel = new GUIContent("Create");
                            if (GUILayout.Button(buttonLabel, GUILayout.Width(GUI.skin.button.CalcSize(buttonLabel).x)))
                            {
                                ExportFountainScriptFile(t);
                            }
                        }
                        else
                        {
                            GUIContent buttonLabel = new GUIContent("Update");
                            if (GUILayout.Button(buttonLabel, GUILayout.Width(GUI.skin.button.CalcSize(buttonLabel).x)))
                            {
                                UpdateFountainScriptFile(t, AssetDatabase.GetAssetPath(t.ScriptFile));
                            }
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("No flowchart attached to this game object", MessageType.Error);
                }
            }
            else
            {
                base.DrawProperty(property, label);
            }
        }
        
        public virtual void UpdateFountainScriptFile(FountainScript fountainScript, string fountainScriptFilePath)
        {
            FountainScript t = target as FountainScript;

            Flowchart flowchart = t.GetComponent<Flowchart>();

            string path = fountainScriptFilePath;
            FountainScript.ImportStrings(t.ScriptFile.text, flowchart);
            string scriptData = FountainScript.ExportStrings(flowchart);
            try
            {
                File.WriteAllText(path, scriptData);

                AssetDatabase.ImportAsset(path);

                TextAsset textAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;
                if (textAsset != null)
                {
                    fountainScript.ScriptFile = textAsset;
                }

                string title = "Fountain Script Export";
                string message = "Exported Fountain Script to:\n\n" + path;
                EditorUtility.DisplayDialog(title, message, "OK");
            }
            catch (IOException)
            {
                string title = "Fountain Script Error";
                string message = "Cannot write to file while it is open. Please close the file before updating it:\n\n" + path;
                EditorUtility.DisplayDialog(title, message, "OK");
            }
        }

        public virtual void ExportFountainScriptFile(FountainScript fountainScript)
        {
            string path = EditorUtility.SaveFilePanelInProject("Export Fountain Script File",
                                                               fountainScript.gameObject.name + ".txt",
                                                               "txt",
                                                               "Please enter a filename to save the script file to");
            if (path.Length == 0)
            {
                return;
            }
            
            File.WriteAllText(path, "");

            AssetDatabase.ImportAsset(path);

            TextAsset textAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;
            if (textAsset != null)
            {
                fountainScript.ScriptFile = textAsset;
            }

            UpdateFountainScriptFile(fountainScript, path);
        }
    }

}

#endif