﻿using ImprobableStudios.ReflectionUtilities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class SaveState<T> : SaveState
    {
        public bool m_Initialized;

        public Dictionary<string, object> m_SavedMemberDictionary = new Dictionary<string, object>();

        public sealed override void Save(object obj)
        {
            Save((T)obj);
        }

        public sealed override void Load(object obj)
        {
            Load((T)obj);
        }

        public virtual void Save(T obj)
        {
            m_SavedMemberDictionary = obj.GetSerializableMemberValues(true);
            Expression<Func<T, object>>[] explicitlySavedProperties = ExplicitlySavedProperties();
            if (explicitlySavedProperties != null)
            {
                foreach (Expression<Func<T, object>> expr in explicitlySavedProperties)
                {
                    MemberInfo member = expr.GetMember();
                    if (member != null)
                    {
                        if (member.CanSerializeMember())
                        {
                            m_SavedMemberDictionary[expr.GetMemberPath()] = member.GetMemberValue(obj);
                        }
                    }
                }
            }
        }

        public virtual void Load(T obj)
        {
            obj.SetSerializableMemberValues(m_SavedMemberDictionary, false);
        }

        public virtual Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return null;
        }

        public override int LoadPriority()
        {
            return 0;
        }
    }

    [Serializable]
    public abstract class SaveState
    {
        public abstract void Save(object obj);

        public abstract void Load(object obj);

        public abstract int LoadPriority();
    }

}
