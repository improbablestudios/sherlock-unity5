﻿namespace ImprobableStudios.SaveSystem
{
    public interface ISavableSetting : ISavable
    {
    }

    public interface ISavable
    {
        string GetKey();
        SaveState GetSaveState();
    }

}
