﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using ImprobableStudios.SaveSystem.FullSerializer;
using ImprobableStudios.SaveSystem.FullSerializer.Internal;
using ImprobableStudios.ReflectionUtilities;

namespace ImprobableStudios.SaveSystem
{

    public static class FullSerializerReflectionExtensions
    {
        public static MemberInfo[] GetSerializableMembers(this Type type)
        {
            return GetSerializableMembers(type, false, new fsConfig());
        }

        public static MemberInfo[] GetSerializableMembers(this Type type, bool fsPropertyRequired)
        {
            return GetSerializableMembers(type, fsPropertyRequired, new fsConfig());
        }

        public static MemberInfo[] GetSerializableMembers(this Type type, bool fsPropertyRequired, fsConfig config)
        {
            List<MemberInfo> serializedMembers = new List<MemberInfo>();

            MemberInfo[] members = type.GetMembers();
            foreach (var member in members)
            {
                // We don't serialize members annotated with any of the ignore
                // serialize attributes
                if (config.IgnoreSerializeAttributes.Any(t => fsPortableReflection.HasAttribute(member, t)))
                {
                    continue;
                }

                PropertyInfo property = member as PropertyInfo;
                FieldInfo field = member as FieldInfo;

                // Early out if it's neither a field or a property, since we
                // don't serialize anything else.
                if (property == null && field == null)
                {
                    continue;
                }

                // Skip properties if we don't want them, to avoid the cost of
                // checking attributes.
                if (property != null && !config.EnablePropertySerialization)
                {
                    continue;
                }

                // If an opt-in annotation is required, then skip the property if
                // it doesn't have one of the serialize attributes
                if (fsPropertyRequired &&
                    !config.SerializeAttributes.Any(t => fsPortableReflection.HasAttribute(member, t)))
                {
                    continue;
                }

                // If an opt-out annotation is required, then skip the property
                // *only if* it has one of the not serialize attributes
                if (!fsPropertyRequired &&
                    config.IgnoreSerializeAttributes.Any(t => fsPortableReflection.HasAttribute(member, t)))
                {
                    continue;
                }

                if (CanSerializeMember(member, fsPropertyRequired, config))
                {
                    serializedMembers.Add(member);
                }
            }

            return serializedMembers.ToArray();
        }

        public static bool CanSerializeMember(this MemberInfo member)
        {
            return CanSerializeMember(member, false, new fsConfig());
        }

        public static bool CanSerializeMember(this MemberInfo member, bool fsPropertyRequired)
        {
            return CanSerializeMember(member, fsPropertyRequired, new fsConfig());
        }


        /// <summary>
        /// Returns if the given property should be serialized.
        /// </summary>
        /// <param name="fsPropertyRequired">
        /// Should a property without any annotations be serialized?
        /// </param>
        public static bool CanSerializeMember(this MemberInfo member, bool fsPropertyRequired, fsConfig config)
        {
            PropertyInfo property = member as PropertyInfo;
            FieldInfo field = member as FieldInfo;

            if (property != null)
            {
                if (CanSerializeProperty(property, fsPropertyRequired, config))
                {
                    return true;
                }
            }
            else if (field != null)
            {
                if (CanSerializeField(field, fsPropertyRequired, config))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns if the given property should be serialized.
        /// </summary>
        /// <param name="fsPropertyRequired">
        /// Should a property without any annotations be serialized?
        /// </param>
        private static bool CanSerializeProperty(this PropertyInfo property, bool fsPropertyRequired, fsConfig config)
        {
            // We don't serialize delegates
            if (typeof(Delegate).IsAssignableFrom(property.PropertyType))
            {
                return false;
            }

            // We don't serialize obsolete properties.
            if (property.IsDefined(typeof(ObsoleteAttribute), true))
            {
                return false;
            }

            var publicGetMethod = property.GetGetMethod(/*nonPublic:*/ false);
            var publicSetMethod = property.GetSetMethod(/*nonPublic:*/ false);

            // We do not bother to serialize static fields.
            if ((publicGetMethod != null && publicGetMethod.IsStatic) ||
                (publicSetMethod != null && publicSetMethod.IsStatic))
            {
                return false;
            }

            // Never serialize indexers. I can't think of a sane way to
            // serialize/deserialize them, and they're normally wrappers around
            // other fields anyway...
            if (property.GetIndexParameters().Length > 0)
            {
                return false;
            }

            // If a property is annotated with one of the serializable
            // attributes, then it should it should definitely be serialized.
            //
            // NOTE: We place this override check *after* the static check,
            //       because we *never* allow statics to be serialized.
            if (config.SerializeAttributes.Any(t => fsPortableReflection.HasAttribute(property, t)))
            {
                return true;
            }

            // If the property cannot be both read and written to, we are not
            // going to serialize it regardless of the default serialization mode
            if (property.CanRead == false || property.CanWrite == false)
            {
                return false;
            }

            // Depending on the configuration options, check whether the property
            // is automatic and if it has a public setter to determine whether it
            // should be serialized
            if ((publicGetMethod != null && (config.SerializeNonPublicSetProperties || publicSetMethod != null)) &&
                (config.SerializeNonAutoProperties || IsAutoProperty(property)))
            {
                return true;
            }

            // Otherwise, we don't bother with serialization
            return !fsPropertyRequired;
        }

        private static bool IsAutoProperty(PropertyInfo property)
        {
            return
                property.CanWrite && property.CanRead &&
                fsPortableReflection.HasAttribute(
                        property.GetGetMethod(), typeof(CompilerGeneratedAttribute), /*shouldCache:*/false);
        }

        private static bool CanSerializeField(this FieldInfo field, bool fsPropertyRequired, fsConfig config)
        {
            // We don't serialize delegates
            if (typeof(Delegate).IsAssignableFrom(field.FieldType))
            {
                return false;
            }

            // We don't serialize obsolete fields.
            if (field.IsDefined(typeof(ObsoleteAttribute), true))
            {
                return false;
            }

            // We don't serialize compiler generated fields.
            if (field.IsDefined(typeof(CompilerGeneratedAttribute), false))
            {
                return false;
            }

            // We don't serialize static fields
            if (field.IsStatic)
            {
                return false;
            }

            // We want to serialize any fields annotated with one of the
            // serialize attributes.
            //
            // NOTE: This occurs *after* the static check, because we *never*
            //       want to serialize static fields.
            if (config.SerializeAttributes.Any(t => fsPortableReflection.HasAttribute(field, t)))
            {
                return true;
            }
            
            if (fsPropertyRequired && field.IsPrivate)
            {
                return false;
            }

            return true;
        }

        public static Dictionary<string, object> GetSerializableMemberValues(this object obj, bool fsPropertyRequired)
        {
            Dictionary<string, object> serializedMemberDictionary = new Dictionary<string, object>();

            Type type = obj.GetType();

            foreach (MemberInfo member in GetSerializableMembers(type, fsPropertyRequired))
            {
                serializedMemberDictionary[member.Name] = member.GetMemberValue(obj);
            }

            return serializedMemberDictionary;
        }

        public static void SetSerializableMemberValues(this object obj, Dictionary<string, object> serializedMemberDictionary, bool fsPropertyRequired)
        {
            Type type = obj.GetType();

            foreach (MemberInfo member in GetSerializableMembers(type, fsPropertyRequired))
            {
                if (serializedMemberDictionary.ContainsKey(member.Name))
                {
                    object value = serializedMemberDictionary[member.Name];
                    member.SetMemberValue(obj, value);
                }
            }
        }
        
        public static void PerformActionOnAllSerializableMembersThatMeetCondition(this object obj, MemberCondition memberCondition, MemberAction memberAction, GetMemberKey getMemberKey, TypeCondition isReflectableType, bool performMemberActionRecursively)
        {
            obj.PerformActionOnAllMembersThatMeetCondition(memberCondition, memberAction, getMemberKey, GetSerializableMembers, isReflectableType, performMemberActionRecursively);
        }
        
        public static string[] GetSerializableMemberPathsRecursive(this object obj, GetMemberKey getMemberKey, TypeCondition isReflectableType, Type memberType = null)
        {
            return GetSerializableSerializedMemberRecursive(obj, getMemberKey, isReflectableType, memberType).Keys.ToArray();
        }

        public static SerializedMember GetSerializableSerializedMember(this object obj, string memberPath, GetMemberKey getMemberKey, TypeCondition isReflectableType, Type memberType = null)
        {
            Dictionary<string, SerializedMember> dictionary = GetSerializableSerializedMemberRecursive(obj, getMemberKey, isReflectableType);
            if (dictionary.ContainsKey(memberPath))
            {
                return dictionary[memberPath];
            }

            return null;
        }

        private static Dictionary<string, SerializedMember> GetSerializableSerializedMemberRecursive(this object obj, GetMemberKey getMemberKey, TypeCondition isReflectableType, Type memberType = null)
        {
            Dictionary<string, SerializedMember> dictionary = new Dictionary<string, SerializedMember>();

            MemberCondition memberCondition = null;
            if (memberType != null)
            {
                memberCondition = (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => memberType == null || memberType.IsAssignableFrom(mt);
            }

            PerformActionOnAllSerializableMembersThatMeetCondition(
                obj,
                memberCondition,
                (int md, string mp, bool hc, MemberInfo m, Type mt, object d, object[] p, MemberInfo dm) => GetSerializableSerializedMemberRecursiveInternal(md, mp, hc, m, mt, d, p, dm, dictionary),
                getMemberKey,
                isReflectableType,
                true);

            return dictionary;
        }

        private static void GetSerializableSerializedMemberRecursiveInternal(int memberDepth, string memberPath, bool hasChildren, MemberInfo member, Type memberType, object declaringObj, object[] propertyParameters, MemberInfo declaringMember, Dictionary<string, SerializedMember> dictionary)
        {
            if (member != null)
            {
                dictionary[memberPath] = new SerializedMember(member, declaringObj, propertyParameters, memberDepth, memberPath, member.Name, hasChildren, false);
            }
        }
    }

}