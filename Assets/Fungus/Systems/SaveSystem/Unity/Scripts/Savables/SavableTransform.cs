﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Linq.Expressions;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class TransformSaveState : TransformSaveState<Transform>
    {
    }

    [RequireComponent(typeof(Transform))]
    [AddComponentMenu("Save System/Savable Transform")]
    public class SavableTransform : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableTransformSaveState : BehaviourSaveState<SavableTransform>
        {
            public override void Save(SavableTransform savableTransform)
            {
                savableTransform.m_saveState.Save(savableTransform.GetComponent<Transform>());

                base.Save(savableTransform);
            }

            public override void Load(SavableTransform savableTransform)
            {
                savableTransform.m_saveState.Load(savableTransform.GetComponent<Transform>());

                base.Load(savableTransform);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected TransformSaveState m_saveState = new TransformSaveState();

        public virtual SaveState GetSaveState()
        {
            return new SavableTransformSaveState();
        }
    }

}