﻿using UnityEngine;
using System;
using DG.Tweening;
using System.Linq.Expressions;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class CameraSaveState : BehaviourSaveState<Camera>
    {
        public override Expression<Func<Camera, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<Camera, object>>[]
            {
                x => x.clearFlags,
                x => x.backgroundColor,
                x => x.rect,
                x => x.nearClipPlane,
                x => x.farClipPlane,
                x => x.fieldOfView,
                x => x.orthographic,
                x => x.orthographicSize,
                x => x.depth,
                x => x.cullingMask,
                x => x.renderingPath,
                x => x.targetTexture,
                x => x.targetDisplay,
                x => x.stereoTargetEye,
                x => x.allowHDR,
                x => x.useOcclusionCulling,
                x => x.stereoConvergence,
                x => x.stereoSeparation,
            };
        }

        public override int LoadPriority()
        {
            return 3;
        }
    }

    [RequireComponent(typeof(Camera))]
    [AddComponentMenu("Save System/Savable Camera")]
    public class SavableCamera : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableCameraSaveState : BehaviourSaveState<SavableCamera>
        {
            public override void Save(SavableCamera savableCamera)
            {
                savableCamera.m_saveState.Save(savableCamera.GetComponent<Camera>());

                base.Save(savableCamera);
            }

            public override void Load(SavableCamera savableCamera)
            {
                savableCamera.m_saveState.Load(savableCamera.GetComponent<Camera>());

                base.Load(savableCamera);
            }
        }
        
        protected CameraSaveState m_saveState = new CameraSaveState();

        public SaveState GetSaveState()
        {
            return new SavableCameraSaveState();
        }

    }

}