﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Linq.Expressions;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class CanvasGroupSaveState : ComponentSaveState<CanvasGroup>
    {
        public override Expression<Func<CanvasGroup, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<CanvasGroup, object>>[]
            {
                x => x.alpha,
                x => x.interactable,
                x => x.blocksRaycasts,
                x => x.ignoreParentGroups,
            };
        }
    }

    [RequireComponent(typeof(CanvasGroup))]
    [AddComponentMenu("Save System/Savable Canvas Group")]
    public class SavableCanvasGroup : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableCanvasGroupSaveState : BehaviourSaveState<SavableCanvasGroup>
        {
            public override void Save(SavableCanvasGroup savableCanvasGroup)
            {
                savableCanvasGroup.m_saveState.Save(savableCanvasGroup.GetComponent<CanvasGroup>());

                base.Save(savableCanvasGroup);
            }

            public override void Load(SavableCanvasGroup savableCanvasGroup)
            {
                savableCanvasGroup.m_saveState.Load(savableCanvasGroup.GetComponent<CanvasGroup>());

                base.Load(savableCanvasGroup);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected CanvasGroupSaveState m_saveState = new CanvasGroupSaveState();
        
        public SaveState GetSaveState()
        {
            return new SavableCanvasGroupSaveState();
        }
    }

}