﻿using UnityEngine;
using System;
using System.Linq.Expressions;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class SpriteRendererSaveState : RendererSaveState<SpriteRenderer>
    {
        public override Expression<Func<SpriteRenderer, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<SpriteRenderer, object>>[]
            {
                x => x.shadowCastingMode,
                x => x.receiveShadows,
                x => x.motionVectorGenerationMode,
                x => x.reflectionProbeUsage,
                x => x.materials,
                x => x.lightProbeProxyVolumeOverride,
                x => x.lightmapIndex,
                x => x.lightmapScaleOffset,
                x => x.lightProbeUsage,
                x => x.probeAnchor,
                x => x.sortingLayerID,
                x => x.sortingOrder,
                x => x.sprite,
                x => x.color,
                x => x.flipX,
                x => x.flipY,
            };
        }

        public override int LoadPriority()
        {
            return 3;
        }
    }


    [RequireComponent(typeof(SpriteRenderer))]
    [AddComponentMenu("Save System/Savable Sprite Renderer")]
    public class SavableSpriteRenderer : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableSpriteRendererSaveState : BehaviourSaveState<SavableSpriteRenderer>
        {
            public override void Save(SavableSpriteRenderer savableSpriteRenderer)
            {
                savableSpriteRenderer.m_saveState.Save(savableSpriteRenderer.GetComponent<SpriteRenderer>());

                base.Save(savableSpriteRenderer);
            }

            public override void Load(SavableSpriteRenderer savableSpriteRenderer)
            {
                savableSpriteRenderer.m_saveState.Load(savableSpriteRenderer.GetComponent<SpriteRenderer>());

                base.Load(savableSpriteRenderer);
            }
        }
        
        protected SpriteRendererSaveState m_saveState = new SpriteRendererSaveState();

        public SaveState GetSaveState()
        {
            return new SpriteRendererSaveState();
        }
    }

}