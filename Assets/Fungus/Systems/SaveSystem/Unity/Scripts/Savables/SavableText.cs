﻿using System;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class TextSaveState : MaskableGraphicSaveState<Text>
    {
        public override Expression<Func<Text, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<Text, object>>[]
            {
                x => x.font,
                x => x.fontSize,
                x => x.fontStyle,
                x => x.resizeTextForBestFit,
                x => x.resizeTextMinSize,
                x => x.resizeTextMaxSize,
                x => x.alignment,
                x => x.alignByGeometry,
                x => x.supportRichText,
                x => x.horizontalOverflow,
                x => x.verticalOverflow,
                x => x.lineSpacing,
                x => x.text,
            };
        }

        public override int LoadPriority()
        {
            return 3;
        }
    }

    [RequireComponent(typeof(Text))]
    [AddComponentMenu("Save System/Savable Text")]
    public class SavableText : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableTextSaveState : BehaviourSaveState<SavableText>
        {
            public override void Save(SavableText savableText)
            {
                savableText.m_saveState.Save(savableText.GetComponent<Text>());

                base.Save(savableText);
            }

            public override void Load(SavableText savableText)
            {
                savableText.m_saveState.Load(savableText.GetComponent<Text>());

                base.Load(savableText);
            }
        }
        
        protected TextSaveState m_saveState = new TextSaveState();
        
        public SaveState GetSaveState()
        {
            return new SavableTextSaveState();
        }
    }

}