﻿using ImprobableStudios.BaseUtilities;
using System;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{
    [Serializable]
    public class AnimatorSaveState : BehaviourSaveState<Animator>
    {
        [Serializable]
        public class AnimatorLayerInfo
        {
            public int m_Index;
            public float m_Weight;
            public int m_CurrentStateShortNameHash;
            public float m_CurrentStateNormalizedTime;

            public AnimatorLayerInfo(Animator animator, int layerIndex)
            {
                this.m_Index = layerIndex;
                this.m_Weight = animator.GetLayerWeight(layerIndex);

                AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(layerIndex);
                this.m_CurrentStateShortNameHash = currentState.shortNameHash;
                this.m_CurrentStateNormalizedTime = currentState.normalizedTime;
            }
        }

        [Serializable]
        public class AnimatorControllerParameterInfo
        {
            public AnimatorControllerParameterType m_Type;
            public int m_NameHash;
            public object m_Value;

            public AnimatorControllerParameterInfo(Animator animator, int parameterIndex)
            {
                AnimatorControllerParameter parameter = animator.parameters[parameterIndex];

                m_Type = parameter.type;
                m_NameHash = parameter.nameHash;
                m_Value = GetParameterValue(animator, parameter.nameHash, parameter.type);
            }

            private object GetParameterValue(Animator animator, int parameterNameHash, AnimatorControllerParameterType parameterType)
            {
                switch (parameterType)
                {
                    case AnimatorControllerParameterType.Float:
                        return animator.GetFloat(parameterNameHash);
                    case AnimatorControllerParameterType.Int:
                        return animator.GetInteger(parameterNameHash);
                    case AnimatorControllerParameterType.Bool:
                        return animator.GetBool(parameterNameHash);
                    case AnimatorControllerParameterType.Trigger:
                        return animator.GetBool(parameterNameHash);
                    default:
                        return null;
                }
            }
        }

        public AnimatorLayerInfo[] m_LayerData;
        public AnimatorControllerParameterInfo[] m_ParameterData;

        public override void Save(Animator animator)
        {
            base.Save(animator);

            // Store the current state for each layer
            m_LayerData = new AnimatorLayerInfo[animator.layerCount];
            for (int i = 0; i < animator.layerCount; i++)
            {
                m_LayerData[i] = new AnimatorLayerInfo(animator, i);
            }

            // Store every parameter
            m_ParameterData = new AnimatorControllerParameterInfo[animator.parameterCount];
            for (int i = 0; i < animator.parameterCount; i++)
            {
                m_ParameterData[i] = new AnimatorControllerParameterInfo(animator, i);
            }
        }

        public override void Load(Animator animator)
        {
            base.Load(animator);

            // Restore the states of each layer
            foreach (AnimatorLayerInfo layer in m_LayerData)
            {
                animator.Play(layer.m_CurrentStateShortNameHash, layer.m_Index, layer.m_CurrentStateNormalizedTime);
                animator.SetLayerWeight(layer.m_Index, layer.m_Weight);
            }

            // Restore the parameters of the animator
            foreach (AnimatorControllerParameterInfo parameter in m_ParameterData)
            {
                switch (parameter.m_Type)
                {
                    case AnimatorControllerParameterType.Float:
                        animator.SetFloat(parameter.m_NameHash, (float)parameter.m_Value);
                        break;
                    case AnimatorControllerParameterType.Int:
                        animator.SetInteger(parameter.m_NameHash, (int)parameter.m_Value);
                        break;
                    case AnimatorControllerParameterType.Bool:
                        animator.SetBool(parameter.m_NameHash, (bool)parameter.m_Value);
                        break;
                    case AnimatorControllerParameterType.Trigger:
                        if ((bool)parameter.m_Value)
                        {
                            animator.SetTrigger(parameter.m_NameHash);
                        }
                        else
                        {
                            animator.ResetTrigger(parameter.m_NameHash);
                        }
                        break;
                }
            }
        }

        public override int LoadPriority()
        {
            return 3;
        }
    }

    [RequireComponent(typeof(Animator))]
    [AddComponentMenu("Save System/Savable Animator")]
    public class SavableAnimator : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableAnimatorSaveState : BehaviourSaveState<SavableAnimator>
        {
            public override void Save(SavableAnimator savableAnimator)
            {
                savableAnimator.m_saveState.Save(savableAnimator.GetComponent<Animator>());

                base.Save(savableAnimator);
            }

            public override void Load(SavableAnimator savableAnimator)
            {
                savableAnimator.m_saveState.Load(savableAnimator.GetComponent<Animator>());

                base.Load(savableAnimator);
            }
        }
        
        protected AnimatorSaveState m_saveState = new AnimatorSaveState();
        
        public SaveState GetSaveState()
        {
            return new SavableAnimatorSaveState();
        }
    }
}