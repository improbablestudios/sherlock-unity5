﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class ImageSaveState : MaskableGraphicSaveState<Image>
    {
        public override Expression<Func<Image, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<Image, object>>[]
            {
                x => x.onCullStateChanged,
                x => x.sprite,
                x => x.type,
                x => x.preserveAspect,
                x => x.fillCenter,
                x => x.fillMethod,
                x => x.fillAmount,
                x => x.fillClockwise,
                x => x.fillOrigin,
            };
        }
    }

    [RequireComponent(typeof(Image))]
    [AddComponentMenu("Save System/Savable Image")]
    public class SavableImage : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableImageSaveState : BehaviourSaveState<SavableImage>
        {
            public override void Save(SavableImage savableImage)
            {
                savableImage.m_saveState.Save(savableImage.GetComponent<Image>());

                base.Save(savableImage);
            }

            public override void Load(SavableImage savableImage)
            {
                savableImage.m_saveState.Load(savableImage.GetComponent<Image>());

                base.Load(savableImage);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected ImageSaveState m_saveState = new ImageSaveState();
        
        public SaveState GetSaveState()
        {
            return new SavableImageSaveState();
        }
    }

}