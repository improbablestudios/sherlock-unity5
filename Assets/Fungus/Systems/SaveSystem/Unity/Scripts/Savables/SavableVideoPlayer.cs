﻿using UnityEngine;
using System;
using System.Linq.Expressions;
using UnityEngine.Video;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class VideoPlayerSaveState : BehaviourSaveState<VideoPlayer>
    {
        public override Expression<Func<VideoPlayer, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<VideoPlayer, object>>[]
            {
                x => x.aspectRatio,
                x => x.audioOutputMode,
                x => x.clip,
                x => x.controlledAudioTrackCount,
                x => x.playbackSpeed,
                x => x.playOnAwake,
                x => x.renderMode,
                x => x.sendFrameReadyEvents,
                x => x.skipOnDrop,
                x => x.source,
                x => x.targetCamera,
                x => x.targetCameraAlpha,
                x => x.targetMaterialProperty,
                x => x.targetMaterialRenderer,
                x => x.targetTexture,
                x => x.time,
                x => x.timeSource,
                x => x.url,
                x => x.waitForFirstFrame,
            };
        }
    }

    [RequireComponent(typeof(VideoPlayer))]
    [AddComponentMenu("Save System/Savable Video Player")]
    public class SavableVideoPlayer : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableVideoPlayerSaveState : BehaviourSaveState<SavableVideoPlayer>
        {
            public override void Save(SavableVideoPlayer savableVideoPlayer)
            {
                savableVideoPlayer.m_saveState.Save(savableVideoPlayer.GetComponent<VideoPlayer>());

                base.Save(savableVideoPlayer);

                savableVideoPlayer.m_isPlaying = savableVideoPlayer.GetComponent<VideoPlayer>().isPlaying;
            }

            public override void Load(SavableVideoPlayer savableVideoPlayer)
            {
                savableVideoPlayer.m_saveState.Load(savableVideoPlayer.GetComponent<VideoPlayer>());

                base.Load(savableVideoPlayer);

                if (savableVideoPlayer.m_isPlaying)
                {
                    savableVideoPlayer.GetComponent<VideoPlayer>().Play();
                }
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected bool m_isPlaying;

        protected VideoPlayerSaveState m_saveState = new VideoPlayerSaveState();
        
        public virtual SaveState GetSaveState()
        {
            return new SavableVideoPlayerSaveState();
        }
    }

}