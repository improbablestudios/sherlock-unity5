using UnityEngine;
using System;
using System.Linq.Expressions;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class AudioSourceSaveState : BehaviourSaveState<AudioSource>
    {
        public override Expression<Func<AudioSource, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<AudioSource, object>>[]
            {
                x => x.outputAudioMixerGroup,
                x => x.clip,
                x => x.playOnAwake,
                x => x.volume,
                x => x.pitch,
                x => x.loop,
                x => x.mute,
                x => x.spatialize,
                x => x.spatializePostEffects,
                x => x.priority,
                x => x.dopplerLevel,
                x => x.minDistance,
                x => x.maxDistance,
                x => x.panStereo,
                x => x.rolloffMode,
                x => x.bypassEffects,
                x => x.bypassListenerEffects,
                x => x.spatialBlend,
                x => x.spread,
                x => x.reverbZoneMix,
                x => x.mute,
                x => x.ignoreListenerPause,
                x => x.ignoreListenerVolume,
                x => x.timeSamples,
                x => x.velocityUpdateMode,

            };
        }
    }

    [RequireComponent(typeof(AudioSource))]
    [AddComponentMenu("Save System/Savable Audio Source")]
    public class SavableAudioSource : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableAudioSourceSaveState : BehaviourSaveState<SavableAudioSource>
        {
            public override void Save(SavableAudioSource savableAudioSource)
            {
                savableAudioSource.m_saveState.Save(savableAudioSource.GetComponent<AudioSource>());

                base.Save(savableAudioSource);

                savableAudioSource.m_isPlaying = savableAudioSource.GetComponent<AudioSource>().isPlaying;
            }

            public override void Load(SavableAudioSource savableAudioSource)
            {
                savableAudioSource.m_saveState.Load(savableAudioSource.GetComponent<AudioSource>());

                base.Load(savableAudioSource);

                if (savableAudioSource.m_isPlaying)
                {
                    savableAudioSource.GetComponent<AudioSource>().Play();
                }
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected bool m_isPlaying;
        
        protected AudioSourceSaveState m_saveState = new AudioSourceSaveState();

        public virtual SaveState GetSaveState()
        {
            return new SavableAudioSourceSaveState();
        }
    }
    
}