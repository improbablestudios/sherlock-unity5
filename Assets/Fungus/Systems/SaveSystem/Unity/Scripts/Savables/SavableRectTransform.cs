﻿using ImprobableStudios.BaseUtilities;
using System;
using System.Linq.Expressions;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class RectTransformSaveState : TransformSaveState<RectTransform>
    {
        public override Expression<Func<RectTransform, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<RectTransform, object>>[]
            {
                x => x.anchorMin,
                x => x.anchorMax,
                x => x.anchoredPosition,
                x => x.sizeDelta,
                x => x.pivot,
            };
        }
    }

    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("Save System/Savable Rect Transform")]
    public class SavableRectTransform : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableRectTransformSaveState : BehaviourSaveState<SavableRectTransform>
        {
            public override void Save(SavableRectTransform savableRectTransform)
            {
                savableRectTransform.m_saveState.Save(savableRectTransform.GetComponent<RectTransform>());

                base.Save(savableRectTransform);
            }

            public override void Load(SavableRectTransform savableRectTransform)
            {
                savableRectTransform.m_saveState.Load(savableRectTransform.GetComponent<RectTransform>());

                base.Load(savableRectTransform);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected RectTransformSaveState m_saveState = new RectTransformSaveState();

        public virtual SaveState GetSaveState()
        {
            return new RectTransformSaveState();
        }
    }

}