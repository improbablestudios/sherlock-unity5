﻿using System;
using ImprobableStudios.SaveSystem.FullSerializer;

namespace ImprobableStudios.SaveSystem
{

    public static class SaveSystemSerializer
    {
        private static fsSerializer s_serializer;

        private static fsSerializer Serializer
        {
            get
            {
                if (s_serializer == null)
                {
                    s_serializer = new fsSerializer();
                    s_serializer.AddConverter(new UnityObjectConverter());
                }
                return s_serializer;
            }
        }
        
        public static string Serialize(Type type, object value)
        {
            // serialize the data
            fsData data;
            Serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();

            // emit the data via JSON
            return fsJsonPrinter.CompressedJson(data);
        }

        public static object Deserialize(Type type, string serializedState)
        {
            // step 1: parse the JSON data
            fsData data = fsJsonParser.Parse(serializedState);

            // step 2: deserialize the data
            object deserialized = null;
            Serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

            return deserialized;
        }
    }

}
