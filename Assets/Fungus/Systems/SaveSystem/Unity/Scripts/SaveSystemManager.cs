﻿using ImprobableStudios.SaveSystem.FullSerializer;
using ImprobableStudios.BaseUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace ImprobableStudios.SaveSystem
{

    public class SaveSystemManager : SettingsScriptableObject<SaveSystemManager>, ISavable
    {
        public enum SerializationType
        {
            Json,
            Binary
        }

        [Serializable]
        public class SaveSystemManagerSaveState : ScriptableObjectSaveState<SaveSystemManager>
        {
        }
        
        private static bool s_isLoading;

        private static string s_loadingSaveFileName = "";

        [Tooltip("The name of the autosave file")]
        [SerializeField]
        [FormerlySerializedAs("autosaveFileName")]
        protected string m_AutosaveFileName = "autosave";

        [Tooltip("The name of the options file")]
        [SerializeField]
        [FormerlySerializedAs("optionsFileName")]
        protected string m_SettingsFileName = "settings";

        [Tooltip("The name of the temp file")]
        [SerializeField]
        [FormerlySerializedAs("tempFileName")]
        protected string m_TempFileName = "temp";

        [Tooltip("If true, serialize save data in a non-human-readable binary file, otherwise, serialize save data in a human-readable json file")]
        [SerializeField]
        [FormerlySerializedAs("binarySerialization")]
        protected SerializationType m_Serialization;
        
        protected string m_sceneName = "";

        private Dictionary<string, ISavable> m_savablesInCode = new Dictionary<string, ISavable>();

        private Dictionary<string, ISavable> m_savablesInScene = new Dictionary<string, ISavable>();

        private static Dictionary<string, SaveState> m_SaveData = new Dictionary<string, SaveState>();

        private static Dictionary<string, SaveState> m_SettingsData = new Dictionary<string, SaveState>();
        
        public static bool IsLoading
        {
            get
            {
                return s_isLoading;
            }
            set
            {
                s_isLoading = value;
            }
        }

        public static string LoadingSaveFileName
        {
            get
            {
                return s_loadingSaveFileName;
            }
            set
            {
                s_loadingSaveFileName = value;
            }
        }

        public string AutosaveFileName
        {
            get
            {
                return m_AutosaveFileName;
            }
            set
            {
                m_AutosaveFileName = value;
            }
        }

        public string SettingsFileName
        {
            get
            {
                return m_SettingsFileName;
            }
            set
            {
                m_SettingsFileName = value;
            }
        }

        public string TempFileName
        {
            get
            {
                return m_TempFileName;
            }
            set
            {
                m_TempFileName = value;
            }
        }

        public SerializationType Serialization
        {
            get
            {
                return m_Serialization;
            }
            set
            {
                m_Serialization = value;
            }
        }

        public Dictionary<string, ISavable> SavablesInCode
        {
            get
            {
                return m_savablesInCode;
            }
        }

        public Dictionary<string, ISavable> SavablesInScene
        {
            get
            {
                return m_savablesInScene;
            }
        }

        public virtual SaveState GetSaveState()
        {
            return new SaveSystemManagerSaveState();
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            RegisterSavablesInScene();
            RegisterSavablesInCode();
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();

            SceneManager.sceneLoaded -= OnSceneWasLoaded;
        }

        public void RegisterSavableInCode(ISavable savable)
        {
            if (!m_savablesInCode.ContainsKey(savable.GetKey()))
            {
                m_savablesInCode.Add(savable.GetKey(), savable);
            }
        }

        public void RegisterSavableInScene(ISavable savable)
        {
            if (!m_savablesInScene.ContainsKey(savable.GetKey()))
            {
                m_savablesInScene.Add(savable.GetKey(), savable);
            }
        }

        public void RegisterSavablesInCode()
        {
            foreach (Type savableType in typeof(ISavable).FindAllDerivedTypes())
            {
                if (!typeof(UnityEngine.Object).IsAssignableFrom(savableType) && savableType.IsInstantiable() && !savableType.IsInterface)
                {
                    RegisterSavableInCode(Activator.CreateInstance(savableType) as ISavable);
                }
            }
        }

        public void RegisterSavablesInScene()
        {
            if (m_savablesInScene != null)
            {
                m_savablesInScene.Clear();
                foreach (ISavable savable in ResourceTracker.GetCachedInstanceObjectsOfType<ISavable>())
                {
                    if ((savable as UnityEngine.Object) != null)
                    {
                        RegisterSavableInScene(savable);
                    }
                }
            }
        }

        public void ReloadScene()
        {
            Save(m_TempFileName);
            Load(m_TempFileName);
            string tempFilePath = Path.Combine(Application.persistentDataPath, GetFileNameWithExtension(m_TempFileName));
            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
        }

        public virtual void SaveSettings()
        {
            RegisterSavablesInScene();

            SaveAll<ISavableSetting>(m_SettingsData);
            CreateSaveFile(m_SettingsFileName, m_SettingsData);
        }

        public virtual void Save(string saveFileName)
        {
            RegisterSavablesInScene();

            m_SaveData.Clear();
            m_sceneName = SceneManager.GetActiveScene().name;
            
            SaveAll<ISavableSetting>(m_SettingsData);
            SaveAll<object>(m_SaveData);

            CreateSaveFile(m_SettingsFileName, m_SettingsData);
            CreateSaveFile(saveFileName, m_SaveData);
        }

        public virtual void Load(string saveFileName)
        {
            IsLoading = true;

            // Deserialize to get name of scene to load
            m_SaveData = LoadSaveFile(saveFileName);

            if (m_SaveData != null)
            {
                s_loadingSaveFileName = saveFileName;
                
                if (m_SaveData.ContainsKey(GetKey()))
                {
                    SaveSystemManagerSaveState state = (SaveSystemManagerSaveState)m_SaveData[GetKey()];
                    
                    if (state != null)
                    {
                        state.Load(this);
                        SceneManager.LoadScene(m_sceneName);
                    }
                }
            }
        }

        void OnSceneWasLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            RegisterSavablesInScene();
            
            m_SettingsData = LoadSaveFile(m_SettingsFileName);
            LoadAll<ISavableSetting>(m_SettingsData);

            if (s_isLoading)
            {
                // Deserialize to replace all PersistentBehaviour references from the old scene with references in the new scene
                m_SaveData = LoadSaveFile(s_loadingSaveFileName);
                LoadAll<object>(m_SaveData);
            }

            s_loadingSaveFileName = "";

            PerformActionAtEndOfFrame(
                () =>
                {
                    s_isLoading = false;
                });
        }

        private IEnumerator PerformActionAtEndOfFrame(UnityAction action)
        {
            yield return new WaitForEndOfFrame();

            if (action != null)
            {
                action();
            }
        }

        protected void CreateSaveFile(string saveFileName, Dictionary<string, SaveState> stateData)
        {
            if (saveFileName != "")
            {
                string serializedJSON = SaveSystemSerializer.Serialize(typeof(Dictionary<string, SaveState>), stateData);
                
                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    PlayerPrefs.SetString(saveFileName, serializedJSON);
                    PlayerPrefs.Save();
                }
                else
                {
                    string saveFilePath = Application.persistentDataPath + "/" + GetFileNameWithExtension(saveFileName);
                    if (m_Serialization == SerializationType.Binary)
                    {
                        FileStream saveFile = File.Create(saveFilePath);
                        (new BinaryFormatter()).Serialize(saveFile, serializedJSON);
                        saveFile.Close();
                    }
                    else if (m_Serialization == SerializationType.Json)
                    {
                        File.WriteAllText(saveFilePath, serializedJSON);
                    }
                }
            }
        }

        public Dictionary<string, SaveState> LoadSaveFile(string saveFileName)
        {
            if (saveFileName != "")
            {
                string serializedJSON = null;
                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    if (PlayerPrefs.HasKey(saveFileName))
                    {
                        serializedJSON = PlayerPrefs.GetString(saveFileName);
                    }
                }
                else
                {
                    string saveFilePath = Path.Combine(Application.persistentDataPath, GetFileNameWithExtension(saveFileName));
                    if (File.Exists(saveFilePath))
                    {
                        if (m_Serialization == SerializationType.Binary)
                        {
                            FileStream saveFile = File.Open(saveFilePath, FileMode.Open);
                            serializedJSON = (new BinaryFormatter()).Deserialize(saveFile) as string;
                            saveFile.Close();
                        }
                        else if (m_Serialization == SerializationType.Json)
                        {
                            serializedJSON = File.ReadAllText(saveFilePath);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(serializedJSON))
                {
                    return SaveSystemSerializer.Deserialize(typeof(Dictionary<string, SaveState>), serializedJSON) as Dictionary<string, SaveState>;
                }
            }

            return null;
        }

        protected string GetFileNameWithExtension(string fileName)
        {
            return fileName + ".dat";
        }

        public void SaveAll<T>(Dictionary<string, SaveState> stateData)
        {
            if (stateData != null)
            {
                List<ISavable> savables = new List<ISavable>();
                savables.AddRange(SavablesInCode.Values);
                savables.AddRange(SavablesInScene.Values);

                foreach (ISavable savable in savables)
                {
                    if (savable is T)
                    {
                        SaveState state = savable.GetSaveState();
                        if (state != null)
                        {
                            state.Save(savable);
                            stateData[savable.GetKey()] = state;
                        }
                    }
                }
            }
        }

        public void LoadAll<T>(Dictionary<string, SaveState> stateData)
        {
            if (stateData != null)
            {
                List<ISavable> savables = new List<ISavable>();
                savables.AddRange(SavablesInCode.Values);
                savables.AddRange(SavablesInScene.Values);

                foreach (ISavable savable in savables.Where(i => i.GetSaveState() != null).OrderBy(i => i.GetSaveState().LoadPriority()))
                {
                    if (savable is T)
                    {
                        string savableKey = savable.GetKey();
                        if (stateData.ContainsKey(savableKey))
                        {
                            SaveState state = stateData[savableKey];
                            if (state != null)
                            {
                                state.Load(savable);
                            }
                        }
                    }
                }
            }
        }
    }

}
