﻿using System;
using ImprobableStudios.SaveSystem.FullSerializer;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ResourceTrackingUtilities;

namespace ImprobableStudios.SaveSystem
{
    
    public class UnityObjectConverter : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return typeof(UnityEngine.Object).IsAssignableFrom(type);
        }

        public override bool RequestCycleSupport(Type storageType)
        {
            return false;
        }

        public override bool RequestInheritanceSupport(Type storageType)
        {
            return true;
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            UnityEngine.Object obj = instance as UnityEngine.Object;
            serialized = new fsData(ResourceTracker.GetObjectKey(obj));
            return fsResult.Success;
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            if (data.IsString == false) return fsResult.Fail("Expected string in " + data);
            
            instance = ResourceTracker.GetCachedObject(storageType, data.AsString);
            return fsResult.Success;
        }
    }

}