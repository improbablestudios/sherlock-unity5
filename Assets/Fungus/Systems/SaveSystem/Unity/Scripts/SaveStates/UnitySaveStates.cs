﻿using System;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class MaskableGraphicSaveState<T> : GraphicSaveState<T> where T : MaskableGraphic
    {
        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.onCullStateChanged,
                x => x.maskable,
            };
        }
    }

    [Serializable]
    public class GraphicSaveState<T> : BehaviourSaveState<T> where T : Graphic
    {
        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.color,
                x => x.material,
                x => x.raycastTarget,
            };
        }
    }

    [Serializable]
    public class RendererSaveState<T> : ComponentSaveState<T> where T : Renderer
    {
        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.enabled,
            };
        }
    }

    [Serializable]
    public class BehaviourSaveState<T> : ComponentSaveState<T> where T : Behaviour
    {
        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.enabled,
            };
        }
    }

    [Serializable]
    public class TransformSaveState<T> : ComponentSaveState<T> where T : Transform
    {
        public Transform m_Parent;

        public override void Save(T transform)
        {
            base.Save(transform);

            m_Parent = transform.parent;
        }

        public override void Load(T transform)
        {
            base.Load(transform);

            transform.SetParent(m_Parent, false);
        }

        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.localPosition,
                x => x.localRotation,
                x => x.localScale,
            };
        }
    }

    [Serializable]
    public class ComponentSaveState<T> : SaveState<T> where T : Component
    {
        public bool m_GameObjectActive;

        public override void Save(T component)
        {
            base.Save(component);

            m_GameObjectActive = component.gameObject.activeSelf;
        }

        public override void Load(T component)
        {
            base.Load(component);

            component.gameObject.SetActive(m_GameObjectActive);
        }

        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.tag,
                x => x.name,
                x => x.hideFlags,
            };
        }
    }


    [Serializable]
    public class ScriptableObjectSaveState<T> : SaveState<T> where T : ScriptableObject
    {
        public override Expression<Func<T, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<T, object>>[]
            {
                x => x.name,
                x => x.hideFlags,
            };
        }
    }

}