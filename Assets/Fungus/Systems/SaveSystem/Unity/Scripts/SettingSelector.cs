﻿using UnityEngine;
using UnityEngine.UI;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.SaveSystem
{

    public abstract class SettingSelector<T> : PersistentBehaviour where T : Selectable
    {
        [Tooltip("If true, save setting as soon as it is changed")]
        [SerializeField]
        protected bool m_SaveSettingsWhenChanged = true;

        public bool SaveSettingsWhenChanged
        {
            get
            {
                return m_SaveSettingsWhenChanged;
            }
            set
            {
                m_SaveSettingsWhenChanged = value;
            }
        }

        protected override void Reset()
        {
            base.Reset();
            SetupControl();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            SetupControl();
        }

        public abstract T GetControl();

        public abstract void SetControl(T control);

        private void SetupControl()
        {
            if (GetControl() == null)
            {
                SetControl(GetComponent<T>());
            }
        }

        protected void OnSettingChanged()
        {
            if (m_SaveSettingsWhenChanged)
            {
                SaveSystemManager.Instance.SaveSettings();
            }
        }
    }

}