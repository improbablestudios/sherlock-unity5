﻿#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

namespace ImprobableStudios.SaveSystem
{

    public class SaveSystemMenuItems
    {
        [MenuItem("Tools/Save System/Savable Animator", false, 100)]
        [MenuItem("GameObject/Save System/Savable Animator", false, 100)]
        static void CreateSavableAnimator()
        {
            PrefabSpawner.SpawnPrefab<SavableAnimator>("~SavableAnimator", true, typeof(Animator));
        }

        [MenuItem("Tools/Save System/Savable Audio Source", false, 100)]
        [MenuItem("GameObject/Save System/Savable Audio Source", false, 100)]
        static void CreateSavableAudioSource()
        {
            PrefabSpawner.SpawnPrefab<SavableAudioSource>("~SavableAudioSource", true, typeof(AudioSource));
        }

        [MenuItem("Tools/Save System/Savable Camera", false, 100)]
        [MenuItem("GameObject/Save System/Savable Camera", false, 100)]
        static void CreateSavableCamera()
        {
            PrefabSpawner.SpawnPrefab<SavableCamera>("~SavableCamera", true, typeof(Camera));
        }

        [MenuItem("Tools/Save System/Savable Canvas Group", false, 100)]
        [MenuItem("GameObject/Save System/Savable Canvas Group", false, 100)]
        static void CreateSavableCanvasGroup()
        {
            PrefabSpawner.SpawnPrefab<SavableCanvasGroup>("~SavableCanvasGroup", true, typeof(CanvasGroup));
        }

        [MenuItem("Tools/Save System/Savable Image", false, 100)]
        [MenuItem("GameObject/Save System/Savable Image", false, 100)]
        static void CreateSavableImage()
        {
            PrefabSpawner.SpawnPrefab<SavableImage>("~SavableImage", true, typeof(Image));
        }

        [MenuItem("Tools/Save System/Savable Rect Transform", false, 100)]
        [MenuItem("GameObject/Save System/Savable Rect Transform", false, 100)]
        static void CreateSavableRectTransform()
        {
            PrefabSpawner.SpawnPrefab<SavableRectTransform>("~SavableRectTransform", true, typeof(RectTransform));
        }

        [MenuItem("Tools/Save System/Savable Sprite Renderer", false, 100)]
        [MenuItem("GameObject/Save System/Savable Sprite Renderer", false, 100)]
        static void CreateSavableSpriteRenderer()
        {
            PrefabSpawner.SpawnPrefab<SavableSpriteRenderer>("~SavableSpriteRenderer", true, typeof(SpriteRenderer));
        }

        [MenuItem("Tools/Save System/Savable Text", false, 100)]
        [MenuItem("GameObject/Save System/Savable Text", false, 100)]
        static void CreateSavableText()
        {
            PrefabSpawner.SpawnPrefab<SavableText>("~SavableText", true, typeof(Text));
        }

        [MenuItem("Tools/Save System/Savable Transform", false, 100)]
        [MenuItem("GameObject/Save System/Savable Transform", false, 100)]
        static void CreateSavableTransform()
        {
            PrefabSpawner.SpawnPrefab<SavableTransform>("~SavableText", true, typeof(Transform));
        }

        [MenuItem("Tools/Save System/Savable Video Player", false, 100)]
        [MenuItem("GameObject/Save System/Savable Video Player", false, 100)]
        static void CreateSavableVideo()
        {
            PrefabSpawner.SpawnPrefab<SavableVideoPlayer>("~SavableVideoPlayer", true, typeof(VideoPlayer));
        }
    }

}
#endif