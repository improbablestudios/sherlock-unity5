﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.SaveSystem
{

    public class SaveSystemManagerSettingsProvider : BaseAssetSettingsProvider<SaveSystemManager>
    {
        public SaveSystemManagerSettingsProvider() : base("Project/Managers/Save System Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new SaveSystemManagerSettingsProvider();
        }
    }

    public class SaveSystemManagerSettingsBuilder : BaseAssetSettingsBuilder<SaveSystemManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif