#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [CanEditMultipleObjects, CustomEditor(typeof(SaveSystemManager), true)]
    public class SaveSystemManagerEditor : IdentifiableScriptableObjectEditor
    {
        public override void DrawNormalInspector()
        {
            SaveSystemManager t = target as SaveSystemManager;

            base.DrawNormalInspector();

            if (Application.isPlaying)
            {
                EditorGUILayout.Separator();

                string testSaveFileName = "test";

                using (new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Test Save"))
                    {
                        t.Save(testSaveFileName);
                        EditorUtility.DisplayDialog("Test Save", "SAVED: " + testSaveFileName, "OK");
                    }

                    GUILayout.Space(10);

                    if (GUILayout.Button("Test Load"))
                    {
                        t.Load(testSaveFileName);
                        EditorUtility.DisplayDialog("Test Load", "LOADED: " + testSaveFileName, "OK");
                    }

                    GUILayout.FlexibleSpace();
                }
            }
        }
    }

}

#endif