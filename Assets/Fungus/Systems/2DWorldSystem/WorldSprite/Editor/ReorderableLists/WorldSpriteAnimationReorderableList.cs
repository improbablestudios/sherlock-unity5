#if UNITY_EDITOR
using ImprobableStudios.ReorderableListUtilities;
using UnityEditor;
using UnityEngine;

namespace ImprobableStudios.WorldSystem2D
{
    public class WorldSpriteAnimationReorderableList : GenericReorderableList
    {
        protected WorldSprite m_worldSprite;

        public WorldSpriteAnimationReorderableList(WorldSprite worldSprite) : base()
        {
            m_worldSprite = worldSprite;
        }

        public WorldSpriteAnimationReorderableList(ReorderableListFlags flags, WorldSprite worldSprite) : base(flags)
        {
            m_worldSprite = worldSprite;
        }

        protected override object Create()
        {
            return WorldSpriteAnimation.CreateNewWorldSpriteAnimation("~WorldSpriteAnimation",m_worldSprite.gameObject);
        }
        
        protected override void Insert(int index, object element)
        {
            base.Insert(index, element);
            WorldSpriteAnimation item = element as WorldSpriteAnimation;
            item.transform.SetSiblingIndex(index);
        }

        protected override void Delete(int[] indices)
        {
            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];

                WorldSpriteAnimation item = m_list[index] as WorldSpriteAnimation;
                if (item != null)
                {
                    Undo.DestroyObjectImmediate(item.gameObject);
                }

                m_list.RemoveAt(index);
            }
        }

        protected override void Move(int sourceIndex, int destIndex)
        {
            WorldSpriteAnimation item = m_list[sourceIndex] as WorldSpriteAnimation;

            m_list.RemoveAt(sourceIndex);

            item.transform.SetSiblingIndex(destIndex);

            if (destIndex > sourceIndex)
                --destIndex;

            m_list.Insert(destIndex, item);
        }

        protected override void DrawElementExpandedContent(Rect position, int index)
        {
            WorldSpriteAnimation item = m_list[index] as WorldSpriteAnimation;

            EditorGUI.ObjectField(position, GUIContent.none, item, typeof(WorldSpriteAnimation), true);
        }
    }
}
#endif