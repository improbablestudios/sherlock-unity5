#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.WorldSystem2D
{
    
    public class WorldSpriteMenuItems 
    {
        [MenuItem("Tools/2D World System/World Sprite", false, 10)]
        [MenuItem("GameObject/2D World System/World Sprite", false, 10)]
        static void CreateWorldSprite()
        {
            PrefabSpawner.SpawnPrefab<WorldSprite>("~WorldSprite", true);
        }

        [MenuItem("Tools/2D World System/World Sprite Animation", false, 100)]
        [MenuItem("GameObject/2D World System/World Sprite Animation", false, 100)]
        static void CreateWorldSpriteAnimation()
        {
            PrefabSpawner.SpawnPrefab<WorldSpriteAnimation>("~WorldSpriteAnimation", true);
        }

        [MenuItem("Tools/2D World System/Utilities/Re-Slice Spritesheets", false, 200)]
        static void ResliceAllWorldSpriteAnimationSpritesheets()
        {
            UnityEngine.Object[] objects = Selection.GetFiltered<UnityEngine.Object>(SelectionMode.DeepAssets);
            try
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Re-Slice Spritesheets", string.Format("Slicing '{0}'", objects[i].name), ((float)i / (float)objects.Length)))
                    {
                        break;
                    }

                    UnityEngine.Object obj = objects[i];

                    GameObject gameObject = obj as GameObject;

                    if (gameObject != null)
                    {
                        foreach (WorldSpriteAnimation wsa in gameObject.GetComponentsInChildren<WorldSpriteAnimation>(true))
                        {
                            wsa.SliceAllSpritesheets();
                            EditorUtility.SetDirty(wsa);
                        }
                    }
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }
    }

}
#endif