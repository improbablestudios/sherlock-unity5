#if UNITY_EDITOR
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.ReorderableListUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using ImprobableStudios.UnityEditorUtilities;
using UnityEngine;
using ImprobableStudios.UnityObjectUtilities;

namespace ImprobableStudios.WorldSystem2D
{

    [CanEditMultipleObjects, CustomEditor(typeof(WorldSpriteAnimation), true)]
    public class WorldSpriteAnimationEditor : PersistentBehaviourEditor
    {
        protected SerializedProperty m_skinsProp;

        protected SerializedProperty m_numberOfRowsProp;
        protected SerializedProperty m_numberOfColumnsProp;

        protected SerializedProperty m_directionsProp;

        protected SerializedProperty m_moveFramesPerSecondProp;

        protected SerializedProperty m_progressiveAnimationExistsProp;
        protected SerializedProperty m_progressiveFramesProp;
        protected SerializedProperty m_progressiveFramesPerSecondProp;
        protected SerializedProperty m_forwardAudioProp;
        protected SerializedProperty m_backwardAudioProp;

        protected SerializedProperty m_loopAnimationExistsProp;
        protected SerializedProperty m_loopFramesProp;
        protected SerializedProperty m_loopFramesPerSecondProp;
        protected SerializedProperty m_loopTypeProp;
        protected SerializedProperty m_startLoopAtMiddleProp;
        protected SerializedProperty m_loopAudioProp;

        protected SerializedPropertyReorderableList m_framesReorderableList;

        protected static WorldSprite m_previewWorldSprite;

        public static double m_PreviewStartTime;
        public static int m_PreviewStartColumn;
        public static bool m_PreviewLoopForward = true;
        public static bool m_PreviewLooping;

        SerializedPropertyReorderableList m_skinsReorderableList;

        public static WorldSprite PreviewWorldSprite
        {
            get
            {
                return m_previewWorldSprite;
            }
            set
            {
                m_previewWorldSprite = value;
            }
        }

        protected static int PreviewSkinIndex
        {
            get
            {
                return m_previewWorldSprite.PreviewSkinIndex;
            }
            set
            {
                m_previewWorldSprite.PreviewSkinIndex = value;
            }
        }

        protected static WorldSpriteAnimation PreviewWorldSpriteAnimation
        {
            get
            {
                return m_previewWorldSprite.PreviewAnimation;
            }
            set
            {
                m_previewWorldSprite.PreviewAnimation = value;
            }
        }

        protected static int PreviewRow
        {
            get
            {
                return PreviewWorldSpriteAnimation.ConvertDirectionToRow(m_previewWorldSprite.PreviewDirection);
            }
            set
            {
                m_previewWorldSprite.PreviewDirection = PreviewWorldSpriteAnimation.ConvertRowToDirection(value);
            }
        }

        protected static int PreviewColumn
        {
            get
            {
                return m_previewWorldSprite.PreviewColumn;
            }
            set
            {
                m_previewWorldSprite.PreviewColumn = value;
            }
        }

        protected static float PreviewSpeed
        {
            get
            {
                return m_previewWorldSprite.PreviewSpeed;
            }
            set
            {
                m_previewWorldSprite.PreviewSpeed = value;
            }
        }

        protected static int PreviewFrameIndex
        {
            get
            {
                return m_previewWorldSprite.PreviewFrameIndex;
            }
        }

        protected static Sprite PreviewFrameSprite
        {
            get
            {
                return m_previewWorldSprite.PreviewFrameSprite;
            }
        }

        public static GUIStyle FrameBoxStyle
        {
            get
            {
                GUIStyle frameBox = new GUIStyle(EditorStyles.numberField);
                frameBox.margin.left = 0;
                frameBox.margin.right = 0;
                frameBox.padding.left = 0;
                frameBox.padding.right = 0;
                frameBox.padding.bottom = 0;
                return frameBox;
            }
        }

        public static GUIStyle CenteredLabelStyle
        {
            get
            {
                GUIStyle centeredLabel = new GUIStyle(EditorStyles.largeLabel);
                centeredLabel.alignment = TextAnchor.MiddleCenter;
                centeredLabel.fontStyle = FontStyle.Bold;
                return centeredLabel;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            WorldSpriteAnimation t = target as WorldSpriteAnimation;
            
            m_previewWorldSprite = t.GetComponentInParent<WorldSprite>(true);
            if (m_previewWorldSprite != null)
            {
                m_previewWorldSprite.PreviewAnimation = t;
            }

            t.Validate();

            m_skinsProp = serializedObject.FindProperty("m_" + nameof(t.Skins));

            m_numberOfRowsProp = serializedObject.FindProperty("m_" + nameof(t.NumberOfRows));
            m_numberOfColumnsProp = serializedObject.FindProperty("m_" + nameof(t.NumberOfColumns));

            m_directionsProp = serializedObject.FindProperty("m_" + nameof(t.Directions));
            
            m_progressiveAnimationExistsProp = serializedObject.FindProperty("m_" + nameof(t.ProgressiveAnimationExists));
            m_progressiveFramesProp = serializedObject.FindProperty("m_" + nameof(t.ProgressiveFrameIndices));
            m_progressiveFramesPerSecondProp = serializedObject.FindProperty("m_" + nameof(t.ProgressiveFramesPerSecond));
            m_forwardAudioProp = serializedObject.FindProperty("m_" + nameof(t.ForwardAudioClip));
            m_backwardAudioProp = serializedObject.FindProperty("m_" + nameof(t.BackwardAudioClip));

            m_loopAnimationExistsProp = serializedObject.FindProperty("m_" + nameof(t.LoopAnimationExists));
            m_loopFramesProp = serializedObject.FindProperty("m_" + nameof(t.LoopFrameIndices));
            m_loopFramesPerSecondProp = serializedObject.FindProperty("m_" + nameof(t.LoopFramesPerSecond));
            m_loopTypeProp = serializedObject.FindProperty("m_" + nameof(t.LoopType));
            m_startLoopAtMiddleProp = serializedObject.FindProperty("m_" + nameof(t.StartAndEndLoopAtMiddle));
            m_loopAudioProp = serializedObject.FindProperty("m_" + nameof(t.LoopAudioClip));
        }
        
        public override void DrawNormalInspector()
        {
            serializedObject.Update();

            WorldSpriteAnimation t = target as WorldSpriteAnimation;

            m_previewWorldSprite = t.WorldSprite;

            if (m_previewWorldSprite == null)
            {
                EditorGUILayout.HelpBox("This World Sprite Animation must be a child of a World Sprite", MessageType.Warning);
                ExtendedEditorGUI.LineSeparator();
                serializedObject.ApplyModifiedProperties();
                return;
            }

            m_previewWorldSprite.PreviewAnimation = t;

            if (t.WorldSprite.SkinNames.Length <= 0)
            {
                EditorGUILayout.HelpBox("Add at least one skin to your world sprite", MessageType.Info);
                serializedObject.ApplyModifiedProperties();
                return;
            }

            bool error = false;

            error = !DrawSpriteSheetSettings(t);

            if (error)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }

            EditorGUILayout.LabelField("Direction Settings", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;
            error = !DrawDirectionSettings(t);
            EditorGUI.indentLevel--;

            if (error)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }

            EditorGUILayout.LabelField("Animation Settings", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;
            DrawProgressiveAnimationSettings(t, PreviewSkinIndex, PreviewRow);
            DrawLoopAnimationSettings(t, PreviewSkinIndex, PreviewRow);
            EditorGUI.indentLevel--;

            if (error)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }

            if (targets.Length == 1 && t.NumberOfColumns != 0 && t.NumberOfRows != 0)
            {
                ExtendedEditorGUI.LineSeparator();

                EditorGUILayout.LabelField("Preview Animation", CenteredLabelStyle);
                EditorGUILayout.Separator();
                PreviewSkinIndex = EditorGUILayout.Popup("Preview Skin", PreviewSkinIndex, t.WorldSprite.SkinNames.ToArray());
                if (!t.IsSkinIndexValid(PreviewSkinIndex))
                {
                    PreviewSkinIndex = t.GetFirstValidSkinIndex();
                }
                if (PreviewRow < 0 || PreviewRow > t.Directions.Length - 1)
                {
                    PreviewRow = 0;
                }
                PreviewRow = EditorGUILayout.Popup("Facing", PreviewRow, t.Directions.Select(i => i.ToString()).ToArray());
                PreviewColumn = EditorGUILayout.IntSlider("Frame Index", PreviewColumn, 0, t.NumberOfColumns - 1);
                DrawPreviewToolbar(t);
                PreviewSpeed = 1;
                DrawWorldSpriteAnimationPreview(t, PreviewRow, PreviewColumn);

                DrawSpriteSheet(t, PreviewSkinIndex, PreviewRow, true, true);
            }
            serializedObject.ApplyModifiedProperties();
            Repaint();
        }

        public bool DrawSpriteSheetSettings(WorldSpriteAnimation t)
        {
            if (m_skinsReorderableList == null)
            {
                m_skinsReorderableList = new SerializedPropertyReorderableList();
            }
            m_skinsReorderableList.Draw(m_skinsProp);

            if (t.Skins.Length <= 0)
            {
                EditorGUILayout.HelpBox("Add at least one skin to your world sprite", MessageType.Info);
                return false;
            }
            
            EditorGUILayout.PropertyField(m_numberOfRowsProp, true);
            EditorGUILayout.PropertyField(m_numberOfColumnsProp, true);
            
            if (GUILayout.Button("Re-Slice", EditorStyles.miniButton))
            {
                foreach (WorldSpriteAnimation wsa in targets)
                {
                    wsa.SliceAllSpritesheets();
                }
            }

            if (!t.IsSkinIndexValid(PreviewSkinIndex))
            {
                PreviewSkinIndex = t.GetFirstValidSkinIndex();
            }

            for (int i = 0; i < t.Skins.Length; i++)
            {
                Skin skin = t.Skins[i];
                if (skin.m_SpriteSheet != null)
                {
                    if (skin.m_SpriteSheet.height % t.NumberOfRows != 0)
                    {
                        DrawSpriteSheet(t, PreviewSkinIndex, PreviewRow, false, false);

                        EditorGUILayout.HelpBox(skin.m_SpriteSheet.name + " height (" + skin.m_SpriteSheet.height + ") is not a multiple of the number of rows (" + t.NumberOfRows + ")", MessageType.Error);
                        return false;
                    }
                    if (skin.m_SpriteSheet.width % t.NumberOfColumns != 0)
                    {
                        DrawSpriteSheet(t, PreviewSkinIndex, PreviewRow, false, false);

                        EditorGUILayout.HelpBox(skin.m_SpriteSheet.name + " width (" + skin.m_SpriteSheet.width + ") is not a multiple of the number of columns (" + t.NumberOfColumns + ")", MessageType.Error);
                        return false;
                    }
                }
                
                if (skin.m_Frames != null && skin.m_Frames.Length > 0)
                {
                    if (skin.m_Frames.Length < t.NumberOfColumns * t.NumberOfRows)
                    {
                        DrawSpriteSheet(t, PreviewSkinIndex, PreviewRow, false, true);

                        EditorGUILayout.HelpBox(t.WorldSprite.SkinNames[i] + " does not have enough frames to arrange into " + t.NumberOfRows + " rows" + " and " + t.NumberOfColumns + " columns" + ".", MessageType.Error);
                        return false;
                    }
                    if (skin.m_Frames.Length > t.NumberOfColumns * t.NumberOfRows)
                    {
                        DrawSpriteSheet(t, PreviewSkinIndex, PreviewRow, false, true);

                        EditorGUILayout.HelpBox(t.WorldSprite.SkinNames[i] + " has too many frames (" + skin.m_Frames.Length + ")  to arrange into " + t.NumberOfRows + " rows" + " and " + t.NumberOfColumns + " columns" + ".", MessageType.Error);
                        return false;
                    }
                }
            }

            if (t.GetFirstValidSkin() == null)
            {
                EditorGUILayout.HelpBox("At least one skin must have valid sprite sheet frames", MessageType.Info);
                return false;
            }

            ExtendedEditorGUI.LineSeparator();
            
            return true;
        }
        
        public bool DrawDirectionSettings(WorldSpriteAnimation t)
        {
            for (int i = 0; i < t.NumberOfRows; i++)
            {
                if (i >= 0 && i < m_directionsProp.arraySize)
                {
                    EditorGUILayout.PropertyField(m_directionsProp.GetArrayElementAtIndex(i), new GUIContent(string.Format("ROW {0}:", i + 1), string.Format("The direction the sprite is facing in row {0}", i + 1)), true);
                }
            }

            return true;
        }

        public void DrawProgressiveAnimationSettings(WorldSpriteAnimation t, int skinIndex, int row)
        {
            EditorGUILayout.PropertyField(m_progressiveAnimationExistsProp, new GUIContent("Progressive Animation", m_progressiveAnimationExistsProp.tooltip), true);
            if (t.ProgressiveAnimationExists)
            {
                EditorGUI.indentLevel++;

                if (t.LoopAnimationExists) // If loop exists, must select which frames are part of the progressive animation
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 15f);
                        using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                        {
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                bool[] progressiveFramesSelected = new bool[t.NumberOfColumns];
                                EditorGUI.BeginChangeCheck();
                                for (int c = 0; c < t.NumberOfColumns; c++)
                                {
                                    progressiveFramesSelected[c] = t.ProgressiveFrameIndices.Contains(c);
                                    progressiveFramesSelected[c] = GUILayout.Toggle(progressiveFramesSelected[c], new GUIContent(c.ToString()), GUI.skin.FindStyle("ButtonMid"));
                                }
                                if (EditorGUI.EndChangeCheck())
                                {
                                    t.ProgressiveFrameIndices = GetSelectedFrameIndices(t.NumberOfRows, t.NumberOfColumns, progressiveFramesSelected, t.ProgressiveFrameIndices);
                                }
                            }

                            DrawSpriteSheet(t, skinIndex, row, false, true);
                            EditorGUILayout.PropertyField(m_progressiveFramesProp, true);
                        }
                    }

                    if (t.ProgressiveFrameIndices.Length == 0)
                    {
                        EditorGUILayout.HelpBox("Select the progressive frames in this animation", MessageType.Info);
                    }
                }

                EditorGUILayout.PropertyField(m_progressiveFramesPerSecondProp, true);
                EditorGUILayout.PropertyField(m_forwardAudioProp, true);
                EditorGUILayout.PropertyField(m_backwardAudioProp, true);
                EditorGUI.indentLevel--;
            }
        }

        public void DrawLoopAnimationSettings(WorldSpriteAnimation t, int skinIndex, int row)
        {
            EditorGUILayout.PropertyField(m_loopAnimationExistsProp, new GUIContent("Loop Animation", m_loopAnimationExistsProp.tooltip), true);
            if (t.LoopAnimationExists)
            {
                EditorGUI.indentLevel++;

                if (t.ProgressiveAnimationExists) // If progressive exists, must select which frames are part of the loop animation
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 15f);
                        using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                        {
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                bool[] loopFramesSelected = new bool[t.NumberOfColumns];
                                EditorGUI.BeginChangeCheck();
                                for (int c = 0; c < t.NumberOfColumns; c++)
                                {
                                    loopFramesSelected[c] = t.LoopFrameIndices.Contains(c);
                                    loopFramesSelected[c] = GUILayout.Toggle(loopFramesSelected[c], new GUIContent(c.ToString()), GUI.skin.FindStyle("ButtonMid"));
                                }
                                if (EditorGUI.EndChangeCheck())
                                {
                                    t.LoopFrameIndices = GetSelectedFrameIndices(t.NumberOfRows, t.NumberOfColumns, loopFramesSelected, t.LoopFrameIndices);
                                }
                            }

                            DrawSpriteSheet(t, skinIndex, row, false, true);
                            EditorGUILayout.PropertyField(m_loopFramesProp, true);
                        }
                    }
                    
                    if (t.LoopFrameIndices.Length == 0)
                    {
                        EditorGUILayout.HelpBox("Select the loop frames in this animation", MessageType.Info);
                    }
                }

                EditorGUILayout.PropertyField(m_loopFramesPerSecondProp, true);
                EditorGUILayout.PropertyField(m_loopTypeProp, true);
                if (t.LoopType == DG.Tweening.LoopType.Yoyo)
                {
                    EditorGUILayout.PropertyField(m_startLoopAtMiddleProp, true);
                }
                EditorGUILayout.PropertyField(m_loopAudioProp, true);
                EditorGUI.indentLevel--;
            }
        }

        private int[] GetSelectedFrameIndices(int numberOfRows, int numberOfColumns, bool[] framesSelected, int[] frameIndices)
        {
            // Add selected frames to progressiveFrames
            List<int> newFrameIndices = frameIndices.ToList();
            if (newFrameIndices == null)
            {
                newFrameIndices = new List<int>();
            }
            for (int r = 0; r < numberOfRows; r++)
            {
                for (int c = 0; c < numberOfColumns; c++)
                {
                    int i = r * numberOfColumns + c;
                    if ((i < numberOfRows * numberOfColumns) && (framesSelected.Length > 0) && (framesSelected[c]))
                    {
                        if (!newFrameIndices.Contains(i))
                        {
                            newFrameIndices.Add(i);
                        }
                    }
                    else
                    {
                        newFrameIndices.Remove(i);
                    }
                }
            }
            newFrameIndices.Sort();
            return newFrameIndices.ToArray();
        }

        public static void DrawWorldSpriteAnimationPreview(WorldSpriteAnimation worldSpriteAnimation, int row, int column)
        {
            if (!worldSpriteAnimation.IsSkinIndexValid(PreviewSkinIndex))
            {
                PreviewSkinIndex = worldSpriteAnimation.GetFirstValidSkinIndex();
            }
            if (!worldSpriteAnimation.IsSkinIndexValid(PreviewSkinIndex))
            {
                return;
            }
            Skin previewSkin = worldSpriteAnimation.Skins[PreviewSkinIndex];
            EditorGUILayout.Separator();
            float scale = 2;

            int frameIndex = row * worldSpriteAnimation.NumberOfColumns + column;
            if (frameIndex >= 0 && frameIndex < previewSkin.m_Frames.Length && previewSkin.m_Frames[frameIndex] != null)
            {
                Sprite sprite = previewSkin.m_Frames[frameIndex];
                Vector2 frameSize = new Vector2(sprite.rect.width * scale, sprite.rect.height * scale);
                Rect position = EditorGUILayout.GetControlRect(false, GUILayout.MinWidth(frameSize.x), GUILayout.MinHeight(frameSize.y));
                DrawSprite(position, sprite, scale);
            }
            EditorGUILayout.Separator();
        }

        public static void DrawPreviewToolbar(WorldSpriteAnimation t)
        {
            GUIContent[] toolbarIcons = new GUIContent[] { new GUIContent("Forward"), new GUIContent("Loop"), new GUIContent("Pause"), new GUIContent("Backwards") };
            int toolbarInt = -1; // Start off with nothing on the toolbar clicked
            toolbarInt = GUILayout.Toolbar(toolbarInt, toolbarIcons, GUILayout.MinHeight(20));
            switch (toolbarInt)
            {
                case 0: // START is clicked
                    StartPreview(t);
                    break;
                case 1: // LOOP is clicked
                    int startFrame = 0;
                    if (t.StartAndEndLoopAtMiddle)
                    {
                        startFrame = t.GetRowLength(t.LoopFrameIndices.Length) / 2;
                    }
                    PreviewColumn = t.LoopFrameIndices[startFrame];
                    LoopPreview(t);
                    break;
                case 2: // PAUSE is clicked
                    PausePreview();
                    break;
                case 3: // STOP is clicked
                    StopPreview(t);
                    break;
            }
        }

        public static void DrawSpriteSheet(WorldSpriteAnimation worldSpriteAnimation, int skinIndex, int highlightedRow, bool highlightCurrentFrame, bool useFrames)
        {
            if (!worldSpriteAnimation.IsSkinIndexValid(PreviewSkinIndex))
            {
                return;
            }

            Skin previewSkin = worldSpriteAnimation.Skins[skinIndex];
            GUIStyle selectedFrame = new GUIStyle(FrameBoxStyle);
            selectedFrame.normal = (EditorStyles.miniButton.active);
            int numberOfRows = worldSpriteAnimation.NumberOfRows;
            int numberOfColumns = worldSpriteAnimation.NumberOfColumns;
            if (useFrames)
            {
                numberOfRows = previewSkin.m_Frames.Length / numberOfColumns;
            }
            for (int r = 0; r < numberOfRows; r++)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    for (int c = 0; c < numberOfColumns; c++)
                    {
                        int i = r * worldSpriteAnimation.NumberOfColumns + c;
                        GUIStyle verticalStyle = FrameBoxStyle;
                        if (PreviewColumn + highlightedRow * worldSpriteAnimation.NumberOfColumns == i && highlightCurrentFrame)
                        {
                            verticalStyle = selectedFrame;
                        }
                        using (new EditorGUILayout.VerticalScope(verticalStyle))
                        {
                            float scale = 1;
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                if (useFrames)
                                {
                                    int frameIndex = r * worldSpriteAnimation.NumberOfColumns + c;
                                    if (frameIndex < previewSkin.m_Frames.Length && previewSkin.m_Frames[frameIndex] != null)
                                    {
                                        Sprite sprite = previewSkin.m_Frames[frameIndex];
                                        Vector2 frameSize = new Vector2(sprite.rect.width * scale, sprite.rect.height * scale);
                                        Rect position = EditorGUILayout.GetControlRect(false, GUILayout.MinWidth(frameSize.x), GUILayout.MinHeight(frameSize.y));
                                        DrawSprite(position, sprite, scale);
                                    }
                                }
                                else
                                {
                                    Vector2 frameSize = GetWorldSpriteAnimationCroppedFrameSize(previewSkin.m_SpriteSheet, worldSpriteAnimation.NumberOfColumns, worldSpriteAnimation.NumberOfRows, scale);
                                    Rect position = EditorGUILayout.GetControlRect(false, GUILayout.MinWidth(frameSize.x), GUILayout.MinHeight(frameSize.y));
                                    DrawSpriteSheetTextureCroppedToFrame(position, previewSkin.m_SpriteSheet, worldSpriteAnimation.NumberOfColumns, worldSpriteAnimation.NumberOfRows, c, r, scale);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public static Vector2 GetWorldSpriteAnimationCroppedFrameSize(Texture2D spriteSheet, int columnCount, int rowCount, float scale)
        {
            float normalizedFrameWidth = (1f / columnCount) * scale;
            float normalizedFrameHeight = (1f / rowCount) * scale;
            return new Vector2(normalizedFrameWidth * spriteSheet.width, normalizedFrameHeight * spriteSheet.height);
        }

        public static void DrawSpriteSheetTextureCroppedToFrame(Rect position, Texture2D spriteSheet, int columnCount, int rowCount, int frameXIndex, int frameYIndex, float scale)
        {
            float normalizedFrameWidth = (1f / columnCount) * scale;
            float normalizedFrameHeight = (1f / rowCount) * scale;
            float normalizedFrameXOffset = normalizedFrameWidth * frameXIndex;
            float normalizedFrameYOffset = normalizedFrameHeight * frameYIndex;
            ExtendedEditorGUI.DrawCroppedTexture(position, spriteSheet, normalizedFrameWidth, normalizedFrameHeight, normalizedFrameXOffset, normalizedFrameYOffset, scale);
        }

        public static void DrawSprite(Rect position, Sprite sprite, float scale)
        {
            Rect framePositionRect = position;
            framePositionRect.width = sprite.rect.width * scale;
            framePositionRect.height = sprite.rect.height * scale;
            framePositionRect.x += (position.width / 2f) - (framePositionRect.width / 2f);
            framePositionRect.y += (position.height / 2f) - (framePositionRect.height / 2f);
            Texture2D tex = sprite.texture;
            Rect frameTextureRect = sprite.rect;
            frameTextureRect.xMin /= tex.width;
            frameTextureRect.xMax /= tex.width;
            frameTextureRect.yMin /= tex.height;
            frameTextureRect.yMax /= tex.height;
            GUI.DrawTextureWithTexCoords(framePositionRect, tex, frameTextureRect);
        }

        protected void OpenImportSettings(string path)
        {
            if (GUILayout.Button("Open Import Settings"))
            {
                EditorUtility.FocusProjectWindow();
                UnityEngine.Object objectToSelect = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
                if (!Application.isPlaying)
                {
                    EditorGUIUtility.PingObject(objectToSelect);
                }
                Selection.activeObject = objectToSelect;

            }
        }

        public static void RemoveUpdateCallbacks()
        {
            EditorApplication.update -= PlayLoopAnimationBackwards;
            EditorApplication.update -= PlayLoopAnimationForwards;
            EditorApplication.update -= PlayStartAnimation;
            EditorApplication.update -= PlayStopAnimation;
        }

        public static void StartPreview(WorldSpriteAnimation t)
        {
            PreviewWorldSpriteAnimation = t;
            m_PreviewStartTime = EditorApplication.timeSinceStartup;
            m_PreviewStartColumn = PreviewColumn;
            m_PreviewLooping = false;

            RemoveUpdateCallbacks();
            EditorApplication.update += PlayStartAnimation;
        }

        public static void LoopPreview(WorldSpriteAnimation t)
        {
            PreviewWorldSpriteAnimation = t;
            m_PreviewLooping = true;
            PlayLoopAnimation();
        }

        public static void PausePreview()
        {
            m_PreviewStartTime = EditorApplication.timeSinceStartup;
            m_PreviewStartColumn = PreviewColumn;

            RemoveUpdateCallbacks();
            EditorApplication.update += PauseAnimation;
        }

        public static void StopPreview(WorldSpriteAnimation t)
        {
            PreviewWorldSpriteAnimation = t;
            m_PreviewStartTime = EditorApplication.timeSinceStartup;
            m_PreviewStartColumn = PreviewColumn;

            if (t.ProgressiveAnimationExists || !m_PreviewLooping)
            {
                RemoveUpdateCallbacks();
                EditorApplication.update += PlayStopAnimation;
            }

            m_PreviewLooping = false;
            m_PreviewLoopForward = true;
        }

        public static void PlayStartAnimation()
        {
            if (PreviewWorldSprite != null)
            {
                int[] progressiveFrames = PreviewWorldSpriteAnimation.ProgressiveFrameIndices;
                double timeElapsed = EditorApplication.timeSinceStartup - m_PreviewStartTime;
                int progressiveFrame = (int)(timeElapsed / (1 / (PreviewWorldSpriteAnimation.ProgressiveFramesPerSecond * PreviewSpeed)));
                if (progressiveFrame <= (progressiveFrames.Length - 1) / PreviewWorldSpriteAnimation.NumberOfRows)
                {
                    PreviewColumn = progressiveFrames[progressiveFrame];
                }
                else
                {
                    PausePreview();
                }
            }
        }

        public static void PlayLoopAnimation()
        {
            if (PreviewWorldSprite != null)
            {
                m_PreviewStartTime = EditorApplication.timeSinceStartup;
                m_PreviewStartColumn = PreviewColumn;

                RemoveUpdateCallbacks();
                if (PreviewWorldSpriteAnimation.LoopType == DG.Tweening.LoopType.Yoyo || !PreviewWorldSpriteAnimation.LoopAnimationExists)
                {
                    if (m_PreviewLoopForward)
                    {
                        EditorApplication.update += PlayLoopAnimationForwards;
                        m_PreviewLoopForward = false;
                    }
                    else
                    {
                        EditorApplication.update += PlayLoopAnimationBackwards;
                        m_PreviewLoopForward = true;
                    }
                }
                else
                {
                    EditorApplication.update += PlayLoopAnimationForwards;
                }
            }
        }

        public static void PlayLoopAnimationForwards()
        {
            if (PreviewWorldSprite != null)
            {
                int[] loopFrames = PreviewWorldSpriteAnimation.LoopFrameIndices;
                double timeElapsed = EditorApplication.timeSinceStartup - m_PreviewStartTime;
                int rowLength = PreviewWorldSpriteAnimation.GetRowLength(loopFrames.Length);
                int loopFrame = (int)(timeElapsed / (1 / (PreviewWorldSpriteAnimation.LoopFramesPerSecond * PreviewSpeed)));

                if (!m_PreviewLooping)
                {
                    if ((PreviewWorldSpriteAnimation.StartAndEndLoopAtMiddle && (loopFrame == rowLength / 2)) ||
                        (!PreviewWorldSpriteAnimation.StartAndEndLoopAtMiddle && (loopFrame == 0)))
                    {
                        PreviewColumn = loopFrames[loopFrame];
                        PausePreview();
                    }
                }
                else if (loopFrame <= (loopFrames.Length - 1) / PreviewWorldSpriteAnimation.NumberOfRows)
                {
                    PreviewColumn = loopFrames[loopFrame];
                }
                else
                {
                    PlayLoopAnimation();
                }
            }
        }

        public static void PlayLoopAnimationBackwards()
        {
            if (PreviewWorldSprite != null)
            {
                int[] loopFrames = PreviewWorldSpriteAnimation.LoopFrameIndices;
                double timeElapsed = EditorApplication.timeSinceStartup - m_PreviewStartTime;
                int rowLength = PreviewWorldSpriteAnimation.GetRowLength(loopFrames.Length);
                int loopFrame = (rowLength - 2) - (int)(timeElapsed / (1 / (PreviewWorldSpriteAnimation.LoopFramesPerSecond * PreviewSpeed)));

                if (!m_PreviewLooping)
                {
                    if ((PreviewWorldSpriteAnimation.StartAndEndLoopAtMiddle && (loopFrame == rowLength / 2)) ||
                        (!PreviewWorldSpriteAnimation.StartAndEndLoopAtMiddle && (loopFrame == 0)))
                    {
                        PreviewColumn = loopFrames[loopFrame];
                        PausePreview();
                    }
                }
                else if (loopFrame >= 1)
                {
                    PreviewColumn = loopFrames[loopFrame];
                }
                else
                {
                    PlayLoopAnimation();
                }
            }
        }

        public static void PauseAnimation()
        {
        }

        public static void PlayStopAnimation() 
        {
            if (PreviewWorldSprite != null)
            {
                int[] progressiveFrames = PreviewWorldSpriteAnimation.ProgressiveFrameIndices;
                double timeElapsed = EditorApplication.timeSinceStartup - m_PreviewStartTime;

                int rowLength = PreviewWorldSpriteAnimation.GetRowLength(progressiveFrames.Length);

                int startFrame = Array.IndexOf(progressiveFrames, m_PreviewStartColumn);
                if (startFrame < 0 || startFrame >= rowLength)
                {
                    startFrame = rowLength - 1;
                }

                int progressiveFrame = (startFrame - (int)(timeElapsed / (1 / (PreviewWorldSpriteAnimation.ProgressiveFramesPerSecond * PreviewSpeed))));

                if (progressiveFrame >= 0)
                {
                    PreviewColumn = progressiveFrames[progressiveFrame];
                }
                else
                {
                    PausePreview();
                }
            }
        }
    }
    
}
#endif