#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using ImprobableStudios.ReorderableListUtilities;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.WorldSystem2D
{

    [CanEditMultipleObjects, CustomEditor(typeof(WorldSprite), true)]
    public class WorldSpriteEditor : PersistentBehaviourEditor
    {
        private Color m_interactionArcColor = Color.yellow;

        protected WorldSpriteAnimationReorderableList m_worldSpriteAnimationReorderableList;
        
        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();
            
            WorldSprite t = target as WorldSprite;

            if (t.SkinNames.Length == 0)
            {
                EditorGUILayout.HelpBox("Add at least one skin to your world sprite", MessageType.Error);
            }

            if (t.hideFlags != HideFlags.HideInHierarchy)
            {
                if (m_worldSpriteAnimationReorderableList == null)
                {
                    m_worldSpriteAnimationReorderableList = new WorldSpriteAnimationReorderableList(t);
                }
                m_worldSpriteAnimationReorderableList.Draw(t.Animations.ToList(), new GUIContent("World Sprite Animations"));
            }
            else
            {
                ReorderableListEditorGUI.DisplayList<WorldSpriteAnimation>(new GUIContent("World Sprite Animations"), t.Animations.ToList(), WorldSpriteAnimationDrawer);
            }

            if (t.CanInteract && t.UseMouseClickToInteract && !Physics2D.queriesStartInColliders)
            {
                using (new EditorGUILayout.VerticalScope(ReorderableListStyles.ContainerStyle))
                {
                    EditorGUILayout.HelpBox("Physics2D queries must start in colliders for the world sprite to correctly interact with objects by clicking", MessageType.Error);
                    if (GUILayout.Button("Update Physics2D Settings"))
                    {
                        Physics2D.queriesStartInColliders = true;
                    }
                }
            }

            if (GUILayout.Button("Update Sprite Import Settings"))
            {
                t.UpdateTextureImportSettings();
            }

            if (t.TextureImportSettingsNeedsUpdate())
            {
                EditorGUILayout.HelpBox("Click 'Update Sprite Import Settings' to fix your sprite's import settings", MessageType.Warning);
            }
        }

        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            WorldSprite t = target as WorldSprite;
            if (property.name == "m_" + nameof(t.Skin))
            {
                int index = property.intValue;
                EditorGUI.BeginChangeCheck();
                index = EditorGUILayout.Popup(label, index, t.SkinNames.Select(i => new GUIContent(i)).ToArray());
                if (EditorGUI.EndChangeCheck())
                {
                    property.intValue = index;
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        private WorldSpriteAnimation WorldSpriteAnimationDrawer(WorldSpriteAnimation item)
        {
            return EditorGUILayout.ObjectField(item, typeof(WorldSpriteAnimation), true) as WorldSpriteAnimation;
        }
        
        public void OnSceneGUI()
        {
            WorldSprite t = target as WorldSprite;

            if (t.Controllable && t.CanInteract)
            {
                Color color = Handles.color;
                Handles.color = m_interactionArcColor;
                bool enabled = GUI.enabled;
                if (!Selection.Contains(t.gameObject))
                {
                    GUI.enabled = false;
                    Handles.color = new Color(0f, 0f, 0f, 0.001f);
                }

                Vector2 position = t.CollisionCollider.bounds.center;
                
                Vector2 direction = t.GetVectorDirection();
                float radius = t.InteractionReachDistance;
                float fov = t.InteractionReachAngle;

                Vector2 rightLineFOV = RotatePointAroundTransform(direction.normalized * radius, -fov / 2);
                Vector2 leftLineFOV = RotatePointAroundTransform(direction.normalized * radius, fov / 2);

                Handles.DrawLine(position, position + (direction.normalized * radius));

                Handles.DrawLine(position, position + rightLineFOV);
                Handles.DrawLine(position, position + leftLineFOV);

                int smoothness = t.InteractionReachArcSmoothness;

                Vector2 p = rightLineFOV;
                for (int i = 1; i <= smoothness; i++)
                {
                    float step = fov / smoothness;
                    Vector2 p1 = RotatePointAroundTransform(direction.normalized * radius, -fov / 2 + step * (i));
                    Handles.DrawLine((position + p), (position + p) + (p1 - p));
                    p = p1;
                }
                
                Handles.color = color;
                GUI.enabled = enabled;
            }
        }

        private Vector2 RotatePointAroundTransform(Vector2 p, float angles)
        {
            return new Vector2(Mathf.Cos((angles) * Mathf.Deg2Rad) * (p.x) - Mathf.Sin((angles) * Mathf.Deg2Rad) * (p.y),
                                Mathf.Sin((angles) * Mathf.Deg2Rad) * (p.x) + Mathf.Cos((angles) * Mathf.Deg2Rad) * (p.y));
        }
    }
}
#endif