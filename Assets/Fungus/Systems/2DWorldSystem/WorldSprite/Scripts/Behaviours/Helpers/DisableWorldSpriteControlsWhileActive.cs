﻿using ImprobableStudios.BaseUtilities;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.WorldSystem2D
{
    [AddComponentMenu("2D World System/World Map/Disable World Sprite Control While Active")]
    public class DisableWorldSpriteControlsWhileActive : PersistentBehaviour
    {
        protected Dictionary<WorldSprite, bool> worldSpriteControllableStates = new Dictionary<WorldSprite, bool>();

        protected override void OnEnable()
        {
            base.OnEnable();

            worldSpriteControllableStates.Clear();
            foreach (WorldSprite worldSprite in GetAllActive<WorldSprite>())
            {
                worldSpriteControllableStates[worldSprite] = worldSprite.Controllable;
                worldSprite.Controllable = false;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            foreach (WorldSprite worldSprite in worldSpriteControllableStates.Keys)
            {
                if (!worldSprite.Controllable)
                {
                    worldSprite.Controllable = worldSpriteControllableStates[worldSprite];
                }
            }
        }
    }

}

