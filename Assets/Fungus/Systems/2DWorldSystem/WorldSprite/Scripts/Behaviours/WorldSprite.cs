using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.EventSystems;
using System.Linq;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace ImprobableStudios.WorldSystem2D
{

    public enum CardinalDirection
    {
        South,
        West,
        East,
        North
    }

    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
    [AddComponentMenu("2D World System/World Sprite")]
    public class WorldSprite : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class WorldSpriteSaveState : BehaviourSaveState<WorldSprite>
        {
            public override void Save(WorldSprite worldSprite)
            {
                base.Save(worldSprite);

                worldSprite.m_localPosition = worldSprite.transform.localPosition;
                if (worldSprite.SpriteRenderers.Length > 0)
                {
                    worldSprite.m_sortingLayerID = worldSprite.SpriteRenderers[0].sortingLayerID;
                }
            }

            public override void Load(WorldSprite worldSprite)
            {
                base.Load(worldSprite);
                
                if (worldSprite.m_animation != null)
                {
                    worldSprite.SetSpriteFrame(worldSprite.m_animation);
                    worldSprite.Resume();
                }

                if (worldSprite.m_emoteAnimation != null)
                {
                    worldSprite.PlayEmote();
                }
            }
        }
        
        [Tooltip("The number of pixels per unit")]
        [Min(1)]
        [Delayed]
        [SerializeField]
        protected int m_PixelsPerUnit = 32;

        [Tooltip("The skin that is being used by this world sprite")]
        [SerializeField]
        protected int m_Skin;

        [Tooltip("The direction the world sprite is facing")]
        [SerializeField]
        protected CardinalDirection m_Direction;

        [Tooltip("The speed at which all animations will be played")]
        [Min(0)]
        [SerializeField]
        protected float m_AnimationSpeed = 1f;

        [Tooltip("The collider that determines how close this world sprite can get to other colliders before colliding")]
        [ChildComponentPopup]
        [SerializeField]
        protected Collider2D m_CollisionCollider;

        [Tooltip("The collider that determines where the world sprite can be clicked to be interacted with")]
        [ChildComponentPopup]
        [SerializeField]
        protected Collider2D m_InteractionCollider;

        [Tooltip("Player can use keys to move this world sprite, and interact with the environment")]
        [SerializeField]
        [FormerlySerializedAs("m_IsControllable")]
        protected bool m_Controllable;

        [Tooltip("If true, allow the world sprite to interact with objects in the environment")]
        [SerializeField]
        protected bool m_CanInteract = true;

        [Tooltip("If true, allow the player to use the submit button defined in the active Standalone Input Module to interact with objects in the enviornment")]
        [SerializeField]
        protected bool m_UseSubmitButtonToInteract = true;

        [Tooltip("If true, allow the player to click on objects in the enviornment to interact with them")]
        [SerializeField]
        protected bool m_UseMouseClickToInteract = true;

        [Tooltip("Specifies how far (in units) the sprite can reach in the direction it is currently facing. The sprite will be able to interact with any colliders that are within reach")]
        [Min(0)]
        [SerializeField]
        protected float m_InteractionReachDistance = 0.8f;

        [Tooltip("Specifies the angle (in degrees) the sprite can reach around the direction it is currently facing. The sprite will be able to interact with any colliders that are within reach")]
        [Range(0, 360)]
        [SerializeField]
        protected int m_InteractionReachAngle = 45;

        [Tooltip("Specifies how smooth the interaction reach arc is in terms of how many points are used to create the arc (less points are more performant)")]
        [Min(1)]
        [SerializeField]
        protected int m_InteractionReachArcSmoothness = 20;

        [Tooltip("If true, the world sprite can move.")]
        [SerializeField]
        protected bool m_CanMove = true;
        
        [Tooltip("The world sprite animation that is played when the player uses controls to move the world sprite")]
        [ChildComponentPopup]
        [SerializeField]
        [FormerlySerializedAs("m_MoveWorldSpriteAnimation")]
        [FormerlySerializedAs("m_ControlledMovePixelAnimation")]
        protected WorldSpriteAnimation m_ControlledMoveAnimation;

        [Tooltip("If true, allow the player to use axis input (joystick, arrow keys, wasd, etc.) to move the world sprite")]
        [SerializeField]
        protected bool m_UseAxisInputToMove = true;

        [Tooltip("If true, allow the player to click to move the world sprite")]
        [SerializeField]
        protected bool m_UseMouseClickToMove = true;

        [Tooltip("If true, allow the world sprite to move diagonally")]
        [SerializeField]
        protected bool m_AllowDiagonalMovement = true;

        [Tooltip("If true, only allow the world sprite to move locked to the grid")]
        [SerializeField]
        protected bool m_LockMovementToGrid;
        
        [Tooltip("The move speed of the world sprite (in units/second)")]
        [Min(0)]
        [SerializeField]
        protected float m_MoveSpeed = 3f;

        [Tooltip("The skins that this world sprite can use")]
        [SerializeField]
        [FormerlySerializedAs("skinNames")]
        [FormerlySerializedAs("m_SkinNames")]
        [FormerlySerializedAs("m_Skins")]
        protected string[] m_SkinNames = new string[] { "Default" };

        protected Vector3 m_localPosition;
        
        protected int m_sortingLayerID;

        protected bool m_interruptAnimation;

        protected WorldSpriteAnimation m_animation;

        protected WorldSpriteAnimation m_emoteAnimation;

        protected UnityEvent m_onEmoteAnimationFinished = new UnityEvent();

        protected UnityEvent m_onNextEmoteAnimationFinished = new UnityEvent();

        protected UnityEvent m_onStepped = new UnityEvent();

        protected UnityEvent m_onNextStepped = new UnityEvent();

        protected UnityEvent m_onCollided = new UnityEvent();

        protected UnityEvent m_onNextCollided = new UnityEvent();

        protected UnityEvent m_onTriggered = new UnityEvent();

        protected UnityEvent m_onNextTriggered = new UnityEvent();

        protected UnityEvent m_onInteracted = new UnityEvent();

        protected UnityEvent m_onNextInteracted = new UnityEvent();

        protected UnityEvent m_onTurned = new UnityEvent();

        protected UnityEvent m_onNextTurned = new UnityEvent();

        protected UnityEvent m_onBeganMove = new UnityEvent();

        protected UnityEvent m_onMove = new UnityEvent();

        protected UnityEvent m_onEndMove = new UnityEvent();
        
        protected bool m_collided;
        
        protected bool m_triggered;
        
        protected Collider2D m_lastCollidedCollider;
        
        protected Collider2D m_lastTriggeredCollider;
        
        protected Collider2D m_lastInteractedCollider;
        
        protected Collider2D m_lastClickedCollider;

        protected Vector2 m_lastMouseClickWorldPosition;
        
        protected Vector2 m_lastMouseClickMoveVelocity;
        
        protected bool m_isMoving;
        
        protected Vector2 m_velocity;
        
        protected bool m_isMovingTowardsMouseClick;

        private WorldSprite m_emote;
        
        private SpriteRenderer[] m_spriteRenderers;

        private Rigidbody2D m_rigidbody2D;

        private PolygonCollider2D m_interactionReachCollider;

        private int m_previewSkinIndex;

        private float m_previewSpeed;

        private CardinalDirection m_previewDirection;

        private int m_previewColumn;

        private WorldSpriteAnimation m_previewAnimation;

        private WorldSpriteAnimation[] m_animations;

#if UNITY_EDITOR
        private KeyValuePair<Sprite, UnityEditor.TextureImporter>[] m_textureImporters;
#endif
        
        public int PixelsPerUnit
        {
            get
            {
                return m_PixelsPerUnit;
            }
            set
            {
                m_PixelsPerUnit = value;
            }
        }

        public int Skin
        {
            get
            {
                return m_Skin;
            }
            set
            {
                ChangeSkin(value);
            }
        }

        public CardinalDirection Direction
        {
            get
            {
                return m_Direction;
            }
            set
            {
                ChangeDirection(value);
            }
        }
        
        public float AnimationSpeed
        {
            get
            {
                return m_AnimationSpeed;
            }
            set
            {
                m_AnimationSpeed = value;
            }
        }
        
        public bool Controllable
        {
            get
            {
                return m_Controllable;
            }
            set
            {
                m_Controllable = value;
                
                UpdateRigidBody2DSettings();

                if (!m_Controllable)
                {
                    StopMoving();
                }
            }
        }

        public bool CanInteract
        {
            get
            {
                return m_CanInteract;
            }
            set
            {
                m_CanInteract = value;
            }
        }

        public bool UseSubmitButtonToInteract
        {
            get
            {
                return m_UseSubmitButtonToInteract;
            }
            set
            {
                m_UseSubmitButtonToInteract = value;
            }
        }

        public bool UseMouseClickToInteract
        {
            get
            {
                return m_UseMouseClickToInteract;
            }
            set
            {
                m_UseMouseClickToInteract = value;
            }
        }

        public bool CanMove
        {
            get
            {
                return m_CanMove;
            }
            set
            {
                m_CanMove = value;

                UpdateRigidBody2DSettings();

                if (!m_CanMove)
                {
                    StopMoving();
                }
            }
        }

        public WorldSpriteAnimation ControlledMoveAnimation
        {
            get
            {
                return m_ControlledMoveAnimation;
            }
            set
            {
                m_ControlledMoveAnimation = value;
            }
        }

        public bool UseAxisInputToMove
        {
            get
            {
                return m_UseAxisInputToMove;
            }
            set
            {
                m_UseAxisInputToMove = value;
            }
        }

        public bool UseMouseClickToMove
        {
            get
            {
                return m_UseMouseClickToMove;
            }
            set
            {
                m_UseMouseClickToMove = value;
            }
        }
        
        public bool AllowDiagonalMovement
        {
            get
            {
                return m_AllowDiagonalMovement;
            }
            set
            {
                m_AllowDiagonalMovement = value;
            }
        }

        public bool LockMovementToGrid
        {
            get
            {
                return m_LockMovementToGrid;
            }
            set
            {
                m_LockMovementToGrid = value;
            }
        }

        public float MoveSpeed
        {
            get
            {
                return m_MoveSpeed;
            }
            set
            {
                m_MoveSpeed = value;
            }
        }
        
        public float InteractionReachDistance
        {
            get
            {
                return m_InteractionReachDistance;
            }
            set
            {
                m_InteractionReachDistance = value;
                SetupInteractionReachCollider();
            }
        }

        public int InteractionReachAngle
        {
            get
            {
                return m_InteractionReachAngle;
            }
            set
            {
                m_InteractionReachAngle = value;
                SetupInteractionReachCollider();
            }
        }

        public int InteractionReachArcSmoothness
        {
            get
            {
                return m_InteractionReachArcSmoothness;
            }
            set
            {
                m_InteractionReachArcSmoothness = value;
                SetupInteractionReachCollider();
            }
        }

        public Collider2D CollisionCollider
        {
            get
            {
                return m_CollisionCollider;
            }
            set
            {
                m_CollisionCollider = value;
                if (m_CollisionCollider != null)
                {
                    m_CollisionCollider.isTrigger = false;
                }
                if (m_InteractionCollider != null && m_InteractionCollider != m_CollisionCollider)
                {
                    m_InteractionCollider.isTrigger = true;
                }
            }
        }

        public Collider2D InteractionCollider
        {
            get
            {
                return m_InteractionCollider;
            }
            set
            {
                m_InteractionCollider = value;
                if (m_CollisionCollider != null)
                {
                    m_CollisionCollider.isTrigger = false;
                }
                if (m_InteractionCollider != null && m_InteractionCollider != m_CollisionCollider)
                {
                    m_InteractionCollider.isTrigger = true;
                }
            }
        }

        public string[] SkinNames
        {
            get
            {
                return m_SkinNames;
            }
            set
            {
                m_SkinNames = value;
            }
        }

        public Collider2D LastCollidedCollider
        {
            get
            {
                return m_lastCollidedCollider;
            }
        }

        public Collider2D LastTriggeredCollider
        {
            get
            {
                return m_lastTriggeredCollider;
            }
        }

        public Collider2D LastInteractedCollider
        {
            get
            {
                return m_lastInteractedCollider;
            }
        }
        
        public SpriteRenderer[] SpriteRenderers
        {
            get
            {
                if (m_spriteRenderers == null)
                {
                    m_spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>(true);
                }
                return m_spriteRenderers.Where(x => x != null).ToArray();
            }
        }

        public Rigidbody2D Rigidbody2D
        {
            get
            {
                if (m_rigidbody2D == null)
                {
                    m_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
                }
                return m_rigidbody2D;
            }
        }
        
        public PolygonCollider2D InteractionReachCollider
        {
            get
            {
                if (m_interactionReachCollider == null)
                {
                    GameObject interactionReachGameObject = new GameObject();
                    interactionReachGameObject.transform.SetParent(transform);
                    interactionReachGameObject.transform.localPosition = Vector3.zero;
                    interactionReachGameObject.transform.localEulerAngles = Vector3.zero;
                    interactionReachGameObject.transform.localScale = Vector3.one;
                    m_interactionReachCollider = interactionReachGameObject.AddComponent<PolygonCollider2D>();
                    m_interactionReachCollider.isTrigger = true;
                    m_interactionReachCollider.gameObject.layer = 2;
                    SetupInteractionReachCollider();
                }
                return m_interactionReachCollider;
            }
        }

        public UnityEvent OnEmoteAnimationFinished
        {
            get
            {
                return m_onEmoteAnimationFinished;
            }
        }

        public UnityEvent OnNextEmoteAnimationFinished
        {
            get
            {
                return m_onNextEmoteAnimationFinished;
            }
        }

        public UnityEvent OnStepped
        {
            get
            {
                return m_onStepped;
            }
        }

        public UnityEvent OnNextStepped
        {
            get
            {
                return m_onNextStepped;
            }
        }

        public UnityEvent OnCollided
        {
            get
            {
                return m_onCollided;
            }
        }

        public UnityEvent OnNextCollided
        {
            get
            {
                return m_onNextCollided;
            }
        }

        public UnityEvent OnTriggered
        {
            get
            {
                return m_onTriggered;
            }
        }

        public UnityEvent OnNextTriggered
        {
            get
            {
                return m_onNextTriggered;
            }
        }

        public UnityEvent OnInteracted
        {
            get
            {
                return m_onInteracted;
            }
        }

        public UnityEvent OnNextInteracted
        {
            get
            {
                return m_onNextInteracted;
            }
        }

        public UnityEvent OnTurned
        {
            get
            {
                return m_onTurned;
            }
        }

        public UnityEvent OnNextTurned
        {
            get
            {
                return m_onNextTurned;
            }
        }

        public UnityEvent OnBeganMove
        {
            get
            {
                return m_onBeganMove;
            }
        }

        public UnityEvent OnMove
        {
            get
            {
                return m_onMove;
            }
        }

        public UnityEvent OnEndMove
        {
            get
            {
                return m_onEndMove;
            }
        }

#if UNITY_EDITOR
        private KeyValuePair<Sprite, UnityEditor.TextureImporter>[] TextureImporters
        {
            get
            {
                if (m_textureImporters == null)
                {
                    m_textureImporters = GetTextureImporters();
                }
                return m_textureImporters;
            }
        }
#endif

        public int PreviewSkinIndex
        {
            get
            {
                return m_previewSkinIndex;
            }
            set
            {
                m_previewSkinIndex = value;
            }
        }

        public CardinalDirection PreviewDirection
        {
            get
            {
                return m_previewDirection;
            }
            set
            {
                m_previewDirection = value;
            }
        }

        public float PreviewSpeed
        {
            get
            {
                return m_previewSpeed;
            }
            set
            {
                m_previewSpeed = value;
            }
        }

        public WorldSpriteAnimation PreviewAnimation
        {
            get
            {
                return m_previewAnimation;
            }
            set
            {
                m_previewAnimation = value;
            }
        }

        public int PreviewColumn
        {
            get
            {
                return m_previewColumn;
            }
            set
            {
                m_previewColumn = value;
            }
        }

        protected int PreviewRow
        {
            get
            {
                return PreviewAnimation.ConvertDirectionToRow(PreviewDirection);
            }
            set
            {
                PreviewDirection = PreviewAnimation.ConvertRowToDirection(value);
            }
        }

        public int PreviewFrameIndex
        {
            get
            {
                if (PreviewAnimation != null)
                {
                    return PreviewRow * PreviewAnimation.NumberOfColumns + PreviewColumn;
                }
                return 0;
            }
        }

        public Sprite PreviewFrameSprite
        {
            get
            {
                if (PreviewAnimation != null &&
                    PreviewSkinIndex >= 0 && PreviewSkinIndex < PreviewAnimation.Skins.Length &&
                    PreviewFrameIndex >= 0 && PreviewFrameIndex < PreviewAnimation.Skins[PreviewSkinIndex].m_Frames.Length)
                {
                    return PreviewAnimation.Skins[PreviewSkinIndex].m_Frames[PreviewFrameIndex];
                }
                return null;
            }
        }

        public WorldSpriteAnimation[] Animations
        {
            get
            {
                UpdateCachedAnimations();
                return m_animations;
            }
        }
        
        public virtual SaveState GetSaveState()
        {
            return new WorldSpriteSaveState();
        }

        protected override void Reset()
        {
            base.Reset();
            
            Validate();
            
            UpdateSpriteSortingOrder();
            
            UpdateCachedAnimations();
        }
        
        protected override void Awake()
        {
            base.Awake();

            m_localPosition = transform.localPosition;
            if (SpriteRenderers.Length > 0)
            {
                m_sortingLayerID = SpriteRenderers[0].sortingLayerID;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Validate();
            
            UpdateSpriteSortingOrder();

            SetupInteractionReachCollider();
        }
        
        protected virtual void FixedUpdate()
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if (m_Controllable)
            {
                Vector2 velocity = Vector2.zero;

                if (m_CanMove)
                {
                    if (m_UseMouseClickToMove)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                            Vector2 clickHeading = (mouseWorldPosition - (Vector2)m_CollisionCollider.bounds.center).normalized;
                            velocity = clickHeading;

                            m_isMovingTowardsMouseClick = true;
                            m_lastMouseClickWorldPosition = mouseWorldPosition;
                            m_lastMouseClickMoveVelocity = velocity;
                        }

                        if (m_isMovingTowardsMouseClick)
                        {
                            Debug.DrawLine(m_CollisionCollider.bounds.center, m_lastMouseClickWorldPosition, Color.green);
                            velocity = m_lastMouseClickMoveVelocity;
                        }
                    }
                    
                    if (m_isMovingTowardsMouseClick && Vector2.Distance(m_CollisionCollider.bounds.center, m_lastMouseClickWorldPosition) < 0.1f)
                    {
                        velocity = Vector2.zero;
                    }

                    if (m_UseAxisInputToMove)
                    {
                        float horizontalInput = Input.GetAxisRaw("Horizontal");
                        float verticalInput = Input.GetAxisRaw("Vertical");
                        Vector2 axisHeading = new Vector2(horizontalInput, verticalInput).normalized;

                        if (!m_isMovingTowardsMouseClick || axisHeading != Vector2.zero)
                        {
                            m_isMovingTowardsMouseClick = false;
                            m_lastMouseClickWorldPosition = Vector2.zero;
                            m_lastMouseClickMoveVelocity = Vector2.zero;
                            if (m_AllowDiagonalMovement || !IsVelocityDiagonal(velocity))
                            {
                                velocity = axisHeading;
                            }
                        }
                    }
                    
                    if (m_LockMovementToGrid)
                    {
                        if (velocity != Vector2.zero)
                        {
                            if (!m_isMoving)
                            {
                                ChangeDirection(GetCardinalDirectionFromVelocity(velocity));

                                Vector2 origPosition = transform.position;
                                Vector2 targetPosition = origPosition + velocity;
                                Vector2 snappedTargetPosition = new Vector2((float)Math.Round(targetPosition.x), (float)Math.Round(targetPosition.y));

                                Collider2D hitCollider = WillCollide(snappedTargetPosition);
                                if (hitCollider == null)
                                {
                                    MoveAnimation(m_ControlledMoveAnimation, snappedTargetPosition, true);
                                }
                                else
                                {
                                    m_lastCollidedCollider = hitCollider;
                                    InvokeOnCollide();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (m_velocity != velocity)
                        {
                            if (velocity == Vector2.zero)
                            {
                                StopMoving();
                            }
                            else
                            {
                                StartMoving(velocity);
                            }
                        }
                    }

                    if (m_velocity == Vector2.zero && velocity != Vector2.zero)
                    {
                        InvokeOnBeganMove();
                    }
                    if (m_velocity != Vector2.zero && velocity == Vector2.zero)
                    {
                        InvokeOnEndMove();
                    }
                    if (velocity != Vector2.zero)
                    {
                        InvokeOnMove();
                    }
                }
                m_velocity = velocity;

                if (m_CanInteract)
                {
                    bool interacted = false;

                    if (m_UseMouseClickToInteract)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            m_lastClickedCollider = GetClosestColliderAtMouseClick();

                            if (m_lastClickedCollider != null)
                            {
                                if (!m_isMovingTowardsMouseClick)
                                {
                                    if (Array.IndexOf(GetCollidersWithinInteractionReach(), m_lastClickedCollider) >= 0)
                                    {
                                        m_lastInteractedCollider = m_lastClickedCollider;
                                        interacted = true;
                                    }
                                }
                            }
                        }
                    }

                    if (m_UseSubmitButtonToInteract)
                    {
                        if (EventSystem.current != null)
                        {
                            StandaloneInputModule standaloneInputModule = EventSystem.current.currentInputModule as StandaloneInputModule;
                            if (standaloneInputModule != null)
                            {
                                if (Input.GetButtonDown(standaloneInputModule.submitButton))
                                {
                                    Collider2D hitCollider = GetClosestColliderWithinInteractionReach();

                                    if (hitCollider != null)
                                    {
                                        if (!m_isMovingTowardsMouseClick)
                                        {
                                            m_lastInteractedCollider = hitCollider;
                                            interacted = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (interacted)
                    {
                        InvokeOnInteract();
                    }
                }
            }
            
            if (Rigidbody2D.bodyType != RigidbodyType2D.Static)
            {
                if (!m_LockMovementToGrid)
                {
                    Vector3 targetPosition = Rigidbody2D.position + (m_velocity * m_MoveSpeed * m_AnimationSpeed * Time.fixedDeltaTime);
                    Rigidbody2D.MovePosition(targetPosition);
                }
            }
        }

        protected virtual void LateUpdate()
        {
            UpdateSpriteSortingOrder();
        }

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if (m_LockMovementToGrid)
            {
                return;
            }

            if (m_collided)
            {
                return;
            }

            m_collided = true;

            m_lastCollidedCollider = collision.collider;

            if (m_isMovingTowardsMouseClick && !Input.GetMouseButtonDown(0))
            {
                StopMoving();
            }
            else if (m_Controllable && m_CanInteract && m_lastClickedCollider != null)
            {
                if (m_isMovingTowardsMouseClick && Array.IndexOf(GetCollidersWithinInteractionReach(), m_lastClickedCollider) >= 0)
                {
                    StopMoving();
                    InvokeOnInteract();
                }
            }

            InvokeOnCollide();
        }

        protected virtual void OnCollisionExit2D(Collision2D collision)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if (Rigidbody2D.bodyType == RigidbodyType2D.Dynamic)
            {
                m_collided = false;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D collider)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if (m_triggered)
            {
                return;
            }
            
            m_triggered = true;

            m_lastTriggeredCollider = collider;
            InvokeOnTrigger();

            if (!m_LockMovementToGrid)
            {
                InvokeOnStep();
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D collider)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if (Rigidbody2D.bodyType == RigidbodyType2D.Dynamic)
            {
                m_triggered = false;
            }
        }
        
        public void Validate()
        {
            if (m_CollisionCollider == null)
            {
                m_CollisionCollider = GetComponent<Collider2D>();
                if (m_CollisionCollider == null)
                {
                    m_CollisionCollider = gameObject.AddComponent<CircleCollider2D>();
                }
            }
            if (m_InteractionCollider == null)
            {
                m_InteractionCollider = GetComponent<Collider2D>();
                if (m_InteractionCollider == null)
                {
                    m_InteractionCollider = gameObject.AddComponent<CircleCollider2D>();
                }
            }
            if (m_CollisionCollider != null)
            {
                m_CollisionCollider.isTrigger = false;
            }
            if (m_InteractionCollider != null && m_InteractionCollider != m_CollisionCollider)
            {
                m_InteractionCollider.isTrigger = true;
            }
            
            if (m_ControlledMoveAnimation != null)
            {
                foreach (WorldSpriteAnimation wsa in Animations)
                {
                    if (wsa == m_ControlledMoveAnimation)
                    {
                        wsa.gameObject.SetActive(true);
                    }
                    else
                    {
                        wsa.gameObject.SetActive(false);
                    }
                }
            }

            if (m_InteractionReachDistance < 0)
            {
                m_InteractionReachDistance = 0;
            }

            UpdateRigidBody2DSettings();

#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                if (TextureImportSettingsNeedsUpdate())
                {
                    UpdateTextureImportSettings();
                }
            }
#endif
        }

        public void UpdateCachedAnimations()
        {
            List<WorldSpriteAnimation> animations = new List<WorldSpriteAnimation>();
            foreach (Transform child in transform)
            {
                animations.AddRange(child.GetComponents<WorldSpriteAnimation>());
            }
            m_animations = animations.ToArray();
        }

        public void UpdateSpriteSortingOrder()
        {
            foreach (SpriteRenderer spriteRenderer in SpriteRenderers)
            {
                if (spriteRenderer != null)
                {
                    spriteRenderer.sortingOrder = (int)(CollisionCollider.bounds.min.y * -100);

                    if (spriteRenderer != null)
                    {
                        if (spriteRenderer.transform != transform)
                        {
                            Vector3 newPosition = spriteRenderer.transform.localPosition;
                            newPosition.z = (spriteRenderer.transform.GetSiblingIndex() + 1) * -0.00001f;
                            spriteRenderer.transform.localPosition = newPosition;
                        }
                    }
                }
            }
        }

        public void UpdateRigidBody2DSettings()
        {
            Rigidbody2D rigidbody2D = Rigidbody2D;
            
            if (m_Controllable && m_CanMove)
            {
                rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            }
            else
            {
                rigidbody2D.bodyType = RigidbodyType2D.Static;
            }

            rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;

            rigidbody2D.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            rigidbody2D.mass = float.MaxValue;
            rigidbody2D.drag = 0f;
            rigidbody2D.angularDrag = 0f;
            rigidbody2D.gravityScale = 0f;
        }

        public Collider2D WillCollide(Vector2 targetPosition)
        {
            return WillCollide((targetPosition - (Vector2)transform.position).normalized, Vector2.Distance(transform.position, targetPosition));
        }

        public Collider2D WillCollide(Vector2 velocity, float distance)
        {
            ContactFilter2D contactFilter = new ContactFilter2D();
            contactFilter.useTriggers = false;
            contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            contactFilter.useLayerMask = true;
            RaycastHit2D[] hitBuffer = new RaycastHit2D[1];
            CollisionCollider.Cast(velocity, contactFilter, hitBuffer, distance);
            Debug.DrawRay(transform.position, velocity * distance, Color.yellow, 0.2f);
            return hitBuffer[0].collider;
        }

        public Collider2D GetClosestColliderAtMouseClick()
        {
            Collider2D[] collidersAtMouseClick = GetCollidersAtMouseClick();
            if (collidersAtMouseClick.Length > 0)
            {
                return collidersAtMouseClick[0];
            }

            return null;
        }

        public Collider2D[] GetCollidersAtMouseClick()
        {
            Vector2 startPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = Vector2.zero;
            float distance = 0f;
            RaycastHit2D[] hits = Physics2D.RaycastAll(startPosition, direction, distance);
            List<Collider2D> collidersAtMouseClick = new List<Collider2D>();
            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit2D hit = hits[i];
                if (hit.collider != m_CollisionCollider && hit.collider != m_InteractionCollider)
                {
                    collidersAtMouseClick.Add(hit.collider);
                }
            }
            return collidersAtMouseClick.ToArray();
        }

        public Collider2D GetClosestColliderWithinInteractionReach()
        {
            Collider2D[] collidersWithinInteractionReach = GetCollidersWithinInteractionReach();
            if (collidersWithinInteractionReach.Length > 0)
            {
                return collidersWithinInteractionReach[0];
            }

            return null;
        }

        public Collider2D[] GetCollidersWithinInteractionReach()
        {
            List<Collider2D> collidersWithinReach = new List<Collider2D>();
            Collider2D[] collidersInView = new Collider2D[100];
            InteractionReachCollider.GetContacts(collidersInView);
            for (int i = 0; i < collidersInView.Length; i++)
            {
                Collider2D collider = collidersInView[i];
                if (collider != m_CollisionCollider && collider != m_InteractionCollider)
                {
                    collidersWithinReach.Add(collider);
                }
            }

            return collidersWithinReach.ToArray();
        }

        public void SetupInteractionReachCollider()
        {
            if (Application.isPlaying)
            {
                List<Vector2> points = new List<Vector2>();

                float radius = m_InteractionReachDistance;
                float totalAngle = m_InteractionReachAngle;
                int smoothness = m_InteractionReachArcSmoothness;
                float rotationAngle = GetRotationAngle() - (totalAngle / 2);

                if (totalAngle % 360 != 0)
                {
                    points.Add(Vector2.zero);
                }

                for (int i = 0; i <= smoothness; i++)
                {
                    float x = radius * Mathf.Cos(rotationAngle * Mathf.Deg2Rad);
                    float y = radius * Mathf.Sin(rotationAngle * Mathf.Deg2Rad);

                    points.Add(new Vector2(x, y));

                    rotationAngle += (float)totalAngle / smoothness;
                }

                if (totalAngle % 360 != 0)
                {
                    points.Add(Vector2.zero);
                }

                InteractionReachCollider.points = points.ToArray();
                InteractionReachCollider.offset = m_CollisionCollider.transform.InverseTransformPoint(m_CollisionCollider.bounds.center);
            }
        }

        public Vector2 GetVectorDirection()
        {
            return GetVectorDirectionFromCardinalDirection(m_Direction);
        }

        public static Vector2 GetVectorDirectionFromCardinalDirection(CardinalDirection direction)
        {
            if (direction == CardinalDirection.South)
            {
                return Vector2.down;
            }
            else if (direction == CardinalDirection.West)
            {
                return Vector2.left;
            }
            else if (direction == CardinalDirection.East)
            {
                return Vector2.right;
            }
            else if (direction == CardinalDirection.North)
            {
                return Vector2.up;
            }
            Debug.LogError("Invalid cardinal direction: " + direction);
            return Vector2.zero;
        }

        protected int GetRotationAngle()
        {
            return GetRotationAngleFromCardinalDirection(m_Direction);
        }

        public static int GetRotationAngleFromCardinalDirection(CardinalDirection direction)
        {
            if (direction == CardinalDirection.South)
            {
                return 270;
            }
            else if (direction == CardinalDirection.West)
            {
                return 180;
            }
            else if (direction == CardinalDirection.East)
            {
                return 0;
            }
            else if (direction == CardinalDirection.North)
            {
                return 90;
            }
            Debug.LogError("Invalid cardinal direction: " + direction);
            return -1;
        }

        public CardinalDirection GetCardinalDirectionFromVelocity(Vector2 velocity)
        {
            float[] angles = new float[4];
            angles[0] = Vector2.Angle(Vector2.down, velocity);
            angles[1] = Vector2.Angle(Vector2.left, velocity);
            angles[2] = Vector2.Angle(Vector2.right, velocity);
            angles[3] = Vector2.Angle(Vector2.up, velocity);
            
            float minAngle = float.MaxValue;
            CardinalDirection direction = CardinalDirection.South;
            for (int i = 0; i < angles.Length; i++)
            {
                float angle = angles[i];
                if (angle < minAngle)
                {
                    minAngle = angle;
                    direction = (CardinalDirection)i;
                }
                else if (angle == minAngle)
                {
                    if (m_Direction != (CardinalDirection)i)
                    {
                        direction = (CardinalDirection)i;
                    }
                }
            }

            return direction;
        }

        public bool IsVelocityDiagonal(Vector2 velocity)
        {
            return (velocity.x != 0 && velocity.y != 0);
        }

        public void Interrupt()
        {
            if (transform != null)
            {
                DOTween.Kill(transform);
            }

            if (m_animation != null)
            {
                foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
                {
                    animationLayer.Interrupt();
                }
            }
        }

        public virtual void Goto(int frame, int loopCount)
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.Goto(frame, loopCount);
            }
        }

        public virtual void Pause()
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.Pause();
            }
        }

        public virtual void Resume()
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.Resume();
            }
        }

        public virtual void Stop()
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.Stop();
            }
        }

        public virtual void Complete()
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.Complete();
            }
        }
        
        public void SetSpriteFrame(WorldSpriteAnimation worldSpriteAnimation, int column = -1, WorldSpriteAnimationProgression animationType = WorldSpriteAnimationProgression.Static, bool interrupt = true)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            ActivateAnimation(worldSpriteAnimation, interrupt);
            
            foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
            {
                if (column < 0)
                {
                    animationLayer.SetSpriteFrame(m_Skin, m_Direction);
                }
                else
                {
                    animationLayer.SetSpriteFrame(m_Skin, m_Direction, column, animationType);
                }
            }
        }

        public void ActivateAnimation(WorldSpriteAnimation worldSpriteAnimation, bool interrupt = true)
        {
            if (interrupt)
            {
                Interrupt();
            }

            gameObject.SetActive(true);

            foreach (WorldSpriteAnimation wsa in Animations)
            {
                if (wsa == worldSpriteAnimation)
                {
                    wsa.gameObject.SetActive(true);
                }
                else
                {
                    wsa.gameObject.SetActive(false);
                }
            }
        }

        public void ShowStaticFrame(WorldSpriteAnimation worldSpriteAnimation, int frameIndex, float duration, bool interrupt = true, UnityAction onComplete = null)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            ActivateAnimation(worldSpriteAnimation, interrupt);

            foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
            {
                if (animationLayer == worldSpriteAnimation)
                {
                    animationLayer.ShowStaticFrame(m_Skin, m_Direction, frameIndex, duration, onComplete);
                }
                else
                {
                    animationLayer.ShowStaticFrame(m_Skin, m_Direction, frameIndex, duration);
                }
            }
        }
        
        public void PlayAnimationForward(WorldSpriteAnimation worldSpriteAnimation, bool interrupt = true, UnityAction onComplete = null)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            ActivateAnimation(worldSpriteAnimation, interrupt);
            
            foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
            {
                if (animationLayer == worldSpriteAnimation)
                {
                    animationLayer.PlayAnimationForward(m_Skin, m_Direction, m_AnimationSpeed, onComplete);
                }
                else
                {
                    animationLayer.PlayAnimationForward(m_Skin, m_Direction, m_AnimationSpeed);
                }
            }
        }
        
        public void PlayAnimationBackwards(WorldSpriteAnimation worldSpriteAnimation, bool interrupt = true, UnityAction onComplete = null)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            ActivateAnimation(worldSpriteAnimation, interrupt);

            foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
            {
                if (animationLayer == worldSpriteAnimation)
                {
                    animationLayer.PlayAnimationBackwards(m_Skin, m_Direction, m_AnimationSpeed, onComplete);
                }
                else
                {
                    animationLayer.PlayAnimationBackwards(m_Skin, m_Direction, m_AnimationSpeed);
                }
            }
        }
        
        public void LoopAnimation(WorldSpriteAnimation worldSpriteAnimation, int loopCount, bool backwards, bool interrupt = true, UnityAction onComplete = null)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            ActivateAnimation(worldSpriteAnimation, interrupt);

            foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
            {
                if (animationLayer == worldSpriteAnimation)
                {
                    animationLayer.LoopAnimation(m_Skin, m_Direction, m_AnimationSpeed, loopCount, backwards, onComplete);
                }
                else
                {
                    animationLayer.LoopAnimation(m_Skin, m_Direction, m_AnimationSpeed, loopCount, backwards);
                }
            }
        }

        public void EndLoopAnimation()
        {
            foreach (WorldSpriteAnimation animationLayer in m_animation.AnimationLayers)
            {
                animationLayer.EndLoopAnimation();
            }
        }

        public void MoveAnimation(WorldSpriteAnimation worldSpriteAnimation, Vector2 position, bool interrupt = true, UnityAction onComplete = null)
        {
            m_animation = worldSpriteAnimation;
            m_interruptAnimation = interrupt;

            m_ControlledMoveAnimation = worldSpriteAnimation;
            m_isMoving = true;
            m_localPosition = position;

            if (onComplete != null)
            {
                OnNextStepped.AddPersistentListener(onComplete);
            }

            PlayMoveAnimation();
        }

        public void PlayMoveAnimation()
        {
            WorldSpriteAnimation worldSpriteAnimation = m_animation;
            Vector3 position = m_localPosition;
            bool interrupt = m_interruptAnimation;

            ActivateAnimation(worldSpriteAnimation, interrupt);

            int rowLength = worldSpriteAnimation.GetRowLength(worldSpriteAnimation.LoopFrameIndices.Length);
            int startColumn = 0;
            if (worldSpriteAnimation.StartAndEndLoopAtMiddle)
            {
                startColumn = rowLength / 2;
            }

            float steps = CalculateSteps(position);

            if (steps > 0)
            {
                SetSpriteFrame(worldSpriteAnimation, startColumn, WorldSpriteAnimationProgression.Loop);

                int loopCount = (int)Math.Ceiling(steps);

                foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
                {
                    animationLayer.LoopAnimation(m_Skin, m_Direction, m_AnimationSpeed, loopCount, false);
                }

                transform.DOLocalMove(position, m_MoveSpeed * m_AnimationSpeed).SetSpeedBased(true).SetEase(Ease.Linear).OnComplete(CompleteMoveAnimation);
            }
            else
            {
                m_localPosition = transform.localPosition;
                CompleteMoveAnimation();
            }
        }

        private void CompleteMoveAnimation()
        {
            transform.localPosition = m_localPosition;
            
            m_isMoving = false;

            InvokeOnStep();
        }

        public void StopMoving()
        {
            if (m_isMoving)
            {
                m_isMoving = false;

                m_isMovingTowardsMouseClick = false;
                m_lastMouseClickWorldPosition = Vector2.zero;
                m_lastMouseClickMoveVelocity = Vector2.zero;

                EndLoopAnimation();
            }

            m_velocity = Vector2.zero;
        }

        public void StartMoving(Vector2 newVelocity)
        {
            CardinalDirection direction = GetCardinalDirectionFromVelocity(newVelocity);
            if (m_Direction != direction)
            {
                ChangeDirection(direction, true);
                SetupInteractionReachCollider();
            }
            
            if (!m_isMoving)
            {
                m_isMoving = true;

                LoopAnimation(m_ControlledMoveAnimation, -1, false);
            }
        }

        public void Emote(WorldSpriteAnimation worldSpriteAnimation, UnityAction onComplete = null)
        {
            m_emoteAnimation = worldSpriteAnimation;

            if (onComplete != null)
            {
                OnNextEmoteAnimationFinished.AddPersistentListener(onComplete);
            }

            PlayEmote();
        }

        private void PlayEmote()
        {
            float tweenSpeed = 1.2f;

            WorldSpriteAnimation worldSpriteAnimation = m_emoteAnimation;

            if (m_emote == null)
            {
                m_emote = InstantiateAndRegister(worldSpriteAnimation.WorldSprite);
                GameObject emoteParent = new GameObject("Emote");
                m_emote.transform.SetParent(emoteParent.transform);
                emoteParent.transform.SetParent(transform);
                m_emote.gameObject.SetActive(true);
            }

            int animationIndex = Array.IndexOf(worldSpriteAnimation.WorldSprite.Animations, worldSpriteAnimation);
            worldSpriteAnimation = m_emote.Animations[animationIndex];

            // World Sprite's always have their pivot in the bottom left
            // This means when you scale them up they expand to the top right.
            // However when we scale emote world sprites, we want them to scale centrally to give a "pop" effect
            // So as a workaround we make the world sprite a child of an empty game object,
            // offset the world sprite's local position by 0.5 and scale the empty game object instead.
            m_emote.transform.parent.transform.localScale = Vector3.one;
            m_emote.transform.localScale = Vector3.one;
            float yOffset = m_InteractionCollider.bounds.size.y;
            m_emote.transform.parent.transform.localPosition = new Vector3(0.5f, yOffset + 0.5f, 0);
            m_emote.transform.localPosition = new Vector3(-0.5f, -0.5f, 0);
            m_emote.transform.parent.transform.DOScale(new Vector3(0.5f, 0.5f, m_emote.transform.parent.transform.localScale.z), tweenSpeed).SetEase(Ease.OutElastic);
            
            m_emote.m_Direction = CardinalDirection.South;
            if (m_emote.isActiveAndEnabled)
            {
                m_emote.PlayAnimationForward(
                    worldSpriteAnimation, 
                    true, 
                    () =>
                    {
                        m_emote.LoopAnimation(worldSpriteAnimation, 1, false);
                        m_emote.transform.parent.transform.DOScale(new Vector3(0f, 0f, 0f), tweenSpeed).SetEase(Ease.InElastic)
                        .OnComplete(CompleteEmote);
                    });
            }
        }

        private void CompleteEmote()
        {
            m_emote.gameObject.SetActive(false);
            m_emoteAnimation = null;

            InvokeOnEmoteAnimationFinished();
        }

        public void SetPosition(Vector2 position)
        {
            m_localPosition = position;
            transform.localPosition = position;
        }

        public Vector2 CalculateNewPosition(CardinalDirection moveDirection, float steps)
        {
            Vector2 position = new Vector2(m_localPosition.x, m_localPosition.y);
            switch (moveDirection)
            {
                case CardinalDirection.South: // v
                    position.y -= steps;
                    break;
                case CardinalDirection.West:  // <
                    position.x -= steps;
                    break;
                case CardinalDirection.East:  // >
                    position.x += steps;
                    break;
                case CardinalDirection.North: // ^
                    position.y += steps;
                    break;
            }
            return position;
        }

        public float CalculateSteps(Vector2 position)
        {
            return Math.Max(Math.Abs(position.x - transform.localPosition.x), Math.Abs(position.y - transform.localPosition.y));
        }

        public float CalculateDistance(Vector2 position)
        {
            return Vector2.Distance(transform.localPosition, position);
        }

        public CardinalDirection CalculateMoveDirection(Vector2 position)
        {
            float xDistance = position.x - transform.localPosition.x;
            float yDistance = position.y - transform.localPosition.y;
            if (xDistance != 0)
            {
                if (xDistance > 0)
                {
                    return CardinalDirection.East;
                }
                else
                {
                    return CardinalDirection.West;
                }
            }
            if (yDistance != 0)
            {
                if (yDistance > 0)
                {
                    return CardinalDirection.North;
                }
                else
                {
                    return CardinalDirection.South;
                }
            }
            return CardinalDirection.South;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void TurnToward(Collider2D targetCollider, bool updateSpriteFrame = true)
        {
            CardinalDirection facing = DirectionToFace(targetCollider);
            ChangeDirection(facing, updateSpriteFrame);
        }

        public void ChangeDirection(CardinalDirection facing, bool updateSpriteFrame = true)
        {
            m_Direction = facing;
            SetupInteractionReachCollider();

            if (updateSpriteFrame)
            {
                WorldSpriteAnimation worldSpriteAnimation = m_animation;
                if (worldSpriteAnimation == null)
                {
                    worldSpriteAnimation = m_ControlledMoveAnimation;
                }

                if (worldSpriteAnimation != null)
                {
                    foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
                    {
                        SetSpriteFrame(animationLayer, -1, WorldSpriteAnimationProgression.Static, false);
                    }
                }
            }

            InvokeOnTurn();
        }

        public void ChangeSkin(int skinIndex, bool updateSpriteFrame = true)
        {
            m_Skin = skinIndex;

            if (updateSpriteFrame)
            {
                WorldSpriteAnimation worldSpriteAnimation = m_animation;
                if (worldSpriteAnimation == null)
                {
                    worldSpriteAnimation = m_ControlledMoveAnimation;
                }

                if (worldSpriteAnimation != null)
                {
                    foreach (WorldSpriteAnimation animationLayer in worldSpriteAnimation.AnimationLayers)
                    {
                        SetSpriteFrame(animationLayer, -1, WorldSpriteAnimationProgression.Static, false);
                    }
                }
            }
        }

        public void ChangeSortingLayer(int sortingLayerID)
        {
            foreach (SpriteRenderer spriteRenderer in SpriteRenderers)
            {
                if (spriteRenderer != null)
                {
                    spriteRenderer.sortingLayerID = sortingLayerID;
                }
            }
            m_sortingLayerID = sortingLayerID;
        }

        private void InvokeOnStep()
        {
            OnStepped.Invoke();
            OnNextStepped.Invoke();
            OnNextStepped.RemoveAllPersistentAndNonPersistentListeners();
        }

        private void InvokeOnEmoteAnimationFinished()
        {
            OnEmoteAnimationFinished.Invoke();
            OnNextEmoteAnimationFinished.Invoke();
            OnNextEmoteAnimationFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        private void InvokeOnCollide()
        {
            OnCollided.Invoke();
            OnNextCollided.Invoke();
            OnNextCollided.RemoveAllPersistentAndNonPersistentListeners();
        }

        private void InvokeOnTrigger()
        {
            OnTriggered.Invoke();
            OnNextTriggered.Invoke();
            OnNextTriggered.RemoveAllPersistentAndNonPersistentListeners();
        }

        private void InvokeOnInteract()
        {
            OnInteracted.Invoke();
            OnNextInteracted.Invoke();
            OnNextInteracted.RemoveAllPersistentAndNonPersistentListeners();
        }
        
        private void InvokeOnTurn()
        {
            OnTurned.Invoke();
            OnNextTurned.Invoke();
            OnNextTurned.RemoveAllPersistentAndNonPersistentListeners();
        }

        private void InvokeOnBeganMove()
        {
            OnBeganMove.Invoke();
        }

        private void InvokeOnMove()
        {
            OnMove.Invoke();
        }

        private void InvokeOnEndMove()
        {
            OnEndMove.Invoke();
        }

        public void DelayedCallback(float delay, UnityAction callback)
        {
            StartCoroutine(ExecuteDelayedCallback(delay, callback));
        }
        private IEnumerator ExecuteDelayedCallback(float time, UnityAction callback)
        {
            yield return new WaitForSeconds(time);
            if (callback != null)
            {
                callback();
            }
        }

        public bool CanSee(Collider2D target)
        {
            if (DirectionToFace(target) == m_Direction)
            {
                return true;
            }
            return false;
        }

        public bool CanReach(Collider2D target)
        {
            if (Array.IndexOf(GetCollidersWithinInteractionReach(), target) >= 0)
            {
                return true;
            }
            return false;
        }

        public bool IsFacing(Collider2D target)
        {
            if (DirectionToFace(target) == m_Direction)
            {
                return true;
            }
            return false;
        }

        public bool IsOverlapping(Collider2D target)
        {
            return m_InteractionCollider.IsTouching(target);
        }

        public CardinalDirection DirectionToFace(Collider2D target)
        {
            Vector2 heading = target.bounds.center - m_CollisionCollider.bounds.center;

            return GetCardinalDirectionFromVelocity(heading);
        }

        public CardinalDirection DirectionToFace(Vector2 position)
        {
            Vector2 offset = new Vector2(0.5f, 0.5f);
            Bounds positionBounds = new Bounds(position + offset, Vector2.one);

            Vector2 heading = positionBounds.center - m_CollisionCollider.bounds.center;

            return GetCardinalDirectionFromVelocity(heading);
        }

        public bool IsSteppingOn(Collider2D target)
        {
            return m_CollisionCollider.IsTouching(target);
        }

        private bool WillCollideWith(Collider2D collider, Vector2 targetDestination)
        {
            if (collider.bounds.Contains(targetDestination))
            {
                return true;
            }
            return false;
        }

#if UNITY_EDITOR
        public void UpdateTextureImportSettings()
        {
            if (TextureImporters != null)
            {
                for (int i = 0; i < TextureImporters.Length; i++)
                {
                    KeyValuePair<Sprite, UnityEditor.TextureImporter> kvp = TextureImporters[i];
                    Sprite sprite = kvp.Key;
                    UnityEditor.TextureImporter textureImporter = kvp.Value;
                    if (textureImporter != null)
                    {
                        UnityEditor.TextureImporterSettings settings = new UnityEditor.TextureImporterSettings();
                        textureImporter.ReadTextureSettings(settings);
                        settings.textureType = UnityEditor.TextureImporterType.Sprite;
                        settings.filterMode = FilterMode.Point;
                        settings.npotScale = UnityEditor.TextureImporterNPOTScale.None;
                        settings.spritePixelsPerUnit = m_PixelsPerUnit;
                        
                        if (settings.spriteMode == (int)UnityEditor.SpriteImportMode.Single)
                        {
                            settings.spriteAlignment = (int)SpriteAlignment.BottomLeft;
                        }
                        if (settings.spriteMode == (int)UnityEditor.SpriteImportMode.Multiple)
                        {
                            if (textureImporter.spritesheet.Length <= 0)
                            {
                                UnityEditor.SpriteMetaData[] spritesheet = new UnityEditor.SpriteMetaData[1];
                                UnityEditor.SpriteMetaData metadata = new UnityEditor.SpriteMetaData();
                                metadata.name = sprite.name + "_" + 0;
                                metadata.rect = new Rect(0, 0, sprite.texture.width, sprite.texture.height);
                                metadata.alignment = Convert.ToInt32(SpriteAlignment.BottomLeft);
                                spritesheet[0] = metadata;
                                textureImporter.spritesheet = spritesheet;
                            }
                        }

                        textureImporter.SetTextureSettings(settings);

                        UnityEditor.EditorUtility.SetDirty(textureImporter);
                        textureImporter.SaveAndReimport();
                    }
                }
            }
        }

        public bool TextureImportSettingsNeedsUpdate()
        {
            if (TextureImporters != null)
            {
                for (int i = 0; i < TextureImporters.Length; i++)
                {
                    KeyValuePair<Sprite, UnityEditor.TextureImporter> kvp = TextureImporters[i];
                    Sprite sprite = kvp.Key;
                    UnityEditor.TextureImporter textureImporter = kvp.Value;
                    if (textureImporter != null)
                    {
                        UnityEditor.TextureImporterSettings settings = new UnityEditor.TextureImporterSettings();
                        textureImporter.ReadTextureSettings(settings);
                        if (settings.textureType != UnityEditor.TextureImporterType.Sprite)
                        {
                            return true;
                        }
                        if (settings.filterMode != FilterMode.Point)
                        {
                            return true;
                        }
                        if (settings.npotScale != UnityEditor.
                            TextureImporterNPOTScale.None)
                        {
                            return true;
                        }
                        if (settings.spritePixelsPerUnit != m_PixelsPerUnit)
                        {
                            return true;
                        }
                        if (settings.spriteMode == (int)UnityEditor.SpriteImportMode.Single)
                        {
                            if (settings.spriteAlignment != (int)SpriteAlignment.BottomLeft)
                            {
                                return true;
                            }
                        }
                        if (settings.spriteMode == (int)UnityEditor.SpriteImportMode.Multiple)
                        {
                            if (textureImporter.spritesheet.Length <= 0)
                            {
                                return true;
                            }
                            else
                            {
                                UnityEditor.SpriteMetaData metadata = textureImporter.spritesheet[0];
                                if (metadata.alignment != Convert.ToInt32(SpriteAlignment.BottomLeft))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        protected KeyValuePair<Sprite, UnityEditor.TextureImporter>[] GetTextureImporters()
        {
            KeyValuePair<Sprite, UnityEditor.TextureImporter>[] textureImporters = new KeyValuePair<Sprite, UnityEditor.TextureImporter>[SpriteRenderers.Length];
            for (int i = 0; i < SpriteRenderers.Length; i++)
            {
                SpriteRenderer spriteRenderer = SpriteRenderers[i];
                if (spriteRenderer != null)
                {
                    Sprite sprite = spriteRenderer.sprite;
                    if (sprite != null)
                    {
                        string path = UnityEditor.AssetDatabase.GetAssetPath(sprite);
                        textureImporters[i] = new KeyValuePair<Sprite, UnityEditor.TextureImporter>(sprite, UnityEditor.AssetImporter.GetAtPath(path) as UnityEditor.TextureImporter);
                    }
                }
            }

            return textureImporters;
        }
#endif

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            Validate();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_CanInteract))
            {
                if (!m_Controllable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_UseSubmitButtonToInteract))
            {
                if (!m_Controllable || !m_CanInteract)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_UseMouseClickToInteract))
            {
                if (!m_Controllable || !m_CanInteract)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_InteractionReachDistance))
            {
                if (!m_Controllable || !m_CanInteract)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_InteractionReachAngle))
            {
                if (!m_Controllable || !m_CanInteract)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_InteractionReachArcSmoothness))
            {
                if (!m_Controllable || !m_CanInteract)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_CanMove))
            {
                if (!m_Controllable)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ControlledMoveAnimation))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_UseAxisInputToMove))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_UseMouseClickToMove))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveSpeed))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AllowDiagonalMovement))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_LockMovementToGrid))
            {
                if (!m_Controllable || !m_CanMove)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyIndentLevelOffset(string propertyPath)
        {
            if (propertyPath == nameof(m_CanInteract))
            {
                return 1;
            }
            if (propertyPath == nameof(m_UseSubmitButtonToInteract))
            {
                return 2;
            }
            if (propertyPath == nameof(m_UseMouseClickToInteract))
            {
                return 2;
            }
            if (propertyPath == nameof(m_InteractionReachDistance))
            {
                return 2;
            }
            if (propertyPath == nameof(m_InteractionReachAngle))
            {
                return 2;
            }
            if (propertyPath == nameof(m_InteractionReachArcSmoothness))
            {
                return 2;
            }
            if (propertyPath == nameof(m_CanMove))
            {
                return 1;
            }
            if (propertyPath == nameof(m_ControlledMoveAnimation))
            {
                return 2;
            }
            if (propertyPath == nameof(m_UseAxisInputToMove))
            {
                return 2;
            }
            if (propertyPath == nameof(m_UseMouseClickToMove))
            {
                return 2;
            }
            if (propertyPath == nameof(m_MoveSpeed))
            {
                return 2;
            }
            if (propertyPath == nameof(m_AllowDiagonalMovement))
            {
                return 2;
            }
            if (propertyPath == nameof(m_LockMovementToGrid))
            {
                return 2;
            }
            return base.GetPropertyIndentLevelOffset(propertyPath);
        }
    }
}
