using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.AudioUtilities;
using ImprobableStudios.BaseUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.WorldSystem2D
{

    [Serializable]
    public class Skin
    {
        [SerializeField]
        [FormerlySerializedAs("spriteSheet")]
        public Texture2D m_SpriteSheet;

        [SerializeField]
        [FormerlySerializedAs("frames")]
        public Sprite[] m_Frames;

        public bool IsValid()
        {
            if (m_Frames == null || m_Frames.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < m_Frames.Length; i++)
            {
                if (m_Frames[i] == null)
                {
                    return false;
                }
            }

            return true;
        }
    }
    
    public enum WorldSpriteAnimationProgression
    {
        Progressive,
        Loop,
        Static,
    }
    
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer))]
    [AddComponentMenu("2D World System/World Sprite Animation")]
    public class WorldSpriteAnimation : PersistentBehaviour
    {
        public const int DEFAULT_PIXELS_PER_UNIT = 32;

        public const int DEFAULT_NUMBER_OF_COLUMNS = 3;

        public const int DEFAULT_NUMBER_OF_ROWS = 4;

        public const float DEFAULT_PROGRESSIVE_FPS = 10f;

        public const float DEFAULT_LOOP_FPS = 6.5f;

        [Tooltip("The number of pixels per unit")]
        [Min(1)]
        [Delayed]
        [SerializeField]
        protected int m_PixelsPerUnit = DEFAULT_PIXELS_PER_UNIT;

        [Tooltip("The different skins that this world sprite animation supports and their corresponding spritesheets")]
        [SerializeField]
        [FormerlySerializedAs("m_Skins")]
        [FormerlySerializedAs("m_SpriteSheets")]
        protected Skin[] m_Skins = new Skin[1];
        
        [Tooltip("The number of columns in each spritesheet for this world sprite animation")]
        [Min(1)]
        [SerializeField]
        protected int m_NumberOfColumns = DEFAULT_NUMBER_OF_COLUMNS;

        [Tooltip("The number of rows in each spritesheet for this world sprite animation")]
        [Min(1)]
        [SerializeField]
        protected int m_NumberOfRows = DEFAULT_NUMBER_OF_ROWS;
        
        [Tooltip("The directions that correspond to each row in the spritesheets (e.g. South = Row 1, West = Row 2, East = Row 3, North = Row 4)")]
        [SerializeField]
        protected CardinalDirection[] m_Directions = new CardinalDirection[] { CardinalDirection.South, CardinalDirection.West, CardinalDirection.East, CardinalDirection.North };
        
        [Tooltip("If true, this animation contains a section that can be played forward and/or backward progressively")]
        [SerializeField]
        protected bool m_ProgressiveAnimationExists;

        [Tooltip("The frames that will be played when the animation is started or stopped (The frames will play forwards when starting and backwards when stopping)")]
        [SerializeField]
        [FormerlySerializedAs("m_ProgressiveFrames")]
        protected int[] m_ProgressiveFrameIndices = new int[0];

        [Tooltip("The default speed of the progressive animation (in frames/second)")]
        [Min(0)]
        [SerializeField]
        protected float m_ProgressiveFramesPerSecond = DEFAULT_PROGRESSIVE_FPS;

        [Tooltip("The audio that will play when this animation is played forwards")]
        [FormerlySerializedAs("m_StartAudio")]
        [SerializeField]
        protected AudioClip m_ForwardAudioClip;

        [Tooltip("The audio that will play when this animation is played backwards")]
        [FormerlySerializedAs("m_StopAudio")]
        [SerializeField]
        protected AudioClip m_BackwardAudioClip;

        [Tooltip("If true, this animation contains a section that loops")]
        [SerializeField]
        [FormerlySerializedAs("m_LoopAnimationExists")]
        [FormerlySerializedAs("m_Loop")]
        protected bool m_LoopAnimationExists = true;

        [Tooltip("The frames that will be played when the animation is looped")]
        [SerializeField]
        [FormerlySerializedAs("m_LoopFrames")]
        protected int[] m_LoopFrameIndices = new int[0];

        [Tooltip("The default speed of the loop animation (in frames/second)")]
        [Min(0)]
        [SerializeField]
        protected float m_LoopFramesPerSecond = DEFAULT_LOOP_FPS;

        [Tooltip("The loop type.")]
        [SerializeField]
        protected LoopType m_LoopType = LoopType.Yoyo;

        [Tooltip("Start the animation from the middle frame. This is useful to match the common left-side/right-side format of YoYo looping animation sprite sheets")]
        [FormerlySerializedAs("m_IdleFrame")]
        [FormerlySerializedAs("m_StartFrame")]
        [SerializeField]
        protected bool m_StartAndEndLoopAtMiddle = true;

        [Tooltip("The audio that will loop while this animation is playing")]
        [SerializeField]
        protected AudioClip m_LoopAudioClip;

        protected int m_currentSkin;

        protected CardinalDirection m_currentDirection;

        protected int m_currentFrameColumn;
        
        protected WorldSpriteAnimationProgression m_currentAnimationType;
        
        protected float m_currentAnimationSpeed;
        
        protected bool m_isLoopingBackwards;
        
        protected int m_currentNumberOfLoopFramesLeft;
        
        protected int m_staticFrame;
        
        protected float m_staticDuration;
        
        protected UnityAction m_playAction;
        
        protected UnityAction m_completeAction;
        
        protected int m_currentAudioTimeSamples;
        
        protected UnityEvent m_onAnimationFinished = new UnityEvent();
        
        protected UnityEvent m_onNextAnimationFinished = new UnityEvent();

        private Coroutine m_animationCoroutine;

        private int[] m_allFrameIndices;

        private AudioSource m_animationAudioSource;

        private WorldSpriteAnimation[] m_animationLayers;

        private SpriteRenderer m_spriteRenderer;

        private WorldSprite m_worldSprite;

        public int PixelsPerUnit
        {
            get
            {
                return m_PixelsPerUnit;
            }
            set
            {
                m_PixelsPerUnit = value;
            }
        }

        public Skin[] Skins
        {
            get
            {
                return m_Skins;
            }
            set
            {
                m_Skins = value;
            }
        }

        public int NumberOfColumns
        {
            get
            {
                return m_NumberOfColumns;
            }
            set
            {
                m_NumberOfColumns = value;
                m_allFrameIndices = GetAllFrameIndices();
            }
        }

        public int NumberOfRows
        {
            get
            {
                return m_NumberOfRows;
            }
            set
            {
                m_NumberOfRows = value;
                m_allFrameIndices = GetAllFrameIndices();
            }
        }

        public CardinalDirection[] Directions
        {
            get
            {
                return m_Directions;
            }
            set
            {
                m_Directions = value;
            }
        }
    
        public bool ProgressiveAnimationExists
        {
            get
            {
                return m_ProgressiveAnimationExists;
            }
            set
            {
                m_ProgressiveAnimationExists = value;
            }
        }

        public int[] ProgressiveFrameIndices
        {
            get
            {
                if (m_ProgressiveAnimationExists && m_LoopAnimationExists)
                {
                    return m_ProgressiveFrameIndices;
                }
                return AllFrameIndices;
            }
            set
            {
                m_ProgressiveFrameIndices = value;
            }
        }

        public float ProgressiveFramesPerSecond
        {
            get
            {
                if (m_ProgressiveAnimationExists)
                {
                    return m_ProgressiveFramesPerSecond;
                }
                if (m_LoopAnimationExists)
                {
                    return m_LoopFramesPerSecond;
                }
                return DEFAULT_PROGRESSIVE_FPS;
            }
            set
            {
                m_ProgressiveFramesPerSecond = value;
            }
        }

        public AudioClip ForwardAudioClip
        {
            get
            {
                return m_ForwardAudioClip;
            }
            set
            {
                m_ForwardAudioClip = value;
            }
        }

        public AudioClip BackwardAudioClip
        {
            get
            {
                return m_BackwardAudioClip;
            }
            set
            {
                m_BackwardAudioClip = value;
            }
        }
        public bool LoopAnimationExists
        {
            get
            {
                return m_LoopAnimationExists;
            }
            set
            {
                m_LoopAnimationExists = value;
            }
        }

        public int[] LoopFrameIndices
        {
            get
            {
                if (m_LoopAnimationExists && m_ProgressiveAnimationExists)
                {
                    return m_LoopFrameIndices;
                }
                return AllFrameIndices;
            }
            set
            {
                m_LoopFrameIndices = value;
            }
        }

        public float LoopFramesPerSecond
        {
            get
            {
                if (LoopAnimationExists)
                {
                    return m_LoopFramesPerSecond;
                }
                if (m_ProgressiveAnimationExists)
                {
                    return m_ProgressiveFramesPerSecond;
                }
                return DEFAULT_LOOP_FPS;
            }
            set
            {
                m_LoopFramesPerSecond = value;
            }
        }

        public LoopType LoopType
        {
            get
            {
                if (m_LoopAnimationExists)
                {
                    return m_LoopType;
                }
                return LoopType.Yoyo;
            }
            set
            {
                m_LoopType = value;
            }
        }

        public bool StartAndEndLoopAtMiddle
        {
            get
            {
                return m_LoopAnimationExists && m_LoopType == LoopType.Yoyo && m_StartAndEndLoopAtMiddle;
            }
            set
            {
                m_StartAndEndLoopAtMiddle = value;
            }
        }

        public AudioClip LoopAudioClip
        {
            get
            {
                return m_LoopAudioClip;
            }
            set
            {
                m_LoopAudioClip = value;
            }
        }

        public int[] AllFrameIndices
        {
            get
            {
                if (m_allFrameIndices == null || m_allFrameIndices.Length == 0)
                {
                    m_allFrameIndices = GetAllFrameIndices();
                }
                
                return m_allFrameIndices;
            }
        }

        public int CurrentFrameColumn
        {
            get
            {
                return m_currentFrameColumn;
            }
        }
        
        public AudioSource AnimationAudioSource
        {
            get
            {
                if (m_animationAudioSource == null)
                {
                    m_animationAudioSource = this.GetRequiredComponent<AudioSource>();
                }
                return m_animationAudioSource;
            }
        }

        public WorldSpriteAnimation[] AnimationLayers
        {
            get
            {
                if (m_animationLayers == null)
                {
                    m_animationLayers = gameObject.GetComponentsInChildren<WorldSpriteAnimation>(true);
                }
                return m_animationLayers;
            }
        }

        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (m_spriteRenderer == null)
                {
                    m_spriteRenderer = gameObject.GetRequiredComponent<SpriteRenderer>();
                }
                return m_spriteRenderer;
            }
        }

        public WorldSprite WorldSprite
        {
            get
            {
                if (m_worldSprite == null)
                {
                    m_worldSprite = gameObject.GetComponentInParent<WorldSprite>(true);
                }
                return m_worldSprite;
            }
        }

        public UnityEvent OnAnimationFinished
        {
            get
            {
                return m_onAnimationFinished;
            }
        }

        public UnityEvent OnNextAnimationFinished
        {
            get
            {
                return m_onNextAnimationFinished;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (WorldSprite != null)
            {
                WorldSprite.UpdateCachedAnimations();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (WorldSprite != null)
            {
                WorldSprite.UpdateCachedAnimations();
            }
        }

        public virtual void Validate()
        {
            if (WorldSprite != null)
            {
                if (m_Skins.Length != WorldSprite.SkinNames.Length || m_Skins.Contains(null))
                {
                    Skin[] newSkins = new Skin[WorldSprite.SkinNames.Length];
                    for (int i = 0; i < newSkins.Length; i++)
                    {
                        if (i >= 0 && i < m_Skins.Length)
                        {
                            if (m_Skins[i] != null)
                            {
                                newSkins[i] = m_Skins[i];
                            }
                            else
                            {
                                newSkins[i] = new Skin();
                            }
                        }
                        else
                        {
                            newSkins[i] = new Skin();
                        }
                    }
                    m_Skins = newSkins;
                }
            }

            if (m_Directions.Length != m_NumberOfRows)
            {
                CardinalDirection[] newDirections = new CardinalDirection[m_NumberOfRows];
                for (int i = 0; i < newDirections.Length; i++)
                {
                    if (i >= 0 && i < m_Directions.Length)
                    {
                        newDirections[i] = m_Directions[i];
                    }
                }
                m_Directions = newDirections;
            }

#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                for (int i = 0; i < m_Skins.Length; i++)
                {
                    Skin skin = m_Skins[i];
                    if (skin == null)
                    {
                        skin = new Skin();
                        m_Skins[i] = skin;
                    }
                    if (skin.m_SpriteSheet != null)
                    {
                        string spriteSheetPath = AssetDatabase.GetAssetPath(skin.m_SpriteSheet);
                        TextureImporter textureImporter = AssetImporter.GetAtPath(spriteSheetPath) as TextureImporter;
                        TextureImporterSettings settings = new TextureImporterSettings();

                        if (skin.m_Frames.Length > 0 && skin.m_Frames != null && skin.m_Frames[0] != null && skin.m_SpriteSheet != skin.m_Frames[0].texture || skin.m_Frames.Length != (m_NumberOfColumns * m_NumberOfRows))
                        {
                            skin.m_Frames = AssetDatabase.LoadAllAssetsAtPath(spriteSheetPath).OfType<Sprite>().OrderBy(x => x.name).ToArray();
                        }

                        if (textureImporter != null)
                        {
                            textureImporter.ReadTextureSettings(settings);

                            if (skin.m_Frames.Length != (m_NumberOfColumns * m_NumberOfRows) ||
                                settings.textureType != TextureImporterType.Sprite ||
                                settings.spriteMode != 2 ||
                                settings.filterMode != FilterMode.Point ||
                                settings.npotScale != TextureImporterNPOTScale.None ||
                                settings.spritePixelsPerUnit != m_PixelsPerUnit ||
                                textureImporter.spritesheet.Length != (m_NumberOfColumns * m_NumberOfRows))
                            {
                                SliceSpritesheet(skin);
                            }
                        }
                    }
                }
            }
#endif

            if (m_Skins != null && m_Skins.Length > 0 && m_Skins[0].m_Frames != null && m_Skins[0].m_Frames.Length > 0)
            {
                int column = 0;
                if (StartAndEndLoopAtMiddle)
                {
                    column = GetRowLength(LoopFrameIndices.Length) / 2;
                }
                SetSpriteFrame(GetFirstValidSkinIndex(), m_Directions[0], column, WorldSpriteAnimationProgression.Loop);
            }
        }

        public int[] GetAllFrameIndices()
        {
            List<int> newLoopFrames = new List<int>();
            for (int r = 0; r < NumberOfRows; r++)
            {
                for (int c = 0; c < NumberOfColumns; c++)
                {
                    int i = r * NumberOfColumns + c;
                    if (!newLoopFrames.Contains(i))
                    {
                        newLoopFrames.Add(i);
                    }
                }
            }
            return newLoopFrames.ToArray();
        }

        public int ConvertDirectionToRow(CardinalDirection direction)
        {
            for (int i = 0; i < m_Directions.Length; i++)
            {
                if (m_Directions[i] == direction)
                {
                    return i;
                }
            }
            return -1;
        }

        public CardinalDirection ConvertRowToDirection(int row)
        {
            if (row < 0 || row > m_Directions.Length - 1)
            {
                return default(CardinalDirection);
            }
            return m_Directions[row];
        }

        public int GetRowLength(int frameCount)
        {
            return frameCount / m_NumberOfRows;
        }

        public int ConvertColumnToFrameIndex(CardinalDirection direction, int column, int rowLength)
        {
            return column + ConvertDirectionToRow(direction) * rowLength;
        }

        public int ConvertColumnToFrame(int row, int column, int rowLength)
        {
            return column + row * rowLength;
        }

        public bool CanFace(CardinalDirection direction)
        {
            return ConvertDirectionToRow(direction) >= 0;
        }

        public Skin GetFirstValidSkin()
        {
            int validSkinIndex = GetFirstValidSkinIndex();
            if (validSkinIndex >= 0)
            {
                return m_Skins[validSkinIndex];
            }
            return null;
        }

        public int GetFirstValidSkinIndex()
        {
            if (m_Skins.Length > 0)
            {
                for (int i = 0; i < m_Skins.Length; i++)
                {
                    Skin skin = m_Skins[i];
                    if (skin != null && skin.IsValid())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public bool IsSkinIndexValid(int skinIndex)
        {
            if (skinIndex >= 0 && skinIndex < m_Skins.Length)
            {
                Skin skin = m_Skins[skinIndex];
                if (skin.IsValid())
                {
                    return true;
                }
            }
            return false;
        }

        public void SetSpriteFrame(int skin, CardinalDirection direction)
        {
            SetSpriteFrame(skin, direction, m_currentFrameColumn, m_currentAnimationType);
        }

        public void SetSpriteFrame(int skin, CardinalDirection direction, int column, WorldSpriteAnimationProgression animationType)
        {
            if (!IsSkinIndexValid(skin))
            {
                skin = GetFirstValidSkinIndex();
                Debug.LogWarning("Pixel animation does not support this skin, using first valid skin instead");
            }

            if (!CanFace(direction))
            {
                direction = Directions[0];
                Debug.LogWarning("Pixel animation does not support this direction, using first valid direction instead");
            }

            int rowLength = NumberOfColumns;
            
            if (animationType == WorldSpriteAnimationProgression.Progressive)
            {
                int frameCount = ProgressiveFrameIndices.Length;
                rowLength = GetRowLength(frameCount);
            }
            else if (animationType == WorldSpriteAnimationProgression.Loop)
            {
                int frameCount = LoopFrameIndices.Length;
                rowLength = GetRowLength(frameCount);
            }

            int frameIndex = ConvertColumnToFrameIndex(direction, column, rowLength);
            if (animationType == WorldSpriteAnimationProgression.Progressive)
            {
                frameIndex = ProgressiveFrameIndices[frameIndex];
            }
            else if (animationType == WorldSpriteAnimationProgression.Loop)
            {
                frameIndex = LoopFrameIndices[frameIndex];
            }

            if ((skin >= 0 && skin < Skins.Length) &&
                (frameIndex >= 0 && frameIndex < Skins[skin].m_Frames.Length))
            {
                m_currentSkin = skin;
                m_currentDirection = direction;
                m_currentFrameColumn = column;
                m_currentAnimationType = animationType;
                SpriteRenderer.sprite = Skins[skin].m_Frames[frameIndex];
            }
        }

        private void InvokeOnAnimationFinished()
        {
            OnAnimationFinished.Invoke();
            OnNextAnimationFinished.Invoke();
            OnNextAnimationFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        public void Interrupt()
        {
            if (m_animationCoroutine != null)
            {
                StopCoroutine(m_animationCoroutine);
                m_animationCoroutine = null;
                InvokeOnAnimationFinished();
            }
        }

        public virtual void Goto(int frame, int loopCount)
        {
            //m_CurrentFrame = frame;
            //m_CurrentLoopCount = loopCount;

            if (m_playAction != null)
            {
                m_playAction();
            }
        }

        public virtual void Pause()
        {
            if (m_animationCoroutine != null)
            {
                StopCoroutine(m_animationCoroutine);
                m_animationCoroutine = null;
            }
        }

        public virtual void Resume()
        {
            if (m_playAction != null)
            {
                m_playAction();
            }
        }

        public virtual void Stop()
        {
            if (m_animationCoroutine != null)
            {
                StopCoroutine(m_animationCoroutine);
                m_animationCoroutine = null;
            }

            m_playAction = null;
            m_completeAction = null;
        }

        public virtual void Complete()
        {
            if (m_animationCoroutine != null)
            {
                StopCoroutine(m_animationCoroutine);
                m_animationCoroutine = null;
            }

            m_playAction = null;
            if (m_completeAction != null)
            {
                m_completeAction();
                m_completeAction = null;

                InvokeOnAnimationFinished();
            }
        }

        public void ShowStaticFrame(int skin, CardinalDirection direction, int frameIndex, float duration, UnityAction onComplete = null)
        {
            m_currentSkin = skin;
            m_currentDirection = direction;
            m_staticFrame = frameIndex;
            m_staticDuration = duration;

            m_currentAudioTimeSamples = 0;

            m_playAction = PlayStaticFrameAnimation;
            m_completeAction = CompleteStaticFrameAnimation;

            Interrupt();

            if (onComplete != null)
            {
                OnNextAnimationFinished.AddPersistentListener(onComplete);
            }

            PlayStaticFrameAnimation();
        }

        private void PlayStaticFrameAnimation()
        {
            gameObject.SetActive(true);
            
            m_animationCoroutine = StartCoroutine(RunStaticFrameAnimation(Complete));
        }

        private IEnumerator RunStaticFrameAnimation(UnityAction onComplete = null)
        {
            SetSpriteFrame(m_currentSkin, m_currentDirection, m_staticFrame, WorldSpriteAnimationProgression.Static);
            
            while (m_staticDuration > 0)
            {
                yield return new WaitForSeconds(1);
                m_staticDuration--;
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public void CompleteStaticFrameAnimation()
        {
            SetSpriteFrame(m_currentSkin, m_currentDirection, m_staticFrame, WorldSpriteAnimationProgression.Static);
        }

        public void PlayAnimationForward(int skin, CardinalDirection direction, float animationSpeed, UnityAction onComplete = null)
        {
            m_currentSkin = skin;
            m_currentDirection = direction;
            m_currentAnimationSpeed = animationSpeed;

            m_currentFrameColumn = 0;
            m_currentAudioTimeSamples = 0;

            m_playAction = PlayAnimationForward;
            m_completeAction = CompleteForwardAnimation;

            Interrupt();

            if (onComplete != null)
            {
                OnNextAnimationFinished.AddPersistentListener(onComplete);
            }

            PlayAnimationForward();
        }

        private void PlayAnimationForward()
        {
            gameObject.SetActive(true);

            m_animationCoroutine = StartCoroutine(RunAnimationForward(Complete));
        }

        protected virtual IEnumerator RunAnimationForward(UnityAction onComplete = null)
        {
            int frameCount = ProgressiveFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);

            if (LoopAudioClip != null)
            {
                AnimationAudioSource.clip = LoopAudioClip;
                if (m_currentAudioTimeSamples < AnimationAudioSource.timeSamples)
                {
                    AnimationAudioSource.timeSamples = m_currentAudioTimeSamples;
                    AudioManager.Instance.Play(AnimationAudioSource, true);
                }
            }
            else if (ForwardAudioClip != null)
            {
                AnimationAudioSource.clip = ForwardAudioClip;
                if (m_currentAudioTimeSamples < AnimationAudioSource.timeSamples)
                {
                    AnimationAudioSource.timeSamples = m_currentAudioTimeSamples;
                    AudioManager.Instance.Play(AnimationAudioSource, false);
                }
            }

            while (m_currentFrameColumn < rowLength)
            {
                SetSpriteFrame(m_currentSkin, m_currentDirection, m_currentFrameColumn, WorldSpriteAnimationProgression.Progressive);
                m_currentAudioTimeSamples = AnimationAudioSource.timeSamples;
                yield return new WaitForSeconds(1 / (ProgressiveFramesPerSecond * m_currentAnimationSpeed));
                m_currentFrameColumn++;
            }

            if (LoopAudioClip != null)
            {
                AudioManager.Instance.Stop(AnimationAudioSource);
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public void CompleteForwardAnimation()
        {
            int frameCount = ProgressiveFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);
            int col = rowLength - 1;
            SetSpriteFrame(m_currentSkin, m_currentDirection, col, WorldSpriteAnimationProgression.Progressive);
        }

        public void PlayAnimationBackwards(int skin, CardinalDirection direction, float animationSpeed, UnityAction onComplete = null)
        {
            m_currentSkin = skin;
            m_currentDirection = direction;
            m_currentAnimationSpeed = animationSpeed;

            int frameCount = ProgressiveFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);
            m_currentFrameColumn = rowLength - 1;
            m_currentAudioTimeSamples = 0;

            m_playAction = PlayAnimationBackwards;
            m_completeAction = CompleteBackwardsAnimation;

            Interrupt();

            if (onComplete != null)
            {
                OnNextAnimationFinished.AddPersistentListener(onComplete);
            }

            PlayAnimationBackwards();
        }

        private void PlayAnimationBackwards()
        {
            gameObject.SetActive(true);

            m_animationCoroutine = StartCoroutine(RunAnimationBackwards(Complete));
        }

        private IEnumerator RunAnimationBackwards(UnityAction onComplete = null)
        {
            if (LoopAudioClip != null)
            {
                AnimationAudioSource.clip = LoopAudioClip;
                if (m_currentAudioTimeSamples < AnimationAudioSource.timeSamples)
                {
                    AnimationAudioSource.timeSamples = m_currentAudioTimeSamples;
                    AudioManager.Instance.Play(AnimationAudioSource, true);
                }
            }
            else if (BackwardAudioClip != null)
            {
                AnimationAudioSource.clip = BackwardAudioClip;
                if (m_currentAudioTimeSamples < AnimationAudioSource.timeSamples)
                {
                    AnimationAudioSource.timeSamples = m_currentAudioTimeSamples;
                    AudioManager.Instance.Play(AnimationAudioSource, false);
                }
            }

            while (m_currentFrameColumn >= 0)
            {
                SetSpriteFrame(m_currentSkin, m_currentDirection, m_currentFrameColumn, WorldSpriteAnimationProgression.Progressive);
                m_currentAudioTimeSamples = AnimationAudioSource.timeSamples;
                yield return new WaitForSeconds(1 / (ProgressiveFramesPerSecond * m_currentAnimationSpeed));
                m_currentFrameColumn--;
            }

            if (LoopAudioClip != null)
            {
                AudioManager.Instance.Stop(AnimationAudioSource);
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public void CompleteBackwardsAnimation()
        {
            int col = 0;
            SetSpriteFrame(m_currentSkin, m_currentDirection, col, WorldSpriteAnimationProgression.Progressive);
        }
        
        public void LoopAnimation(int skin, CardinalDirection direction, float animationSpeed, int loopCount, bool backwards, UnityAction onComplete = null)
        {
            m_currentSkin = skin;
            m_currentDirection = direction;
            m_currentAnimationSpeed = animationSpeed;
            m_isLoopingBackwards = backwards;

            int frameCount = LoopFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);

            m_currentFrameColumn = 0;
            if (StartAndEndLoopAtMiddle)
            {
                m_currentFrameColumn = rowLength / 2;
            }
            
            m_currentNumberOfLoopFramesLeft = -1;
            if (loopCount >= 0)
            {
                m_currentNumberOfLoopFramesLeft = rowLength;
                m_currentNumberOfLoopFramesLeft = (int)(loopCount * rowLength);
            }

            m_currentAudioTimeSamples = 0;

            m_playAction = PlayLoopAnimation;
            m_completeAction = CompleteLoopAnimation;

            Interrupt();

            if (onComplete != null)
            {
                OnNextAnimationFinished.AddPersistentListener(onComplete);
            }

            PlayLoopAnimation();
        }

        private void PlayLoopAnimation()
        {
            gameObject.SetActive(true);
            m_animationCoroutine = StartCoroutine(RunLoopAnimation(Complete));
        }

        private IEnumerator RunLoopAnimation(UnityAction onComplete = null)
        {
            int frameCount = LoopFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);

            if (LoopAudioClip != null)
            {
                AnimationAudioSource.clip = LoopAudioClip;
                if (m_currentAudioTimeSamples < AnimationAudioSource.timeSamples)
                {
                    AnimationAudioSource.timeSamples = m_currentAudioTimeSamples;
                    AudioManager.Instance.Play(AnimationAudioSource, true);
                }
            }

            m_currentFrameColumn++;
            
            while (m_currentNumberOfLoopFramesLeft > 0 || m_currentNumberOfLoopFramesLeft < 0)
            {
                SetSpriteFrame(m_currentSkin, m_currentDirection, m_currentFrameColumn, WorldSpriteAnimationProgression.Loop);
                m_currentAudioTimeSamples = AnimationAudioSource.timeSamples;
                yield return new WaitForSeconds(1 / (LoopFramesPerSecond * m_currentAnimationSpeed));

                if (!m_isLoopingBackwards && m_currentFrameColumn >= rowLength - 1)
                {
                    if (LoopType == LoopType.Yoyo)
                    {
                        m_isLoopingBackwards = true;
                    }
                    else
                    {
                        m_currentFrameColumn = -1;
                    }
                }
                else if (m_isLoopingBackwards && m_currentFrameColumn <= 0)
                {
                    if (LoopType == LoopType.Yoyo)
                    {
                        m_isLoopingBackwards = false;
                    }
                    else
                    {
                        m_currentFrameColumn = rowLength;
                    }
                }

                if (!m_isLoopingBackwards)
                {
                    m_currentFrameColumn++;
                }
                else
                {
                    m_currentFrameColumn--;
                }

                m_currentNumberOfLoopFramesLeft--;
            }

            if (LoopAudioClip != null)
            {
                AudioManager.Instance.Stop(AnimationAudioSource);
            }

            if (onComplete != null)
            {
                onComplete();
            }
        }

        public void CompleteLoopAnimation()
        {
            int frameCount = LoopFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);
            int col = 0;
            if (StartAndEndLoopAtMiddle)
            {
                col = rowLength / 2;
            }

            SetSpriteFrame(m_currentSkin, m_currentDirection, col, WorldSpriteAnimationProgression.Loop);
        }

        public void EndLoopAnimation()
        {
            int frameCount = LoopFrameIndices.Length;
            int rowLength = GetRowLength(frameCount);

            if (m_currentNumberOfLoopFramesLeft >= 0)
            {
                m_currentNumberOfLoopFramesLeft = m_currentNumberOfLoopFramesLeft % rowLength;
            }
            else
            {
                m_currentNumberOfLoopFramesLeft = 1;
            }
        }
        
#if UNITY_EDITOR
        public void SliceAllSpritesheets()
        {
            foreach (Skin skin in m_Skins)
            {
                if (skin.m_SpriteSheet != null)
                {
                    SliceSpritesheet(skin);
                }
            }
        }

        public void SliceSpritesheet(Skin skin)
        {
            if (skin.m_SpriteSheet.width % m_NumberOfColumns != 0)
            {
                //Debug.LogError(skin.m_SpriteSheet + " width (" + skin.m_SpriteSheet.width + ") is not a multiple of number of columns (" + m_NumberOfColumns + ")");
                return;
            }
            if (skin.m_SpriteSheet.height % m_NumberOfRows != 0)
            {
                //Debug.LogError(skin.m_SpriteSheet + " height (" + skin.m_SpriteSheet.height + ") is not a multiple of number of rows (" + m_NumberOfRows + ")");
                return;
            }
            string path = AssetDatabase.GetAssetPath(skin.m_SpriteSheet);
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

            TextureImporterSettings settings = new TextureImporterSettings();
            textureImporter.ReadTextureSettings(settings);
            settings.textureType = TextureImporterType.Sprite;
            settings.spriteMode = 2;
            settings.filterMode = FilterMode.Point;
            settings.npotScale = TextureImporterNPOTScale.None;
            settings.spritePixelsPerUnit = m_PixelsPerUnit;

            int spriteIndex = 0;
            int paddingAmount = ((m_NumberOfColumns * m_NumberOfRows) - 1).ToString().Length;
            List<SpriteMetaData> spriteList = new List<SpriteMetaData>();
            for (int y = skin.m_SpriteSheet.height - 1; y >= 0; y--)
            {
                for (int x = 0; x < skin.m_SpriteSheet.width; x++)
                {
                    float frameWidth = skin.m_SpriteSheet.width / m_NumberOfColumns;
                    float frameHeight = skin.m_SpriteSheet.height / m_NumberOfRows;
                    if (x % frameWidth == 0 && y % frameHeight == 0)
                    {
                        SpriteMetaData metadata = new SpriteMetaData();
                        metadata.name = skin.m_SpriteSheet.name + "_" + spriteIndex.ToString("D" + paddingAmount);
                        spriteIndex++;
                        metadata.rect = new Rect(x, y, frameWidth, frameHeight);
                        metadata.alignment = Convert.ToInt32(SpriteAlignment.BottomLeft);
                        spriteList.Add(metadata);
                    }
                }
            }
            
            textureImporter.spritesheet = spriteList.ToArray();
            textureImporter.SetTextureSettings(settings);

            EditorUtility.SetDirty(textureImporter);
            textureImporter.SaveAndReimport();

            string spriteSheetPath = AssetDatabase.GetAssetPath(skin.m_SpriteSheet);
            skin.m_Frames = AssetDatabase.LoadAllAssetsAtPath(spriteSheetPath).OfType<Sprite>().OrderBy(i => i.name).ToArray();
        }
        
        public static WorldSpriteAnimation CreateNewWorldSpriteAnimation(string animationName, GameObject parent)
        {
            GameObject worldSpriteAnimationObject = new GameObject(animationName);
            worldSpriteAnimationObject.AddComponent<WorldSpriteAnimation>();
            worldSpriteAnimationObject.transform.SetParent(parent.transform);
            worldSpriteAnimationObject.transform.Translate(parent.transform.position);
            worldSpriteAnimationObject.transform.localScale = new Vector3(1f, 1f, 1f);
            Undo.RegisterCreatedObjectUndo(parent, "Create World Sprite Animation");
            return worldSpriteAnimationObject.GetComponent<WorldSpriteAnimation>();
        }
#endif

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

            Validate();
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ForwardAudioClip))
            {
                if (m_LoopAudioClip != null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_ForwardAudioClip))
            {
                if (m_LoopAudioClip != null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_BackwardAudioClip))
            {
                if (m_LoopAudioClip != null)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}