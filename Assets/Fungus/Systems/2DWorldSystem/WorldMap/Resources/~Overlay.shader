﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/OverlayBlend"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Main Color", Color) = (1,1,1,1)
//        _OverlayTex ("Overlay", 2D) = "white" {}
//        _BackgroundTex ("Background", 2D) = "white" {}
    }
   
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector"="True" 
            "RenderType" = "Transparent"
            "PreviewType"="Plane"
        }

        Cull Off
        Lighting Off
          ZWrite Off
        ZTest Always
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha
        
        GrabPass { }
       
        Pass
        {
            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members grabUV)
//#pragma exclude_renderers d3d11 xbox360
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            sampler2D _GrabTexture;
            fixed4 _GrabTexture_ST;
            fixed4 _Color;

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex        : POSITION;
                float2 texcoord      : TEXCOORD0;
                float4 grabUV         : TEXCOORD1;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
                OUT.grabUV = ComputeGrabScreenPos(OUT.vertex);

//                // Snapping params
//                float hpcX = _ScreenParams.x * 0.5;
//                float hpcY = _ScreenParams.y * 0.5;
//            #ifdef UNITY_HALF_TEXEL_OFFSET
//                float hpcOX = -0.5;
//                float hpcOY = 0.5;
//            #else
//                float hpcOX = 0;
//                float hpcOY = 0;
//            #endif  
//                // Snap
//                float pos = floor((OUT.vertex.x / OUT.vertex.w) * hpcX + 0.5f) + hpcOX;
//                OUT.vertex.x = pos / hpcX * OUT.vertex.w;
//
//                pos = floor((OUT.vertex.y / OUT.vertex.w) * hpcY + 0.5f) + hpcOY;
//                OUT.vertex.y = pos / hpcY * OUT.vertex.w;

                return OUT;
            }
           
            fixed4 frag (v2f i) : COLOR
            {
                fixed4 diffuse = tex2D(_MainTex, i.texcoord);
                fixed4 text = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(i.grabUV));
                fixed luminance =  dot(diffuse, fixed4(0.2126, 0.7152, 0.0722, 0));
                fixed oldAlpha = diffuse.a;

//                if (luminance < 0.5)
//                    diffuse *= 2 * text;
//                else
//                    diffuse = 1-2*(1-diffuse)*(1-text);
                fixed4 darken = step(0.5, 1-luminance) * (diffuse * 2 * text);
                fixed4 brighten = step(0.5, luminance) * (1 - 2*(1-diffuse)*(1-text));
                diffuse = darken + brighten;
                diffuse.a  = oldAlpha * text.a;

                return diffuse * _Color;
            }
            ENDCG
        }
    }
    Fallback off
}