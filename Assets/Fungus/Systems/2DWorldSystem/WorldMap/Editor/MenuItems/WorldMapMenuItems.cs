#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.WorldSystem2D
{
    
    public class WorldMapMenuItems 
    {
        [MenuItem("Tools/2D World System/World Map", false, 10)]
        [MenuItem("GameObject/2D World System/World Map", false, 10)]
        static void CreateWorldMap()
        {
            PrefabSpawner.SpawnPrefab<WorldMap>("~WorldMap", true);
        }
    }

}
#endif