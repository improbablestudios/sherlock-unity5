using UnityEngine;
using System;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.BaseUtilities;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ImprobableStudios.WorldSystem2D
{

    public enum TileEdge
    {
        Top,
        Left,
        Right,
        Bottom
    }

    public enum TimeOfDay
    {
        Morning,
        Afternoon,
        Evening,
        Night
    }

    [AddComponentMenu("2D World System/World Map")]
    public class WorldMap : PersistentBehaviour, ISavable
    {
        public class WorldMapSaveState : BehaviourSaveState<WorldMap>
        {
        }
        
#if UNITY_EDITOR
        private readonly Color GRID_AWAKE_COLOR = new Color(1, 0, 1, 0.75f);

        private readonly Color GRID_ASLEEP_COLOR = new Color(1, 0, 1, 0.36f);
#endif

        [Tooltip("The number of pixels per unit")]
        [Min(1)]
        [Delayed]
        [SerializeField]
        protected int m_PixelsPerUnit = 32;

        [Tooltip("The part of the map that that renders on top of all world sprites")]
        [SerializeField]
        protected SpriteRenderer m_Foreground;

        [Tooltip("The part of the map that that renders behind all world sprites")]
        [SerializeField]
        protected SpriteRenderer m_Background;

        [Tooltip("The size of the map (in pixels)")]
        [SerializeField]
        protected Vector2 m_MapSize = new Vector2(800, 600);
        
        protected WorldSprite[] m_worldSpritesOnWorldMap;
        
        public int PixelsPerUnit
        {
            get
            {
                return m_PixelsPerUnit;
            }
            set
            {
                m_PixelsPerUnit = value;
            }
        }

        public SpriteRenderer Foreground
        {
            get
            {
                return m_Foreground;
            }
            set
            {
                m_Foreground = value;
            }
        }

        public SpriteRenderer Background
        {
            get
            {
                return m_Background;
            }
            set
            {
                m_Background = value;
            }
        }

        public Vector2 MapSize
        {
            get
            {
                return m_MapSize;
            }
            set
            {
                m_MapSize = value;
            }
        }

        public WorldSprite[] WorldSpritesOnWorldMap
        {
            get
            {
                return m_worldSpritesOnWorldMap;
            }
        }

        public virtual SaveState GetSaveState()
        {
            return new WorldMapSaveState();
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            WorldMap activeWorldMap = GetPrimaryActive<WorldMap>();

            if (activeWorldMap != null && activeWorldMap != this)
            {
                activeWorldMap.gameObject.SetActive(false);
            }

            SetPrimaryActive(this);

            if(m_worldSpritesOnWorldMap != null)
            {
                foreach(WorldSprite worldSprite in m_worldSpritesOnWorldMap)
                {
                    worldSprite.gameObject.SetActive(true);
                }
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_worldSpritesOnWorldMap = GetAllActive<WorldSprite>();
            foreach (WorldSprite worldSprite in m_worldSpritesOnWorldMap)
            {
                worldSprite.gameObject.SetActive(false);
            }
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        public void Show()
        {
            this.gameObject.SetActive(true);
        }

        public void Switch()
        {
            WorldMap activeWorldMap = GetPrimaryActive<WorldMap>();
            if (activeWorldMap != this)
            {
                if (activeWorldMap != null)
                {
                    activeWorldMap.Hide();
                }
            }
            this.Show();
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            SpriteRenderer spriteRenderer = null;

            if (spriteRenderer == null && m_Background != null)
            {
                spriteRenderer = m_Background;
            }

            if (spriteRenderer == null && m_Foreground != null)
            {
                spriteRenderer = m_Foreground;
            }

            if (spriteRenderer == null)
            {
                return;
            }

            int hotControl = GUIUtility.hotControl;
            Color color = Handles.color;
            bool enabled = GUI.enabled;

            Handles.color = GRID_AWAKE_COLOR;
            if (!this.IsParentOrSelfSelected())
            {
                GUI.enabled = false;
                Handles.color = GRID_ASLEEP_COLOR;
            }

            Sprite worldMapLayer = spriteRenderer.sprite;

            if (worldMapLayer == null)
            {
                return;
            }
            
            GUIStyle tileLabelStyle = new GUIStyle(EditorStyles.label);
            tileLabelStyle.fontStyle = FontStyle.Bold;
            tileLabelStyle.fontSize = 12;
            tileLabelStyle.alignment = TextAnchor.MiddleCenter;
            GUIStyle tooltipStyle = new GUIStyle(tileLabelStyle);
            tooltipStyle.normal.background = Texture2D.whiteTexture;
            tooltipStyle.fontSize = 10;
            
            float guiTileWidth = Math.Abs(HandleUtility.WorldToGUIPoint(new Vector3(1, 0, 0)).x - HandleUtility.WorldToGUIPoint(new Vector3(0, 0, 0)).x);
            float guiTileHeight = Math.Abs(HandleUtility.WorldToGUIPoint(new Vector3(0, 1, 0)).y - HandleUtility.WorldToGUIPoint(new Vector3(0, 0, 0)).y);
            tileLabelStyle.fixedWidth = guiTileWidth;
            tileLabelStyle.fixedHeight = guiTileHeight;
            tooltipStyle.fixedWidth = guiTileWidth;
            tooltipStyle.fixedHeight = guiTileHeight;
            tooltipStyle.padding = new RectOffset(0, 0, 0, 0);
            tooltipStyle.margin = new RectOffset(0, 0, 0, 0);

            int startingCoordinateX = (int)Math.Floor(transform.position.x);
            int startingCoordinateY = (int)Math.Floor(transform.position.y);
            
            float mapUnitWidth = (worldMapLayer.rect.width * spriteRenderer.transform.lossyScale.x) / worldMapLayer.pixelsPerUnit;
            float mapUnitHeight = (worldMapLayer.rect.height * spriteRenderer.transform.lossyScale.y) / worldMapLayer.pixelsPerUnit;

            int endingCoordinateX = (int)Math.Ceiling(transform.position.x + mapUnitWidth);
            int endingCoordinateY = (int)Math.Ceiling(transform.position.y + mapUnitHeight);

            float z = transform.position.z;

            for (int x = startingCoordinateX; x < endingCoordinateX; x++)
            {
                for (int y = startingCoordinateY; y < endingCoordinateY; y++)
                {
                    string xLabel = x.ToString();
                    string yLabel = y.ToString();
                    string tooltip = xLabel + "," + yLabel;

                    if (x == startingCoordinateX)
                    {
                        Handles.DrawLine(new Vector3(endingCoordinateX, y, z),
                                         new Vector3(startingCoordinateX, y, z));

                        Vector3 leftTileCenter = new Vector3(startingCoordinateX + 0.5f - 1, y + 0.5f, z);
                        Handles.Label(leftTileCenter, yLabel, tileLabelStyle);

                        Vector3 rightTileCenter = new Vector3(endingCoordinateX + 0.5f, y + 0.5f, z);
                        Handles.Label(rightTileCenter, new GUIContent(yLabel), tileLabelStyle);
                    }
                    if (y == startingCoordinateY)
                    {
                        Handles.DrawLine(new Vector3(x, startingCoordinateY, z),
                                         new Vector3(x, endingCoordinateY, z));

                        Vector3 topTileCenter = new Vector3(x + 0.5f, endingCoordinateY + 0.5f, z);
                        Handles.Label(topTileCenter, new GUIContent(xLabel), tileLabelStyle);

                        Vector3 bottomTileCenter = new Vector3(x + 0.5f, startingCoordinateY + 0.5f - 1, z);
                        Handles.Label(bottomTileCenter, new GUIContent(xLabel), tileLabelStyle);
                    }

                    Rect tile = new Rect(x, y, 1, 1);
                    if (tile.Contains(HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin))
                    {
                        Vector3 leftTileCenter = new Vector3(x + 0.5f, y + 0.5f, z);
                        Handles.Label(leftTileCenter, new GUIContent(tooltip), tooltipStyle);
                    }
                }
            }
            HandleUtility.Repaint();

            Handles.color = color;
            GUI.enabled = enabled;
        }

        public void UpdateTextureImportSettings(Sprite mapLayer)
        {
            string path = AssetDatabase.GetAssetPath(mapLayer.texture);
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            if (textureImporter != null)
            {
                TextureImporterSettings settings = new TextureImporterSettings();
                textureImporter.ReadTextureSettings(settings);
                settings.textureType = TextureImporterType.Sprite;
                settings.filterMode = FilterMode.Point;
                settings.npotScale = TextureImporterNPOTScale.None;
                settings.spritePixelsPerUnit = m_PixelsPerUnit;

                if (settings.spriteMode == (int)SpriteImportMode.Single)
                {
                    settings.spriteAlignment = (int)SpriteAlignment.BottomLeft;
                }

                textureImporter.SetTextureSettings(settings);

                EditorUtility.SetDirty(textureImporter);
                textureImporter.SaveAndReimport();
            }
        }

        public bool TextureImportSettingsNeedsUpdate(Sprite mapLayer)
        {
            string path = AssetDatabase.GetAssetPath(mapLayer.texture);
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            if (textureImporter != null)
            {
                TextureImporterSettings settings = new TextureImporterSettings();
                textureImporter.ReadTextureSettings(settings);
                if (settings.textureType != TextureImporterType.Sprite)
                {
                    return true;
                }
                if (settings.filterMode != FilterMode.Point)
                {
                    return true;
                }
                if (settings.npotScale != TextureImporterNPOTScale.None)
                {
                    return true;
                }
                if (settings.spritePixelsPerUnit != m_PixelsPerUnit)
                {
                    return true;
                }
                if (settings.spriteMode == (int)SpriteImportMode.Single)
                {
                    if (settings.spriteAlignment != (int)SpriteAlignment.BottomLeft)
                    {
                        return true;
                    }
                }
                if (settings.spriteMode == (int)SpriteImportMode.Multiple)
                {
                    if (textureImporter.spritesheet.Length <= 0)
                    {
                        return true;
                    }
                    else
                    {
                        SpriteMetaData metadata = textureImporter.spritesheet[0];
                        if (metadata.alignment != Convert.ToInt32(SpriteAlignment.BottomLeft))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
#endif

        public static Vector2 CoordToPixelPosition(Vector2 coord, Sprite refSprite)
        {
            return new Vector2((coord.x * refSprite.pixelsPerUnit) + (refSprite.pixelsPerUnit / 2), (coord.y * refSprite.pixelsPerUnit) + (refSprite.pixelsPerUnit / 2));
        }

        protected override void OnPropertyChanged(string propertyPath)
        {
            base.OnPropertyChanged(propertyPath);

#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                if (propertyPath == nameof(m_Foreground))
                {
                    if (m_Foreground != null && m_Foreground.sprite != null)
                    {
                        if (TextureImportSettingsNeedsUpdate(m_Foreground.sprite))
                        {
                            UpdateTextureImportSettings(m_Foreground.sprite);
                        }
                    }
                }
                if (propertyPath == nameof(m_Background))
                {
                    if (m_Background != null && m_Background.sprite != null)
                    {
                        if (TextureImportSettingsNeedsUpdate(m_Background.sprite))
                        {
                            UpdateTextureImportSettings(m_Background.sprite);
                        }
                    }
                }
            }
#endif
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_MapSize))
            {
                if (m_Background != null && m_Background.sprite != null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

    }
    
}