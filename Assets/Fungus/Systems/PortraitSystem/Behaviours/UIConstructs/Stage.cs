using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Serialization;
using DG.Tweening;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.CharacterSystem;

namespace ImprobableStudios.PortraitSystem
{

    [Serializable]
    public class PortraitState
    {
        public bool m_OnScreen;
        public bool m_Dimmed;
        public int m_PortraitIndex;
        public PortraitFacingDirection m_Facing;
        public Stage m_Stage;
        public int m_PositionIndex;
        public Image m_PortraitImage;
    }

    [AddComponentMenu("Portrait System/Stage")]
    public class Stage : UIConstruct
    {
        private static Stage s_defaultStage;
        
        [Tooltip("If true, when a character starts speaking through a dialog box, dim all non-speaking characters on this stage")]
        [SerializeField]
        [FormerlySerializedAs("dimPortraits")]
        protected bool m_DimPortraits;

        [Tooltip("The default move speed of all portraits on this stage")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("m_MoveSpeed")]
        protected float m_PortraitFadeDuration = 0.3f;

        [Tooltip("The default move speed of all portraits on this stage")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("m_MoveSpeed")]
        protected float m_PortraitMoveSpeed = 1000f;

        [Tooltip("The default shift speed of all portraits on this stage")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("m_ShiftSpeed")]
        protected float m_PortraitShiftSpeed = 1000f;

        [Tooltip("The default fade ease of all portraits on this stage")]
        [SerializeField]
        [FormerlySerializedAs("fadeEase")]
        [FormerlySerializedAs("m_FadeEase")]
        protected Ease m_PortraitFadeEase = Ease.InOutQuad;
        
        [Tooltip("The default move ease of all portraits on this stage")]
        [SerializeField]
        [FormerlySerializedAs("moveEase")]
        [FormerlySerializedAs("m_MoveEase")]
        protected Ease m_PortraitMoveEase = Ease.InOutQuad;

        [Tooltip("The default shift ease of all portraits on this stage")]
        [SerializeField]
        [FormerlySerializedAs("m_ShiftEase")]
        protected Ease m_PortraitShiftEase = Ease.InOutQuad;

        [Tooltip("The positions where portraits can appear on this stage")]
        [SerializeField]
        [FormerlySerializedAs("positions")]
        [FormerlySerializedAs("m_Positions")]
        [CheckNotEmpty]
        protected List<StagePosition> m_PortraitPositions = new List<StagePosition>();

        protected Dictionary<Character, PortraitState> m_portraitStates = new Dictionary<Character, PortraitState>();

        private Canvas m_canvas;
        
        public static Stage DefaultStage
        {
            get
            {
                if (s_defaultStage == null)
                {
                    s_defaultStage = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<Stage>("~Stage"));
                }
                return s_defaultStage;
            }
        }


        public bool DimPortraits
        {
            get
            {
                return m_DimPortraits;
            }
            set
            {
                m_DimPortraits = value;
            }
        }

        public float PortraitFadeDuration
        {
            get
            {
                return m_PortraitFadeDuration;
            }
            set
            {
                m_PortraitFadeDuration = value;
            }
        }

        public float PortraitMoveSpeed
        {
            get
            {
                return m_PortraitMoveSpeed;
            }
            set
            {
                m_PortraitMoveSpeed = value;
            }
        }

        public float PortraitShiftSpeed
        {
            get
            {
                return m_PortraitShiftSpeed;
            }
            set
            {
                m_PortraitShiftSpeed = value;
            }
        }

        public Ease PortraitFadeEase
        {
            get
            {
                return m_PortraitFadeEase;
            }
            set
            {
                m_PortraitFadeEase = value;
            }
        }

        public Ease PortraitMoveEase
        {
            get
            {
                return m_PortraitMoveEase;
            }
            set
            {
                m_PortraitMoveEase = value;
            }
        }

        public Ease PortraitShiftEase
        {
            get
            {
                return m_PortraitShiftEase;
            }
            set
            {
                m_PortraitShiftEase = value;
            }
        }

        public StagePosition[] PortraitPositions
        {
            get
            {
                return m_PortraitPositions.ToArray();
            }
        }

        public Dictionary<Character, PortraitState> PortraitStates
        {
            get
            {
                return m_portraitStates;
            }
        }

        public Canvas Canvas
        {
            get
            {
                if (m_canvas == null)
                {
                    m_canvas = GetComponentInChildren<Canvas>();
                }
                return m_canvas;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Character.OnSpeak.AddListener(OnSpeak);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Character.OnSpeak.RemoveListener(OnSpeak);
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            foreach (KeyValuePair<Character, PortraitState> kvp in m_portraitStates)
            {
                SetupPortrait(kvp.Key, kvp.Value);
            }
        }
        
        public StagePosition GetPosition(int positionIndex)
        {
            if (positionIndex < 0 || positionIndex >= m_PortraitPositions.Count)
            {
                return null;
            }
            return m_PortraitPositions[positionIndex];
        }

        public int GetPositionIndex(StagePosition position)
        {
            return m_PortraitPositions.IndexOf(position);
        }

        protected virtual void Start()
        {
            if (Canvas != null)
            {
                // Ensure the stage canvas is active
                Canvas.gameObject.SetActive(true);
            }
        }

        public static Image CreatePortraitObject(string name)
        {
            GameObject portraitObj = new GameObject(name + " (Portrait)", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image));
            Image portraitImage = portraitObj.GetComponent<Image>();
            portraitImage.preserveAspect = true;
            portraitImage.type = Image.Type.Tiled;
            Material portraitMaterial = Instantiate(Resources.Load("~CrossfadeMaterial")) as Material;
            portraitImage.material = portraitMaterial;
            return portraitImage;
        }

        public void SetupPortrait(Character character, PortraitState portraitState)
        {
            int portraitIndex = portraitState.m_PortraitIndex;
            Portrait portrait = character.Portraits[portraitIndex];
            int positionIndex = portraitState.m_PositionIndex;

            SetRectTransform(portraitState.m_PortraitImage.GetComponent<RectTransform>(), m_PortraitPositions[positionIndex].GetComponent<RectTransform>());
            portraitState.m_PortraitImage.material.SetFloat("_Fade", 0);
            if (portraitState.m_OnScreen)
            {
                portraitState.m_PortraitImage.material.SetTexture("_TexStart", portrait.m_Sprite.texture);
            }
            else
            {
                Texture2D blankTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                blankTexture.SetPixel(0, 0, new Color(0f, 0f, 0f, 0f));
                blankTexture.Apply();
                portraitState.m_PortraitImage.material.SetTexture("_TexStart", blankTexture as Texture);
            }
            if (portraitState.m_Facing != portrait.m_Facing)
            {
                portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 1);
            }
            else
            {
                portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 0);
            }
            if (portraitState.m_Dimmed)
            {
                portraitState.m_PortraitImage.material.SetColor("_Color", new Color(0.5f, 0.5f, 0.5f, 1f));
            }
            else
            {
                portraitState.m_PortraitImage.material.SetColor("_Color", new Color(1f, 1f, 1f, 1f));
            }
            portraitState.m_PortraitImage.material.SetFloat("_Alpha", 1);
            portraitState.m_PortraitImage.type = Image.Type.Simple;
        }

        public static void SetRectTransform(RectTransform portraitRectTransform, RectTransform positionRectTransform)
        {
            portraitRectTransform.SetParent(positionRectTransform, true);
            portraitRectTransform.GetComponent<RectTransform>().anchorMin = positionRectTransform.anchorMin;
            portraitRectTransform.GetComponent<RectTransform>().anchorMax = positionRectTransform.anchorMax;
            portraitRectTransform.GetComponent<RectTransform>().pivot = positionRectTransform.pivot;
            portraitRectTransform.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
            portraitRectTransform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            portraitRectTransform.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            portraitRectTransform.GetComponent<RectTransform>().sizeDelta = positionRectTransform.sizeDelta;
        }
        
        private PortraitState GetPortraitState(Character character)
        {
            PortraitState portraitState = null;
            if (m_portraitStates.ContainsKey(character))
            {
                portraitState = m_portraitStates[character];
            }
            else
            {
                portraitState = new PortraitState();
                if (portraitState.m_PortraitImage == null)
                {
                    portraitState.m_PortraitImage = CreatePortraitObject(character.name);
                }
                m_portraitStates[character] = portraitState;
            }

            return portraitState;
        }

        public void ShowPortrait(Character character, Sprite portraitSprite, PortraitFacingDirection facing, StagePosition position, float fadeDuration, Ease fadeEase, float moveSpeed, Ease moveEase, UnityAction onKill, UnityAction onComplete)
        {
            gameObject.SetActive(true);

            SetPrimaryActive(this);

            PortraitState portraitState = GetPortraitState(character);

            if (!portraitState.m_OnScreen)
            {
                moveSpeed = 0;
            }

            // Selected "use previous portraitSprite"
            if (portraitSprite == null)
            {
                if (portraitState.m_PortraitIndex >= 0 && portraitState.m_PortraitIndex < character.Portraits.Length)
                {
                    portraitSprite = character.Portraits[portraitState.m_PortraitIndex].m_Sprite;
                }
            }
            
            // Selected "use previous position"
            if (position == null)
            {
                if (portraitState.m_PositionIndex >= 0 && portraitState.m_PositionIndex < m_PortraitPositions.Count)
                {
                    position = m_PortraitPositions[portraitState.m_PositionIndex];
                }
            }
            
            // Selected "use previous facing direction"
            if (facing == PortraitFacingDirection.Front)
            {
                facing = portraitState.m_Facing;
            }

            SetupPortrait(character, portraitState);

            Portrait portrait = character.GetPortrait(portraitSprite);
            if (portrait != null)
            {
                if (facing != portrait.m_Facing)
                {
                    if (portraitState.m_PositionIndex != m_PortraitPositions.IndexOf(position))
                    {
                        if (moveSpeed <= 0)
                        {
                            portraitState.m_PortraitImage.material.SetTexture("_TexStart", GetBlankTexture());
                        }
                        portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 1);
                    }
                    portraitState.m_PortraitImage.material.SetFloat("_FlipEnd", 1);
                }
                else
                {
                    if (portraitState.m_PositionIndex != m_PortraitPositions.IndexOf(position))
                    {
                        if (moveSpeed <= 0)
                        {
                            portraitState.m_PortraitImage.material.SetTexture("_TexStart", GetBlankTexture());
                        }
                        portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 0);
                    }
                    portraitState.m_PortraitImage.material.SetFloat("_FlipEnd", 0);
                }
            }

            foreach (Character c in m_portraitStates.Keys.ToList())
            {
                if (c != character && m_PortraitPositions.IndexOf(position) == m_portraitStates[c].m_PositionIndex)
                {
                    HidePortrait(c, facing, null, fadeDuration, fadeEase, moveSpeed, moveEase, true, null, null);
                }
            }

            portraitState.m_PortraitImage.material.SetTexture("_TexEnd", portraitSprite.texture);

            GetSequence(character, position, fadeDuration, fadeEase, moveSpeed, moveEase)
                .OnKill(
                    () =>
                    {
                        onKill?.Invoke();
                    })
                .OnComplete(
                    () =>
                    {
                        onComplete?.Invoke();
                    });

            portraitState.m_OnScreen = true;
            portraitState.m_PortraitIndex = character.GetPortraitIndex(portrait);
            portraitState.m_Facing = facing;
            portraitState.m_Stage = this;
            if (position != null)
            {
                portraitState.m_PositionIndex = this.m_PortraitPositions.IndexOf(position);
            }
            m_portraitStates[character] = portraitState;
        }

        public void HidePortrait(Character character, PortraitFacingDirection facing, StagePosition position, float fadeDuration, Ease fadeEase, float moveSpeed, Ease moveEase, bool removeFromStage, UnityAction onKill, UnityAction onComplete)
        {
            gameObject.SetActive(true);

            SetPrimaryActive(this);

            PortraitState portraitState = GetPortraitState(character);

            if (!portraitState.m_OnScreen)
            {
                return;
            }

            Sprite portraitSprite = null;

            if (portraitState.m_PortraitIndex >= 0 && portraitState.m_PortraitIndex < character.Portraits.Length)
            {
                portraitSprite = character.Portraits[portraitState.m_PortraitIndex].m_Sprite;
            }

            // Selected "use previous facing direction"
            if (facing == PortraitFacingDirection.Front)
            {
                facing = portraitState.m_Facing;
            }

            // Selected "use previous position"
            if (position == null)
            {
                if (portraitState.m_PositionIndex >= 0 && portraitState.m_PositionIndex < m_PortraitPositions.Count)
                {
                    position = m_PortraitPositions[portraitState.m_PositionIndex];
                }
            }

            SetupPortrait(character, portraitState);
            
            Portrait portrait = character.GetPortrait(portraitSprite);
            if (portrait != null)
            {
                if (facing != portrait.m_Facing)
                {
                    portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 1);
                    portraitState.m_PortraitImage.material.SetFloat("_FlipEnd", 1);
                }
                else
                {
                    portraitState.m_PortraitImage.material.SetFloat("_FlipStart", 0);
                    portraitState.m_PortraitImage.material.SetFloat("_FlipEnd", 0);
                }
            }
            
            portraitState.m_PortraitImage.material.SetTexture("_TexEnd", GetBlankTexture());

            GetSequence(character, position, fadeDuration, fadeEase, moveSpeed, moveEase)
                .OnKill(
                    () =>
                    {
                        onKill?.Invoke();
                    })
                .OnComplete(
                    () =>
                    {
                        onComplete?.Invoke();
                    });

            portraitState.m_OnScreen = false;
            portraitState.m_Stage = this;
            if (position != null)
            {
                portraitState.m_PositionIndex = this.m_PortraitPositions.IndexOf(position);
            }
            if (removeFromStage)
            {
                m_portraitStates.Remove(character);
            }
        }

        private Texture GetBlankTexture()
        {
            Texture2D blankTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            blankTexture.SetPixel(0, 0, new Color(0f, 0f, 0f, 0f));
            blankTexture.Apply();
            return blankTexture as Texture;
        }

        public void MovePortraitToFront(Character character)
        {
            gameObject.SetActive(true);

            PortraitState portraitState = GetPortraitState(character);

            portraitState.m_PortraitImage.transform.SetSiblingIndex(portraitState.m_PortraitImage.transform.parent.childCount);
        }

        protected Sequence GetSequence(Character character, StagePosition position, float fadeDuration, Ease fadeEase, float moveSpeed, Ease moveEase)
        {
            Sequence sequence = DOTween.Sequence();

            PortraitState portraitState = GetPortraitState(character);

            if (position != null)
            {
                portraitState.m_PortraitImage.rectTransform.SetParent(position.transform, true);

                Vector2 endAnchorPosition = Vector2.zero;

                if (moveSpeed <= 0)
                {
                    if (portraitState.m_PositionIndex != m_PortraitPositions.IndexOf(position) || !portraitState.m_OnScreen)
                    {
                        portraitState.m_PortraitImage.rectTransform.anchoredPosition = position.ShiftOffset;
                        sequence.Join(
                            portraitState.m_PortraitImage.rectTransform.DOAnchorPos(endAnchorPosition, m_PortraitShiftSpeed)
                            .SetEase(m_PortraitShiftEase)
                            .SetSpeedBased(true));
                    }
                }
                else
                {
                    sequence.Join(
                       portraitState.m_PortraitImage.rectTransform.DOAnchorPos(endAnchorPosition, moveSpeed)
                        .SetEase(moveEase)
                        .SetSpeedBased(true));
                }
            }

            if (fadeDuration <= 0)
            {
                portraitState.m_PortraitImage.material.SetFloat("_Fade", 1);
            }
            else
            {
                sequence.Join(
                portraitState.m_PortraitImage.material.DOFloat(1, "_Fade", fadeDuration)
                    .SetEase(fadeEase));
            }

            return sequence;
        }

        public void Dim(Character character)
        {
            PortraitState portraitState = GetPortraitState(character);

            if (portraitState.m_Dimmed == false)
            {
                if (portraitState.m_PortraitImage != null)
                {
                    float duration = m_PortraitFadeDuration;
                    if (duration == 0) duration = float.Epsilon;
                    Color color = new Color(0.5f, 0.5f, 0.5f, 1f);
                    portraitState.m_PortraitImage.material.DOColor(color, "_Color", duration)
                        .SetEase(m_PortraitFadeEase)
                        .OnComplete(
                            () => 
                            {
                                portraitState.m_PortraitImage.material.SetColor("_Color", color);
                                portraitState.m_Dimmed = true;
                            });
                }
            }
        }

        public void Undim(Character character)
        {
            PortraitState portraitState = GetPortraitState(character);

            if (portraitState.m_Dimmed == true)
            {
                float duration = m_PortraitFadeDuration;
                if (duration == 0) duration = float.Epsilon;
                Color color = new Color(1f, 1f, 1f, 1f);
                portraitState.m_PortraitImage.material.DOColor(color, "_Color", duration)
                    .SetEase(m_PortraitFadeEase)
                    .OnComplete(
                        () => 
                        {
                            portraitState.m_PortraitImage.material.SetColor("_Color", color);
                            portraitState.m_Dimmed = false;
                        });
            }
        }

        public void UndimAllPortraits()
        {
            foreach (Character character in m_portraitStates.Keys)
            {
                Undim(character);
            }
        }

        public void DimNonSpeakingPortraits(Character speakingCharacter)
        {
            if (m_DimPortraits && m_portraitStates.ContainsKey(speakingCharacter))
            {
                foreach (Character c in m_portraitStates.Keys)
                {
                    if (c != speakingCharacter)
                    {
                        Dim(c);
                    }
                    else
                    {
                        Undim(c);
                    }
                }
            }
        }

        protected override void ShowUI(bool instantly, UnityAction onKill, UnityAction onComplete)
        {
            gameObject.SetActive(true);

            float fadeDuration = m_PortraitFadeDuration;
            float moveSpeed = m_PortraitMoveSpeed;
            if (instantly)
            {
                fadeDuration = 0;
                moveSpeed = 0;
            }

            bool first = true;
            foreach (KeyValuePair<Character, PortraitState> kvp in m_portraitStates)
            {
                UnityAction doOnKill = null;
                UnityAction doOnComplete = null;
                if (first)
                {
                    first = false;
                    doOnKill = onKill;
                    doOnComplete = onComplete;
                }
                ShowPortrait(kvp.Key, kvp.Key.Portraits[kvp.Value.m_PortraitIndex].m_Sprite, kvp.Value.m_Facing, m_PortraitPositions[kvp.Value.m_PositionIndex], fadeDuration, m_PortraitFadeEase, moveSpeed, m_PortraitMoveEase, doOnKill, doOnComplete);
            }
        }

        protected override void HideUI(bool instantly, bool disableOnHidden, UnityAction onKill, UnityAction onComplete)
        {
            float fadeDuration = m_PortraitFadeDuration;
            float moveSpeed = m_PortraitMoveSpeed;
            if (instantly)
            {
                fadeDuration = 0;
                moveSpeed = 0;
            }

            bool first = true;
            foreach (Character c in m_portraitStates.Keys.ToList())
            {
                UnityAction doOnKill = null;
                UnityAction doOnComplete = null;
                if (first)
                {
                    first = false;
                    doOnKill = onKill;
                    doOnComplete = onComplete;
                }
                HidePortrait(c, PortraitFacingDirection.Front, null, fadeDuration, m_PortraitFadeEase, moveSpeed, m_PortraitMoveEase, false, doOnKill, doOnComplete);
            }

            DOVirtual.DelayedCall(
                fadeDuration, 
                () =>
                {
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
        }

        public void MoveToFront()
        {
            gameObject.SetActive(true);

            SetPrimaryActive(this);

            foreach (Stage s in GetAllActive<Stage>())
            {
                if (s == this)
                {
                    s.Canvas.sortingOrder = 1;
                }
                else
                {
                    s.Canvas.sortingOrder = 0;
                }
            }
        }
        
        public void OnSpeak(Character character)
        {
            if (m_DimPortraits)
            {
                DimNonSpeakingPortraits(character);
            }
        }
    }
}

