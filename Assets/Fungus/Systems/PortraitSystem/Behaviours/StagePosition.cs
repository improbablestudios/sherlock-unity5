﻿using UnityEngine;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.PortraitSystem
{

    [AddComponentMenu("Portrait System/Stage Position")]
    public class StagePosition : PersistentBehaviour
    {
        [Tooltip("The offset at which portraits are first shown in this position and then slide into place.")]
        [SerializeField]
        protected Vector2 m_ShiftOffset;

        public Vector2 ShiftOffset
        {
            get
            {
                return m_ShiftOffset;
            }
            set
            {
                m_ShiftOffset = value;
            }
        }
    }

}