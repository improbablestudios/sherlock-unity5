#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.PortraitSystem
{

    public class PortraitSystemMenuItems
    {
        [MenuItem("Tools/Portrait System/Stage", false, 10)]
        [MenuItem("GameObject/Portrait System/Stage", false, 10)]
        static void CreateStage()
        {
            PrefabSpawner.SpawnPrefab<Stage>("~Stage", true);
        }

        [MenuItem("Tools/Portrait System/Stage Position", false, 100)]
        [MenuItem("GameObject/Portrait System/Stage Position", false, 100)]
        static void CreateStagePosition()
        {
            PrefabSpawner.SpawnPrefab<StagePosition>("~StagePosition", true);
        }
    }

}

#endif