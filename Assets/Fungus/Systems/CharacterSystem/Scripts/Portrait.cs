﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace ImprobableStudios.CharacterSystem
{
    public enum PortraitFacingDirection
    {
        Front,
        Left,
        Right
    }

    [Serializable]
    public class Portrait
    {
        [FormerlySerializedAs("sprite")]
        public Sprite m_Sprite;

        [FormerlySerializedAs("facing")]
        public PortraitFacingDirection m_Facing;
    }
}
