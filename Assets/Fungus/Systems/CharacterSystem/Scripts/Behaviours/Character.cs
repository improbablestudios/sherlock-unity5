using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.Events;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.TextStylePaletteUtilities;

namespace ImprobableStudios.CharacterSystem
{

    [AddComponentMenu("Character System/Character")]
    public class Character : PersistentBehaviour, ISavable, ILocalizable
    {
        public class SpeakEvent : UnityEvent<Character>
        {
        }

        [Serializable]
        public class CharacterSaveState : BehaviourSaveState<Character>
        {
        }
        
        [Tooltip("The name of the character")]
        [Localize]
        [SerializeField]
        [FormerlySerializedAs("nameText")]
        [FormerlySerializedAs("m_NameText")]
        protected string m_DisplayName;

        [Tooltip("The sound to play when the character is talking")]
        [FormerlySerializedAs("voiceSoundEffect")]
        [SerializeField]
        protected AudioClip m_VoiceSoundEffect;

        [Tooltip("The color that represents this character")]
        [SerializeField]
        [FormerlySerializedAs("nameColor")]
        protected Color m_Color = Color.white;

        [Tooltip("The text style used for this character's dialog")]
        [SerializeField]
        protected TextStyle m_TextStyle;

        [Tooltip("Portraits that represent this character")]
        [FormerlySerializedAs("portraits")]
        [SerializeField]
        protected Portrait[] m_Portraits = new Portrait[0];
        
        protected static SpeakEvent m_onSpeak = new SpeakEvent();
        
        public string DisplayName
        {
            get
            {
                return m_DisplayName;
            }
            set
            {
                m_DisplayName = value;
            }
        }

        public AudioClip VoiceSoundEffect
        {
            get
            {
                return m_VoiceSoundEffect;
            }
            set
            {
                m_VoiceSoundEffect = value;
            }
        }

        public Color Color
        {
            get
            {
                return m_Color;
            }
            set
            {
                m_Color = value;
            }
        }

        public TextStyle TextStyle
        {
            get
            {
                return m_TextStyle;
            }
            set
            {
                m_TextStyle = value;
            }
        }

        public Portrait[] Portraits
        {
            get
            {
                return m_Portraits;
            }
            set
            {
                m_Portraits = value;
            }
        }

        public static SpeakEvent OnSpeak
        {
            get
            {
                return m_onSpeak;
            }
        }
        
        public virtual SaveState GetSaveState()
        {
            return new CharacterSaveState();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public Sprite GetPortrait(int portraitIndex)
        {
            if (portraitIndex < 0 || portraitIndex >= m_Portraits.Length)
            {
                return null;
            }
            return m_Portraits[portraitIndex].m_Sprite;
        }

        public Portrait GetPortrait(Sprite sprite)
        {
            for (int i = 0; i < m_Portraits.Length; i++)
            {
                Portrait p = m_Portraits[i];
                if (p.m_Sprite == sprite)
                {
                    return p;
                }
            }
            return null;
        }

        public int GetPortraitIndex(Sprite sprite)
        {
            for (int i = 0; i < m_Portraits.Length; i++)
            {
                Portrait p = m_Portraits[i];
                if (p.m_Sprite == sprite)
                {
                    return i;
                }
            }
            return -1;
        }

        public int GetPortraitIndex(Portrait portrait)
        {
            for (int i = 0; i < m_Portraits.Length; i++)
            {
                Portrait p = m_Portraits[i];
                if (p == portrait)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Speak()
        {
            m_onSpeak.Invoke(this);
        }

        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return null;
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}