#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.CharacterSystem
{

    public class CharacterSystemMenuItems
    {
        [MenuItem("Tools/Character System/Character", false, 10)]
        [MenuItem("GameObject/Character System/Character", false, 10)]
        static void CreateCharacter()
        {
            PrefabSpawner.SpawnPrefab<Character>("~Character", true);
        }
    }
}

#endif