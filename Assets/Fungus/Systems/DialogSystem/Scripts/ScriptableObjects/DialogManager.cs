﻿using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SaveSystem;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.DialogSystem
{
    
    public class DialogManager : SettingsScriptableObject<DialogManager>, ISavableSetting
    {
        [Serializable]
        public class DialogManagerSaveState : ScriptableObjectSaveState<DialogManager>
        {
            public override int LoadPriority()
            {
                return -2;
            }
        }

        [Tooltip("If true, allow the player to use the submit button to advance dialog")]
        [SerializeField]
        public bool m_UseSubmitButtonToAdvanceDialog = true;

        [Tooltip("If true, allow the player to use the skip button to skip dialog")]
        [SerializeField]
        public bool m_UseSkipButtonToSkipDialog = true;

        [Tooltip("If true, allow the player to use the skip button to skip dialog")]
        [SerializeField]
        public string m_SkipButton = "Fire3";

        [Tooltip("The amount of delay before a dialog box automatically advances (if auto is on)")]
        [Min(0)]
        [SerializeField]
        protected float m_AutoAdvanceDelay = 1f;

        [Tooltip("If true, the system will only skip dialog that has already been seen. Otherwise, the system will skip all dialog")]
        [SerializeField]
        protected bool m_OnlySkipSeenDialog = true;

        [Tooltip("If true, the system will record dialog history")]
        [SerializeField]
        protected bool m_RecordHistory = true;
        
        protected bool m_ignoreInput;
        
        protected bool m_isAutoOn;
        
        protected bool m_isSkipOn;

        private UnityEvent m_onAutoChanged = new UnityEvent();
        
        private UnityEvent m_onSkipChanged = new UnityEvent();
        
        public bool UseSubmitButtonToAdvanceDialog
        {
            get
            {
                return m_UseSubmitButtonToAdvanceDialog;
            }
            set
            {
                m_UseSubmitButtonToAdvanceDialog = value;
            }
        }

        public bool UseSkipButtonToSkipDialog
        {
            get
            {
                return m_UseSkipButtonToSkipDialog;
            }
            set
            {
                m_UseSkipButtonToSkipDialog = value;
            }
        }

        public string SkipButton
        {
            get
            {
                return m_SkipButton;
            }
            set
            {
                m_SkipButton = value;
            }
        }

        public float AutoAdvanceDelay
        {
            get
            {
                return m_AutoAdvanceDelay;
            }
            set
            {
                m_AutoAdvanceDelay = value;
            }
        }

        public bool OnlySkipSeenDialog
        {
            get
            {
                return m_OnlySkipSeenDialog;
            }
            set
            {
                m_OnlySkipSeenDialog = value;
            }
        }

        public bool RecordHistory
        {
            get
            {
                return m_RecordHistory;
            }
            set
            {
                m_RecordHistory = value;
            }
        }

        public bool IgnoreInput
        {
            get
            {
                return m_ignoreInput;
            }
            set
            {
                m_ignoreInput = value;
            }
        }

        public bool IsAutoOn
        {
            get
            {
                return m_isAutoOn;
            }
            set
            {
                m_isAutoOn = value;
                OnAutoChanged.Invoke();
            }
        }

        public bool IsSkipOn
        {
            get
            {
                return m_isSkipOn;
            }
            set
            {
                m_isSkipOn = value;
                OnSkipChanged.Invoke();
            }
        }

        public UnityEvent OnAutoChanged
        {
            get
            {
                return m_onAutoChanged;
            }
        }
        
        public UnityEvent OnSkipChanged
        {
            get
            {
                return m_onSkipChanged;
            }
        }

        public SaveState GetSaveState()
        {
            return new DialogManagerSaveState();
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_SkipButton))
            {
                if (!m_UseSkipButtonToSkipDialog)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}
