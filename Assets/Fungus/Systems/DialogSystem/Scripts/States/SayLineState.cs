﻿using ImprobableStudios.CharacterSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ImprobableStudios.DialogSystem
{

    [Serializable]
    public class SayLineState : DialogLineState
    {
        protected Character m_character;
        protected string m_nameText;
        protected Dictionary<string, string> m_nameLocalizedDictionary;
        protected AudioClip m_voiceOver;
        protected Dictionary<string, AudioClip> m_voiceOverLocalizedDictionary;
        protected Sprite m_portrait;
        protected bool m_instantlyAutoAdvance;

        public Character Character
        {
            get
            {
                return m_character;
            }
        }

        public string NameText
        {
            get
            {
                return m_nameText;
            }
        }

        public Dictionary<string, string> NameLocalizedDictionary
        {
            get
            {
                return m_nameLocalizedDictionary;
            }
        }

        public AudioClip VoiceOver
        {
            get
            {
                return m_voiceOver;
            }
        }

        public Dictionary<string, AudioClip> VoiceOverLocalizedDictionary
        {
            get
            {
                return m_voiceOverLocalizedDictionary;
            }
        }

        public Sprite Portrait
        {
            get
            {
                return m_portrait;
            }
        }

        public bool InstantlyAutoAdvance
        {
            get
            {
                return m_instantlyAutoAdvance;
            }
        }

        public SayLineState(
            Dictionary<string, string> dialogLocalizedDictionary,
            string dialogText,
            Character character,
            Dictionary<string, string> nameLocalizedDictionary,
            string nameText,
            Dictionary<string, AudioClip> voiceOverLocalizedDictionary,
            AudioClip voiceOver,
            Sprite portrait,
            bool continuePrevious,
            bool instantlyAutoAdvance) : base(dialogLocalizedDictionary, dialogText, continuePrevious)
        {
            m_character = character;
            m_nameLocalizedDictionary = nameLocalizedDictionary;
            m_nameText = nameText;
            m_voiceOverLocalizedDictionary = voiceOverLocalizedDictionary;
            m_voiceOver = voiceOver;
            m_portrait = portrait;
            m_instantlyAutoAdvance = instantlyAutoAdvance;
        }
    }

}