﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ImprobableStudios.DialogSystem
{

    [Serializable]
    public class ChoiceLineState : DialogLineState
    {
        [SerializeField]
        protected Sprite m_icon;

        protected UnityAction m_callback;
        
        public Sprite Icon
        {
            get
            {
                return m_icon;
            }
        }

        public UnityAction Callback
        {
            get
            {
                return m_callback;
            }
        }
        
        public ChoiceLineState(
            Dictionary<string, string> dialogLocalizedDictionary,
            string dialogText,
            Sprite icon,
            UnityAction onChosen) : base(dialogLocalizedDictionary, dialogText, false)
        {
            m_icon = icon;
            m_callback = onChosen;
        }
    }


}