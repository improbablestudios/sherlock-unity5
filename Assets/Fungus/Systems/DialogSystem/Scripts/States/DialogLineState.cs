﻿using System;
using System.Collections.Generic;

namespace ImprobableStudios.DialogSystem
{
    
    [Serializable]
    public class DialogLineState
    {
        protected int m_activeDialogLineIndex;
        
        protected string m_dialogText;

        protected Dictionary<string, string> m_dialogLocalizedDictionary;

        protected bool m_continuePrevious;

        protected bool m_wasVisited;

        public int ActiveDialogLineIndex
        {
            get
            {
                return m_activeDialogLineIndex;
            }
            set
            {
                m_activeDialogLineIndex = value;
            }
        }

        public string DialogText
        {
            get
            {
                return m_dialogText;
            }
        }

        public Dictionary<string, string> DialogLocalizedDictionary
        {
            get
            {
                return m_dialogLocalizedDictionary;
            }
        }

        public bool ContinuePrevious
        {
            get
            {
                return m_continuePrevious;
            }
        }

        public bool WasVisited
        {
            get
            {
                return m_wasVisited;
            }
            set
            {
                m_wasVisited = value;
            }
        }

        public DialogLineState(int activeDialogLineIndex, bool wasVisited)
        {
            m_activeDialogLineIndex = activeDialogLineIndex;
            m_wasVisited = wasVisited;
        }

        public DialogLineState(
            Dictionary<string, string> dialogLocalizedDictionary,
            string dialogText,
            bool continuePrevious)
        {
            m_dialogText = dialogText;
            m_dialogLocalizedDictionary = dialogLocalizedDictionary;
            m_continuePrevious = continuePrevious;
        }
    }
    
}