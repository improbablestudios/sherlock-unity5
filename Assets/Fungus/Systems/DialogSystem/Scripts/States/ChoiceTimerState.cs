﻿using System;
using UnityEngine.Events;

namespace ImprobableStudios.DialogSystem
{

    [Serializable]
    public class ChoiceTimerState
    {
        protected float m_duration;

        protected int m_targetDialogLineIndex;

        protected UnityAction m_callback;

        public float Duration
        {
            get
            {
                return m_duration;
            }
        }

        public int TargetDialogLineIndex
        {
            get
            {
                return m_targetDialogLineIndex;
            }
        }

        public UnityAction Callback
        {
            get
            {
                return m_callback;
            }
        }

        public ChoiceTimerState(float duration, int targetDialogLineIndex, UnityAction onTimerFinished)
        {
            m_duration = duration;
            m_targetDialogLineIndex = targetDialogLineIndex;
            m_callback = onTimerFinished;
        }
    }

}