﻿using UnityEngine.Events;

namespace ImprobableStudios.DialogSystem
{

    public interface ISayUI : IDialogUI
    {
        void DisplaySay(SayLineState sayState, UnityAction onComplete = null);
        bool CanDisplayPortrait();
        bool CanDisplayName();
        bool CanPlayVoiceOver();
        SayLineUI GetSayLineUI();
        SayLineState[] GetActiveSayLineStates();
    }

}