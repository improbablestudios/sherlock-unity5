﻿namespace ImprobableStudios.DialogSystem
{

    public interface IDialogUI
    {
        DialogUI GetDialogUI();
    }

}