﻿using UnityEngine.Events;

namespace ImprobableStudios.DialogSystem
{

    public interface IChoiceUI : IDialogUI
    {
        void DisplayChoice(ChoiceLineState choiceState);
        void DisplayChoiceTimer(float duration, int targetDialogLineIndex, UnityAction callback);
        void CancelChoices(UnityAction onComplete = null);
        UnityEvent GetOnNextChoiceFinishedEvent();
        void InvokeChoiceFinishedEvent();
        bool CanDisplayIcon();
        ChoiceLineUI GetChoiceLineUI();
        ChoiceLineState[] GetActiveChoiceLineStates();
    }

}