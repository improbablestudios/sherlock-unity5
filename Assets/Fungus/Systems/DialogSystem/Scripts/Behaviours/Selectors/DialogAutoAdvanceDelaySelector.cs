﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.SaveSystem;

namespace ImprobableStudios.DialogSystem
{

    [Serializable]
    [AddComponentMenu("Dialog System/Selector/Dialog Auto Advance Delay Selector")]
    public class DialogAutoAdvanceDelaySelector : SettingSelector<Slider>
    {
        [Tooltip("The slider that can be used to select the auto advance delay for dialog")]
        [SerializeField]
        [FormerlySerializedAs("slider")]
        protected Slider m_Slider;

        public Slider Slider
        {
            get
            {
                return m_Slider;
            }
            set
            {
                m_Slider = value;
            }
        }

        public override Slider GetControl()
        {
            return m_Slider;
        }

        public override void SetControl(Slider control)
        {
            m_Slider = control;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Slider != null)
            {
                m_Slider.value = DialogManager.Instance.AutoAdvanceDelay;
                m_Slider.onValueChanged.AddListener(OnValueChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Slider != null)
            {
                m_Slider.onValueChanged.RemoveListener(OnValueChanged);
            }
        }

        protected virtual void OnValueChanged(float value)
        {
            DialogManager.Instance.AutoAdvanceDelay = value;
            OnSettingChanged();
        }
    }

}