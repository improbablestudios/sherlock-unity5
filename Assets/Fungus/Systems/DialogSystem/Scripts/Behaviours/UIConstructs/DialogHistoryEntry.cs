﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImprobableStudios.UIUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ImprobableStudios.DialogSystem
{
    
    public abstract class DialogHistoryEntry<T> : DialogHistoryEntry where T : DialogLineState
    {
        protected List<T> m_dialogLineStates = new List<T>();
        
        public T[] Infos
        {
            get
            {
                return m_dialogLineStates.ToArray();
            }
            set
            {
                m_dialogLineStates = value.ToList();
            }
        }
        
        public override Type GetDialogLineStateType()
        {
            return typeof(T);
        }

        public override sealed void AddEntryInfo(DialogUI dialogUI, DialogLineState dialogLineState)
        {
            T dialogLineStateT = dialogLineState as T;
            if (dialogLineStateT != null)
            {
                m_dialogUI = dialogUI;
                m_dialogLineStates.Add(dialogLineStateT);

                RestoreDialogLine(dialogLineState);
            }
        }

        protected abstract void SetupDialogLine(T dialogLineState);

        protected abstract void StartDialogLine(T dialogLineState, UnityAction onComplete = null);

        protected virtual void RestoreDialogLine(T dialogLineState)
        {
            Writer writer = DialogHistory.Writer;

            if (m_DialogText != null)
            {
                if (!dialogLineState.ContinuePrevious)
                {
                    m_DialogText.text = "";
                }

                m_DialogText.text += writer.GetFormattedText(dialogLineState.DialogText);
            }
        }

        public override sealed void ShowDialogLine(DialogLineState dialogLineState, float delay, UnityAction onComplete = null)
        {
            SetupDialogLine(dialogLineState as T);
            Show(delay, () => StartDialogLine(dialogLineState as T, onComplete));
        }

        public override sealed void RestoreDialogLine(DialogLineState dialogLineState)
        {
            SetupDialogLine(dialogLineState as T);
            RestoreDialogLine(dialogLineState as T);
        }
    }

    public abstract class DialogHistoryEntry : DialogLineUI
    {
        private DialogHistory m_dialogHistory;
        
        public DialogHistory DialogHistory
        {
            get
            {
                if (m_dialogHistory == null)
                {
                    m_dialogHistory = GetComponentInParent<DialogHistory>();
                }
                return m_dialogHistory;
            }
        }

        public abstract void AddEntryInfo(DialogUI dialogUI, DialogLineState dialogLineState);
    }

}