﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.InputUtilities;
using System.Linq;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace ImprobableStudios.DialogSystem
{
    
    [AddComponentMenu("Dialog System/Dialog Scroll")]
    public class DialogScroll : DialogUI, ISayUI, IChoiceUI
    {
        public static DialogScroll s_defaultDialogScroll;
        
        [Tooltip("The graphic that will advance the dialog when clicked")]
        [SerializeField]
        protected Graphic m_ClickTargetGraphic;

        [Tooltip("The ui that will be used to display say dialog lines")]
        [SerializeField]
        [CheckNotNull]
        protected SayLineUI m_SayLineUI;

        [Tooltip("The ui that will be used to display choice dialog lines")]
        [SerializeField]
        [CheckNotNull]
        protected ChoiceLineUI m_ChoiceLineUI;

        [Tooltip("The timer that is used to set a limit on how long the player has to select a choice")]
        [SerializeField]
        [CheckNotNull]
        protected Timer m_ChoiceTimer;

        [Tooltip("The button that is used to cancel all choices")]
        [SerializeField]
        protected Button m_CancelChoiceButton;

        [Tooltip("If true, select the first choice by default")]
        [SerializeField]
        protected bool m_SelectFirstChoiceByDefault = true;

        [Tooltip("The delay between the animation of each choice")]
        [Min(0)]
        [SerializeField]
        protected float m_ChoiceTransitionOffset;
        
        protected UnityEvent m_onChoiceFinished = new UnityEvent();
        
        protected UnityEvent m_onNextChoiceFinished = new UnityEvent();
        
        protected ChoiceTimerState m_choiceTimerState;
        
        protected int m_currentActiveLineIndex;

        protected List<DialogLineState> m_activeDialogLineStates = new List<DialogLineState>();
        
        private List<DialogLineUI> m_availableDialogLines = new List<DialogLineUI>();

        private Tween m_onChoicesHiddenTween;

        public static DialogScroll DefaultDialogScroll
        {
            get
            {
                if (s_defaultDialogScroll == null)
                {
                    s_defaultDialogScroll = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<DialogScroll>("~DialogScroll"));
                }
                return s_defaultDialogScroll;
            }
        }

        public Graphic ClickTargetGraphic
        {
            get
            {
                return m_ClickTargetGraphic;
            }
            set
            {
                m_ClickTargetGraphic = value;
            }
        }

        public SayLineUI SayLineUI
        {
            get
            {
                return m_SayLineUI;
            }
            set
            {
                m_SayLineUI = value;
            }
        }

        public ChoiceLineUI ChoiceLineUI
        {
            get
            {
                return m_ChoiceLineUI;
            }
            set
            {
                m_ChoiceLineUI = value;
            }
        }

        public Timer ChoiceTimerUI
        {
            get
            {
                return m_ChoiceTimer;
            }
            set
            {
                m_ChoiceTimer = value;
            }
        }

        public Button CancelChoiceButton
        {
            get
            {
                return m_CancelChoiceButton;
            }
            set
            {
                m_CancelChoiceButton = value;
            }
        }

        public bool SelectFirstChoiceByDefault
        {
            get
            {
                return m_SelectFirstChoiceByDefault;
            }
            set
            {
                m_SelectFirstChoiceByDefault = value;
            }
        }

        public float ChoiceTransitionOffset
        {
            get
            {
                return m_ChoiceTransitionOffset;
            }
            set
            {
                m_ChoiceTransitionOffset = value;
            }
        }
        
        public UnityEvent OnChoiceFinished
        {
            get
            {
                return m_onChoiceFinished;
            }
        }

        public UnityEvent OnNextChoiceFinished
        {
            get
            {
                return m_onNextChoiceFinished;
            }
        }

        public SayLineState[] ActiveSayLineStates
        {
            get
            {
                return m_activeDialogLineStates.OfType<SayLineState>().ToArray();
            }
        }

        public ChoiceLineState[] ActiveChoiceLineStates
        {
            get
            {
                return m_activeDialogLineStates.OfType<ChoiceLineState>().ToArray();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_ClickTargetGraphic != null)
            {
                PointerClickDetector pointerClickDetector = m_ClickTargetGraphic.gameObject.AddComponent<PointerClickDetector>();
                pointerClickDetector.OnInputDetected.AddListener(OnTargetGraphicClicked);
            }

            if (m_choiceTimerState == null)
            {
                if (m_ChoiceTimer != null)
                {
                    m_ChoiceTimer.gameObject.SetActive(false);
                }
            }

            if (m_CancelChoiceButton != null)
            {
                m_CancelChoiceButton.onClick.AddListener(CancelChoices);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_ClickTargetGraphic != null)
            {
                PointerClickDetector pointerClickDetector = m_ClickTargetGraphic.gameObject.GetComponent<PointerClickDetector>();
                if (pointerClickDetector != null)
                {
                    pointerClickDetector.OnInputDetected.RemoveListener(OnTargetGraphicClicked);
                }
            }

            if (m_CancelChoiceButton != null)
            {
                m_CancelChoiceButton.onClick.RemoveListener(CancelChoices);
            }

            if (m_choiceTimerState == null)
            {
                m_ChoiceTimer.gameObject.SetActive(false);
            }
            
            if (m_onChoicesHiddenTween != null && m_onChoicesHiddenTween.IsPlaying())
            {
                m_onChoicesHiddenTween.Complete();
            }
        }

        protected virtual void OnTargetGraphicClicked()
        {
            if (!DialogManager.Instance.IgnoreInput)
            {
                m_SayLineUI.AdvanceDialog();
            }
        }

        public void DisplaySay(SayLineState sayState, UnityAction onComplete = null)
        {
            sayState.ActiveDialogLineIndex = m_activeDialogLineStates.Count;

            m_activeDialogLineStates.Add(sayState);

            UnityAction onWritten = 
                () =>
                {
                    UpdateCurrentActiveLine();

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                };

            m_SayLineUI.SetupDialogLine(sayState);
            Show(
                () =>
                {
                    CreateAndShowLine(sayState, 0f, onWritten);

                    SetupChoices();
                });
        }

        public void DisplayChoice(ChoiceLineState choiceState)
        {
            choiceState.ActiveDialogLineIndex = m_activeDialogLineStates.Count;

            m_activeDialogLineStates.Add(choiceState);

            m_ChoiceLineUI.SetupDialogLine(choiceState);

            Show(
                () =>
                {
                    CreateAndShowLine(choiceState, m_ChoiceTransitionOffset);

                    SetupChoices();
                });
        }

        public void DisplayChoiceTimer(float duration, int targetActiveDialogLineIndex, UnityAction onTimerFinished)
        {
            m_choiceTimerState = new ChoiceTimerState(duration, targetActiveDialogLineIndex, onTimerFinished);
            
            Show();

            if (m_ChoiceTimer != null)
            {
                m_ChoiceTimer.RaiseCounter(duration, true, null, OnTimeout);
            }
        }

        public void CancelChoices()
        {
            CancelChoices(null);
        }

        public void CancelChoices(UnityAction onComplete)
        {
            FinishChoice(null, onComplete);
        }

        public UnityEvent GetOnNextChoiceFinishedEvent()
        {
            return m_onNextChoiceFinished;
        }

        protected void SetupChoices()
        {
            StartCoroutine(PerformActionAtEndOfFrame(SetupInitialSelection));
        }
        
        protected void SetupInitialSelection()
        {
            if (m_SelectFirstChoiceByDefault)
            {
                int firstChoiceIndex = GetFirstActiveChoiceIndex();
                if (firstChoiceIndex >= 0 && firstChoiceIndex < m_availableDialogLines.Count)
                {
                    if (EventSystem.current != null)
                    {
                        EventSystem.current.SetSelectedGameObject((m_availableDialogLines[firstChoiceIndex] as ChoiceLineUI).Button.gameObject);
                    }
                }
            }
        }

        public virtual void FinishChoice(ChoiceLineUI choiceLineUI, UnityAction onComplete = null)
        {
            UpdateCurrentActiveLine();

            RemoveListenersFromAllChoices();

            if (!gameObject.activeInHierarchy)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }
            
            if (choiceLineUI != null)
            {
                choiceLineUI.Finish(() => HideUnselectedChoices(choiceLineUI, onComplete));
            }
            else
            {
                HideUnselectedChoices(choiceLineUI, onComplete);
            }
        }

        protected void HideUnselectedChoices(ChoiceLineUI selectedChoice, UnityAction onComplete)
        {
            float transitionOffset = 0;
            for (int i = m_currentActiveLineIndex; i < m_availableDialogLines.Count; i++)
            {
                DialogLineUI dialogLineUI = m_availableDialogLines[i];
                ChoiceLineUI choiceLineUI = dialogLineUI as ChoiceLineUI;
                if (choiceLineUI != null)
                {
                    if (choiceLineUI == selectedChoice)
                    {
                        selectedChoice.Button.interactable = false;
                    }
                    else
                    {
                        choiceLineUI.Hide(transitionOffset, false);
                        transitionOffset += m_ChoiceTransitionOffset;
                    }
                }
            }

            m_onChoicesHiddenTween = DOVirtual.DelayedCall(
                transitionOffset,
                () =>
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
        }

        public int GetFirstActiveChoiceIndex()
        {
            for (int i = m_currentActiveLineIndex; i < m_availableDialogLines.Count; i++)
            {
                ChoiceLineUI choice = m_availableDialogLines[i] as ChoiceLineUI;
                
                if (choice != null)
                {
                    return i;
                }
            }

            return -1;
        }

        protected void UpdateCurrentActiveLine()
        {
            m_currentActiveLineIndex = m_availableDialogLines.Count;
        }

        protected void RemoveListenersFromAllChoices()
        {
            for (int i = m_currentActiveLineIndex; i < m_availableDialogLines.Count; i++)
            {
                DialogLineUI dialogLineUI = m_availableDialogLines[i];
                ChoiceLineUI choiceLineUI = dialogLineUI as ChoiceLineUI;
                if (choiceLineUI != null)
                {
                    choiceLineUI.Button.onClick.RemoveAllPersistentAndNonPersistentListeners();
                }
            }
        }

        protected virtual void OnTimeout()
        {
            OnTimeout(m_choiceTimerState.TargetDialogLineIndex, m_choiceTimerState.Callback);
        }

        protected virtual void OnTimeout(int targetActiveDialogLineIndex, UnityAction onTimerFinished)
        {
            ChoiceLineUI selectedChoice = null;
            if (targetActiveDialogLineIndex >= 0)
            {
                ChoiceLineState choiceState = null;

                if (targetActiveDialogLineIndex < m_activeDialogLineStates.Count)
                {
                    choiceState = m_activeDialogLineStates[targetActiveDialogLineIndex] as ChoiceLineState;
                }

                if (choiceState != null)
                {
                    if (targetActiveDialogLineIndex < m_availableDialogLines.Count)
                    {
                        selectedChoice = m_availableDialogLines[targetActiveDialogLineIndex] as ChoiceLineUI;
                    }

                    choiceState.WasVisited = true;

                    InvokeOnDialog(choiceState);
                }
            }

            FinishChoice(selectedChoice, onTimerFinished);
        }
        
        public void InvokeChoiceFinishedEvent()
        {
            OnChoiceFinished.Invoke();
            OnNextChoiceFinished.Invoke();
            OnNextChoiceFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            m_SayLineUI.gameObject.SetActive(false);
            m_ChoiceLineUI.gameObject.SetActive(false);

            while (m_availableDialogLines.Count > 0)
            {
                DestroyLine(0);
            }

            for (int i = 0; i < m_activeDialogLineStates.Count; i++)
            {
                DialogLineState dialogLineState = m_activeDialogLineStates[i];
                if (i < m_currentActiveLineIndex)
                {
                    DialogLineUI dialogLine = CreateAndRestoreLine(dialogLineState);
                    ChoiceLineState choiceLineState = dialogLineState as ChoiceLineState;
                    if (choiceLineState != null)
                    {
                        ChoiceLineUI choiceLine = dialogLine as ChoiceLineUI;
                        if (choiceLine != null)
                        {
                            if (choiceLineState.WasVisited)
                            {
                                choiceLine.Button.interactable = false;
                            }
                            else
                            {
                                choiceLine.gameObject.SetActive(false);
                            }
                        }
                    }
                }
                else
                {
                    CreateAndShowLine(dialogLineState, 0f);
                }
            }

            if (m_choiceTimerState != null)
            {
                DisplayChoiceTimer(m_choiceTimerState.Duration, m_choiceTimerState.TargetDialogLineIndex, m_choiceTimerState.Callback);
            }
            else
            {
                m_ChoiceTimer.gameObject.SetActive(false);
            }
        }

        protected virtual void DestroyLine(int index)
        {
            Destroy(m_availableDialogLines[index].gameObject);
            m_availableDialogLines.RemoveAt(index);
        }

        protected virtual DialogLineUI CreateAndRestoreLine(DialogLineState lineState)
        {
            SayLineState sayState = lineState as SayLineState;
            ChoiceLineState choiceState = lineState as ChoiceLineState;

            if (sayState != null)
            {
                SayLineUI sayLine = CreateLine(m_SayLineUI, lineState);
                sayLine.RestoreDialogLine(sayState);
                return sayLine;
            }

            if (choiceState != null)
            {
                ChoiceLineUI choiceLine = CreateLine(m_ChoiceLineUI, lineState);
                choiceLine.RestoreDialogLine(choiceState);
                return CreateLine(m_ChoiceLineUI, lineState);
            }
            return null;
        }

        protected virtual DialogLineUI CreateAndShowLine(DialogLineState lineState, float choiceTransitionOffset, UnityAction onComplete = null)
        {
            SayLineState sayState = lineState as SayLineState;
            ChoiceLineState choiceState = lineState as ChoiceLineState;

            if (sayState != null)
            {
                UnityAction onDialogFinished = 
                    () =>
                    {
                        sayState.WasVisited = true;

                        InvokeOnDialog(sayState);

                        if (onComplete != null)
                        {
                            onComplete();
                        }
                    };

                SayLineUI sayLine = CreateLine(m_SayLineUI, lineState);
                sayLine.ShowDialogLine(sayState, onDialogFinished);
                return sayLine;
            }

            if (choiceState != null)
            {
                ChoiceLineUI choiceLine = CreateLine(m_ChoiceLineUI, lineState);

                UnityAction onDialogFinished = 
                    () =>
                    {
                        choiceState.WasVisited = true;

                        InvokeOnDialog(choiceState);
                    
                        FinishChoice(choiceLine, onComplete);
                    };

                float choiceTransitionDelay = 0;

                if (choiceTransitionOffset > 0)
                {
                    int firstChoiceIndex = GetFirstActiveChoiceIndex();
                    if (firstChoiceIndex < 0)
                    {
                        firstChoiceIndex = choiceState.ActiveDialogLineIndex;
                    }
                    int choiceIndex = choiceState.ActiveDialogLineIndex - firstChoiceIndex;
                    choiceTransitionDelay = choiceIndex * choiceTransitionOffset;
                }

                choiceLine.ShowDialogLine(choiceState, choiceTransitionDelay, onDialogFinished);
                if (onComplete != null)
                {
                    onComplete();
                }
                return CreateLine(m_ChoiceLineUI, lineState);
            }
            return null;
        }

        protected T CreateLine<T>(T lineTemplate, DialogLineState lineState) where T : DialogLineUI
        {
            return CreateLine(lineTemplate, lineState) as T;
        }

        protected DialogLineUI CreateLine(DialogLineUI lineTemplate, DialogLineState lineState)
        {
            lineTemplate.gameObject.SetActive(false);

            DialogLineUI line = null;

            if (lineState.ContinuePrevious)
            {
                line = GetPreviousDialogScrollLine(lineTemplate.GetType());
            }

            if (line == null)
            {
                line = InstantiateAndRegister(lineTemplate);
                line.transform.SetParent(lineTemplate.transform.parent, false);
                line.gameObject.name = "Line " + m_availableDialogLines.Count;
                m_availableDialogLines.Add(line);
            }

            if (line != null)
            {
                line.gameObject.SetActive(true);
            }

            return line;
        }
        
        protected DialogLineUI GetPreviousDialogScrollLine(Type type)
        {
            for (int i = m_availableDialogLines.Count - 1; i >= 0; i++)
            {
                DialogLineUI line = m_availableDialogLines[i] as DialogLineUI;
                if (type.IsAssignableFrom(line.GetType()))
                {
                    return line;
                }
            }
            return null;
        }

        public void CloseDialogScroll(bool clearAfterClosing, UnityAction onComplete)
        {
            UnityAction onClosed = 
                () =>
                {
                    if (clearAfterClosing)
                    {
                        ClearDialogScroll();
                    }

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                };
            Hide(onClosed);
        }

        public void ClearDialogScroll()
        {
            while (m_availableDialogLines.Count > 0)
            {
                DestroyLine(0);
            }

            m_activeDialogLineStates.Clear();
        }

        public bool CanDisplayName()
        {
            return (m_SayLineUI != null && m_SayLineUI.NameText != null);
        }

        public bool CanDisplayPortrait()
        {
            return (m_SayLineUI != null && m_SayLineUI.PortraitImage != null);
        }

        public bool CanPlayVoiceOver()
        {
            return (m_SayLineUI != null && m_SayLineUI.WriterAudio != null);
        }

        public bool CanDisplayIcon()
        {
            return (m_ChoiceLineUI != null && m_ChoiceLineUI.IconImage != null);
        }

        public DialogUI GetDialogUI()
        {
            return this;
        }

        public SayLineUI GetSayLineUI()
        {
            return m_SayLineUI;
        }

        public ChoiceLineUI GetChoiceLineUI()
        {
            return m_ChoiceLineUI;
        }

        public SayLineState[] GetActiveSayLineStates()
        {
            return ActiveSayLineStates;
        }
        
        public ChoiceLineState[] GetActiveChoiceLineStates()
        {
            return ActiveChoiceLineStates;
        }
    }

}