using ImprobableStudios.BaseUtilities;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using ImprobableStudios.UIUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;
using ImprobableStudios.UnityEventUtilities;

namespace ImprobableStudios.DialogSystem
{

    [AddComponentMenu("Dialog System/Dialog Menu")]
    public class DialogMenu : DialogUI, IChoiceUI
    {
        private static DialogMenu s_defaultDialogMenu;

        [Tooltip("The ui that will be used to display choice dialog lines")]
        [SerializeField]
        [FormerlySerializedAs("templateChoice")]
        [FormerlySerializedAs("m_TemplateChoice")]
        [FormerlySerializedAs("m_ChoiceTemplate")]
        [CheckNotNull]
        protected ChoiceLineUI m_ChoiceLineUI;

        [Tooltip("The timer that is used to set a limit on how long the player has to select an choice")]
        [SerializeField]
        [FormerlySerializedAs("m_Slider")]
        [FormerlySerializedAs("m_Timer")]
        protected Timer m_ChoiceTimer;

        [Tooltip("The button that is used to cancel all choices")]
        [SerializeField]
        protected Button m_CancelChoiceButton;

        [Tooltip("If true, select the first choice by default")]
        [SerializeField]
        [FormerlySerializedAs("selectFirstButtonByDefault")]
        [FormerlySerializedAs("m_SelectFirstButtonByDefault")]
        protected bool m_SelectFirstChoiceByDefault = true;

        [Tooltip("The delay between the animation of each choice")]
        [Min(0)]
        [SerializeField]
        [FormerlySerializedAs("transitionOffset")]
        [FormerlySerializedAs("m_TransitionOffset")]
        protected float m_ChoiceTransitionOffset;
        
        protected UnityEvent m_onChoiceFinished = new UnityEvent();
        
        protected UnityEvent m_onNextChoiceFinished = new UnityEvent();
        
        protected ChoiceTimerState m_choiceTimerState;
        
        protected List<ChoiceLineState> m_activeChoiceLineStates = new List<ChoiceLineState>();

        private List<ChoiceLineUI> m_availableChoiceLines = new List<ChoiceLineUI>();
        
        private Tween m_onChoicesHiddenTween;

        public static DialogMenu DefaultDialogMenu
        {
            get
            {
                if (s_defaultDialogMenu == null)
                {
                    s_defaultDialogMenu = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<DialogMenu>("~DialogMenu"));
                }
                return s_defaultDialogMenu;
            }
        }

        public ChoiceLineUI ChoiceLineUI
        {
            get
            {
                return m_ChoiceLineUI;
            }
            set
            {
                m_ChoiceLineUI = value;
            }
        }

        protected Timer ChoiceTimer
        {
            get
            {
                if (m_ChoiceTimer == null)
                {
                    m_ChoiceTimer = GetComponentInChildren<Timer>();
                }
                return m_ChoiceTimer;
            }
            set
            {
                m_ChoiceTimer = value;
            }
        }

        public Button CancelChoiceButton
        {
            get
            {
                return m_CancelChoiceButton;
            }
            set
            {
                m_CancelChoiceButton = value;
            }
        }

        public bool SelectFirstChoiceByDefault
        {
            get
            {
                return m_SelectFirstChoiceByDefault;
            }
            set
            {
                m_SelectFirstChoiceByDefault = value;
            }
        }

        public float ChoiceTransitionOffset
        {
            get
            {
                return m_ChoiceTransitionOffset;
            }
            set
            {
                m_ChoiceTransitionOffset = value;
            }
        }
        
        public UnityEvent OnChoiceFinished
        {
            get
            {
                return m_onChoiceFinished;
            }
        }

        public UnityEvent OnNextChoiceFinished
        {
            get
            {
                return m_onNextChoiceFinished;
            }
        }

        public ChoiceTimerState ChoiceTimerState
        {
            get
            {
                return m_choiceTimerState;
            }
        }

        public ChoiceLineState[] ActiveChoiceLineStates
        {
            get
            {
                return m_activeChoiceLineStates.ToArray();
            }
        }

        public ChoiceLineUI[] ActiveChoiceLines
        {
            get
            {
                return m_availableChoiceLines.ToArray();
            }
        }
        
        protected override void OnInstantiate()
        {
            base.OnInstantiate();

            if (Application.isPlaying)
            {
                if (m_ChoiceLineUI != null)
                {
                    m_ChoiceLineUI.gameObject.SetActive(false);
                }

                m_availableChoiceLines = GetComponentsInChildren<ChoiceLineUI>(true).ToList();
                if (m_ChoiceLineUI != null)
                {
                    m_availableChoiceLines.Remove(m_ChoiceLineUI);
                }
                foreach (ChoiceLineUI choice in m_availableChoiceLines)
                {
                    choice.gameObject.SetActive(false);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_choiceTimerState == null)
            {
                if (m_ChoiceTimer != null)
                {
                    m_ChoiceTimer.gameObject.SetActive(false);
                }
            }
            
            if (m_CancelChoiceButton != null)
            {
                m_CancelChoiceButton.onClick.AddListener(CancelChoices);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_CancelChoiceButton != null)
            {
                m_CancelChoiceButton.onClick.RemoveListener(CancelChoices);
            }

            if (m_onChoicesHiddenTween != null && m_onChoicesHiddenTween.IsPlaying())
            {
                m_onChoicesHiddenTween.Complete();
            }
        }

        public virtual void DisplayChoices(List<ChoiceLineState> choices)
        {
            for (int i = 0; i < choices.Count; i++)
            {
                ChoiceLineState choiceState = choices[i];
                choiceState.ActiveDialogLineIndex = i;
            }

            m_activeChoiceLineStates = choices;

            Show();

            foreach (ChoiceLineState choiceState in choices)
            {
                CreateChoice(choiceState);
            }

            SetupChoices();
        }

        public void DisplayChoice(ChoiceLineState choiceState)
        {
            choiceState.ActiveDialogLineIndex = m_activeChoiceLineStates.Count;

            m_activeChoiceLineStates.Add(choiceState);

            Show();

            CreateChoice(choiceState);

            SetupChoices();
        }
        
        public void DisplayChoiceTimer(float duration, int targetActiveDialogLineIndex, UnityAction onTimerFinished)
        {
            m_choiceTimerState = new ChoiceTimerState(duration, targetActiveDialogLineIndex, onTimerFinished);

            Show();

            if (m_ChoiceTimer != null)
            {
                m_ChoiceTimer.RaiseCounter(duration, true, null, OnTimeout);
            }
        }

        public void CancelChoices()
        {
            CancelChoices(null);
        }

        public void CancelChoices(UnityAction onComplete)
        {
            FinishChoice(null, onComplete);
        }

        public UnityEvent GetOnNextChoiceFinishedEvent()
        {
            return m_onNextChoiceFinished;
        }

        protected void SetupChoices()
        {
            StartCoroutine(PerformActionAtEndOfFrame(SetupInitialSelection));
        }
        
        protected void SetupInitialSelection()
        {
            if (m_SelectFirstChoiceByDefault)
            {
                if (m_availableChoiceLines.Count > 0)
                {
                    if (EventSystem.current != null)
                    {
                        EventSystem.current.SetSelectedGameObject(m_availableChoiceLines[0].Button.gameObject);
                    }
                }
            }
        }
        
        protected virtual void OnTimeout()
        {
            OnTimeout(m_choiceTimerState.TargetDialogLineIndex, m_choiceTimerState.Callback);
        }

        protected virtual void OnTimeout(int targetActiveDialogLineIndex, UnityAction onTimerFinished)
        {
            ChoiceLineUI selectedChoice = null;
            if (targetActiveDialogLineIndex >= 0)
            {
                ChoiceLineState choiceState = null;

                if (targetActiveDialogLineIndex < m_activeChoiceLineStates.Count)
                {
                    choiceState = m_activeChoiceLineStates[targetActiveDialogLineIndex] as ChoiceLineState;
                }

                if (choiceState != null)
                {
                    if (targetActiveDialogLineIndex < m_availableChoiceLines.Count)
                    {
                        selectedChoice = m_availableChoiceLines[targetActiveDialogLineIndex];
                    }

                    choiceState.WasVisited = true;

                    InvokeOnDialog(choiceState);
                }
            }

            FinishChoice(selectedChoice, onTimerFinished);
        }
        
        public void InvokeChoiceFinishedEvent()
        {
            OnChoiceFinished.Invoke();
            OnNextChoiceFinished.Invoke();
            OnNextChoiceFinished.RemoveAllPersistentAndNonPersistentListeners();
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            if (m_ChoiceLineUI != null)
            {
                m_ChoiceLineUI.gameObject.SetActive(false);
            }

            ClearDialogMenu();

            foreach (ChoiceLineState choiceLineState in m_activeChoiceLineStates)
            {
                CreateChoice(choiceLineState);
            }

            if (m_choiceTimerState != null)
            {
                DisplayChoiceTimer(m_choiceTimerState.Duration, m_choiceTimerState.TargetDialogLineIndex, m_choiceTimerState.Callback);
            }
            else
            {
                m_ChoiceTimer.gameObject.SetActive(false);
            }
        }

        public override void KillAllOtherActiveTransitions(string currentState, UnityAction onComplete = null)
        {
            if (currentState == nameof(UITransitionStateType.Show))
            {
                if (m_onChoicesHiddenTween != null && m_onChoicesHiddenTween.IsPlaying())
                {
                    m_onChoicesHiddenTween.Complete();
                }
            }

            for (int i = 0; i < m_availableChoiceLines.Count; i++)
            {
                ChoiceLineUI choiceUI = m_availableChoiceLines[i];

                if (choiceUI != null)
                {
                    choiceUI.KillAllOtherActiveTransitions(currentState);
                }
            }

            base.KillAllOtherActiveTransitions(currentState, onComplete);
        }

        public virtual void FinishChoice(ChoiceLineUI choiceLineUI, UnityAction onComplete = null)
        {
            DialogBox.StopAndHideActiveDialogBox();

            RemoveListenersFromAllChoices();

            if (!gameObject.activeInHierarchy)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            if (choiceLineUI != null)
            {
                choiceLineUI.Finish(() => Hide(onComplete));
            }
            else
            {
                Hide(onComplete);
            }
        }

        protected override void HideUI(bool instantly, bool disableOnHidden, UnityAction onKill, UnityAction onComplete)
        {
            HideChoices(instantly, () => base.HideUI(instantly, disableOnHidden, () => { ClearDialogMenu(); onKill?.Invoke(); }, () => { ClearDialogMenu(); onComplete?.Invoke(); }));
            
            if (m_ChoiceTimer != null)
            {
                m_choiceTimerState = null;
                m_ChoiceTimer.Hide();
            }
        }

        protected void HideChoices(bool instantly, UnityAction onComplete = null)
        {
            if (instantly)
            {
                for (int i = 0; i < m_availableChoiceLines.Count; i++)
                {
                    ChoiceLineUI choiceUI = m_availableChoiceLines[i];

                    if (choiceUI != null)
                    {
                        UnityAction onHidden = null;
                        if (i == m_availableChoiceLines.Count - 1)
                        {
                            onHidden = onComplete;
                        }
                        choiceUI.HideInstantly(false, onHidden);
                    }
                }
            }
            else
            {
                float transitionOffset = 0;
                for (int i = 0; i < m_availableChoiceLines.Count; i++)
                {
                    ChoiceLineUI choiceUI = m_availableChoiceLines[i];

                    if (choiceUI != null)
                    {
                        choiceUI.Hide(transitionOffset, false);
                        transitionOffset += m_ChoiceTransitionOffset;
                    }
                }

                m_onChoicesHiddenTween = DOVirtual.DelayedCall(
                    transitionOffset,
                    () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete.Invoke();
                        }
                    });
            }
        }

        public int GetFirstInactiveChoiceIndex()
        {
            for (int i = 0; i < m_availableChoiceLines.Count; i++)
            {
                ChoiceLineUI option = m_availableChoiceLines[i];
                if (option != null && !option.IsDoingShowTransitionOrFullyShown)
                {
                    return i;
                }
            }

            return -1;
        }

        protected virtual void DeactivateChoice(int index)
        {
            m_availableChoiceLines[index].gameObject.SetActive(false);
        }

        protected virtual void CreateChoice(ChoiceLineState choiceState)
        {
            if (m_ChoiceLineUI != null)
            {
                m_ChoiceLineUI.gameObject.SetActive(false);
            }

            ChoiceLineUI choiceUI = null;

            int choiceIndex = GetFirstInactiveChoiceIndex();
            if (choiceIndex > -1)
            {
                choiceUI = m_availableChoiceLines[choiceIndex];
            }

            if (choiceUI == null)
            {
                if (m_ChoiceLineUI != null)
                {
                    choiceUI = InstantiateAndRegister(m_ChoiceLineUI);
                    choiceUI.transform.SetParent(m_ChoiceLineUI.transform.parent, false);
                    choiceIndex = m_availableChoiceLines.Count;
                    choiceUI.gameObject.name = "Choice " + choiceIndex;
                    m_availableChoiceLines.Add(choiceUI);
                }
            }

            if (choiceUI != null)
            {
                UnityAction onDialogFinished = () => OnChoiceMade(choiceState, choiceUI);
                
                choiceUI.ShowDialogLine(choiceState, choiceIndex * m_ChoiceTransitionOffset, onDialogFinished);
                choiceUI.Button.enabled = true;
            }
        }

        protected virtual void OnChoiceMade(ChoiceLineState choiceState, ChoiceLineUI choiceLineUI)
        {
            choiceState.WasVisited = true;

            InvokeOnDialog(choiceState);
            
            FinishChoice(choiceLineUI, choiceState.Callback);
        }

        protected virtual void ClearDialogMenu()
        {
            for (int i = 0; i < m_availableChoiceLines.Count; i++)
            {
                DeactivateChoice(i);
            }

            m_activeChoiceLineStates.Clear();
        }

        protected void RemoveListenersFromAllChoices()
        {
            for (int i = 0; i < m_availableChoiceLines.Count; i++)
            {
                ChoiceLineUI choiceUI = m_availableChoiceLines[i];

                if (choiceUI != null)
                {
                    choiceUI.Button.enabled = false;
                    choiceUI.Button.onClick.RemoveAllPersistentAndNonPersistentListeners();
                }
            }
        }

        public bool CanDisplayIcon()
        {
            return (m_ChoiceLineUI != null && m_ChoiceLineUI.IconImage != null);
        }

        public DialogUI GetDialogUI()
        {
            return this;
        }

        public SayLineUI GetSayLineUI()
        {
            return null;
        }

        public ChoiceLineUI GetChoiceLineUI()
        {
            return m_ChoiceLineUI;
        }

        public ChoiceLineState[] GetActiveChoiceLineStates()
        {
            return ActiveChoiceLineStates;
        }
    }

}
