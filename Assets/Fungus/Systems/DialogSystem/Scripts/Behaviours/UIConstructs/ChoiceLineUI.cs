﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.DialogSystem
{

    [AddComponentMenu("Dialog System/Choice Line UI")]
    public class ChoiceLineUI : DialogLineUI<ChoiceLineState>
    {
        [Tooltip("The image used to display the choice icon")]
        [SerializeField]
        [FormerlySerializedAs("iconImage")]
        protected Image m_IconImage;

        [Tooltip("The toggle that is turned on when the choice has been previously chosen")]
        [SerializeField]
        [FormerlySerializedAs("selectedToggle")]
        [FormerlySerializedAs("m_SelectedToggle")]
        protected Toggle m_PreviouslyChosenToggle;

        [Tooltip("The button the player must click to choose this choice")]
        [SerializeField]
        [FormerlySerializedAs("button")]
        [CheckNotNull]
        protected Button m_Button;
        
        public Image IconImage
        {
            get
            {
                return m_IconImage;
            }
            set
            {
                m_IconImage = value;
            }
        }
        
        public Toggle PreviouslyChosenToggle
        {
            get
            {
                return m_PreviouslyChosenToggle;
            }
            set
            {
                m_PreviouslyChosenToggle = value;
            }
        }

        public Button Button
        {
            get
            {
                return m_Button;
            }
            set
            {
                m_Button = value;
            }
        }
        
        public override void SetupDialogLine(ChoiceLineState choiceState)
        {
            base.SetupDialogLine(choiceState);

            SetDialogText(choiceState.DialogLocalizedDictionary, choiceState.DialogText);
            SetIconImage(choiceState.Icon);
            SetPreviouslyChosenToggle(choiceState.WasVisited);
        }

        protected override void StartDialogLine(ChoiceLineState choiceState, UnityAction onChosen = null)
        {
            SetButton(onChosen);
        }

        protected override void RestoreDialogLine(ChoiceLineState sayState)
        {
        }

        private void SetIconImage(Sprite icon)
        {
            if (m_IconImage != null)
            {
                if (icon != null)
                {
                    m_IconImage.sprite = icon;
                    m_IconImage.gameObject.SetActive(true);
                }
                else
                {
                    m_IconImage.gameObject.SetActive(false);
                }
            }
        }

        private void SetPreviouslyChosenToggle(bool previouslyChosen)
        {
            if (m_PreviouslyChosenToggle != null)
            {
                m_PreviouslyChosenToggle.isOn = previouslyChosen;
            }
        }

        private void SetButton(UnityAction onClick)
        {
            m_Button.onClick.AddListener(onClick);
        }
    }

}
