﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.CharacterSystem;
using ImprobableStudios.TextStylePaletteUtilities;

namespace ImprobableStudios.DialogSystem
{

    [AddComponentMenu("Dialog System/Say Line UI")]
    [RequireComponent(typeof(Writer))]
    public class SayLineUI : DialogLineUI<SayLineState>
    {
        [Tooltip("The text ui used to display the speaking character's name")]
        [SerializeField]
        [FormerlySerializedAs("characterNameText")]
        [FormerlySerializedAs("m_CharacterNameText")]
        protected Text m_NameText;
        
        [Tooltip("The image used to display a character's portrait")]
        [SerializeField]
        [FormerlySerializedAs("image")]
        [FormerlySerializedAs("m_Image")]
        protected Image m_PortraitImage;

        [Tooltip("The graphics that will be displayed in the speaking character's display color")]
        [SerializeField]
        protected Graphic[] m_CharacterColoredGraphics = new Graphic[0];

        [Tooltip("The text that will be displayed in the speaking character's text style")]
        [SerializeField]
        protected Text[] m_CharacterStyledTexts = new Text[0];

        protected Dictionary<Text, FontData> m_cachedDefaultTextStyles;

        private Writer m_writer;

        private WriterAudio m_writerAudio;

        protected Coroutine m_writeCoroutine;

        public Text NameText
        {
            get
            {
                return m_NameText;
            }
            set
            {
                m_NameText = value;
            }
        }

        public Image PortraitImage
        {
            get
            {
                return m_PortraitImage;
            }
            set
            {
                m_PortraitImage = value;
            }
        }

        public Graphic[] CharacterColoredGraphics
        {
            get
            {
                return m_CharacterColoredGraphics;
            }
            set
            {
                m_CharacterColoredGraphics = value;
            }
        }

        public Text[] CharacterStyledTexts
        {
            get
            {
                return m_CharacterStyledTexts;
            }
            set
            {
                m_CharacterStyledTexts = value;
            }
        }

        public Writer Writer
        {
            get
            {
                if (m_writer == null)
                {
                    m_writer = gameObject.GetRequiredComponent<Writer>();
                }
                return m_writer;
            }
        }

        public WriterAudio WriterAudio
        {
            get
            {
                if (m_writerAudio == null)
                {
                    m_writerAudio = gameObject.GetComponent<WriterAudio>();
                }
                return m_writerAudio;
            }
        }

        public SayLineUI()
        {
            if (m_cachedDefaultTextStyles == null)
            {
                m_cachedDefaultTextStyles = new Dictionary<Text, FontData>();
                foreach (Text text in m_CharacterStyledTexts)
                {
                    if (text != null)
                    {
                        m_cachedDefaultTextStyles[text] = text.GetFontData();
                    }
                }
            }
        }

        public override void SetupDialogLine(SayLineState sayState)
        {
            base.SetupDialogLine(sayState);

            Character character = sayState.Character;
            Dictionary<string, string> nameLocalizedDictionary = sayState.NameLocalizedDictionary;
            string nameText = sayState.NameText;
            Sprite portrait = sayState.Portrait;

            // Character's name takes precedence over name text if character provided
            if (character != null)
            {
                nameText = character.DisplayName;
                nameLocalizedDictionary = character.SafeGetLocalizedDictionary<string>("m_" + nameof(character.DisplayName));
            }

            SetCharacter(character);
            SetPortrait(portrait);
            SetNameText(nameLocalizedDictionary, nameText);
        }

        protected override void StartDialogLine(SayLineState sayState, UnityAction onComplete = null)
        {
            Dictionary<string, string> dialogLocalizedDictionary = sayState.DialogLocalizedDictionary;
            string dialogText = sayState.DialogText;
            Character character = sayState.Character;
            Dictionary<string, AudioClip> voiceOverLocalizedDictionary = sayState.VoiceOverLocalizedDictionary;
            AudioClip voiceOver = sayState.VoiceOver;
            bool continuePrevious = sayState.ContinuePrevious;
            bool instantlyAutoAdvance = sayState.InstantlyAutoAdvance;

            // Voice over clip takes precedence over a character sound effect if provided
            if (voiceOver == null && character != null)
            {
                voiceOver = character.VoiceSoundEffect;
                voiceOverLocalizedDictionary = character.SafeGetLocalizedDictionary<AudioClip>("m_" + character.GetLocalizedMemberKey(x => x.VoiceSoundEffect));
            }

            WriteDialogText(dialogLocalizedDictionary, dialogText, continuePrevious, instantlyAutoAdvance, voiceOver, sayState.WasVisited, onComplete);
        }

        protected override void RestoreDialogLine(SayLineState sayState)
        {
            if (m_DialogText != null)
            {
                if (!sayState.ContinuePrevious)
                {
                    m_DialogText.text = "";
                }

                m_DialogText.text += Writer.GetFormattedText(sayState.DialogText);
            }
        }

        protected virtual void WriteDialogText(Dictionary<string, string> dialogLocalizedDictionary, string dialogText, bool continuePrevious, bool instantlyAutoAdvance, AudioClip voiceOver, bool wasPreviouslySeen, UnityAction onComplete)
        {
            Writer writer = Writer;

            if (writer.IsWriting())
            {
                writer.Finish();
            }

            if (!continuePrevious)
            {
                ClearDialogText();
            }

            float autoAdvanceDelay = -1f;
            if (instantlyAutoAdvance)
            {
                autoAdvanceDelay = 0;
            }

            if (DialogManager.Instance.IsSkipOn && (!DialogManager.Instance.OnlySkipSeenDialog || wasPreviouslySeen))
            {
                m_writeCoroutine = writer.WriteInstantly(dialogLocalizedDictionary, dialogText, continuePrevious, voiceOver, onComplete);
            }
            else
            {
                if (DialogManager.Instance.IsAutoOn)
                {
                    if (!instantlyAutoAdvance)
                    {
                        autoAdvanceDelay = DialogManager.Instance.AutoAdvanceDelay;
                    }
                }

                m_writeCoroutine = writer.WriteAndWaitForInput(dialogLocalizedDictionary, dialogText, continuePrevious, autoAdvanceDelay, voiceOver, onComplete);
            }
        }

        protected virtual void ClearDialogText()
        {
            if (m_DialogText != null)
            {
                m_DialogText.text = "";
            }
        }

        public void AdvanceDialog()
        {
            Writer.Advance();
        }

        public void AutoAdvanceDialogWhenFinished()
        {
            Writer.AutoAdvanceWhenFinished(DialogManager.Instance.AutoAdvanceDelay);
        }

        public void SkipDialog()
        {
            Writer.Skip();
        }

        public virtual void Exit()
        {
            Writer.Exit();
        }

        public virtual void Complete()
        {
            Writer.Complete();
        }

        public virtual void SetCharacter(Character character)
        {
            foreach (Graphic graphic in m_CharacterColoredGraphics)
            {
                if (character != null && graphic != null)
                {
                    graphic.canvasRenderer.SetColor(character.Color);
                }
                else
                {
                    graphic.canvasRenderer.SetColor(Color.white);
                }
            }

            foreach (Text text in m_CharacterStyledTexts)
            {
                if (character != null && text != null && character.TextStyle != null)
                {
                    text.SetFontData(character.TextStyle.FontData);
                }
                else
                {
                    if (m_cachedDefaultTextStyles.ContainsKey(text))
                    {
                        text.SetFontData(m_cachedDefaultTextStyles[text]);
                    }
                }
            }

            if (character != null)
            {
                character.Speak();
            }
        }

        public virtual void SetPortrait(Sprite sprite)
        {
            if (m_PortraitImage == null)
            {
                return;
            }

            if (sprite == null)
            {
                m_PortraitImage.gameObject.SetActive(false);
            }
            else
            {
                m_PortraitImage.sprite = sprite;
                m_PortraitImage.gameObject.SetActive(true);
            }
        }

        protected virtual void SetNameText(Dictionary<string, string> localizedDictionary, string nameText)
        {
            if (m_NameText != null)
            {
                m_NameText.RegisterLocalizableText(localizedDictionary);
            }
            SetNameText(nameText);
        }

        protected virtual void SetNameText(string nameText)
        {
            if (m_NameText != null)
            {
                if (!string.IsNullOrEmpty(nameText))
                {
                    m_NameText.text = nameText;
                    m_NameText.gameObject.SetActive(true);
                }
                else
                {
                    m_NameText.gameObject.SetActive(false);
                }
            }
        }
    }
}