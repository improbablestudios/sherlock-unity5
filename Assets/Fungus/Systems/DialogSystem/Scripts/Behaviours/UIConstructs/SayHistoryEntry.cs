﻿using ImprobableStudios.CharacterSystem;
using ImprobableStudios.ReflectionUtilities;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.TextStylePaletteUtilities;

namespace ImprobableStudios.DialogSystem
{
    
    [AddComponentMenu("Dialog System/History/Say History Entry")]
    public class SayHistoryEntry : DialogHistoryEntry<SayLineState>
    {
        [Tooltip("The text ui used to display a character name")]
        [SerializeField]
        protected Text m_NameText;
        
        [Tooltip("The image ui used to display a character's portrait")]
        [SerializeField]
        protected Image m_PortraitImage;

        [Tooltip("The graphics that will be displayed in the speaking character's display color")]
        [SerializeField]
        protected Graphic[] m_CharacterColoredGraphics = new Graphic[0];

        [Tooltip("The text that will be displayed in the speaking character's dialog text style")]
        [SerializeField]
        protected Text[] m_CharacterStyledTexts = new Text[0];

        [Tooltip("When this button is clicked, the voiceover for this entry will play")]
        [SerializeField]
        protected Button m_PlayButton;
        
        protected Dictionary<Text, FontData> m_cachedDefaultTextStyles;

        public Text NameText
        {
            get
            {
                return m_NameText;
            }
            set
            {
                m_NameText = value;
            }
        }

        public Image PortraitImage
        {
            get
            {
                return m_PortraitImage;
            }
            set
            {
                m_PortraitImage = value;
            }
        }

        public Graphic[] CharacterColoredGraphics
        {
            get
            {
                return m_CharacterColoredGraphics;
            }
            set
            {
                m_CharacterColoredGraphics = value;
            }
        }

        public Text[] CharacterStyledTexts
        {
            get
            {
                return m_CharacterStyledTexts;
            }
            set
            {
                m_CharacterStyledTexts = value;
            }
        }

        public Button PlayButton
        {
            get
            {
                return m_PlayButton;
            }
            set
            {
                m_PlayButton = value;
            }
        }
        
        public SayHistoryEntry()
        {
            if (m_cachedDefaultTextStyles == null)
            {
                m_cachedDefaultTextStyles = new Dictionary<Text, FontData>();
                foreach (Text text in m_CharacterStyledTexts)
                {
                    if (text != null)
                    {
                        m_cachedDefaultTextStyles[text] = text.GetFontData();
                    }
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_PlayButton != null)
            {
                m_PlayButton.onClick.AddListener(Play);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_PlayButton != null)
            {
                m_PlayButton.onClick.RemoveListener(Play);
            }
        }
        
        public virtual void Play()
        {
            Play(0);
        }

        protected override void SetupDialogLine(SayLineState sayState)
        {
            Character character = sayState.Character;
            string characterName = sayState.NameText;
            Sprite portrait = sayState.Portrait;

            SetCharacter(character);
            SetPortrait(portrait);
            SetNameText(characterName);
        }
        
        protected override void StartDialogLine(SayLineState sayState, UnityAction onComplete = null)
        {
            Writer writer = DialogHistory.Writer;
            ISayUI sayUI = m_dialogUI as ISayUI;

            if (sayUI != null)
            {
                Dictionary<string, object> serializedValueDictionary = sayUI.GetSayLineUI().Writer.GetUnitySerializedValueDictionary();
                writer.SetUnitySerializedValues(serializedValueDictionary);
                writer.TargetTextObject = m_DialogText.gameObject;

                WriteDialogText(sayState.DialogText, sayState.ContinuePrevious, sayState.VoiceOver, onComplete);
            }
        }
        
        public virtual void Play(int startingIndex)
        {
            if (startingIndex < m_dialogLineStates.Count)
            {
                SayLineState sayState = m_dialogLineStates[startingIndex];
                SetupDialogLine(sayState);
                StartDialogLine(sayState, () => Play(startingIndex + 1));
            }
        }

        protected void WriteDialogText(string text, bool continuePrevious, AudioClip voiceOver, UnityAction onComplete)
        {
            Writer writer = DialogHistory.Writer;

            if (writer.IsWriting())
            {
                writer.Finish();
            }

            if (continuePrevious)
            {
                ClearDialogText();
            }

            writer.Write(text, continuePrevious, voiceOver, onComplete);
        }

        protected virtual void SetCharacter(Character character)
        {
            foreach (Graphic graphic in m_CharacterColoredGraphics)
            {
                if (character != null && graphic != null)
                {
                    graphic.canvasRenderer.SetColor(character.Color);
                }
                else
                {
                    graphic.canvasRenderer.SetColor(Color.white);
                }
            }
            
            foreach (Text text in m_CharacterStyledTexts)
            {
                if (character != null && text != null && character.TextStyle != null)
                {
                    text.SetFontData(character.TextStyle.FontData);
                }
                else
                {
                    if (m_cachedDefaultTextStyles.ContainsKey(text))
                    {
                        text.SetFontData(m_cachedDefaultTextStyles[text]);
                    }
                }
            }
        }

        protected virtual void SetPortrait(Sprite portrait)
        {
            if (m_PortraitImage != null)
            {
                if (portrait == null)
                {
                    m_PortraitImage.gameObject.SetActive(false);
                }
                else
                {
                    m_PortraitImage.sprite = portrait;
                    m_PortraitImage.gameObject.SetActive(true);
                }
            }
        }

        protected virtual void ClearDialogText()
        {
            if (m_DialogText != null)
            {
                m_DialogText.text = "";
            }
        }

        protected virtual void SetNameText(string nameText)
        {
            if (m_NameText != null)
            {
                if (!string.IsNullOrEmpty(nameText))
                {
                    m_NameText.text = nameText;
                    m_NameText.gameObject.SetActive(true);
                }
                else
                {
                    m_NameText.gameObject.SetActive(false);
                }
            }
        }
    }
}