﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.Events;
using ImprobableStudios.Localization;
using ImprobableStudios.UIUtilities;

namespace ImprobableStudios.DialogSystem
{

    public abstract class DialogLineUI<T> : DialogLineUI where T : DialogLineState
    {
        public virtual void SetupDialogLine(T dialogLineState)
        {
            if (m_DialogText != null)
            {
                if (!dialogLineState.ContinuePrevious)
                {
                    m_DialogText.text = "";
                }
            }
        }

        protected abstract void StartDialogLine(T dialogLineState, UnityAction onComplete = null);

        protected abstract void RestoreDialogLine(T dialogLineState);

        public override sealed void ShowDialogLine(DialogLineState dialogLineState, float delay, UnityAction onComplete = null)
        {
            SetupDialogLine(dialogLineState as T);
            Show(delay, () => StartDialogLine(dialogLineState as T, onComplete));
        }

        public override sealed void RestoreDialogLine(DialogLineState dialogLineState)
        {
            SetupDialogLine(dialogLineState as T);
            RestoreDialogLine(dialogLineState as T);
        }

        public override Type GetDialogLineStateType()
        {
            return typeof(T);
        }
    }

    public abstract class DialogLineUI : UIConstruct
    {
        [FormerlySerializedAs("optionText")]
        [FormerlySerializedAs("m_OptionText")]
        public Text m_DialogText;

        protected DialogUI m_dialogUI;

        public Text DialogText
        {
            get
            {
                return m_DialogText;
            }
            set
            {
                m_DialogText = value;
            }
        }
        
        public DialogUI DialogUI
        {
            get
            {
                if (m_dialogUI == null)
                {
                    m_dialogUI = GetComponentInParent<DialogUI>();
                }
                return m_dialogUI;
            }
        }

        protected override void OnInstantiate()
        {
            base.OnInstantiate();

            if (Application.isPlaying)
            {
                SetDialogText("");
            }
        }

        public void ShowDialogLine(DialogLineState dialogLineState, UnityAction onComplete = null)
        {
            ShowDialogLine(dialogLineState, 0f, onComplete);
        }

        public abstract void ShowDialogLine(DialogLineState dialogLineState, float delay, UnityAction onComplete = null);

        public abstract void RestoreDialogLine(DialogLineState dialogLineState);

        public abstract Type GetDialogLineStateType();

        protected virtual void SetDialogText(Dictionary<string, string> dialogLocalizedDictionary, string dialogText)
        {
            if (m_DialogText != null)
            {
                m_DialogText.RegisterLocalizableText(dialogLocalizedDictionary);
            }
            SetDialogText(dialogText);
        }

        protected virtual void SetDialogText(string dialogText)
        {
            if (m_DialogText != null)
            {
                m_DialogText.text = dialogText;
            }
        }
    }

}