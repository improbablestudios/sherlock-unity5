﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ImprobableStudios.DialogSystem
{
    
    [AddComponentMenu("Dialog System/History/Choice History Entry")]
    public class ChoiceHistoryEntry : DialogHistoryEntry<ChoiceLineState>
    {
        [Tooltip("The image ui used to display the choice's icon")]
        [SerializeField]
        protected Image m_IconImage;
        
        public Image IconImage
        {
            get
            {
                return m_IconImage;
            }
            set
            {
                m_IconImage = value;
            }
        }
        
        protected override void SetupDialogLine(ChoiceLineState choiceState)
        {
            Sprite icon = choiceState.Icon;

            SetIcon(icon);
        }
        
        protected override void StartDialogLine(ChoiceLineState dialogLineState, UnityAction onComplete = null)
        {
        }

        protected virtual void SetIcon(Sprite icon)
        {
            if (m_IconImage != null)
            {
                if (icon == null)
                {
                    m_IconImage.gameObject.SetActive(false);
                }
                else
                {
                    m_IconImage.sprite = icon;
                    m_IconImage.gameObject.SetActive(true);
                }
            }
        }
    }
}