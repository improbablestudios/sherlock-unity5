using ImprobableStudios.BaseUtilities;
using ImprobableStudios.InputUtilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UIUtilities;

namespace ImprobableStudios.DialogSystem
{

    [AddComponentMenu("Dialog System/Dialog Box")]
    public class DialogBox : DialogUI, ISayUI
    {
        private static DialogBox s_defaultDialogBox;

        [Tooltip("The ui that will be used to display say dialog lines")]
        [SerializeField]
        protected SayLineUI m_SayLineUI;

        [Tooltip("The graphic that will advance the text when clicked")]
        [SerializeField]
        protected Graphic m_ClickTargetGraphic;

        [Tooltip("The image that will appear once the text has finished typing")]
        [SerializeField]
        [FormerlySerializedAs("continueButton")]
        protected Image m_ContinueImage;

        [Tooltip("The button that raises the dialog history")]
        [SerializeField]
        protected Button m_HistoryButton;

        [Tooltip("The toggle that when on causes the dialog to auto advance after finishing writing")]
        [SerializeField]
        protected Toggle m_AutoToggle;

        [Tooltip("The toggle that when on causes the dialog to instantly skip")]
        [SerializeField]
        protected Toggle m_SkipToggle;

        [Tooltip("If true, the dialog box will hide itself if it goes unsued for too long. Otherwise, it will always remain on screen")]
        [SerializeField]
        protected bool m_HideWhenUnused = true;

        [Tooltip("The amount of seconds this dialog box will remain visible and waiting for use after the player advances the dialog (If a say or choice command is not executed within this time, the dialog box will hide itself)")]
        [Min(0)]
        [SerializeField]
        protected float m_UnusedThreshold = 0.5f;

        protected List<SayLineState> m_activeSayLineStates = new List<SayLineState>();

        protected bool m_isBeingUsed;
        
        protected bool m_wasVisited;
        
        public static DialogBox DefaultDialogBox
        {
            get
            {
                if (s_defaultDialogBox == null)
                {
                    s_defaultDialogBox = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<DialogBox>("~DialogBox"));
                }
                return s_defaultDialogBox;
            }
        }

        public SayLineUI SayLineUI
        {
            get
            {
                return m_SayLineUI;
            }
            set
            {
                m_SayLineUI = value;
            }
        }

        public Graphic ClickTargetGraphic
        {
            get
            {
                return m_ClickTargetGraphic;
            }
            set
            {
                m_ClickTargetGraphic = value;
            }
        }

        public Image ContinueImage
        {
            get
            {
                return m_ContinueImage;
            }
            set
            {
                m_ContinueImage = value;
            }
        }

        public Button HistoryButton
        {
            get
            {
                return m_HistoryButton;
            }
            set
            {
                m_HistoryButton = value;
            }
        }

        public Toggle AutoToggle
        {
            get
            {
                return m_AutoToggle;
            }
            set
            {
                m_AutoToggle = value;
            }
        }

        public Toggle SkipToggle
        {
            get
            {
                return m_SkipToggle;
            }
            set
            {
                m_SkipToggle = value;
            }
        }

        public bool HideWhenUnused
        {
            get
            {
                return m_HideWhenUnused;
            }
            set
            {
                m_HideWhenUnused = value;
            }
        }

        public float UnusedThreshold
        {
            get
            {
                return m_UnusedThreshold;
            }
            set
            {
                m_UnusedThreshold = value;
            }
        }

        public SayLineState[] ActiveSayLineStates
        {
            get
            {
                return m_activeSayLineStates.ToArray();
            }
        }
        
        protected virtual void Start()
        {
            // Add a raycaster if none already exists so we can handle dialog input
            UICanvas.GetRequiredComponent<GraphicRaycaster>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_ClickTargetGraphic != null)
            {
                PointerClickDetector pointerClickDetector = m_ClickTargetGraphic.gameObject.AddComponent<PointerClickDetector>();
                pointerClickDetector.OnInputDetected.AddListener(OnTargetGraphicClicked);
            }

            if (m_HistoryButton != null)
            {
                m_HistoryButton.onClick.AddListener(OnHistoryButtonClicked);
            }

            if (m_AutoToggle != null)
            {
                m_AutoToggle.isOn = DialogManager.Instance.IsAutoOn;
                m_AutoToggle.onValueChanged.AddListener(OnAutoToggleValueChanged);
                DialogManager.Instance.OnAutoChanged.AddListener(OnAutoChanged);
            }

            if (m_SkipToggle != null)
            {
                m_SkipToggle.isOn = DialogManager.Instance.IsSkipOn;
                m_SkipToggle.onValueChanged.AddListener(OnSkipToggleValueChanged);
                DialogManager.Instance.OnSkipChanged.AddListener(OnSkipChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_ClickTargetGraphic != null)
            {
                PointerClickDetector pointerClickDetector = m_ClickTargetGraphic.gameObject.GetComponent<PointerClickDetector>();
                if (pointerClickDetector != null)
                {
                    pointerClickDetector.OnInputDetected.RemoveListener(OnTargetGraphicClicked);
                }
            }

            if (m_HistoryButton != null)
            {
                m_HistoryButton.onClick.RemoveListener(OnHistoryButtonClicked);
            }

            if (m_AutoToggle != null)
            {
                m_AutoToggle.onValueChanged.RemoveListener(OnAutoToggleValueChanged);
                DialogManager.Instance.OnAutoChanged.RemoveListener(OnAutoChanged);
            }

            if (m_SkipToggle != null)
            {
                m_SkipToggle.onValueChanged.RemoveListener(OnSkipToggleValueChanged);
                DialogManager.Instance.OnSkipChanged.RemoveListener(OnSkipChanged);
            }
        }
        
        protected override void Update()
        {
            base.Update();

            if (!DialogManager.Instance.IgnoreInput)
            {
                if (DialogManager.Instance.UseSubmitButtonToAdvanceDialog)
                {
                    if (EventSystem.current != null)
                    {
                        StandaloneInputModule standaloneInputModule = EventSystem.current.currentInputModule as StandaloneInputModule;
                        if (standaloneInputModule != null)
                        {
                            if (Input.GetButtonDown(standaloneInputModule.submitButton))
                            {
                                m_SayLineUI.AdvanceDialog();
                            }
                        }
                    }
                }

                if (DialogManager.Instance.UseSkipButtonToSkipDialog)
                {
                    if (EventSystem.current != null)
                    {
                        if (Input.GetButtonDown(DialogManager.Instance.SkipButton))
                        {
                            DialogManager.Instance.IsSkipOn = true;

                            if (!DialogManager.Instance.OnlySkipSeenDialog || m_wasVisited)
                            {
                                m_SayLineUI.SkipDialog();
                            }
                        }
                    }

                    if (Input.GetButtonUp(DialogManager.Instance.SkipButton))
                    {
                        DialogManager.Instance.IsSkipOn = false;
                    }
                }
            }
        }

        protected virtual void LateUpdate()
        {
            if (m_ContinueImage != null)
            {
                m_ContinueImage.gameObject.SetActive(m_SayLineUI.Writer.IsWaitingForInput);
            }
        }

        protected virtual void OnTargetGraphicClicked()
        {
            if (!DialogManager.Instance.IgnoreInput)
            {
                m_SayLineUI.AdvanceDialog();
            }
        }

        protected virtual void OnHistoryButtonClicked()
        {
            if (m_DialogHistory != null)
            {
                m_DialogHistory.Show();
            }
        }

        protected virtual void OnAutoToggleValueChanged(bool value)
        {
            DialogManager.Instance.IsAutoOn = value;

            if (value)
            {
                m_SayLineUI.AutoAdvanceDialogWhenFinished();
            }
        }

        protected virtual void OnAutoChanged()
        {
            m_AutoToggle.isOn = DialogManager.Instance.IsAutoOn;
        }

        protected virtual void OnSkipToggleValueChanged(bool value)
        {
            DialogManager.Instance.IsSkipOn = value;

            if (value)
            {
                if (!DialogManager.Instance.OnlySkipSeenDialog || m_wasVisited)
                {
                    m_SayLineUI.SkipDialog();
                }
            }
        }

        protected virtual void OnSkipChanged()
        {
            m_SkipToggle.isOn = DialogManager.Instance.IsSkipOn;
        }
        
        public virtual void DisplaySay(SayLineState sayState, UnityAction onComplete = null)
        {
            sayState.ActiveDialogLineIndex = m_activeSayLineStates.Count;

            m_activeSayLineStates.Add(sayState);

            m_isBeingUsed = true;
            m_wasVisited = sayState.WasVisited;

            UnityAction onFinished = 
                () =>
                {
                    sayState.WasVisited = true;
                
                    if (!sayState.ContinuePrevious)
                    {
                        m_activeSayLineStates.Clear();
                    }

                    InvokeOnDialog(sayState);

                    HideConditionally(onComplete);
                };

            if (m_SkipToggle != null)
            {
                m_SkipToggle.interactable = (!DialogManager.Instance.OnlySkipSeenDialog || m_wasVisited);
            }

            m_SayLineUI.SetupDialogLine(sayState);
            Show(() => m_SayLineUI.ShowDialogLine(sayState, onFinished));
        }
        
        public override void RebuildUI()
        {
            base.RebuildUI();

            foreach (SayLineState sayLineState in m_activeSayLineStates)
            {
                m_SayLineUI.RestoreDialogLine(sayLineState);
            }
        }

        public static void StopAndHideActiveDialogBox()
        {
            DialogBox activeDialogBox = GetPrimaryActive<DialogBox>();
            if (activeDialogBox != null)
            {
                activeDialogBox.m_SayLineUI.Exit();
                activeDialogBox.HideConditionally();
            }
        }
        
        public virtual void HideConditionally(UnityAction onComplete = null)
        {
            m_isBeingUsed = false;
            if (!m_HideWhenUnused)
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            }
            else if (m_HideWhenUnused && m_UnusedThreshold > 0)
            {
                if (onComplete != null)
                {
                    onComplete();
                }
                Invoke("HideAfterDelay", m_UnusedThreshold);
            }
            else if (m_HideWhenUnused && m_UnusedThreshold == 0)
            {
                Hide(onComplete);
            }
        }
        
        private void HideAfterDelay()
        {
            DialogMenu activeDialogMenu = GetPrimaryActive<DialogMenu>();
            if (!IsDoingShowTransition &&
                !m_isBeingUsed &&
                !(activeDialogMenu != null && activeDialogMenu.isActiveAndEnabled))
            {
                Hide();
            }
        }

        public bool CanDisplayName()
        {
            return (m_SayLineUI != null && m_SayLineUI.NameText != null);
        }

        public bool CanDisplayPortrait()
        {
            return (m_SayLineUI != null && m_SayLineUI.PortraitImage != null);
        }

        public bool CanPlayVoiceOver()
        {
            return (m_SayLineUI != null && m_SayLineUI.WriterAudio != null);
        }

        public DialogUI GetDialogUI()
        {
            return this;
        }

        public SayLineUI GetSayLineUI()
        {
            return m_SayLineUI;
        }

        public ChoiceLineUI GetChoiceLineUI()
        {
            return null;
        }

        public SayLineState[] GetActiveSayLineStates()
        {
            return ActiveSayLineStates;
        }
    }

}
