﻿using UnityEngine;
using UnityEngine.Events;
using ImprobableStudios.UIUtilities;

namespace ImprobableStudios.DialogSystem
{
    public class DialogEvent : UnityEvent<DialogUI, DialogLineState>
    {
    }

    public abstract class DialogUI : UIConstruct
    {
        [Tooltip("Determines which dialog history (if any) that the dialog displayed in this ui should be recorded to")]
        [SerializeField]
        protected DialogHistory m_DialogHistory;
        
        private static DialogEvent m_onDialog = new DialogEvent();

        public DialogHistory DialogHistory
        {
            get
            {
                return m_DialogHistory;
            }
            set
            {
                m_DialogHistory = value;
            }
        }
        
        public static DialogEvent OnDialog
        {
            get
            {
                return m_onDialog;
            }
        }
        
        protected void InvokeOnDialog(DialogLineState dialogLineState)
        {
            if (DialogManager.Instance.RecordHistory)
            {
                if (m_DialogHistory != null)
                {
                    m_DialogHistory.RecordDialog(this, dialogLineState);
                }
            }
            OnDialog.Invoke(this, dialogLineState);
        }

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return true;
        }
    }

}
