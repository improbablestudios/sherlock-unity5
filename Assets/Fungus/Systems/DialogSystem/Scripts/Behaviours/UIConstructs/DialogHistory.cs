﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UIUtilities;

namespace ImprobableStudios.DialogSystem
{

    [AddComponentMenu("Dialog System/History/Dialog History")]
    [RequireComponent(typeof(Writer))]
    public class DialogHistory : UIConstruct
    {
        public static DialogHistory s_defaultDialogHistory;
        
        [Tooltip("The template that will be used to create say entries")]
        [SerializeField]
        [CheckNotNull]
        protected SayHistoryEntry m_SayEntryTemplate;

        [Tooltip("The template that will be used to create choice entries")]
        [SerializeField]
        [CheckNotNull]
        protected ChoiceHistoryEntry m_ChoiceEntryTemplate;

        [Tooltip("The button used to close the dialog history")]
        [SerializeField]
        protected Button m_CloseButton;

        [Tooltip("The maximum number of entries that will be saved in the dialog history")]
        [Min(0)]
        [SerializeField]
        protected int m_MaximumNumberOfEntries;
        
        protected List<KeyValuePair<DialogUI, DialogLineState>> m_entryStates = new List<KeyValuePair<DialogUI, DialogLineState>>();

        protected List<DialogHistoryEntry> m_entries = new List<DialogHistoryEntry>();

        private Writer m_writer;
        
        public static DialogHistory DefaultDialogHistory
        {
            get
            {
                if (s_defaultDialogHistory == null)
                {
                    s_defaultDialogHistory = PersistableLoaderExtensions.LoadPersistentInstance(Resources.Load<DialogHistory>("~DialogHistory"));
                }
                return s_defaultDialogHistory;
            }
        }

        public SayHistoryEntry SayEntryTemplate
        {
            get
            {
                return m_SayEntryTemplate;
            }
            set
            {
                m_SayEntryTemplate = value;
                RebuildUI();
            }
        }

        public ChoiceHistoryEntry ChoiceEntryTemplate
        {
            get
            {
                return m_ChoiceEntryTemplate;
            }
            set
            {
                m_ChoiceEntryTemplate = value;
                RebuildUI();
            }
        }

        public Button CloseButton
        {
            get
            {
                return m_CloseButton;
            }
            set
            {
                m_CloseButton = value;
            }
        }

        public int MaximumNumberOfEntries
        {
            get
            {
                return m_MaximumNumberOfEntries;
            }
            set
            {
                m_MaximumNumberOfEntries = value;
            }
        }

        public Writer Writer
        {
            get
            {
                if (m_writer == null)
                {
                    m_writer = gameObject.GetRequiredComponent<Writer>();
                }
                return m_writer;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_CloseButton != null)
            {
                m_CloseButton.onClick.AddListener(Hide);
            }

            RebuildUI();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_CloseButton != null)
            {
                m_CloseButton.onClick.RemoveListener(Hide);
            }
        }

        public void RecordDialog(DialogUI dialogUI, DialogLineState dialogLineState)
        {
            if (!IsEntryTemplateValid(typeof(SayHistoryEntry), m_SayEntryTemplate))
            {
                return;
            }

            if (!IsEntryTemplateValid(typeof(ChoiceHistoryEntry), m_ChoiceEntryTemplate))
            {
                return;
            }

            if (m_MaximumNumberOfEntries >= 0)
            {
                while (m_entryStates.Count > 0 && m_entryStates.Count >= m_MaximumNumberOfEntries)
                {
                    m_entryStates.RemoveAt(0);
                }
            }

            if (m_MaximumNumberOfEntries < 0 || m_MaximumNumberOfEntries > 0)
            {
                KeyValuePair<DialogUI, DialogLineState> info = new KeyValuePair<DialogUI, DialogLineState>(dialogUI, dialogLineState);
                m_entryStates.Add(info);
            }
        }

        public void ClearHistory()
        {
            while (m_entries.Count > 0)
            {
                DestroyEntry(0);
            }

            m_entryStates.Clear();
        }

        public override void RebuildUI()
        {
            base.RebuildUI();

            if (!IsEntryTemplateValid(typeof(SayHistoryEntry), m_SayEntryTemplate))
            {
                return;
            }

            if (!IsEntryTemplateValid(typeof(ChoiceHistoryEntry), m_ChoiceEntryTemplate))
            {
                return;
            }

            m_SayEntryTemplate.gameObject.SetActive(false);
            m_ChoiceEntryTemplate.gameObject.SetActive(false);

            while (m_entries.Count > 0)
            {
                DestroyEntry(0);
            }

            foreach (KeyValuePair<DialogUI, DialogLineState> info in m_entryStates)
            {
                CreateEntry(info.Key, info.Value);
            }
        }

        protected virtual void DestroyEntry(int index)
        {
            Destroy(m_entries[index].gameObject);
            m_entries.RemoveAt(index);
        }

        protected virtual void CreateEntry(DialogUI dialogUI, DialogLineState dialogLineState)
        {
            m_SayEntryTemplate.gameObject.SetActive(false);
            m_ChoiceEntryTemplate.gameObject.SetActive(false);

            DialogHistoryEntry entry = null;

            if (dialogLineState.ContinuePrevious)
            {
                entry = GetPreviousDialogHistoryEntry(dialogUI, dialogLineState);
            }

            if (entry == null)
            {
                DialogHistoryEntry entryTemplate = null;
                if (dialogLineState is SayLineState)
                {
                    entryTemplate = m_SayEntryTemplate;
                }
                else if (dialogLineState is ChoiceLineState)
                {
                    entryTemplate = m_ChoiceEntryTemplate;
                }
                entry = InstantiateAndRegister(entryTemplate);
                entry.transform.SetParent(entryTemplate.transform.parent, false);
                entry.gameObject.name = "Entry " + m_entries.Count;
                m_entries.Add(entry);
            }

            if (entry != null)
            {
                entry.gameObject.SetActive(true);

                entry.AddEntryInfo(dialogUI, dialogLineState);
            }
        }
        
        protected DialogHistoryEntry GetPreviousDialogHistoryEntry(DialogUI dialogUI, DialogLineState dialogLineState)
        {
            for (int i = m_entries.Count - 1; i >= 0; i++)
            {
                DialogHistoryEntry entry = m_entries[i];
                if (entry.DialogUI == dialogUI && entry.GetDialogLineStateType() == dialogLineState.GetType())
                {
                    return entry;
                }
            }
            return null;
        }
        
        protected virtual bool IsEntryTemplateValid(Type entryTemplateType, DialogHistoryEntry entryTemplate)
        {
            if (entryTemplate == null)
            {
                Debug.LogError("The entry template is not assigned. The template needs to be assigned and must have a " + entryTemplateType.Name + " component attached to represent the entry.", this);
                return false;
            }
            else
            {
                if (!(entryTemplate.transform is RectTransform))
                {
                    Debug.LogError("The entry template is not valid. The " + entryTemplateType.Name + " component must have a RectTransform attached.", entryTemplate);
                    return false;
                }
                else if (entryTemplate.DialogText == null)
                {
                    Debug.LogError("The entry template is not valid. The " + entryTemplateType.Name + " component must have a Dialog Text assigned.", entryTemplate);
                    return false;
                }
                
                return true;
            }
        }
    }

}
