﻿#if UNITY_EDITOR
using ImprobableStudios.ProjectSettingsUtilities;
using UnityEditor;
using UnityEditor.Build;

namespace ImprobableStudios.DialogSystem
{

    public class DialogManagerSettingsProvider : BaseAssetSettingsProvider<DialogManager>
    {
        public DialogManagerSettingsProvider() : base("Project/Managers/Dialog Manager")
        {
        }

        [SettingsProvider]
        static SettingsProvider CreateProjectSettingsProvider()
        {
            return new DialogManagerSettingsProvider();
        }
    }

    public class DialogManagerSettingsBuilder : BaseAssetSettingsBuilder<DialogManager>, IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
    }

}
#endif