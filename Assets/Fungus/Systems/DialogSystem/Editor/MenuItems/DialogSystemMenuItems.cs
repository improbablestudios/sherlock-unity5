#if UNITY_EDITOR
using UnityEditor;
using ImprobableStudios.BaseUtilities;

namespace ImprobableStudios.DialogSystem
{

    public class DialogSystemMenuItems
    {
        [MenuItem("Tools/Dialog System/Dialog Box", false, 10)]
        [MenuItem("GameObject/Dialog System/Dialog Box", false, 10)]
        static void CreateDialogBox()
        {
            PrefabSpawner.SpawnPrefab<DialogBox>("~DialogBox", true);
        }

        [MenuItem("Tools/Dialog System/Dialog Menu", false, 10)]
        [MenuItem("GameObject/Dialog System/Dialog Menu", false, 10)]
        static void CreateDialogMenu()
        {
            PrefabSpawner.SpawnPrefab<DialogMenu>("~DialogMenu", true);
        }

        [MenuItem("Tools/Dialog System/Dialog Scroll", false, 10)]
        [MenuItem("GameObject/Dialog System/Dialog Scroll", false, 10)]
        static void CreateDialogScroll()
        {
            PrefabSpawner.SpawnPrefab<DialogScroll>("~DialogScroll", true);
        }

        [MenuItem("Tools/Dialog System/Dialog History", false, 100)]
        [MenuItem("GameObject/Dialog System/Dialog History", false, 100)]
        static void CreateDialogHistory()
        {
            PrefabSpawner.SpawnPrefab<DialogHistory>("~DialogHistory", true);
        }
    }

}
#endif