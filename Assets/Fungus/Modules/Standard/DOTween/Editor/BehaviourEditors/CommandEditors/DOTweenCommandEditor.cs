#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using Fungus.Commands;
using Fungus.Variables;
using System.Linq;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SerializedPropertyUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(DOTweenCommand))]
    public abstract class DOTweenCommandEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.name == GetTargetPropertyName())
            {
                Rect fieldPosition = EditorGUI.PrefixLabel(position, label);
                base.DrawProperty(fieldPosition, property, GUIContent.none);
                Texture2D buttonIcon = EditorGUIUtility.FindTexture("Animation.Record");
                Rect buttonRect = new Rect(fieldPosition);
                buttonRect.size = new Vector2(buttonIcon.width, buttonIcon.height);
                buttonRect.x -= buttonRect.width + 2;
                if (GUI.Button(buttonRect, new GUIContent(buttonIcon, "Use current values of target component")))
                {
                    CopyButton();
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public abstract string GetTargetPropertyName();

        public abstract void CopyButton();
        
        public static void ConditionalVector3DataField(SerializedProperty vector3DataProp, SerializedProperty showXProp, SerializedProperty showYProp, SerializedProperty showZProp, GUIContent label)
        {
            Vector3Data vector3Data = vector3DataProp.GetPropertyValue() as Vector3Data;
            float height = EditorGUIUtility.singleLineHeight;
            if (!vector3Data.IsConstant)
            {
                height = EditorGUI.GetPropertyHeight(vector3DataProp, label, true);
                height += EditorGUIUtility.singleLineHeight;
            }
            ConditionalVector3DataField(EditorGUILayout.GetControlRect(GUILayout.Height(height)), vector3DataProp, showXProp, showYProp, showZProp, label);
        }

        public static float GetConditionalVector3DataFieldHeight(SerializedProperty vector3DataProp, GUIContent label)
        {
            Vector3Data vector3Data = vector3DataProp.GetPropertyValue() as Vector3Data;
            float height = EditorGUIUtility.singleLineHeight;
            if (!vector3Data.IsConstant)
            {
                height = EditorGUI.GetPropertyHeight(vector3DataProp, label, true);
                height += EditorGUIUtility.singleLineHeight;
            }
            return height;
        }

        public static void ConditionalVector3DataField(Rect position, SerializedProperty vector3DataProp, SerializedProperty showXProp, SerializedProperty showYProp, SerializedProperty showZProp, GUIContent label)
        {
            Vector3Data vector3Data = vector3DataProp.GetPropertyValue() as Vector3Data;
            Rect contentPosition = position;
            contentPosition.width -= FungusGUI.VariableDataTypeFieldWidth;
            Rect vector3Position = position;
            if (!vector3Data.IsConstant)
            {
                vector3Position.height -= EditorGUIUtility.singleLineHeight;
            }
            VariableEditor.VariableDataField(vector3Position, vector3DataProp, label, new Type[] { typeof(Vector3) }, null, () => ConditionalVector3Field(contentPosition, vector3DataProp.FindPropertyRelative(VariableData.GetDataValPropertyName()), showXProp, showYProp, showZProp, label));
            if (!vector3Data.IsConstant)
            {
                Rect xyzPosition = contentPosition;
                xyzPosition.y += vector3Position.height;
                xyzPosition.height = EditorGUIUtility.singleLineHeight;
                XYZField(xyzPosition, showXProp, showYProp, showZProp);
            }
        }

        public static void ConditionalVector3Field(Rect position, SerializedProperty property, SerializedProperty showXProp, SerializedProperty showYProp, SerializedProperty showZProp, GUIContent label)
        {
            EditorGUI.PrefixLabel(position, label);

            Rect pos = position;

            EditorGUIUtility.labelWidth = 0;
            float prefixLabelWidth = EditorGUIUtility.labelWidth;
            pos.x += prefixLabelWidth;
            EditorGUIUtility.labelWidth = 12;
            
            GUIContent xlabel = new GUIContent("X");
            pos.width = EditorStyles.toggle.CalcSize(xlabel).x + 2f;
            EditorGUI.PropertyField(pos, showXProp, xlabel, true);

            pos.x += pos.width;
            pos.width = (position.width - prefixLabelWidth) * (0.3333f) - 28;
            float x = property.vector3Value.x;
            if (showXProp.boolValue)
            {
                x = EditorGUI.FloatField(pos, GUIContent.none, x);
            }

            pos.x += pos.width;
            GUIContent ylabel = new GUIContent("Y");
            pos.width = EditorStyles.toggle.CalcSize(ylabel).x + 2f;
            EditorGUI.PropertyField(pos, showYProp, ylabel, true);

            pos.x += pos.width;
            pos.width = (position.width - prefixLabelWidth) * (0.3333f) - 28;
            float y = property.vector3Value.y;
            if (showYProp.boolValue)
            {
                y = EditorGUI.FloatField(pos, GUIContent.none, y);
            }

            pos.x += pos.width;
            GUIContent zlabel = new GUIContent("Z");
            pos.width = EditorStyles.toggle.CalcSize(zlabel).x + 2f;
            EditorGUI.PropertyField(pos, showZProp, zlabel, true);

            pos.x += pos.width;
            pos.width = (position.width - prefixLabelWidth) * (0.3333f) - 28;
            float z = property.vector3Value.z;
            if (showZProp.boolValue)
            {
                z = EditorGUI.FloatField(pos, GUIContent.none, z);
            }

            property.vector3Value = new Vector3(x, y, z);

            EditorGUIUtility.labelWidth = 0;
        }

        public static void XYZField(Rect position, SerializedProperty showXProp, SerializedProperty showYProp, SerializedProperty showZProp)
        {
            EditorGUIUtility.labelWidth = 0;

            Rect contentPosition = EditorGUI.PrefixLabel(position, new GUIContent(" "));

            float toggleWidth = contentPosition.width / 3f;
            
            EditorGUIUtility.labelWidth = 12;

            contentPosition.width = toggleWidth;
            EditorGUI.PropertyField(contentPosition, showXProp, new GUIContent("X"), true);

            contentPosition.x += contentPosition.width;
            contentPosition.width = toggleWidth;
            EditorGUI.PropertyField(contentPosition, showYProp, new GUIContent("Y"), true);

            contentPosition.x += contentPosition.width;
            contentPosition.width = toggleWidth;
            EditorGUI.PropertyField(contentPosition, showZProp, new GUIContent("Z"), true);
            
            EditorGUIUtility.labelWidth = 0;
        }

        public static void ConditionalVector2DataField(SerializedProperty vector2DataProp, SerializedProperty showXProp, SerializedProperty showYProp, GUIContent label)
        {
            Vector2Data vector2Data = vector2DataProp.GetPropertyValue() as Vector2Data;
            float height = EditorGUIUtility.singleLineHeight;
            if (!vector2Data.IsConstant)
            {
                height = EditorGUI.GetPropertyHeight(vector2DataProp, label, true);
                height += EditorGUIUtility.singleLineHeight;
            }
            ConditionalVector2DataField(EditorGUILayout.GetControlRect(GUILayout.Height(height)), vector2DataProp, showXProp, showYProp, label);
        }

        public static float GetConditionalVector2DataFieldHeight(SerializedProperty vector2DataProp, GUIContent label)
        {
            Vector2Data vector2Data = vector2DataProp.GetPropertyValue() as Vector2Data;
            float height = EditorGUIUtility.singleLineHeight;
            if (!vector2Data.IsConstant)
            {
                height = EditorGUI.GetPropertyHeight(vector2DataProp, label, true);
                height += EditorGUIUtility.singleLineHeight;
            }
            return height;
        }

        public static void ConditionalVector2DataField(Rect position, SerializedProperty vector2DataProp, SerializedProperty showXProp, SerializedProperty showYProp, GUIContent label)
        {
            Vector2Data vector2Data = vector2DataProp.GetPropertyValue() as Vector2Data;
            Rect contentPosition = position;
            contentPosition.width -= FungusGUI.VariableDataTypeFieldWidth;
            Rect vector2Position = position;
            if (!vector2Data.IsConstant)
            {
                vector2Position.height -= EditorGUIUtility.singleLineHeight;
            }
            VariableEditor.VariableDataField(vector2Position, vector2DataProp, label, new Type[] { typeof(Vector2) }, null, () => ConditionalVector2Field(position, vector2DataProp.FindPropertyRelative(VariableData.GetDataValPropertyName()), showXProp, showYProp, label));
            if (!vector2Data.IsConstant)
            {
                Rect xyPosition = contentPosition;
                xyPosition.y += vector2Position.height;
                xyPosition.height = EditorGUIUtility.singleLineHeight;
                XYField(xyPosition, showXProp, showYProp);
            }
        }

        public static void ConditionalVector2Field(Rect position, SerializedProperty property, SerializedProperty showXProp, SerializedProperty showYProp, GUIContent label)
        {
            EditorGUI.PrefixLabel(position, label);

            Rect pos = position;

            EditorGUIUtility.labelWidth = 0;
            float prefixLabelWidth = EditorGUIUtility.labelWidth;
            pos.x += prefixLabelWidth;
            EditorGUIUtility.labelWidth = 12;

            GUIContent xlabel = new GUIContent("X");
            pos.width = EditorStyles.toggle.CalcSize(xlabel).x + 2f;
            EditorGUI.PropertyField(pos, showXProp, xlabel, true);

            pos.x += pos.width;
            pos.width = (position.width - prefixLabelWidth) * (0.5f) - 28;
            float x = property.vector2Value.x;
            if (showXProp.boolValue)
            {
                x = EditorGUI.FloatField(pos, GUIContent.none, x);
            }

            pos.x += pos.width;

            GUIContent ylabel = new GUIContent("Y");
            pos.width = EditorStyles.toggle.CalcSize(ylabel).x + 2f;
            EditorGUI.PropertyField(pos, showYProp, ylabel, true);

            pos.x += pos.width;
            pos.width = (position.width - prefixLabelWidth) * (0.5f) - 28;
            float y = property.vector2Value.y;
            if (showYProp.boolValue)
            {
                y = EditorGUI.FloatField(pos, GUIContent.none, y);
            }

            property.vector2Value = new Vector2(x, y);

            EditorGUIUtility.labelWidth = 0;
        }

        public static void XYField(Rect position, SerializedProperty showXProp, SerializedProperty showYProp)
        {
            EditorGUIUtility.labelWidth = 0;

            Rect contentPosition = EditorGUI.PrefixLabel(position, new GUIContent(" "));

            float toggleWidth = contentPosition.width / 2f;

            EditorGUIUtility.labelWidth = 12;

            contentPosition.width = toggleWidth;
            EditorGUI.PropertyField(contentPosition, showXProp, new GUIContent("X"), true);

            contentPosition.x += contentPosition.width;
            contentPosition.width = toggleWidth;
            EditorGUI.PropertyField(contentPosition, showYProp, new GUIContent("Y"), true);

            EditorGUIUtility.labelWidth = 0;
        }
    }
}
#endif