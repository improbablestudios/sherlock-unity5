﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{
    [VariableInfo("DOTween")]
    [AddComponentMenu("Fungus/Variables/DOTween/Loop Type Variable")]
    public class LoopTypeVariable : Variable<LoopType>
    {
    }

    [Serializable]
    public class LoopTypeData : VariableData<LoopType>
    {
        public LoopTypeData()
        {
        }

        public LoopTypeData(LoopType v)
        {
            m_DataVal = v;
        }
    }

}