﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("List/DOTween")]
    [AddComponentMenu("Fungus/Variables/List/DOTween/Path Type List Variable")]
    public class PathTypeListVariable : Variable<List<PathType>>
    {
    }

    [Serializable]
    public class PathTypeListData : VariableData<List<PathType>>
    {
        public PathTypeListData()
        {
            m_DataVal = new List<PathType>();
        }

        public PathTypeListData(List<PathType> v)
        {
            m_DataVal = v;
        }
    }

}