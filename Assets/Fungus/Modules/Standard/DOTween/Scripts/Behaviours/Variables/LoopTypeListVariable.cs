﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("List/DOTween")]
    [AddComponentMenu("Fungus/Variables/List/DOTween/Loop Type List Variable")]
    public class LoopTypeListVariable : Variable<List<LoopType>>
    {
    }

    [Serializable]
    public class LoopTypeListData : VariableData<List<LoopType>>
    {
        public LoopTypeListData()
        {
            m_DataVal = new List<LoopType>();
        }

        public LoopTypeListData(List<LoopType> v)
        {
            m_DataVal = v;
        }
    }

}