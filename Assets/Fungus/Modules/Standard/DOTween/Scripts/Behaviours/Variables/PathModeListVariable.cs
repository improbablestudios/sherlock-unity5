﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("List/DOTween")]
    [AddComponentMenu("Fungus/Variables/List/DOTween/Path Mode List Variable")]
    public class PathModeListVariable : Variable<List<PathMode>>
    {
    }

    [Serializable]
    public class PathModeListData : VariableData<List<PathMode>>
    {
        public PathModeListData()
        {
            m_DataVal = new List<PathMode>();
        }

        public PathModeListData(List<PathMode> v)
        {
            m_DataVal = v;
        }
    }

}