﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Fungus.Variables
{
    [VariableInfo("DOTween")]
    [AddComponentMenu("Fungus/Variables/DOTween/Ease Variable")]
    public class EaseVariable : Variable<Ease>
    {
    }

    [Serializable]
    public class EaseData : VariableData<Ease>
    {
        public EaseData()
        {
        }

        public EaseData(Ease v)
        {
            m_DataVal = v;
        }
    }

}