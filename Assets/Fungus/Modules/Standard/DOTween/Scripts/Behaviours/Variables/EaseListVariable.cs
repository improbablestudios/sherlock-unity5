﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{
    [VariableInfo("List/DOTween")]
    [AddComponentMenu("Fungus/Variables/List/DOTween/Ease List Variable")]
    public class EaseListVariable : Variable<List<Ease>>
    {
    }

    [Serializable]
    public class EaseListData : VariableData<List<Ease>>
    {
        public EaseListData()
        {
            m_DataVal = new List<Ease>();
        }

        public EaseListData(List<Ease> v)
        {
            m_DataVal = v;
        }
    }

}