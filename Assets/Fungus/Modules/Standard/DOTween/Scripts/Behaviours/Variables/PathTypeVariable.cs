﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("DOTween")]
    [AddComponentMenu("Fungus/Variables/DOTween/Path Type Variable")]
    public class PathTypeVariable : Variable<PathType>
    {
    }

    [Serializable]
    public class PathTypeData : VariableData<PathType>
    {
        public PathTypeData()
        {
        }

        public PathTypeData(PathType v)
        {
            m_DataVal = v;
        }
    }

}