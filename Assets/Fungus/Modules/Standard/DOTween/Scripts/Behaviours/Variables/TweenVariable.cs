﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("DOTween")]
    [AddComponentMenu("Fungus/Variables/DOTween/Tween Variable")]
    public class TweenVariable : Variable<Tween>
    {
    }

    [Serializable]
    public class TweenData : VariableData<Tween>
    {
        public TweenData()
        {
        }

        public TweenData(Tween v)
        {
            m_DataVal = v;
        }
    }

}