﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [VariableInfo("List/DOTween")]
    [AddComponentMenu("Fungus/Variables/List/DOTween/Tween List Variable")]
    public class TweenListVariable : Variable<List<Tween>>
    {
    }

    [Serializable]
    public class TweenListData : VariableData<List<Tween>>
    {
        public TweenListData()
        {
            m_DataVal = new List<Tween>();
        }

        public TweenListData(List<Tween> v)
        {
            m_DataVal = v;
        }
    }

}