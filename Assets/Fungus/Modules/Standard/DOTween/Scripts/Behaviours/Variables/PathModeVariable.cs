﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{
    [VariableInfo("DOTween")]
    [AddComponentMenu("Fungus/Variables/DOTween/Path Mode Variable")]
    public class PathModeVariable : Variable<PathMode>
    {
    }

    [Serializable]
    public class PathModeData : VariableData<PathMode>
    {
        public PathModeData()
        {
        }

        public PathModeData(PathMode v)
        {
            m_DataVal = v;
        }
    }

}