﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Restart a tween.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Restart Tween")]
    public class RestartTween : DOTweenControl
    {
        [Tooltip("Includes the tween delay, otherwise skips it")]
        [FormerlySerializedAs("_includeDelay")]
        public BooleanData m_IncludeDelay = new BooleanData(true);

        public override void ControlAllTweens()
        {
            bool includeDelay = m_IncludeDelay.Value;
            DOTween.RestartAll(includeDelay);
        }

        public override void ControlTweensOnComponent(Component component)
        {
            bool includeDelay = m_IncludeDelay.Value;
            DOTween.Restart(component, includeDelay);
        }

        public override void ControlTween(Tween tween)
        {
            bool includeDelay = m_IncludeDelay.Value;
            tween.Restart(includeDelay);
        }
    }

}