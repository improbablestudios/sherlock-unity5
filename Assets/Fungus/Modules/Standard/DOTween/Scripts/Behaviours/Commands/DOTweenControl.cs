﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class DOTweenControl : Command
    {
        public enum TargetType
        {
            AllTweens,
            TweensOnGameObject,
            Tween
        }

        [Tooltip("Which tweens should be affected by this command")]
        public TargetType m_TweenTarget;

        [Tooltip("The target game object")]
        public GameObjectData m_TargetObject = new GameObjectData();

        [Tooltip("The target tween")]
        public TweenData m_TargetTween = new TweenData();

        public override void OnEnter()
        {
            TargetType tweenTarget = m_TweenTarget;
            GameObject targetObj = m_TargetObject.Value;
            Tween targetTween = m_TargetTween.Value;

            if (tweenTarget == TargetType.AllTweens)
            {
                ControlAllTweens();
            }
            else if (tweenTarget == TargetType.TweensOnGameObject)
            {
                foreach (Component component in gameObject.GetComponents<Component>())
                {
                    ControlTweensOnComponent(component);
                }
            }
            else if (tweenTarget == TargetType.Tween)
            {
                ControlTween(targetTween);
            }

            Continue();
        }

        public abstract void ControlAllTweens();

        public abstract void ControlTweensOnComponent(Component component);

        public abstract void ControlTween(Tween tween);
        
        public override string GetSummary()
        {
            string tweenString = "";
            if (m_TweenTarget == TargetType.AllTweens)
            {
                tweenString = "All";
            }
            else if (m_TweenTarget == TargetType.TweensOnGameObject)
            {
                tweenString = "\"" + m_TargetObject.ToString() + "\"";
            }

            return string.Format("{0}", tweenString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetObject))
            {
                if (m_TweenTarget == TargetType.TweensOnGameObject)
                {
                    if ((Application.isPlaying || m_TargetObject.IsConstant) && m_TargetObject.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetTween))
            {
                if (m_TweenTarget == TargetType.Tween)
                {
                    if ((Application.isPlaying || m_TargetTween.IsConstant) && m_TargetTween.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetObject))
            {
                if (m_TweenTarget != TargetType.TweensOnGameObject)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetTween))
            {
                if (m_TweenTarget != TargetType.Tween)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
