﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Send a tween to its end position (has no effect with tweens that have infinite loops).")]
    [AddComponentMenu("Fungus/Commands/DOTween/Complete Tween")]
    public class CompleteTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.CompleteAll(true);
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.Complete(component, true);
        }

        public override void ControlTween(Tween tween)
        {
            tween.Complete(true);
        }
    }

}