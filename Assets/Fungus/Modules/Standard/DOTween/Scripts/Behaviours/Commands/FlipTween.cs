﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Flip the direction of a tween (backwards if it was going forward or viceversa).")]
    [AddComponentMenu("Fungus/Commands/DOTween/Flip Tween")]
    public class FlipTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.FlipAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.Flip(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.Flip();
        }
    }

}