﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Play a tween forward.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Play Tween Forward")]
    public class PlayTweenForward : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.PlayForwardAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.PlayForward(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.PlayForward();
        }
    }

}