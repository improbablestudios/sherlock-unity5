﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Play a tween backwards.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Play Tween Backwards")]
    public class PlayTweenBackwards : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.PlayBackwardsAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.PlayBackwards(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.PlayBackwards();
        }
    }

}