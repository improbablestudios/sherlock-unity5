﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Play a tween.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Play Tween")]
    public class PlayTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.PlayAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.Play(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.Play();
        }
    }

}