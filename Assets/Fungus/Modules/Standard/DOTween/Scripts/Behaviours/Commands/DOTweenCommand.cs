﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using System;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;

namespace Fungus.Commands
{
    
    public abstract class DOTweenCommand : ReturnValueCommand<Tween>
    {
        [Tooltip("The time in seconds the animation will take to complete")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData();

        [Tooltip("Sets the tween as speed based (the duration will represent the number of units/degrees the tween moves x second)")]
        [FormerlySerializedAs("m_SetSpeedBased")]
        public BooleanData m_SpeedBased = new BooleanData();
        
        [Tooltip("Sets the tween as relative (the endValue will be calculated as startValue + endValue instead of being used directly)")]
        [FormerlySerializedAs("m_AddPosition")]
        [FormerlySerializedAs("m_AddSize")]
        [FormerlySerializedAs("m_AddAnchor")]
        [FormerlySerializedAs("m_AddAnchor")]
        [FormerlySerializedAs("m_AddPivot")]
        [FormerlySerializedAs("m_AddRotation")]
        [FormerlySerializedAs("m_AddScale")]
        public BooleanData m_Relative = new BooleanData();

        [Tooltip("The shape of the easing curve applied to the animation")]
        [FormerlySerializedAs("_ease")]
        public EaseData m_Ease = new EaseData(Ease.Linear);

        [Tooltip("The number of times to loop")]
        [DataMin(-1)]
        [FormerlySerializedAs("_loopCount")]
        public IntegerData m_LoopCount = new IntegerData();

        [Tooltip("The loop type")]
        [FormerlySerializedAs("_loopType")]
        public LoopTypeData m_LoopType = new LoopTypeData();

        [Tooltip("The variable to store the tween in")]
        public TweenVariable m_ReturnVariable;

        protected Tween m_tween;
        
        public override Tween GetReturnValue()
        {
            return PerformTween(GetTween());
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        protected Tween PerformTween(Tween tween)
        {
            m_tween = tween;

            bool speedBased = m_SpeedBased.Value;
            bool relative = m_Relative.Value;
            int loopCount = m_LoopCount.Value;
            LoopType loopType = m_LoopType.Value;
            Ease ease = m_Ease.Value;

            if (tween != null)
            {
                tween
                    .SetSpeedBased(speedBased)
                    .SetRelative(relative)
                    .SetEase(ease)
                    .SetLoops(loopCount, loopType)
                    .OnComplete(OnFinished);
            }
            return tween;
        }

        private void OnFinished()
        {
            OnTweenComplete();

            ContinueAfterFinished();
        }

        public virtual void OnTweenComplete()
        {
        }

        public override void OnReset()
        {
            m_tween.Goto(0f);
        }

        public override void OnPause()
        {
            m_tween.Pause();
        }

        public override void OnResume()
        {
            m_tween.Play();
        }

        public override void OnStop()
        {
            m_tween.Kill();
        }

        public override void OnComplete()
        {
            m_tween.Complete();
        }

        public abstract Tween GetTween();
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Ease))
            {
                if (!ShouldWait(false))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_LoopCount))
            {
                if (!ShouldWait(false))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_LoopType))
            {
                if (!ShouldWait(false) ||
                   (m_LoopCount.IsConstant && m_LoopCount.Value == 0))
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == GetReturnVariablePropertyPath())
            {
                return null;
            }
            return base.GetPropertyError(propertyPath);
        }
    }

}