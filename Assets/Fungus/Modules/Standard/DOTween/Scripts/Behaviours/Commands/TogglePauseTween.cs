﻿using UnityEngine;
using DG.Tweening;
using System;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Play a tween if it was paused, or pause it if it was playing.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Toggle Pause Tween")]
    public class TogglePauseTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.TogglePauseAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.TogglePause(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.TogglePause();
        }
    }

}