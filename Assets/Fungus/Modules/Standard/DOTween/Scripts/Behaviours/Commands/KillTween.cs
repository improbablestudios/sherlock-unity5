﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Kill a tween.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Kill Tween")]
    public class KillTween : DOTweenControl 
    {
        [Tooltip("Instantly complete the tween before killing it")]
        [FormerlySerializedAs("_complete")]
        public BooleanData m_Complete = new BooleanData(true);
        
        public override void ControlAllTweens()
        {
            bool complete = m_Complete.Value;
            DOTween.KillAll(complete);
        }

        public override void ControlTweensOnComponent(Component component)
        {
            bool complete = m_Complete.Value;
            DOTween.Kill(component, complete);
        }

        public override void ControlTween(Tween tween)
        {
            bool complete = m_Complete.Value;
            tween.Kill(complete);
        }
    }

}