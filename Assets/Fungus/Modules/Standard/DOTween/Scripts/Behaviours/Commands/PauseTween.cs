﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Pause a tween.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Pause Tween")]
    public class PauseTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.PauseAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.Pause(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.Pause();
        }
    }

}