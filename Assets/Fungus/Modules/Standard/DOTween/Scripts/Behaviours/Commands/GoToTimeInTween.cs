﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Send a tween to the given position in time.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Go To Time In Tween")]
    public class GoToTimeInTween : DOTweenControl
    {
        [Tooltip("Time position to reach (if higher than the whole tween duration the tween will simply reach its end).")]
        [DataMin(0)]
        [FormerlySerializedAs("_time")]
        public FloatData m_Time = new FloatData();

        [Tooltip("Tween will play after reaching the given position, otherwise it will be paused.")]
        [FormerlySerializedAs("_andPlay")]
        public BooleanData m_AndPlay = new BooleanData(true);

        public override void ControlAllTweens()
        {
            float time = m_Time.Value;
            bool andPlay = m_AndPlay.Value;
            DOTween.GotoAll(time, andPlay);
        }

        public override void ControlTweensOnComponent(Component component)
        {
            float time = m_Time.Value;
            bool andPlay = m_AndPlay.Value;
            DOTween.Goto(component, time, andPlay);
        }

        public override void ControlTween(Tween tween)
        {
            float time = m_Time.Value;
            bool andPlay = m_AndPlay.Value;
            tween.Goto(time, andPlay);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + " to " + string.Format("{0}", m_Time);
        }
    }

}