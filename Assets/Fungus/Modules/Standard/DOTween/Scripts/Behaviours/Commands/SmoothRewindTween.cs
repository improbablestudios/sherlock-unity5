﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Smoothly rewind a tween (delays excluded).\nA 'smooth rewind' animates the tween to its start position(instead of jumping to it), skipping all elapsed loops(except in case of LoopType.Incremental) while keeping the animation fluent.\nIf called on a tween who is still waiting for its delay to happen, it will simply set the delay to 0 and pause the tween.\nNOTE: A tween that was smoothly rewinded will have its play direction flipped.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Smooth Rewind Tween")]
    public class SmoothRewindTween : DOTweenControl
    {
        public override void ControlAllTweens()
        {
            DOTween.SmoothRewindAll();
        }

        public override void ControlTweensOnComponent(Component component)
        {
            DOTween.SmoothRewind(component);
        }

        public override void ControlTween(Tween tween)
        {
            tween.SmoothRewind();
        }
    }

}