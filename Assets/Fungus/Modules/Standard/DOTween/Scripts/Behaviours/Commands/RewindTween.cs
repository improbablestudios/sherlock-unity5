﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("DOTween",
                 "Rewind and pause a tween.")]
    [AddComponentMenu("Fungus/Commands/DOTween/Rewind Tween")]
    public class RewindTween : DOTweenControl
    {
        [Tooltip("Includes the tween delay, otherwise skips it")]
        [FormerlySerializedAs("_includeDelay")]
        public BooleanData m_IncludeDelay = new BooleanData(true);

        public override void ControlAllTweens()
        {
            bool includeDelay = m_IncludeDelay.Value;
            DOTween.RewindAll(includeDelay);
        }

        public override void ControlTweensOnComponent(Component component)
        {
            bool includeDelay = m_IncludeDelay.Value;
            DOTween.Rewind(component, includeDelay);
        }

        public override void ControlTween(Tween tween)
        {
            bool includeDelay = m_IncludeDelay.Value;
            tween.Rewind(includeDelay);
        }
    }

}