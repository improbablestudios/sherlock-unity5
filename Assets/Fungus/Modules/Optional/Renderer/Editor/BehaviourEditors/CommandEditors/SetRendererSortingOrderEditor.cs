#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetRendererSortingOrder), true)]
    public class SetRendererSortingOrderEditor : CommandEditor
    {
        public static Renderer m_PreviewRenderer;

        public override void DrawPreview()
        {
            SetRendererSortingOrder t = target as SetRendererSortingOrder;

            if (!Application.isPlaying)
            {
                if (t.m_Renderer.IsConstant && t.m_Renderer.Value != null)
                {
                    m_PreviewRenderer = PreviewManager.GetPreviewObject(m_PreviewRenderer, t.m_Renderer.Value);

                    if (m_PreviewRenderer != null)
                    {
                        if (t.m_SortingOrder.IsConstant)
                        {
                            m_PreviewRenderer.sortingOrder = t.m_SortingOrder.Value;
                            EditorUtility.SetDirty(m_PreviewRenderer);
                            UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                        }
                    }
                }
            }
        }
    }

}
#endif