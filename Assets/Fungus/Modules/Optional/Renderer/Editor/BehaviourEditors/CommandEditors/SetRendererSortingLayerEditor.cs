#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetRendererSortingLayer), true)]
    public class SetRendererSortingLayerEditor : CommandEditor
    {
        public static Renderer m_PreviewRenderer;

        public override void DrawPreview()
        {
            if (serializedObject != null)
            {
                SetRendererSortingLayer t = target as SetRendererSortingLayer;

                if (!Application.isPlaying)
                {
                    if (t.m_Renderer.IsConstant && t.m_Renderer.Value != null)
                    {
                        m_PreviewRenderer = PreviewManager.GetPreviewObject(m_PreviewRenderer, t.m_Renderer.Value);

                        if (m_PreviewRenderer != null)
                        {
                            if (t.m_SortingLayer.IsConstant)
                            {
                                m_PreviewRenderer.sortingLayerID = t.m_SortingLayer.Value;
                                EditorUtility.SetDirty(m_PreviewRenderer);
                                UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                            }
                        }
                    }
                }
            }
        }
    }

}
#endif