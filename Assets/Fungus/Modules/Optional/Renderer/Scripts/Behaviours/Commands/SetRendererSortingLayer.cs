﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer",
                 "Set a renderer's sorting layer.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Set Renderer Sorting Layer")]
    public class SetRendererSortingLayer : Command 
    {
        [Tooltip("The target renderer")]
        [FormerlySerializedAs("_renderer")]
        [CheckNotNullData]
        public RendererData m_Renderer = new RendererData();
        
        [Tooltip("The new layer name")]
        [SortingLayerDataField]
        [FormerlySerializedAs("_sortingLayerName")]
        public IntegerData m_SortingLayer = new IntegerData();
        
        public override void OnEnter()
        {
            Renderer renderer = m_Renderer.Value;
            int sortingLayerID = m_SortingLayer.Value;
            
            renderer.sortingLayerID = sortingLayerID;

            Continue();
        }
        
        public override string GetSummary()
        {
            string sortingLayerString = m_SortingLayer.ToString();
            if (m_SortingLayer.IsConstant)
            {
                sortingLayerString = SortingLayer.IDToName(m_SortingLayer.Value);
            }
            return string.Format("\"{0}\" to \"{1}\"", m_Renderer, sortingLayerString);
        }
    }
}