﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer",
                 "Set a renderer's sorting order.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Set Renderer Sorting Order")]
    public class SetRendererSortingOrder : Command 
    {
        [Tooltip("The target renderer")]
        [FormerlySerializedAs("_renderer")]
        [CheckNotNullData]
        public RendererData m_Renderer = new RendererData();

        [Tooltip("The new sorting order")]
        [DataMin(0)]
        [FormerlySerializedAs("_sortingOrder")]
        public IntegerData m_SortingOrder = new IntegerData();

        public override void OnEnter()
        {
            Renderer renderer = m_Renderer.Value;
            int sortingOrder = m_SortingOrder.Value;
            
            renderer.sortingOrder = sortingOrder;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to \"{1}\"", m_Renderer, m_SortingOrder);
        }
    }
}
