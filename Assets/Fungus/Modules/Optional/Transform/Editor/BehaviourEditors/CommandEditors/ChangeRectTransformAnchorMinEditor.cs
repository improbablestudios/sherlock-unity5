#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeRectTransformAnchorMin), true)]
    public class ChangeRectTransformAnchorMinEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformAnchorMin t = target as ChangeRectTransformAnchorMin;

            if (property.name == nameof(t.m_AnchorMin))
            {
                SerializedProperty changeMinXProp = serializedObject.FindProperty(nameof(t.m_ChangeX));
                SerializedProperty changeMinYProp = serializedObject.FindProperty(nameof(t.m_ChangeY));
                ConditionalVector2DataField(position, property, changeMinXProp, changeMinYProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformAnchorMin t = target as ChangeRectTransformAnchorMin;

            if (property.name == nameof(t.m_AnchorMin))
            {
                return GetConditionalVector2DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            ChangeRectTransformAnchorMin t = target as ChangeRectTransformAnchorMin;
            return nameof(t.m_RectTransform);
        }

        public override void CopyButton()
        {
            ChangeRectTransformAnchorMin t = target as ChangeRectTransformAnchorMin;
            if (t.m_RectTransform.IsConstant && t.m_RectTransform.Value != null)
            {
                if (t.m_RectTransform.Value.GetComponent<RectTransform>() != null)
                {
                    t.m_AnchorMin.Value = t.m_RectTransform.Value.GetComponent<RectTransform>().anchorMin;
                }
            }
        }
    }
}
#endif