#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ScaleTransform), true)]
    public class ScaleTransformEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ScaleTransform t = target as ScaleTransform;

            if (property.name == nameof(t.m_Scale))
            {
                SerializedProperty scaleXProp = serializedObject.FindProperty(nameof(t.m_ScaleX));
                SerializedProperty scaleYProp = serializedObject.FindProperty(nameof(t.m_ScaleY));
                SerializedProperty scaleZProp = serializedObject.FindProperty(nameof(t.m_ScaleZ));
                ConditionalVector3DataField(position, property, scaleXProp, scaleYProp, scaleZProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ScaleTransform t = target as ScaleTransform;

            if (property.name == nameof(t.m_Scale))
            {
                return GetConditionalVector3DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            ScaleTransform t = target as ScaleTransform;
            return nameof(t.m_Transform);
        }

        public override void CopyButton()
        {
            ScaleTransform t = target as ScaleTransform;
            if (t.m_Transform.IsConstant && t.m_Transform.Value != null)
            {
                t.m_Scale.Value = t.m_Transform.Value.localScale;
            }
        }
    }
}
#endif