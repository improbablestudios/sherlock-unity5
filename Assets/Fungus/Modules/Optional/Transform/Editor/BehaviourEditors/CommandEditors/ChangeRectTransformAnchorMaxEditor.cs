#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeRectTransformAnchorMax), true)]
    public class ChangeRectTransformAnchorMaxEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformAnchorMax t = target as ChangeRectTransformAnchorMax;

            if (property.name == nameof(t.m_AnchorMax))
            {
                SerializedProperty changeMaxXProp = serializedObject.FindProperty(nameof(t.m_ChangeX));
                SerializedProperty changeMaxYProp = serializedObject.FindProperty(nameof(t.m_ChangeY));
                ConditionalVector2DataField(position, property, changeMaxXProp, changeMaxYProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformAnchorMax t = target as ChangeRectTransformAnchorMax;

            if (property.name == nameof(t.m_AnchorMax))
            {
                return GetConditionalVector2DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            ChangeRectTransformAnchorMax t = target as ChangeRectTransformAnchorMax;
            return nameof(t.m_RectTransform);
        }

        public override void CopyButton()
        {
            ChangeRectTransformAnchorMax t = target as ChangeRectTransformAnchorMax;
            if (t.m_RectTransform.IsConstant && t.m_RectTransform.Value != null)
            {
                if (t.m_RectTransform.Value.GetComponent<RectTransform>() != null)
                {
                    t.m_AnchorMax.Value = t.m_RectTransform.Value.GetComponent<RectTransform>().anchorMax;
                }
            }
        }
    }
}
#endif