#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeRectTransformSize), true)]
    public class ChangeRectTransformSizeEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformSize t = target as ChangeRectTransformSize;

            if (property.name == nameof(t.m_Size))
            {
                SerializedProperty changeXProp = serializedObject.FindProperty(nameof(t.m_ChangeX));
                SerializedProperty changeYProp = serializedObject.FindProperty(nameof(t.m_ChangeY));
                ConditionalVector2DataField(position, property, changeXProp, changeYProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformSize t = target as ChangeRectTransformSize;

            if (property.name == nameof(t.m_Size))
            {
                return GetConditionalVector2DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            ChangeRectTransformSize t = target as ChangeRectTransformSize;
            return nameof(t.m_RectTransform);
        }

        public override void CopyButton()
        {
            ChangeRectTransformSize t = target as ChangeRectTransformSize;
            if (t.m_RectTransform.IsConstant && t.m_RectTransform.Value != null)
            {
                if (t.m_RectTransform.Value.GetComponent<RectTransform>() != null)
                {
                    t.m_Size.Value = t.m_RectTransform.Value.GetComponent<RectTransform>().sizeDelta;
                }
            }
        }
    }
}
#endif