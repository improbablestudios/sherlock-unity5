#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(MoveTransform), true)]
    public class MoveTransformEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            MoveTransform t = target as MoveTransform;

            if (property.name == nameof(t.m_Position))
            {
                SerializedProperty moveXProp = serializedObject.FindProperty(nameof(t.m_MoveX));
                SerializedProperty moveYProp = serializedObject.FindProperty(nameof(t.m_MoveY));
                SerializedProperty moveZProp = serializedObject.FindProperty(nameof(t.m_MoveZ));
                ConditionalVector3DataField(position, property, moveXProp, moveYProp, moveZProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            MoveTransform t = target as MoveTransform;

            if (property.name == nameof(t.m_Position))
            {
                return GetConditionalVector3DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            MoveTransform t = target as MoveTransform;
            return nameof(t.m_Transform);
        }

        public override void CopyButton()
        {
            MoveTransform t = target as MoveTransform;
            if (t.m_Transform.IsConstant && t.m_Transform.Value != null)
            {
                if (t.m_Transform.Value is RectTransform)
                {
                    t.m_Position.Value = ((RectTransform)t.m_Transform.Value).anchoredPosition;
                }
                else
                {
                    if (t.m_Local.Value)
                    {
                        t.m_Position.Value = t.m_Transform.Value.localPosition;
                    }
                    else
                    {
                        t.m_Position.Value = t.m_Transform.Value.position;
                    }
                }
            }
        }
    }
}
#endif