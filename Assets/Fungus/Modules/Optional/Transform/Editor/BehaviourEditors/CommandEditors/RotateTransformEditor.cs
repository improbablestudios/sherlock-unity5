#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(RotateTransform), true)]
    public class RotateTransformEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            RotateTransform t = target as RotateTransform;

            if (property.name == nameof(t.m_Rotation))
            {
                SerializedProperty rotateXProp = serializedObject.FindProperty(nameof(t.m_RotateX));
                SerializedProperty rotateYProp = serializedObject.FindProperty(nameof(t.m_RotateY));
                SerializedProperty rotateZProp = serializedObject.FindProperty(nameof(t.m_RotateZ));
                ConditionalVector3DataField(position, property, rotateXProp, rotateYProp, rotateZProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            RotateTransform t = target as RotateTransform;

            if (property.name == nameof(t.m_Rotation))
            {
                return GetConditionalVector3DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            RotateTransform t = target as RotateTransform;
            return nameof(t.m_Transform);
        }

        public override void CopyButton()
        {
            RotateTransform t = target as RotateTransform;
            if (t.m_Transform.IsConstant && t.m_Transform.Value != null)
            {
                if (t.m_Local.Value)
                {
                    t.m_Rotation.Value = t.m_Transform.Value.localEulerAngles;
                }
                else
                {
                    t.m_Rotation.Value = t.m_Transform.Value.eulerAngles;
                }
            }
        }
    }
}
#endif