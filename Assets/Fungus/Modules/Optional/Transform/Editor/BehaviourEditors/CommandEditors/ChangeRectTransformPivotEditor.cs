#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeRectTransformPivot), true)]
    public class ChangeRectTransformPivotEditor : DOTweenCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformPivot t = target as ChangeRectTransformPivot;

            if (property.name == nameof(t.m_Pivot))
            {
                SerializedProperty changeXProp = serializedObject.FindProperty(nameof(t.m_ChangeX));
                SerializedProperty changeYProp = serializedObject.FindProperty(nameof(t.m_ChangeY));
                ConditionalVector2DataField(position, property, changeXProp, changeYProp, label);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ChangeRectTransformPivot t = target as ChangeRectTransformPivot;

            if (property.name == nameof(t.m_Pivot))
            {
                return GetConditionalVector2DataFieldHeight(property, label);
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        public override string GetTargetPropertyName()
        {
            ChangeRectTransformPivot t = target as ChangeRectTransformPivot;
            return nameof(t.m_RectTransform);
        }

        public override void CopyButton()
        {
            ChangeRectTransformPivot t = target as ChangeRectTransformPivot;
            if (t.m_RectTransform.IsConstant && t.m_RectTransform.Value != null)
            {
                if (t.m_RectTransform.Value.GetComponent<RectTransform>() != null)
                {
                    t.m_Pivot.Value = t.m_RectTransform.Value.GetComponent<RectTransform>().pivot;
                }
            }
        }
    }
}
#endif