﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Change a transform's position.")]
    [AddComponentMenu("Fungus/Commands/Transform/Move Transform Along Path")]
    public class MoveTransformAlongPath : TransformTweenCommand
    {
        [Tooltip("The waypoints to go through")]
        [CheckNotEmptyData]
        public Vector3ListData m_Waypoints = new Vector3ListData();

        [Tooltip("The type of path: Linear (straight path) or CatmullRom (curved CatmullRom path)")]
        public PathTypeData m_PathType = new PathTypeData();

        [Tooltip("The path mode, used to determine correct LookAt options: Ignore (ignores any lookAt option passed), 3D, side-scroller 2D, top-down 2D")]
        public PathModeData m_PathMode = new PathModeData();

        [Tooltip("The resolution of the path (useless in case of Linear paths): higher resolutions make for more detailed curved paths but are more expensive. Defaults to 10, but a value of 5 is usually enough if you don't have dramatic long curves between waypoints")]
        [DataMin(0)]
        public IntegerData m_Resolution = new IntegerData();

        [Tooltip("The color of the path (shown when gizmos are active in the Play panel and the tween is running)")]
        public ColorData m_GizmoColor = new ColorData();

        [Tooltip("If true animate relative to the parent, otherwise animate in world space")]
        [FormerlySerializedAs("m_Locally")]
        public BooleanData m_Local = new BooleanData(true);

        public override Tween GetTween()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3[] waypoints = m_Waypoints.Value.ToArray();
            PathType pathType = m_PathType.Value;
            PathMode pathMode = m_PathMode.Value;
            int resolution = m_Resolution.Value;
            Color gizmoColor = m_GizmoColor.Value;
            bool locally = m_Local.Value;

            Tweener tween = null;
            RectTransform rectTransform = transform as RectTransform;
            if (rectTransform != null)
            {
                tween = rectTransform.DOPath(waypoints, duration, pathType, pathMode, resolution, gizmoColor);
            }
            else
            {
                if (locally)
                {
                    tween = transform.DOLocalPath(waypoints, duration, pathType, pathMode, resolution, gizmoColor);
                }
                else
                {
                    tween = transform.DOPath(waypoints, duration, pathType, pathMode, resolution, gizmoColor);
                }
            }

            return tween;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Local))
            {
                if (m_Transform.IsConstant && m_Transform.Value is RectTransform)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"{1}", m_Transform, GetLocalSummary(m_Local)) + GetDurationSummary(m_Duration);
        }
    }

}