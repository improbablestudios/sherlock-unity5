﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Change a rect transform's min anchor.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Change Rect Transform Anchor Min")]
    public class ChangeRectTransformAnchorMin : RectTransformTweenCommand
    {
        [Tooltip("The anchor min to change")]
        [FormerlySerializedAs("_anchorMin")]
        public Vector2Data m_AnchorMin = new Vector2Data(new Vector2(0.5f, 0.5f));

        [Tooltip("The tween will affect x")]
        [FormerlySerializedAs("changeX")]
        public bool m_ChangeX = true;

        [Tooltip("The tween will affect y")]
        [FormerlySerializedAs("changeY")]
        public bool m_ChangeY = true;
        
        public override Tween GetTween()
        {
            RectTransform rectTransform = m_RectTransform.Value;
            float duration = m_Duration.Value;
            Vector2 min = m_AnchorMin.Value;

            if (!m_ChangeX)
            {
                min.x = rectTransform.anchorMin.x;
            }
            if (!m_ChangeY)
            {
                min.y = rectTransform.anchorMin.y;
            }

            return rectTransform.DOAnchorMin(min, duration);
        }

        public override string GetSummary()
        {
            string vector2String = m_AnchorMin.ToString();
            if (m_AnchorMin.IsConstant)
            {
                vector2String = GetConditionalVector2String(m_AnchorMin.Value, m_ChangeX, m_ChangeY);
            }
            return string.Format("\"{0}\" to {1}", m_RectTransform, vector2String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ChangeX))
            {
                return false;
            }
            if (propertyPath == nameof(m_ChangeY))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}