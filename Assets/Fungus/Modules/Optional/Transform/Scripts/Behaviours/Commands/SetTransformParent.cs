﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Set the parent of a transform")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Set Transform Parent")]
    public class SetTransformParent : TransformCommand
    {
        [Tooltip("The new parent of this transform")]
        [FormerlySerializedAs("_parentTransform")]
        public TransformData m_ParentTransform = new TransformData();

        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            Transform parentTransform = m_ParentTransform.Value;
            
            transform.SetParent(parentTransform);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to \"{0}\"", m_ParentTransform);
        }
    }
}