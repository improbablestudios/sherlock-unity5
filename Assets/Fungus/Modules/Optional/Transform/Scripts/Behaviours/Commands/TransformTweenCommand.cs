﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class TransformTweenCommand : DOTweenCommand
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public TransformData m_Transform = new TransformData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Transform) + GetDurationSummary(m_Duration);
        }
        
        protected string GetConditionalVector3String(Vector3 vector3, bool showX, bool showY, bool showZ)
        {
            if (showX && showY && showZ)
            {
                return string.Format("({0}, {1}, {2})", vector3.x, vector3.y, vector3.z);
            }
            else if (showX && showY && !showZ)
            {
                return string.Format("({0}, {1})", vector3.x, vector3.y);
            }
            else if (showX && !showY && showZ)
            {
                return string.Format("(x={0}, z={1})", vector3.x, vector3.z);
            }
            else if (!showX && showY && showZ)
            {
                return string.Format("(y={0}, z={1})", vector3.y, vector3.z);
            }
            else if (showX && !showY && !showZ)
            {
                return string.Format("(x={0})", vector3.x);
            }
            else if (!showX && showY && !showZ)
            {
                return string.Format("(y={0})", vector3.y);
            }
            else if (!showX && !showY && showZ)
            {
                return string.Format("(z={0})", vector3.z);
            }
            return vector3.ToString();
        }

        protected string GetLocalSummary(BooleanData local)
        {
            if (!m_Transform.IsConstant || !(m_Transform.Value is RectTransform))
            {
                if (!local.IsConstant)
                {
                    return " locally if " + local.ToString();
                }
                else if (local.Value)
                {
                    return " locally";
                }
                else if (!local.Value)
                {
                    return " globally";
                }
            }

            return "";
        }
    }

}


