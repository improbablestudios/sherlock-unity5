﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class GetTransformVector2PropertyCommand : ReturnValueCommand<Vector2>
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public RectTransformData m_RectTransform = new RectTransformData();
        
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public Vector3Variable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\"", m_ReturnVariable, m_RectTransform);
        }
    }

}