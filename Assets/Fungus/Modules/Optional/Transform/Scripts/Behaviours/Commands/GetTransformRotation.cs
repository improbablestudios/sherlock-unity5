﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.VectorUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Get a transform's rotation.")]
    [AddComponentMenu("Fungus/Commands/Transform/Get Transform Rotation")]
    public class GetTransformRotation : GetTransformVector3PropertyCommand
    {
        [Tooltip("Normalize the angle so value can't be greater than 360 (e.g. 360 = 0, 540 = 180)")]
        [FormerlySerializedAs("_normalize")]
        public BooleanData m_Normalize = new BooleanData(true);
        
        public override Vector3 GetReturnValue()
        {
            Transform transform = m_Transform.Value;
            bool local = m_Local.Value;
            bool normalize = m_Normalize.Value;

            Vector3 rotation = Vector3.zero;

            if (local)
            {
                rotation = transform.localEulerAngles;
            }
            else
            {
                rotation = transform.eulerAngles;
            }

            if (normalize)
            {
                rotation = new Vector3(Math3d.NormalizeAngle((int)rotation.x), Math3d.NormalizeAngle((int)rotation.y), Math3d.NormalizeAngle((int)rotation.z));
            }
            
            return rotation;
        }

        public override string GetSummary()
        {
            return base.GetSummary() + GetLocalSummary() + " rotation";
        }

    }

}