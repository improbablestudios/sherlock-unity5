﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Make a transform the first child of its parent")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Set Transform As First Sibling")]
    public class SetTransformAsFirstSibling : TransformCommand
    {
        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            
            transform.SetAsFirstSibling();

            Continue();
        }
    }
}