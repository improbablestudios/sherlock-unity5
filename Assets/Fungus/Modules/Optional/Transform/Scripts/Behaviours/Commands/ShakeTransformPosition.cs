﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Shake",
                 "Shake a transform's position.")]
    [AddComponentMenu("Fungus/Commands/Transform/Shake/Shake Transform Position")]
    public class ShakeTransformPosition : ShakeTransformCommand
    {
        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3 strength = m_Strength.Value;
            int vibrato = m_Vibrato.Value;
            float randomness = m_Randomness.Value;
            bool fadeOutShake = m_FadeOutShake.Value;

            Tweener tween = null;
            RectTransform rectTransform = transform as RectTransform;
            if (rectTransform != null)
            {
                tween = rectTransform.DOShakeAnchorPos(duration, strength, vibrato, randomness, false, fadeOutShake);
            }
            else
            {
                tween = transform.DOShakePosition(duration, strength, vibrato, randomness, false, fadeOutShake);
            }
            tween.OnComplete(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
    }

}