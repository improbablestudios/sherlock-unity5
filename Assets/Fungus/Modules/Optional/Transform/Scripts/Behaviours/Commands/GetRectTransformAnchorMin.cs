﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Get a rect transform's anchor min.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Get Rect Transform Anchor Min")]
    public class GetRectTransformAnchorMin : GetTransformVector2PropertyCommand
    {
        public override Vector2 GetReturnValue()
        {
            RectTransform transform = m_RectTransform.Value;

            return transform.anchorMin;
        }

        public override string GetSummary()
        {
            return base.GetSummary() + " anchor min";
        }

    }

}