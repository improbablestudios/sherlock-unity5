using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Change a transform's position.")]
    [AddComponentMenu("Fungus/Commands/Transform/Move Transform")]
    public class MoveTransform : TransformTweenCommand
    {
        [Tooltip("The position to move to")]
        [FormerlySerializedAs("_position")]
        public Vector3Data m_Position = new Vector3Data();

        [Tooltip("The tween will affect the x axis")]
        [FormerlySerializedAs("moveX")]
        public bool m_MoveX = true;

        [Tooltip("The tween will affect the y axis")]
        [FormerlySerializedAs("moveY")]
        public bool m_MoveY = true;

        [Tooltip("The tween will affect the z axis")]
        [FormerlySerializedAs("moveZ")]
        public bool m_MoveZ = true;

        [Tooltip("If true animate relative to the parent, otherwise animate in world space")]
        [FormerlySerializedAs("_locally")]
        [FormerlySerializedAs("m_Locally")]
        public BooleanData m_Local = new BooleanData(true);

        public override Tween GetTween()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3 position = m_Position.Value;
            bool locally = m_Local.Value;

            Tweener tween = null;
            RectTransform rectTransform = transform as RectTransform;
            if (rectTransform != null)
            {
                if (!m_MoveX)
                {
                    position.x = rectTransform.anchoredPosition.x;
                }
                if (!m_MoveY)
                {
                    position.y = rectTransform.anchoredPosition.y;
                }

                tween = rectTransform.DOAnchorPos(position, duration);
            }
            else
            {
                if (locally)
                {
                    if (!m_MoveX)
                    {
                        position.x = transform.localPosition.x;
                    }
                    if (!m_MoveY)
                    {
                        position.y = transform.localPosition.y;
                    }
                    if (!m_MoveZ)
                    {
                        position.z = transform.localPosition.z;
                    }
                    
                    tween = transform.DOLocalMove(position, duration);
                }
                else
                {
                    if (!m_MoveX)
                    {
                        position.x = transform.position.x;
                    }
                    if (!m_MoveY)
                    {
                        position.y = transform.position.y;
                    }
                    if (!m_MoveZ)
                    {
                        position.z = transform.position.z;
                    }
                    
                    tween = transform.DOMove(position, duration);
                }
            }

            return tween;
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_MoveX))
            {
                return false;
            }
            if (propertyPath == nameof(m_MoveY))
            {
                return false;
            }
            if (propertyPath == nameof(m_MoveZ))
            {
                return false;
            }
            if (propertyPath == nameof(m_Local))
            {
                if (m_Transform.IsConstant && m_Transform.Value is RectTransform)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override string GetSummary()
        {
            string vector3String = m_Position.ToString();
            if (m_Position.IsConstant)
            {
                vector3String = GetConditionalVector3String(m_Position.Value, m_MoveX, m_MoveY, m_MoveZ);
            }

            return string.Format("\"{0}\"{1} to {2}", m_Transform, GetLocalSummary(m_Local), vector3String) + GetDurationSummary(m_Duration);
        }
    }

}