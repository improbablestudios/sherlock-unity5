﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class TransformCommand : Command
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public TransformData m_Transform = new TransformData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Transform);
        }
    }

}


