﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class GetTransformCommand : ReturnValueCommand<Transform>
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public TransformData m_Transform = new TransformData();
        
        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public TransformVariable m_ReturnVariable;
        
        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }
    }

}