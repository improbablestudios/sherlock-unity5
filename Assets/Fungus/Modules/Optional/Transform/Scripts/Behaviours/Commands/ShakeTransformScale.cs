﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Shake",
                 "Shake a transform's scale.")]
    [AddComponentMenu("Fungus/Commands/Transform/Shake/Shake Transform Scale")]
    public class ShakeTransformScale : ShakeTransformCommand
    {
        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3 strength = m_Strength.Value;
            int vibrato = m_Vibrato.Value;
            float randomness = m_Randomness.Value;
            bool fadeOutShake = m_FadeOutShake.Value;
            
            Tweener tween = transform.DOShakeScale(duration, strength, vibrato, randomness, fadeOutShake);
            tween.OnComplete(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
    }

}