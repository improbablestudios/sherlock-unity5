﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Get the parent of a transform.")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Get Transform Parent")]
    public class GetTransformParent : GetTransformCommand
    {
        public override Transform GetReturnValue()
        {
            Transform transform = m_Transform.Value;

            return transform.parent;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = parent of \"{1}\"", m_ReturnVariable, m_Transform);
        }
    }

}