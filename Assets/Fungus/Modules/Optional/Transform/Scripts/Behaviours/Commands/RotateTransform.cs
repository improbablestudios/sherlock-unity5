using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Change a transform's rotation.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rotate Transform")]
    public class RotateTransform : TransformTweenCommand 
    {
        [Tooltip("The rotation to rotate to")]
        [FormerlySerializedAs("_rotation")]
        public Vector3Data m_Rotation = new Vector3Data();

        [Tooltip("The tween will affect the x axis")]
        [FormerlySerializedAs("rotateX")]
        public bool m_RotateX = true;

        [Tooltip("The tween will affect the y axis")]
        [FormerlySerializedAs("rotateY")]
        public bool m_RotateY = true;

        [Tooltip("The tween will affect the z axis")]
        [FormerlySerializedAs("rotateZ")]
        public bool m_RotateZ = true;
        
        [Tooltip("If true animate relative to the parent, otherwise animate in world space")]
        [FormerlySerializedAs("_locally")]
        [FormerlySerializedAs("m_Locally")]
        public BooleanData m_Local = new BooleanData(true);

        public override Tween GetTween()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3 rotation = m_Rotation.Value;
            bool locally = m_Local.Value;

            Tweener tween = null;
            if (locally)
            {
                if (!m_RotateX)
                {
                    rotation.x = transform.localEulerAngles.x;
                }
                if (!m_RotateY)
                {
                    rotation.y = transform.localEulerAngles.y;
                }
                if (!m_RotateZ)
                {
                    rotation.z = transform.localEulerAngles.z;
                }

                tween = transform.DOLocalRotate(rotation, duration);
            }
            else
            {
                if (!m_RotateX)
                {
                    rotation.x = transform.eulerAngles.x;
                }
                if (!m_RotateY)
                {
                    rotation.y = transform.eulerAngles.y;
                }
                if (!m_RotateZ)
                {
                    rotation.z = transform.eulerAngles.z;
                }

                tween = transform.DORotate(rotation, duration);
            }

            return tween;
        }
        
        public override string GetSummary()
        {
            string vector3String = m_Rotation.ToString();
            if (m_Rotation.IsConstant)
            {
                vector3String = GetConditionalVector3String(m_Rotation.Value, m_RotateX, m_RotateY, m_RotateZ);
            }
            
            return string.Format("\"{0}\"{1} to {2}", m_Transform, GetLocalSummary(m_Local), vector3String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_RotateX))
            {
                return false;
            }
            if (propertyPath == nameof(m_RotateY))
            {
                return false;
            }
            if (propertyPath == nameof(m_RotateZ))
            {
                return false;
            }
            if (propertyPath == nameof(m_Local))
            {
                if (m_Transform.IsConstant && m_Transform.Value is RectTransform)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}