﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Make a transform the last child of its parent")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Set Transform As Last Sibling")]
    public class SetTransformAsLastSibling : TransformCommand
    {
        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            
            transform.SetAsLastSibling();

            Continue();
        }
    }
}