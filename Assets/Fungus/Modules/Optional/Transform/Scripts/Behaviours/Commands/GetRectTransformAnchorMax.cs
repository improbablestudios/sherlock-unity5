﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Get a rect transform's anchor max.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Get Rect Transform Anchor Max")]
    public class GetRectTransformAnchorMax : GetTransformVector2PropertyCommand
    {
        public override Vector2 GetReturnValue()
        {
            RectTransform transform = m_RectTransform.Value;
            
            return transform.anchorMax;
        }

        public override string GetSummary()
        {
            return base.GetSummary() + " anchor max";
        }

    }

}