﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Get a transform's scale.")]
    [AddComponentMenu("Fungus/Commands/Transform/Get Transform Scale")]
    public class GetTransformScale : GetTransformVector3PropertyCommand
    {
        public override Vector3 GetReturnValue()
        {
            Transform transform = m_Transform.Value;
            bool local = m_Local.Value;

            if (local)
            {
                return transform.localScale;
            }
            else
            {
                return transform.lossyScale;
            }
        }

        public override string GetSummary()
        {
            return base.GetSummary() + GetLocalSummary() + " scale";
        }
    }

}