﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Get a transform's position.")]
    [AddComponentMenu("Fungus/Commands/Transform/Get Transform Position")]
    public class GetTransformPosition : GetTransformVector3PropertyCommand
    {
        public override Vector3 GetReturnValue()
        {
            Transform transform = m_Transform.Value;
            bool local = m_Local.Value;

            RectTransform rectTransform = transform as RectTransform;

            if (rectTransform != null)
            {
                return rectTransform.anchoredPosition3D;
            }
            else
            {
                if (local)
                {
                    return transform.localPosition;
                }
                else
                {
                    return transform.position;
                }
            }
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + GetLocalSummary() + " position";
        }

    }

}