﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Get the child of a transform at a certain index.")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Get Transform Child")]
    public class GetTransformChild : GetTransformCommand
    {
        [Tooltip("The index of the child")]
        [DataMin(0)]
        public IntegerData m_Index = new IntegerData();

        public override Transform GetReturnValue()
        {
            Transform transform = m_Transform.Value;
            int index = m_Index.Value;

            return transform.GetChild(index);
        }
        
        public override string GetSummary()
        {
            return string.Format("{0} = child of \"{1}\" at index", m_ReturnVariable, m_Transform, m_Index);
        }
    }

}