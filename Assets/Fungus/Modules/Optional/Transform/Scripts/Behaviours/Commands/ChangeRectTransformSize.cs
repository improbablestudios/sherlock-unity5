﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Change a rect transform's size.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Change Rect Transform Size")]
    public class ChangeRectTransformSize : RectTransformTweenCommand
    {
        [Tooltip("The size to scale to")]
        [FormerlySerializedAs("_size")]
        public Vector2Data m_Size = new Vector2Data(new Vector2(100,100));

        [Tooltip("The tween will affect x")]
        [FormerlySerializedAs("changeX")]
        public bool m_ChangeX = true;

        [Tooltip("The tween will affect y")]
        [FormerlySerializedAs("changeY")]
        public bool m_ChangeY = true;

        public override Tween GetTween()
        {
            RectTransform rectTransform = m_RectTransform.Value;
            float duration = m_Duration.Value;
            Vector2 size = m_Size.Value;
            
            if (!m_ChangeX)
            {
                size.x = rectTransform.sizeDelta.x;
            }
            if (!m_ChangeY)
            {
                size.y = rectTransform.sizeDelta.y;
            }

            return rectTransform.DOSizeDelta(size, duration);
        }

        public override string GetSummary()
        {
            string vector2String = m_Size.ToString();
            if (m_Size.IsConstant)
            {
                vector2String = GetConditionalVector2String(m_Size.Value, m_ChangeX, m_ChangeY);
            }
            return string.Format("\"{0}\" to {1}", m_RectTransform, vector2String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ChangeX))
            {
                return false;
            }
            if (propertyPath == nameof(m_ChangeY))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}