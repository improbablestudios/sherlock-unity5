﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Hierarchy",
                 "Set a transform's sibling index")]
    [AddComponentMenu("Fungus/Commands/Transform/Hierarchy/Set Transform Sibling Index")]
    public class SetTransformSiblingIndex : TransformCommand
    {
        [Tooltip("The new position of this transform in the hierarchy")]
        [DataMin(0)]
        [FormerlySerializedAs("_siblingIndex")]
        public IntegerData m_SiblingIndex = new IntegerData();

        public override void OnEnter()
        {
            Transform transform = m_Transform.Value;
            
            transform.SetSiblingIndex(m_SiblingIndex.Value);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_SiblingIndex);
        }
    }
}