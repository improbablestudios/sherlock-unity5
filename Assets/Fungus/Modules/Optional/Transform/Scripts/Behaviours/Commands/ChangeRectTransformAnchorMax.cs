﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Change a rect transform's max anchor.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Change Rect Transform Anchor Max")]
    public class ChangeRectTransformAnchorMax : RectTransformTweenCommand
    {
        [Tooltip("The anchor max to change")]
        [FormerlySerializedAs("_anchorMax")]
        public Vector2Data m_AnchorMax = new Vector2Data(new Vector2(0.5f, 0.5f));

        [Tooltip("The tween will affect x")]
        [FormerlySerializedAs("changeX")]
        public bool m_ChangeX = true;

        [Tooltip("The tween will affect y")]
        [FormerlySerializedAs("changeY")]
        public bool m_ChangeY = true;

        public override Tween GetTween()
        {
            RectTransform rectTransform = m_RectTransform.Value;
            float duration = m_Duration.Value;
            Vector2 max = m_AnchorMax.Value;

            if (!m_ChangeX)
            {
                max.x = rectTransform.anchorMax.x;
            }
            if (!m_ChangeY)
            {
                max.y = rectTransform.anchorMax.y;
            }

            return rectTransform.DOAnchorMax(max, duration);
        }

        public override string GetSummary()
        {
            string vector2String = m_AnchorMax.ToString();
            if (m_AnchorMax.IsConstant)
            {
                vector2String = GetConditionalVector2String(m_AnchorMax.Value, m_ChangeX, m_ChangeY);
            }
            return string.Format("\"{0}\" to {1}", m_RectTransform, vector2String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ChangeX))
            {
                return false;
            }
            if (propertyPath == nameof(m_ChangeY))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}