﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Get a rect transform's pivot.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Get Rect Transform Pivot")]
    public class GetRectTransformPivot : GetTransformVector2PropertyCommand
    {
        public override Vector2 GetReturnValue()
        {
            RectTransform transform = m_RectTransform.Value;

            return transform.pivot;
        }

        public override string GetSummary()
        {
            return base.GetSummary() + " pivot";
        }

    }

}