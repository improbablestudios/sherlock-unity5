﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class RectTransformTweenCommand : DOTweenCommand
    {
        [Tooltip("The target rect transform")]
        [FormerlySerializedAs("_rectTransform")]
        [CheckNotNullData]
        public RectTransformData m_RectTransform = new RectTransformData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_RectTransform) + GetDurationSummary(m_Duration);
        }
        
        protected string GetConditionalVector2String(Vector2 vector2, bool showX, bool showY)
        {
            if (showX && showY)
            {
                return string.Format("({0}, {1})", vector2.x, vector2.y);
            }
            else if (showX && !showY)
            {
                return string.Format("(x={0})", vector2.x);
            }
            else if (!showX && showY)
            {
                return string.Format("(y={0})", vector2.y);
            }
            return vector2.ToString();
        }
    }
}