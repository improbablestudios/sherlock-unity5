﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Change a rect transform's pivot.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Change Rect Transform Pivot")]
    public class ChangeRectTransformPivot : RectTransformTweenCommand
    {
        [Tooltip("The pivot to change")]
        [FormerlySerializedAs("_pivot")]
        public Vector2Data m_Pivot = new Vector2Data(new Vector2(0.5f, 0.5f));

        [Tooltip("The tween will affect x")]
        [FormerlySerializedAs("changeX")]
        public bool m_ChangeX = true;

        [Tooltip("The tween will affect y")]
        [FormerlySerializedAs("changeY")]
        public bool m_ChangeY = true;

        public override Tween GetTween()
        {
            RectTransform rectTransform = m_RectTransform.Value;
            float duration = m_Duration.Value;
            Vector2 pivot = m_Pivot.Value;
            
            if (!m_ChangeX)
            {
                pivot.x = rectTransform.pivot.x;
            }
            if (!m_ChangeY)
            {
                pivot.y = rectTransform.pivot.y;
            }

            return rectTransform.DOPivot(pivot, duration);
        }

        public override string GetSummary()
        {
            string vector2String = m_Pivot.ToString();
            if (m_Pivot.IsConstant)
            {
                vector2String = GetConditionalVector2String(m_Pivot.Value, m_ChangeX, m_ChangeY);
            }
            return string.Format("\"{0}\" to {1}", m_RectTransform, vector2String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ChangeX))
            {
                return false;
            }
            if (propertyPath == nameof(m_ChangeY))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}