﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{
    
    public abstract class GetTransformVector3PropertyCommand : ReturnValueCommand<Vector3>
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public TransformData m_Transform = new TransformData();

        [Tooltip("Get local or global value")]
        [FormerlySerializedAs("_local")]
        public BooleanData m_Local = new BooleanData(true);

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public Vector3Variable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\"", m_ReturnVariable, m_Transform);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Local))
            {
                if (m_Transform.IsConstant && m_Transform.Value is RectTransform)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        protected string GetLocalSummary()
        {
            if (!m_Transform.IsConstant || !(m_Transform.Value is RectTransform))
            {
                if (!m_Local.IsConstant)
                {
                    return " local if " + m_Local.ToString();
                }
                else if (m_Local.Value)
                {
                    return " local";
                }
                else if (!m_Local.Value)
                {
                    return " global";
                }
            }

            return "";
        }
    }

}