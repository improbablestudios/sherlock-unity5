﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform/Rect Transform",
                 "Get a rect transform's size.")]
    [AddComponentMenu("Fungus/Commands/Transform/Rect Transform/Get Rect Transform Size")]
    public class GetRectTransformSize : GetTransformVector2PropertyCommand
    {
        public override Vector2 GetReturnValue()
        {
            RectTransform transform = m_RectTransform.Value;

            return transform.sizeDelta;
        }

        public override string GetSummary()
        {
            return base.GetSummary() + " size";
        }

    }

}