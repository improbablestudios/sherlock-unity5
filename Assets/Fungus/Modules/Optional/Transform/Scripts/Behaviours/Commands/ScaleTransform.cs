using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Transform",
                 "Change a transform's scale.")]
    [AddComponentMenu("Fungus/Commands/Transform/Scale Transform")]
    public class ScaleTransform : TransformTweenCommand 
    {
        [Tooltip("The scale to scale to")]
        [FormerlySerializedAs("_scale")]
        public Vector3Data m_Scale = new Vector3Data(new Vector3(1f, 1f, 1f));

        [Tooltip("The tween will affect the x axis")]
        [FormerlySerializedAs("scaleX")]
        public bool m_ScaleX = true;

        [Tooltip("The tween will affect the y axis")]
        [FormerlySerializedAs("scaleY")]
        public bool m_ScaleY = true;

        [Tooltip("The tween will affect the z axis")]
        [FormerlySerializedAs("scaleZ")]
        public bool m_ScaleZ = true;

        public override Tween GetTween()
        {
            Transform transform = m_Transform.Value;
            float duration = m_Duration.Value;
            Vector3 scale = m_Scale.Value;

            if (!m_ScaleX)
            {
                scale.x = transform.localScale.x;
            }
            if (!m_ScaleY)
            {
                scale.y = transform.localScale.y;
            }
            if (!m_ScaleZ)
            {
                scale.z = transform.localScale.z;
            }

            return transform.DOScale(scale, duration);
        }
        
        public override string GetSummary()
        {
            string vector3String = m_Scale.ToString();
            if (m_Scale.IsConstant)
            {
                vector3String = GetConditionalVector3String(m_Scale.Value, m_ScaleX, m_ScaleY, m_ScaleZ);
            }

            return string.Format("\"{0}\" to {1}", m_Transform, vector3String) + GetDurationSummary(m_Duration);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_ScaleX))
            {
                return false;
            }
            if (propertyPath == nameof(m_ScaleY))
            {
                return false;
            }
            if (propertyPath == nameof(m_ScaleZ))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}