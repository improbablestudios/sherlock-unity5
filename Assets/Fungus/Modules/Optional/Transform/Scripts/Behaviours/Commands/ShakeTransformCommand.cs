﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class ShakeTransformCommand : Command
    {
        [Tooltip("The target transform")]
        [FormerlySerializedAs("_transform")]
        [CheckNotNullData]
        public TransformData m_Transform = new TransformData();

        [Tooltip("Magnitude of shake effect in x,y,z axes")]
        [FormerlySerializedAs("_strength")]
        public Vector3Data m_Strength = new Vector3Data();

        [Tooltip("How much will the shake vibrate")]
        [DataMin(0)]
        [FormerlySerializedAs("_vibrato")]
        public IntegerData m_Vibrato = new IntegerData(10);

        [Tooltip("How much the shake will be random (Setting it to 0 will shake along a single direction)")]
        [DataRange(0, 180)]
        [FormerlySerializedAs("_randomness")]
        public FloatData m_Randomness = new FloatData(90f);

        [Tooltip("Shake will automatically fadeOut smoothly within the tween's duration")]
        [FormerlySerializedAs("_fadeOutShake")]
        public BooleanData m_FadeOutShake = new BooleanData(true);

        [Tooltip("The time in seconds the animation will take to complete")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData(1f);
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Transform) + GetDurationSummary(m_Duration);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }
    }
}


