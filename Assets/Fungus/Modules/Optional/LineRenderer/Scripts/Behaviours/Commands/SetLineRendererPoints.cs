﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Line Renderer",
                 "Set the vector3 points that make up a line renderer's line.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Line Renderer/Set Line Renderer Points")]
    public class SetLineRendererPoints : Command
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        [Tooltip("The points that make up the line")]
        [CheckNotEmptyData]
        public Vector3ListData m_Points = new Vector3ListData();

        public override void OnEnter()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;
            Vector3[] points = m_Points.Value.ToArray();

            lineRenderer.SetPositions(points);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" points = {1}", m_LineRenderer, m_Points);
        }
    }

}