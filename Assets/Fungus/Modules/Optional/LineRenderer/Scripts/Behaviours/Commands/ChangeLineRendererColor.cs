﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.DOTweenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Line Renderer",
                 "Change the color of a line.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Line Renderer/Change Line Renderer Color")]
    public class ChangeLineRendererColor : DOTweenCommand
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        [Tooltip("The color to change to at the start of the line")]
        public ColorData m_StartColor = new ColorData();

        [Tooltip("The color to change to at the end of the line")]
        public ColorData m_EndColor = new ColorData();

        public override Tween GetTween()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;
            float duration = m_Duration.Value;
            Color startColor = m_StartColor.Value;
            Color endColor = m_EndColor.Value;

            Sequence sequence = DOTween.Sequence();
            sequence.Join(lineRenderer.DOStartColor(startColor, duration).SetEase(Ease.Linear));
            sequence.Join(lineRenderer.DOEndColor(endColor, duration).SetEase(Ease.Linear));

            return sequence;
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_LineRenderer, m_StartColor) + GetDurationSummary(m_Duration);
        }
    }

}