﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DOTweenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Line Renderer",
                 "Animate a line being drawn.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Line Renderer/Draw Line")]
    public class DrawLine : DOTweenCommand
    {
        [Tooltip("The line renderer to use to draw the line")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        public override Tween GetTween()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;
            float duration = m_Duration.Value;

            return lineRenderer.DODraw(duration);
        }
        
        public override string GetSummary()
        {
            return string.Format("using \"{0}\"", m_LineRenderer) + GetDurationSummary(m_Duration);
        }
    }

}