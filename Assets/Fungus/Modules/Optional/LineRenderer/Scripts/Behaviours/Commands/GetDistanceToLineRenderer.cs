﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.VectorUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Line",
                 "Get the distance of a point to a line.")]
    [AddComponentMenu("Fungus/Commands/Variable/Line/Get Distance To Line Renderer")]
    public class GetDistanceToLineRenderer : NumericReturnValueCommand
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        [Tooltip("The point to measure the distance from")]
        public Vector3Data m_Point = new Vector3Data();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;

            Vector3[] points = new Vector3[lineRenderer.positionCount];
            lineRenderer.GetPositions(points);

            Vector3 point = m_Point.Value;

            float minDistance = float.MaxValue;
            for (int i = 0; i < points.Length - 1; i++)
            {
                float pointDistance = Math3d.PointDistanceToLine(points[i], points[i + 1], point);
                if (pointDistance < minDistance)
                {
                    minDistance = pointDistance;
                }
            }

            return minDistance;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = distance from \"{1}\" to {2}", m_ReturnVariable, m_LineRenderer, m_Point);
        }
    }

}