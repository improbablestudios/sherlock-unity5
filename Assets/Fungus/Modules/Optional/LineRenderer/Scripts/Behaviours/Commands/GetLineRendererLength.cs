﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Variable/Line",
                 "Get the length of a line.")]
    [AddComponentMenu("Fungus/Commands/Variable/Line/Get Line Renderer Length")]
    public class GetLineRendererLength : NumericReturnValueCommand
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;

            Vector3[] points = new Vector3[lineRenderer.positionCount];
            lineRenderer.GetPositions(points);
            
            float length = 0f;
            for (int i = 0; i < points.Length - 1; i++)
            {
                length += Vector3.Distance(points[i], points[i + 1]);
            }

            return length;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = length of \"{1}\"", m_ReturnVariable, m_LineRenderer);
        }
    }

}