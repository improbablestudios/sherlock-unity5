﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using System.Linq;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Line Renderer",
                 "Get the vector3 points that make up a line renderer's line.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Line Renderer/Get Line Renderer Points")]
    public class GetLineRendererPoints : ReturnValueCommand<List<Vector3>>
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public LineRendererData m_LineRenderer = new LineRendererData();

        [Tooltip("The variable to store the result in")]
        public Vector3ListVariable m_ReturnVariable;
        
        public override List<Vector3> GetReturnValue()
        {
            LineRenderer lineRenderer = m_LineRenderer.Value;

            Vector3[] points = new Vector3[lineRenderer.positionCount];
            lineRenderer.GetPositions(points);

            return points.ToList();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_LineRenderer);
        }
    }

}