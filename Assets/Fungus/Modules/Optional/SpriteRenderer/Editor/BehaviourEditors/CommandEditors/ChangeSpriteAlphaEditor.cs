#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeSpriteRendererAlpha), true)]
    public class ChangeSpriteAlphaEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeSpriteRendererAlpha t = target as ChangeSpriteRendererAlpha;
            return nameof(t.m_SpriteRenderer);
        }

        public override void CopyButton()
        {
            ChangeSpriteRendererAlpha t = target as ChangeSpriteRendererAlpha;
            if (t.m_SpriteRenderer.IsConstant && t.m_SpriteRenderer.Value != null)
            {
                t.m_Alpha.Value = t.m_SpriteRenderer.Value.color.a;
            }
        }
    }

}
#endif