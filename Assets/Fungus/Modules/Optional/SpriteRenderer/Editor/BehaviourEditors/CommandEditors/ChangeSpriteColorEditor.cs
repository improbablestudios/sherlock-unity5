#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeSpriteRendererColor), true)]
    public class ChangeSpriteColorEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeSpriteRendererColor t = target as ChangeSpriteRendererColor;
            return nameof(t.m_SpriteRenderer);
        }

        public override void CopyButton()
        {
            ChangeSpriteRendererColor t = target as ChangeSpriteRendererColor;
            if (t.m_SpriteRenderer.IsConstant && t.m_SpriteRenderer.Value != null)
            {
                t.m_Color.Value = t.m_SpriteRenderer.Value.color;
            }
        }
    }

}
#endif