﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class SpriteRendererTweenCommand : DOTweenCommand
    {
        [Tooltip("The target sprite renderer")]
        [FormerlySerializedAs("_spriteRenderer")]
        [CheckNotNullData]
        public SpriteRendererData m_SpriteRenderer = new SpriteRendererData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_SpriteRenderer);
        }
    }

}

