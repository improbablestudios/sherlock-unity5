﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Sprite Renderer",
                 "Change the alpha of a sprite renderer.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Sprite Renderer/Change Sprite Renderer Alpha")]
    public class ChangeSpriteRendererAlpha : SpriteRendererTweenCommand
    {
        [Tooltip("The alpha to change to")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_alpha")]
        public FloatData m_Alpha = new FloatData();

        public override Tween GetTween()
        {
            SpriteRenderer spriteRenderer = m_SpriteRenderer.Value;
            float duration = m_Duration.Value;
            float alpha = m_Alpha.Value;
            
            return spriteRenderer.DOFade(alpha, duration);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Alpha) + GetDurationSummary(m_Duration);
        }
    }

}