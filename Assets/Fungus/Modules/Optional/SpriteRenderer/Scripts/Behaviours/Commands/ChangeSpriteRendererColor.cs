﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Sprite Renderer",
                 "Change the color of a sprite renderer.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Sprite Renderer/Change Sprite Renderer Color")]
    public class ChangeSpriteRendererColor : SpriteRendererTweenCommand
    {
        [Tooltip("The color to change to")]
        [FormerlySerializedAs("_color")]
        public ColorData m_Color = new ColorData();

        public override Tween GetTween()
        {
            SpriteRenderer spriteRenderer = m_SpriteRenderer.Value;
            float duration = m_Duration.Value;
            Color color = m_Color.Value;
            
            return spriteRenderer.DOColor(color, duration);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Color) + GetDurationSummary(m_Duration);
        }
    }

}