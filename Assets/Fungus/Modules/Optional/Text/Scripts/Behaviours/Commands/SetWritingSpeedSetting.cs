﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Writing",
                 "Change how fast text will be written.")]
    [AddComponentMenu("Fungus/Commands/Setting/Writing/Set Writing Speed Setting")]
    public class SetWritingSpeedSetting : SettingCommand
    {
        [Tooltip("Determines how fast text will be written")]
        [DataMin(0)]
        public FloatData m_WritingSpeedMultiplier = new FloatData(1f);

        protected override void ChangeSetting()
        {
            float writingSpeedMultiplier = m_WritingSpeedMultiplier.Value;

            WriterManager.Instance.WritingSpeedMultiplier = writingSpeedMultiplier;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_WritingSpeedMultiplier);
        }
    }

}