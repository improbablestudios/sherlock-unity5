using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.UnityObjectUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Text",
                 "Get the value of a text property on a text component.")]
    [AddComponentMenu("Fungus/Commands/Text/Get Text")]
    public class GetText : ReturnValueCommand<string>
    {
        [Tooltip("The target text component")]
        [ComponentDataField(typeof(Text), typeof(TextMesh), typeof(InputField), typeof(ITextComponent))]
        [CheckNotNullData]
        public ComponentData m_TextComponent = new ComponentData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            Component textComponent = m_TextComponent.Value;
            
            return textComponent.GetText();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" text", m_ReturnVariable, m_TextComponent);
        }
    }
    
}