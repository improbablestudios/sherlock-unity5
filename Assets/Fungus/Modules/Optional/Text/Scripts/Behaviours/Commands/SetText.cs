using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Text",
                 "Set the text property on a text component.")]
    [AddComponentMenu("Fungus/Commands/Text/Set Text")]
    public class SetText : Command, ILocalizable
    {
        [Tooltip("The target text component")]
        [ComponentDataField(typeof(Text), typeof(TextMesh), typeof(InputField), typeof(ITextComponent))]
        [CheckNotNullData]
        public ComponentData m_TextComponent = new ComponentData();

        [Tooltip("The new text")]
        [StringDataArea(3, 10)]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData();

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            Component textComponent = m_TextComponent.Value;

            Flowchart flowchart = GetFlowchart();
            
            if (m_Text.VariableDataType == VariableDataType.Value)
            {
                textComponent.RegisterLocalizableText(LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_Text.Value, flowchart.FormatString));
            }
            else if (m_Text.VariableDataType == VariableDataType.Variable)
            {
                textComponent.RegisterLocalizableText(m_Text.Variable.GetLocalizedDictionary(flowchart.FormatString));
            }

            textComponent.SetText(flowchart.FormatString(m_Text.Value));

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" text = {1}", m_TextComponent, m_Text);
        }
        
        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }
    
}
