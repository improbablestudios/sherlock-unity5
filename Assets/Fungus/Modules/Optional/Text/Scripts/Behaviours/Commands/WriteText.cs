using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Text",
                 "Write text to a text component.")]
    [AddComponentMenu("Fungus/Commands/Text/Write Text")]
    public class WriteText : Command, ILocalizable
    {
        public enum TextColor
        {
            Default,
            SetVisible,
            SetAlpha,
            SetColor
        }

        [Tooltip("The target text component")]
        [ComponentDataField(typeof(Text), typeof(TextMesh), typeof(InputField), typeof(ITextComponent))]
        [CheckNotNullData]
        public ComponentData m_TextComponent = new ComponentData();

        [Tooltip("String value to assign to the text object")]
        [StringDataArea(3, 10)]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData();

        [Tooltip("If true, append this text to the exisiting text. Otherwise, overwrite existing text with this text")]
        public BooleanData m_ContinuePrevious = new BooleanData();

        [Tooltip("How the color of the text should be changed when writing")]
        public TextColor m_TextColor = TextColor.Default;

        [Tooltip("The alpha the text color will be set to when revealed")]
        [DataRange(0, 1)]
        public FloatData m_SetAlpha = new FloatData(1f);

        [Tooltip("The color the text color will be set to when revealed")]
        public ColorData m_SetColor = new ColorData(Color.white);

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        protected Writer GetWriter()
        {
            Component textComponent = m_TextComponent.Value;

            Writer writer = textComponent.GetComponent<Writer>();
            if (writer == null)
            {
                writer = textComponent.gameObject.AddComponent<Writer>() as Writer;
            }

            return writer;
        }

        public override void OnEnter()
        {
            Component textComponent = m_TextComponent.Value;
            bool waitUntilFinished = m_WaitUntilFinished.Value;
            bool continuePrevious = m_ContinuePrevious.Value;

            if (textComponent == null)
            {
                Continue();
                return;
            }

            Writer writer = GetWriter();
            if (writer == null)
            {
                Continue();
                return;
            }

            switch (m_TextColor)
            {
                case TextColor.SetAlpha:
                    writer.SetTextAlpha(m_SetAlpha.Value);
                    break;
                case TextColor.SetColor:
                    writer.SetTextColor(m_SetColor.Value);
                    break;
                case TextColor.SetVisible:
                    writer.SetTextAlpha(1f);
                    break;
            }
            
            if (m_Text.VariableDataType == VariableDataType.Value)
            {
                writer.Write(LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_Text.Value, base.FormatString), base.FormatString(m_Text.Value), continuePrevious, null, ContinueAfterFinished);
            }
            else if (m_Text.VariableDataType == VariableDataType.Variable)
            {
                writer.Write(m_Text.Variable.GetLocalizedDictionary(FormatString), FormatString(m_Text.Value), continuePrevious, null, ContinueAfterFinished);
            }
            else
            {
                writer.Write(FormatString(m_Text.Value), continuePrevious, null, ContinueAfterFinished);
            }

            if (!WillWait())
            {
                Continue();
            }
        }

        public override void OnStop()
        {
            GetWriter().Exit();
        }

        public override void OnComplete()
        {
            GetWriter().Complete();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" : \"{1}\"", m_TextComponent, m_Text);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_SetAlpha))
            {
                if (m_TextColor != WriteText.TextColor.SetAlpha)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_SetColor))
            {
                if (m_TextColor != WriteText.TextColor.SetColor)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
        
        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}
