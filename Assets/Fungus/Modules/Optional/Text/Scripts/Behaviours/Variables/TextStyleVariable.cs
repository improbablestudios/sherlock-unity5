﻿using System;
using UnityEngine;
using ImprobableStudios.TextStylePaletteUtilities;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Text Style Variable")]
    public class TextStyleVariable : Variable<TextStyle>
    {
    }

    [Serializable]
    public class TextStyleData : VariableData<TextStyle>
    {
        public TextStyleData()
        {
        }

        public TextStyleData(TextStyle v)
        {
            m_DataVal = v;
        }
    }

}