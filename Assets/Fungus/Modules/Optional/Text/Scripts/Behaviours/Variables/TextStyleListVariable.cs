﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.TextStylePaletteUtilities;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Text Style List Variable")]
    public class TextStyleListVariable : Variable<List<TextStyle>>
    {
    }

    [Serializable]
    public class TextStyleListData : VariableData<List<TextStyle>>
    {
        public TextStyleListData()
        {
            m_DataVal = new List<TextStyle>();
        }

        public TextStyleListData(List<TextStyle> v)
        {
            m_DataVal = v;
        }
    }

}