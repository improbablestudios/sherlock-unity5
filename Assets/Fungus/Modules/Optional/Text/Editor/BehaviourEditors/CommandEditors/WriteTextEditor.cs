#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Fungus.Commands;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.BaseUtilities;

namespace Fungus.CommandEditors
{
    
    [CanEditMultipleObjects, CustomEditor(typeof(WriteText), true)]
    public class WriteTextEditor : CommandEditor
    {
        private GUIContent m_textTooLongWarningLabel = new GUIContent("Warning: The text is too long to fit in the selected text box.");

        static public bool s_ShowTagHelp;
        
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            WriteText t = target as WriteText;

            if (property.name == nameof(t.m_Text))
            {
                if (t.m_Text.IsConstant)
                {
                    float tagHelpHeight = GetTagHelpHeight();
                    position.height -= tagHelpHeight;
                    base.DrawProperty(position, property, label);
                    position.y += position.height;
                    position.height = tagHelpHeight;
                    DrawTagHelp(position);
                }
                else
                {
                    base.DrawProperty(position, property, label);
                }

                EditorGUIUtility.labelWidth = 0;
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            WriteText t = target as WriteText;

            if (property.name == nameof(t.m_Text))
            {
                if (t.m_Text.IsConstant)
                {
                    return base.GetPropertyHeight(property, label) + GetTagHelpHeight();
                }
            }

            return base.GetPropertyHeight(property, label);
        }

        public static void DrawTagHelp(Rect position)
        {
            GUIContent buttonLabel = new GUIContent("Tag Help", "View available tags");

            float rightIndent = FungusGUI.VariableDataTypeFieldWidth;
            position.xMax -= rightIndent;

            position.height = GetTagHelpButtonHeight();
            Rect buttonPosition = position;
            buttonPosition.xMin += position.xMax - EditorStyles.miniButton.CalcSize(buttonLabel).x - 8;

            if (GUI.Button(buttonPosition, buttonLabel, EditorStyles.miniButton))
            {
                s_ShowTagHelp = !s_ShowTagHelp;
            }
            if (s_ShowTagHelp)
            {
                position.y += position.height;
                position.height = GetTagHelpLabelHeight();
                DrawTagHelpLabel(position);
            }
        }

        public static float GetTagHelpHeight()
        {
            float height = GetTagHelpButtonHeight();
            if (s_ShowTagHelp)
            {
                height += GetTagHelpLabelHeight();
            }
            return height;
        }

        private static float GetTagHelpButtonHeight()
        {
            return EditorGUIUtility.singleLineHeight;
        }

        private static float GetTagHelpLabelHeight()
        {
            GUIContent label = new GUIContent(WriterManager.GetTagHelpText());
            return EditorStyles.helpBox.CalcSize(label).y;
        }

        protected static void DrawTagHelpLabel(Rect position)
        {
            GUIContent label = new GUIContent(WriterManager.GetTagHelpText());
            EditorGUI.SelectableLabel(position, label.text, EditorStyles.helpBox);
        }

        public static bool IsTextTooLongToFit(Text text, string value)
        {
            if (text != null)
            {
                if (text.transform.parent.GetComponent<LayoutGroup>() == null)
                {
                    if (Writer.WillMoveTruncatedTextToNextDialogBox(text, value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected void DrawTextTooLongToFitWarning(Rect position)
        {
            float rightIndent = FungusGUI.VariableDataTypeFieldWidth;
            position.xMax -= rightIndent;

            EditorGUI.HelpBox(position, m_textTooLongWarningLabel.text, MessageType.Warning);
        }

        protected float GetTextTooLongToFitWarningHeight()
        {
            return EditorStyles.helpBox.CalcSize(m_textTooLongWarningLabel).y;
        }

    }

}
#endif