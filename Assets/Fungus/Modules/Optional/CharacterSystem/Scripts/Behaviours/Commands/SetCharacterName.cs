﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Set a character's name.")]
    [AddComponentMenu("Fungus/Commands/Character/Set Character Name")]
    public class SetCharacterName : Command
    {
        [Tooltip("The target character")]
        [FormerlySerializedAs("_character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("String value to assign to the character's name")]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData();

        public override void OnEnter()
        {
            Character character = m_Character.Value;
            string text = m_Text.Value;

            Flowchart flowchart = GetFlowchart();
            
            character.DisplayName = flowchart.FormatString(text);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" name = {1}", m_Character, m_Text);
        }
    }

}
