﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Get a character's color.")]
    [AddComponentMenu("Fungus/Commands/Character/Get Character Color")]
    public class GetCharacterColor : ReturnValueCommand<Color>
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("The variable to store the result in")]
        public ColorVariable m_ReturnVariable;

        public override Color GetReturnValue()
        {
            Character character = m_Character.Value;

            return character.Color;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" color", m_ReturnVariable, m_Character);
        }
    }

}