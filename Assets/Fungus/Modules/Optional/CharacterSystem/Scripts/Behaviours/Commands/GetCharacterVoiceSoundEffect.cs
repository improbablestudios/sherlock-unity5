﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Get a character's voice sound effect.")]
    [AddComponentMenu("Fungus/Commands/Character/Get Character Voice Sound Effect")]
    public class GetCharacterVoiceSoundEffect : ReturnValueCommand<AudioClip>
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("The variable to store the result in")]
        public AudioClipVariable m_ReturnVariable;

        public override AudioClip GetReturnValue()
        {
            Character character = m_Character.Value;

            return character.VoiceSoundEffect;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" voice sound effect", m_ReturnVariable, m_Character);
        }
    }

}