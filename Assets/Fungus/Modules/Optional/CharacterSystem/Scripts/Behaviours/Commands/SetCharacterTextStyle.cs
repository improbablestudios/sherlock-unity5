﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;
using ImprobableStudios.TextStylePaletteUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Set a character's text style.")]
    [AddComponentMenu("Fungus/Commands/Character/Set Character Text Style")]
    public class SetCharacterTextStyle : Command
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("Text style to use for the character's text style")]
        public TextStyleData m_TextStyle = new TextStyleData();

        public override void OnEnter()
        {
            Character character = m_Character.Value;
            TextStyle textStyle = m_TextStyle.Value;

            character.TextStyle = textStyle;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" text style = {1}", m_Character, m_TextStyle);
        }
    }

}
