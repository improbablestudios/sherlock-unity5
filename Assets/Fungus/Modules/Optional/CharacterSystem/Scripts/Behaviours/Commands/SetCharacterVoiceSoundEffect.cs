﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Set a character's voice sound effect.")]
    [AddComponentMenu("Fungus/Commands/Character/Set Character Voice Sound Effect")]
    public class SetCharacterVoiceSoundEffect : Command
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("Audio clip to use for the character's voice sound effect")]
        [Localize]
        public AudioClipData m_VoiceSoundEffect = new AudioClipData();

        public override void OnEnter()
        {
            Character character = m_Character.Value;
            AudioClip voiceSoundEffect = m_VoiceSoundEffect.Value;

            character.VoiceSoundEffect = voiceSoundEffect;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" voice sound effect = {1}", m_Character, m_VoiceSoundEffect);
        }
    }

}
