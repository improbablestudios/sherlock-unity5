﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Set a character's color.")]
    [AddComponentMenu("Fungus/Commands/Character/Set Character Color")]
    public class SetCharacterColor : Command
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("Color to use for the character's color")]
        public ColorData m_Color = new ColorData();

        public override void OnEnter()
        {
            Character character = m_Character.Value;
            Color color = m_Color.Value;

            character.Color = color;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" color = {1}", m_Character, m_Color);
        }
    }

}
