﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Get a character's name.")]
    [AddComponentMenu("Fungus/Commands/Character/Get Character Name")]
    public class GetCharacterName : ReturnValueCommand<string>
    {
        [Tooltip("The target character")]
        [FormerlySerializedAs("_character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            Character character = m_Character.Value;
            
            return character.DisplayName;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" name", m_ReturnVariable, m_Character);
        }
    }

}