﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;
using ImprobableStudios.TextStylePaletteUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Character",
                 "Get a character's text style.")]
    [AddComponentMenu("Fungus/Commands/Character/Get Character Text Style")]
    public class GetCharacterTextStyle : ReturnValueCommand<TextStyle>
    {
        [Tooltip("The target character")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("The variable to store the result in")]
        public TextStyleVariable m_ReturnVariable;
        
        public override TextStyle GetReturnValue()
        {
            Character character = m_Character.Value;

            return character.TextStyle;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" text style", m_ReturnVariable, m_Character);
        }
    }

}