﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Portrait Facing Direction List Variable")]
    public class PortraitFacingDirectionListVariable : Variable<List<PortraitFacingDirection>>
    {
    }

    [Serializable]
    public class PortraitFacingDirectionListData : VariableData<List<PortraitFacingDirection>>
    {
        public PortraitFacingDirectionListData()
        {
            m_DataVal = new List<PortraitFacingDirection>();
        }

        public PortraitFacingDirectionListData(List<PortraitFacingDirection> v)
        {
            m_DataVal = v;
        }
    }

}
