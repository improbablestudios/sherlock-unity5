﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Character Variable")]
    public class CharacterVariable : Variable<Character>
    {
    }

    [Serializable]
    public class CharacterData : VariableData<Character>
    {
        public CharacterData()
        {
        }

        public CharacterData(Character v)
        {
            m_DataVal = v;
        }
    }

}