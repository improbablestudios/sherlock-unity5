﻿using System;
using UnityEngine;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Portrait Facing Direction Variable")]
    public class PortraitFacingDirectionVariable : Variable<PortraitFacingDirection>
    {
    }

    [Serializable]
    public class PortraitFacingDirectionData : VariableData<PortraitFacingDirection>
    {
        public PortraitFacingDirectionData()
        {
        }

        public PortraitFacingDirectionData(PortraitFacingDirection v)
        {
            m_DataVal = v;
        }
    }

}
