﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Character List Variable")]
    public class CharacterListVariable : Variable<List<Character>>
    {
    }

    [Serializable]
    public class CharacterListData : VariableData<List<Character>>
    {
        public CharacterListData()
        {
            m_DataVal = new List<Character>();
        }

        public CharacterListData(List<Character> v)
        {
            m_DataVal = v;
        }
    }

}