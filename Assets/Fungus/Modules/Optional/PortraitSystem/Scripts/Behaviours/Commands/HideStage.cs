﻿using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Commands
{

    [CommandInfo("Portrait/Stage",
                 "Hide a stage on which character portraits are displayed.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Stage/Hide Stage")]
    public class HideStage : AnimateStageCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;
            bool useDefaultSettings = m_UseDefaultSettings.Value;
            float fadeDuration = m_FadeDuration.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }

            TransitionStates transitionStates = stage.GetTransitionStates();
            SimpleHideTransition simpleHideTransition = transitionStates?.GetTransition<SimpleHideTransition>();

            // Use default settings
            if (useDefaultSettings)
            {
                if (simpleHideTransition != null)
                {
                    fadeDuration = simpleHideTransition.FadeTransition.Duration;
                }
            }

            if (simpleHideTransition != null)
            {
                simpleHideTransition.FadeTransition.Duration = fadeDuration;
            }
            stage.Hide(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
    }

}