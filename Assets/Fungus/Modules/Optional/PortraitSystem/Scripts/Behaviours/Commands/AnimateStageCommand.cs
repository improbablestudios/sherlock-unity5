﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{
    
    public abstract class AnimateStageCommand : StageCommand
    {
        [Tooltip("Use default stage settings")]
        public BooleanData m_UseDefaultSettings = new BooleanData(true);

        [Tooltip("Determines how long it takes the stage to fade in")]
        [DataMin(0)]
        public FloatData m_FadeDuration = new FloatData();

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Stage);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_FadeDuration.IsConstant) || m_FadeDuration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_FadeDuration))
            {
                if (!m_UseDefaultSettings.IsConstant || m_UseDefaultSettings.Value)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}