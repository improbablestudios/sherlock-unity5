﻿using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using UnityEngine;

namespace Fungus.Commands
{

    [CommandInfo("Portrait/Stage",
                 "Move a stage on which character portraits are displayed to the front.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Stage/Move Stage To Front")]
    public class MoveStageToFront : StageCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }

            stage.MoveToFront();

            Continue();
        }
    }

}