﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class PortraitCommand : StageCommand
    {
        [Tooltip("Character to display")]
        [CheckNotNullData]
        public CharacterData m_Character = new CharacterData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Character);
        }
    }

}