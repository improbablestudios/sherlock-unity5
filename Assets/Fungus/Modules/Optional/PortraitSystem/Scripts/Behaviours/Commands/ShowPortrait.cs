﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Portrait",
                 "Show a character portrait.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Show Portrait")]
    public class ShowPortrait : AnimatePortraitCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;
            Character character = m_Character.Value;
            Sprite portrait = m_Portrait.Value;
            int portraitIndex = m_PortraitIndex.Value;
            PortraitFacingDirection facing = m_Facing.Value;
            bool moveInstantly = m_MoveInstantly.Value;
            StagePosition position = m_StagePosition.Value;
            int positionIndex = m_StagePositionIndex.Value;
            bool useDefaultSettings = m_UseDefaultSettings.Value;
            float fadeDuration = m_FadeDuration.Value;
            Ease fadeEase = m_FadeEase.Value;
            float moveSpeed = m_MoveSpeed.Value;
            Ease moveEase = m_MoveEase.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }

            if (!m_Character.IsConstant)
            {
                portrait = character.GetPortrait(portraitIndex);
            }

            if (!m_Stage.IsConstant)
            {
                position = stage.GetPosition(positionIndex);
            }

            // Use default settings
            if (useDefaultSettings)
            {
                fadeDuration = stage.PortraitFadeDuration;
                fadeEase = stage.PortraitFadeEase;
                moveSpeed = stage.PortraitMoveSpeed;
                moveEase = stage.PortraitMoveEase;
            }

            if (moveInstantly)
            {
                moveSpeed = 0;
            }

            stage.ShowPortrait(character, portrait, facing, position, fadeDuration, fadeEase, moveSpeed, moveEase, ContinueAfterFinished, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

    }

}