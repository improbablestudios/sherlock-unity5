﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    [CommandInfo("Portrait",
                 "Hide a character portrait.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Hide Portrait")]
    public class HidePortrait : AnimatePortraitCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;
            Character character = m_Character.Value;
            PortraitFacingDirection facing = m_Facing.Value;
            bool moveInstantly = m_MoveInstantly.Value;
            StagePosition position = m_StagePosition.Value;
            int positionIndex = m_StagePositionIndex.Value;
            bool useDefaultSettings = m_UseDefaultSettings.Value;
            float fadeDuration = m_FadeDuration.Value;
            Ease fadeEase = m_FadeEase.Value;
            float moveSpeed = m_MoveSpeed.Value;
            Ease moveEase = m_MoveEase.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }
            
            if (!m_Stage.IsConstant)
            {
                position = stage.GetPosition(positionIndex);
            }
            
            // Use default settings
            if (useDefaultSettings)
            {
                fadeDuration = stage.PortraitFadeDuration;
                fadeEase = stage.PortraitFadeEase;
                moveSpeed = stage.PortraitMoveSpeed;
                moveEase = stage.PortraitMoveEase;
            }

            if (moveInstantly)
            {
                moveSpeed = 0;
            }

            stage.HidePortrait(character, facing, position, fadeDuration, fadeEase, moveSpeed, moveEase, true, ContinueAfterFinished, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Portrait))
            {
                return false;
            }
            if (propertyPath == nameof(m_PortraitIndex))
            {
                return false;
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}