﻿using ImprobableStudios.CharacterSystem;
using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using UnityEngine;

namespace Fungus.Commands
{

    [CommandInfo("Portrait",
                 "Move a character portrait to the front.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Move Portrait To Front")]
    public class MovePortraitToFront : PortraitCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;
            Character character = m_Character.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }
            
            stage.MovePortraitToFront(character);

            Continue();
        }
    }

}