﻿using Fungus.Variables;
using ImprobableStudios.PortraitSystem;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Commands
{

    [CommandInfo("Portrait/Stage",
                 "Show a stage on which character portraits are displayed.")]
    [AddComponentMenu("Fungus/Commands/Portrait/Stage/Show Stage")]
    public class ShowStage : AnimateStageCommand
    {
        public override void OnEnter()
        {
            Stage stage = m_Stage.Value;
            bool useDefaultSettings = m_UseDefaultSettings.Value;
            float fadeDuration = m_FadeDuration.Value;

            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }

            TransitionStates transitionStates = stage.GetTransitionStates();
            SimpleShowTransition simpleShowTransition = transitionStates?.GetTransition<SimpleShowTransition>();

            // Use default settings
            if (useDefaultSettings)
            {
                if (simpleShowTransition != null)
                {
                    fadeDuration = simpleShowTransition.FadeTransition.Duration;
                }
            }

            if (simpleShowTransition != null)
            {
                simpleShowTransition.FadeTransition.Duration = fadeDuration;
            }
            stage.Show(ContinueAfterFinished);
            
            if (!WillWait())
            {
                Continue();
            }
        }
    }

}