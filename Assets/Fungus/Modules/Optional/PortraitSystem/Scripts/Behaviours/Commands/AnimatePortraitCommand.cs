﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{

    public abstract class AnimatePortraitCommand : PortraitCommand
    {
        [Tooltip("The portrait to display")]
        public SpriteData m_Portrait = new SpriteData();

        [Tooltip("The portrait index to display")]
        [DataMin(-1)]
        public IntegerData m_PortraitIndex = new IntegerData(0);

        [Tooltip("The direction character is facing")]
        public PortraitFacingDirectionData m_Facing = new PortraitFacingDirectionData();

        [Tooltip("Move the portrait to this position")]
        [ObjectDataPopup("<Previous>")]
        public StagePositionData m_StagePosition = new StagePositionData();

        [Tooltip("Move the portrait to this position index")]
        [DataMin(-1)]
        [FormerlySerializedAs("m_PositionIndex")]
        public IntegerData m_StagePositionIndex = new IntegerData(0);

        [Tooltip("If true, show the portrait instantly at its new position. Otherwise, animate it moving to its new position")]
        public BooleanData m_MoveInstantly = new BooleanData(true);

        [Tooltip("Use the default stage settings")]
        public BooleanData m_UseDefaultSettings = new BooleanData(true);

        [Tooltip("Determines how long it takes a portrait to fade in")]
        [DataMin(0)]
        public FloatData m_FadeDuration = new FloatData();

        [Tooltip("The fade ease type")]
        public EaseData m_FadeEase = new EaseData(Ease.InOutQuad);

        [Tooltip("Determines how fast the portraits move between positions")]
        [DataMin(0)]
        public FloatData m_MoveSpeed = new FloatData();

        [Tooltip("The movement ease type")]
        public EaseData m_MoveEase = new EaseData(Ease.InOutQuad);

        public override string GetSummary()
        {
            string characterSummary = "";
            string fromPositionSummary = "";
            string toPositionSummary = "";
            string portraitSummary = "";
            string facingSummary = "";

            characterSummary = m_Character.ToString();

            if (!m_Character.IsConstant)
            {
                portraitSummary = string.Format(" - {0}", m_PortraitIndex);
            }
            else if (!m_Portrait.IsConstant || m_Portrait.Value != null)
            {
                portraitSummary = string.Format(" - {0}", m_Portrait);
            }

            string toPositionPrefixSummary = " ";
            if (m_MoveInstantly.IsConstant && !m_MoveInstantly.Value)
            {
                toPositionPrefixSummary = "move to";
            }
            else if (m_MoveInstantly.IsConstant && m_MoveInstantly.Value)
            {
                toPositionPrefixSummary = "at";
            }
            else if (!m_MoveInstantly.IsConstant)
            {
                toPositionPrefixSummary = m_MoveInstantly.ToString();
            }

            if (!m_Stage.IsConstant)
            {
                toPositionSummary = string.Format(" {0} \"{1}\"", toPositionPrefixSummary, m_StagePositionIndex);
            }
            else if (!m_StagePosition.IsConstant || m_StagePosition.Value != null)
            {
                toPositionSummary = string.Format(" {0} \"{1}\"", toPositionPrefixSummary, m_StagePosition);
            }

            if (!m_Facing.IsConstant)
            {
                facingSummary = string.Format(" facing \"{0}\"", facingSummary);
            }
            else if (m_Facing.IsConstant && m_Facing.Value == PortraitFacingDirection.Left)
            {
                facingSummary = string.Format(" facing \"{0}\"", "<--");
            }
            else if (m_Facing.IsConstant && m_Facing.Value == PortraitFacingDirection.Right)
            {
                facingSummary = string.Format(" facing \"{0}\"", "-->");
            }

            return string.Format("\"{0}{1}\"{2}{3}{4}", characterSummary, portraitSummary, facingSummary, fromPositionSummary, toPositionSummary);
        }

        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Character.IsConstant) || m_Character.Value != null)
            {
                if ((!runtime && !m_FadeDuration.IsConstant) || m_FadeDuration.Value > 0)
                {
                    return true;
                }
                if ((!runtime && !m_MoveInstantly.IsConstant) || !m_MoveInstantly.Value)
                {
                    return true;
                }
            }

            return base.ShouldWait(runtime);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Character))
            {
                if ((Application.isPlaying || m_Character.IsConstant) && m_Character.Value != null)
                {
                    if (m_Character.Value.Portraits.Length <= 0)
                    {
                        return "No portraits available for this character";
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Portrait))
            {
                if (!m_Character.IsConstant || (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_PortraitIndex))
            {
                if (m_Character.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Facing))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if (m_Character.IsConstant && m_Character.Value != null &&
                    m_Portrait.IsConstant && m_Portrait.Value != null &&
                    m_Character.Value.GetPortrait(m_Portrait.Value) != null &&
                    m_Character.Value.GetPortrait(m_Portrait.Value).m_Facing == PortraitFacingDirection.Front)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_StagePosition))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if (!m_Stage.IsConstant || m_Stage.Value == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_StagePositionIndex))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if (m_Stage.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveInstantly))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if ((m_Stage.IsConstant && (!m_StagePosition.IsConstant || m_StagePosition.Value == null)) ||
                    (!m_Stage.IsConstant && (!m_StagePositionIndex.IsConstant || m_StagePositionIndex.Value < 0)))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_UseDefaultSettings))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_FadeDuration))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if ((!m_UseDefaultSettings.IsConstant || m_UseDefaultSettings.Value))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_FadeEase))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if ((!m_UseDefaultSettings.IsConstant || m_UseDefaultSettings.Value))
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveSpeed))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if ((!m_UseDefaultSettings.IsConstant || m_UseDefaultSettings.Value))
                {
                    return false;
                }
                if ((m_Stage.IsConstant && (!m_StagePosition.IsConstant || m_StagePosition.Value == null)) ||
                    (!m_Stage.IsConstant && (!m_StagePositionIndex.IsConstant || m_StagePositionIndex.Value < 0)))
                {
                    return false;
                }
                if (!m_MoveInstantly.IsConstant || m_MoveInstantly.Value)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MoveEase))
            {
                if (m_Character.IsConstant && (m_Character.Value == null || m_Character.Value.Portraits.Length <= 0))
                {
                    return false;
                }
                if ((!m_UseDefaultSettings.IsConstant || m_UseDefaultSettings.Value))
                {
                    return false;
                }
                if ((m_Stage.IsConstant && (!m_StagePosition.IsConstant || m_StagePosition.Value == null)) ||
                    (!m_Stage.IsConstant && (!m_StagePositionIndex.IsConstant || m_StagePositionIndex.Value < 0)))
                {
                    return false;
                }
                if (!m_MoveInstantly.IsConstant || m_MoveInstantly.Value)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}