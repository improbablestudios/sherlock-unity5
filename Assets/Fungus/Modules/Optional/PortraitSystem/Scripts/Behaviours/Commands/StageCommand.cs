﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class StageCommand : Command
    {
        [Tooltip("The target stage")]
        [ObjectDataPopup("<Default>", true)]
        [CheckNotNullData]
        public StageData m_Stage = new StageData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Stage);
        }
    }

}