﻿using ImprobableStudios.PortraitSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Stage Position List Variable")]
    public class StagePositionListVariable : Variable<List<StagePosition>>
    {
    }

    [Serializable]
    public class StagePositionListData : VariableData<List<StagePosition>>
    {
        public StagePositionListData()
        {
            m_DataVal = new List<StagePosition>();
        }

        public StagePositionListData(List<StagePosition> v)
        {
            m_DataVal = v;
        }
    }

}
