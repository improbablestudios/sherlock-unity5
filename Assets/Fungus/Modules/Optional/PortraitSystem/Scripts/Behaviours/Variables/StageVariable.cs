﻿using System;
using UnityEngine;
using ImprobableStudios.PortraitSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Stage Variable")]
    public class StageVariable : Variable<Stage>
    {
    }

    [Serializable]
    public class StageData : VariableData<Stage>
    {
        public StageData()
        {
        }

        public StageData(Stage v)
        {
            m_DataVal = v;
        }
    }

}
