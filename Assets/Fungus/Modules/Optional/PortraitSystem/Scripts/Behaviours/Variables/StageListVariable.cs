﻿using ImprobableStudios.PortraitSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Stage List Variable")]
    public class StageListVariable : Variable<List<Stage>>
    {
    }

    [Serializable]
    public class StageListData : VariableData<List<Stage>>
    {
        public StageListData()
        {
            m_DataVal = new List<Stage>();
        }

        public StageListData(List<Stage> v)
        {
            m_DataVal = v;
        }
    }

}
