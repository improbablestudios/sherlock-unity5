﻿using System;
using UnityEngine;
using ImprobableStudios.PortraitSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Stage Position Variable")]
    public class StagePositionVariable : Variable<StagePosition>
    {
    }

    [Serializable]
    public class StagePositionData : VariableData<StagePosition>
    {
        public StagePositionData()
        {
        }

        public StagePositionData(StagePosition v)
        {
            m_DataVal = v;
        }
    }

}
