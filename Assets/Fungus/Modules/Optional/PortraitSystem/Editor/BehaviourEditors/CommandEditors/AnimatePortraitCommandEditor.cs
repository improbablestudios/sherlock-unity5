#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.CharacterSystem;
using ImprobableStudios.PortraitSystem;
using ImprobableStudios.SearchPopupUtilities;
using System;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(AnimatePortraitCommand), true)]
    public class AnimatePortraitCommandEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            AnimatePortraitCommand t = target as AnimatePortraitCommand;

            if (property.name == nameof(t.m_Character))
            {
                EditorGUI.BeginChangeCheck();
                base.DrawProperty(position, property, label);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                if (t.m_Character.IsConstant && t.m_Character.Value != null && t.m_Character.Value.Portraits.Length <= 0)
                {
                    EditorGUILayout.HelpBox("This character has no portraits. Please add portraits to the character before using this command.", MessageType.Error);
                }
            }
            else if (property.name == nameof(t.m_Portrait))
            {
                VariableEditor.VariableDataField<Sprite>(position, property, label, PortraitPopup);
            }
            else if (property.name == nameof(t.m_PortraitIndex))
            {
                EditorGUI.BeginChangeCheck();
                base.DrawProperty(position, property, label);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                if (!t.m_PortraitIndex.Variable && t.m_PortraitIndex.Value < 0)
                {
                    EditorGUILayout.HelpBox("Portrait Index can't be less than 0.", MessageType.Error);
                }
            }
            else if (property.name == nameof(t.m_Facing))
            {
                VariableEditor.VariableDataField<PortraitFacingDirection>(position, property, label, FacingPopup);
            }
            else if (property.name == nameof(t.m_StagePosition))
            {
                VariableEditor.VariableDataField<StagePosition>(position, property, label, StagePositionPopup);
            }
            else if (property.name == nameof(t.m_StagePositionIndex))
            {
                EditorGUI.BeginChangeCheck();
                base.DrawProperty(position, property, label);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                if (!t.m_StagePositionIndex.Variable && t.m_StagePositionIndex.Value < 0)
                {
                    EditorGUILayout.HelpBox("Position Index can't be less than 0.", MessageType.Error);
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override void DrawPreview()
        {
            AnimatePortraitCommand t = target as AnimatePortraitCommand;
            SerializedProperty portraitProp = serializedObject.FindProperty(nameof(t.m_Portrait));
            SerializedProperty portraitIndexProp = serializedObject.FindProperty(nameof(t.m_PortraitIndex));
            SerializedProperty characterProp = serializedObject.FindProperty(nameof(t.m_Character));
            SerializedProperty facingProp = serializedObject.FindProperty(nameof(t.m_Facing));

            if ((t.m_Character.IsConstant && t.m_Character.Value != null) &&
                (t.m_Portrait.IsConstant && t.m_Portrait.Value != null) &&
                t.GetType() != typeof(HidePortrait))
            {
                EditorGUI.BeginChangeCheck();
                DrawPortraitPreview(portraitProp, characterProp, facingProp);
                if (EditorGUI.EndChangeCheck())
                {
                    portraitIndexProp.FindPropertyRelative(VariableData.GetDataValPropertyName()).intValue = Array.IndexOf(GetPortraitSprites(t.m_Character.Value), t.m_Portrait.Value);
                }
            }
        }

        public static void DrawPortraitPreview(SerializedProperty portraitProp, SerializedProperty characterProp, SerializedProperty facingProp = null)
        {
            Sprite portrait = portraitProp.FindPropertyRelative(VariableData.GetDataValPropertyName()).objectReferenceValue as Sprite;
            Character character = characterProp.FindPropertyRelative(VariableData.GetDataValPropertyName()).objectReferenceValue as Character;
            PortraitFacingDirection facing = PortraitFacingDirection.Front;
            if (facingProp != null)
            {
                facing = (PortraitFacingDirection)facingProp.FindPropertyRelative(VariableData.GetDataValPropertyName()).enumValueIndex;
            }

            Portrait characterPortrait = character.GetPortrait(portrait);

            if (characterPortrait != null)
            {
                DrawPreviewToolbar(portraitProp, GetPortraitSprites(character).ToArray());

                if (portrait != null)
                {
                    Texture2D characterTexture = portrait.texture;

                    float aspect = (float)characterTexture.width / (float)characterTexture.height;
                    Rect previewRect = GUILayoutUtility.GetAspectRect(aspect, GUILayout.Width(100), GUILayout.ExpandWidth(true));

                    if (characterTexture != null)
                    {
                        MakePortraitWrapRepeat(characterTexture);
                        if (facing == characterPortrait.m_Facing || facing == PortraitFacingDirection.Front)
                        {
                            GUI.DrawTextureWithTexCoords(previewRect, characterTexture, new Rect(1, 1, 1, 1));
                        }
                        else
                        {
                            GUI.DrawTextureWithTexCoords(previewRect, characterTexture, new Rect(1, 1, -1, 1));
                        }
                    }
                }
            }
        }

        protected Stage GetStage()
        {
            AnimatePortraitCommand t = target as AnimatePortraitCommand;

            Stage stage = t.m_Stage.Value;
            
            if (stage == null)
            {
                stage = Stage.DefaultStage;
            }

            return stage;
        }

        protected static Sprite[] GetPortraitSprites(Character character)
        {
            return character.Portraits.Select(i => i.m_Sprite).ToArray();
        }

        protected void PortraitPopup(Rect position, SerializedProperty property, GUIContent label)
        {
            AnimatePortraitCommand t = target as AnimatePortraitCommand;
            SearchPopupGUI.ObjectSearchPopupField<Sprite>(position, property, label, new GUIContent("<Previous>"), GetPortraitSprites(t.m_Character.Value));
        }

        protected void FacingPopup(Rect position, SerializedProperty property, GUIContent label)
        {
            GUIContent[] facingArrows = new GUIContent[]
                    {
                            new GUIContent("<Previous>"),
                            new GUIContent("<--"),
                            new GUIContent("-->"),
                    };
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            int facing = property.enumValueIndex;
            facing = EditorGUI.Popup(position, label, facing, facingArrows);
            if (EditorGUI.EndChangeCheck())
            {
                property.enumValueIndex = facing;
            }
            EditorGUI.EndProperty();
        }

        protected void StagePositionPopup(Rect position, SerializedProperty property, GUIContent label)
        {
            AnimatePortraitCommand t = target as AnimatePortraitCommand;
            SearchPopupGUI.ObjectSearchPopupField<StagePosition>(position, property, label, new GUIContent("<Previous>"), t.m_Stage.Value.PortraitPositions);
        }

        private static void MakePortraitWrapRepeat(Texture2D portrait)
        {
            string path = AssetDatabase.GetAssetPath(portrait);

            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

            if (!textureImporter.textureType.Equals(TextureImporterType.Sprite) || !textureImporter.wrapMode.Equals(TextureWrapMode.Repeat))
            {
                textureImporter.textureType = TextureImporterType.Sprite;
                textureImporter.wrapMode = TextureWrapMode.Repeat;

                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            }
        }
    }
}
#endif