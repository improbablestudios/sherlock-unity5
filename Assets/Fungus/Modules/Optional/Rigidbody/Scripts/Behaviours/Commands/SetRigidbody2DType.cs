﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Rigidbody",
                 "Set the body type of a rigidbody2D.")]
    [AddComponentMenu("Fungus/Commands/Rigidbody/Set Rigidbody2D Type")]
    public class SetRigidbody2DType : Command
    {
        [Tooltip("The target rigidbody2D")]
        [CheckNotNullData]
        public Rigidbody2DData m_Rigidbody2D = new Rigidbody2DData();

        [Tooltip("The new type")]
        public RigidbodyType2DData m_BodyType = new RigidbodyType2DData();

        public override void OnEnter()
        {
            Rigidbody2D rigidbody2D = m_Rigidbody2D.Value;
            RigidbodyType2D bodyType = m_BodyType.Value;

            rigidbody2D.bodyType = bodyType;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("of \"{0}\" to {1}", m_Rigidbody2D, m_BodyType);
        }
    }

}