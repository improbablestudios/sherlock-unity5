﻿using ImprobableStudios.DOTweenUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Animated Sprite Variable")]
    public class AnimatedSpriteVariable : Variable<AnimatedSprite>
    {
    }

    [Serializable]
    public class AnimatedSpriteData : VariableData<AnimatedSprite>
    {
        public AnimatedSpriteData()
        {
        }

        public AnimatedSpriteData(AnimatedSprite v)
        {
            m_DataVal = v;
        }
    }

}