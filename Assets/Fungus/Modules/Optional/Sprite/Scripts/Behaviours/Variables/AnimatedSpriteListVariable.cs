﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DOTweenUtilities;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Animated Sprite List Variable")]
    public class AnimatedSpriteListVariable : Variable<List<AnimatedSprite>>
    {
    }

    [Serializable]
    public class AnimatedSpriteListData : VariableData<List<AnimatedSprite>>
    {
        public AnimatedSpriteListData()
        {
            m_DataVal = new List<AnimatedSprite>();
        }

        public AnimatedSpriteListData(List<AnimatedSprite> v)
        {
            m_DataVal = v;
        }
    }

}