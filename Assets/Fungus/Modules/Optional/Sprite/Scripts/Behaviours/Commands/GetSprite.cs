﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.UnityObjectUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Sprite",
                 "Get the value of the sprite property on a sprite component.")]
    [AddComponentMenu("Fungus/Commands/Sprite/Get Sprite")]
    public class GetSprite : ReturnValueCommand<Sprite>
    {
        [Tooltip("The target sprite component")]
        [ComponentDataField(typeof(SpriteRenderer), typeof(Image), typeof(ISpriteComponent))]
        [CheckNotNullData]
        public ComponentData m_SpriteComponent = new ComponentData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public SpriteVariable m_ReturnVariable;
        
        public override Sprite GetReturnValue()
        {
            Component spriteComponent = m_SpriteComponent.Value;
            
            return spriteComponent.GetSprite();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" sprite", m_ReturnVariable, m_SpriteComponent);
        }
    }

}