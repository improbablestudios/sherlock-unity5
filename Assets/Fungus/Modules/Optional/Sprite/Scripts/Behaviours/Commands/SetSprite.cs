﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityObjectUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Sprite",
                 "Set the sprite property on an sprite component.")]
    [AddComponentMenu("Fungus/Commands/Sprite/Set Sprite")]
    public class SetSprite : Command, ILocalizable
    {
        [Tooltip("The target sprite component")]
        [ComponentDataField(typeof(SpriteRenderer), typeof(Image), typeof(ISpriteComponent))]
        [CheckNotNullData]
        public ComponentData m_SpriteComponent = new ComponentData();

        [Tooltip("The new sprite")]
        [Localize]
        [FormerlySerializedAs("_sprite")]
        public SpriteData m_Sprite = new SpriteData();

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            Component spriteComponent = m_SpriteComponent.Value;
            Sprite sprite = m_Sprite.Value;

            if (m_Sprite.VariableDataType == VariableDataType.Value)
            {
                spriteComponent.RegisterLocalizableSprite(this.SafeGetLocalizedDictionary(x => x.m_Sprite.Value));
            }
            else if (m_Sprite.VariableDataType == VariableDataType.Variable)
            {
                spriteComponent.RegisterLocalizableSprite(m_Sprite.Variable.GetLocalizedDictionary());
            }

            spriteComponent.SetSprite(sprite);

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" sprite = {1}", m_SpriteComponent, m_Sprite);
        }
        
        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}