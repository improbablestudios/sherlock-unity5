﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.UnityObjectUtilities;
using ImprobableStudios.DOTweenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Sprite",
                 "Play a sprite frame animation.")]
    [AddComponentMenu("Fungus/Commands/Sprite/Animate Sprite")]
    public class AnimateSprite : DOTweenCommand
    {
        [Tooltip("Sprite component to get sprite value from")]
        [ComponentDataField(typeof(SpriteRenderer), typeof(Image), typeof(ISpriteComponent))]
        [CheckNotNullData]
        public ComponentData m_SpriteComponent = new ComponentData();

        [Tooltip("Sprite frames to use for animation")]
        [CheckNotEmptyData]
        public SpriteListData m_Frames = new SpriteListData();

        [Tooltip("Specify FPS for the animation instead of duration")]
        public BooleanData m_UseFramesPerSecond = new BooleanData(true);

        [Tooltip("How many frames to display per second")]
        [DataMin(0)]
        public IntegerData m_FramesPerSecond = new IntegerData(24);

        public override Tween GetTween()
        {
            Component spriteComponent = m_SpriteComponent.Value;
            Sprite[] frames = m_Frames.Value.ToArray();
            float duration = m_Duration.Value;
            bool useFramesPerSecond = m_UseFramesPerSecond.Value;
            int framesPerSecond = m_FramesPerSecond.Value;

            if (useFramesPerSecond)
            {
                return spriteComponent.DOFrameAnimation(frames, framesPerSecond);
            }
            else
            {
                return spriteComponent.DOFrameAnimation(frames, duration);
            }
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_SpriteComponent);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_UseFramesPerSecond.IsConstant) ||
                ((m_UseFramesPerSecond.Value && m_FramesPerSecond.Value > 0) || (!m_UseFramesPerSecond.Value && m_Duration.Value > 0)))
            {
                return true;
            }
            return false;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_FramesPerSecond))
            {
                if (m_UseFramesPerSecond.IsConstant && !m_UseFramesPerSecond.Value)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Duration))
            {
                if (m_UseFramesPerSecond.IsConstant && m_UseFramesPerSecond.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
