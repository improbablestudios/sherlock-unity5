﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.UIUtilities;

namespace Fungus
{

    [Serializable]
    [AddComponentMenu("Fungus/Behaviours/UI/Setting/Writing Speed Multiplier Selector")]
    public class WritingSpeedMultiplierSelector : SettingSelector<Slider>
    {
        [Tooltip("The slider that can be used to select the writing speed multiplier")]
        [SerializeField]
        [FormerlySerializedAs("slider")]
        protected Slider m_Slider;

        public Slider Slider
        {
            get
            {
                return m_Slider;
            }
            set
            {
                m_Slider = value;
            }
        }

        public override Slider GetControl()
        {
            return m_Slider;
        }

        public override void SetControl(Slider control)
        {
            m_Slider = control;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Slider != null)
            {
                m_Slider.value = WriterManager.Instance.WritingSpeedMultiplier;
                m_Slider.onValueChanged.AddListener(OnValueChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Slider != null)
            {
                m_Slider.onValueChanged.RemoveListener(OnValueChanged);
            }
        }

        protected virtual void OnValueChanged(float value)
        {
            WriterManager.Instance.WritingSpeedMultiplier = value;
            OnSettingChanged();
        }
    }

}