﻿using ImprobableStudios.SaveSystem;
using System;
using System.Linq.Expressions;
using ImprobableStudios.UIUtilities;

namespace Fungus
{

    [Serializable]
    public class WriterManagerSaveState : ScriptableObjectSaveState<WriterManager>
    {
        public override Expression<Func<WriterManager, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<WriterManager, object>>[]
            {
                x => x.WritingSpeedMultiplier
            };
        }

        public override int LoadPriority()
        {
            return -2;
        }
    }

    public class SavableWriterManager : ISavableSetting
    {
        [Serializable]
        public class SavableWriterManagerSaveState : SaveState<SavableWriterManager>
        {
            public override void Save(SavableWriterManager savable)
            {
                savable.m_saveState.Save(WriterManager.Instance);

                base.Save(savable);
            }

            public override void Load(SavableWriterManager savable)
            {
                savable.m_saveState.Load(WriterManager.Instance);

                base.Load(savable);
            }
        }
        
        protected WriterManagerSaveState m_saveState = new WriterManagerSaveState();

        public SaveState GetSaveState()
        {
            return new SavableWriterManagerSaveState();
        }

        public string GetKey()
        {
            return WriterManager.Instance.GetKey();
        }
    }

}
