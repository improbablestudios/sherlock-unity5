﻿using ImprobableStudios.BaseUtilities;
using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class UIConstructSaveState : UIConstructState<UIConstruct>
    {
    }

    [Serializable]
    public class UIConstructState<T> : BehaviourSaveState<T> where T : UIConstruct
    {
        public override void Save(T component)
        {
            base.Save(component);
        }

        public override void Load(T component)
        {
            base.Load(component);

            component.RebuildUI();
        }
    }


    [RequireComponent(typeof(UIConstruct))]
    [AddComponentMenu("Save System/Savable UI Construct")]
    public class SavableUIConstruct : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableUIConstructSaveState : BehaviourSaveState<SavableUIConstruct>
        {
            public override void Save(SavableUIConstruct savableUIConstruct)
            {
                savableUIConstruct.m_saveState.Save(savableUIConstruct.GetComponent<UIConstruct>());

                base.Save(savableUIConstruct);
            }

            public override void Load(SavableUIConstruct savableUIConstruct)
            {
                savableUIConstruct.m_saveState.Load(savableUIConstruct.GetComponent<UIConstruct>());

                base.Load(savableUIConstruct);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected UIConstructSaveState m_saveState = new UIConstructSaveState();

        public SaveState GetSaveState()
        {
            return new SavableUIConstructSaveState();
        }
    }

}