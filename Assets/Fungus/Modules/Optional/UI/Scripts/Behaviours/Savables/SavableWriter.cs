﻿using ImprobableStudios.BaseUtilities;
using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace ImprobableStudios.SaveSystem
{

    [Serializable]
    public class WriterSaveState : WriterState<Writer>
    {
    }

    [Serializable]
    public class WriterState<T> : BehaviourSaveState<T> where T : Writer
    {
        public override void Save(T component)
        {
            base.Save(component);
        }

        public override void Load(T component)
        {
            base.Load(component);

            component.Rewrite();
        }
    }


    [RequireComponent(typeof(Writer))]
    [AddComponentMenu("Save System/Savable Writer")]
    public class SavableWriter : PersistentBehaviour, ISavable
    {
        [Serializable]
        public class SavableWriterSaveState : BehaviourSaveState<SavableWriter>
        {
            public override void Save(SavableWriter savable)
            {
                savable.m_saveState.Save(savable.GetComponent<Writer>());

                base.Save(savable);
            }

            public override void Load(SavableWriter savable)
            {
                savable.m_saveState.Load(savable.GetComponent<Writer>());

                base.Load(savable);
            }

            public override int LoadPriority()
            {
                return 3;
            }
        }
        
        protected WriterSaveState m_saveState = new WriterSaveState();

        public SaveState GetSaveState()
        {
            return new SavableWriterSaveState();
        }
    }

}