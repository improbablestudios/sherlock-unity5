﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI",
                 "Show a ui with a transition.")]
    [AddComponentMenu("Fungus/Commands/UI/Show UI")]
    public class ShowUI : Command
    {
        [Tooltip("The target UI")]
        [CheckNotNullData]
        public UIConstructData m_UI = new UIConstructData();

        public override void OnEnter()
        {
            UIConstruct ui = m_UI.Value;

            ui.Show(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_UI);
        }
    }

}