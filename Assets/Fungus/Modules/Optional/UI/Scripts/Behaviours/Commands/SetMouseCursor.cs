﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Cursor",
                 "Set the mouse cursor sprite.")]
    [AddComponentMenu("Fungus/Commands/UI/Cursor/Set Mouse Cursor")]
    public class SetMouseCursor : Command 
    {
        [Tooltip("Texture to use for cursor. Will use default mouse cursor if no sprite is specified")]
        [FormerlySerializedAs("_cursorTexture")]
        [CheckNotNullData]
        public Texture2DData m_CursorTexture = new Texture2DData();

        [Tooltip("The offset from the top left of the texture to use as the target point")]
        [FormerlySerializedAs("_hotSpot")]
        public Vector2Data m_HotSpot = new Vector2Data();

        // Cached static cursor settings
        protected static Texture2D s_activeCursorTexture;
        protected static Vector2 s_activeHotspot;

         public static void ResetMouseCursor()
        {
            // Change mouse cursor back to most recent settings
            Cursor.SetCursor(s_activeCursorTexture, s_activeHotspot, CursorMode.Auto);
        }

        public override void OnEnter()
        {
            Texture2D cursorTexture = m_CursorTexture.Value;
            Vector2 hotSpot = m_HotSpot.Value;

            Cursor.SetCursor(cursorTexture, hotSpot, CursorMode.Auto);

            s_activeCursorTexture = cursorTexture;
            s_activeHotspot = hotSpot;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("{0}", m_CursorTexture);
        }
    }
}