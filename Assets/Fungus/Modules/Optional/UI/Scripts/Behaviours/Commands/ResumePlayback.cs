﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Playback",
                 "Resume playback.")]
    [AddComponentMenu("Fungus/Commands/Playback/Resume Playback")]
    public class ResumePlayback : Command
    {
        [Tooltip("The target playback controller")]
        [CheckNotNullData]
        public PlaybackControllerData m_PlaybackController = new PlaybackControllerData();

        public override void OnEnter()
        {
            PlaybackController playbackController = m_PlaybackController.Value;

            playbackController.ResumePlayback();

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_PlaybackController);
        }
    }

}
