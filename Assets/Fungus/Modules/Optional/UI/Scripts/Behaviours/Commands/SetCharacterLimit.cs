﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Input Field",
                 "Set the character limit of an input field.")]
    [AddComponentMenu("Fungus/Commands/UI/Input Field/Set Character Limit")]
    public class SetCharacterLimit : Command
    {
        [Tooltip("The target input field")]
        [CheckNotNullData]
        public InputFieldData m_InputField = new InputFieldData();

        [Tooltip("The max number of characters that can be entered into the target input field")]
        public IntegerData m_CharacterLimit = new IntegerData(1);

        public override void OnEnter()
        {
            InputField inputField = m_InputField.Value;
            int characterLimit = m_CharacterLimit.Value;

            inputField.characterLimit = characterLimit;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_InputField, m_CharacterLimit);
        }
    }

}