﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Dropdown",
                 "Set the selected image of a dropdown.")]
    [AddComponentMenu("Fungus/Commands/UI/Dropdown/Set Dropdown Selected Image")]
    public class SetDropdownSelectedImage : Command
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The text of the dropdown option to select")]
        public SpriteData m_Value = new SpriteData();

        public override void OnEnter()
        {
            Dropdown dropdown = m_Dropdown.Value;
            Sprite value = m_Value.Value;

            dropdown.value = dropdown.options.FindIndex(i => i.image == value);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Dropdown, m_Value);
        }
    }

}