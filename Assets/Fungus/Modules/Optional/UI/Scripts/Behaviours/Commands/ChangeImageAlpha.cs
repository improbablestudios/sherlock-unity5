﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Image",
                 "Change the alpha of an image.")]
    [AddComponentMenu("Fungus/Commands/UI/Image/Change Image Alpha")]
    public class ChangeImageAlpha : DOTweenCommand
    {
        [Tooltip("The target image")]
        [FormerlySerializedAs("_image")]
        [CheckNotNullData]
        public ImageData m_Image = new ImageData();

        [Tooltip("The alpha to change to")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_alpha")]
        public FloatData m_Alpha = new FloatData();

        public override Tween GetTween()
        {
            Image image = m_Image.Value;
            float duration = m_Duration.Value;
            float alpha = m_Alpha.Value;
            
            return image.DOFade(alpha, duration);
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Image, m_Alpha) + GetDurationSummary(m_Duration);
        }
    }

}