﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Dropdown",
                 "Set the selected text of a dropdown.")]
    [AddComponentMenu("Fungus/Commands/UI/Dropdown/Set Dropdown Selected Text")]
    public class SetDropdownSelectedText : Command
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The text of the dropdown option to select")]
        public StringData m_Value = new StringData();

        public override void OnEnter()
        {
            Dropdown dropdown = m_Dropdown.Value;
            string value = m_Value.Value;

            dropdown.value = dropdown.options.FindIndex(i => i.text == value);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Dropdown, m_Value);
        }
    }

}