﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Dropdown",
                 "Set the selected index of a dropdown.")]
    [AddComponentMenu("Fungus/Commands/UI/Dropdown/Set Dropdown Selected Index")]
    public class SetDropdownSelectedIndex : Command
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The index of the dropdown option to select")]
        [DataMin(-1)]
        public IntegerData m_Value = new IntegerData();

        public override void OnEnter()
        {
            Dropdown dropdown = m_Dropdown.Value;
            int value = m_Value.Value;

            dropdown.value = value;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Dropdown, m_Value);
        }
    }

}