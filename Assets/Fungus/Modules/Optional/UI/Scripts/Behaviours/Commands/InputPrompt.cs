﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using System;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Prompt the user to enter text.")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Input Prompt")]
    public class InputPrompt : GenericMenuPrompt, IConnectNodes, ILocalizable
    {
        [Tooltip("The target input menu")]
        [ComponentDataField("<Default>", true, typeof(InputMenu))]
        public InputMenuData m_InputMenu = new InputMenuData();

        [Tooltip("The placeholder text to display in the input menu")]
        [Localize]
        public StringData m_PlaceholderText = new StringData();

        [Tooltip("The type of content the user should enter in the input menu")]
        public InputFieldContentTypeData m_ContentType = new InputFieldContentTypeData();

        [Tooltip("How many characters must be entered into the input field before input can be submitted")]
        [DataMin(0)]
        public IntegerData m_CharacterMinimum = new IntegerData();

        [Tooltip("How many characters the input field is limited to. 0 = infinite")]
        [DataMin(0)]
        public IntegerData m_CharacterLimit = new IntegerData();

        [Tooltip("The block to execute when this option is selected")]
        [FormerlySerializedAs("_targetBlock")]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if this block should pause, stop, or continue executing commands, or wait until the called target block finishes")]
        [FormerlySerializedAs("_targetBlock")]
        [CheckNotNullData]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        public override VariableData GetMenuData()
        {
            return m_InputMenu;
        }

        public override Menu GetMenu()
        {
            Menu optionMenu = m_InputMenu.Value;

            if (optionMenu == null)
            {
                optionMenu = InputMenu.DefaultInputMenu;
            }

            return optionMenu;
        }
        
        public override void OnEnter()
        {
            InputMenu inputMenu = GetMenu() as InputMenu;
            InputField.ContentType contentType = m_ContentType.Value;
            int characterMinimum = m_CharacterMinimum.Value;
            int characterLimit = m_CharacterLimit.Value;
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            SetupMenu();

            InputState inputState = new InputState(GetPlaceholderLocalizedDictionary(), GetFormattedPlaceholderText(), contentType, characterMinimum, characterLimit);

            inputMenu.DisplayMenuItem(inputState);

            inputMenu.OnSubmit.AddListener(ExecuteTargetBlock);

            Continue();
        }

        public void ExecuteTargetBlock(string returnValue)
        {
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            if (m_ReturnVariable != null)
            {
                m_ReturnVariable.Value = returnValue;
            }

            this.CallBlock(targetBlock, callMode);
        }

        public string GetFormattedPlaceholderText()
        {
            return FormatString(m_PlaceholderText.Value);
        }

        public Dictionary<string, string> GetPlaceholderLocalizedDictionary()
        {
            Dictionary<string, string> localizedDictionary = null;
            if (m_PlaceholderText.VariableDataType == VariableDataType.Value)
            {
                localizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_PlaceholderText.Value, base.FormatString);
            }
            else if (m_PlaceholderText.VariableDataType == VariableDataType.Variable)
            {
                localizedDictionary = m_PlaceholderText.Variable.GetLocalizedDictionary(FormatString);
            }

            return localizedDictionary;
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_ContentType) || propertyPath == nameof(m_ReturnVariable))
            {
                if (m_ContentType.IsConstant)
                {
                    if (m_ReturnVariable != null)
                    {
                        InputField.ContentType contentType = m_ContentType.Value;
                        Type type = null;
                        if (contentType == InputField.ContentType.DecimalNumber)
                        {
                            type = typeof(float);
                        }
                        if (contentType == InputField.ContentType.IntegerNumber || contentType == InputField.ContentType.Pin)
                        {
                            type = typeof(int);
                        }
                        if (contentType == InputField.ContentType.Name)
                        {
                            type = typeof(string);
                        }
                        if (type != null && !m_ReturnVariable.ValueType.IsAssignableFrom(type))
                        {
                            return string.Format("Content Type '{0}' and Return Variable type '{1}' don't match", type.Name, FlowchartItemInfoAttribute.GetInfo(m_ReturnVariable.GetType()).GetName(m_ReturnVariable.GetType()));
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath != nameof(m_InputMenu) && Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}
