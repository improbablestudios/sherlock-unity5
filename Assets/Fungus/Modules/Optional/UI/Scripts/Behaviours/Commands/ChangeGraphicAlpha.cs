﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Image",
                 "Change the alpha of an graphic (image, text, etc).")]
    [AddComponentMenu("Fungus/Commands/UI/Graphic/Change Graphic Alpha")]
    public class ChangeGraphicAlpha : DOTweenCommand
    {
        [Tooltip("The target graphic")]
        [CheckNotNullData]
        public GraphicData m_Graphic = new GraphicData();

        [Tooltip("The alpha to change to")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_alpha")]
        public FloatData m_Alpha = new FloatData();

        public override Tween GetTween()
        {
            Graphic graphic = m_Graphic.Value;
            float duration = m_Duration.Value;
            float alpha = m_Alpha.Value;

            return graphic.DOFade(alpha, duration);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Graphic, m_Alpha) + GetDurationSummary(m_Duration);
        }
    }

}