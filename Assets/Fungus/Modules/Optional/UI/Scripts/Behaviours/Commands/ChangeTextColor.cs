﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Text",
                 "Change the color of text.")]
    [AddComponentMenu("Fungus/Commands/UI/Text/Change Text Color")]
    public class ChangeTextColor : DOTweenCommand
    {
        [Tooltip("The target text")]
        [FormerlySerializedAs("_text")]
        [CheckNotNullData]
        public TextData m_Text = new TextData();

        [Tooltip("The color to change to")]
        [FormerlySerializedAs("_color")]
        public ColorData m_Color = new ColorData();

        public override Tween GetTween()
        {
            Text text = m_Text.Value;
            float duration = m_Duration.Value;
            Color color = m_Color.Value;

            return text.DOColor(color, duration);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Text, m_Color) + GetDurationSummary(m_Duration);
        }
    }

}