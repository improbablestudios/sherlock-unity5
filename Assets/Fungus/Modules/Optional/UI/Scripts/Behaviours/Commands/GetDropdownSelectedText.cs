﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Dropdown",
                 "Get the text of the option currently selected in the dropdown menu.")]
    [AddComponentMenu("Fungus/Commands/UI/Dropdown/Get Dropdown Selected Text")]
    public class GetDropdownSelectedText : ReturnValueCommand<string>
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetReturnValue()
        {
            Dropdown dropdown = m_Dropdown.Value;

            if (dropdown.value < dropdown.options.Count)
            {
                return dropdown.options[dropdown.value].text;
            }

            return "";
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" value", m_ReturnVariable, m_Dropdown);
        }
    }

}