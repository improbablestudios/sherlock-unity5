﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.UIUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Add an option to an option menu")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Option")]
    public class Option : OptionMenuCommand, IConnectNodes, ILocalizable
    {
        [Tooltip("The text to display on the menu button")]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData("");

        [Tooltip("The icon to display on the menu button")]
        [FormerlySerializedAs("_icon")]
        public SpriteData m_Icon = new SpriteData();

        [Tooltip("The block to execute when this option is selected")]
        [FormerlySerializedAs("_targetBlock")]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if this block should pause, stop, or continue executing commands, or wait until the called target block finishes")]
        [FormerlySerializedAs("_targetBlock")]
        [CheckNotNullData]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);

        [Tooltip("The variable to store the result in")]
        public IntegerVariable m_ReturnVariable;

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        protected int m_displayIndex;

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            OptionMenu optionMenu = GetOptionMenu();
            Sprite icon = m_Icon.Value;
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            string optionText = GetFormattedOptionText();
            Dictionary<string, string> optionLocalizedDictionary = GetOptionLocalizedDictionary();

            OptionState optionState = new OptionState(optionLocalizedDictionary, optionText, icon);

            m_displayIndex = optionMenu.DisplayMenuItem(optionState);

            optionMenu.OnSubmit.AddListener(ExecuteTargetBlock);

            Continue();
        }

        public void ExecuteTargetBlock(int returnValue)
        {
            if (m_displayIndex == returnValue)
            {
                Block targetBlock = m_TargetBlock.Value;
                CallMode callMode = m_CallMode.Value;

                if (m_ReturnVariable != null)
                {
                    m_ReturnVariable.Value = returnValue;
                }

                this.CallBlock(targetBlock, callMode);
            }
        }

        public string GetFormattedOptionText()
        {
            return FormatString(m_Text.Value);
        }

        public Dictionary<string, string> GetOptionLocalizedDictionary()
        {
            Dictionary<string, string> optionLocalizedDictionary = null;
            if (m_Text.VariableDataType == VariableDataType.Value)
            {
                optionLocalizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_Text.Value, base.FormatString);
            }
            else if (m_Text.VariableDataType == VariableDataType.Variable)
            {
                optionLocalizedDictionary = m_Text.Variable.GetLocalizedDictionary(FormatString);
            }

            return optionLocalizedDictionary;
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" : {1}", m_Text, m_TargetBlock);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Icon))
            {
                if ((m_OptionMenu.IsConstant && m_OptionMenu.Value != null) && !m_OptionMenu.Value.CanDisplayIcon())
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}
