﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Canvas",
                 "Set the sort order of a canvas.")]
    [AddComponentMenu("Fungus/Commands/UI/Canvas/Set Canvas Sort Order")]
    public class SetCanvasSortOrder : Command
    {
        [Tooltip("The target canvas")]
        [FormerlySerializedAs("_canvas")]
        [CheckNotNullData]
        public CanvasData m_Canvas = new CanvasData();

        [Tooltip("The new sorting order")]
        [FormerlySerializedAs("_sortingOrder")]
        public IntegerData m_SortingOrder = new IntegerData();

        public override void OnEnter()
        {
            Canvas canvas = m_Canvas.Value;
            
            canvas.sortingOrder = m_SortingOrder.Value;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Canvas, m_SortingOrder);
        }
    }

}