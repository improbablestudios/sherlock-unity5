﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Toggle",
                 "Set the state of a toggle.")]
    [AddComponentMenu("Fungus/Commands/UI/Toggle/Set Toggle Value")]
    public class SetToggleValue : Command
    {
        [Tooltip("The target toggle")]
        [CheckNotNullData]
        public ToggleData m_Toggle = new ToggleData();

        [Tooltip("The state the toggle will be set to")]
        [FormerlySerializedAs("m_IsOn")]
        public BooleanData m_Value = new BooleanData(true);

        public override void OnEnter()
        {
            Toggle toggle = m_Toggle.Value;
            bool value = m_Value.Value;

            toggle.isOn = value;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Toggle, m_Value);
        }
    }

}