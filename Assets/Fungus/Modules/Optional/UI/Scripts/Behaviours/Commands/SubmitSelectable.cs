﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using UnityEngine.EventSystems;

namespace Fungus.Commands
{

    [CommandInfo("UI/Selectable",
                 "Submit a selectable.")]
    [AddComponentMenu("Fungus/Commands/UI/Selectable/Submit Selectable")]
    public class SubmitSelectable : Command
    {
        [Tooltip("The target selectable")]
        [CheckNotNullData]
        public SelectableData m_Selectable = new SelectableData();

        public override void OnEnter()
        {
            Selectable selectable = m_Selectable.Value;
            
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            ExecuteEvents.Execute(selectable.gameObject, pointer, ExecuteEvents.submitHandler);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Selectable);
        }
    }

}
