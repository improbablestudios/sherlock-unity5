﻿using UnityEngine;
using System.Collections;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    public abstract class OptionMenuCommand : Command
    {
        [Tooltip("The target option menu")]
        [ComponentDataField("<Default>", true, typeof(OptionMenu))]
        public OptionMenuData m_OptionMenu = new OptionMenuData();

        public OptionMenu GetOptionMenu()
        {
            OptionMenu optionMenu = m_OptionMenu.Value;

            if (optionMenu == null)
            {
                optionMenu = OptionMenu.DefaultOptionMenu;
            }

            return optionMenu;
        }

    }

}