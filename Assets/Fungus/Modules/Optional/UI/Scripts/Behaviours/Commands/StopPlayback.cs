﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Playback",
                 "Stop playback.")]
    [AddComponentMenu("Fungus/Commands/Playback/Stop Playback")]
    public class StopPlayback : Command
    {
        [Tooltip("The target playback controller")]
        [CheckNotNullData]
        public PlaybackControllerData m_PlaybackController = new PlaybackControllerData();

        public override void OnEnter()
        {
            PlaybackController playbackController = m_PlaybackController.Value;

            playbackController.SkipPlayback();

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_PlaybackController);
        }
    }

}
