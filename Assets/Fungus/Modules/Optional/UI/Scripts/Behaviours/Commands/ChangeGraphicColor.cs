﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Image",
                 "Change the color of an graphic.")]
    [AddComponentMenu("Fungus/Commands/UI/Graphic/Change Graphic Color")]
    public class ChangeGraphicColor : DOTweenCommand
    {
        [Tooltip("The target graphic")]
        [CheckNotNullData]
        public GraphicData m_Graphic = new GraphicData();

        [Tooltip("The color to change to")]
        public ColorData m_Color = new ColorData();

        public override Tween GetTween()
        {
            Graphic graphic = m_Graphic.Value;
            float duration = m_Duration.Value;
            Color color = m_Color.Value;

            return graphic.DOColor(color, duration);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Graphic, m_Color) + GetDurationSummary(m_Duration);
        }
    }

}