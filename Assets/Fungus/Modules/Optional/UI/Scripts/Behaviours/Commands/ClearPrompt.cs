﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Clear and hide an active prompt")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Clear Prompt")]
    public class ClearPrompt : MenuCommand
    {
        public override void OnEnter()
        {
            Menu menu = m_Menu.Value;

            menu.ClearMenu(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}