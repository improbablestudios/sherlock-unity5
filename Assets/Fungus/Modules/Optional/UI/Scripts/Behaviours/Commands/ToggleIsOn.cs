﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Toggle",
                 "Check whether or not a toggle is on.")]
    [AddComponentMenu("Fungus/Commands/UI/Toggle/Toggle Is On")]
    public class ToggleIsOn : ReturnValueCommand<bool>
    {
        [Tooltip("The target toggle")]
        [CheckNotNullData]
        public ToggleData m_Toggle = new ToggleData();

        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            Toggle toggle = m_Toggle.Value;

            return toggle.isOn;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" text", m_ReturnVariable, m_Toggle);
        }
    }

}