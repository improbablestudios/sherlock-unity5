﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using System;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Prompt the user to select an option.")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Option Prompt")]
    public class OptionPrompt : GenericMenuPrompt, ILocalizable
    {
        [Tooltip("The target option menu")]
        [ComponentDataField("<Default>", true, typeof(OptionMenu))]
        public OptionMenuData m_OptionMenu = new OptionMenuData();

        public override VariableData GetMenuData()
        {
            return m_OptionMenu;
        }

        public override Menu GetMenu()
        {
            Menu optionMenu = m_OptionMenu.Value;

            if (optionMenu == null)
            {
                optionMenu = OptionMenu.DefaultOptionMenu;
            }

            return optionMenu;
        }

        public override void OnEnter()
        {
            SetupMenu();

            Continue();
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath != nameof(m_OptionMenu) && Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}
