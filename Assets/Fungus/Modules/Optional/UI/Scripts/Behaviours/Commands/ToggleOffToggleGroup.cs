﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Toggle",
                 "Toggle off all toggles in a group.")]
    [AddComponentMenu("Fungus/Commands/UI/Toggle/Toggle Off Toggle Group")]
    public class ToggleOffToggleGroup : Command
    {
        [Tooltip("The target toggle group")]
        [CheckNotNullData]
        public ToggleGroupData m_ToggleGroup = new ToggleGroupData();

        public override void OnEnter()
        {
            ToggleGroup toggleGroup = m_ToggleGroup.Value;

            toggleGroup.SetAllTogglesOff();

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_ToggleGroup);
        }
    }

}