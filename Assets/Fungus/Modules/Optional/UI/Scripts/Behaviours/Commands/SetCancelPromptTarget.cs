﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.UIUtilities;
using System;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Make a menu's cancel button execute a target block when pressed")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Set Cancel Prompt Target")]
    public class SetCancelPromptTarget : OptionMenuCommand, IConnectNodes, ILocalizable
    {
        [Tooltip("The block to execute when this option is selected")]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if this block should pause, stop, or continue executing commands, or wait until the called target block finishes")]
        [CheckNotNullData]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);
        
        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            OptionMenu optionMenu = GetOptionMenu();
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;
            
            optionMenu.OnCancel.AddListener(ExecuteTargetBlock);

            Continue();
        }

        public void ExecuteTargetBlock()
        {
            OptionMenu optionMenu = GetOptionMenu();
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            optionMenu.OnCancel.RemoveListener(ExecuteTargetBlock);
            this.CallBlock(targetBlock, callMode);
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_TargetBlock);
        }
        
        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}
