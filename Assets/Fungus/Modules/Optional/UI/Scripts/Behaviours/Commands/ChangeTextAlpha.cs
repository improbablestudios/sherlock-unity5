﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Text",
                 "Change the alpha of text.")]
    [AddComponentMenu("Fungus/Commands/UI/Text/Change Text Alpha")]
    public class ChangeTextAlpha : DOTweenCommand
    {
        [Tooltip("The target text")]
        [FormerlySerializedAs("_text")]
        [CheckNotNullData]
        public TextData m_Text = new TextData();

        [Tooltip("The alpha to change to")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_alpha")]
        public FloatData m_Alpha = new FloatData();

        public override Tween GetTween()
        {
            Text text = m_Text.Value;
            float duration = m_Duration.Value;
            float alpha = m_Alpha.Value;
            
            return text.DOFade(alpha, duration);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Text, m_Alpha) + GetDurationSummary(m_Duration);
        }
    }

}