﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Selectable",
                 "Give a selectable focus.")]
    [AddComponentMenu("Fungus/Commands/UI/Selectable/Select Selectable")]
    public class SelectSelectable : Command
    {
        [Tooltip("The target selectable")]
        [FormerlySerializedAs("_selectable")]
        [CheckNotNullData]
        public SelectableData m_Selectable = new SelectableData();

        public override void OnEnter()
        {
            Selectable selectable = m_Selectable.Value;
            
            selectable.Select();

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Selectable);
        }
    }

}