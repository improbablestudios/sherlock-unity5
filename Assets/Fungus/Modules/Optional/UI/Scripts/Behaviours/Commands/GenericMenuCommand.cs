﻿using UnityEngine;
using System.Collections;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    public abstract class GenericMenuCommand : Command
    {
        public abstract VariableData GetMenuData();

        public abstract Menu GetMenu();
    }

}