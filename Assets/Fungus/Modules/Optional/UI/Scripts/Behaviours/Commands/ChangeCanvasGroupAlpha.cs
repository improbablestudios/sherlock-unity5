﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Canvas",
                 "Change the alpha of a canvas group.")]
    [AddComponentMenu("Fungus/Commands/UI/Canvas/Change Canvas Alpha")]
    public class ChangeCanvasGroupAlpha : DOTweenCommand
    {
        [Tooltip("The target canvas group")]
        [FormerlySerializedAs("_canvasGroup")]
        [CheckNotNullData]
        public CanvasGroupData m_CanvasGroup = new CanvasGroupData();

        [Tooltip("The alpha to change to")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_alpha")]
        public FloatData m_Alpha = new FloatData();

        public override Tween GetTween()
        {
            CanvasGroup canvasGroup = m_CanvasGroup.Value;
            float duration = m_Duration.Value;
            float alpha = m_Alpha.Value;
            
            return canvasGroup.DOFade(alpha, duration);
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_CanvasGroup, m_Alpha) + GetDurationSummary(m_Duration);
        }
    }

}