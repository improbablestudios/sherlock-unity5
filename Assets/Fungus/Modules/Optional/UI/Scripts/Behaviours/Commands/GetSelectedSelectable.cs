﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using UnityEngine.EventSystems;

namespace Fungus.Commands
{

    [CommandInfo("UI/Selectable",
                 "Get the selectable that is currently selected.")]
    [AddComponentMenu("Fungus/Commands/UI/Selectable/Get Selected Selectable")]
    public class GetSelectedSelectable : ReturnValueCommand<Selectable>
    {
        [Tooltip("The variable to store the result in")]
        public SelectableVariable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override Selectable GetReturnValue()
        {
            EventSystem currentEventSystem = EventSystem.current;
            if (currentEventSystem != null)
            {
                GameObject selectedGameObject = currentEventSystem.currentSelectedGameObject;
                if (selectedGameObject != null)
                {
                    return selectedGameObject.GetComponent<Selectable>();
                }
            }

            return null;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = currently selected selectable", m_ReturnVariable);
        }
    }

}