﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Slider",
                 "Change the value of a slider.")]
    [AddComponentMenu("Fungus/Commands/UI/Slider/Change Slider Value")]
    public class ChangeSliderValue : DOTweenCommand 
    {
        [Tooltip("The target slider")]
        [FormerlySerializedAs("_slider")]
        [CheckNotNullData]
        public SliderData m_Slider = new SliderData();

        [Tooltip("Fill amount to assign to the slider object")]
        [FormerlySerializedAs("_value")]
        public FloatData m_Value = new FloatData();

        public override Tween GetTween()
        {
            Slider slider = m_Slider.Value;
            float value = m_Value.Value;
            float duration = m_Duration.Value;
            
            return slider.DOValue(value, duration);
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Slider, m_Value) + GetDurationSummary(m_Duration);
        }
    }

}