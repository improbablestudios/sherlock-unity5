﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Input Field",
                 "Activate an input field to begin processing keyboard events.")]
    [AddComponentMenu("Fungus/Commands/UI/Input Field/Activate Input Field")]
    public class ActivateInputField : Command
    {
        [Tooltip("The target input field")]
        [FormerlySerializedAs("_inputField")]
        [CheckNotNullData]
        public InputFieldData m_InputField = new InputFieldData();

        [Tooltip("Start with all text selected")]
        [FormerlySerializedAs("_selectAllText")]
        public BooleanData m_SelectAllText = new BooleanData(true);

        public override void OnEnter()
        {
            InputField inputField = m_InputField.Value;
            bool selectAllText = m_SelectAllText.Value;

            inputField.ActivateInputField();

            if (selectAllText)
            {
                SelectAll(inputField);
            }

            Continue();
        }

        public void SelectAll(InputField inputField)
        {
            Event keyboardEvent = Event.KeyboardEvent(KeyCode.A.ToString());
            keyboardEvent.modifiers = SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX ? EventModifiers.Command : EventModifiers.Control;
            inputField.ProcessEvent(keyboardEvent);
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_InputField);
        }
    }

}