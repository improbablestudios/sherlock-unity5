﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Slider",
                 "Get the value of a slider.")]
    [AddComponentMenu("Fungus/Commands/UI/Slider/Get Slider Value")]
    public class GetSliderValue : NumericReturnValueCommand
    {
        [Tooltip("The target slider")]
        [CheckNotNullData]
        public SliderData m_Slider = new SliderData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override object GetReturnValue()
        {
            Slider slider = m_Slider.Value;

            return slider.value;
        }
        
        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" value", m_ReturnVariable, m_Slider);
        }
    }

}