﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;
using UnityEngine.UI;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Set the default selection of a menu")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Set Default Menu Selection")]
    public class SetDefaultMenuSelection : GenericMenuCommand
    {
        [Tooltip("The target option menu")]
        [ComponentDataField("<Default>", true)]
        public MenuData m_Menu = new MenuData();

        [Tooltip("The new default selection")]
        public SelectableData m_DefaultSelection = new SelectableData();
        
        protected void SetupMenu()
        {
            Menu menu = GetMenu();
            Selectable defaultSelection = m_DefaultSelection.Value;

            menu.SelectByDefault = defaultSelection;
        }
        
        public override VariableData GetMenuData()
        {
            return m_Menu;
        }

        public override Menu GetMenu()
        {
            Menu optionMenu = m_Menu.Value;

            if (optionMenu == null)
            {
                optionMenu = InputMenu.DefaultInputMenu;
            }

            return optionMenu;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" Default Selection = \"{1}\"", m_Menu, m_DefaultSelection);
        }
    }

}
