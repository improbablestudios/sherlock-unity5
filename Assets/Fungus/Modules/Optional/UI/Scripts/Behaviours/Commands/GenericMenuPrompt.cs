﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.ReflectionUtilities;
using UnityEngine.Serialization;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{
    
    public abstract class GenericMenuPrompt : GenericMenuCommand, ILocalizable
    {
        [Tooltip("The title text")]
        [Localize]
        public StringData m_TitleText = new StringData("");

        [Tooltip("The message text")]
        [StringDataArea(3, 10)]
        [Localize]
        public StringData m_MessageText = new StringData("");

        [Tooltip("The icon")]
        public SpriteData m_Icon = new SpriteData();

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }
        
        protected void SetupMenu()
        {
            Menu menu = GetMenu();
            Sprite icon = m_Icon.Value;

            string titleText = GetFormattedTitleText();
            Dictionary<string, string> titleLocalizedDictionary = GetTitleLocalizedDictionary();

            string messageText = GetFormattedMessageText();
            Dictionary<string, string> messageLocalizedDictionary = GetMessageLocalizedDictionary();

            menu.SetPrompt(new MenuPromptState(titleLocalizedDictionary, titleText, messageLocalizedDictionary, messageText, icon));
        }

        public string GetFormattedTitleText()
        {
            return FormatString(m_TitleText.Value);
        }

        public Dictionary<string, string> GetTitleLocalizedDictionary()
        {
            Dictionary<string, string> localizedDictionary = null;
            if (m_TitleText.VariableDataType == VariableDataType.Value)
            {
                localizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_TitleText.Value, base.FormatString);
            }
            else if (m_TitleText.VariableDataType == VariableDataType.Variable)
            {
                localizedDictionary = m_TitleText.Variable.GetLocalizedDictionary(FormatString);
            }

            return localizedDictionary;
        }

        public string GetFormattedMessageText()
        {
            return FormatString(m_MessageText.Value);
        }

        public Dictionary<string, string> GetMessageLocalizedDictionary()
        {
            Dictionary<string, string> localizedDictionary = null;
            if (m_MessageText.VariableDataType == VariableDataType.Value)
            {
                localizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_MessageText.Value, base.FormatString);
            }
            else if (m_MessageText.VariableDataType == VariableDataType.Variable)
            {
                localizedDictionary = m_MessageText.Variable.GetLocalizedDictionary(FormatString);
            }

            return localizedDictionary;
        }
        
        public override string GetSummary()
        {
            string titleText = m_TitleText.Value;
            string messageText = m_MessageText.Value;
            string menuText = "";
            List<string> menuTexts = new List<string>();

            if (!string.IsNullOrEmpty(titleText))
            {
                titleText = string.Format("\"{0}\"", m_TitleText);
                menuTexts.Add(titleText);
            }

            if (!string.IsNullOrEmpty(messageText))
            {
                messageText = string.Format("\"{0}\"", m_MessageText);
                menuTexts.Add(messageText);
            }

            menuText = string.Join(" : ", menuTexts.ToArray());

            return menuText;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            VariableData menuData = GetMenuData();
            Menu menu = menuData.Value as Menu;
            if (propertyPath == nameof(m_TitleText))
            {
                if ((menuData.IsConstant && menu != null) && menu.TitleText == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_MessageText))
            {
                if ((menuData.IsConstant && menu != null) && menu.MessageText == null)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Icon))
            {
                if ((menuData.IsConstant && menu != null) && menu.IconImage == null)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}
