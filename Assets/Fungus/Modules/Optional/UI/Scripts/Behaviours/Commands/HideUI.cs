﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI",
                 "Hide a ui with a transition.")]
    [AddComponentMenu("Fungus/Commands/UI/Hide UI")]
    public class HideUI : Command
    {
        [Tooltip("The target UI")]
        [CheckNotNullData]
        public UIConstructData m_UI = new UIConstructData();

        public BooleanData m_DisableAfterHiding = new BooleanData(true);

        public override void OnEnter()
        {
            UIConstruct ui = m_UI.Value;
            bool disableAfterHiding = m_DisableAfterHiding.Value;

            ui.Hide(disableAfterHiding, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_UI);
        }
    }

}