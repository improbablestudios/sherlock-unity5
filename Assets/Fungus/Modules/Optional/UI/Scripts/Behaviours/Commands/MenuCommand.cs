﻿using UnityEngine;
using System.Collections;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class MenuCommand : Command
    {
        [Tooltip("The target menu")]
        public MenuData m_Menu = new MenuData();
    }

}