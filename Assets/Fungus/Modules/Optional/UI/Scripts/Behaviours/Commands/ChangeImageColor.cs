﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Image",
                 "Change the color of an image.")]
    [AddComponentMenu("Fungus/Commands/UI/Image/Change Image Color")]
    public class ChangeImageColor : DOTweenCommand
    {
        [Tooltip("The target image")]
        [FormerlySerializedAs("_image")]
        [CheckNotNullData]
        public ImageData m_Image = new ImageData();

        [Tooltip("The color to change to")]
        [FormerlySerializedAs("_color")]
        public ColorData m_Color = new ColorData();

        public override Tween GetTween()
        {
            Image image = m_Image.Value;
            float duration = m_Duration.Value;
            Color color = m_Color.Value;
            
            return image.DOColor(color, duration);
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_Image, m_Color) + GetDurationSummary(m_Duration);
        }
    }

}