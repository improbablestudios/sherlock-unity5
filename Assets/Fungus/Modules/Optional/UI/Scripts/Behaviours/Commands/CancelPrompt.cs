﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("UI/Prompt",
                 "Cancel and hide an active prompt")]
    [AddComponentMenu("Fungus/Commands/UI/Prompt/Cancel Prompt")]
    public class CancelPrompt : MenuCommand
    {
        public override void OnEnter()
        {
            Menu menu = m_Menu.Value;

            menu.CancelMenu(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}