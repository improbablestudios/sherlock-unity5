﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("UI/Dropdown",
                 "Get the index of the option currently selected in the dropdown menu.")]
    [AddComponentMenu("Fungus/Commands/UI/Dropdown/Get Dropdown Selected Index")]
    public class GetDropdownSelectedIndex : ReturnValueCommand<int>
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The variable to store the result in")]
        public IntegerVariable m_ReturnVariable;

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override int GetReturnValue()
        {
            Dropdown dropdown = m_Dropdown.Value;

            return dropdown.value;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" value", m_ReturnVariable, m_Dropdown);
        }
    }

}