﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Playback",
        "The block will execute when playback is replayed.")]
    [AddComponentMenu("Fungus/Event Handlers/Playback/Playback Replayed")]
    public class PlaybackReplayed : UnityEventHandler
    {
        public PlaybackControllerData m_PlaybackController = new PlaybackControllerData();

        protected override UnityEvent GetUnityEvent()
        {
            PlaybackController playbackController = m_PlaybackController.Value as PlaybackController;

            if (playbackController != null)
            {
                return playbackController.OnReplayed;
            }
            return null;
        }

        public override string GetPropertyError(string propertyName)
        {
            if (propertyName == nameof(m_PlaybackController))
            {
                if (m_PlaybackController == null)
                {
                    return "No Playback Controller selected";
                }
            }

            return base.GetPropertyError(propertyName);
        }
    }
}