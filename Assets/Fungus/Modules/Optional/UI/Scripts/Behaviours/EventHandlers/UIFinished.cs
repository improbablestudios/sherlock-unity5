﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("UI",
                      "This block will execute when the ui finishes.")]
    [AddComponentMenu("Fungus/Event Handlers/UI/UI Finished")]
    public class UIFinished : UnityEventHandler
    {
        [Tooltip("The target ui")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_Counter")]
        public UIConstructData m_UI = new UIConstructData();

        protected override UnityEvent GetUnityEvent()
        {
            UIConstruct ui = m_UI.Value;

            if (ui != null)
            {
                return ui.OnFinished;
            }

            return null;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" finished", m_UI);
        }
    }

}