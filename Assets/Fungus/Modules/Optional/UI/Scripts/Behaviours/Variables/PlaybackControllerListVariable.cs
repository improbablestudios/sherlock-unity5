﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/UI Construct List Variable")]
    public class PlaybackControllerListVariable : Variable<List<PlaybackController>>
    {
    }

    [Serializable]
    public class PlaybackControllerListData : VariableData<List<PlaybackController>>
    {
        public PlaybackControllerListData()
        {
            m_DataVal = new List<PlaybackController>();
        }

        public PlaybackControllerListData(List<PlaybackController> v)
        {
            m_DataVal = v;
        }
    }

}
