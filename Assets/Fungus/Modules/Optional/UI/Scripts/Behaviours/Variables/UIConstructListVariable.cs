﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/UI Construct List Variable")]
    public class UIConstructListVariable : Variable<List<UIConstruct>>
    {
    }

    [Serializable]
    public class UIConstructListData : VariableData<List<UIConstruct>>
    {
        public UIConstructListData()
        {
            m_DataVal = new List<UIConstruct>();
        }

        public UIConstructListData(List<UIConstruct> v)
        {
            m_DataVal = v;
        }
    }

}
