﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Menu Variable")]
    public class MenuVariable : Variable<Menu>
    {
    }

    [Serializable]
    public class MenuData : VariableData<Menu>
    {
        public MenuData()
        {
        }

        public MenuData(Menu v)
        {
            m_DataVal = v;
        }
    }

}