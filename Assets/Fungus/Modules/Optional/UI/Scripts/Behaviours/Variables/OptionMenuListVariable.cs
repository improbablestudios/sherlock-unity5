﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Option Menu List Variable")]
    public class OptionMenuListVariable : Variable<List<OptionMenu>>
    {
    }

    [Serializable]
    public class OptionMenuListData : VariableData<List<OptionMenu>>
    {
        public OptionMenuListData()
        {
            m_DataVal = new List<OptionMenu>();
        }

        public OptionMenuListData(List<OptionMenu> v)
        {
            m_DataVal = v;
        }
    }

}