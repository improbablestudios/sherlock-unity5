﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/UI Construct Variable")]
    public class UIConstructVariable : Variable<UIConstruct>
    {
    }

    [Serializable]
    public class UIConstructData : VariableData<UIConstruct>
    {
        public UIConstructData()
        {
        }

        public UIConstructData(UIConstruct v)
        {
            m_DataVal = v;
        }
    }

}
