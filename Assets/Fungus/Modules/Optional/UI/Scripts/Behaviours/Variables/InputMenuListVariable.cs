﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Input Menu List Variable")]
    public class InputMenuListVariable : Variable<List<InputMenu>>
    {
    }

    [Serializable]
    public class InputMenuListData : VariableData<List<InputMenu>>
    {
        public InputMenuListData()
        {
            m_DataVal = new List<InputMenu>();
        }

        public InputMenuListData(List<InputMenu> v)
        {
            m_DataVal = v;
        }
    }

}