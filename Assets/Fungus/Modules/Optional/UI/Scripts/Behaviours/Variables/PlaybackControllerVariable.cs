﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Playback Controller Variable")]
    public class PlaybackControllerVariable : Variable<PlaybackController>
    {
    }

    [Serializable]
    public class PlaybackControllerData : VariableData<PlaybackController>
    {
        public PlaybackControllerData()
        {
        }

        public PlaybackControllerData(PlaybackController v)
        {
            m_DataVal = v;
        }
    }

}
