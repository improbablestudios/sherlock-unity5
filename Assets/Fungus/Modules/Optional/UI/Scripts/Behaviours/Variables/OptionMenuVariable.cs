﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Option Menu Variable")]
    public class OptionMenuVariable : Variable<OptionMenu>
    {
    }

    [Serializable]
    public class OptionMenuData : VariableData<OptionMenu>
    {
        public OptionMenuData()
        {
        }

        public OptionMenuData(OptionMenu v)
        {
            m_DataVal = v;
        }
    }

}