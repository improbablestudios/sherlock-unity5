﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Menu List Variable")]
    public class MenuListVariable : Variable<List<Menu>>
    {
    }

    [Serializable]
    public class MenuListData : VariableData<List<Menu>>
    {
        public MenuListData()
        {
            m_DataVal = new List<Menu>();
        }

        public MenuListData(List<Menu> v)
        {
            m_DataVal = v;
        }
    }

}