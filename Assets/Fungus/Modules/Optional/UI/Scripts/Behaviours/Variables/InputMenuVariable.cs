﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Input Menu Variable")]
    public class InputMenuVariable : Variable<InputMenu>
    {
    }

    [Serializable]
    public class InputMenuData : VariableData<InputMenu>
    {
        public InputMenuData()
        {
        }

        public InputMenuData(InputMenu v)
        {
            m_DataVal = v;
        }
    }

}