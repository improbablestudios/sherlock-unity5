﻿#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeGraphicColor), true)]
    public class ChangeGraphicColorEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeGraphicColor t = target as ChangeGraphicColor;
            return nameof(t.m_Graphic);
        }

        public override void CopyButton()
        {
            ChangeGraphicColor t = target as ChangeGraphicColor;
            if (t.m_Graphic.IsConstant && t.m_Graphic.Value != null)
            {
                t.m_Color.Value = t.m_Graphic.Value.color;
            }
        }
    }

}
#endif