#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeTextAlpha), true)]
    public class ChangeTextAlphaEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeTextAlpha t = target as ChangeTextAlpha;
            return nameof(t.m_Text);
        }

        public override void CopyButton()
        {
            ChangeTextAlpha t = target as ChangeTextAlpha;
            if (t.m_Text.IsConstant && t.m_Text.Value != null)
            {
                t.m_Alpha.Value = t.m_Text.Value.color.a;
            }
        }
    }

}
#endif