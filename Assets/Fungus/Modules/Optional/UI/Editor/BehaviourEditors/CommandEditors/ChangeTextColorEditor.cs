#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeTextColor), true)]
    public class ChangeTextColorEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeTextColor t = target as ChangeTextColor;
            return nameof(t.m_Text);
        }

        public override void CopyButton()
        {
            ChangeTextColor t = target as ChangeTextColor;
            if (t.m_Text.IsConstant && t.m_Text.Value != null)
            {
                t.m_Color.Value = t.m_Text.Value.color;
            }
        }
    }

}
#endif