#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeSliderValue), true)]
    public class ChangeSliderValueEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeSliderValue t = target as ChangeSliderValue;
            return nameof(t.m_Slider);
        }

        public override void CopyButton()
        {
            ChangeSliderValue t = target as ChangeSliderValue;
            if (t.m_Slider.IsConstant && t.m_Slider.Value != null)
            {
                t.m_Value.Value = t.m_Slider.Value.value;
            }
        }
    }

}
#endif