#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeCanvasGroupAlpha), true)]
    public class ChangeCanvasGroupAlphaEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeCanvasGroupAlpha t = target as ChangeCanvasGroupAlpha;
            return nameof(t.m_CanvasGroup);
        }

        public override void CopyButton()
        {
            ChangeCanvasGroupAlpha t = target as ChangeCanvasGroupAlpha;
            if (t.m_CanvasGroup.IsConstant && t.m_CanvasGroup.Value != null)
            {
                t.m_Alpha.Value = t.m_CanvasGroup.Value.alpha;
            }
        }
    }
}
#endif