#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeImageAlpha), true)]
    public class ChangeImageAlphaEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeImageAlpha t = target as ChangeImageAlpha;
            return nameof(t.m_Image);
        }

        public override void CopyButton()
        {
            ChangeImageAlpha t = target as ChangeImageAlpha;
            if (t.m_Image.IsConstant && t.m_Image.Value != null)
            {
                t.m_Alpha.Value = t.m_Image.Value.color.a;
            }
        }
    }

}
#endif