#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeImageColor), true)]
    public class ChangeImageColorEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeImageColor t = target as ChangeImageColor;
            return nameof(t.m_Image);
        }

        public override void CopyButton()
        {
            ChangeImageColor t = target as ChangeImageColor;
            if (t.m_Image.IsConstant && t.m_Image.Value != null)
            {
                t.m_Color.Value = t.m_Image.Value.color;
            }
        }
    }

}
#endif