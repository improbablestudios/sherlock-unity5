﻿#if UNITY_EDITOR
using UnityEditor;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ChangeGraphicAlpha), true)]
    public class ChangeGraphicAlphaEditor : DOTweenCommandEditor
    {
        public override string GetTargetPropertyName()
        {
            ChangeGraphicAlpha t = target as ChangeGraphicAlpha;
            return nameof(t.m_Graphic);
        }

        public override void CopyButton()
        {
            ChangeGraphicAlpha t = target as ChangeGraphicAlpha;
            if (t.m_Graphic.IsConstant && t.m_Graphic.Value != null)
            {
                t.m_Alpha.Value = t.m_Graphic.Value.color.a;
            }
        }
    }

}
#endif