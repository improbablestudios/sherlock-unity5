using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    // This command is called "Call Method" because a) it's more descriptive than Send Message and we're already have
    // a Send Message command for sending messages to trigger block execution.
    [CommandInfo("Scripting",
                 "Call a named method on a game object using the GameObject.SendMessage() system.")]
    [AddComponentMenu("Fungus/Commands/Scripting/Call Method")]
    public class CallMethod : Command
    {
        [Tooltip("Target monobehavior which contains the method we want to call")]
        [FormerlySerializedAs("_targetObject")]
        [CheckNotNullData]
        public GameObjectData m_TargetObject = new GameObjectData();

        [Tooltip("Name of the method to call")]
        [FormerlySerializedAs("_methodName")]
        [CheckNotEmptyData]
        public StringData m_MethodName = new StringData("");

        [Tooltip("Delay (in seconds) before the method will be called")]
        [DataMin(0)]
        [FormerlySerializedAs("_delay")]
        public FloatData m_Delay = new FloatData();

        public override void OnEnter()
        {
            float delay = m_Delay.Value;
            
            if (delay == 0f)
            {
                CallTheMethod();
            }
            else
            {
                Invoke("CallTheMethod", delay);
            }

            Continue();
        }

        protected virtual void CallTheMethod()
        {
            GameObject targetObject = m_TargetObject.Value;
            string methodName = m_MethodName.Value;
            targetObject.SendMessage(methodName, SendMessageOptions.DontRequireReceiver);
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" : {1}", m_TargetObject, m_MethodName);
        }
    }

}