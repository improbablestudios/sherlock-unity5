using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using Object = System.Object;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Scripting",
                 "Write a debug log message to the debug console.")]
    [AddComponentMenu("Fungus/Commands/Scripting/Debug Log")]
    public class DebugLog : Command 
    {
        [Tooltip("Display type of debug log info")]
        [FormerlySerializedAs("logType")]
        public DebugLogType m_LogType;

        [Tooltip("Text to write to the debug log (Supports variable substitution, e.g. {$Myvar})")]
        [FormerlySerializedAs("_logMessage")]
        public StringData m_LogMessage = new StringData();

        [Tooltip("This object will be highlighted in the heirarchy when the debug log message is clicked in the console window")]
        [FormerlySerializedAs("_logMessage")]
        public ObjectData m_LogObject = new ObjectData();

        public override void OnEnter ()
        {
            string logMessage = m_LogMessage.Value;
            UnityEngine.Object obj = m_LogObject.Value;

            Block parentBlock = ParentBlock;

            string message = string.Format("'Log ({0}:{1})': {2}", parentBlock, CommandIndex, logMessage);

            GetFlowchart().Log(FlowchartLoggingOptions.Command, m_LogType, message, obj, true);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}: {1}", m_LogType.ToString().ToUpper(), m_LogMessage);
        }
    }

}