﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Scripting",
                 "Open a URL in the user's default browser.")]
    [AddComponentMenu("Fungus/Commands/Scripting/Open URL")]
    public class OpenURL : Command
    {
        [Tooltip("URL to open in the browser")]
        [FormerlySerializedAs("_url")]
        public StringData m_Url = new StringData();

        public override void OnEnter()
        {
            string url = m_Url.Value;

            Application.OpenURL(url);

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Url);
        }
    }

}