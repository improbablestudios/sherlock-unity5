﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Counter Warning Variable")]
    public class CounterWarningVariable : Variable<CounterWarning>
    {
    }

    [Serializable]
    public class CounterWarningData : VariableData<CounterWarning>
    {
        public CounterWarningData()
        {
        }

        public CounterWarningData(CounterWarning v)
        {
            m_DataVal = v;
        }
    }

}