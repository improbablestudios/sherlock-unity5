﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Counter Variable")]
    public class CounterVariable : Variable<Counter>
    {
    }

    [Serializable]
    public class CounterData : VariableData<Counter>
    {
        public CounterData()
        {
        }

        public CounterData(Counter v)
        {
            m_DataVal = v;
        }
    }

}