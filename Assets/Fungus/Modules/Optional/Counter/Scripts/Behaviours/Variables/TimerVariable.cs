﻿using System;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Timer Variable")]
    public class TimerVariable : Variable<Timer>
    {
    }

    [Serializable]
    public class TimerData : VariableData<Timer>
    {
        public TimerData()
        {
        }

        public TimerData(Timer v)
        {
            m_DataVal = v;
        }
    }

}