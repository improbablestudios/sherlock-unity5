﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Timer List Variable")]
    public class TimerListVariable : Variable<List<Timer>>
    {
    }

    [Serializable]
    public class TimerListData : VariableData<List<Timer>>
    {
        public TimerListData()
        {
            m_DataVal = new List<Timer>();
        }

        public TimerListData(List<Timer> v)
        {
            m_DataVal = v;
        }
    }

}