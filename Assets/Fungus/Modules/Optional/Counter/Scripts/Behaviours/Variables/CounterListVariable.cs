﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Counter List Variable")]
    public class CounterListVariable : Variable<List<Counter>>
    {
    }

    [Serializable]
    public class CounterListData : VariableData<List<Counter>>
    {
        public CounterListData()
        {
            m_DataVal = new List<Counter>();
        }

        public CounterListData(List<Counter> v)
        {
            m_DataVal = v;
        }
    }

}