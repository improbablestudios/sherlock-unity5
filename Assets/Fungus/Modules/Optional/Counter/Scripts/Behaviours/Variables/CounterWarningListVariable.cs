﻿using System;
using System.Collections.Generic;
using ImprobableStudios.UIUtilities;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Counter Warning List Variable")]
    public class CounterWarningListVariable : Variable<List<CounterWarning>>
    {
    }

    [Serializable]
    public class CounterWarningListData : VariableData<List<CounterWarning>>
    {
        public CounterWarningListData()
        {
            m_DataVal = new List<CounterWarning>();
        }

        public CounterWarningListData(List<CounterWarning> v)
        {
            m_DataVal = v;
        }
    }

}