﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{
    
    public abstract class CounterCommand : Command
    {
        [Tooltip("The target counter")]
        [FormerlySerializedAs("_counter")]
        [ObjectDataPopup("<Default>", true)]
        public CounterData m_Counter = new CounterData();

        protected override void Reset()
        {
            base.Reset();

            m_WaitUntilFinished.Value = true;
        }

        public Counter GetCounter()
        {
            Counter counter = m_Counter.Value as Counter;

            if (counter == null)
            {
                counter = Counter.DefaultCounter;
            }

            return counter;
        }

        public override string GetSummary()
        {
            string counterString = m_Counter.ToString();
            if (m_Counter.IsConstant && m_Counter.Value == null)
            {
                counterString = "<Default>";
            }
            return string.Format("\"{0}\"", counterString);
        }
    }
}
