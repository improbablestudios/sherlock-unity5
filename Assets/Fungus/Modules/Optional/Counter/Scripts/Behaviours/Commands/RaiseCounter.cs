﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Raise a counter until the target value is reached.")]
    [AddComponentMenu("Fungus/Commands/Counter/Raise Counter")]
    public class RaiseCounter : CounterCommand
    {
        [Tooltip("When this target value is reached the timer will be stopped and hidden")]
        [FormerlySerializedAs("_duration")]
        [FormerlySerializedAs("m_Duration")]
        public FloatData m_TargetValue = new FloatData(10f);

        [Tooltip("The counter will countdown from the max value")]
        [FormerlySerializedAs("_countdown")]
        public BooleanData m_Countdown = new BooleanData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float targetValue = m_TargetValue.Value;
            bool countdown = m_Countdown.Value;
            
            counter.RaiseCounter(targetValue, countdown,  null, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" until value reaches {0}", m_TargetValue);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}