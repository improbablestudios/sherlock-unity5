﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set a counter's min value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Min")]
    public class SetCounterMin : CounterCommand
    {
        [Tooltip("The min value the counter will count down to or count up from")]
        public FloatData m_MinValue = new FloatData(0f);

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float maxValue = m_MinValue.Value;

            counter.SetCounterMin(maxValue);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_MinValue);
        }
    }

}