﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set whether or not the counter will finish only when it is currently visible.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Only Finish If Visible")]
    public class SetCounterOnlyFinishIfVisible : CounterCommand
    {
        [Tooltip("If true, the counter will only finish when it is currently visible, otherwise, it will finish even if it is not visible")]
        public BooleanData m_OnlyFinishWhenVisible = new BooleanData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            bool onlyFinishWhenVisible = m_OnlyFinishWhenVisible.Value;

            counter.OnlyFinishWhenVisible = onlyFinishWhenVisible;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_OnlyFinishWhenVisible);
        }
    }

}