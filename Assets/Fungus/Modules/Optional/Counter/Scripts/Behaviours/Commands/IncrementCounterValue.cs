﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Increment a counter's value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Increment Counter Value")]
    public class IncrementCounterValue : CounterCommand
    {
        [Tooltip("The amount the counter value will be incremented by")]
        [DataMin(0)]
        [FormerlySerializedAs("_amount")]
        public FloatData m_Amount = new FloatData(1);

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float amount = m_Amount.Value;

            counter.IncrementCounter(amount);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" by {0}", m_Amount);
        }
    }
}
