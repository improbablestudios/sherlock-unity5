﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set the info text of a counter.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Info Text")]
    public class SetCounterInfoText : CounterCommand, ILocalizable
    {
        [Tooltip("The text that will be displayed in the counter")]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();

            Flowchart flowchart = GetFlowchart();

            if (m_Text.VariableDataType == VariableDataType.Value)
            {
                counter.SetCounterInfoText(LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_Text.Value, flowchart.FormatString), flowchart.FormatString(m_Text.Value));
            }
            else if (m_Text.VariableDataType == VariableDataType.Variable)
            {
                counter.SetCounterInfoText(m_Text.Variable.GetLocalizedDictionary(flowchart.FormatString), flowchart.FormatString(m_Text.Value));
            }
            else
            {
                counter.SetCounterInfoText(flowchart.FormatString(m_Text.Value));
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Text);
        }

        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}
