﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set whether or not a counter will hide after it has reached it's target value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Hide After Finished")]
    public class SetCounterHideAfterFinished : CounterCommand
    {
        [Tooltip("If true, the counter will hide after it is finished, otherwise, it will stay visible")]
        public BooleanData m_HideAfterFinished = new BooleanData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            bool hideAfterFinished = m_HideAfterFinished.Value;

            counter.HideAfterFinished = hideAfterFinished;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_HideAfterFinished);
        }
    }

}