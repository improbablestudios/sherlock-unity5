﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Timer",
                 "Stop a timer.")]
    [AddComponentMenu("Fungus/Commands/Counter/Timer/Stop Timer")]
    public class StopTimer : TimerCommand
    {
        public override void OnEnter()
        {
            Timer timer = GetTimer();

            timer.StopTimer();

            Continue();
        }
    }

}