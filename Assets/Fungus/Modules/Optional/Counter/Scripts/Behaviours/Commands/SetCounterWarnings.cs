﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set warnings on a counter")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Warnings")]
    public class SetCounterWarnings : CounterCommand
    {
        [Tooltip("The warnings")]
        [FormerlySerializedAs("counterWarnings")]
        public CounterWarningListData m_CounterWarnings = new CounterWarningListData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            List<CounterWarning> counterWarnings = m_CounterWarnings.Value;

            counter.SetCounterWarnings(counterWarnings.ToArray());

            Continue();
        }
    }

}