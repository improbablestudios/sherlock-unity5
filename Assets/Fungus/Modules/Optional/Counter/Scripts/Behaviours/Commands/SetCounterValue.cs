﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Set a counter's value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Set Counter Value")]
    public class SetCounterValue : CounterCommand
    {
        [Tooltip("If true, set counter value to max value")]
        [FormerlySerializedAs("_maxValue")]
        public BooleanData m_MaxValue = new BooleanData();

        [Tooltip("The value at which this event will be fired")]
        [FormerlySerializedAs("_value")]
        public FloatData m_Value = new FloatData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            bool maxValue = m_MaxValue.Value;
            float value = m_Value.Value;

            if (maxValue)
            {
                counter.SetCounterValueToMax();
            }
            else
            {
                counter.SetCounterValue(value);
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            string valueString = m_Value.ToString();
            if (!m_MaxValue.IsConstant)
            {
                valueString = string.Format("max value if {0}, otherwise {1}", m_MaxValue, m_Value);
            }
            else if (m_MaxValue.IsConstant && m_MaxValue.Value)
            {
                valueString = "max value";
            }

            return base.GetSummary() + string.Format(" to {0}", valueString);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Value))
            {
                if (m_MaxValue.IsConstant && m_MaxValue.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
