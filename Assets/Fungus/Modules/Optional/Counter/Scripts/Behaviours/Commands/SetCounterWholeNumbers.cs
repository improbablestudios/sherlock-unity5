﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set whether or not a counter's value will be a whole number.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Whole Numbers")]
    public class SetCounterWholeNumbers : CounterCommand
    {
        [Tooltip("If true, ensure the counter value is always a whole number")]
        public BooleanData m_WholeNumbers = new BooleanData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            bool wholeNumbers = m_WholeNumbers.Value;

            counter.SetCounterWholeNumbers(wholeNumbers);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_WholeNumbers);
        }
    }

}