﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Decrement a counter's value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Decrement Counter Value")]
    public class DecrementCounterValue : CounterCommand
    {
        [Tooltip("The amount the counter value will be decremented by")]
        [DataMin(0)]
        [FormerlySerializedAs("_amount")]
        public FloatData m_Amount = new FloatData(1);

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float amount = m_Amount.Value;

            counter.DecrementCounter(amount);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" by {0}", m_Amount);
        }
    }
}
