﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set a counter's max value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Max")]
    public class SetCounterMax : CounterCommand
    {
        [Tooltip("The max value the counter will count down from or count up to")]
        [FormerlySerializedAs("_maxValue")]
        public FloatData m_MaxValue = new FloatData(10f);

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float maxValue = m_MaxValue.Value;

            counter.SetCounterMax(maxValue);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_MaxValue);
        }
    }

}