﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Reset a counter.")]
    [AddComponentMenu("Fungus/Commands/Counter/Reset Counter")]
    public class ResetCounter : CounterCommand
    {
        public override void OnEnter()
        {
            Counter counter = GetCounter();

            counter.ResetCounter();

            Continue();
        }
    }

}