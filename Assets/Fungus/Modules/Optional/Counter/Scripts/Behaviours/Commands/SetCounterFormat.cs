﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set a counter's text display format.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Format")]
    public class SetCounterFormat : CounterCommand, ILocalizable
    {
        [Tooltip("The format that will be used to display the counter value {0} and max value {1} (e.g. {0}/{1})")]
        [Localize]
        [FormerlySerializedAs("_timeFormat")]
        [FormerlySerializedAs("m_TimeFormat")]
        public StringData m_ValueFormat = new StringData("{0}/{1}");

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            string timeFormat = m_ValueFormat.Value;

            Flowchart flowchart = GetFlowchart();

            if (m_ValueFormat.VariableDataType == VariableDataType.Value)
            {
                counter.SetCounterFormat(LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_ValueFormat.Value, flowchart.FormatString), flowchart.FormatString(m_ValueFormat.Value));
            }
            else if (m_ValueFormat.VariableDataType == VariableDataType.Variable)
            {
                counter.SetCounterFormat(m_ValueFormat.Variable.GetLocalizedDictionary(flowchart.FormatString), flowchart.FormatString(m_ValueFormat.Value));
            }
            else
            {
                counter.SetCounterFormat(flowchart.FormatString(m_ValueFormat.Value));
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_ValueFormat);
        }
        
        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}