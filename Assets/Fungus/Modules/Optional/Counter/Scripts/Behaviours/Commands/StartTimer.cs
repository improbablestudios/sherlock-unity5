﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Timer",
                 "Start a timer.")]
    [AddComponentMenu("Fungus/Commands/Counter/Timer/Start Timer")]
    public class StartTimer : TimerCommand
    {
        public override void OnEnter()
        {
            Timer timer = GetTimer();

            timer.StartTimer();

            Continue();
        }
    }

}