﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter",
                 "Get a counter's current value.")]
    [AddComponentMenu("Fungus/Commands/Counter/Get Counter Value")]
    public class GetCounterValue : NumericReturnValueCommand
    {
        [Tooltip("The target counter")]
        [ObjectDataPopup(true)]
        [FormerlySerializedAs("_counter")]
        [CheckNotNullData]
        public CounterData m_Counter = new CounterData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        [VariableOfTypeField(typeof(int), typeof(float))]
        public Variable m_ReturnVariable;
        
        public override object GetReturnValue()
        {
            Counter counter = m_Counter.Value;

            return counter.GetDisplayedValue();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Counter);
        }
    }

}