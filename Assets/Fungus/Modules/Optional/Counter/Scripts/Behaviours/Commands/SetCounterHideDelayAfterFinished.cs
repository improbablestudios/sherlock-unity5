﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set the delay in seconds between a counter finishing and hiding.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Hide Delay After Finished")]
    public class SetCounterHideDelayAfterFinished : CounterCommand
    {
        [Tooltip("The delay in seconds between the counter finishing and hiding")]
        [DataMin(0)]
        [FormerlySerializedAs("m_HideDelayAfterFinished")]
        public FloatData m_HideAfterFinishedDelay = new FloatData();

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            float hideAfterFinishedDelay = m_HideAfterFinishedDelay.Value;

            counter.HideAfterFinishedDelay = hideAfterFinishedDelay;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_HideAfterFinishedDelay);
        }
    }

}