﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Counter/Setup",
                 "Set a counter to count down or count up.")]
    [AddComponentMenu("Fungus/Commands/Counter/Setup/Set Counter Direction")]
    public class SetCounterDirection : CounterCommand
    {
        [Tooltip("If true the counter will count down, if false the counter will count up")]
        [FormerlySerializedAs("_countdown")]
        public BooleanData m_Countdown = new BooleanData(true);

        public override void OnEnter()
        {
            Counter counter = GetCounter();
            bool countdown = m_Countdown.Value;

            counter.SetCounterDirection(countdown);

            Continue();
        }
        
        public override string GetSummary()
        {
            string countdownString = "";
            if (!m_Countdown.IsConstant)
            {
                countdownString = "countdown if " + m_Countdown.ToString();
            }
            else if (m_Countdown.Value)
            {
                countdownString = "count down";
            }
            else if (!m_Countdown.Value)
            {
                countdownString = "count up";
            }
            
            return base.GetSummary() + string.Format(" to {0}", countdownString);
        }
    }

}