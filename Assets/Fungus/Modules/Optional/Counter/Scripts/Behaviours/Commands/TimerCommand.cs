﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.Commands
{

    public abstract class TimerCommand : Command
    {
        [Tooltip("The target timer")]
        [FormerlySerializedAs("_counter")]
        [FormerlySerializedAs("m_Counter")]
        [ObjectDataPopup("<Default>", true)]
        public TimerData m_Timer = new TimerData();

        public Timer GetTimer()
        {
            Timer timer = m_Timer.Value as Timer;

            if (timer == null)
            {
                timer = Timer.DefaultTimer;
            }

            return timer;
        }

        public override string GetSummary()
        {
            string timerString = m_Timer.ToString();
            if (m_Timer.IsConstant && m_Timer.Value == null)
            {
                timerString = "<Default>";
            }
            return string.Format("\"{0}\"", timerString);
        }
    }
}
