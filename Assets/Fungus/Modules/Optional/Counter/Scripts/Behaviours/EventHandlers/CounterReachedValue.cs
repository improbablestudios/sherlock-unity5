﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.UIUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Counter",
                      "This block will execute when the counter reaches the specified value.")]
    [AddComponentMenu("Fungus/Event Handlers/Counter/Counter Reached Value")]
    public class CounterReachedValue : EventHandler
    {
        [Tooltip("The target counter")]
        [ObjectDataPopup("<Default>", true)]
        [FormerlySerializedAs("_counter")]
        public CounterData m_Counter = new CounterData();

        [Tooltip("The value at which this event will be fired")]
        [FormerlySerializedAs("_value")]
        public FloatData m_Value = new FloatData();

        protected bool m_valueWasReached = false;

        public Counter GetCounter()
        {
            Counter counter = m_Counter.Value as Counter;

            if (counter == null)
            {
                counter = Counter.DefaultCounter;
            }

            return counter;
        }

        protected void Update()
        {
            Counter counter = GetCounter();
            float value = m_Value.Value;
            
            if (counter.Countdown)
            {
                if (m_valueWasReached && counter.GetDisplayedValue() > value)
                {
                    m_valueWasReached = false;
                }
                if (!m_valueWasReached && counter.GetDisplayedValue() <= value)
                {
                    OnCounterReachedValue();
                }
            }
            else
            {
                if (m_valueWasReached && counter.GetDisplayedValue() < value)
                {
                    m_valueWasReached = false;
                }
                if (!m_valueWasReached && counter.GetDisplayedValue() >= value)
                {
                    OnCounterReachedValue();
                }
            }
        }

        public void OnCounterReachedValue()
        {
            m_valueWasReached = true;
            ExecuteBlock();
        }

        public override string GetSummary()
        {
            string counterString = m_Counter.ToString();
            if (m_Counter.IsConstant && m_Counter.Value == null)
            {
                counterString = "<Default>";
            }
            return string.Format("\"{0}\" reached {1}", counterString, m_Value.ToString());
        }
    }

}