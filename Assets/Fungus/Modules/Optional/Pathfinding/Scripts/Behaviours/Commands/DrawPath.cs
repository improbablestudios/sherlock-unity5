﻿using UnityEngine;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.PathfindingUtilities;
using ImprobableStudios.DOTweenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Path Renderer",
                 "Animate a line being drawn along the shortest path between two points.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Path Renderer/Draw Path")]
    public class DrawPath : DOTweenCommand
    {
        [Tooltip("The target path renderer")]
        [CheckNotNullData]
        public PathRendererData m_PathRenderer = new PathRendererData();

        private bool m_previousPathRendererCanSearch;

        public override void OnEnter()
        {
            PathRenderer pathRenderer = m_PathRenderer.Value;

            pathRenderer.ForceScanGraphs();
            pathRenderer.ForceSearchPath(Draw);

            if (!WillWait())
            {
                Continue();
            }
        }

        private void Draw()
        {
            PathRenderer pathRenderer = m_PathRenderer.Value;
            PerformTween(GetTween());
        }

        public override Tween GetTween()
        {
            PathRenderer pathRenderer = m_PathRenderer.Value;
            float duration = m_Duration.Value;

            // Turn off path searching when drawing line.
            m_previousPathRendererCanSearch = pathRenderer.CanSearch;
            pathRenderer.CanSearch = false;

            return pathRenderer.LineRenderer.DODraw(pathRenderer.CalculatedPath, duration);
        }

        public override void OnTweenComplete()
        {
            // Revert PathFinder CanSearch variable once done drawing line.
            PathRenderer pathRenderer = m_PathRenderer.Value;
            pathRenderer.CanSearch = m_previousPathRendererCanSearch;
        }
        
        public override string GetSummary()
        {
            return string.Format("using \"{0}\"", m_PathRenderer) + GetDurationSummary(m_Duration);
        }
    }

}