﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using System.Linq;
using Fungus.Variables;
using ImprobableStudios.PathfindingUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Renderer/Path Renderer",
                 "Get the vector3 points that make up a path renderer's line.")]
    [AddComponentMenu("Fungus/Commands/Renderer/Path Renderer/Get Path Renderer Points")]
    public class GetPathRendererPoints : ReturnValueCommand<List<Vector3>>
    {
        [Tooltip("The target line renderer")]
        [CheckNotNullData]
        public PathRendererData m_PathRenderer = new PathRendererData();

        [Tooltip("The variable to store the result in")]
        public Vector3ListVariable m_ReturnVariable;
        
        public override List<Vector3> GetReturnValue()
        {
            PathRenderer pathRenderer = m_PathRenderer.Value;

            return pathRenderer.CalculatedPath.ToList();
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_PathRenderer);
        }
    }

}