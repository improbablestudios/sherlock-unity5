﻿using ImprobableStudios.PathfindingUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Path Renderer Variable")]
    public class PathRendererVariable : Variable<PathRenderer>
    {
    }

    [Serializable]
    public class PathRendererData : VariableData<PathRenderer>
    {
        public PathRendererData()
        {
        }

        public PathRendererData(PathRenderer v)
        {
            m_DataVal = v;
        }
    }

}