﻿using ImprobableStudios.PathfindingUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Path Renderer List Variable")]
    public class PathRendererListVariable : Variable<List<PathRenderer>>
    {
    }

    [Serializable]
    public class PathRendererListData : VariableData<List<PathRenderer>>
    {
        public PathRendererListData()
        {
            m_DataVal = new List<PathRenderer>();
        }

        public PathRendererListData(List<PathRenderer> v)
        {
            m_DataVal = v;
        }
    }

}