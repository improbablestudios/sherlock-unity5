#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.Localization;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetLanguageSetting), true)]
    public class SetLanguageSettingEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SetLanguageSetting t = target as SetLanguageSetting;

            if (property.name == nameof(t.m_LanguageCode))
            {
                VariableEditor.VariableDataField<string>(position, property, label, DrawLanguagePopup);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public void DrawLanguagePopup(Rect position, SerializedProperty property, GUIContent label)
        {
            int languageCodeIndex = Array.IndexOf(LocalizationManager.Instance.Languages, property.stringValue);
            languageCodeIndex = EditorGUI.Popup(position, label, languageCodeIndex, LocalizationManager.Instance.Languages.Select(i => new GUIContent(i)).ToArray());
            if (languageCodeIndex >= 0 && languageCodeIndex < LocalizationManager.Instance.Languages.Length)
            {
                property.stringValue = LocalizationManager.Instance.Languages[languageCodeIndex];
            }
        }

    }
}
#endif