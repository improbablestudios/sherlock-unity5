﻿using ImprobableStudios.Localization;
using ImprobableStudios.SaveSystem;
using System;
using System.Linq.Expressions;

namespace Fungus
{
    
    [Serializable]
    public class LocalizationManagerSaveState : ScriptableObjectSaveState<LocalizationManager>
    {
        public override Expression<Func<LocalizationManager, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<LocalizationManager, object>>[]
            {
                x => x.ActiveLanguage
            };
        }
        
        public override int LoadPriority()
        {
            return -2;
        }
    }

    public class SavableLocalizationManager : ISavableSetting
    {
        [Serializable]
        public class SavableLocalizationManagerSaveState : SaveState<SavableLocalizationManager>
        {
            public override void Save(SavableLocalizationManager savable)
            {
                savable.m_saveState.Save(LocalizationManager.Instance);

                base.Save(savable);
            }

            public override void Load(SavableLocalizationManager savable)
            {
                savable.m_saveState.Load(LocalizationManager.Instance);

                base.Load(savable);
            }
        }
        
        protected LocalizationManagerSaveState m_saveState = new LocalizationManagerSaveState();

        public SaveState GetSaveState()
        {
            return new SavableLocalizationManagerSaveState();
        }

        public string GetKey()
        {
            return LocalizationManager.Instance.GetKey();
        }
    }

}
