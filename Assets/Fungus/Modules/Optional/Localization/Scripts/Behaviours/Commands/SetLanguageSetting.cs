﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{
    [CommandInfo("Localization",
                 "Change the application's active language. A localization builder with a localization file must be present in the scene.")]
    [AddComponentMenu("Fungus/Commands/Localization/Set Language")]
    public class SetLanguageSetting : SettingCommand
    {
        [Tooltip("The language to change to")]
        [FormerlySerializedAs("_languageCode")]
        [FormerlySerializedAs("m_LanguageCode")]
        [FormerlySerializedAs("m_Language")]
        [LanguageDataField]
        [CheckNotEmptyData]
        public StringData m_LanguageCode = new StringData();

        protected override void ChangeSetting()
        {
            string languageCode = m_LanguageCode.Value;

            LocalizationManager.Instance.ActiveLanguage = languageCode;
        }

        public override string GetSummary()
        {
            if (m_LanguageCode.IsConstant)
            {
                return LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(m_LanguageCode.Value);
            }

            return string.Format("{0}", m_LanguageCode);
        }
    }
}