﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{

    [CommandInfo("Localization",
                 "Convert a language display name to a language code.")]
    [AddComponentMenu("Fungus/Commands/Localization/Get Language Code From Language Display Name")]
    public class GetLanguageCodeFromLanguageDisplayName : ReturnValueCommand<string>
    {
        [Tooltip("The language display name")]
        [CheckNotEmptyData]
        public StringData m_LanguageDisplayName = new StringData();

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            return LocalizationEntry.GetLanguageCodeFromLanguageDisplayName(m_LanguageDisplayName.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" language code", m_ReturnVariable, m_LanguageDisplayName);
        }
    }

}