﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{

    [CommandInfo("Localization",
                 "Convert a language code to a language display name.")]
    [AddComponentMenu("Fungus/Commands/Localization/Get Language Display Name From Language Code")]
    public class GetLanguageDisplayNameFromLanguageCode : ReturnValueCommand<string>
    {
        [Tooltip("The language code")]
        [CheckNotEmptyData]
        public StringData m_LanguageCode = new StringData();

        [Tooltip("The variable to store the result in")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            return LocalizationEntry.GetLanguageDisplayNameFromLanguageCode(m_LanguageCode.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" language display name", m_ReturnVariable, m_LanguageCode);
        }
    }

}