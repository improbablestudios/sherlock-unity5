﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.Localization;
using ImprobableStudios.SaveSystem;

namespace Fungus
{

    [Serializable]
    [AddComponentMenu("Fungus/Behaviours/UI/Setting/Language Selector")]
    public class LanguageSelector : SettingSelector<Dropdown>
    {
        [Tooltip("Dropdown that can be used to select the active language")]
        [SerializeField]
        [FormerlySerializedAs("dropdown")]
        [CheckNotNull]
        protected Dropdown m_Dropdown;

        public Dropdown Dropdown
        {
            get
            {
                return m_Dropdown;
            }
            set
            {
                m_Dropdown = value;
            }
        }

        public override Dropdown GetControl()
        {
            return m_Dropdown;
        }

        public override void SetControl(Dropdown control)
        {
            m_Dropdown = control;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Dropdown != null)
            {
                List<Dropdown.OptionData> languageOptions = new List<Dropdown.OptionData>();
                foreach (string language in LocalizationManager.Instance.Languages)
                {
                    Dropdown.OptionData option = new Dropdown.OptionData();
                    option.text = language;
                    languageOptions.Add(option);
                }

                m_Dropdown.options.Clear();
                m_Dropdown.AddOptions(languageOptions);
                m_Dropdown.value = Array.IndexOf(LocalizationManager.Instance.Languages, LocalizationManager.Instance.ActiveLanguage);
                m_Dropdown.onValueChanged.AddListener(OnValueChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Dropdown != null)
            {
                m_Dropdown.onValueChanged.RemoveListener(OnValueChanged);
            }
        }

        protected virtual void OnValueChanged(int languageIndex)
        {
            string language = LocalizationManager.Instance.Languages[languageIndex];
            LocalizationManager.Instance.ActiveLanguage = language;
            OnSettingChanged();
        }
    }

}