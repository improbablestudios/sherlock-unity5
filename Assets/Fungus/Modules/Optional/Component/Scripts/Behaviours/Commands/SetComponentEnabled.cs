﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Component",
                 "Set a component in the scene to be enabled or disabled.")]
    [AddComponentMenu("Fungus/Commands/Component/Set Component Enabled")]
    public class SetComponentEnabled : ComponentCommand
    {
        [Tooltip("If true, enable the component, else disable the component")]
        public BooleanData m_Enable = new BooleanData();

        public override void OnEnter()
        {
            Component component = m_Component.Value;
            bool enable = m_Enable.Value;

            Behaviour behaviour = component as Behaviour;
            Renderer renderer = component as Renderer;

            if (behaviour != null)
            {
                behaviour.enabled = enable;
            }
            if (renderer != null)
            {
                renderer.enabled = enable;
            }

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" enable = {0}", m_Enable);
        }
    }

}