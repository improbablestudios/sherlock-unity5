﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Component",
                 "Get the game object a component is attached to.")]
    [AddComponentMenu("Fungus/Commands/Component/Get Component GameObject")]
    public class GetComponentGameObject : ReturnValueCommand<GameObject>
    {
        [Tooltip("The target component")]
        [CheckNotNullData]
        public ComponentData m_Component = new ComponentData();

        [Tooltip("The variable to store the result in")]
        public GameObjectVariable m_ReturnVariable;
        
        public override GameObject GetReturnValue()
        {
            Component component = m_Component.Value;

            return component.gameObject;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" game object", m_ReturnVariable, m_Component);
        }
    }

}