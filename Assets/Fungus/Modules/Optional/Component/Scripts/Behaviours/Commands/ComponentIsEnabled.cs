﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Component",
                 "Check whether or not a component is enabled.")]
    [AddComponentMenu("Fungus/Commands/Component/Component Is Enabled")]
    public class ComponentIsEnabled : ReturnValueCommand<bool>
    {
        [Tooltip("Component to get the active value from")]
        [CheckNotNullData]
        public ComponentData m_Component = new ComponentData();
        
        [Tooltip("Boolean variable to store the boolean value in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            Component component = m_Component.Value;

            Behaviour behaviour = component as Behaviour;
            Renderer renderer = component as Renderer;
            
            if (behaviour != null)
            {
                return behaviour.enabled;
            }
            if (renderer != null)
            {
                return renderer.enabled;
            }

            return true;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" is enabled", m_ReturnVariable, m_Component);
        }
    }

}