﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Component",
                 "Check whether or not a component is active.")]
    [AddComponentMenu("Fungus/Commands/Component/Component Is Active")]
    public class ComponentIsActive : ReturnValueCommand<bool>
    {
        [Tooltip("Component to get the active value from")]
        [CheckNotNullData]
        public ComponentData m_Component = new ComponentData();
        
        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            Component component = m_Component.Value;

            Behaviour behaviour = component as Behaviour;
            Renderer renderer = component as Renderer;

            if (behaviour != null)
            {
                return behaviour.enabled && behaviour.gameObject.activeInHierarchy;
            }
            if (renderer != null)
            {
                return renderer.enabled && renderer.gameObject.activeInHierarchy;
            }

            return true;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" is active", m_ReturnVariable, m_Component);
        }
    }

}