﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{
    public abstract class ComponentCommand : Command
    {
        [Tooltip("The target component")]
        [CheckNotNullData]
        public ComponentData m_Component = new ComponentData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Component);
        }
    }

}