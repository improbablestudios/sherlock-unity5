﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Collider",
                 "Set the offset of a collider2D.")]
    [AddComponentMenu("Fungus/Commands/Collider/Set Collider2D Offset")]
    public class SetCollider2DOffset : Command
    {
        [Tooltip("The target collider2D")]
        [CheckNotNullData]
        public Collider2DData m_Collider2D = new Collider2DData();

        [Tooltip("If true, the collider2D is a trigger")]
        public Vector2Data m_Offset = new Vector2Data();

        public override void OnEnter()
        {
            Collider2D collider2D = m_Collider2D.Value;
            Vector2 offset = m_Offset.Value;

            collider2D.offset = offset;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" offset = {1}", m_Collider2D, m_Offset);
        }
    }

}