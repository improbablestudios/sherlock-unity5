﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Collider",
                 "Set whether or not a collider2D is a trigger or not. " +
                 "Triggers will invoke trigger events when touched and will not be collided with. " +
                 "Non-triggers will invoke collision events when touched and will be collided with.")]
    [AddComponentMenu("Fungus/Commands/Collider/Set Collider2D Is Trigger")]
    public class SetCollider2DIsTrigger : Command
    {
        [Tooltip("The target collider2D")]
        [CheckNotNullData]
        public Collider2DData m_Collider2D = new Collider2DData();

        [Tooltip("If true, the collider2D is a trigger")]
        public BooleanData m_IsTrigger = new BooleanData();

        public override void OnEnter()
        {
            Collider2D collider2D = m_Collider2D.Value;
            bool isTrigger = m_IsTrigger.Value;

            collider2D.isTrigger = isTrigger;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" is trigger = {1}", m_Collider2D, m_IsTrigger);
        }
    }

}