﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Collider",
                 "Set whether or not a collider is a trigger or not. " +
                 "Triggers will invoke trigger events when touched and will not be collided with. " +
                 "Non-triggers will invoke collision events when touched and will be collided with.")]
    [AddComponentMenu("Fungus/Commands/Collider/Set Collider Is Trigger")]
    public class SetColliderIsTrigger : Command
    {
        [Tooltip("The target collider")]
        [CheckNotNullData]
        public ColliderData m_Collider = new ColliderData();

        [Tooltip("If true, the collider is a trigger")]
        public BooleanData m_IsTrigger = new BooleanData();

        public override void OnEnter()
        {
            Collider Collider = m_Collider.Value;
            bool isTrigger = m_IsTrigger.Value;

            Collider.isTrigger = isTrigger;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" is trigger = {1}", m_Collider, m_IsTrigger);
        }
    }

}