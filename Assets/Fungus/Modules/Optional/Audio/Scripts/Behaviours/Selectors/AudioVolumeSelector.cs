﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;
using UnityEngine.UI;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.AudioUtilities;

namespace Fungus
{

    [Serializable]
    [AddComponentMenu("Fungus/Behaviours/UI/Setting/Audio Volume Multiplier Selector")]
    public class AudioVolumeSelector : SettingSelector<Slider>
    {
        [Tooltip("Slider that can be used to select the volume")]
        [SerializeField]
        [FormerlySerializedAs("slider")]
        [CheckNotNull]
        protected Slider m_Slider;

        [Tooltip("The audio mixer group this will affect")]
        [SerializeField]
        [CheckNotNull]
        protected AudioMixerGroup m_AudioMixerGroup;
        
        public Slider Slider
        {
            get
            {
                return m_Slider;
            }
            set
            {
                m_Slider = value;
            }
        }
        
        public AudioMixerGroup AudioMixerGroup
        {
            get
            {
                return m_AudioMixerGroup;
            }
            set
            {
                m_AudioMixerGroup = value;
            }
        }

        public override Slider GetControl()
        {
            return m_Slider;
        }

        public override void SetControl(Slider control)
        {
            m_Slider = control;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Slider != null)
            {
                m_Slider.value = AudioManager.Instance.GetLinearVolumeOfAudioMixerGroup(m_AudioMixerGroup);
                m_Slider.onValueChanged.AddListener(OnValueChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Slider != null)
            {
                m_Slider.onValueChanged.RemoveListener(OnValueChanged);
            }
        }

        protected virtual void OnValueChanged(float volume)
        {
            AudioManager.Instance.SetLinearVolumeOfAudioMixerGroup(m_AudioMixerGroup, volume);
            OnSettingChanged();
        }
    }

}