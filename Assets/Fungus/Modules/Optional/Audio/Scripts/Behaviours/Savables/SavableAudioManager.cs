﻿using ImprobableStudios.AudioUtilities;
using ImprobableStudios.SaveSystem;
using System;
using System.Linq.Expressions;
using UnityEngine.Audio;

namespace Fungus
{

    [Serializable]
    public class AudioManagerSaveState : ScriptableObjectSaveState<AudioManager>
    {
        public override void Save(AudioManager audioManager)
        {
            base.Save(audioManager);

            if (audioManager.AudioMixer != null)
            {
                foreach (AudioMixerGroup audioMixerGroup in audioManager.AudioMixer.FindMatchingGroups(""))
                {
                    AudioMixerGroupInfo amgi = audioManager.GetAudioMixerGroupInfo(audioMixerGroup);
                    if (amgi != null)
                    {
                        amgi.m_CurrentVolume = audioManager.GetLinearVolumeOfAudioMixerGroup(audioMixerGroup);
                    }
                }
            }
        }

        public override void Load(AudioManager audioManager)
        {
            base.Load(audioManager);

            if (audioManager.AudioMixer != null)
            {
                foreach (AudioMixerGroup audioMixerGroup in audioManager.AudioMixer.FindMatchingGroups(""))
                {
                    AudioMixerGroupInfo amgi = audioManager.GetAudioMixerGroupInfo(audioMixerGroup);
                    if (amgi != null)
                    {
                        audioManager.SetLinearVolumeOfAudioMixerGroup(audioMixerGroup, amgi.m_CurrentVolume);
                    }
                }
            }
        }

        public override int LoadPriority()
        {
            return -2;
        }
    }

    public class SavableAudioManager : ISavableSetting
    {
        [Serializable]
        public class SavableAudioManagerSaveState : SaveState<SavableAudioManager>
        {
            public override void Save(SavableAudioManager savable)
            {
                savable.m_saveState.Save(AudioManager.Instance);

                base.Save(savable);
            }

            public override void Load(SavableAudioManager savable)
            {
                savable.m_saveState.Load(AudioManager.Instance);

                base.Load(savable);
            }
        }

        protected AudioManagerSaveState m_saveState = new AudioManagerSaveState();

        public SaveState GetSaveState()
        {
            return new SavableAudioManagerSaveState();
        }

        public string GetKey()
        {
            return AudioManager.Instance.GetKey();
        }
    }

}
