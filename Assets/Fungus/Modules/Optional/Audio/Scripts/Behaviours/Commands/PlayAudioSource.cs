﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Play an audio source.")]
    [AddComponentMenu("Fungus/Commands/Audio/Play Audio Source")]
    public class PlayAudioSource : SingleAudioSourceCommand
    {
        [Tooltip("Loop the audio when it reaches the end")]
        [FormerlySerializedAs("_loop")]
        public BooleanData m_Loop = new BooleanData();
        
        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            bool loop = m_Loop.Value;
            
            AudioManager.Instance.Play(audioSource, loop, fadeDuration, onComplete);
        }
        
        public override string GetSummary()
        {
            string loopString = " looping if " + m_Loop.ToString();
            if (m_Loop.IsConstant)
            {
                if (m_Loop.Value)
                {
                    loopString = " looping";
                }
                else
                {
                    loopString = "";
                }
            }
            
            return base.GetSummary() + string.Format(" {0}", loopString) + GetFadeString();
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}