﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Make an audio source emit 2D or 3D sound.")]
    [AddComponentMenu("Fungus/Commands/Audio/Set Audio Source Spatial Blend")]
    public class SetAudioSourceSpatialBlend : AudioSourceCommand
    {
        [Tooltip("The target audio source")]
        [FormerlySerializedAs("_audioSource")]
        [CheckNotNullData]
        public AudioSourceData m_AudioSource = new AudioSourceData();

        [Tooltip("If set to 1, the posiiton of the audio source relative to the main camera will determine how loud it sounds. If set to 0, the audio source will be the same volume regardless of it's position relative to the camera.")]
        [DataRange(0, 1)]
        public FloatData m_SpatialBlend = new FloatData();

        public override void OnEnter()
        {
            AudioSource audioSource = m_AudioSource.Value;
            float spatialBlend = m_SpatialBlend.Value;

            audioSource.spatialBlend = spatialBlend;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to {1}", m_AudioSource, m_SpatialBlend);
        }
    }

}