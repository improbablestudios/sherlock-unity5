﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Audio",
                 "Change the audio volume setting.")]
    [AddComponentMenu("Fungus/Commands/Setting/Audio/Set Audio Volume Setting")]
    public class SetAudioVolumeSetting : SettingCommand
    {
        [Tooltip("The audio mixer group to change the volume of")]
        public AudioMixerGroupData m_AudioMixerGroup = new AudioMixerGroupData();

        [Tooltip("The new volume")]
        [DataRange(0f, 1f)]
        public FloatData m_Volume = new FloatData(1f);

        protected override void ChangeSetting()
        {
            AudioMixerGroup audioMixerGroup = m_AudioMixerGroup.Value;
            float volume = m_Volume.Value;

            AudioManager.Instance.SetLinearVolumeOfAudioMixerGroup(audioMixerGroup, volume);
        }

        public override string GetSummary()
        {
            return string.Format("{0} to {1}", m_AudioMixerGroup, m_Volume);
        }
    }

}