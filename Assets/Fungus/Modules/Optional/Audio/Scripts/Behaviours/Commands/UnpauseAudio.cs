﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Unpause audio.")]
    [AddComponentMenu("Fungus/Commands/Audio/Unpause Audio")]
    public class UnpauseAudio : MultiAudioCommand
    {
        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.UnPause(audioSource, fadeDuration, onComplete);
        }

        protected override void ControlAllAudioSources(float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.UnPauseAll(fadeDuration, onComplete);
        }

        protected override void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.UnPauseAllInAudioMixerGroup(audioMixerGroup, fadeDuration, onComplete);
        }
    }

}