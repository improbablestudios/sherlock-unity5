﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Pause audio.")]
    [AddComponentMenu("Fungus/Commands/Audio/Pause Audio")]
    public class PauseAudio : MultiAudioCommand
    {
        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.Pause(audioSource, fadeDuration, onComplete);
        }

        protected override void ControlAllAudioSources(float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.PauseAll(fadeDuration, onComplete);
        }

        protected override void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.PauseAllInAudioMixerGroup(audioMixerGroup, fadeDuration, onComplete);
        }
    }

}