﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Set the clip of an audio source.")]
    [AddComponentMenu("Fungus/Commands/Audio/Set Audio Source Clip")]
    public class SetAudioSourceClip : AudioSourceCommand, ILocalizable
    {
        [Tooltip("The target audio source")]
        [FormerlySerializedAs("_audioSource")]
        [CheckNotNullData]
        public AudioSourceData m_AudioSource = new AudioSourceData();

        [Tooltip("The audio clip")]
        [Localize]
        [FormerlySerializedAs("_audioClip")]
        [CheckNotNullData]
        public AudioClipData m_AudioClip = new AudioClipData();

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            AudioSource audioSource = m_AudioSource.Value;
            AudioClip audioClip = m_AudioClip.Value;

            if (m_AudioClip.VariableDataType == VariableDataType.Value)
            {
                audioSource.RegisterLocalizableAudioClip(this.SafeGetLocalizedDictionary(x => x.m_AudioClip.Value));
            }
            else if (m_AudioClip.VariableDataType == VariableDataType.Variable)
            {
                audioSource.RegisterLocalizableAudioClip(m_AudioClip.Variable.GetLocalizedDictionary());
            }

            audioSource.clip = audioClip;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" to \"{1}\"", m_AudioSource, m_AudioClip);
        }


        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
    }

}