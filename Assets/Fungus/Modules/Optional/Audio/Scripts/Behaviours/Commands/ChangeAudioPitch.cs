﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Change audio pitch.")]
    [AddComponentMenu("Fungus/Commands/Audio/Change Audio Pitch")]
    public class ChangeAudioPitch : MultiAudioCommand
    {
        [Tooltip("The pitch to play the audio at")]
        [DataRange(-3, 3)]
        [FormerlySerializedAs("_pitch")]
        public FloatData m_Pitch = new FloatData(1f);

        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            float pitch = m_Pitch.Value;

            AudioManager.Instance.ChangePitch(audioSource, pitch, fadeDuration, onComplete);
        }

        protected override void ControlAllAudioSources(float fadeDuration, UnityAction onComplete)
        {
            float pitch = m_Pitch.Value;

            AudioManager.Instance.ChangePitchOfAll(pitch, fadeDuration, onComplete);
        }

        protected override void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete)
        {
            float pitch = m_Pitch.Value;

            AudioManager.Instance.ChangePitchOfAudioMixerGroup(audioMixerGroup, pitch, fadeDuration, onComplete);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Pitch) + GetFadeString();
        }
    }

}