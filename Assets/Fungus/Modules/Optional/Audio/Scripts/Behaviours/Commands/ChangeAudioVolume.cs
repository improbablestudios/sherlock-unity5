﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Change audio volume.")]
    [AddComponentMenu("Fungus/Commands/Audio/Change Audio Volume")]
    public class ChangeAudioVolume : MultiAudioCommand
    {
        [Tooltip("The volume to play the audio at")]
        [DataRange(0, 1)]
        [FormerlySerializedAs("_volume")]
        public FloatData m_Volume = new FloatData(1f);
        
        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            float volume = m_Volume.Value;

            AudioManager.Instance.ChangeVolume(audioSource, volume, fadeDuration, onComplete);
        }

        protected override void ControlAllAudioSources(float fadeDuration, UnityAction onComplete)
        {
            float volume = m_Volume.Value;

            AudioManager.Instance.ChangeVolumeOfAll(volume, fadeDuration, onComplete);
        }

        protected override void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete)
        {
            float volume = m_Volume.Value;

            AudioManager.Instance.ChangeVolumeOfAudioMixerGroup(audioMixerGroup, volume, fadeDuration, onComplete);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Volume) + GetFadeString();
        }
    }

}