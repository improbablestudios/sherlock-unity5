﻿using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class AudioSourceCommand : Command
    {
        protected string GetFadeSummary(FloatData fadeDuration)
        {
            return GetDurationSummary(fadeDuration, "fade over");
        }
    }

}