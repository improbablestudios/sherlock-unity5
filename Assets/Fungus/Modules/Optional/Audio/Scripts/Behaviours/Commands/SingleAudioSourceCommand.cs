﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class SingleAudioSourceCommand : AudioSourceCommand
    {
        [Tooltip("The target audio mixer group")]
        [CheckNotNullData]
        public AudioSourceData m_AudioSource = new AudioSourceData();
        
        [Tooltip("The time it will take to fade from the current volume to zero")]
        [DataMin(0)]
        [FormerlySerializedAs("_fadeDuration")]
        public FloatData m_FadeDuration = new FloatData();
        
        public override void OnEnter()
        {
            AudioSource audioSource = m_AudioSource.Value;
            float fadeDuration = m_FadeDuration.Value;

            ControlAudioSource(audioSource, fadeDuration, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }

        protected abstract void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete);
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_AudioSource);
        }

        protected string GetFadeString()
        {
            return GetFadeSummary(m_FadeDuration);
        }

        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_FadeDuration.IsConstant) || m_FadeDuration.Value > 0)
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_FadeDuration))
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}