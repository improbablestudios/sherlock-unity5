﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class MultiAudioCommand : AudioSourceCommand
    {
        public enum TargetType
        {
            AudioSource,
            AllAudioSources,
            AudioMixerGroup
        }

        [Tooltip("The target type")]
        [FormerlySerializedAs("targetType")]
        [FormerlySerializedAs("m_TargetType")]
        [FormerlySerializedAs("m_AudioSourceTarget")]
        public TargetType m_AudioTarget;

        [Tooltip("The target audio source")]
        public AudioSourceData m_AudioSource = new AudioSourceData();

        [Tooltip("The target audio mixer group")]
        public AudioMixerGroupData m_AudioMixerGroup = new AudioMixerGroupData();
        
        [Tooltip("The time it will take to fade from the current volume to zero")]
        [DataMin(0)]
        [FormerlySerializedAs("_fadeDuration")]
        public FloatData m_FadeDuration = new FloatData();
        
        public override void OnEnter()
        {
            AudioSource audioSource = m_AudioSource.Value;
            AudioMixerGroup audioMixerGroup = m_AudioMixerGroup.Value;
            float fadeDuration = m_FadeDuration.Value;

            switch (m_AudioTarget)
            {
                case TargetType.AudioSource:
                    ControlAudioSource(audioSource, fadeDuration, ContinueAfterFinished);
                    break;
                case TargetType.AllAudioSources:
                    ControlAllAudioSources(fadeDuration, ContinueAfterFinished);
                    break;
                case TargetType.AudioMixerGroup:
                    ControlAudioMixerGroup(audioMixerGroup, fadeDuration, ContinueAfterFinished);
                    break;
            }

            if (!WillWait())
            {
                Continue();
            }
        }

        protected abstract void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete);

        protected abstract void ControlAllAudioSources(float fadeDuration, UnityAction onComplete);

        protected abstract void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete);

        public override string GetSummary()
        {
            string targetTypeString = "";
            switch (m_AudioTarget)
            {
                case TargetType.AudioSource:
                    targetTypeString = "\"" + m_AudioSource.ToString() + "\"";
                    break;
                case TargetType.AllAudioSources:
                    targetTypeString = "All";
                    break;
                case TargetType.AudioMixerGroup:
                    targetTypeString = "AudioMixerGroup (" + m_AudioMixerGroup.ToString() + ")";
                    break;
            }

            return string.Format("{0}", targetTypeString);
        }

        protected string GetFadeString()
        {
            return GetFadeSummary(m_FadeDuration);
        }

        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_FadeDuration.IsConstant) || m_FadeDuration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_AudioSource))
            {
                if (m_AudioTarget == TargetType.AudioSource)
                {
                    if ((Application.isPlaying || m_AudioSource.IsConstant) && m_AudioSource.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_AudioMixerGroup))
            {
                if (m_AudioTarget == TargetType.AudioMixerGroup)
                {
                    if ((Application.isPlaying || m_AudioMixerGroup.IsConstant) && m_AudioMixerGroup.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_AudioSource))
            {
                if (m_AudioTarget != TargetType.AudioSource)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AudioMixerGroup))
            {
                if (m_AudioTarget != TargetType.AudioMixerGroup)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_FadeDuration))
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}