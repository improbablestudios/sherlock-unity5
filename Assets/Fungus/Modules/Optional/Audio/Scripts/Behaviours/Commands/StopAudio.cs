﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.AudioUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Audio Source",
                 "Stop audio.")]
    [AddComponentMenu("Fungus/Commands/Audio/Stop Audio")]
    public class StopAudio : MultiAudioCommand
    {
        protected override void ControlAudioSource(AudioSource audioSource, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.Stop(audioSource, fadeDuration, onComplete);
        }

        protected override void ControlAllAudioSources(float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.StopAll(fadeDuration, onComplete);
        }

        protected override void ControlAudioMixerGroup(AudioMixerGroup audioMixerGroup, float fadeDuration, UnityAction onComplete)
        {
            AudioManager.Instance.StopAllInAudioMixerGroup(audioMixerGroup, fadeDuration, onComplete);
        }
    }

}