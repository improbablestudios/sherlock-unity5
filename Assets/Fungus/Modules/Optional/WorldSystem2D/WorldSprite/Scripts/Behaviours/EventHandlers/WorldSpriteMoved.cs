﻿using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("World Sprite",
                      "This block will execute when a sprite steps on a collider.")]
    [AddComponentMenu("Fungus/Event Handlers/World Sprite/World Sprite Moved")]
    public class WorldSpriteMoved : WorldSpriteEventHandler
    {
        public enum MoveTrigger
        {
            BeganMove,
            IsMoving,
            EndedMove,
        }

        [Tooltip("Only detect event at start of movement, during movement, or at the end of movement")]
        public MoveTrigger m_MoveTrigger = MoveTrigger.EndedMove;

        public BooleanData m_Anywhere = new BooleanData();

        protected override bool HasTarget()
        {
            return !((Application.isPlaying || m_Anywhere.IsConstant) && m_Anywhere.Value);
        }

        protected override UnityEvent GetUnityEvent()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;

            if (worldSprite != null)
            {
                if (m_MoveTrigger == MoveTrigger.BeganMove)
                {
                    return worldSprite.OnBeganMove;
                }
                else if (m_MoveTrigger == MoveTrigger.IsMoving)
                {
                    return worldSprite.OnMove;
                }
                else if (m_MoveTrigger == MoveTrigger.EndedMove)
                {
                    return worldSprite.OnEndMove;
                }
            }

            return null;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            bool anywhere = m_Anywhere.Value;
            WorldSprite worldSprite = m_WorldSprite.Value;
            Collider2D targetCollider = GetTargetCollider(true);

            if (anywhere || worldSprite.CollisionCollider.IsTouching(targetCollider))
            {
                return true;
            }

            return false;
        }

        public override string GetSummary()
        {
            string targetString = "Anywhere";
            if (!m_Anywhere.IsConstant || !m_Anywhere.Value)
            {
                targetString = GetTargetString();
            }
            return string.Format("{0} {1} {2}", m_WorldSprite, m_MoveTrigger, targetString);
        }
    }
}