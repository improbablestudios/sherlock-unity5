﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Cardinal Direction Variable")]
    public class CardinalDirectionVariable : Variable<CardinalDirection>
    {
    }

    [Serializable]
    public class CardinalDirectionData : VariableData<CardinalDirection>
    {
        public CardinalDirectionData()
        {
        }

        public CardinalDirectionData(CardinalDirection v)
        {
            m_DataVal = v;
        }
    }

}