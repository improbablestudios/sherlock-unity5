﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/World Sprite Animation List Variable")]
    public class WorldSpriteAnimationListVariable : Variable<List<WorldSpriteAnimation>>
    {
    }

    [Serializable]
    public class WorldSpriteAnimationListData : VariableData<List<WorldSpriteAnimation>>
    {
        public WorldSpriteAnimationListData()
        {
            m_DataVal = new List<WorldSpriteAnimation>();
        }

        public WorldSpriteAnimationListData(List<WorldSpriteAnimation> v)
        {
            m_DataVal = v;
        }
    }

}
