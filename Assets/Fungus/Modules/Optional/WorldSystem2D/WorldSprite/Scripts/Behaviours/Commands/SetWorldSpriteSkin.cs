﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the world sprite's current skin.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Skin")]
    public class SetWorldSpriteSkin : WorldSpriteCommand
    {
        [Tooltip("The skin to use")]
        [DataMin(0)]
        public IntegerData m_SkinIndex = new IntegerData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            int skin = m_SkinIndex.Value;

            if (worldSprite != null)
            {
                worldSprite.Skin = skin;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_SkinIndex);
        }
    }

}