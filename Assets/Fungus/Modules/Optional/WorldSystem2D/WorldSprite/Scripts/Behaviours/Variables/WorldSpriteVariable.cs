﻿using System;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/World Sprite Variable")]
    public class WorldSpriteVariable : Variable<WorldSprite>
    {
    }

    [Serializable]
    public class WorldSpriteData : VariableData<WorldSprite>
    {
        public WorldSpriteData()
        {
        }

        public WorldSpriteData(WorldSprite v)
        {
            m_DataVal = v;
        }
    }

}
