using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can be controlled by the player.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Controllable")]
    public class SetWorldSpriteControllable : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite can be controlled by the player")]
        public BooleanData m_Controllable = new BooleanData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_Controllable.Value;
            
            if (worldSprite != null)
            {
                worldSprite.Controllable = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_Controllable);
        }
    }

}