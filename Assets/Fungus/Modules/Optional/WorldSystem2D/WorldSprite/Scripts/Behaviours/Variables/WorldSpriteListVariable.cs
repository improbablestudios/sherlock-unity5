﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/World Sprite List Variable")]
    public class WorldSpriteListVariable : Variable<List<WorldSprite>>
    {
    }

    [Serializable]
    public class WorldSpriteListData : VariableData<List<WorldSprite>>
    {
        public WorldSpriteListData()
        {
            m_DataVal = new List<WorldSprite>();
        }

        public WorldSpriteListData(List<WorldSprite> v)
        {
            m_DataVal = v;
        }
    }

}
