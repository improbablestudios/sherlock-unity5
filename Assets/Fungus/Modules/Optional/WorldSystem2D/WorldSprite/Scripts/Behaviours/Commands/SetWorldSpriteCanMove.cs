﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can be moved by the player.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Can Move")]
    public class SetWorldSpriteCanMove : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite can be moved by the player")]
        public BooleanData m_CanMove = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_CanMove.Value;

            if (worldSprite != null)
            {
                worldSprite.CanMove = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_CanMove);
        }
    }

}