﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    public abstract class WorldSpriteCommand : Command
    {
        [Tooltip("The target world sprite")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_PixelSprite")]
        public WorldSpriteData m_WorldSprite = new WorldSpriteData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_WorldSprite);
        }

        public virtual WorldSpriteAnimation[] GetAnimations()
        {
            if (m_WorldSprite.IsConstant && m_WorldSprite.Value != null)
            {
                return m_WorldSprite.Value.Animations;
            }

            return new WorldSpriteAnimation[0];
        }
    }

}