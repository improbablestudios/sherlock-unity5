﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Animate a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Animate World Sprite")]
    public class AnimateWorldSprite : WorldSpriteCommand
    {
        [Tooltip("The animation to play")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_PixelAnimation")]
        public WorldSpriteAnimationData m_Animation = new WorldSpriteAnimationData();

        [Tooltip("The index of the animation to play")]
        [DataMin(0)]
        [FormerlySerializedAs("m_PixelAnimationIndex")]
        public IntegerData m_AnimationIndex = new IntegerData();

        [Tooltip("If true, play the animation looping")]
        public BooleanData m_Looping = new BooleanData();

        [Tooltip("The number of times to loop (Set to -1 to loop forever)")]
        [DataMin(-1)]
        public IntegerData m_LoopCount = new IntegerData(-1);
        
        [Tooltip("If true, play the animation backwards")]
        public BooleanData m_Backwards = new BooleanData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            WorldSpriteAnimation animation = m_Animation.Value;
            int animationIndex = m_AnimationIndex.Value;
            bool backwards = m_Backwards.Value;
            bool looping = m_Looping.Value;
            int loopCount = m_LoopCount.Value;

            if (!m_WorldSprite.IsConstant)
            {
                WorldSpriteAnimation[] worldSpriteAnimations = GetAnimations();
                if (animationIndex < 0 || animationIndex >= worldSpriteAnimations.Length)
                {
                    Log(DebugLogType.Error, "Invalid World Sprite Animation Index");
                    return;
                }
                else
                {
                    animation = worldSpriteAnimations[animationIndex];
                }
            }
            if (animation == null)
            {
                Log(DebugLogType.Error, "No World Sprite Animation");
                return;
            }

            if (!animation.CanFace(worldSprite.Direction))
            {
                Log(DebugLogType.Warning, "Pixel animation does not support this direction, using default direction instead");
                worldSprite.ChangeDirection(animation.Directions[0], false);
            }

            if (looping)
            {
                worldSprite.LoopAnimation(animation, loopCount, backwards, true, ContinueAfterFinished);
            }
            else
            {
                if (backwards)
                {
                    worldSprite.PlayAnimationBackwards(animation, true, ContinueAfterFinished);
                }
                else
                {
                    worldSprite.PlayAnimationForward(animation, true, ContinueAfterFinished);
                }
            }

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            string progression = "";
            if (!m_Backwards.IsConstant || m_Backwards.Value)
            {
                progression = " backwards";
            }

            if ((!m_Looping.IsConstant || m_Looping.Value) && (!m_LoopCount.IsConstant || m_LoopCount.Value > 1))
            {
                return string.Format("\"{0}\" {1}{2} looping {3} times", m_WorldSprite, m_Animation, progression, m_LoopCount);
            }
            if ((!m_Looping.IsConstant || m_Looping.Value) && (!m_LoopCount.IsConstant || m_LoopCount.Value < 0))
            {
                return string.Format("\"{0}\" {1}{2} looping forever", m_WorldSprite, m_Animation, progression);
            }

            return string.Format("\"{0}\" {1}{2}", m_WorldSprite, m_Animation, progression);
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Animation))
            {
                if (m_WorldSprite.IsConstant)
                {
                    if ((Application.isPlaying || m_Animation.IsConstant) && m_Animation.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Animation))
            {
                if (!m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AnimationIndex))
            {
                if (m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_LoopCount))
            {
                if (m_Looping.IsConstant && !m_Looping.Value)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}
