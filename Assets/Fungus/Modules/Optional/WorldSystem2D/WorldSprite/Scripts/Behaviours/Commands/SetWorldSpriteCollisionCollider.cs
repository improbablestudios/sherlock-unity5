﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Sets the collision collider of a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Collision Collider")]
    public class SetWorldSpriteCollisionCollider : WorldSpriteCommand
    {
        [Tooltip("The collider that is used to determine how a world sprite will collide with other colliders")]
        [CheckNotNullData]
        public Collider2DData m_CollisionCollider = new Collider2DData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            Collider2D collider2D = m_CollisionCollider.Value;

            if (worldSprite != null)
            {
                worldSprite.CollisionCollider = collider2D;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_CollisionCollider);
        }
    }

}