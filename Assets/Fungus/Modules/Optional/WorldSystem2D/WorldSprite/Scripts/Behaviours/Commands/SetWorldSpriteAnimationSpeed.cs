﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the speed the sprite will animate.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Animation Speed")]
    public class SetWorldSpriteAnimationSpeed : WorldSpriteCommand
    {
        [Tooltip("The speed this sprite will animate")]
        public FloatData m_AnimationSpeed = new FloatData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            float speed = m_AnimationSpeed.Value;

            if (worldSprite != null)
            {
                worldSprite.AnimationSpeed = speed;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_AnimationSpeed);
        }
    }

}