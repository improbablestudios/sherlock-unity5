﻿using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.WorldSystem2D;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Animate an emote over the world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Emote World Sprite")]
    public class EmoteWorldSprite : WorldSpriteCommand
    {
        [Tooltip("The emote animation to play")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_EmotePixelAnimation")]
        public WorldSpriteAnimationData m_EmoteAnimation = new WorldSpriteAnimationData();
        
        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            WorldSpriteAnimation emoteAnimation = m_EmoteAnimation.Value;
            
            worldSprite.Emote(emoteAnimation, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_EmoteAnimation);
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        public override string GetPropertyError(string propertyPath)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                if (propertyPath == nameof(m_EmoteAnimation))
                {
                    if (GetAnimations().Length == 0)
                    {
                        return "No emote world sprite animations found in assets";
                    }
                }
            }  
#endif

            return base.GetPropertyError(propertyPath);
        }

#if UNITY_EDITOR
        public override WorldSpriteAnimation[] GetAnimations()
        {
            return ResourceTracker.GetCachedPrefabObjectsOfType<WorldSpriteAnimation>().ToList().FindAll(i => i.transform.parent != null && i.transform.parent.name.ToLower().Contains("emote")).ToArray();
        }
#endif
    }

}
