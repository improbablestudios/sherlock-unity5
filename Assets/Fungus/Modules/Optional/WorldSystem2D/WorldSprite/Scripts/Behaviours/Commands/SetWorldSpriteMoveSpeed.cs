﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the speed at which the sprite will move in units per second.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Move Speed")]
    public class SetWorldSpriteMoveSpeed : WorldSpriteCommand
    {
        [Tooltip("The speed this sprite will move")]
        [DataMin(0)]
        public FloatData m_MoveSpeed = new FloatData(3f);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            float speed = m_MoveSpeed.Value;

            if (worldSprite != null)
            {
                worldSprite.MoveSpeed = speed;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_MoveSpeed);
        }
    }

}