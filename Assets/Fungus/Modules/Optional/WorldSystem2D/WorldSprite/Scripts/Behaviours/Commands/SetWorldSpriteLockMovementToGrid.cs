﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite is restricted to grid movement.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Lock Movement To Grid")]
    public class SetWorldSpriteLockMovementToGrid : WorldSpriteCommand
    {
        [Tooltip("Set to true to lock a world sprite's movement to the grid")]
        public BooleanData m_LockMovementToGrid = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_LockMovementToGrid.Value;

            if (worldSprite != null)
            {
                worldSprite.LockMovementToGrid = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_LockMovementToGrid);
        }
    }

}