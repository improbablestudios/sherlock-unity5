﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can be moved by the player clicking with their mouse.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Use Mouse Click To Move")]
    public class SetWorldSpriteUseMouseClickToMove : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite can be moved by the player clicking with their mouse")]
        public BooleanData m_UseMouseClickToMove = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_UseMouseClickToMove.Value;

            if (worldSprite != null)
            {
                worldSprite.UseMouseClickToMove = active;
            }

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_UseMouseClickToMove);
        }
    }

}