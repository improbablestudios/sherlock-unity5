﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Show a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Show World Sprite")]
    public class ShowWorldSprite : WorldSpriteCommand
    {
        [Tooltip("The animation to show a frame of")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_PixelAnimation")]
        public WorldSpriteAnimationData m_Animation = new WorldSpriteAnimationData();

        [Tooltip("The index of the animation to show a frame of")]
        [DataMin(-1)]
        [FormerlySerializedAs("m_PixelAnimationIndex")]
        public IntegerData m_AnimationIndex = new IntegerData();
        
        [Tooltip("The frame to display")]
        [DataMin(0)]
        public IntegerData m_Frame = new IntegerData();

        [Tooltip("The number of seconds to show this frame")]
        [DataMin(0)]
        public FloatData m_Duration = new FloatData();

        [Tooltip("Show sprite at its previous location")]
        public BooleanData m_AtPreviousPosition = new BooleanData(true);

        [Tooltip("Show sprite at this position")]
        public Vector2Data m_Position = new Vector2Data();
        
        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            WorldSpriteAnimation animation = m_Animation.Value;
            int animationIndex = m_AnimationIndex.Value;
            int frame = m_Frame.Value;
            float duration = m_Duration.Value;
            bool atPreviousPosition = m_AtPreviousPosition.Value;
            Vector2 position = m_Position.Value;

            if (worldSprite == null)
            {
                Log(DebugLogType.Error, "No World Sprite");
            }

            if (!m_WorldSprite.IsConstant)
            {
                WorldSpriteAnimation[] worldSpriteAnimations = GetAnimations();
                if (animationIndex < 0 || animationIndex >= worldSpriteAnimations.Length)
                {
                    Log(DebugLogType.Error, "Invalid World Sprite Animation Index");
                    return;
                }
                else
                {
                    animation = worldSpriteAnimations[animationIndex];
                }
            }
            if (animation == null)
            {
                Log(DebugLogType.Error, "No World Sprite Animation");
                return;
            }

            if (!animation.CanFace(worldSprite.Direction))
            {
                Log(DebugLogType.Warning, "Pixel animation does not support this direction, using default direction instead");
                worldSprite.ChangeDirection(animation.Directions[0], false);
            }

            if (!atPreviousPosition)
            {
                worldSprite.SetPosition(position);
            }

            worldSprite.ShowStaticFrame(animation, frame, duration, true, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            string worldSpriteAnimationNameString = m_Animation.ToString();

            if (!m_WorldSprite.IsConstant)
            {
                worldSpriteAnimationNameString = m_AnimationIndex.ToString();
            }

            return base.GetSummary() + string.Format(" {0} {1}", worldSpriteAnimationNameString, m_Frame);
        }

        public override bool ShouldWait(bool runtime)
        {
            if (((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0))
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Animation))
            {
                if (!m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AnimationIndex))
            {
                if (m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Position))
            {
                if (!m_AtPreviousPosition.IsConstant || m_AtPreviousPosition.Value)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}
