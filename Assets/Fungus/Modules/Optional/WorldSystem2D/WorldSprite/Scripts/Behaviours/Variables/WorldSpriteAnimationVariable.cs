﻿using System;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/World Sprite Animation Variable")]
    public class WorldSpriteAnimationVariable : Variable<WorldSpriteAnimation>
    {
    }

    [Serializable]
    public class WorldSpriteAnimationData : VariableData<WorldSpriteAnimation>
    {
        public WorldSpriteAnimationData()
        {
        }

        public WorldSpriteAnimationData(WorldSpriteAnimation v)
        {
            m_DataVal = v;
        }
    }

}
