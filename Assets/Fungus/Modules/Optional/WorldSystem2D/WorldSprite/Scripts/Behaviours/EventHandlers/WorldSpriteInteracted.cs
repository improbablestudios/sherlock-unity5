﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("World Sprite",
                      "This block will execute when a sprite interacts with a collider.")]
    [AddComponentMenu("Fungus/Event Handlers/World Sprite/World Sprite Interacted")]
    public class WorldSpriteInteracted : WorldSpriteEventHandler
    {
        public BooleanData m_WithAnything = new BooleanData();

        protected override bool HasTarget()
        {
            return !((Application.isPlaying || m_WithAnything.IsConstant) && m_WithAnything.Value);
        }

        protected override UnityEvent GetUnityEvent()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;

            if (worldSprite != null)
            {
                return worldSprite.OnInteracted;
            }

            return null;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            bool withAnything = m_WithAnything.Value;
            WorldSprite worldSprite = m_WorldSprite.Value;
            Collider2D targetCollider = GetTargetCollider(false);
            TargetType targetType = m_TargetType;
            WorldSprite targetWorldSprite = m_TargetWorldSprite.Value;

            if (withAnything || 
                worldSprite.LastInteractedCollider == targetCollider || 
                (targetType == TargetType.WorldSprite && 
                Array.IndexOf(targetWorldSprite.GetComponentsInChildren<Collider2D>(), worldSprite.LastInteractedCollider) >= 0))
            {
                return true;
            }

            return false;
        }

        public override string GetSummary()
        {
            string targetString = "Anything";
            if (!m_WithAnything.IsConstant || !m_WithAnything.Value)
            {
                targetString = GetTargetString();
            }
            return string.Format("{0} Interacted With {1}", m_WorldSprite, targetString);
        }
    }
}