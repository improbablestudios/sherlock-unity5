﻿using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("World Sprite",
                      "This block will execute when a sprite faces a collider.")]
    [AddComponentMenu("Fungus/Event Handlers/World Sprite/World Sprite Turned")]
    public class WorldSpriteTurned : WorldSpriteEventHandler
    {
        public BooleanData m_TowardAnywhere = new BooleanData();

        protected override bool HasTarget()
        {
            return !((Application.isPlaying || m_TowardAnywhere.IsConstant) && m_TowardAnywhere.Value);
        }

        protected override UnityEvent GetUnityEvent()
        {
            WorldSprite playerWorldSprite = m_WorldSprite.Value;

            return playerWorldSprite.OnTurned;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            bool towardAnywhere = m_TowardAnywhere.Value;
            WorldSprite playerWorldSprite = m_WorldSprite.Value;
            Collider2D targetCollider = GetTargetCollider(true);

            if (towardAnywhere || playerWorldSprite.IsFacing(targetCollider))
            {
                return true;
            }

            return false;
        }
        
        public override string GetSummary()
        {
            string targetString = "Anywhere";
            if (!m_TowardAnywhere.IsConstant || !m_TowardAnywhere.Value)
            {
                targetString = GetTargetString();
            }
            return string.Format("{0} Turned Toward {1}", m_WorldSprite, targetString);
        }
    }
}