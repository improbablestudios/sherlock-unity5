﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can interact with other world sprites using the submit button.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Use Mouse Click To Interact")]
    public class SetWorldSpriteUseMouseClickToInteract : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite will be able to interact with other world sprites using the submit button")]
        public BooleanData m_UseMouseClickToInteract = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_UseMouseClickToInteract.Value;

            if (worldSprite != null)
            {
                worldSprite.UseMouseClickToInteract = active;
            }

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_UseMouseClickToInteract);
        }
    }

}