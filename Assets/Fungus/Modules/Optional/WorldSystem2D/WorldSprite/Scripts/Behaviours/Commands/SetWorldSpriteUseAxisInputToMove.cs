﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can be moved by the player using axis input.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Use Axis Input To Move")]
    public class SetWorldSpriteUseAxisInputToMove : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite can be moved by the player using axis input")]
        public BooleanData m_UseAxisInputToMove = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_UseAxisInputToMove.Value;

            if (worldSprite != null)
            {
                worldSprite.UseAxisInputToMove = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_UseAxisInputToMove);
        }
    }

}