﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the world sprite animation that is played when the player uses controls to move the world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Controlled Move Animation")]
    public class SetWorldSpriteControlledMoveAnimation : WorldSpriteCommand
    {
        [Tooltip("The world sprite animation that is played when the player uses controls to move the world sprite")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_ControlledMovePixelAnimation")]
        public WorldSpriteAnimationData m_ControlledMoveAnimation = new WorldSpriteAnimationData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            WorldSpriteAnimation animation = m_ControlledMoveAnimation.Value;

            if (worldSprite != null)
            {
                worldSprite.ControlledMoveAnimation = animation;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_ControlledMoveAnimation);
        }
    }

}