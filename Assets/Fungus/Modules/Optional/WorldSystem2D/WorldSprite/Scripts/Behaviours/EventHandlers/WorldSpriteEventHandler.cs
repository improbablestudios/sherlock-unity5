﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.EventHandlers
{

    public abstract class WorldSpriteEventHandler : UnityEventHandler
    {
        public enum TargetType
        {
            WorldSprite,
            Position,
            Collider
        }

        [Tooltip("The player world sprite that will interact with the map environment")]
        [FormerlySerializedAs("_worldSprite")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_PixelSprite")]
        public WorldSpriteData m_WorldSprite = new WorldSpriteData();
        
        [Tooltip("The type of targetCollider that is being interacted with")]
        [FormerlySerializedAs("_targetType")]
        public TargetType m_TargetType;

        [Tooltip("The object that is being interacted with")]
        [FormerlySerializedAs("_targetWorldSprite")]
        [FormerlySerializedAs("m_TargetPixelSprite")]
        public WorldSpriteData m_TargetWorldSprite = new WorldSpriteData();

        [Tooltip("The position that is being interacted with")]
        [FormerlySerializedAs("_targetCoordinate")]
        [FormerlySerializedAs("m_TargetCoordinate")]
        public Vector2Data m_TargetPosition = new Vector2Data();

        [Tooltip("The collider that is being interacted with")]
        [FormerlySerializedAs("_targetCollider")]
        public Collider2DData m_TargetCollider = new Collider2DData();

        protected BoxCollider2D m_targetPositionCollider;

        protected virtual void Start()
        {
            if (m_TargetType == TargetType.Position)
            {
                if (m_targetPositionCollider == null)
                {
                    GameObject go = new GameObject("World Sprite Target", typeof(BoxCollider2D));
                    m_targetPositionCollider = go.GetComponent<BoxCollider2D>();
                    m_targetPositionCollider.offset = new Vector2(0.5f, 0.5f);
                    m_targetPositionCollider.size = new Vector2(1f, 1f);
                    m_targetPositionCollider.isTrigger = !CanCollideWithPosition();
                    m_targetPositionCollider.transform.position = m_TargetPosition.Value;
                }
            }
        }

        protected virtual bool CanCollideWithPosition()
        {
            return false;
        }

        protected virtual bool HasTarget()
        {
            return true;
        }

        protected Collider2D GetTargetCollider(bool useCollisionCollider)
        {
            switch (m_TargetType)
            {
                case TargetType.WorldSprite:
                    if (m_TargetWorldSprite.Value != null)
                    {
                        if (useCollisionCollider)
                        {
                            return m_TargetWorldSprite.Value.CollisionCollider;
                        }
                        else
                        {
                            return m_TargetWorldSprite.Value.InteractionCollider;
                        }
                    }
                    return null;
                case TargetType.Position:
                    return m_targetPositionCollider;
                case TargetType.Collider:
                    return m_TargetCollider.Value;

            }
            return null;
        }

        protected string GetTargetString()
        {
            switch (m_TargetType)
            {
                case TargetType.WorldSprite:
                    return m_TargetWorldSprite.ToString();
                case TargetType.Position:
                    return m_TargetPosition.ToString();
                case TargetType.Collider:
                    return m_TargetCollider.ToString();

            }
            return "<None>";
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetWorldSprite))
            {
                if (HasTarget() && m_TargetType == TargetType.WorldSprite)
                {
                    if ((Application.isPlaying || m_TargetWorldSprite.IsConstant) && m_TargetWorldSprite.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetCollider))
            {
                if (HasTarget() && m_TargetType == TargetType.Collider)
                {
                    if ((Application.isPlaying || m_TargetCollider.IsConstant) && m_TargetCollider.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetType))
            {
                if (!HasTarget())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetWorldSprite))
            {
                if (!HasTarget() || m_TargetType != TargetType.WorldSprite)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                if (!HasTarget() || m_TargetType != TargetType.Position)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetCollider))
            {
                if (!HasTarget() || m_TargetType != TargetType.Collider)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
        
        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetType))
            {
                return 1;
            }
            if (propertyPath == nameof(m_TargetWorldSprite))
            {
                return 1;
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                return 1;
            }
            if (propertyPath == nameof(m_TargetCollider))
            {
                return 1;
            }
            return base.GetPropertyOrder(propertyPath);
        }
    }

}