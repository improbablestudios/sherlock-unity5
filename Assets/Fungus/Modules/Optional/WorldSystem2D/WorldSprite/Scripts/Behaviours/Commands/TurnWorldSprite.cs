﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Turn a world sprite toward a direction, position, world sprite, or collider.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Turn World Sprite")]
    public class TurnWorldSprite : WorldSpriteCommand
    {
        public enum TargetType
        {
            Direction,
            Position,
            WorldSprite,
            Collider,
        }
        
        [Tooltip("The type of target to turn towards")]
        public TargetType m_TurnToward;
        
        [Tooltip("The direction to turn towards")]
        public CardinalDirectionData m_TargetDirection = new CardinalDirectionData();

        [Tooltip("The position to turn towards")]
        public Vector2Data m_TargetPosition = new Vector2Data();

        [Tooltip("The world sprite to turn towards")]
        [FormerlySerializedAs("m_TargetPixelSprite")]
        public WorldSpriteData m_TargetWorldSprite = new WorldSpriteData();

        [Tooltip("The collider to turn towards")]
        public Collider2DData m_TargetCollider = new Collider2DData();

        protected BoxCollider2D m_targetPositionCollider;

        protected virtual void Start()
        {
            if (m_TurnToward == TargetType.Position)
            {
                if (m_targetPositionCollider == null)
                {
                    GameObject go = new GameObject("World Sprite Target", typeof(BoxCollider2D));
                    m_targetPositionCollider = go.GetComponent<BoxCollider2D>();
                    m_targetPositionCollider.offset = new Vector2(0.5f, 0.5f);
                    m_targetPositionCollider.size = new Vector2(1f, 1f);
                    m_targetPositionCollider.isTrigger = true;
                    m_targetPositionCollider.transform.position = m_TargetPosition.Value;
                }
            }
        }

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            CardinalDirection targetDirection = m_TargetDirection.Value;
            Vector2 targetPosition = m_TargetPosition.Value;
            WorldSprite targetWorldSprite = m_TargetWorldSprite.Value;
            Collider2D targetCollider = m_TargetCollider.Value;

            if (m_TurnToward == TargetType.Direction)
            {
                worldSprite.ChangeDirection(targetDirection);
            }
            else if (m_TurnToward == TargetType.Position)
            {
                worldSprite.TurnToward(m_targetPositionCollider);
            }
            else if (m_TurnToward == TargetType.WorldSprite)
            {
                worldSprite.TurnToward(targetWorldSprite.CollisionCollider);
            }
            else if (m_TurnToward == TargetType.Collider)
            {
                worldSprite.TurnToward(targetCollider);
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            object targetObject = null;
            if (m_TurnToward == TargetType.Direction)
            {
                targetObject = m_TargetDirection;
            }
            else if (m_TurnToward == TargetType.Position)
            {
                targetObject = m_TargetPosition;
            }
            else if (m_TurnToward == TargetType.WorldSprite)
            {
                targetObject = m_TargetWorldSprite;
            }
            else if (m_TurnToward == TargetType.Collider)
            {
                targetObject = m_TargetCollider;
            }
            return base.GetSummary() + string.Format(" toward {0}", targetObject);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetWorldSprite))
            {
                if (m_TurnToward == TargetType.WorldSprite)
                {
                    if ((Application.isPlaying || m_TargetWorldSprite.IsConstant) && m_TargetWorldSprite.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetCollider))
            {
                if (m_TurnToward == TargetType.Collider)
                {
                    if ((Application.isPlaying || m_TargetCollider.IsConstant) && m_TargetCollider.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetDirection))
            {
                if (m_TurnToward != TargetType.Direction)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                if (m_TurnToward != TargetType.Position)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetWorldSprite))
            {
                if (m_TurnToward != TargetType.WorldSprite)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetCollider))
            {
                if (m_TurnToward != TargetType.Collider)
                {
                    return false;
                }
            }
            
            return base.IsPropertyVisible(propertyPath);
        }
    }

}
