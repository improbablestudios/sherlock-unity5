﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Hide a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Hide World Sprite")]
    public class HideWorldSprite : WorldSpriteCommand
    {
        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            
            worldSprite.Hide();

            Continue();
        }
    }

}
