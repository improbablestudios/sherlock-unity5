﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Move a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Move World Sprite")]
    public class MoveWorldSprite : WorldSpriteCommand
    {
        [Tooltip("The animation to play")]
        [CheckNotNullData]
        [FormerlySerializedAs("m_PixelAnimation")]
        public WorldSpriteAnimationData m_Animation = new WorldSpriteAnimationData();

        [Tooltip("The index of the animation to play")]
        [DataMin(-1)]
        [FormerlySerializedAs("m_PixelAnimationIndex")]
        public IntegerData m_AnimationIndex = new IntegerData();
        
        [Tooltip("Move the sprite to this position")]
        public Vector2Data m_Position = new Vector2Data();

        [Tooltip("If true, move this sprite relative to its current position")]
        public BooleanData m_Relative = new BooleanData();
        
        [Tooltip("If true, the sprite will face the direction they are moving")]
        public BooleanData m_FaceMoveDirection = new BooleanData(true);

        [Tooltip("The direction the world sprite is facing")]
        public CardinalDirectionData m_FacingDirection = new CardinalDirectionData();
        
        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            WorldSpriteAnimation animation = m_Animation.Value;
            int animationIndex = m_AnimationIndex.Value;
            Vector2 position = m_Position.Value;
            bool relative = m_Relative.Value;
            bool faceMoveDirection = m_FaceMoveDirection.Value;
            CardinalDirection facingDirection = m_FacingDirection.Value;

            if (!m_WorldSprite.IsConstant)
            {
                WorldSpriteAnimation[] worldSpriteAnimations = GetAnimations();
                if (animationIndex < 0 || animationIndex >= worldSpriteAnimations.Length)
                {
                    Log(DebugLogType.Error, "Invalid World Sprite Animation Index");
                    return;
                }
                else
                {
                    animation = worldSpriteAnimations[animationIndex];
                }
            }
            if (animation == null)
            {
                Log(DebugLogType.Error, "No World Sprite Animation");
                return;
            }

            if (relative)
            {
                position += (Vector2)worldSprite.transform.localPosition;
            }

            if (faceMoveDirection)
            {
                facingDirection = worldSprite.DirectionToFace(position);
            }
            
            if (!animation.CanFace(facingDirection))
            {
                facingDirection = animation.Directions[0];
                Log(DebugLogType.Warning, "Pixel animation does not support this direction, using default direction instead");
            }

            worldSprite.ChangeDirection(facingDirection, false);

            worldSprite.MoveAnimation(animation, position, true, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            if (m_Relative.IsConstant && m_Relative.Value)
            {
                if (!m_FaceMoveDirection.IsConstant || !m_FaceMoveDirection.Value)
                {
                    return base.GetSummary() + string.Format(" {0} {1} steps facing {2}", m_Animation, m_Position, m_FacingDirection);
                }
                return base.GetSummary() + string.Format(" {0} {1} steps", m_Animation, m_Position);
            }
            else
            {
                if (!m_FaceMoveDirection.IsConstant || !m_FaceMoveDirection.Value)
                {
                    return base.GetSummary() + string.Format(" {0} to {1} facing {2}", m_Animation, m_Position, m_FacingDirection);
                }
                return base.GetSummary() + string.Format(" {0} to {1}", m_Animation, m_Position);
            }
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Animation))
            {
                if (!m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_AnimationIndex))
            {
                if (m_WorldSprite.IsConstant)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_FacingDirection))
            {
                if (!m_FaceMoveDirection.IsConstant || m_FaceMoveDirection.Value)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}
