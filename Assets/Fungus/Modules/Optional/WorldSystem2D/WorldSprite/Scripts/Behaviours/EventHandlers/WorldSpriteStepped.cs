﻿using UnityEngine;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("World Sprite",
                      "This block will execute when a sprite steps on a collider.")]
    [AddComponentMenu("Fungus/Event Handlers/World Sprite/World Sprite Stepped")]
    public class WorldSpriteStepped : WorldSpriteEventHandler
    {
        public BooleanData m_OnAnything = new BooleanData();

        protected override bool HasTarget()
        {
            return !((Application.isPlaying || m_OnAnything.IsConstant) && m_OnAnything.Value);
        }

        protected override UnityEvent GetUnityEvent()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;

            if (worldSprite != null)
            {
                return worldSprite.OnStepped;
            }

            return null;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            bool onAnything = m_OnAnything.Value;
            WorldSprite worldSprite = m_WorldSprite.Value;
            Collider2D targetCollider = GetTargetCollider(true);

            if (onAnything || worldSprite.CollisionCollider.IsTouching(targetCollider))
            {
                return true;
            }

            return false;
        }

        public override string GetSummary()
        {
            string targetString = "Anything";
            if (!m_OnAnything.IsConstant || !m_OnAnything.Value)
            {
                targetString = GetTargetString();
            }
            return string.Format("{0} Stepped On {1}", m_WorldSprite, targetString);
        }
    }
}