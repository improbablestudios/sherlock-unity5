﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can be move diagonally.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Allow Diagonal Movement")]
    public class SetWorldSpriteAllowDiagonalMovement : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite will be able to move diagonally")]
        public BooleanData m_AllowDiagonalMovement = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_AllowDiagonalMovement.Value;

            if (worldSprite != null)
            {
                worldSprite.AllowDiagonalMovement = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_AllowDiagonalMovement);
        }
    }

}