﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite",
                 "Set the sorting layer of a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Set World Sprite Sorting Layer")]
    public class SetWorldSpriteSortingLayer : WorldSpriteCommand
    {
        [Tooltip("The new layer")]
        [SortingLayerDataField]
        public IntegerData m_SortingLayer = new IntegerData();
        
        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            int sortingLayer = m_SortingLayer.Value;

            worldSprite.ChangeSortingLayer(sortingLayer);

            Continue();
        }

        public override string GetSummary()
        {
            string sortingLayerString = m_SortingLayer.ToString();
            if (m_SortingLayer.IsConstant)
            {
                sortingLayerString = SortingLayer.IDToName(m_SortingLayer.Value);
            }
            return base.GetSummary() + string.Format(" to {0}", sortingLayerString);
        }
    }

}
