﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the interaction collider of a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Interaction Collider")]
    public class SetWorldSpriteInteractionCollider : WorldSpriteCommand
    {
        [Tooltip("The collider that is used to determine where a world sprite can be interacted with")]
        [CheckNotNullData]
        public Collider2DData m_InteractionCollider = new Collider2DData();

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            Collider2D collider2D = m_InteractionCollider.Value;

            if (worldSprite != null)
            {
                worldSprite.InteractionCollider = collider2D;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_InteractionCollider);
        }
    }

}