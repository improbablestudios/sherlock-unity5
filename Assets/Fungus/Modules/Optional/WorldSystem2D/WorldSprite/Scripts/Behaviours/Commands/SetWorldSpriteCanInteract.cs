﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can interact with other world sprites.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Can Interact")]
    public class SetWorldSpriteCanInteract : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite will be able to interact with other world sprites")]
        public BooleanData m_CanInteract = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_CanInteract.Value;

            if (worldSprite != null)
            {
                worldSprite.CanInteract = active;
            }

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_CanInteract);
        }
    }

}