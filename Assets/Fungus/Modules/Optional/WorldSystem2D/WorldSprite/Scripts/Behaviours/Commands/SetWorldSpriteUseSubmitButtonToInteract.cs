﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set whether or not a world sprite can interact with other world sprites using the submit button.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Use Submit Button To Interact")]
    public class SetWorldSpriteUseSubmitButtonToInteract : WorldSpriteCommand
    {
        [Tooltip("If true, the world sprite will be able to interact with other world sprites when the player presses the submit button")]
        public BooleanData m_UseSubmitButtonToInteract = new BooleanData(true);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            bool active = m_UseSubmitButtonToInteract.Value;

            if (worldSprite != null)
            {
                worldSprite.UseSubmitButtonToInteract = active;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_UseSubmitButtonToInteract);
        }
    }

}