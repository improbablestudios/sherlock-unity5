﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Sprite/Setup",
                 "Set the interaction reach of a world sprite.")]
    [AddComponentMenu("Fungus/Commands/World Sprite/Setup/Set World Sprite Interaction Reach")]
    public class SetWorldSpriteInteractionReach : WorldSpriteCommand
    {
        [Tooltip("Specifies how far (in units) the world sprite can reach in the direction it is currently facing (The sprite will be able to interact with any colliders that are within reach)")]
        [DataMin(0)]
        public FloatData m_InteractionReachDistance = new FloatData(0.4f);

        [Tooltip("Specifies the angle (in degrees) the world sprite can reach around the direction it is currently facing (The sprite will be able to interact with any colliders that are within reach)")]
        [DataRange(0, 360)]
        public IntegerData m_InteractionReachAngle = new IntegerData(45);

        [Tooltip("Specifies how smooth the interaction reach arc is in terms of how many points are used to create the arc (less points are more performant)")]
        [DataMin(1)]
        public IntegerData m_InteractionReachArcSmoothness = new IntegerData(20);

        public override void OnEnter()
        {
            WorldSprite worldSprite = m_WorldSprite.Value;
            float interactionReachDistance = m_InteractionReachDistance.Value;
            int interactionReachAngle = m_InteractionReachAngle.Value;
            int interactionReachArcSmoothness = m_InteractionReachArcSmoothness.Value;

            if (worldSprite != null)
            {
                worldSprite.InteractionReachDistance = interactionReachDistance;
                worldSprite.InteractionReachAngle = interactionReachAngle;
                worldSprite.InteractionReachArcSmoothness = interactionReachArcSmoothness;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" distance = {0}, angle = {1}, arc smoothness = {2}", m_InteractionReachDistance, m_InteractionReachAngle, m_InteractionReachArcSmoothness);
        }
    }

}