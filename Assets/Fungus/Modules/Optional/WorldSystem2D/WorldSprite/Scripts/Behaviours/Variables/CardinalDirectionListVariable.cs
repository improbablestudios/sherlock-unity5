﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Cardinal Direction List Variable")]
    public class CardinalDirectionListVariable : Variable<List<CardinalDirection>>
    {
    }

    [Serializable]
    public class CardinalDirectionListData : VariableData<List<CardinalDirection>>
    {
        public CardinalDirectionListData()
        {
            m_DataVal = new List<CardinalDirection>();
        }

        public CardinalDirectionListData(List<CardinalDirection> v)
        {
            m_DataVal = v;
        }
    }

}