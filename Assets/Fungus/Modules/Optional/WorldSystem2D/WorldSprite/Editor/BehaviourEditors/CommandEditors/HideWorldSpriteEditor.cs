#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(HideWorldSprite), true)]
    public class HideWorldSpriteEditor : WorldSpriteCommandEditor
    {
        protected override void UpdateWorldSpriteScenePreview()
        {
            if (!Application.isPlaying)
            {
                HideWorldSprite t = target as HideWorldSprite;

                if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null)
                {
                    m_PreviewWorldSprite.gameObject.SetActive(false);
                    UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                }
            }
        }
    }

}

#endif