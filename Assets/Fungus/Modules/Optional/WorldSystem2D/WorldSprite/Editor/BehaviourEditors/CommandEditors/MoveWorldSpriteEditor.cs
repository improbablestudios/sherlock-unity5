#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using Fungus.Commands;
using System;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(MoveWorldSprite), true)]
    public class MoveWorldSpriteEditor : WorldSpriteCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            MoveWorldSprite t = target as MoveWorldSprite;

            if (property.name == nameof(t.m_Animation))
            {
                VariableEditor.VariableDataField<WorldSpriteAnimation>(position, property, label, WorldSpriteAnimationPopup);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected override void DrawWorldSpritePreview()
        {
            MoveWorldSprite t = target as MoveWorldSprite;

            if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null &&
                t.m_Animation.IsConstant && t.m_Animation.Value != null)
            {
                SerializedProperty worldSpriteAnimationProp = serializedObject.FindProperty(nameof(t.m_Animation));
                
                DrawPreviewToolbar(worldSpriteAnimationProp, m_worldSpriteAnimations.ToArray());
  
                WorldSpriteAnimationEditor.DrawPreviewToolbar(t.m_Animation.Value);
                WorldSprite worldSprite = t.m_WorldSprite.Value;
                worldSprite.PreviewSpeed = 1f;
                worldSprite.PreviewAnimation = t.m_Animation.Value;
                worldSprite.PreviewSkinIndex = PreviewSkinIndexField(t.m_Animation.Value, worldSprite.PreviewSkinIndex);
                if (worldSprite.PreviewFrameSprite != null)
                {
                    WorldSpriteAnimationEditor.DrawWorldSpriteAnimationPreview(t.m_Animation.Value, worldSprite.PreviewAnimation.ConvertDirectionToRow(worldSprite.PreviewDirection), worldSprite.PreviewColumn);
                }
            }
        }

        protected override void UpdateWorldSpriteScenePreview()
        {
            if (!Application.isPlaying)
            {
                MoveWorldSprite t = target as MoveWorldSprite;

                if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null &&
                    t.m_Animation.IsConstant && t.m_Animation.Value != null)
                {
                    m_PreviewWorldSprite.gameObject.SetActive(true);

                    Vector2 position = Vector2.zero;
                    if (t.m_Position.IsConstant)
                    {
                        if (t.m_Relative.IsConstant && t.m_Relative.Value)
                        {
                            position = m_PreviewWorldSprite.transform.localPosition + (Vector3)t.m_Position.Value;
                        }   
                        else
                        {
                            position = t.m_Position.Value;
                        }
                    }

                    WorldSprite previewWorldSprite = t.m_WorldSprite.Value;

                    if (position != (Vector2)m_PreviewWorldSprite.transform.localPosition)
                    {
                        CardinalDirection direction = m_PreviewWorldSprite.DirectionToFace(position);
                        previewWorldSprite.PreviewDirection = direction;
                    }

                    WorldSprite worldSprite = t.m_WorldSprite.Value;
                    if (worldSprite.PreviewFrameSprite != null)
                    {
                        m_PreviewWorldSprite.Direction = worldSprite.PreviewDirection;
                        int animationIndex = Array.IndexOf(t.m_WorldSprite.Value.Animations, t.m_Animation.Value);
                        WorldSpriteAnimation worldSpriteAnimation = m_PreviewWorldSprite.Animations[animationIndex];
                        m_PreviewWorldSprite.ActivateAnimation(worldSpriteAnimation);
                        m_PreviewWorldSprite.SetSpriteFrame(worldSpriteAnimation, worldSprite.PreviewColumn, WorldSpriteAnimationProgression.Static);
                        foreach (SpriteRenderer spriteRenderer in m_PreviewWorldSprite.SpriteRenderers)
                        {
                            EditorUtility.SetDirty(spriteRenderer);
                        }
                        UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                    }

                    m_PreviewWorldSprite.transform.localPosition = position;
                }
            }
        }
    }

}

#endif