#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Fungus.Commands;
using System;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(TurnWorldSprite), true)]
    public class TurnWorldSpriteEditor : WorldSpriteCommandEditor
    {
        protected override void UpdateWorldSpriteScenePreview()
        {
            if (!Application.isPlaying)
            {
                TurnWorldSprite t = target as TurnWorldSprite;

                if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null)
                {
                    if (t.m_TurnToward == TurnWorldSprite.TargetType.Direction)
                    {
                        CardinalDirection direction = t.m_TargetDirection.Value;
                        WorldSprite worldSprite = t.m_WorldSprite.Value;
                        worldSprite.PreviewDirection = direction;

                        if (t.m_WorldSprite.Value.PreviewAnimation != null && worldSprite.PreviewFrameSprite != null)
                        {
                            m_PreviewWorldSprite.Direction = worldSprite.PreviewDirection;
                            int animationIndex = Array.IndexOf(t.m_WorldSprite.Value.Animations, t.m_WorldSprite.Value.PreviewAnimation);
                            WorldSpriteAnimation worldSpriteAnimation = m_PreviewWorldSprite.Animations[animationIndex];
                            m_PreviewWorldSprite.ActivateAnimation(worldSpriteAnimation);
                            m_PreviewWorldSprite.SetSpriteFrame(worldSpriteAnimation, worldSprite.PreviewColumn, WorldSpriteAnimationProgression.Static);
                            foreach (SpriteRenderer spriteRenderer in m_PreviewWorldSprite.SpriteRenderers)
                            {
                                EditorUtility.SetDirty(spriteRenderer);
                            }
                            UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                        }
                    }
                }
            }
        }
    }

}

#endif