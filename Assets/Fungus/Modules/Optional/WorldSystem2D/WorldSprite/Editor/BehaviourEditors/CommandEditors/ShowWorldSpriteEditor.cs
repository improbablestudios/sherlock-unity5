#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using Fungus.Commands;
using System;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(ShowWorldSprite), true)]
    public class ShowWorldSpriteEditor : WorldSpriteCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            ShowWorldSprite t = target as ShowWorldSprite;

            if (property.name == nameof(t.m_Animation))
            {
                VariableEditor.VariableDataField<WorldSpriteAnimation>(position, property, label, WorldSpriteAnimationPopup);
            }
            else if (property.name == nameof(t.m_Frame))
            {
                VariableEditor.VariableDataField<int>(position, property, label, FramePopup);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected override void DrawWorldSpritePreview()
        {
            ShowWorldSprite t = target as ShowWorldSprite;

            if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null &&
                t.m_Animation.IsConstant && t.m_Animation.Value != null)
            {
                SerializedProperty worldSpriteAnimationProp = serializedObject.FindProperty(nameof(t.m_Animation));

                DrawPreviewToolbar(worldSpriteAnimationProp, m_worldSpriteAnimations.ToArray());
                WorldSprite worldSprite = t.m_WorldSprite.Value;
                worldSprite.PreviewColumn = t.m_Frame.Value;
                worldSprite.PreviewSkinIndex = PreviewSkinIndexField(t.m_Animation.Value, worldSprite.PreviewSkinIndex);
                worldSprite.PreviewDirection = PreviewDirectionField(t.m_Animation.Value, worldSprite.PreviewDirection);
                if (worldSprite.PreviewFrameSprite != null)
                {
                    WorldSpriteAnimationEditor.DrawWorldSpriteAnimationPreview(t.m_Animation.Value, worldSprite.PreviewAnimation.ConvertDirectionToRow(worldSprite.PreviewDirection), worldSprite.PreviewColumn);
                }
            }
        }

        protected override void UpdateWorldSpriteScenePreview()
        {
            if (!Application.isPlaying)
            {
                ShowWorldSprite t = target as ShowWorldSprite;

                if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null &&
                t.m_Animation.IsConstant && t.m_Animation.Value != null)
                {
                    m_PreviewWorldSprite.gameObject.SetActive(true);

                    WorldSprite previewWorldSprite = t.m_WorldSprite.Value;
                    previewWorldSprite.PreviewSpeed = 1f;
                    previewWorldSprite.PreviewAnimation = t.m_Animation.Value;
                    previewWorldSprite.PreviewColumn = t.m_Frame.Value;

                    if (t.m_AtPreviousPosition.IsConstant && !t.m_AtPreviousPosition.Value)
                    {
                        if (t.m_Position.IsConstant)
                        {
                            m_PreviewWorldSprite.transform.localPosition = t.m_Position.Value;
                        }
                    }

                    WorldSprite worldSprite = t.m_WorldSprite.Value;
                    if (worldSprite.PreviewFrameSprite != null)
                    {
                        m_PreviewWorldSprite.Direction = worldSprite.PreviewDirection;
                        int animationIndex = Array.IndexOf(t.m_WorldSprite.Value.Animations, t.m_Animation.Value);
                        WorldSpriteAnimation worldSpriteAnimation = m_PreviewWorldSprite.Animations[animationIndex];
                        m_PreviewWorldSprite.ActivateAnimation(worldSpriteAnimation);
                        m_PreviewWorldSprite.SetSpriteFrame(worldSpriteAnimation, worldSprite.PreviewColumn, WorldSpriteAnimationProgression.Static);
                        foreach (SpriteRenderer spriteRenderer in m_PreviewWorldSprite.SpriteRenderers)
                        {
                            EditorUtility.SetDirty(spriteRenderer);
                        }
                        UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                    }
                }
            }
        }

        protected void FramePopup(Rect position, SerializedProperty property, GUIContent label)
        {
            ShowWorldSprite t = target as ShowWorldSprite;

            if (t.m_WorldSprite.IsConstant && t.m_AnimationIndex.IsConstant && t.m_Animation.Value != null)
            {
                EditorGUI.IntSlider(position, property, 0, t.m_Animation.Value.NumberOfColumns - 1, label);
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }
    }

}

#endif