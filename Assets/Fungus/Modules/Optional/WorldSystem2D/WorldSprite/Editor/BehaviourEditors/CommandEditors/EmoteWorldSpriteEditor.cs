#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Fungus.Commands;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(EmoteWorldSprite), true)]
    public class EmoteWorldSpriteEditor : WorldSpriteCommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EmoteWorldSprite t = target as EmoteWorldSprite;

            if (property.name == nameof(t.m_EmoteAnimation))
            {
                VariableEditor.VariableDataField<WorldSpriteAnimation>(position, property, label, WorldSpriteAnimationPopup);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected override void DrawWorldSpritePreview()
        {
            EmoteWorldSprite t = target as EmoteWorldSprite;

            if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null &&
                t.m_EmoteAnimation.IsConstant && t.m_EmoteAnimation.Value != null)
            {
                SerializedProperty worldSpriteAnimationProp = serializedObject.FindProperty(nameof(t.m_EmoteAnimation));

                m_worldSpriteAnimations = t.GetAnimations();

                DrawPreviewToolbar(worldSpriteAnimationProp, m_worldSpriteAnimations.ToArray());

                WorldSpriteAnimationEditor.DrawPreviewToolbar(t.m_EmoteAnimation.Value);
                WorldSprite worldSprite = t.m_EmoteAnimation.Value.WorldSprite;
                worldSprite.PreviewAnimation = t.m_EmoteAnimation.Value;
                worldSprite.PreviewSpeed = 1f;
                worldSprite.PreviewSkinIndex = 0;
                worldSprite.PreviewDirection = CardinalDirection.South;
                if (worldSprite.PreviewFrameSprite != null)
                {
                    WorldSpriteAnimationEditor.DrawWorldSpriteAnimationPreview(t.m_EmoteAnimation.Value, worldSprite.PreviewAnimation.ConvertDirectionToRow(worldSprite.PreviewDirection), worldSprite.PreviewColumn);
                }
            }
        }
    }

}

#endif