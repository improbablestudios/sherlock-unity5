#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Fungus.Commands;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.WorldSystem2D;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(WorldSpriteCommand), true)]
    public class WorldSpriteCommandEditor : CommandEditor
    {
        protected WorldSpriteAnimation[] m_worldSpriteAnimations = new WorldSpriteAnimation[0];
        
        public static WorldSprite m_PreviewWorldSprite;
        
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            m_worldSpriteAnimations = GetWorldSpriteAnimations();

            base.DrawProperty(position, property, label);
        }

        public override sealed void DrawPreview()
        {
            BaseDrawWorldSpritePreview();
            BaseUpdateWorldSpriteScenePreview();
        }

        private void BaseDrawWorldSpritePreview()
        {
            if (serializedObject != null)
            {
                WorldSpriteCommand t = target as WorldSpriteCommand;
                WorldSpriteAnimationEditor.PreviewWorldSprite = t.m_WorldSprite.Value;
                DrawWorldSpritePreview();
            }
        }

        private void BaseUpdateWorldSpriteScenePreview()
        {
            if (serializedObject != null)
            {
                WorldSpriteCommand t = target as WorldSpriteCommand;

                if (!Application.isPlaying)
                {
                    if (t.m_WorldSprite.IsConstant && t.m_WorldSprite.Value != null)
                    {
                        m_PreviewWorldSprite = PreviewManager.GetPreviewObject(m_PreviewWorldSprite, t.m_WorldSprite.Value);
                    }

                    if (m_PreviewWorldSprite != null)
                    {
                        UpdateWorldSpriteScenePreview();
                    }
                }
            }
        }
        
        protected virtual void DrawWorldSpritePreview()
        {
        }

        protected virtual void UpdateWorldSpriteScenePreview()
        {
        }

        protected GUIContent[] GetSkinNameLabels()
        {
            WorldSpriteCommand t = target as WorldSpriteCommand;
            return t.m_WorldSprite.Value.SkinNames.Select(i => new GUIContent(i)).ToArray();
        }
        
        protected void WorldSpriteAnimationPopup(Rect position, SerializedProperty property, GUIContent label)
        {
            SearchPopupGUI.ObjectSearchPopupField(position, property, label, null, m_worldSpriteAnimations);
        }

        protected int PreviewSkinIndexField(WorldSpriteAnimation WorldSpriteAnimation, int previewSkinIndex)
        {
            GUIContent[] skinLabels = GetSkinNameLabels();
            List<int> animationSkinIndices = new List<int>();
            List<GUIContent> animationSkinLabels = new List<GUIContent>();
            for (int i = 0; i < WorldSpriteAnimation.Skins.Length; i++)
            {
                if (WorldSpriteAnimation.Skins[i].m_SpriteSheet != null)
                {
                    if (i >= 0 && i < skinLabels.Length)
                    {
                        animationSkinLabels.Add(skinLabels[i]);
                        animationSkinIndices.Add(i);
                    }
                }
            }

            EditorGUI.BeginChangeCheck();

            int selectedIndex = animationSkinIndices.IndexOf(previewSkinIndex);

            selectedIndex = EditorGUILayout.Popup(new GUIContent("Preview Skin"), selectedIndex, animationSkinLabels.ToArray());

            if (EditorGUI.EndChangeCheck())
            {
                previewSkinIndex = animationSkinIndices[selectedIndex];
            }

            if (previewSkinIndex < 0 || previewSkinIndex >= WorldSpriteAnimation.Skins.Length)
            {
                previewSkinIndex = 0;
            }

            if (WorldSpriteAnimation.Skins[previewSkinIndex].m_SpriteSheet == null)
            {
                previewSkinIndex = WorldSpriteAnimation.GetFirstValidSkinIndex();
            }

            if (previewSkinIndex < 0 || previewSkinIndex >= WorldSpriteAnimation.Skins.Length)
            {
                previewSkinIndex = -1;
            }

            return previewSkinIndex;
        }

        protected virtual WorldSpriteAnimation[] GetWorldSpriteAnimations()
        {
            WorldSpriteCommand t = target as WorldSpriteCommand;

            return t.GetAnimations();
        }

        protected CardinalDirection PreviewDirectionField(WorldSpriteAnimation WorldSpriteAnimation, CardinalDirection previewDirection)
        {
            int previewRow = Array.IndexOf(WorldSpriteAnimation.Directions, previewDirection);
            return WorldSpriteAnimation.Directions[PreviewRowField(WorldSpriteAnimation, previewRow)];
        }

        protected int PreviewRowField(WorldSpriteAnimation WorldSpriteAnimation, int previewRow)
        {
            previewRow = EditorGUILayout.Popup("Preview Direction", previewRow, WorldSpriteAnimation.Directions.Select(i => i.ToString()).ToArray());

            if (previewRow < 0 || previewRow >= WorldSpriteAnimation.Directions.Length)
            {
                previewRow = 0;
            }

            return previewRow;
        }
    }

}

#endif