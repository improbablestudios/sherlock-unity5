#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(WorldMapCommand), true)]
    public class WorldMapCommandEditor : CommandEditor
    {
        private Editor m_cachedGameObjectEditor;
        private WorldMap m_currentWorldMap;

        public Transform m_PreviewRoot;
        public WorldMap m_PreviewWorldMap;
        public static WorldMap m_ActivePreviewWorldMap;

        public override void DrawNormalInspector()
        {
            base.DrawNormalInspector();

            WorldMapCommand t = target as WorldMapCommand;

            if (t.m_WorldMap.IsConstant && t.m_WorldMap.Value != null)
            {
                if (m_currentWorldMap != t.m_WorldMap.Value)
                {
                    FocusOnMap(t.m_WorldMap.Value);
                }
                m_currentWorldMap = t.m_WorldMap.Value;
            }
        }

        public override sealed void DrawPreview()
        {
            BaseDrawWorldMapPreview();
            BaseUpdateWorldMapScenePreview();
        }

        private void BaseDrawWorldMapPreview()
        {
            DrawWorldMapPreview();

            Repaint();
        }

        private void BaseUpdateWorldMapScenePreview()
        {
            if (!Application.isPlaying)
            {
                UpdateWorldMapScenePreview();
            }
        }

        protected virtual void DrawWorldMapPreview()
        {
        }

        protected virtual void UpdateWorldMapScenePreview()
        {
            WorldMapCommand t = target as WorldMapCommand;

            if (t.m_WorldMap.IsConstant && t.m_WorldMap.Value != null)
            {
                m_PreviewWorldMap = PreviewManager.GetPreviewObject(m_PreviewWorldMap, t.m_WorldMap.Value);
            }
        }

        public void FocusOnMap(WorldMap worldMap)
        {
            if (worldMap.gameObject.scene.IsValid())
            {
                UnityEngine.Object[] selectedObjects = Selection.objects.ToArray();
                Selection.activeGameObject = worldMap.gameObject;
                if (SceneView.lastActiveSceneView != null)
                {
                    SceneView.lastActiveSceneView.FrameSelected();
                }
                Selection.objects = selectedObjects;
            }
        }

        public void DrawMapPreview(WorldMap worldMap)
        {
            Vector2 mapSize = worldMap.MapSize;
            if (worldMap.Background != null && worldMap.Background.sprite != null)
            {
                mapSize = worldMap.Background.sprite.rect.size;
            }

            float aspect = mapSize.x / mapSize.y;

            using (new EditorGUILayout.VerticalScope())
            {
                Rect position = GUILayoutUtility.GetAspectRect(aspect);

                GameObject root = worldMap.gameObject;
                root = PrefabUtility.GetOutermostPrefabInstanceRoot(root);
                if (root != null)
                {
                    if (m_cachedGameObjectEditor == null || m_cachedGameObjectEditor.target != root)
                    {
                        if (m_cachedGameObjectEditor != null)
                        {
                            DestroyImmediate(m_cachedGameObjectEditor);
                        }
                        m_cachedGameObjectEditor = CreateEditor(root);
                    }

                    if (m_cachedGameObjectEditor != null && m_cachedGameObjectEditor.target != null)
                    {
                        using (new GUI.GroupScope(new Rect(position.x, position.y, mapSize.x, mapSize.y)))
                        {
                            GUIStyle style = new GUIStyle();
                            style.normal.background = Texture2D.blackTexture;
                            m_cachedGameObjectEditor.OnPreviewGUI(new Rect(0, 0, position.width, position.height), style);
                        }
                    }
                }
            }
        }
    }

}

#endif