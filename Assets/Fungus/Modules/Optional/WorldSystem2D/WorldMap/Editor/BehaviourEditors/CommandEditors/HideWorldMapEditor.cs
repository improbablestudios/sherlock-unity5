#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.WorldSystem2D;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(HideWorldMap), true)]
    public class HideWorldMapEditor : WorldMapCommandEditor
    {
        protected override void DrawWorldMapPreview()
        {
            HideWorldMap t = target as HideWorldMap;

            if (t.m_WorldMap.IsConstant && t.m_WorldMap.Value != null)
            {
                SerializedProperty worldMapProp = serializedObject.FindProperty(nameof(t.m_WorldMap));
                DrawPreviewToolbar(worldMapProp, ResourceTracker.GetCachedPrefabObjectsOfType<WorldMap>().ToArray());
                DrawMapPreview(t.m_WorldMap.Value);
            }
        }

        protected override void UpdateWorldMapScenePreview()
        {
            base.UpdateWorldMapScenePreview();
            if (m_PreviewWorldMap != null)
            {
                m_PreviewWorldMap.gameObject.SetActive(false);
                foreach (WorldSprite worldSprite in PersistentBehaviour.GetAllActive<WorldSprite>())
                {
                    worldSprite.gameObject.SetActive(false);
                }
                UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
            }
        }

    }

}

#endif