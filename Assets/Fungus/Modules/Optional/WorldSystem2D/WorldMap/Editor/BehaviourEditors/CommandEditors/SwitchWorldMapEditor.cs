#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.WorldSystem2D;
using ImprobableStudios.ResourceTrackingUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SwitchWorldMap), true)]
    public class SwitchWorldMapEditor : WorldMapCommandEditor
    {
        protected override void DrawWorldMapPreview()
        {
            SwitchWorldMap t = target as SwitchWorldMap;

            if (t.m_WorldMap.IsConstant && t.m_WorldMap.Value != null)
            {
                SerializedProperty worldMapProp = serializedObject.FindProperty(nameof(t.m_WorldMap));
                DrawPreviewToolbar(worldMapProp, ResourceTracker.GetCachedPrefabObjectsOfType<WorldMap>().ToArray());
                DrawMapPreview(t.m_WorldMap.Value);
            }
        }

        protected override void UpdateWorldMapScenePreview()
        {
            if (m_ActivePreviewWorldMap != null)
            {
                m_ActivePreviewWorldMap.gameObject.SetActive(false);
                foreach (WorldSprite worldSprite in PersistentBehaviour.GetAllActive<WorldSprite>())
                {
                    worldSprite.gameObject.SetActive(false);
                }
            }

            base.UpdateWorldMapScenePreview();

            if (m_PreviewWorldMap != null)
            {
                if (!m_PreviewWorldMap.gameObject.activeSelf)
                {
                    m_PreviewWorldMap.gameObject.SetActive(true);
                    FocusOnMap(m_PreviewWorldMap);
                    UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                }

                m_ActivePreviewWorldMap = m_PreviewWorldMap;
            }
        }

    }

}

#endif