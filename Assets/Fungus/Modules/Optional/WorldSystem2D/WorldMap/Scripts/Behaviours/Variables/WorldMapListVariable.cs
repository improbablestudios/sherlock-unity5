﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/World Map List Variable")]
    public class WorldMapListVariable : Variable<List<WorldMap>>
    {
    }

    [Serializable]
    public class WorldMapListData : VariableData<List<WorldMap>>
    {
        public WorldMapListData()
        {
            m_DataVal = new List<WorldMap>();
        }

        public WorldMapListData(List<WorldMap> v)
        {
            m_DataVal = v;
        }
    }

}
