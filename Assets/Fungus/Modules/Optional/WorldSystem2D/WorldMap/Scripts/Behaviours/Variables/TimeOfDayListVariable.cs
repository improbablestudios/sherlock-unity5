﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Time Of Day List Variable")]
    public class TimeOfDayListVariable : Variable<List<TimeOfDay>>
    {
    }

    [Serializable]
    public class TimeOfDayListData : VariableData<List<TimeOfDay>>
    {
        public TimeOfDayListData()
        {
            m_DataVal = new List<TimeOfDay>();
        }

        public TimeOfDayListData(List<TimeOfDay> v)
        {
            m_DataVal = v;
        }
    }

}