﻿using System;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Time Of Day Variable")]
    public class TimeOfDayVariable : Variable<TimeOfDay>
    {
    }

    [Serializable]
    public class TimeOfDayData : VariableData<TimeOfDay>
    {
        public TimeOfDayData()
        {
        }

        public TimeOfDayData(TimeOfDay v)
        {
            m_DataVal = v;
        }
    }

}