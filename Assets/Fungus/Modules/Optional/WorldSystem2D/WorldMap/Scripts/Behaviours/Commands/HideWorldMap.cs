﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Map",
                 "Hide a world map.")]
    [AddComponentMenu("Fungus/Commands/World Map/Hide World Map")]
    public class HideWorldMap : WorldMapCommand
    {
        public override void OnEnter()
        {
            WorldMap worldMap = m_WorldMap.Value;

            if (worldMap == null)
            {
                worldMap = GetPrimaryActive<WorldMap>();
            }

            if (worldMap != null)
            {
                worldMap.Hide();
            }

            Continue();
        }
    }

}