﻿using System;
using UnityEngine;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/World Map Variable")]
    public class WorldMapVariable : Variable<WorldMap>
    {
    }

    [Serializable]
    public class WorldMapData : VariableData<WorldMap>
    {
        public WorldMapData()
        {
        }

        public WorldMapData(WorldMap v)
        {
            m_DataVal = v;
        }
    }

}
