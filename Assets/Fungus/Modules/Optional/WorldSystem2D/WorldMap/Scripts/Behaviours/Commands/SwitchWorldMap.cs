﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    [CommandInfo("World Map",
                 "Hide the last activated world map, and show a new world map.")]
    [AddComponentMenu("Fungus/Commands/World Map/Switch World Map")]
    public class SwitchWorldMap : WorldMapCommand
    {
        public override void OnEnter()
        {
            WorldMap worldMap = m_WorldMap.Value;
            
            if (worldMap != null)
            {
                worldMap.Switch();
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("Current with \"{0}\"", m_WorldMap);
        }
    }

}