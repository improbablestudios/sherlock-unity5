﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.WorldSystem2D;

namespace Fungus.Commands
{

    public abstract class WorldMapCommand : Command
    {
        [Tooltip("The target world map")]
        [ObjectDataPopup("<Previous>")]
        [FormerlySerializedAs("m_PixelMap")]
        public WorldMapData m_WorldMap = new WorldMapData();
        
        public override string GetSummary()
        {
            string worldMapName = "<Previous>";
            if (!m_WorldMap.IsConstant || m_WorldMap.Value != null)
            {
                worldMapName = m_WorldMap.ToString();
            }
            return string.Format("\"{0}\"", worldMapName);
        }
    }

}