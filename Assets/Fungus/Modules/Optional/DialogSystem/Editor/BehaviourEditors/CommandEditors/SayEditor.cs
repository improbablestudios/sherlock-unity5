#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Fungus.Commands;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.UIUtilities;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.DialogSystem;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus.CommandEditors
{
    
    [CanEditMultipleObjects, CustomEditor(typeof(Say), true)]
    public class SayEditor : CommandEditor
    {
        private GUIContent m_textTooLongWarningLabel = new GUIContent("Warning: The text is too long to fit in the selected text box.");

        static public bool s_ShowTagHelp;
        
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label) 
        {
            Say t = target as Say;

            if (property.name == nameof(t.m_DialogText))
            {
                position.height -= GetExtraDialogTextHeight();
                base.DrawProperty(position, property, label);

                if (IsTextTooLongToFit())
                {
                    position.y += position.height;
                    position.height = GetTextTooLongToFitWarningHeight();
                    DrawTextTooLongToFitWarning(position);
                }

                if (t.m_DialogText.IsConstant)
                {
                    position.y += position.height;
                    position.height = GetTagHelpHeight();
                    DrawTagHelp(position);
                }

                EditorGUIUtility.labelWidth = 0;
            }
            else if (property.name == nameof(t.m_Portrait))
            {
                VariableEditor.VariableDataField<Sprite>(position, property, label, ImagePopup);
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            Say t = target as Say;

            if (property.name == nameof(t.m_DialogText))
            {
                return base.GetPropertyHeight(property, label) + GetExtraDialogTextHeight();
            }

            return base.GetPropertyHeight(property, label);
        }

        protected float GetExtraDialogTextHeight()
        {
            Say t = target as Say;

            float height = 0f;

            if (IsTextTooLongToFit())
            {
                height += GetTextTooLongToFitWarningHeight();
            }

            if (t.m_DialogText.IsConstant)
            {
                height += GetTagHelpHeight();
            }

            return height;
        }

        protected void ImagePopup(Rect position, SerializedProperty property, GUIContent label)
        {
            Say t = target as Say;
            if (t.m_Character.Value != null)
            {
                SearchPopupGUI.ObjectSearchPopupField<Sprite>(position, property, label, new GUIContent("<Previous>"), t.m_Character.Value.Portraits.Select(i => i.m_Sprite).ToArray());
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        public override void DrawPreview()
        {
            Say t = target as Say;
            SerializedProperty portraitProp = serializedObject.FindProperty(nameof(t.m_Portrait));
            SerializedProperty characterProp = serializedObject.FindProperty(nameof(t.m_Character));

            if (((t.m_Character.IsConstant && t.m_Character.Value != null) &&
                t.m_Character.Value.Portraits.Length > 0) &&
                (((t.m_DialogUI.IsConstant && t.m_DialogUI.Value != null) &&
                (t.m_DialogUI.Value as ISayUI).CanDisplayPortrait())))
            {
                if (t.m_Portrait.Variable == null && t.m_Portrait.Value != null)
                {
                    AnimatePortraitCommandEditor.DrawPortraitPreview(portraitProp, characterProp);
                }
            }
        }

        public static void DrawTagHelp(Rect position)
        {
            GUIContent buttonLabel = new GUIContent("Tag Help", "View available tags");

            float rightIndent = FungusGUI.VariableDataTypeFieldWidth;
            position.xMax -= rightIndent;
            
            position.height = GetTagHelpButtonHeight();
            Rect buttonPosition = position;
            buttonPosition.xMin += position.xMax - EditorStyles.miniButton.CalcSize(buttonLabel).x - 8;

            if (GUI.Button(buttonPosition, buttonLabel, EditorStyles.miniButton))
            {
                s_ShowTagHelp = !s_ShowTagHelp;
            }
            if (s_ShowTagHelp)
            {
                position.y += position.height;
                position.height = GetTagHelpLabelHeight();
                DrawTagHelpLabel(position);
            }
        }

        public static float GetTagHelpHeight()
        {
            float height = GetTagHelpButtonHeight();
            if (s_ShowTagHelp)
            {
                height += GetTagHelpLabelHeight();
            }
            return height;
        }

        private static float GetTagHelpButtonHeight()
        {
            return EditorGUIUtility.singleLineHeight;
        }

        private static float GetTagHelpLabelHeight()
        {
            GUIContent label = new GUIContent(WriterManager.GetTagHelpText());
            return EditorStyles.helpBox.CalcSize(label).y;
        }

        protected static void DrawTagHelpLabel(Rect position)
        {
            GUIContent label = new GUIContent(WriterManager.GetTagHelpText());
            EditorGUI.SelectableLabel(position, label.text, EditorStyles.helpBox);
        }

        protected bool IsTextTooLongToFit()
        {
            Say t = target as Say;

            if (t.m_DialogText.IsConstant && t.m_DialogText.Value != null)
            {
                if (t.m_DialogUI.IsConstant && t.m_DialogUI.Value != null)
                {
                    if (!Application.isPlaying)
                    {
                        SayLineUI sayLineUI = (t.m_DialogUI.Value as ISayUI).GetSayLineUI();
                        if (sayLineUI != null)
                        {
                            Text dialogText = sayLineUI.DialogText;
                            if (dialogText != null)
                            {
                                if (dialogText.transform.parent.GetComponent<LayoutGroup>() == null)
                                {
                                    if (Writer.WillMoveTruncatedTextToNextDialogBox(dialogText, t.m_DialogText.Value))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        protected void DrawTextTooLongToFitWarning(Rect position)
        {
            float rightIndent = FungusGUI.VariableDataTypeFieldWidth;
            position.xMax -= rightIndent;

            EditorGUI.HelpBox(position, m_textTooLongWarningLabel.text, MessageType.Warning);
        }

        protected float GetTextTooLongToFitWarningHeight()
        {
            return EditorStyles.helpBox.CalcSize(m_textTooLongWarningLabel).y;
        }
    }
    
}
#endif