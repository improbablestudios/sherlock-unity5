using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using Fungus.Variables;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.DialogSystem;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Display a timer bar that reflects the timelimit for a choice. If a player fails to select a choice in time, a choice will be selected for them or a target block will be executed.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Choice Timer")]
    public class ChoiceTimer : ChoiceDialogUICommand, IConnectNodes
    {
        public enum ExpireAction
        {
            SelectChoice,
            ExecuteBlock,
        }

        [Tooltip("The length of time to display the timer for")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData(10f);
        
        [Tooltip("Determines what happens when the timer expires")]
        public ExpireAction m_OnExpire;

        [Tooltip("The choice to select when the timer expires")]
        public ChoiceData m_TargetChoice = new ChoiceData();

        [Tooltip("The block to execute when the timer expires")]
        [FormerlySerializedAs("_targetBlock")]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if this block should pause, stop, or continue executing commands, or wait until the called target block finishes")]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";
        
        public override void OnEnter()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();
            float duration = m_Duration.Value;
            Choice targetChoice = m_TargetChoice.Value;
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;
            
            if (choiceUI != null &&
                targetBlock != null)
            {
                choiceUI.DisplayChoiceTimer(duration, targetChoice.DialogLineState.ActiveDialogLineIndex, ExecuteTargetBlock);
            }

            Continue();
        }

        public void ExecuteTargetBlock()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();
            Choice targetChoice = m_TargetChoice.Value;
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            if (m_OnExpire == ExpireAction.SelectChoice)
            {
                targetChoice.ExecuteTargetBlock();
            }
            else if (m_OnExpire == ExpireAction.ExecuteBlock)
            {
                if (targetBlock != null)
                {
                    targetBlock.OnNextExecutionFinished.AddPersistentListener(choiceUI.InvokeChoiceFinishedEvent);
                }

                this.CallBlock(targetBlock, callMode);
            }
        }

        public override string GetSummary()
        {
            return string.Format("{0} seconds : {1}", m_Duration, m_TargetBlock);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetChoice))
            {
                if (m_OnExpire == ExpireAction.SelectChoice)
                {
                    if ((Application.isPlaying || m_TargetChoice.IsConstant) && m_TargetChoice.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetBlock))
            {
                if (m_OnExpire == ExpireAction.ExecuteBlock)
                {
                    if ((Application.isPlaying || m_TargetBlock.IsConstant) && m_TargetBlock.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }
        
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetChoice))
            {
                if (m_OnExpire != ExpireAction.SelectChoice)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetBlock))
            {
                if (m_OnExpire != ExpireAction.ExecuteBlock)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_CallMode))
            {
                if (m_OnExpire != ExpireAction.ExecuteBlock)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public Type GetNodeType()
        {
            return typeof(Block);
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}