﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Dialog",
                 "Change the amount of time dialog boxes will wait for input before auto advancing.")]
    [AddComponentMenu("Fungus/Commands/Setting/Dialog/Set Dialog Auto Advance Setting")]
    public class SetDialogAutoAdvanceDelaySetting : SettingCommand
    {
        [Tooltip("Determines which dialog will be skipped (if skip is on)")]
        [DataMin(0)]
        public FloatData m_AutoAdvanceDelay = new FloatData(1f);

        protected override void ChangeSetting()
        {
            float autoAdvanceDelay = m_AutoAdvanceDelay.Value;

            DialogManager.Instance.AutoAdvanceDelay = autoAdvanceDelay;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_AutoAdvanceDelay);
        }
    }

}