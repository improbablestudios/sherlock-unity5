﻿using System;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    public abstract class DialogCommand : Command, ISavable
    {
        [Serializable]
        public class DialogCommandSaveState : CommandSaveState<DialogCommand>
        {
            public override void Save(DialogCommand dialogCommand)
            {
                dialogCommand.m_dialogLineIndex = dialogCommand.m_dialogLineState.ActiveDialogLineIndex;
                dialogCommand.m_wasVisited = dialogCommand.m_dialogLineState.WasVisited;

                base.Save(dialogCommand);
            }

            public override void Load(DialogCommand dialogCommand)
            {
                base.Load(dialogCommand);

                dialogCommand.m_dialogLineState = new DialogLineState(dialogCommand.m_dialogLineIndex, dialogCommand.m_wasVisited);
            }
        }

        protected int m_dialogLineIndex;

        protected bool m_wasVisited;

        protected DialogLineState m_dialogLineState;

        public DialogLineState DialogLineState
        {
            get
            {
                return m_dialogLineState;
            }
        }
        
        public virtual SaveState GetSaveState()
        {
            return new DialogCommandSaveState();
        }
    }

}