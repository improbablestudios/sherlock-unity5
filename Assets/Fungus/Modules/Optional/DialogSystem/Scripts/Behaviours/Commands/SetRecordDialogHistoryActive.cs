﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Set whether or not to record dialog history.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Set Record Dialog History Active")]
    public class SetRecordDialogHistoryActive : Command
    {
        [Tooltip("If true, dialog history will be recorded")]
        [FormerlySerializedAs("_interactable")]
        [FormerlySerializedAs("m_Interactable")]
        public BooleanData m_Active = new BooleanData(true);

        public override void OnEnter()
        {
            bool active = m_Active.Value;

            DialogManager.Instance.RecordHistory = active;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Active);
        }
    }

}