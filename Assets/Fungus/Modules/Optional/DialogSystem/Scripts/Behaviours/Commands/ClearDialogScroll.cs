﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Clear all dialog from a dialog scroll.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Clear Dialog Scroll")]
    public class ClearDialogScroll : Command
    {
        [Tooltip("The target dialog scroll")]
        [ObjectDataPopup("<Default>", true)]
        public DialogScrollData m_DialogScroll = new DialogScrollData();

        public override void OnEnter()
        {
            DialogScroll dialogScroll = m_DialogScroll.Value;

            if (dialogScroll != null)
            {
                dialogScroll.ClearDialogScroll();
            }

            Continue();
        }
    }

}