using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.DialogSystem;
using ImprobableStudios.CharacterSystem;

namespace Fungus.Commands
{
    [CommandInfo("Dialog",
                 "Write dialog text in a ui.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Say")]
    public class Say : SayDialogUICommand, IExplicitLocalizable
    {
        [Tooltip("The character that is speaking")]
        [FormerlySerializedAs("_character")]
        public CharacterData m_Character = new CharacterData();

        [Tooltip("The text in the header of the dialog box")]
        [Localize("(Character Name)")]
        [FormerlySerializedAs("_characterName")]
        [FormerlySerializedAs("m_CharacterName")]
        public StringData m_NameText = new StringData("");

        [Tooltip("The text that will be typed in the dialog box")]
        [StringDataArea(3, 10)]
        [Localize]
        [FormerlySerializedAs("_text")]
        [FormerlySerializedAs("m_Text")]
        public StringData m_DialogText = new StringData("");

        [Tooltip("The voiceover audio to play when writing the text")]
        [Localize("(Voice Over Clip)")]
        [FormerlySerializedAs("_voiceOver")]
        public AudioClipData m_VoiceOver = new AudioClipData();

        [Tooltip("The portrait that represents speaking character")]
        [FormerlySerializedAs("_portrait")]
        public SpriteData m_Portrait = new SpriteData();

        [Tooltip("Continue typing this text in the previous dialog box.")]
        [FormerlySerializedAs("_continuePrevious")]
        public BooleanData m_ContinuePrevious = new BooleanData();
        
        [Tooltip("Instantly auto advance to the next command as soon as the dialog has finished writing. (Don't wait for user input to advance)")]
        public BooleanData m_InstantlyAutoAdvance = new BooleanData();

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            ISayUI sayUI = GetSayDialogUI();
            Character character = m_Character.Value;
            AudioClip voiceOver = m_VoiceOver.Value;
            bool continuePrevious = m_ContinuePrevious.Value;
            bool instantlyAutoAdvance = m_InstantlyAutoAdvance.Value;
            Sprite portrait = m_Portrait.Value;
            
            string nameText = GetFormattedNameText();
            Dictionary<string, string> nameLocalizedDictionary = GetNameLocalizedDictionary();

            string dialogText = GetFormattedDialogText();
            Dictionary<string, string> dialogLocalizedDictionary = GetDialogLocalizedDictionary();

            Dictionary<string, AudioClip> voiceOverLocalizedDictionary = GetVoiceOverLocalizedDictionary();

            SayLineState sayState = new SayLineState(dialogLocalizedDictionary, dialogText, character, nameLocalizedDictionary, nameText, voiceOverLocalizedDictionary, voiceOver, portrait, continuePrevious, instantlyAutoAdvance);

            if (m_dialogLineState != null)
            {
                sayState.WasVisited = m_dialogLineState.WasVisited;
            }

            m_dialogLineState = sayState;
            
            sayUI.DisplaySay(sayState, ContinueAfterFinished);
        }
        
        public string GetFormattedNameText()
        {
            return FormatString(m_NameText.Value);
        }

        public Dictionary<string, string> GetNameLocalizedDictionary()
        {
            Dictionary<string, string> nameLocalizedDictionary = null;
            if (m_NameText.VariableDataType == VariableDataType.Value)
            {
                nameLocalizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_NameText.Value, base.FormatString);
            }
            else if (m_NameText.VariableDataType == VariableDataType.Variable)
            {
                nameLocalizedDictionary = m_NameText.Variable.GetLocalizedDictionary(FormatString);
            }

            return nameLocalizedDictionary;
        }

        public string GetFormattedDialogText()
        {
            return FormatString(m_DialogText.Value);
        }

        public Dictionary<string, string> GetDialogLocalizedDictionary()
        {
            Dictionary<string, string> dialogLocalizedDictionary = null;
            if (m_DialogText.VariableDataType == VariableDataType.Value)
            {
                dialogLocalizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_DialogText.Value, base.FormatString);
            }
            else if (m_DialogText.VariableDataType == VariableDataType.Variable)
            {
                dialogLocalizedDictionary = m_DialogText.Variable.GetLocalizedDictionary(FormatString);
            }

            return dialogLocalizedDictionary;
        }

        public Dictionary<string, AudioClip> GetVoiceOverLocalizedDictionary()
        {
            Dictionary<string, AudioClip> voiceOverLocalizedDictionary = null;
            if (m_VoiceOver.VariableDataType == VariableDataType.Value)
            {
                voiceOverLocalizedDictionary = this.SafeGetLocalizedDictionary(x => x.m_VoiceOver.Value);
            }
            else if (m_VoiceOver.VariableDataType == VariableDataType.Variable)
            {
                voiceOverLocalizedDictionary = m_VoiceOver.Variable.GetLocalizedDictionary();
            }

            return voiceOverLocalizedDictionary;
        }

        public override string GetSummary()
        {
            string namePrefix = "";
            if (!m_Character.IsConstant)
            {
                namePrefix = m_Character.ToString();
            }
            else if (m_Character.Value != null)
            {
                namePrefix = m_Character.Value.DisplayName;
            }
            if (!m_ContinuePrevious.IsConstant)
            {
                namePrefix = namePrefix + " (CONT'D IF " + m_ContinuePrevious.ToString() + ")";
            }
            else if (m_ContinuePrevious.Value)
            {
                namePrefix = namePrefix + " (CONT'D)";
            }
            if (!string.IsNullOrEmpty(namePrefix))
            {
                namePrefix += ": ";
            }
            return string.Format("{0}\"{1}\"", namePrefix, m_DialogText);
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }

        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_NameText))
            {
                if (((m_DialogUI.IsConstant && m_DialogUI.Value != null) && !(m_DialogUI.Value as ISayUI).CanDisplayName()) ||
                    (m_Character.IsConstant && m_Character.Value != null))

                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_Portrait))
            {
                if ((m_DialogUI.IsConstant && m_DialogUI.Value != null) && !(m_DialogUI.Value as ISayUI).CanDisplayPortrait())
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_VoiceOver))
            {
                if ((m_DialogUI.IsConstant && m_DialogUI.Value != null) && !(m_DialogUI.Value as ISayUI).CanPlayVoiceOver())
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        //
        // ILocalizable implementation
        //
        
        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }

        public virtual void ExplicitlyGetSourceValues(Dictionary<string, object> dictionary)
        {
        }

        public virtual void ExplicitlySetLocalizedValues(Dictionary<string, object> dictionary)
        {
        }

        public virtual void ExplicitlyGetDescriptions(Dictionary<string, string> dictionary)
        {
            string description = "";
            string characterString = null;
            if (!m_Character.IsConstant)
            {
                characterString = m_Character.ToString();
            }
            else if (m_Character.IsConstant && m_Character.Value != null)
            {
                characterString = m_Character.Value.DisplayName;
            }
            else if (m_Character.IsConstant && m_Character.Value == null)
            {
                characterString = m_NameText.Value;
            }
            if (!m_ContinuePrevious.IsConstant)
            {
                characterString += " (CONT'D IF " + m_ContinuePrevious.ToString() + ")";
            }
            else if (m_ContinuePrevious.Value)
            {
                characterString += " (CONT'D)";
            }
            if (!string.IsNullOrEmpty(characterString))
            {
                description += characterString;
            }

            dictionary["_text.Value"] = description;
        }

        public virtual void ExplicitlyGetConverters(Dictionary<string, Type> dictionary)
        {
        }
    }

}
