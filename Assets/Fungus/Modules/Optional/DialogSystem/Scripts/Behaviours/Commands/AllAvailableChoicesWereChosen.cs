﻿using UnityEngine;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Check if all currently available choices were chosen.")]
    [AddComponentMenu("Fungus/Commands/Dialog/All Available Choices Were Chosen")]
    public class AllAvailableChoicesWereChosen : ReturnValueCommand<bool>
    {
        [Tooltip("The target choice dialog ui")]
        [ComponentDataField("<Default>", true, typeof(IChoiceUI))]
        public DialogUIData m_DialogUI = new DialogUIData();

        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;

        public IChoiceUI GetChoiceDialogUI()
        {
            IChoiceUI choiceUI = m_DialogUI.Value as IChoiceUI;

            if (choiceUI == null)
            {
                choiceUI = DialogMenu.DefaultDialogMenu;
            }

            return choiceUI;
        }
        
        public override bool GetReturnValue()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();

            foreach (ChoiceLineState choiceLineState in choiceUI.GetActiveChoiceLineStates())
            {
                if (!choiceLineState.WasVisited)
                {
                    return false;
                }
            }

            return true;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            string dialogUIString = m_DialogUI.ToString();

            if (m_DialogUI.IsConstant && m_DialogUI.Value != null)
            {
                dialogUIString = m_DialogUI.Value.ToString();
            }

            return string.Format("{0} = all choices were chosen in \"{1}\"", m_ReturnVariable, dialogUIString);
        }
    }

}