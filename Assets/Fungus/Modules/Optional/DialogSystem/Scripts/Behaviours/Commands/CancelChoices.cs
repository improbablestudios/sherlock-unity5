﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Clear all active choices and hide the dialog ui")]
    [AddComponentMenu("Fungus/Commands/Dialog/Cancel Choices")]
    public class CancelChoices : ChoiceDialogUICommand
    {
        public override void OnEnter()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();

            choiceUI.CancelChoices(ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}