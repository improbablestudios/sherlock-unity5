﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Clear dialog history.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Clear Dialog History")]
    public class ClearDialogHistory: Command
    {
        public DialogHistoryData m_DialogHistory = new DialogHistoryData();

        public override void OnEnter()
        {
            DialogHistory dialogHistory = m_DialogHistory.Value;

            if (dialogHistory != null)
            {
                dialogHistory.ClearHistory();
            }

            Continue();
        }
    }

}