﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{
    
    public abstract class ChoiceDialogUICommand : DialogCommand
    {
        [Tooltip("The target choice dialog ui")]
        [ComponentDataField("<Default>", true, typeof(IChoiceUI))]
        public DialogUIData m_DialogUI = new DialogUIData();

        public IChoiceUI GetChoiceDialogUI()
        {
            IChoiceUI choiceUI = m_DialogUI.Value as IChoiceUI;

            if (choiceUI == null)
            {
                choiceUI = DialogMenu.DefaultDialogMenu;
            }

            return choiceUI;
        }
    }

}