﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Dialog",
                 "Change the setting that determines which dialog will be skipped (if skip is on).")]
    [AddComponentMenu("Fungus/Commands/Setting/Dialog/Set Dialog Skip Mode Setting")]
    public class SetDialogSkipModeSetting : SettingCommand
    {
        [Tooltip("If true, the system will only skip dialog that has already been seen, otherwise, the system will skip all dialog.")]
        public BooleanData m_OnlySkipSeenDialog = new BooleanData();

        protected override void ChangeSetting()
        {
            bool active = m_OnlySkipSeenDialog.Value;

            DialogManager.Instance.OnlySkipSeenDialog = active;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_OnlySkipSeenDialog);
        }
    }

}