﻿using UnityEngine;
using Fungus.Variables;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Flow/Wait",
                 "Wait until the user has selected a choice and the choice's target block has finished executing before executing the next command in the block.")]
    [AddComponentMenu("Fungus/Commands/Flow/Wait/Wait Until Choice Block Finished")]
    public class WaitUntilChoiceFinished : ChoiceDialogUICommand
    {
        public override void OnEnter()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();

            choiceUI.GetOnNextChoiceFinishedEvent().AddPersistentListener(Continue);
        }
        
        public override bool IsWaitUntilFinishedPropertyVisible()
        {
            return false;
        }

        public override bool ShouldWait(bool runtime)
        {
            return true;
        }
    }

}