﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    public abstract class DialogBoxCommand : DialogCommand
    {
        [Tooltip("The target dialog box")]
        [ObjectDataPopup("<Default>", true)]
        [FormerlySerializedAs("_dialogBox")]
        public DialogBoxData m_DialogBox = new DialogBoxData();

        public DialogBox GetDialogBox()
        {
            DialogBox dialogBox = m_DialogBox.Value;

            if (dialogBox == null)
            {
                dialogBox = DialogBox.DefaultDialogBox;
            }

            return dialogBox;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_DialogBox);
        }
    }

}