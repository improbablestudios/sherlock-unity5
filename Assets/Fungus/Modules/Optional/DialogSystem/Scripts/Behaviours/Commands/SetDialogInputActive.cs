﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Set whether or not any currently active dialog boxes will respond to user input.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Set Dialog Input Active")]
    public class SetDialogInputActive : Command
    {
        [Tooltip("If false, dialog boxes will ignore user input")]
        [FormerlySerializedAs("_interactable")]
        [FormerlySerializedAs("m_Interactable")]
        public BooleanData m_Active = new BooleanData(true);

        public override void OnEnter()
        {
            bool active = m_Active.Value;

            DialogManager.Instance.IgnoreInput = !active;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Active);
        }
    }

}