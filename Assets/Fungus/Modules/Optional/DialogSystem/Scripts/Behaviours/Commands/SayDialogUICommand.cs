﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    public abstract class SayDialogUICommand : DialogCommand
    {
        [Tooltip("The target say dialog ui")]
        [ComponentDataField("<Default>", true, typeof(ISayUI))]
        public DialogUIData m_DialogUI = new DialogUIData();
        
        public ISayUI GetSayDialogUI()
        {
            ISayUI sayUI = m_DialogUI.Value as ISayUI;

            if (sayUI == null)
            {
                sayUI = DialogBox.DefaultDialogBox;
            }

            return sayUI;
        }
    }

}