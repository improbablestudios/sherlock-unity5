﻿using UnityEngine;
using Fungus.Variables;
using ImprobableStudios.DialogSystem;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Set where whether or not the dialog box will hide itself if it goes unused for too long.")]
    [AddComponentMenu("Fungus/Commands/Dialog/Set Dialog Box Hide When Unused")]
    public class SetDialogBoxHideWhenUnused : DialogBoxCommand
    {
        [Tooltip("If true, the dialog box will hide itself if it goes unsued for too long, otherwise, it will always remain on screen")]
        public BooleanData m_HideWhenUnused = new BooleanData(true);
        
        public override void OnEnter()
        {
            DialogBox dialogBox = GetDialogBox();
            bool hideWhenUnused = m_HideWhenUnused.Value;

            dialogBox.HideWhenUnused = hideWhenUnused;

            Continue();
        }
    }

}
