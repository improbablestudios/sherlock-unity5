using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.Generic;
using Fungus.Variables;
using ImprobableStudios.Localization;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.GraphWindowUtilities;
using ImprobableStudios.UnityEventUtilities;
using ImprobableStudios.DialogSystem;
using System;

namespace Fungus.Commands
{

    [CommandInfo("Dialog",
                 "Show a choice in a dialog menu")]
    [AddComponentMenu("Fungus/Commands/Dialog/Choice")]
    public class Choice : ChoiceDialogUICommand, IConnectNodes, ILocalizable
    {
        [Tooltip("The text to display on the menu button")]
        [Localize]
        [FormerlySerializedAs("_text")]
        public StringData m_Text = new StringData("");

        [Tooltip("The icon to display on the menu button")]
        [FormerlySerializedAs("_icon")]
        public SpriteData m_Icon = new SpriteData();

        [Tooltip("The block to execute when this option is selected")]
        [FormerlySerializedAs("_targetBlock")]
        [CheckNotNullData]
        public BlockData m_TargetBlock = new BlockData();

        [Tooltip("Select if this block should pause, stop, or continue executing commands, or wait until the called target block finishes")]
        [FormerlySerializedAs("_targetBlock")]
        [CheckNotNullData]
        public CallModeData m_CallMode = new CallModeData(CallMode.WaitUntilFinished);

        [Tooltip("Editor only description text to display next to the connection arrow that represents this command")]
        [Separator]
        public string m_Description = "";

        protected int m_choiceIndex;

        public int ChoiceIndex
        {
            get
            {
                return m_choiceIndex;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            LocalizationManager.Instance.RegisterLocalizable(this);
        }

        public override void OnEnter()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();
            Sprite icon = m_Icon.Value;
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            string dialogText = GetFormattedDialogText();
            Dictionary<string, string> dialogLocalizedDictionary = GetDialogLocalizedDictionary();

            ChoiceLineState choiceState = new ChoiceLineState(dialogLocalizedDictionary, dialogText, icon, ExecuteTargetBlock);

            if (m_dialogLineState != null)
            {
                choiceState.WasVisited = m_dialogLineState.WasVisited;
            }

            m_dialogLineState = choiceState;

            choiceUI.DisplayChoice(choiceState);

            Continue();
        }

        public void ExecuteTargetBlock()
        {
            IChoiceUI choiceUI = GetChoiceDialogUI();
            Block targetBlock = m_TargetBlock.Value;
            CallMode callMode = m_CallMode.Value;

            if (targetBlock != null)
            {
                targetBlock.OnNextExecutionFinished.AddPersistentListener(choiceUI.InvokeChoiceFinishedEvent);
            }

            this.CallBlock(targetBlock, callMode);
        }

        public string GetFormattedDialogText()
        {
            return FormatString(m_Text.Value);
        }

        public Dictionary<string, string> GetDialogLocalizedDictionary()
        {
            Dictionary<string, string> choiceLocalizedDictionary = null;
            if (m_Text.VariableDataType == VariableDataType.Value)
            {
                choiceLocalizedDictionary = LocalizableExtensions.SafeGetLocalizedDictionary(this, x => x.m_Text.Value, base.FormatString);
            }
            else if (m_Text.VariableDataType == VariableDataType.Variable)
            {
                choiceLocalizedDictionary = m_Text.Variable.GetLocalizedDictionary(FormatString);
            }

            return choiceLocalizedDictionary;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\" : {1}", m_Text, m_TargetBlock);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Icon))
            {
                if ((m_DialogUI.IsConstant && (m_DialogUI.Value as IChoiceUI) != null) && !(m_DialogUI.Value as IChoiceUI).CanDisplayIcon())
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        //
        // IConnectNodes implementation
        //

        public Connection[] GetSwitchConnections()
        {
            List<Connection> connections = new List<Connection>();

            if (m_TargetBlock.IsConstant && m_TargetBlock.Value != null)
            {
                connections.Add(new Connection(ParentBlock, m_TargetBlock.Value, m_Description));
            }

            return connections.ToArray();
        }

        public Connection[] GetActivateConnections()
        {
            return new Connection[0];
        }

        public Connection[] GetDeactivateConnections()
        {
            return new Connection[0];
        }

        //
        // ILocalizable implementation
        //

        public virtual string GetLocalizableOrderedPriority()
        {
            return GetOrderedId();
        }

        public virtual void OnLocalizeComplete()
        {
        }
        
        public Type GetNodeType()
        {
            return typeof(Block);
        }

        //
        // IConnectNodes implementation
        //

        public UnityEngine.Object GetUnityObject()
        {
            return this;
        }

        public string[] GetSwitchNodePropertyPaths()
        {
            if (enabled)
            {
                if (!m_TargetBlock.IsConstant || m_TargetBlock.Value != null)
                {
                    return new string[] { nameof(m_TargetBlock) + "." + "m_" + nameof(m_TargetBlock.DataVal) };
                }
            }
            return null;
        }

        public string[] GetActivateNodePropertyPaths()
        {
            return null;
        }

        public string[] GetDeactivateNodePropertyPaths()
        {
            return null;
        }

        public string GetDescription()
        {
            return m_Description;
        }
    }

}
