﻿using System;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Dialog Box Variable")]
    public class DialogBoxVariable : Variable<DialogBox>
    {
    }

    [Serializable]
    public class DialogBoxData : VariableData<DialogBox>
    {
        public DialogBoxData()
        {
        }

        public DialogBoxData(DialogBox v)
        {
            m_DataVal = v;
        }
    }

}