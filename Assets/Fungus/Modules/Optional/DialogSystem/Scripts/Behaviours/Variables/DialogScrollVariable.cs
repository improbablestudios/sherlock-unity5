﻿using System;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Dialog Scroll Variable")]
    public class DialogScrollVariable : Variable<DialogScroll>
    {
    }

    [Serializable]
    public class DialogScrollData : VariableData<DialogScroll>
    {
        public DialogScrollData()
        {
        }

        public DialogScrollData(DialogScroll v)
        {
            m_DataVal = v;
        }
    }

}