﻿using System;
using System.Collections.Generic;
using Fungus.Commands;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Choice List Variable")]
    public class ChoiceListVariable : Variable<List<Choice>>
    {
    }

    [Serializable]
    public class ChoiceListData : VariableData<List<Choice>>
    {
        public ChoiceListData()
        {
            m_DataVal = new List<Choice>();
        }

        public ChoiceListData(List<Choice> v)
        {
            m_DataVal = v;
        }
    }

}
