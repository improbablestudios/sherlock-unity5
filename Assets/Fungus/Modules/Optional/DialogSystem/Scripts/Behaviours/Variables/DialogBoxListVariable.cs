﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Dialog Box List Variable")]
    public class DialogBoxListVariable : Variable<List<DialogBox>>
    {
    }

    [Serializable]
    public class DialogBoxListData : VariableData<List<DialogBox>>
    {
        public DialogBoxListData()
        {
            m_DataVal = new List<DialogBox>();
        }

        public DialogBoxListData(List<DialogBox> v)
        {
            m_DataVal = v;
        }
    }

}