﻿using System;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Dialog Menu Variable")]
    public class DialogMenuVariable : Variable<DialogMenu>
    {
    }

    [Serializable]
    public class DialogMenuData : VariableData<DialogMenu>
    {
        public DialogMenuData()
        {
        }

        public DialogMenuData(DialogMenu v)
        {
            m_DataVal = v;
        }
    }
    
}