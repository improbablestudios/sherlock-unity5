﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Dialog Scroll List Variable")]
    public class DialogScrollListVariable : Variable<List<DialogScroll>>
    {
    }

    [Serializable]
    public class DialogScrollListData : VariableData<List<DialogScroll>>
    {
        public DialogScrollListData()
        {
            m_DataVal = new List<DialogScroll>();
        }

        public DialogScrollListData(List<DialogScroll> v)
        {
            m_DataVal = v;
        }
    }

}