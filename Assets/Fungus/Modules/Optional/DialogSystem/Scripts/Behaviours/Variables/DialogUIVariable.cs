﻿using System;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Dialog UI Variable")]
    public class DialogUIVariable : Variable<DialogUI>
    {
    }

    [Serializable]
    public class DialogUIData : VariableData<DialogUI>
    {
        public DialogUIData()
        {
        }

        public DialogUIData(DialogUI v)
        {
            m_DataVal = v;
        }
    }

}