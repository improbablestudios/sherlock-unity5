﻿using System;
using Fungus.Commands;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Choice Variable")]
    public class ChoiceVariable : Variable<Choice>
    {
    }

    [Serializable]
    public class ChoiceData : VariableData<Choice>
    {
        public ChoiceData()
        {
        }

        public ChoiceData(Choice v)
        {
            m_DataVal = v;
        }
    }

}
