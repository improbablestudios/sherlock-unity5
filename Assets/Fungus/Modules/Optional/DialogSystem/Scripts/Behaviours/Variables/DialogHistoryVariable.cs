﻿using System;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Dialog History Variable")]
    public class DialogHistoryVariable : Variable<DialogHistory>
    {
    }

    [Serializable]
    public class DialogHistoryData : VariableData<DialogHistory>
    {
        public DialogHistoryData()
        {
        }

        public DialogHistoryData(DialogHistory v)
        {
            m_DataVal = v;
        }
    }

}