﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Dialog UI List Variable")]
    public class DialogUIListVariable : Variable<List<DialogUI>>
    {
    }

    [Serializable]
    public class DialogUIListData : VariableData<List<DialogUI>>
    {
        public DialogUIListData()
        {
            m_DataVal = new List<DialogUI>();
        }

        public DialogUIListData(List<DialogUI> v)
        {
            m_DataVal = v;
        }
    }

}