﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Dialog History List Variable")]
    public class DialogHistoryListVariable : Variable<List<DialogHistory>>
    {
    }

    [Serializable]
    public class DialogHistoryListData : VariableData<List<DialogHistory>>
    {
        public DialogHistoryListData()
        {
            m_DataVal = new List<DialogHistory>();
        }

        public DialogHistoryListData(List<DialogHistory> v)
        {
            m_DataVal = v;
        }
    }

}