﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ImprobableStudios.DialogSystem;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Dialog Menu List Variable")]
    public class DialogMenuListVariable : Variable<List<DialogMenu>>
    {
    }

    [Serializable]
    public class DialogMenuListData : VariableData<List<DialogMenu>>
    {
        public DialogMenuListData()
        {
            m_DataVal = new List<DialogMenu>();
        }

        public DialogMenuListData(List<DialogMenu> v)
        {
            m_DataVal = v;
        }
    }

}