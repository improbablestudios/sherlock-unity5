﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation",
                 "Play an animator state")]
    [AddComponentMenu("Fungus/Commands/Animation/Play Animator State")]
    public class PlayAnimatorState : AnimatorCommand 
    {
        [Tooltip("The name of the state you want to play")]
        [FormerlySerializedAs("_stateName")]
        [CheckNotEmptyData]
        public StringData m_StateName = new StringData();

        [Tooltip("The layer to play animation on")]
        [DataMin(0)]
        [FormerlySerializedAs("_layer")]
        public IntegerData m_Layer = new IntegerData(0);

        [Tooltip("The start time of animation")]
        [FormerlySerializedAs("_time")]
        [DataRange(0,1)]
        public FloatData m_Time = new FloatData(0f);

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string stateName = m_StateName.Value;
            int layer = m_Layer.Value;
            float time = m_Time.Value;
            
            animator.Play(stateName, layer, time);

            Continue();
        }


        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" play \"{0}\"", m_StateName);
        }
    }

}


