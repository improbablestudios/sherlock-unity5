using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation/Parameter",
                 "Set an integer parameter in an animator")]
    [AddComponentMenu("Fungus/Commands/Animation/Parameter/Set Animator Integer")]
    public class SetAnimatorInteger : SetAnimatorParameterCommand
    {
        [Tooltip("The integer value to set the parameter to")]
        [FormerlySerializedAs("_value")]
        public IntegerData m_Value = new IntegerData();

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string parameterName = m_ParameterName.Value;
            int value = m_Value.Value;
            
            animator.SetInteger(parameterName, value);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_Value);
        }
    }

}