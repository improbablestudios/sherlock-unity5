﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation",
                 "Set the speed of an animator")]
    [AddComponentMenu("Fungus/Commands/Animation/Set Animator Speed")]
    public class SetAnimatorSpeed : AnimatorCommand
    {
        [Tooltip("The new speed")]
        [FormerlySerializedAs("_speed")]
        public FloatData m_Speed = new FloatData();

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            float speed = m_Speed.Value;
            
            animator.speed = speed;

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Speed);
        }
    }

}


