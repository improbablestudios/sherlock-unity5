using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation/Parameter",
                 "Set a boolean parameter in an animator")]
    [AddComponentMenu("Fungus/Commands/Animation/Parameter/Set Animator Boolean")]
    public class SetAnimatorBoolean : SetAnimatorParameterCommand
    {
        [Tooltip("The boolean value to set the parameter to")]
        [FormerlySerializedAs("_value")]
        public BooleanData m_Value = new BooleanData();

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string parameterName = m_ParameterName.Value;
            bool value = m_Value.Value;
            
            animator.SetBool(parameterName, value);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_Value);
        }
    }

}