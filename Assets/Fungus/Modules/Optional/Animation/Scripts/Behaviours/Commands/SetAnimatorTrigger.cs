using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation/Parameter",
                 "Set a trigger parameter in an animator")]
    [AddComponentMenu("Fungus/Commands/Animation/Parameter/Set Animator Trigger")]
    public class SetAnimatorTrigger : SetAnimatorParameterCommand
    {
        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string parameterName = m_ParameterName.Value;
            
            animator.SetTrigger(parameterName);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" set", m_ParameterName);
        }
    }

}