﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class SetAnimatorParameterCommand : AnimatorCommand
    {
        [Tooltip("The name of the trigger animator parameter that will have its value changed")]
        [FormerlySerializedAs("_parameterName")]
        [CheckNotEmptyData]
        public StringData m_ParameterName = new StringData();
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" (\"{0}\") =", m_ParameterName);
        }
    }

}