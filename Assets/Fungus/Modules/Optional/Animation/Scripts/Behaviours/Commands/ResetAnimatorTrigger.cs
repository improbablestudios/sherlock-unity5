using UnityEngine;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation/Parameter",
                 "Reset a trigger parameter in an animator.")]
    [AddComponentMenu("Fungus/Commands/Animation/Parameter/Reset Animator Trigger")]
    public class ResetAnimatorTrigger : SetAnimatorParameterCommand
    {
        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string parameterName = m_ParameterName.Value;

            animator.ResetTrigger(parameterName);

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" reset");
        }
    }

}