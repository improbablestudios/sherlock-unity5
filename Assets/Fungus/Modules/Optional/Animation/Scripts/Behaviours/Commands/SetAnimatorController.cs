﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation",
                 "Set an animator's animator controller")]
    [AddComponentMenu("Fungus/Commands/Animation/Animator Controller")]
    public class SetAnimatorController : AnimatorCommand
    {
        [Tooltip("The name of the state you want to play")]
        [FormerlySerializedAs("runtimeAnimatorController")]
        [CheckNotNullData]
        public RuntimeAnimatorControllerData m_RuntimeAnimatorController = new RuntimeAnimatorControllerData();

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            RuntimeAnimatorController runtimeAnimatorController = m_RuntimeAnimatorController.Value;
            
            animator.runtimeAnimatorController = runtimeAnimatorController;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to \"{0}\"", m_RuntimeAnimatorController);
        }
    }

}


