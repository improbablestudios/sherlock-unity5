using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Animation/Parameter",
                 "Set a float parameter in an animator")]
    [AddComponentMenu("Fungus/Commands/Animation/Parameter/Set Animator Float")]
    public class SetAnimatorFloat : SetAnimatorParameterCommand
    {
        [Tooltip("The float value to set the parameter to")]
        [FormerlySerializedAs("_value")]
        public FloatData m_Value = new FloatData();

        public override void OnEnter()
        {
            Animator animator = m_Animator.Value;
            string parameterName = m_ParameterName.Value;
            float value = m_Value.Value;
            
            animator.SetFloat(parameterName, value);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" {0}", m_Value);
        }
    }

}