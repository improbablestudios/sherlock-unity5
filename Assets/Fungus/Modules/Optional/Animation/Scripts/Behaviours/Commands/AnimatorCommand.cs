﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class AnimatorCommand : Command
    {
        [Tooltip("The target animator")]
        [FormerlySerializedAs("_animator")]
        [CheckNotNullData]
        public AnimatorData m_Animator = new AnimatorData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Animator);
        }
    }

}
