#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using Fungus.Commands;
using ImprobableStudios.UnityEditorUtilities;
using ImprobableStudios.SearchPopupUtilities;

namespace Fungus.CommandEditors
{

    [CanEditMultipleObjects, CustomEditor(typeof(SetAnimatorParameterCommand), true)]
    public class SetAnimatorParameterCommandEditor : CommandEditor
    {
        protected override void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            SetAnimatorParameterCommand t = target as SetAnimatorParameterCommand;

            if (property.name == nameof(t.m_ParameterName))
            {
                if (!t.m_Animator.IsConstant || t.m_Animator.Value == null || t.m_Animator.Value.runtimeAnimatorController == null)
                {
                    base.DrawProperty(position, property, label);
                }
                else
                {
                    AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Trigger;
                    if (t.GetType() == typeof(SetAnimatorBoolean))
                    {
                        parameterType = AnimatorControllerParameterType.Bool;
                    }
                    else if (t.GetType() == typeof(SetAnimatorInteger))
                    {
                        parameterType = AnimatorControllerParameterType.Int;
                    }
                    else if (t.GetType() == typeof(SetAnimatorFloat))
                    {
                        parameterType = AnimatorControllerParameterType.Float;
                    }
                    else if (t.GetType() == typeof(SetAnimatorTrigger) || t.GetType() == typeof(ResetAnimatorTrigger))
                    {
                        parameterType = AnimatorControllerParameterType.Trigger;
                    }

                    VariableEditor.VariableDataField<string>(
                        position, 
                        property, 
                        label, 
                        (Rect r, SerializedProperty p, GUIContent l) => 
                        {
                            ParameterNamePopup(r, p, l, parameterType);
                        });
                }
            }
            else
            {
                base.DrawProperty(position, property, label);
            }
        }

        protected void ParameterNamePopup(Rect position, SerializedProperty property, GUIContent label, AnimatorControllerParameterType parameterType)
        {
            SetAnimatorParameterCommand t = target as SetAnimatorParameterCommand;

            if (!t.m_Animator.IsConstant || t.m_Animator == null || t.m_Animator.Value.runtimeAnimatorController == null)
            {
                EditorGUILayout.PropertyField(property, label, true);
            }
            else
            {
                string parameterName = property.stringValue;
                AnimatorController ac = t.m_Animator.Value.runtimeAnimatorController as AnimatorController;
                string[] parameterNames = ac.parameters.Where(i => i.type == parameterType).Select(i => i.name).ToArray();
                SearchPopupGUI.TextSearchPopupField(position, property, label, null, parameterNames, false);
            }
        }
    }

}
#endif