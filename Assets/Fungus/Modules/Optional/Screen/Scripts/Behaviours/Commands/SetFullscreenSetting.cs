using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the application to fullscreen or windowed.")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Fullscreen Setting")]
    public class SetFullscreenSetting : SettingCommand 
    {
        public BooleanData m_Fullscreen = new BooleanData(true);

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.Fullscreen = m_Fullscreen.Value;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Fullscreen);
        }
    }

}