﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the sleep timeout of the screen.")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Screen Sleep Timeout Setting")]
    public class SetScreenSleepTimeoutSetting : SettingCommand
    {
        [Tooltip("The amount of time (in seconds) after the last active user interaction that the screen will dim (Set to -1 for NeverSleep or -2 for SystemSetting)")]
        [DataMin(-2)]
        public IntegerData m_SleepTimeout = new IntegerData();

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.SleepTimeout = m_SleepTimeout.Value;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_SleepTimeout);
        }
    }

}