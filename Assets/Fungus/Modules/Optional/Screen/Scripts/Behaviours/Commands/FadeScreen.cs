﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Screen",
                 "Fade the screen using a color. Setting the fade color alpha to 1 will obscure the screen, setting the fade color alpha to 0 will reveal the screen.")]
    [AddComponentMenu("Fungus/Commands/Screen/Fade Screen")]
    public class FadeScreen : Command 
    {
        [Tooltip("Color to render fullscreen fade texture with when screen is obscured.")]
        [FormerlySerializedAs("_fadeColor")]
        public ColorData m_FadeColor = new ColorData(Color.black);

        [Tooltip("The order of the fade layer.")]
        [FormerlySerializedAs("_sortingOrder")]
        public IntegerData m_SortingOrder = new IntegerData(100);

        [Tooltip("Time for fade effect to complete")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData(1f);

        public override void OnEnter()
        {
            Color fadeColor = m_FadeColor.Value;
            int sortingOrder = m_SortingOrder.Value;
            float duration = m_Duration.Value;
            
            CameraManager.Instance.Fade(fadeColor, duration, Ease.Linear, sortingOrder, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
        
        public override string GetSummary()
        {
            return string.Format("to {0}", m_FadeColor) + GetDurationSummary(m_Duration);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }
    }

}