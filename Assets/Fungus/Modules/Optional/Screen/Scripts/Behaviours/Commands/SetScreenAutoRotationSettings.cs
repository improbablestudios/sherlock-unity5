﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the autorotation settings for the screen.")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Screen Auto Rotation Setting")]
    public class SetScreenAutoRotationSettings : SettingCommand
    {
        public BooleanData m_AutorotateToLandscapeLeft = new BooleanData();
        public BooleanData m_AutorotateToLandscapeRight = new BooleanData();
        public BooleanData m_AutorotateToPortrait = new BooleanData();
        public BooleanData m_AutorotateToPortraitUpsideDown = new BooleanData();

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.AutorotateToLandscapeLeft = m_AutorotateToLandscapeLeft.Value;
            ScreenManager.Instance.AutorotateToLandscapeRight = m_AutorotateToLandscapeRight.Value;
            ScreenManager.Instance.AutorotateToPortrait = m_AutorotateToPortrait.Value;
            ScreenManager.Instance.AutorotateToPortraitUpsideDown = m_AutorotateToPortraitUpsideDown.Value;
        }

        public override string GetSummary()
        {
            return string.Format("Left: {0}, Right: {1}, Up: {2}, Down: {3}", m_AutorotateToLandscapeLeft, m_AutorotateToLandscapeRight, m_AutorotateToPortrait, m_AutorotateToPortraitUpsideDown);
        }
    }

}