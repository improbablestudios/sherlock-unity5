﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the current resolution of the screen.")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Screen Resolution Setting")]
    public class SetScreenResolutionSetting : SettingCommand
    {
        public ResolutionData m_Resolution = new ResolutionData();

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.Resolution = m_Resolution.Value;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Resolution);
        }
    }

}