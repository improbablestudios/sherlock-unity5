﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the fullscreen mode")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Fullscreen Mode Setting")]
    public class SetFullscreenModeSetting : SettingCommand
    {
        public FullScreenModeData m_FullscreenMode = new FullScreenModeData();

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.FullscreenMode = m_FullscreenMode.Value;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_FullscreenMode);
        }
    }

}