﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ScreenUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Setting/Screen",
                 "Set the current orientation of the screen.")]
    [AddComponentMenu("Fungus/Commands/Setting/Screen/Set Screen Orientation Setting")]
    public class SetScreenOrientationSetting : SettingCommand
    {
        public ScreenOrientationData m_Orientation = new ScreenOrientationData();

        protected override void ChangeSetting()
        {
            ScreenManager.Instance.Orientation = m_Orientation.Value;
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Orientation);
        }
    }

}