﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using ImprobableStudios.BaseUtilities;
using ImprobableStudios.SaveSystem;
using ImprobableStudios.ScreenUtilities;

namespace Fungus
{

    [Serializable]
    [AddComponentMenu("Fungus/Behaviours/UI/Setting/Full Screen Selector")]
    public class FullscreenSelector : SettingSelector<Toggle>
    {
        [Tooltip("Toggle that can be used to toggle between fullscreen and windowed.")]
        [SerializeField]
        [FormerlySerializedAs("toggle")]
        [CheckNotNull]
        protected Toggle m_Toggle;

        public Toggle Toggle
        {
            get
            {
                return m_Toggle;
            }
            set
            {
                m_Toggle = value;
            }
        }

        public override Toggle GetControl()
        {
            return m_Toggle;
        }

        public override void SetControl(Toggle control)
        {
            m_Toggle = control;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Toggle != null)
            {
                m_Toggle.isOn = ScreenManager.Instance.Fullscreen;
                m_Toggle.onValueChanged.AddListener(OnValueChanged);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Toggle != null)
            {
                m_Toggle.onValueChanged.RemoveListener(OnValueChanged);
            }
        }

        protected virtual void OnValueChanged(bool value)
        {
            ScreenManager.Instance.Fullscreen = value;
            OnSettingChanged();
        }
    }

}