﻿using ImprobableStudios.SaveSystem;
using System;
using System.Linq.Expressions;
using ImprobableStudios.ScreenUtilities;

namespace Fungus
{

    [Serializable]
    public class ScreenManagerSaveState : ScriptableObjectSaveState<ScreenManager>
    {
        public override Expression<Func<ScreenManager, object>>[] ExplicitlySavedProperties()
        {
            return new Expression<Func<ScreenManager, object>>[]
            {
                x => x.UseNativeResolution,
                x => x.Resolution,
                x => x.Fullscreen,
                x => x.FullscreenMode,
                x => x.SleepTimeout,
                x => x.Orientation,
                x => x.AutorotateToLandscapeLeft,
                x => x.AutorotateToLandscapeRight,
                x => x.AutorotateToPortrait,
                x => x.AutorotateToPortraitUpsideDown,
            };
        }

        public override int LoadPriority()
        {
            return -2;
        }
    }

    public class SavableScreenManager : ISavableSetting
    {
        [Serializable]
        public class SavableScreenManagerSaveState : SaveState<SavableScreenManager>
        {
            public override void Save(SavableScreenManager savable)
            {
                savable.m_saveState.Save(ScreenManager.Instance);

                base.Save(savable);
            }

            public override void Load(SavableScreenManager savable)
            {
                savable.m_saveState.Load(ScreenManager.Instance);

                base.Load(savable);
            }
        }

        protected ScreenManagerSaveState m_saveState = new ScreenManagerSaveState();

        public SaveState GetSaveState()
        {
            return new SavableScreenManagerSaveState();
        }

        public string GetKey()
        {
            return ScreenManager.Instance.GetKey();
        }
    }

}
