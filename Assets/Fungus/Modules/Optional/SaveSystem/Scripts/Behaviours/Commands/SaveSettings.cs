﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Setting",
                "Save current settings to the settings file")]
    [AddComponentMenu("Fungus/Commands/Setting/Save Settings")]
    public class SaveSettings : Command
    {
        public override void OnEnter()
        {
            SaveSystemManager.Instance.SaveSettings();
            Continue();
        }
    }

}