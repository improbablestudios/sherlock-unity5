﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Save",
                "Save current scene to a file.")]
    [AddComponentMenu("Fungus/Commands/Save/Save To File")]
    public class SaveToFile : Command
    {
        [Tooltip("Save file number")]
        [DataMin(0)]
        [FormerlySerializedAs("_saveFileNumber")]
        public IntegerData m_SaveFileNumber = new IntegerData();

        public override void OnEnter()
        {
            SaveSystemManager.Instance.Save("save " + m_SaveFileNumber.Value.ToString());
            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_SaveFileNumber);
        }
        
        public override bool WillSkipWhenLoading()
        {
            return true;
        }
    }

}