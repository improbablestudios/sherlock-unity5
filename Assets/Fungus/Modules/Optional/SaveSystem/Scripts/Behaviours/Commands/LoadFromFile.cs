﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.SaveSystem;

namespace Fungus.Commands
{

    [CommandInfo("Save",
                "Load a scene from a file.")]
    [AddComponentMenu("Fungus/Commands/Save/Load From File")]
    public class LoadFromFile : Command
    {
        [Tooltip("Save file number")]
        [DataMin(0)]
        [FormerlySerializedAs("_saveFileNumber")]
        public IntegerData m_SaveFileNumber = new IntegerData();

        public override void OnEnter()
        {
            SaveSystemManager.Instance.Load("save " + m_SaveFileNumber.Value.ToString());
            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_SaveFileNumber);
        }
    }

}