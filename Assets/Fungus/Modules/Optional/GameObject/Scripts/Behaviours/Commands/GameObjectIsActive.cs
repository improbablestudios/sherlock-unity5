﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Check whether or not a game object is active.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Game Object Is Active")]
    public class GameObjectIsActive : ReturnValueCommand<bool> 
    {
        [Tooltip("Game object to get the active value from")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;

            return gameObject.activeInHierarchy;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" is active", m_ReturnVariable, m_Object);
        }
    }
    
}