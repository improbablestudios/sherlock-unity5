﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Set the tag of a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Set Game Object Tag")]
    public class SetGameObjectTag : GameObjectCommand
    {
        [Tooltip("The new tag")]
        [TagDataField]
        public StringData m_Tag = new StringData();

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            string tag = m_Tag.Value;

            gameObject.tag = tag;

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_Tag);
        }
    }

}