﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Get the layer of a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Get Game Object Layer")]
    public class GetGameObjectLayer : ReturnValueCommand<int>
    {
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        [LayerDataField]
        public IntegerVariable m_ReturnVariable;
        
        public override int GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;

            return gameObject.layer;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" layer", m_ReturnVariable, m_Object);
        }
    }

}