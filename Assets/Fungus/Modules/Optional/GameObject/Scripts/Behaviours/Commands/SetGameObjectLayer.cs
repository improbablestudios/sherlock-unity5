﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Set the layer of a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Set Game Object Layer")]
    public class SetGameObjectLayer : GameObjectCommand
    {
        [Tooltip("The new layer")]
        [DataMin(0)]
        [LayerDataField]
        public IntegerData m_Layer = new IntegerData();

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            int layer = m_Layer.Value;

            gameObject.layer = layer;

            Continue();
        }
        
        public override string GetSummary()
        {
            string layerString = m_Layer.ToString();
            if (m_Layer.IsConstant)
            {
                layerString = LayerMask.LayerToName(m_Layer.Value);
            }
            return base.GetSummary() + string.Format(" to {0}", layerString);
        }
    }

}