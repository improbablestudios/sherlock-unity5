﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Get a list of components on a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Get Game Object Components")]
    public class GetGameObjectComponents : ReturnValueCommand<List<Component>>
    {
        [Tooltip("The target game object")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The component type")]
        [CheckNotEmptyData]
        [VariableValueTypeNameDataField(typeof(Component))]
        public StringData m_ComponentTypeName = new StringData();

        [Tooltip("If true, also get components in children")]
        public BooleanData m_IncludeChildren = new BooleanData();

        [Tooltip("If true, also get components that are inactive")]
        public BooleanData m_IncludeInactiveComponents = new BooleanData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(List<Component>))]
        public Variable m_ReturnVariable;
        
        public override List<Component> GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;
            bool includeChildren = m_IncludeChildren.Value;
            bool includeInactiveComponents = m_IncludeInactiveComponents.Value;
            string componentTypeName = m_ComponentTypeName.Value;
            Type type = null;
            if (!string.IsNullOrEmpty(componentTypeName))
            {
                type = Type.GetType(componentTypeName);
            }

            if (includeChildren)
            {
                return gameObject.GetComponentsInChildren(type, includeInactiveComponents).ToList();
            }
            else
            {
                return gameObject.GetComponents(type).ToList();
            }
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            string componentTypeNameString = m_ComponentTypeName.ToString();
            if (m_ComponentTypeName.IsConstant)
            {
                componentTypeNameString = Type.GetType(m_ComponentTypeName.Value).Name;
            }

            string summary = string.Format("{0} = component on \"{1}\" of type {2}", m_ReturnVariable, m_Object, componentTypeNameString);
            if (m_IncludeInactiveComponents.IsConstant)
            {
                if (m_IncludeInactiveComponents.Value)
                {
                    return summary + " (including inactive)";
                }
                else
                {
                    return summary;
                }
            }
            return summary + string.Format(" (including inactive if {0})", m_IncludeInactiveComponents);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_ComponentTypeName) || propertyPath == nameof(m_ReturnVariable))
            {
                if (m_ComponentTypeName.IsConstant)
                {
                    if (m_ReturnVariable != null)
                    {
                        string componentTypeName = m_ComponentTypeName.Value;
                        Type type = null;
                        if (!string.IsNullOrEmpty(componentTypeName))
                        {
                            type = Type.GetType(componentTypeName);
                            if (type != null && !m_ReturnVariable.ValueType.GetGenericArguments()[0].IsAssignableFrom(type))
                            {
                                return string.Format("Component Type '{0}' and Return Variable type '{1}' don't match", type.Name, FlowchartItemInfoAttribute.GetInfo(m_ReturnVariable.GetType()).GetName(m_ReturnVariable.GetType()));
                            }
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_IncludeInactiveComponents))
            {
                if (m_IncludeChildren.IsConstant && !m_IncludeChildren.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}