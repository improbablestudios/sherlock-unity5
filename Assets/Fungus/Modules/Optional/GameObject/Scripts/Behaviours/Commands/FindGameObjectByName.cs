﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Search all the active game objects for a game object with the specified name.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Find Game Object By Name")]
    public class FindGameObjectByName : ReturnValueCommand<GameObject>
    {
        [Tooltip("The game object name to search for")]
        [FormerlySerializedAs("_gameObject")]
        public StringData m_GameObjectName = new StringData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public GameObjectVariable m_ReturnVariable;
        
        public override GameObject GetReturnValue()
        {
            return GameObject.Find(m_GameObjectName.Value);
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = game object with name \"{1}\"", m_ReturnVariable, m_GameObjectName);
        }
    }

}