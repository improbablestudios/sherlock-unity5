using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Destroy a game object in the scene.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Destroy Game Object")]
    public class DestroyGameObject : GameObjectCommand
    {
        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            
            Destroy(gameObject);

            Continue();
        }
    }

}