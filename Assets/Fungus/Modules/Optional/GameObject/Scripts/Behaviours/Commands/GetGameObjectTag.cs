﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Get the tag of a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Get Game Object Tag")]
    public class GetGameObjectTag : ReturnValueCommand<string>
    {
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public StringVariable m_ReturnVariable;
        
        public override string GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;

            return gameObject.tag;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" tag", m_ReturnVariable, m_Object);
        }
    }

}