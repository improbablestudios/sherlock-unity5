﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class GameObjectCommand : Command
    {
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [FormerlySerializedAs("m_SourceObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Object);
        }
    }

}