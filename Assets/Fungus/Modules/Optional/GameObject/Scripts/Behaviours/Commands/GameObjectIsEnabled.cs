﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Check whether or not a component is enabled.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Game Object Is Enabled")]
    public class GameObjectIsEnabled : ReturnValueCommand<bool>
    {
        [Tooltip("GameObject to get the active value from")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The variable to store the result in")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;

            return gameObject.activeSelf;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("{0} = \"{1}\" is enabled", m_ReturnVariable, m_Object);
        }
    }

}