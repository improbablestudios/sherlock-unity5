﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Get a component on a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Get Game Object Component")]
    public class GetGameObjectComponent : ReturnValueCommand<Component>
    {
        [Tooltip("The target game object")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The component type")]
        [CheckNotEmptyData]
        [VariableValueTypeNameDataField(typeof(Component))]
        public StringData m_ComponentTypeName = new StringData();

        [Tooltip("If true, also get components in children")]
        public BooleanData m_IncludeChildren = new BooleanData();

        [Tooltip("If true, also get components that are inactive")]
        public BooleanData m_IncludeInactiveComponent = new BooleanData();

        [Tooltip("The variable to store the result in")]
        [VariableOfTypeField(typeof(Component))]
        public Variable m_ReturnVariable;
        
        public override Component GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;
            bool includeChildren = m_IncludeChildren.Value;
            bool includeInactiveComponent = m_IncludeInactiveComponent.Value;
            string componentTypeName = m_ComponentTypeName.Value;
            Type type = null;
            if (!string.IsNullOrEmpty(componentTypeName))
            {
                type = Type.GetType(componentTypeName);
            }

            if (includeChildren)
            {
                return gameObject.GetComponentInChildren(type, includeInactiveComponent);
            }
            else
            {
                return gameObject.GetComponent(type);
            }
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            string componentTypeNameString = m_ComponentTypeName.ToString();
            if (m_ComponentTypeName.IsConstant)
            {
                componentTypeNameString = Type.GetType(m_ComponentTypeName.Value).Name;
            }

            string summary = string.Format("{0} = components on \"{1}\" of type {2}", m_ReturnVariable, m_Object, componentTypeNameString);
            if (m_IncludeInactiveComponent.IsConstant)
            {
                if (m_IncludeInactiveComponent.Value)
                {
                    return summary + " (including inactive)";
                }
                else
                {
                    return summary;
                }
            }
            return summary + string.Format(" (including inactive if {0})", m_IncludeInactiveComponent);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_ComponentTypeName) || propertyPath == nameof(m_ReturnVariable))
            {
                if (m_ComponentTypeName.IsConstant)
                {
                    if (m_ReturnVariable != null)
                    {
                        string componentTypeName = m_ComponentTypeName.Value;
                        Type type = null;
                        if (!string.IsNullOrEmpty(componentTypeName))
                        {
                            type = Type.GetType(componentTypeName);
                            if (type != null && !m_ReturnVariable.ValueType.IsAssignableFrom(type))
                            {
                                return string.Format("Component Type '{0}' and Return Variable type '{1}' don't match", type.Name, FlowchartItemInfoAttribute.GetInfo(m_ReturnVariable.GetType()).GetName(m_ReturnVariable.GetType()));
                            }
                        }
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_IncludeInactiveComponent))
            {
                if (m_IncludeChildren.IsConstant && !m_IncludeChildren.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}