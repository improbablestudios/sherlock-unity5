using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Activate or deactivate a game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Set Game Object Active")]
    public class SetGameObjectActive : GameObjectCommand
    {
        [Tooltip("If true, enable the game object, else disable the game object")]
        [FormerlySerializedAs("_active")]
        [FormerlySerializedAs("m_Active")]
        [FormerlySerializedAs("m_Enable")]
        public BooleanData m_Active = new BooleanData();

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            bool active = m_Active.Value;
            
            gameObject.SetActive(active);

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" active = {0}", m_Active);
        }
    }

}