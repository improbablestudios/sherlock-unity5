using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Game Object",
                 "Create a new instance of a scene or prefab game object.")]
    [AddComponentMenu("Fungus/Commands/Game Object/Instantiate Game Object")]
    public class InstantiateGameObject : ReturnValueCommand<GameObject>
    {
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [FormerlySerializedAs("m_SourceObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("storeInVariable")]
        [FormerlySerializedAs("m_StoreInVariable")]
        public GameObjectVariable m_ReturnVariable;

        protected override bool WillLoadPersistableFieldsOnAwake()
        {
            return false;
        }

        public override GameObject GetReturnValue()
        {
            GameObject obj = m_Object.Value as GameObject;

            return InstantiateAndRegister(obj) as GameObject;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Object);
        }
    }

}