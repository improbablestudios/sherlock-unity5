﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not a game object will respond to any sort of user input (i.e. clicking, dragging, hovering, etc).")]
    [AddComponentMenu("Fungus/Commands/Input/Set Interactable")]
    public class SetInteractable : Command
    {
        [GameObjectWithComponentDataField(typeof(Selectable), typeof(CanvasGroup), typeof(Collider), typeof(Collider2D))]
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The game object will respond to user input")]
        [FormerlySerializedAs("_interactable")]
        public BooleanData m_Interactable = new BooleanData(true);

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            bool interactable = m_Interactable.Value;
            
            Selectable targetSelectable = gameObject.GetComponent<Selectable>();
            if (targetSelectable != null)
            {
                targetSelectable.interactable = interactable;
            }

            CanvasGroup targetCanvasGroup = gameObject.GetComponent<CanvasGroup>();
            if (targetCanvasGroup != null)
            {
                targetCanvasGroup.interactable = interactable;
            }

            InputDetector[] targetInputDetectors = gameObject.GetComponents<InputDetector>();
            foreach (InputDetector targetInputDetector in targetInputDetectors)
            {
                targetInputDetector.DetectInput = interactable;
            }

            Draggable targetDraggable = gameObject.GetComponent<Draggable>();
            if (targetDraggable != null)
            {
                targetDraggable.Interactable = interactable;
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Object) + string.Format(" interactable = {0}", m_Interactable);
        }
    }

}