﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not a draggable object will rotate when dragged.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Draggable Rotatable")]
    public class SetDraggableRotatable : DraggableCommand
    {
        [Tooltip("If true, the draggable will rotate when dragged")]
        [FormerlySerializedAs("_movable")]
        public BooleanData m_Rotatable = new BooleanData(true);

        public override void OnEnter()
        {
            Draggable draggable = m_Draggable.Value;
            bool movable = m_Rotatable.Value;

            draggable.Rotatable = movable;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" rotatable = {1}", m_Draggable, m_Rotatable);
        }
    }

}