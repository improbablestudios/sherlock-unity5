﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class AnimateDraggableCommand : DraggableCommand
    {
        [Tooltip("The duration of the move")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData();

        [Tooltip("The ease type")]
        [FormerlySerializedAs("_ease")]
        public EaseData m_Ease = new EaseData(Ease.InOutQuad);
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Draggable) + GetDurationSummary(m_Duration);
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }
        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Ease))
            {
                if (m_Duration.IsConstant && m_Duration.Value <= 0)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }
    }

}