﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not a game object will respond to being hovered over.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Hoverable")]
    public class SetHoverable : MouseInputCommand
    {
        [Tooltip("The game object will respond to hovering")]
        [FormerlySerializedAs("_interactable")]
        [FormerlySerializedAs("m_Interactable")]
        public BooleanData m_Hoverable = new BooleanData(true);

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            bool hoverable = m_Hoverable.Value;

            PointerEnterDetector pointerEnterDetector = gameObject.GetComponent<PointerEnterDetector>();
            if (pointerEnterDetector == null)
            {
                pointerEnterDetector = gameObject.AddComponent<PointerEnterDetector>();
            }
            pointerEnterDetector.DetectInput = hoverable;

            PointerExitDetector pointerExitDetector = gameObject.GetComponent<PointerExitDetector>();
            if (pointerExitDetector == null)
            {
                pointerExitDetector = gameObject.AddComponent<PointerExitDetector>();
            }
            pointerExitDetector.DetectInput = hoverable;

            Continue();
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" hoverable = {0}", m_Hoverable);
        }
    }

}