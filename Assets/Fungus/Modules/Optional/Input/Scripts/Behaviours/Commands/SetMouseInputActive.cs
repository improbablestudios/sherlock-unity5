﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not the application will respond to mouse input.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Mouse Input Active")]
    public class SetMouseInputActive : Command
    {
        [Tooltip("The application will respond to mouse input")]
        public BooleanData m_Active = new BooleanData(true);

        public override void OnEnter()
        {
            bool active = m_Active.Value;

            ControllableInputModule.Instance.ProcessMouseEvents = active;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Active);
        }
    }

}