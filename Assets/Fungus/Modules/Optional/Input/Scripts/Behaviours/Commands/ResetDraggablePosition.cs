﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Reset a draggable object to its original position.")]
    [AddComponentMenu("Fungus/Commands/Input/Reset Draggable Position")]
    public class ResetDraggablePosition : AnimateDraggableCommand
    {
        public override void OnEnter()
        {
            Draggable draggable = m_Draggable.Value;
            float duration = m_Duration.Value;
            Ease ease = m_Ease.Value;

            draggable.ResetPosition(duration, ease, ContinueAfterFinished);

            if (!WillWait())
            {
                Continue();
            }
        }
    }

}