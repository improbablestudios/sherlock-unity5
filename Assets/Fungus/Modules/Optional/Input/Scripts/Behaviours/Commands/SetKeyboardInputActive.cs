﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not the application will respond to keyboard input.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Keyboard Input Active")]
    public class SetKeyboardInputActive : Command
    {
        [Tooltip("The application will respond to keyboard input.")]
        public BooleanData m_Active = new BooleanData(true);

        public override void OnEnter()
        {
            bool active = m_Active.Value;

            ControllableInputModule.Instance.ProcessKeyboardEvents = active;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Active);
        }
    }

}