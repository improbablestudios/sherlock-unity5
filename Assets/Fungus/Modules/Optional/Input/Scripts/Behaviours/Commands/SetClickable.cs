﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not a game object will respond to being clicked.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Clickable")]
    public class SetClickable : MouseInputCommand
    {
        [Tooltip("Controls if the object is clickable or not")]
        [FormerlySerializedAs("_interactable")]
        [FormerlySerializedAs("m_Interactable")]
        public BooleanData m_Clickable = new BooleanData();

        public override void OnEnter()
        {
            GameObject gameObject = m_Object.Value;
            bool clickable = m_Clickable.Value;
            
            Button button = gameObject.GetComponent<Button>();
            if (button != null)
            {
                button.interactable = clickable;
            }
            
            PointerClickDetector clickDetector = gameObject.GetComponent<PointerClickDetector>();
            if (clickDetector == null)
            {
                clickDetector = gameObject.AddComponent<PointerClickDetector>();
            }
            clickDetector.DetectInput = clickable;

            Continue();
        }
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\" clickable = {1}", m_Object, m_Clickable);
        }
    }

}