﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class DraggableCommand : Command
    {
        [Tooltip("The target draggable")]
        [CheckNotNullData]
        public DraggableData m_Draggable = new DraggableData();
       
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Draggable);
        }
    }

}