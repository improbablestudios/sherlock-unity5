﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Check whether or not a game object is on target.")]
    [AddComponentMenu("Fungus/Commands/Input/Game Object Is On Target")]
    public class GameObjectIsOnTarget : ReturnValueCommand<bool>
    {
        [Tooltip("The target draggable")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();

        [Tooltip("The target type")]
        [FormerlySerializedAs("targetType")]
        public MoveTargetType m_TargetType;

        [Tooltip("The target zone")]
        [FormerlySerializedAs("_targetZone")]
        public TargetZoneData m_TargetZone = new TargetZoneData();

        [Tooltip("The target grid")]
        [FormerlySerializedAs("_targetGrid")]
        public TargetGridData m_TargetGrid = new TargetGridData();

        [Tooltip("The target tile")]
        [FormerlySerializedAs("_targetTile")]
        public Vector2Data m_TargetTile = new Vector2Data();

        [Tooltip("The target position")]
        [FormerlySerializedAs("_targetPosition")]
        public Vector2Data m_TargetPosition = new Vector2Data();

        [Tooltip("The variable to store the result in")]
        [FormerlySerializedAs("returnVariable")]
        public BooleanVariable m_ReturnVariable;
        
        public override bool GetReturnValue()
        {
            GameObject gameObject = m_Object.Value;
            Vector2 gameObjectPosition = gameObject.transform.position;
            Vector2 targetTileCoordinates = m_TargetTile.Value;
            TargetZone targetZone = m_TargetZone.Value;
            TargetGrid targetGrid = m_TargetGrid.Value;
            Vector2 targetPosition = m_TargetPosition.Value;

            if (m_TargetType == MoveTargetType.Zone)
            {
                if (targetZone != null)
                {
                    if (targetZone.Contains(gameObjectPosition))
                    {
                        return true;
                    }
                }
            }
            else if (m_TargetType == MoveTargetType.Grid)
            {
                if (targetGrid != null)
                {
                    if (targetGrid.TileContains(targetTileCoordinates, gameObjectPosition))
                    {
                        return true;
                    }
                }
            }
            else if (m_TargetType == MoveTargetType.Position)
            {
                if (targetPosition == gameObjectPosition)
                {
                    return true;
                }
            }

            return false;
        }

        public override Variable GetReturnVariable()
        {
            return m_ReturnVariable;
        }

        public override string GetSummary()
        {
            VariableData target = null;

            if (m_TargetType == MoveTargetType.Zone)
            {
                target = m_TargetZone;
            }
            else if (m_TargetType == MoveTargetType.Grid)
            {
                target = m_TargetGrid;
            }
            else if (m_TargetType == MoveTargetType.Position)
            {
                target = m_TargetPosition;
            }

            string positionString = "";
            if (m_TargetType == MoveTargetType.Grid)
            {
                positionString = " " + "(" + m_TargetTile.ToString() + ")";
            }

            return string.Format("{0} = \"{1}\" is on \"{2}\"{3}", m_ReturnVariable, m_Object, target, positionString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetType == MoveTargetType.Zone)
                {
                    if ((Application.isPlaying || m_TargetZone.IsConstant) && m_TargetZone.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetGrid))
            {
                if (m_TargetType == MoveTargetType.Grid)
                {
                    if ((Application.isPlaying || m_TargetGrid.IsConstant) && m_TargetGrid.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetType != MoveTargetType.Zone)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetGrid))
            {
                if (m_TargetType != MoveTargetType.Grid)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetTile))
            {
                if (m_TargetType != MoveTargetType.Grid)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                if (m_TargetType != MoveTargetType.Position)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}