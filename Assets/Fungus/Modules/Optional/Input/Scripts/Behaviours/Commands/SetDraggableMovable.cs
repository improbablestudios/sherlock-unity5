﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set whether or not a draggable object will move when dragged.")]
    [AddComponentMenu("Fungus/Commands/Input/Set Draggable Movable")]
    public class SetDraggableMovable : DraggableCommand
    {
        [Tooltip("If true, the draggable will move when dragged")]
        [FormerlySerializedAs("_movable")]
        public BooleanData m_Movable = new BooleanData(true);
        
        public override void OnEnter()
        {
            Draggable draggable = m_Draggable.Value;
            bool movable = m_Movable.Value;

            draggable.Movable = movable;

            Continue();
        }
        
        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" movable = {1}", m_Draggable, m_Movable);
        }
    }

}