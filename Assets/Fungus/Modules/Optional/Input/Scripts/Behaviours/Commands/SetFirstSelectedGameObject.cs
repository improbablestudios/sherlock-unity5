﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Input",
                 "Set the game object that will be selected first.")]
    [AddComponentMenu("Fungus/Commands/Input/Set First Selected Game Object")]
    public class SetFirstSelectedGameObject : Command
    {
        [Tooltip("The game object that will be selected first")]
        public GameObjectData m_Object = new GameObjectData();

        public override void OnEnter()
        {
            GameObject obj = m_Object.Value;

            EventSystem.current.firstSelectedGameObject = obj;

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("{0}", m_Object);
        }
    }

}