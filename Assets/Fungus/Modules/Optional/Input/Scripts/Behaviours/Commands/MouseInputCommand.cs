﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class MouseInputCommand : Command
    {
        [GameObjectWithComponentDataField(typeof(Selectable), typeof(Collider), typeof(Collider2D))]
        [Tooltip("The target game object")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();
        
        public override string GetSummary()
        {
            return string.Format("\"{0}\"", m_Object);
        }
    }

}