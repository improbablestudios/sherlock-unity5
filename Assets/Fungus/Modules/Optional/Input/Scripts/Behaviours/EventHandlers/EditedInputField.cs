﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/UI",
                      "This block will execute when the user finishes editing the text in the input field.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/UI/Edited")]
    public class EditedInputField : UIInputEventHandler
    {
        [Tooltip("The UI Input Field that the user can enter text into")]
        [FormerlySerializedAs("_inputField")]
        [CheckNotNullData]
        public InputFieldData m_InputField = new InputFieldData();

        [Tooltip("The value to compare")]
        public StringData m_Value = new StringData();
        
        protected override void AddListener()
        {
            InputField inputField = m_InputField.Value;
            inputField.onEndEdit.AddListener(OnEndEdit);
        }

        protected override void RemoveListener()
        {
            InputField inputField = m_InputField.Value;
            inputField.onEndEdit.AddListener(OnEndEdit);
        }

        public void OnEndEdit(string v)
        {
            string value = m_Value.Value;
            switch (m_IsChanged)
            {
                case ValueCondition.ToAnyValue:
                    ExecuteBlock();
                    break;
                case ValueCondition.ToValue:
                    if (v == value)
                    {
                        ExecuteBlock();
                    }
                    break;
                case ValueCondition.ToValueOtherThan:
                    if (v != value)
                    {
                        ExecuteBlock();
                    }
                    break;
            }
        }

        public override string GetSummary()
        {
            return string.Format("{0} \"{1}\"", GetType().Name, m_InputField) + GetChangedSummary(m_Value);
        }
    }

}
