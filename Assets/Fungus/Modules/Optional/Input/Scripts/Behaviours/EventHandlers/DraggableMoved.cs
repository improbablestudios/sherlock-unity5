﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/Draggable",
                      "This block will execute when the player moves a draggable object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Draggable/Draggable Moved")]
    public class DraggableMoved : DraggableEventHandler
    {
        [Tooltip("Only detect event at start of movement, during movement, or at the end of movement")]
        public MoveTrigger m_MoveTrigger = MoveTrigger.EndedMove;

        [Tooltip("Detect event when object is moved on target, off target, or anywhere")]
        [FormerlySerializedAs("isDropped")]
        [FormerlySerializedAs("m_IsDropped")]
        [FormerlySerializedAs("m_IsMoved")]
        public TargetPositionCondition m_TargetPositionCondition;

        [Tooltip("The target type")]
        [FormerlySerializedAs("targetType")]
        public MoveTargetType m_TargetType;

        [Tooltip("The target zone")]
        [FormerlySerializedAs("_targetZone")]
        public TargetZoneData m_TargetZone = new TargetZoneData();

        [Tooltip("The target grid")]
        [FormerlySerializedAs("_targetGrid")]
        public TargetGridData m_TargetGrid = new TargetGridData();

        [Tooltip("The target tile")]
        [FormerlySerializedAs("_targetTile")]
        public Vector2Data m_TargetTile = new Vector2Data();

        [Tooltip("The target position")]
        [FormerlySerializedAs("_targetPosition")]
        public Vector2Data m_TargetPosition = new Vector2Data();

        protected override UnityEvent GetUnityEvent()
        {
            Draggable draggable = m_Draggable.Value;

            if (m_MoveTrigger == MoveTrigger.BeganMove)
            {
                return draggable.OnBeginMove;
            }
            else if (m_MoveTrigger == MoveTrigger.IsMoving)
            {
                return draggable.OnMove;
            }
            else if (m_MoveTrigger == MoveTrigger.EndedMove)
            {
                return draggable.OnEndMove;
            }

            return null;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            Draggable draggable = m_Draggable.Value;
            Vector2 gameObjectPosition = draggable.transform.position;
            Vector2 targetTileCoordinates = m_TargetTile.Value;
            TargetZone targetZone = m_TargetZone.Value;
            TargetGrid targetGrid = m_TargetGrid.Value;
            Vector2 targetPosition = m_TargetPosition.Value;

            switch (m_TargetPositionCondition)
            {
                case TargetPositionCondition.Anywhere:
                    return true;
                case TargetPositionCondition.OnTarget:
                    if (m_TargetType == MoveTargetType.Zone)
                    {
                        if (targetZone != null)
                        {
                            if (targetZone.Contains(gameObjectPosition))
                            {
                                return true;
                            }
                        }
                    }
                    else if (m_TargetType == MoveTargetType.Grid)
                    {
                        if (targetGrid != null)
                        {
                            if (targetGrid.TileContains(targetTileCoordinates, gameObjectPosition))
                            {
                                return true;
                            }
                        }
                    }
                    else if (m_TargetType == MoveTargetType.Position)
                    {
                        if (targetPosition == gameObjectPosition)
                        {
                            return true;
                        }
                    }
                    break;
                case TargetPositionCondition.OffTarget:
                    if (m_TargetType == MoveTargetType.Zone)
                    {
                        if (targetZone != null)
                        {
                            if (!targetZone.Contains(gameObjectPosition))
                            {
                                return true;
                            }
                        }
                    }
                    else if (m_TargetType == MoveTargetType.Grid)
                    {
                        if (targetGrid != null)
                        {
                            if (!targetGrid.TileContains(targetTileCoordinates, gameObjectPosition))
                            {
                                return true;
                            }
                        }
                    }
                    else if (m_TargetType == MoveTargetType.Position)
                    {
                        if (targetPosition != gameObjectPosition)
                        {
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }
        
        public override string GetSummary()
        {
            string targetString = "";

            if (m_TargetPositionCondition != TargetPositionCondition.Anywhere)
            {
                if (m_TargetType == MoveTargetType.Zone)
                {
                    targetString = string.Format(" \"{0}\"", m_TargetZone);
                }
                else if (m_TargetType == MoveTargetType.Grid)
                {
                    targetString = string.Format(" \"{0}\"", m_TargetGrid);
                }
                else if (m_TargetType == MoveTargetType.Position)
                {
                    targetString = string.Format(" {0}", m_TargetPosition);
                }
            }

            return string.Format("\"{0}\" {1} {2}{3}", m_Draggable, m_MoveTrigger, m_TargetPositionCondition, targetString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetType == MoveTargetType.Zone)
                {
                    if ((Application.isPlaying || m_TargetZone.IsConstant) && m_TargetZone.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetGrid))
            {
                if (m_TargetType == MoveTargetType.Grid)
                {
                    if ((Application.isPlaying || m_TargetGrid.IsConstant) && m_TargetGrid.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetType))
            {
                if (m_TargetPositionCondition == TargetPositionCondition.Anywhere)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetPositionCondition == TargetPositionCondition.Anywhere)
                {
                    return false;
                }
                if (m_TargetType != MoveTargetType.Zone)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetGrid))
            {
                if (m_TargetPositionCondition == TargetPositionCondition.Anywhere)
                {
                    return false;
                }
                if (m_TargetType != MoveTargetType.Grid)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetTile))
            {
                if (m_TargetPositionCondition == TargetPositionCondition.Anywhere)
                {
                    return false;
                }
                if (m_TargetType != MoveTargetType.Grid)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                if (m_TargetPositionCondition == TargetPositionCondition.Anywhere)
                {
                    return false;
                }
                if (m_TargetType != MoveTargetType.Position)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}