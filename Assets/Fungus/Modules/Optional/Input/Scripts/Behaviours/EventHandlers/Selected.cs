﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when the user selects the target object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Selected")]
    public class Selected : InputEventHandler
    {
        public override Type GetInputDetectorType()
        {
            return typeof(SelectDetector);
        }
    }

}
