using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using System.Collections.Generic;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when a key press event occurs.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Pressed Key")]
    public class PressedKey : EventHandler
    {
        public enum KeyPressType
        {
            KeyDown,    // Execute once when the key is pressed down
            KeyUp,        // Execute once when the key is released
            KeyRepeat    // Execute once per frame when key is held down
        }

        [Tooltip("The type of keypress to activate on")]
        [FormerlySerializedAs("keyPressType")]
        public KeyPressType m_KeyPressType;

        [Tooltip("If true, the event will execute when any key is pressed. Otherwise, it will only execute if a certain key is pressed")]
        public BooleanData m_Any = new BooleanData();

        [Tooltip("Keycode of the key to activate on")]
        [FormerlySerializedAs("_keyCode")]
        public KeyCodeData m_KeyCode = new KeyCodeData();
        
        protected virtual void Update()
        {
            if (ControllableInputModule.Instance != null && ControllableInputModule.Instance.ProcessKeyboardEvents)
            {
                bool any = m_Any.Value;
                KeyCode keyCode = m_KeyCode.Value;

                switch (m_KeyPressType)
                {
                    case KeyPressType.KeyDown:
                        if (any)
                        {
                            if (Input.anyKeyDown)
                            {
                                ExecuteBlock();
                            }
                        }
                        else
                        {
                            if (Input.GetKeyDown(keyCode))
                            {
                                ExecuteBlock();
                            }
                        }
                        break;
                    case KeyPressType.KeyUp:
                        if (any)
                        {
                            foreach (KeyCode key in System.Enum.GetValues(typeof(KeyCode)))
                            {
                                if (Input.GetKeyUp(key))
                                {
                                    ExecuteBlock();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (Input.GetKeyUp(keyCode))
                            {
                                ExecuteBlock();
                            }
                        }
                        break;
                    case KeyPressType.KeyRepeat:
                        if (any)
                        {
                            if (Input.anyKey)
                            {
                                ExecuteBlock();
                            }
                        }
                        else
                        {
                            if (Input.GetKey(keyCode))
                            {
                                ExecuteBlock();
                            }
                        }
                        break;
                }
            }
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_KeyCode))
            {
                if (m_Any.IsConstant && m_Any.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override string GetSummary()
        {
            string summary = "Any Key";
            if (!m_Any.IsConstant || !m_Any.Value)
            {
                summary = m_KeyCode.ToString();
            }
            switch (m_KeyPressType)
            {
            case KeyPressType.KeyDown:
                summary += " Down";
                break;
            case KeyPressType.KeyUp:
                summary += " Up";
                break;
            case KeyPressType.KeyRepeat:
                summary += " Repeat";
                break;
            }
            return summary;
        }
    }

}