﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when the user clicks on the target object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Clicked")]
    public class Clicked : InputEventHandler
    {
        public override Type GetInputDetectorType()
        {
            return typeof(PointerClickDetector);
        }

        protected override UnityEvent GetUnityEvent()
        {
            GameObject target = m_Object.Value;
            
            if (target != null)
            {
                Button targetButton = target.gameObject.GetComponent<Button>();
                if (targetButton == null)
                {
                    if (m_detector != null)
                    {
                        return m_detector.OnInputDetected;
                    }
                }
                else
                {
                    return targetButton.onClick;
                }
            }

            return null;
        }
    }

}
