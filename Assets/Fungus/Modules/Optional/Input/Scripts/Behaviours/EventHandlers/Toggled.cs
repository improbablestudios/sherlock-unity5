﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/UI",
                      "This block will execute when the user selects a toggle.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/UI/Toggled")]
    public class Toggled : UIInputEventHandler
    {
        [Tooltip("The target toggle")]
        [FormerlySerializedAs("_toggle")]
        [CheckNotNullData]
        public ToggleData m_Toggle = new ToggleData();

        [Tooltip("The value to compare")]
        public BooleanData m_Value = new BooleanData(true);
        
        protected override void AddListener()
        {
            Toggle toggle = m_Toggle.Value;
            toggle.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void RemoveListener()
        {
            Toggle toggle = m_Toggle.Value;
            toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(bool v)
        {
            bool value = m_Value.Value;
            switch (m_IsChanged)
            {
                case ValueCondition.ToAnyValue:
                    ExecuteBlock();
                    break;
                case ValueCondition.ToValue:
                    if (v == value)
                    {
                        ExecuteBlock();
                    }
                    break;
                case ValueCondition.ToValueOtherThan:
                    if (v != value)
                    {
                        ExecuteBlock();
                    }
                    break;
            }
        }

        public override string GetSummary()
        {
            return string.Format("{0} \"{1}\"", GetType().Name, m_Toggle) + GetChangedSummary(m_Value);
        }
    }

}
