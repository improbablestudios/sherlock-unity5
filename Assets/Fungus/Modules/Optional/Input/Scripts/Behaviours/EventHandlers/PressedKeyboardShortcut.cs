﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when a keyboard shortcut press event occurs.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Pressed Keyboard Shortcut")]
    public class PressedKeyboardShortcut : UnityEventHandler
    {
        [Tooltip("The target keyboard shortcut")]
        [FormerlySerializedAs("keyboardShortcut")]
        [CheckNotNullData]
        public KeyboardShortcutData m_KeyboardShortcut;

        protected override UnityEvent GetUnityEvent()
        {
            if (m_KeyboardShortcut.Value != null)
            {
                return m_KeyboardShortcut.Value.OnPressed;
            }
            return null;
        }
        
        public override string GetSummary()
        {
            return m_KeyboardShortcut + " " + "Shortcut Pressed";
        }
    }

}