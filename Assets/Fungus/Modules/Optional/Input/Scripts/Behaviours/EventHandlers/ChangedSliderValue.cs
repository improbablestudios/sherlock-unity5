﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/UI",
                      "This block will execute when the user changes the value of a slider.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/UI/Changed Slider Value")]
    public class ChangedSliderValue : UIInputEventHandler
    {
        [Tooltip("The target slider")]
        [CheckNotNullData]
        public SliderData m_Slider = new SliderData();

        [Tooltip("The value to compare")]
        public IntegerData m_Value = new IntegerData();
        
        protected override void AddListener()
        {
            Slider slider = m_Slider.Value;
            slider.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void RemoveListener()
        {
            Slider slider = m_Slider.Value;
            slider.onValueChanged.AddListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(float v)
        {
            float value = m_Value.Value;
            switch (m_IsChanged)
            {
                case ValueCondition.ToAnyValue:
                    ExecuteBlock();
                    break;
                case ValueCondition.ToValue:
                    if (v == value)
                    {
                        ExecuteBlock();
                    }
                    break;
                case ValueCondition.ToValueOtherThan:
                    if (v != value)
                    {
                        ExecuteBlock();
                    }
                    break;
            }
        }

        public override string GetSummary()
        {
            return string.Format("{0} of \"{1}\"", GetType().Name, m_Slider) + GetChangedSummary(m_Value);
        }
    }

}
