﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.ReflectionUtilities;
using ImprobableStudios.InputUtilities;
using ImprobableStudios.VectorUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/Draggable",
                      "This block will execute when the player rotates a draggable object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Draggable/Draggable Rotated")]
    public class DraggableRotated : DraggableEventHandler
    {
        [Tooltip("Only detect event at start of rotation, during rotation, or at the end of rotation")]
        public RotateTrigger m_RotateTrigger = RotateTrigger.EndedRotate;

        [Tooltip("Detect event when object is rotated to the specified rotation, to a rotation other than the specified rotation, or to any rotation")]
        [FormerlySerializedAs("m_IsRotated")]
        public TargetRotationCondition m_TargetRotationCondition;

        [Tooltip("The target type")]
        [FormerlySerializedAs("targetType")]
        public RotateTargetType m_TargetType;

        [Tooltip("The target zone")]
        [FormerlySerializedAs("_targetZone")]
        public TargetZoneData m_TargetZone = new TargetZoneData();

        [Tooltip("The target rotation")]
        [FormerlySerializedAs("_targetRotation")]
        public Vector3Data m_TargetRotation = new Vector3Data();

        protected override UnityEvent GetUnityEvent()
        {
            Draggable draggable = m_Draggable.Value;

            if (m_RotateTrigger == RotateTrigger.BeganRotate)
            {
                return draggable.OnBeginRotate;
            }
            else if (m_RotateTrigger == RotateTrigger.IsRotating)
            {
                return draggable.OnRotate;
            }
            else if (m_RotateTrigger == RotateTrigger.EndedRotate)
            {
                return draggable.OnEndRotate;
            }

            return null;
        }

        protected override bool ShouldExecuteBlockWhenUnityEventInvoked()
        {
            Draggable draggable = m_Draggable.Value;
            TargetZone targetZone = m_TargetZone.Value;
            Vector3 targetRotation = m_TargetRotation.Value;

            Vector3 normalizedObjectRotation = new Vector3(Math3d.NormalizeAngle((int)draggable.transform.eulerAngles.x), Math3d.NormalizeAngle((int)draggable.transform.eulerAngles.y), Math3d.NormalizeAngle((int)draggable.transform.eulerAngles.z));

            Vector3 normalizedTargetRotation = Vector3.zero;
            if (m_TargetType == RotateTargetType.Zone)
            {
                normalizedTargetRotation = new Vector3(Math3d.NormalizeAngle((int)targetZone.transform.eulerAngles.x), Math3d.NormalizeAngle((int)targetZone.transform.eulerAngles.y), Math3d.NormalizeAngle((int)targetZone.transform.eulerAngles.z));
            }
            else if (m_TargetType == RotateTargetType.Rotation)
            {
                normalizedTargetRotation = new Vector3(Math3d.NormalizeAngle((int)targetRotation.x), Math3d.NormalizeAngle((int)targetRotation.y), Math3d.NormalizeAngle((int)targetRotation.z));
            }

            switch (m_TargetRotationCondition)
            {
                case TargetRotationCondition.ToAnyRotation:
                    return true;
                case TargetRotationCondition.ToSpecifiedRotation:
                    if (normalizedObjectRotation == normalizedTargetRotation)
                    {
                        return true;
                    }
                    break;
                case TargetRotationCondition.NotToSpecifiedRotation:
                    if (normalizedObjectRotation != normalizedTargetRotation)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        public override string GetSummary()
        {
            string targetString = "";

            if (m_TargetType == RotateTargetType.Zone)
            {
                targetString = string.Format(" \"{0}\"", m_TargetZone);
            }
            else if (m_TargetType == RotateTargetType.Rotation)
            {
                targetString = string.Format(" \"{0}\"", m_TargetRotation);
            }

            return string.Format("\"{0}\" {1} {2}{3}", m_Draggable, m_RotateTrigger, m_TargetRotationCondition, targetString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetType == RotateTargetType.Zone)
                {
                    if ((Application.isPlaying || m_TargetZone.IsConstant) && m_TargetZone.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetType))
            {
                if (m_TargetRotationCondition == TargetRotationCondition.ToAnyRotation)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetZone))
            {
                if (m_TargetRotationCondition == TargetRotationCondition.ToAnyRotation)
                {
                    return false;
                }
                if (m_TargetType != RotateTargetType.Zone)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetRotation))
            {
                if (m_TargetRotationCondition == TargetRotationCondition.ToAnyRotation)
                {
                    return false;
                }
                if (m_TargetType != RotateTargetType.Rotation)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (Array.IndexOf(this.GetDeclaredMemberPaths(), propertyPath) >= 0)
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}