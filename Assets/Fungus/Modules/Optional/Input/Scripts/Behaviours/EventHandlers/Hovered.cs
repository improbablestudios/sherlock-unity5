﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when the user hovers over the target object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Hovered")]
    public class Hovered : InputEventHandler
    {
        [Tooltip("Determines if block executes when pointer enters or exits the target object")]
        [FormerlySerializedAs("isHovered")]
        public OnOffCondition m_IsHovered;

        public override Type GetInputDetectorType()
        {
            if (m_IsHovered == OnOffCondition.On)
            {
                return typeof(PointerEnterDetector);
            }
            else if (m_IsHovered == OnOffCondition.Off)
            {
                return typeof(PointerExitDetector);
            }

            return null;
        }

        protected override UnityEvent GetUnityEvent()
        {
            return m_detector.OnInputDetected;
        }
    }

}
