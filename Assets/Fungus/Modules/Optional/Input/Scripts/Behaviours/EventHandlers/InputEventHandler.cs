﻿using UnityEngine;
using System;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;
using UnityEngine.Events;

namespace Fungus.EventHandlers
{

    public abstract class InputEventHandler : UnityEventHandler
    {
        public enum OnOffCondition
        {
            On,
            Off
        }

        [Tooltip("The target object")]
        [GameObjectWithComponentDataField(
            typeof(Selectable), typeof(Collider), typeof(Collider2D), typeof(IInputEventGenerator))]
        [FormerlySerializedAs("_targetObject")]
        [FormerlySerializedAs("_gameObject")]
        [CheckNotNullData]
        public GameObjectData m_Object = new GameObjectData();
        
        protected InputDetector m_detector;

        protected override void Awake()
        {
            base.Awake();

            GameObject target = m_Object.Value;

            if (target != null)
            {
                m_detector = target.gameObject.GetComponent(GetInputDetectorType()) as InputDetector;
                if (m_detector == null)
                {
                    m_detector = target.gameObject.AddComponent(GetInputDetectorType()) as InputDetector;
                }
            }
        }
        
        public abstract Type GetInputDetectorType();
        
        public override string GetSummary()
        {
            return string.Format("{0} \"{1}\"", GetType().Name, m_Object);
        }

        protected override UnityEvent GetUnityEvent()
        {
            return m_detector.OnInputDetected;
        }
        
        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Object))
            {
                if ((Application.isPlaying || m_Object.IsConstant) && m_Object.Value != null)
                {
                    return InputDetector.GetInputDetectionError(m_Object.Value.gameObject);
                }
            }
            
            return base.GetPropertyError(propertyPath);
        }
    }

}
