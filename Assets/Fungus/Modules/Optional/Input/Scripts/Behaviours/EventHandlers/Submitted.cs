﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when the user submits the target object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Submitted")]
    public class Submitted : InputEventHandler
    {
        public override Type GetInputDetectorType()
        {
            return typeof(SubmitDetector);
        }
    }

}
