﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using System;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input/UI",
                      "This block will execute when the user selects a different item in a dropdown.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/UI/Changed Dropdown Selection")]
    public class ChangedDropdownSelection : UIInputEventHandler
    {
        [Tooltip("The target dropdown")]
        [CheckNotNullData]
        public DropdownData m_Dropdown = new DropdownData();

        [Tooltip("The selected index to compare")]
        [DataMin(-1)]
        public IntegerData m_Value = new IntegerData();
        
        protected override void AddListener()
        {
            Dropdown dropdown = m_Dropdown.Value;
            dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void RemoveListener()
        {
            Dropdown dropdown = m_Dropdown.Value;
            dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        protected virtual void OnValueChanged(int v)
        {
            int value = m_Value.Value;
            switch (m_IsChanged)
            {
                case ValueCondition.ToAnyValue:
                    ExecuteBlock();
                    break;
                case ValueCondition.ToValue:
                    if (v == value)
                    {
                        ExecuteBlock();
                    }
                    break;
                case ValueCondition.ToValueOtherThan:
                    if (v != value)
                    {
                        ExecuteBlock();
                    }
                    break;
            }
        }

        public override string GetSummary()
        {
            return string.Format("{0} of \"{1}\"", GetType().Name, m_Dropdown) + GetChangedSummary(m_Value);
        }
    }

}
