﻿using UnityEngine;
using System;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    public abstract class UIInputEventHandler : EventHandler
    {
        public enum ValueCondition
        {
            ToAnyValue,
            ToValue,
            ToValueOtherThan,
        }

        [Tooltip("Determines whether block executes when a certain value is changed to a value, not to a value, or to any value")]
        public ValueCondition m_IsChanged = ValueCondition.ToValue;
        
        protected string GetChangedSummary(VariableData value)
        {
            switch (m_IsChanged)
            {
                case ValueCondition.ToValue:
                    return string.Format(" to {0}", value);
                case ValueCondition.ToValueOtherThan:
                    return string.Format(" not to {0}", value);
            }

            return "";
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == "m_Value")
            {
                if (m_IsChanged == ValueCondition.ToAnyValue)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_IsChanged))
            {
                return 1;
            }
            if (propertyPath == "m_Value")
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}
