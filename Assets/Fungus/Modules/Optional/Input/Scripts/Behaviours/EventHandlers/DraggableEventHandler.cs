﻿using UnityEngine;
using Fungus.Variables;

namespace Fungus.EventHandlers
{

    public abstract class DraggableEventHandler : UnityEventHandler
    {
        public enum MoveTrigger
        {
            BeganMove,
            IsMoving,
            EndedMove,
        }

        public enum RotateTrigger
        {
            BeganRotate,
            IsRotating,
            EndedRotate,
        }

        public enum TargetPositionCondition
        {
            OnTarget,
            OffTarget,
            Anywhere
        }

        public enum TargetRotationCondition
        {
            ToSpecifiedRotation,
            NotToSpecifiedRotation,
            ToAnyRotation
        }

        [Tooltip("The target draggable")]
        [CheckNotNullData]
        public DraggableData m_Draggable = new DraggableData();
        
        public override string GetSummary()
        {
            return string.Format("{0} \"{1}\"", GetType().Name, m_Draggable.ToString());
        }
    }
}

