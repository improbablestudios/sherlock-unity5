﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Fungus.Variables;
using ImprobableStudios.InputUtilities;

namespace Fungus.EventHandlers
{

    [EventHandlerInfo("Input",
                      "This block will execute when the user cancels the target object.")]
    [AddComponentMenu("Fungus/Event Handlers/Input/Canceled")]
    public class Canceled : InputEventHandler
    {
        public override Type GetInputDetectorType()
        {
            return typeof(CancelDetector);
        }
    }

}
