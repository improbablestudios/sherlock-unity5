﻿using ImprobableStudios.InputUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Input Type List Variable")]
    public class InputTypeListVariable : Variable<List<InputType>>
    {
    }

    [Serializable]
    public class InputTypeListData : VariableData<List<InputType>>
    {
        public InputTypeListData()
        {
            m_DataVal = new List<InputType>();
        }

        public InputTypeListData(List<InputType> v)
        {
            m_DataVal = v;
        }
    }

}
