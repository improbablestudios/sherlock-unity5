﻿using ImprobableStudios.InputUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Target Zone Variable")]
    public class TargetZoneVariable : Variable<TargetZone>
    {
    }

    [Serializable]
    public class TargetZoneData : VariableData<TargetZone>
    {
        public TargetZoneData()
        {
        }

        public TargetZoneData(TargetZone v)
        {
            m_DataVal = v;
        }
    }

}
