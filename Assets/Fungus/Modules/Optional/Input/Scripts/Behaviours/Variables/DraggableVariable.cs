﻿using ImprobableStudios.InputUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Draggable Variable")]
    public class DraggableVariable : Variable<Draggable>
    {
    }

    [Serializable]
    public class DraggableData : VariableData<Draggable>
    {
        public DraggableData()
        {
        }

        public DraggableData(Draggable v)
        {
            m_DataVal = v;
        }
    }

}
