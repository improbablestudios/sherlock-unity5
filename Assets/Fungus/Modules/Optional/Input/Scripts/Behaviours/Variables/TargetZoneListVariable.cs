﻿using ImprobableStudios.InputUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Target Zone List Variable")]
    public class TargetZoneListVariable : Variable<List<TargetZone>>
    {
    }

    [Serializable]
    public class TargetZoneListData : VariableData<List<TargetZone>>
    {
        public TargetZoneListData()
        {
            m_DataVal = new List<TargetZone>();
        }

        public TargetZoneListData(List<TargetZone> v)
        {
            m_DataVal = v;
        }
    }

}
