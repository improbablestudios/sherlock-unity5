﻿using ImprobableStudios.InputUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Draggable List Variable")]
    public class DraggableListVariable : Variable<List<Draggable>>
    {
    }

    [Serializable]
    public class DraggableListData : VariableData<List<Draggable>>
    {
        public DraggableListData()
        {
            m_DataVal = new List<Draggable>();
        }

        public DraggableListData(List<Draggable> v)
        {
            m_DataVal = v;
        }
    }

}
