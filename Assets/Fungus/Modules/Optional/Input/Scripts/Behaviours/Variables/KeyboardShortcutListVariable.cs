﻿using ImprobableStudios.InputUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Keyboard Shortcut List Variable")]
    public class KeyboardShortcutListVariable : Variable<List<KeyboardShortcut>>
    {
    }

    [Serializable]
    public class KeyboardShortcutListData : VariableData<List<KeyboardShortcut>>
    {
        public KeyboardShortcutListData()
        {
            m_DataVal = new List<KeyboardShortcut>();
        }

        public KeyboardShortcutListData(List<KeyboardShortcut> v)
        {
            m_DataVal = v;
        }
    }

}
