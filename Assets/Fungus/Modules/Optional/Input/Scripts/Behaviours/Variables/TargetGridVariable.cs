﻿using ImprobableStudios.InputUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Target Grid Variable")]
    public class TargetGridVariable : Variable<TargetGrid>
    {
    }

    [Serializable]
    public class TargetGridData : VariableData<TargetGrid>
    {
        public TargetGridData()
        {
        }

        public TargetGridData(TargetGrid v)
        {
            m_DataVal = v;
        }
    }

}
