﻿using ImprobableStudios.InputUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/Target Grid List Variable")]
    public class TargetGridListVariable : Variable<List<TargetGrid>>
    {
    }

    [Serializable]
    public class TargetGridListData : VariableData<List<TargetGrid>>
    {
        public TargetGridListData()
        {
            m_DataVal = new List<TargetGrid>();
        }

        public TargetGridListData(List<TargetGrid> v)
        {
            m_DataVal = v;
        }
    }

}
