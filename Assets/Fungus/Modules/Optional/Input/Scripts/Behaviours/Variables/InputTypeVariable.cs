﻿using ImprobableStudios.InputUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Input Type Variable")]
    public class InputTypeVariable : Variable<InputType>
    {
    }

    [Serializable]
    public class InputTypeData : VariableData<InputType>
    {
        public InputTypeData()
        {
        }

        public InputTypeData(InputType v)
        {
            m_DataVal = v;
        }
    }

}
