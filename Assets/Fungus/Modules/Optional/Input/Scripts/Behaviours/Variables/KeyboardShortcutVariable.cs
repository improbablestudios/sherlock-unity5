﻿using ImprobableStudios.InputUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/Keyboard Shortcut Variable")]
    public class KeyboardShortcutVariable : Variable<KeyboardShortcut>
    {
    }

    [Serializable]
    public class KeyboardShortcutData : VariableData<KeyboardShortcut>
    {
        public KeyboardShortcutData()
        {
        }

        public KeyboardShortcutData(KeyboardShortcut v)
        {
            m_DataVal = v;
        }
    }

}
