﻿using ImprobableStudios.CameraUtilities;
using System;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/Fungus/View Variable")]
    public class ViewVariable : Variable<View>
    {
    }

    [Serializable]
    public class ViewData : VariableData<View>
    {
        public ViewData()
        {
        }

        public ViewData(View v)
        {
            m_DataVal = v;
        }
    }

}