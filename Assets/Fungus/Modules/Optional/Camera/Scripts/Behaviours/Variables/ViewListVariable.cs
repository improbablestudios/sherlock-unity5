﻿using ImprobableStudios.CameraUtilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Fungus.Variables
{

    [AddComponentMenu("Fungus/Variables/List/Fungus/View List Variable")]
    public class ViewListVariable : Variable<List<View>>
    {
    }

    [Serializable]
    public class ViewListData : VariableData<List<View>>
    {
        public ViewListData()
        {
            m_DataVal = new List<View>();
        }

        public ViewListData(List<View> v)
        {
            m_DataVal = v;
        }
    }

}