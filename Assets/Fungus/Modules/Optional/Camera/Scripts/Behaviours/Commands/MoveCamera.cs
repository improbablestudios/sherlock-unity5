using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Move the camera to a location.")]
    [AddComponentMenu("Fungus/Commands/Camera/Move Camera")]
    public class MoveCamera : AnimateToTargetCameraCommand
    {
        protected override void AnimateCamera(Camera camera, View targetView, float duration, Ease ease, UnityAction onComplete)
        {
            CameraManager.Instance.PanToView(camera, targetView, duration, ease, onComplete);
        }

        protected override void AnimateCamera(Camera camera, Vector3 targetPosition, float duration, Ease ease, UnityAction onComplete)
        {
            CameraManager.Instance.PanToPosition(camera, targetPosition, duration, ease, onComplete);
        }

        protected override void AnimateCamera(Camera camera, GameObject targetObject, float duration, Ease ease, UnityAction onComplete)
        {
            CameraManager.Instance.PanToPosition(camera, targetObject.transform.position, duration, ease, onComplete);
        }
    }

}