using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera/Swipe",
                 "Deactivate swipe panning mode.")]
    [AddComponentMenu("Fungus/Commands/Camera/Swipe/Dectivate Camera Swipe Mode")]
    public class DeactivateCameraSwipeMode : MainCameraCommand 
    {
        protected override void ControlCamera(Camera camera)
        {
            CameraManager.Instance.DeactivateCameraSwipeMode(camera);
        }
    }

}