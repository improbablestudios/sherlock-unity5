using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Fade the camera out and in again at a specified location.")]
    [AddComponentMenu("Fungus/Commands/Camera/Fade Camera")]
    public class FadeCamera : AnimateToTargetCameraCommand
    {
        [Tooltip("Color to render fullscreen fade texture with when screen is obscured.")]
        [FormerlySerializedAs("_fadeColor")]
        public ColorData m_FadeColor = new ColorData(Color.black);

        [Tooltip("The order of the fade layer.")]
        [FormerlySerializedAs("_sortingOrder")]
        public IntegerData m_SortingOrder = new IntegerData(-100);
       
        protected override void AnimateCamera(Camera camera, View targetView, float duration, Ease ease, UnityAction onComplete)
        {
            Color fadeColor = m_FadeColor.Value;
            int sortingOrder = m_SortingOrder.Value;

            CameraManager.Instance.FadeToView(camera, targetView, fadeColor, duration, ease, sortingOrder, onComplete);
        }

        protected override void AnimateCamera(Camera camera, Vector3 targetPosition, float duration, Ease ease, UnityAction onComplete)
        {
            Color fadeColor = m_FadeColor.Value;
            int sortingOrder = m_SortingOrder.Value;

            CameraManager.Instance.FadeToPosition(camera, targetPosition, fadeColor, duration, ease, sortingOrder, onComplete);
        }

        protected override void AnimateCamera(Camera camera, GameObject targetObject, float duration, Ease ease, UnityAction onComplete)
        {
            Color fadeColor = m_FadeColor.Value;
            int sortingOrder = m_SortingOrder.Value;

            CameraManager.Instance.FadeToPosition(camera, targetObject.transform.position, fadeColor, duration, ease, sortingOrder, onComplete);
        }
    }

}