using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera/Swipe",
                 "Activate swipe panning mode where clicking and dragging will pan the camera between viewA & viewB.")]
    [AddComponentMenu("Fungus/Commands/Camera/Swipe/Activate Camera Swipe Mode")]
    public class ActivateCameraSwipeMode : AnimateCameraCommand 
    {
        [Tooltip("Defines one extreme of the scrollable area that the player can pan around")]
        [FormerlySerializedAs("_viewA")]
        [CheckNotNullData]
        public ViewData m_ViewA = new ViewData();

        [Tooltip("Defines one extreme of the scrollable area that the player can pan around")]
        [FormerlySerializedAs("_viewB")]
        [CheckNotNullData]
        public ViewData m_ViewB = new ViewData();
        
        [Tooltip("Multiplier factor for speed of swipe pan")]
        [DataMin(0)]
        [FormerlySerializedAs("_speedMultiplier")]
        public FloatData m_SpeedMultiplier = new FloatData(10);

        protected override void AnimateCamera(Camera camera, float duration, Ease ease, UnityAction onComplete)
        {
            View viewA = m_ViewA.Value;
            View viewB = m_ViewB.Value;
            float speedMultiplier = m_SpeedMultiplier.Value;
            
            CameraManager.Instance.ActivateCameraSwipeMode(camera, viewA, viewB, speedMultiplier, duration, ease, onComplete);
        }

        public override string GetSummary()
        {
            string swipeString = "";
            if (m_MainCamera.IsConstant && !m_MainCamera.Value)
            {
                swipeString = "swipe ";
            }
            
            return base.GetSummary() + string.Format(" {0}{1} to {2}", swipeString, m_ViewA, m_ViewB) + GetDurationSummary(m_Duration);
        }
    }

}