﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Set how the camera will sort sprites on the same sorting layer, sort order, and z position.")]
    [AddComponentMenu("Fungus/Commands/Camera/Set Camera Transparency Sorting")]
    public class SetCameraTransparencySorting : MainCameraCommand
    {
        [Tooltip("Use main camera")]
        [FormerlySerializedAs("_useMainCamera")]
        public TransparencySortModeData m_TransparencySortMode = new TransparencySortModeData();

        [Tooltip("Camera that will follow the object")]
        [FormerlySerializedAs("_camera")]
        public Vector3Data m_TransparencySortAxis = new Vector3Data();

        protected override void ControlCamera(Camera camera)
        {
            TransparencySortMode transparencySortMode = m_TransparencySortMode.Value;
            Vector3 transparencySortAxis = m_TransparencySortAxis.Value;

            camera.transparencySortMode = transparencySortMode;
            camera.transparencySortAxis = transparencySortAxis;
        }
        
        public override string GetSummary()
        {
            string transparencySortAxisString = "";
            if (!m_TransparencySortMode.IsConstant || m_TransparencySortMode.Value == TransparencySortMode.CustomAxis)
            {
                transparencySortAxisString = " " + m_TransparencySortAxis;
            }

            return base.GetSummary() + string.Format(" to {0}{1}", m_TransparencySortMode, transparencySortAxisString);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TransparencySortAxis))
            {
                if (m_TransparencySortMode.IsConstant && m_TransparencySortMode.Value != TransparencySortMode.CustomAxis)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }
}
