﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera/Follow",
                 "Make the camera follow an object.")]
    [AddComponentMenu("Fungus/Commands/Camera/Follow/Start Following With Camera")]
    public class StartFollowingWithCamera : AnimateCameraCommand
    {
        [Tooltip("GameObject to follow")]
        [FormerlySerializedAs("_targetObject")]
        [CheckNotNullData]
        public GameObjectData m_TargetObject = new GameObjectData();
        
        protected override void AnimateCamera(Camera camera, float duration, Ease ease, UnityAction onComplete)
        {
            GameObject targetObject = m_TargetObject.Value;

            CameraManager.Instance.StartFollowingWithCamera(camera, targetObject, duration, ease, onComplete);
        }
        
        public override string GetSummary()
        {
            string followString = "";
            if (m_MainCamera.IsConstant && !m_MainCamera.Value)
            {
                followString = "follow ";
            }

            string targetString = string.Format("\"{0}\"", m_TargetObject);

            return base.GetSummary() + string.Format(" {0}{1}", followString, targetString) + GetDurationSummary(m_Duration);
        }
    }
}
