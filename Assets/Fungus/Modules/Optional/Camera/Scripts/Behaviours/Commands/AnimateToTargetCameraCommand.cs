﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{
    
    public abstract class AnimateToTargetCameraCommand : AnimateCameraCommand
    {
        [Tooltip("The target the camera should fade to")]
        [FormerlySerializedAs("_cameraTarget")]
        [FormerlySerializedAs("m_Target")]
        public TargetType m_CameraTarget;

        [Tooltip("View to transition to when Fade is complete")]
        [FormerlySerializedAs("_targetView")]
        public ViewData m_TargetView = new ViewData();

        [Tooltip("Position to move to when Fade is complete")]
        [FormerlySerializedAs("_targetPosition")]
        public Vector3Data m_TargetPosition = new Vector3Data();

        [Tooltip("GameObject to move to when Fade is complete")]
        [FormerlySerializedAs("_targetObject")]
        public GameObjectData m_TargetObject = new GameObjectData();
        
        protected sealed override void AnimateCamera(Camera camera, float duration, Ease ease, UnityAction onComplete)
        {
            View targetView = m_TargetView.Value;
            Vector3 targetPosition = m_TargetPosition.Value;
            GameObject targetObject = m_TargetObject.Value;

            switch (m_CameraTarget)
            {
                case TargetType.View:
                    AnimateCamera(camera, targetView, duration, ease, ContinueAfterFinished);
                    break;
                case TargetType.Position:
                    AnimateCamera(camera, targetPosition, duration, ease, ContinueAfterFinished);
                    break;
                case TargetType.GameObject:
                    AnimateCamera(camera, targetObject, duration, ease, ContinueAfterFinished);
                    break;
            }
        }

        protected abstract void AnimateCamera(Camera camera, View targetView, float duration, Ease ease, UnityAction onComplete);

        protected abstract void AnimateCamera(Camera camera, Vector3 targetPosition, float duration, Ease ease, UnityAction onComplete);

        protected abstract void AnimateCamera(Camera camera, GameObject targetObject, float duration, Ease ease, UnityAction onComplete);

        public override string GetSummary()
        {
            string targetString = "";
            switch (m_CameraTarget)
            {
                case TargetType.View:
                    targetString = string.Format("\"{0}\"", m_TargetView);
                    break;
                case TargetType.Position:
                    targetString = string.Format("{0}", m_TargetPosition);
                    break;
                case TargetType.GameObject:
                    targetString = string.Format("\"{0}\"", m_TargetObject);
                    break;
            }

            return base.GetSummary() + string.Format(" to {0}", targetString) + GetDurationSummary(m_Duration);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetView))
            {
                if (m_CameraTarget == TargetType.View)
                {
                    if ((Application.isPlaying || m_TargetView.IsConstant) && m_TargetView.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }
            if (propertyPath == nameof(m_TargetObject))
            {
                if (m_CameraTarget == TargetType.GameObject)
                {
                    if ((Application.isPlaying || m_TargetObject.IsConstant) && m_TargetObject.Value == null)
                    {
                        return "No Target Object selected";
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_TargetView))
            {
                if (m_CameraTarget != TargetType.View)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetPosition))
            {
                if (m_CameraTarget != TargetType.Position)
                {
                    return false;
                }
            }
            if (propertyPath == nameof(m_TargetObject))
            {
                if (m_CameraTarget != TargetType.GameObject)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}