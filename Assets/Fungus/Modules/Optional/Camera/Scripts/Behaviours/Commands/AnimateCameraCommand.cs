﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class AnimateCameraCommand : MainCameraCommand
    {
        public enum TargetType
        {
            Position,
            GameObject,
            View,
        }

        [Tooltip("The duration of the camera animation")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData();
        
        [Tooltip("The ease type of the camera animation")]
        [FormerlySerializedAs("_ease")]
        public EaseData m_Ease = new EaseData(Ease.InOutQuad);

        public override void OnEnter()
        {
            ControlCamera(GetCamera());

            if (!WillWait())
            {
                Continue();
            }
        }

        protected sealed override void ControlCamera(Camera camera)
        {
            float duration = m_Duration.Value;
            Ease ease = m_Ease.Value;
            
            AnimateCamera(camera, duration, ease, ContinueAfterFinished);
        }

        protected abstract void AnimateCamera(Camera camera, float duration, Ease ease, UnityAction onComplete);
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }

            return base.ShouldWait(runtime);
        }
        

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Ease))
            {
                if (m_Duration.IsConstant && m_Duration.Value <= 0)
                {
                    return false;
                }
            }

            return base.IsPropertyVisible(propertyPath);
        }

        public override int GetPropertyOrder(string propertyPath)
        {
            if (propertyPath == nameof(m_Duration))
            {
                return 1;
            }
            if (propertyPath == nameof(m_Ease))
            {
                return 1;
            }

            return base.GetPropertyOrder(propertyPath);
        }
    }

}