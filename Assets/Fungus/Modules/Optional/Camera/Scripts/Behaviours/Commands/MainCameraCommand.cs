﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;

namespace Fungus.Commands
{

    public abstract class MainCameraCommand : Command
    {
        [Tooltip("Control the main camera")]
        [FormerlySerializedAs("_useMainCamera")]
        [FormerlySerializedAs("m_UseMainCamera")]
        public BooleanData m_MainCamera = new BooleanData(true);

        [Tooltip("The camera to control")]
        [FormerlySerializedAs("_camera")]
        public CameraData m_Camera = new CameraData();

        public override void OnEnter()
        {
            ControlCamera(GetCamera());

            Continue();
        }

        protected Camera GetCamera()
        {
            bool useMainCamera = m_MainCamera.Value;
            Camera camera = m_Camera.Value;

            if (useMainCamera)
            {
                camera = Camera.main;
                if (camera == null)
                {
                    camera = GameObject.FindObjectOfType<Camera>();
                }
            }

            return camera;
        }

        protected abstract void ControlCamera(Camera camera);
        
        public override string GetSummary()
        {
            string cameraString = "";
            if (m_MainCamera.IsConstant && !m_MainCamera.Value)
            {
                cameraString = "\"" + m_Camera.ToString() + "\"" + " ";
            }

            return string.Format("{0}", cameraString);
        }

        public override string GetPropertyError(string propertyPath)
        {
            if (propertyPath == nameof(m_Camera))
            {
                if ((Application.isPlaying || m_MainCamera.IsConstant) && !m_MainCamera.Value)
                {
                    if ((Application.isPlaying || m_Camera.IsConstant) && m_Camera.Value == null)
                    {
                        return CheckNotNullDataAttribute.GetDefaultErrorMessage(propertyPath);
                    }
                }
            }

            return base.GetPropertyError(propertyPath);
        }

        public override bool IsPropertyVisible(string propertyPath)
        {
            if (propertyPath == nameof(m_Camera))
            {
                if (m_MainCamera.IsConstant && m_MainCamera.Value)
                {
                    return false;
                }
            }
            return base.IsPropertyVisible(propertyPath);
        }
    }

}