﻿using UnityEngine;
using UnityEngine.Serialization;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera/Follow",
                 "Make the camera stop following an object.")]
    [AddComponentMenu("Fungus/Commands/Camera/Follow/Stop Following With Camera")]
    public class StopFollowingWithCamera : MainCameraCommand
    {
        protected override void ControlCamera(Camera camera)
        {
            CameraManager.Instance.StopFollowingWithCamera(camera);
        }
    }

}