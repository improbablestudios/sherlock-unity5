using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Shake the camera.")]
    [AddComponentMenu("Fungus/Commands/Camera/Shake Camera")]
    public class ShakeCamera : MainCameraCommand 
    {
        [Tooltip("Time for camera shake effect to complete")]
        [DataMin(0)]
        [FormerlySerializedAs("_duration")]
        public FloatData m_Duration = new FloatData(0.5f);

        [Tooltip("Magnitude of shake effect in x & y axes")]
        [FormerlySerializedAs("_strength")]
        public Vector2Data m_Strength = new Vector2Data(new Vector2(3,3));

        [Tooltip("How much will the shake vibrate")]
        [DataMin(0)]
        [FormerlySerializedAs("_vibrato")]
        public IntegerData m_Vibrato = new IntegerData(10);

        [Tooltip("How much the shake will be random (Setting it to 0 will shake along a single direction)")]
        [DataRange(0, 180)]
        [FormerlySerializedAs("_randomness")]
        public FloatData m_Randomness = new FloatData(90f);

        [Tooltip("Shake will automatically fade out smoothly within the tween's duration")]
        [FormerlySerializedAs("_fadeOutShake")]
        public BooleanData m_FadeOutShake = new BooleanData(true);
        
        public override void OnEnter()
        {
            ControlCamera(GetCamera());

            if (!WillWait())
            {
                Continue();
            }
        }
        
        protected override void ControlCamera(Camera camera)
        {
            float duration = m_Duration.Value;
            Vector2 strength = m_Strength.Value;
            int vibrato = m_Vibrato.Value;
            float randomness = m_Randomness.Value;
            bool fadeOutShake = m_FadeOutShake.Value;

            Camera.main.DOShakePosition(duration, strength, vibrato, randomness, fadeOutShake)
                .OnComplete(ContinueAfterFinished);
        }

        public override string GetSummary()
        {
            return GetDurationSummary(m_Duration, "for");
        }
        
        public override bool ShouldWait(bool runtime)
        {
            if ((!runtime && !m_Duration.IsConstant) || m_Duration.Value > 0)
            {
                return true;
            }
            return base.ShouldWait(runtime);
        }
    }

}