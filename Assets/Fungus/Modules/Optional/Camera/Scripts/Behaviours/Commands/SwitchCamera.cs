﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Fungus.Variables;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Make a camera the main camera.")]
    [AddComponentMenu("Fungus/Commands/Camera/Switch Camera")]
    public class SwitchCamera : Command
    {
        [Tooltip("Camera to switch to")]
        [FormerlySerializedAs("_camera")]
        [CheckNotNullData]
        public CameraData m_Camera = new CameraData();

        public override void OnEnter()
        {
            Camera camera = m_Camera.Value;
            
            if (Camera.main != null)
            {
                Camera.main.gameObject.SetActive(false);
            }
            camera.gameObject.SetActive(true);
            camera.tag = "MainCamera";

            Continue();
        }

        public override string GetSummary()
        {
            return string.Format("to \"{0}\"", m_Camera);
        }
    }

}