﻿using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using UnityEngine.Events;
using Fungus.Variables;
using ImprobableStudios.CameraUtilities;

namespace Fungus.Commands
{

    [CommandInfo("Camera",
                 "Zoom the camera.")]
    [AddComponentMenu("Fungus/Commands/Camera/Zoom Camera")]
    public class ZoomCamera : AnimateCameraCommand
    {
        [Tooltip("Amount to zoom by")]
        [DataMin(0)]
        [FormerlySerializedAs("_zoomLevel")]
        public FloatData m_ZoomLevel = new FloatData(1f);

        protected override void AnimateCamera(Camera camera, float duration, Ease ease, UnityAction onComplete)
        {
            float zoomLevel = m_ZoomLevel.Value;

            CameraManager.Instance.Zoom(camera, zoomLevel, duration, ease, onComplete);
        }

        public override string GetSummary()
        {
            return base.GetSummary() + string.Format(" to {0}", m_ZoomLevel) + GetDurationSummary(m_Duration);
        }
    }

}