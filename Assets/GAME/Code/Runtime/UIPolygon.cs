/// Credit CiaccoDavide
/// Sourced from - http://ciaccodavi.de/unity/uipolygon

using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Primitives/UI Polygon")]
    public class UIPolygon : MaskableGraphic
    {
        [SerializeField]
        private Texture m_Texture;

        [SerializeField]
        private bool m_Fill = true;

        [SerializeField]
        private float m_Thickness = 5;

        [SerializeField]
        [Range(3, 360)]
        private int m_Sides = 3;

        [SerializeField]
        [Range(0, 360)]
        private float m_Rotation = 0;

        [SerializeField]
        [Range(0, 1)]
        private float[] m_VerticesDistances = new float[3];

        private float m_size = 0;
        private float[] m_previousVerticesDistances = new float[0];

        public Texture Texture
        {
            get
            {
                return m_Texture;
            }
            set
            {
                m_Texture = value;
                Validate();
            }
        }

        public bool Fill
        {
            get
            {
                return m_Fill;
            }
            set
            {
                m_Fill = value;
                Validate();
            }
        }

        public float Thickness
        {
            get
            {
                return m_Thickness;
            }
            set
            {
                m_Thickness = value;
                Validate();
            }
        }

        public int Sides
        {
            get
            {
                return m_Sides;
            }
            set
            {
                m_Sides = value;
                Validate();
            }
        }

        public float Rotation
        {
            get
            {
                return m_Rotation;
            }
            set
            {
                m_Rotation = value;
                Validate();
            }
        }

        public float[] VerticesDistances
        {
            get
            {
                return m_VerticesDistances;
            }
            set
            {
                m_VerticesDistances = value;
                Validate();
            }
        }

        public override Texture mainTexture
        {
            get
            {
                return m_Texture == null ? s_WhiteTexture : m_Texture;
            }
        }

        public Texture texture
        {
            get
            {
                return m_Texture;
            }
            set
            {
                if (m_Texture == value) return;
                m_Texture = value;
                SetVerticesDirty();
                SetMaterialDirty();
            }
        }

        protected virtual void Validate()
        {
#if UNITY_EDITOR
            OnValidate();
#endif
        }

        private void Update()
        {
            m_size = rectTransform.rect.width;
            if (rectTransform.rect.width > rectTransform.rect.height)
                m_size = rectTransform.rect.height;
            else
                m_size = rectTransform.rect.width;
            m_Thickness = (float)Mathf.Clamp(m_Thickness, 0, m_size / 2);

            if (m_VerticesDistances.Length != m_previousVerticesDistances.Length || !m_VerticesDistances.SequenceEqual(m_previousVerticesDistances))
            {
                Validate();
            }
            m_previousVerticesDistances = m_VerticesDistances.ToArray();
        }

        public void DrawPolygon(int _sides)
        {
            m_Sides = _sides;
            m_VerticesDistances = new float[_sides + 1];
            for (int i = 0; i < _sides; i++) m_VerticesDistances[i] = 1; ;
            m_Rotation = 0;
        }

        public void DrawPolygon(int _sides, float[] _VerticesDistances)
        {
            m_Sides = _sides;
            m_VerticesDistances = _VerticesDistances;
            m_Rotation = 0;
        }

        public void DrawPolygon(int _sides, float[] _VerticesDistances, float _rotation)
        {
            m_Sides = _sides;
            m_VerticesDistances = _VerticesDistances;
            m_Rotation = _rotation;
        }

        protected UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs)
        {
            UIVertex[] vbo = new UIVertex[4];
            for (int i = 0; i < vertices.Length; i++)
            {
                var vert = UIVertex.simpleVert;
                vert.color = color;
                vert.position = vertices[i];
                vert.uv0 = uvs[i];
                vbo[i] = vert;
            }
            return vbo;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            Vector2 prevX = Vector2.zero;
            Vector2 prevY = Vector2.zero;
            Vector2 uv0 = new Vector2(0, 0);
            Vector2 uv1 = new Vector2(0, 1);
            Vector2 uv2 = new Vector2(1, 1);
            Vector2 uv3 = new Vector2(1, 0);
            Vector2 pos0;
            Vector2 pos1;
            Vector2 pos2;
            Vector2 pos3;
            float degrees = 360f / m_Sides;
            int vertices = m_Sides + 1;
            if (m_VerticesDistances.Length != vertices)
            {
                m_VerticesDistances = new float[vertices];
                for (int i = 0; i < vertices - 1; i++) m_VerticesDistances[i] = 1;
            }
            // last vertex is also the first!
            m_VerticesDistances[vertices - 1] = m_VerticesDistances[0];
            for (int i = 0; i < vertices; i++)
            {
                float outer = -rectTransform.pivot.x * m_size * m_VerticesDistances[i];
                float inner = -rectTransform.pivot.x * m_size * m_VerticesDistances[i] + m_Thickness;
                float rad = Mathf.Deg2Rad * (i * degrees + m_Rotation);
                float c = Mathf.Cos(rad);
                float s = Mathf.Sin(rad);
                uv0 = new Vector2(0, 1);
                uv1 = new Vector2(1, 1);
                uv2 = new Vector2(1, 0);
                uv3 = new Vector2(0, 0);
                pos0 = prevX;
                pos1 = new Vector2(outer * c, outer * s);
                if (m_Fill)
                {
                    pos2 = Vector2.zero;
                    pos3 = Vector2.zero;
                }
                else
                {
                    pos2 = new Vector2(inner * c, inner * s);
                    pos3 = prevY;
                }
                prevX = pos1;
                prevY = pos2;
                vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));
            }
        }
    }
}
