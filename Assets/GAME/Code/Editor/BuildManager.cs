#if UNITY_EDITOR
using UnityEditor;
using System;
using ImprobableStudios.SceneUtilities;

public class BuildManager
{
    [MenuItem("Build/Build All", false, 0)]
    static void BuildAll()
    {
        string buildPath = GetBuildPath();
        BuildOptions buildOptions = GetBuildOptions();

        Build(buildPath, buildOptions, BuildTarget.WebGL, BuildTargetGroup.WebGL);
        Build(buildPath, buildOptions, BuildTarget.StandaloneWindows64, BuildTargetGroup.Standalone);
        Build(buildPath, buildOptions, BuildTarget.StandaloneOSX, BuildTargetGroup.Standalone);
    }

    [MenuItem("Build/Build WebGL", false, 0)]
    public static void BuildWebGL()
    {
        Build(GetBuildPath(), GetBuildOptions(), BuildTarget.WebGL, BuildTargetGroup.WebGL);
    }

    [MenuItem("Build/Build Windows", false, 0)]
    public static void BuildWindows()
    {
        Build(GetBuildPath(), GetBuildOptions(), BuildTarget.StandaloneWindows64, BuildTargetGroup.Standalone);
    }

    [MenuItem("Build/Build OSX", false, 0)]
    public static void BuildOSX()
    {
        Build(GetBuildPath(), GetBuildOptions(), BuildTarget.StandaloneOSX, BuildTargetGroup.Standalone);
    }
    
    [MenuItem("Build/Update Scenes In Build", false, 20)]
    public static void UpdateScenesInBuild()
    {
        foreach (string guid in AssetDatabase.FindAssets("t:scene"))
        {
            BuildUtilities.AddSceneToBuild(AssetDatabase.GUIDToAssetPath(guid));
        }
        BuildUtilities.UpdateSceneSelectors();
    }

    public static void Build(string buildPath, BuildOptions buildOptions, BuildTarget buildTarget, BuildTargetGroup buildTargetGroup)
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        buildPlayerOptions.locationPathName = buildPath;
        buildPlayerOptions.options = buildOptions;
        buildPlayerOptions.target = buildTarget;
        buildPlayerOptions.targetGroup = buildTargetGroup;
        buildPlayerOptions.scenes = BuildUtilities.GetScenePaths(true);

        if (buildTarget == BuildTarget.WebGL)
        {
            if (buildOptions == BuildOptions.Development)
            {
                PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.FullWithStacktrace;
            }
            else
            {
                PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
            }
        }
        
        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }

    static string GetBuildPath()
    {
        string buildPath = GetArg("-buildPath");
        if (buildPath == null)
        {
            buildPath = EditorUtility.SaveFolderPanel("Build", "", "Build");
        }

        return buildPath;
    }

    static BuildOptions GetBuildOptions()
    {
        string buildOptionsString = GetArg("-buildOptions");
        BuildOptions buildOptions = BuildOptions.None;
        if (buildOptionsString != null)
        {
            buildOptions = (BuildOptions)Enum.Parse(typeof(BuildOptions), buildOptionsString);
        }

        return buildOptions;
    }

    // Helper function for getting the command line arguments
    static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }
        return null;
    }
}
#endif