#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

[InitializeOnLoad]
public static class CleanupEmptyFolders
{
    static CleanupEmptyFolders()
    {
        Cleanup();
    }
        
    private static void Cleanup()
    {
        if (!EditorApplication.isPlayingOrWillChangePlaymode)
        {
            var directoryInfo = new DirectoryInfo(Application.dataPath);
            foreach (var subDirectory in directoryInfo.GetDirectories("*.*", SearchOption.AllDirectories))
            {
                if (subDirectory.Exists)
                {
                    ScanDirectory(subDirectory);
                }
            }
        }
    }

    private static void ScanDirectory(DirectoryInfo subDirectory)
    {
        var filesInSubDirectory = subDirectory.GetFiles("*.*", SearchOption.AllDirectories);

        if (filesInSubDirectory.Length == 0 ||
            !filesInSubDirectory.Any(t => t.FullName.EndsWith(".meta") == false))
        {
            subDirectory.Delete(true);
        }
    }
}
#endif