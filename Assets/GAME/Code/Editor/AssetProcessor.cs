#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class TexturePreProcessor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        if (!assetPath.StartsWith("Assets/GAME"))
        {
            return;
        }

        Texture texture = AssetDatabase.LoadAssetAtPath<Texture>(assetPath);
        TextureImporter textureImporter = assetImporter as TextureImporter;

        TextureImporterSettings settings = new TextureImporterSettings();
        textureImporter.ReadTextureSettings(settings);
        settings.textureType = TextureImporterType.Sprite;
        settings.spritePixelsPerUnit = 32;

        textureImporter.SetTextureSettings(settings);

        if (texture != null)
        {
            TextureImporterPlatformSettings webGLPlatformSettings = textureImporter.GetPlatformTextureSettings("WebGL");
            webGLPlatformSettings.overridden = true;
            webGLPlatformSettings.maxTextureSize = 1024;
            webGLPlatformSettings.format = TextureImporterFormat.DXT5Crunched;
            webGLPlatformSettings.crunchedCompression = true;
            webGLPlatformSettings.compressionQuality = 100;
            textureImporter.SetPlatformTextureSettings(webGLPlatformSettings);

            TextureImporterPlatformSettings standalonePlatformSettings = textureImporter.GetPlatformTextureSettings("Standalone");
            standalonePlatformSettings.overridden = true;
            standalonePlatformSettings.maxTextureSize = 2048;
            standalonePlatformSettings.format = TextureImporterFormat.BC7;
            textureImporter.SetPlatformTextureSettings(standalonePlatformSettings);
        }
    }
}

public class SceneProcessor : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (string path in importedAssets)
        {
            if (path.EndsWith(".unity"))
            {
                BuildManager.UpdateScenesInBuild();
            }
        }

        foreach (string path in deletedAssets)
        {
            if (path.EndsWith(".unity"))
            {
                BuildManager.UpdateScenesInBuild();
            }
        }

        foreach (string path in movedAssets)
        {
            if (path.EndsWith(".unity"))
            {
                BuildManager.UpdateScenesInBuild();
            }
        }
    }
}
#endif