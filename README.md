SHERLOCK: THE GAME IS ON
=========================

An easy to use visual editor for Sherlock: The Game Is On

### To use the visual editor, you'll need the following programs: ###

- Sourcetree (latest version): 
    - https://www.sourcetreeapp.com/download/
- Unity (latest version):
    - http://unity3d.com/get-unity/download/archive

### TUTORIAL ###

https://sites.google.com/site/improbablestudios/editor